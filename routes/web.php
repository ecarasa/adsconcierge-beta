<?php
use App\Dashboard;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/


Route::post('/get-logout', 'Auth\LoginController@getLogout');

/* Route::get('/test-remote-log', function() {
	Log::channel('ovhlogs')->info('Prueba de mensaje desde route',  [ '_context' => null, '_action' => null, 'short_message' => "", 'short_message' => "" ]);
	//Log::channel('custom')->info('Prueba de custom desde route',  [ '_context' => null, '_action' => null, 'short_message' => "", 'short_message' => "" ]);
	//Log::channel('ovhlogsmonolog')->info('Prueba de ovhlogsmonolog desde route',  [ '_context' => null, '_action' => null, 'short_message' => "", 'short_message' => "" ]);
}); */
/* 
	// emiliano test
	Route::get('/test-remote-log', function() { 
		Log::channel('ovhlogs')->info('Prueba de mensaje desde route',  [ '_context' => null, '_action' => null, 'short_message' => "", 'short_message' => "" ]);
	}); 
*/

Route::get('/clear-all', function() {
	Artisan::call('cache:clear');
	Artisan::call('route:clear');
	Artisan::call('config:clear');
	Artisan::call('view:clear');
	return "All cleared";
});

Auth::routes([
    'login'    => true, 
    'logout'   => true, 
    'register' => false, 
    'reset'    => true,   // for resetting passwords
    'confirm'  => false,  // for additional password confirmations
    'verify'   => false,  // for email verification
]);

Route::get('/blank', function(){ return view('template.blank', [ 'title' => 'Página en blanco' ]); });

Route::group([ 'middleware' => [ 'auth', 'web' ] ], function() {

	Route::get('/home/{campaing_array}', 'HomeController@index')->name('home');
	Route::get('/', 'HomeController@index')->name('index');

	/****************************************
	 * User
	 ****************************************/
/**
    desactivado jr, son vistas que no se usan
  la vista de users, existe y se usara para sub-users

  
	Route::get('/users', function () {
	    return view('template.users',[
	    	'title' 		=>	'Users',
	    	'newElement'	=>	'New user',
	    	'columns' 		=>	[ 'id', 'name','email', 'rol', 'Actions' ],
	    	'api'			=>	'/api/users',
	    	'base' 			=>	'users',
	    	'entity'		=>	'Usuario'
	    ]);
	})->name('userlist');
  

  
	Route::get('/users/new', 'UserController@new')->name('newuser');
	Route::get('/users/{id}/edit', 'UserController@edit')->name('edituser');
	Route::get('/users/{id}/delete', 'UserController@delete')->name('deleteuser');
	Route::post('/users/create', 'UserController@store')->name('createuser');
	Route::post('/users/update', 'UserController@update')->name('updateuser');
**/
	Route::get('/perfil', 'UserController@perfil')->name('perfil');
	Route::post('/update_perfil', 'UserController@update_perfil')->name('update_perfil');
	Route::get('/delete_perfil', 'UserController@delete_perfil')->name('delete_perfil');
	/****************************************
	 * Fin User
	****************************************/

	/****************************************
	 * Customers
	 ****************************************/
	Route::get('/customers', 'CustomerController@show')->name('clientslist');
	Route::get('/customers/new', 'CustomerController@new')->name('newclients');
	Route::get('/customers/{id}/edit', 'CustomerController@edit')->name('editclients');
	
	Route::post('/customers/edit/name', 'CustomerController@editName')->name('editName');
	Route::post('/customers/edit/defaultCustomer', 'CustomerController@defaultCustomer')->name('defaultCustomer');

	Route::post('/customers/create', 'CustomerController@store')->name('createclient');
	Route::post('/customers/update', 'CustomerController@update')->name('updateclient');
	/****************************************
	 * END Customers
	****************************************/

	Route::get('/connections/{text?}', 'HomeController@connections')->name('connections');
	Route::get('/ads_accounts/{id?}', 'HomeController@ads_accounts')->name('ads_accounts');
	Route::get('/properties/{id?}', 'HomeController@properties')->name('properties');

	/****************************************
	 * Campañas
	 ****************************************/
	Route::get('/campana', 'HomeController@campana')->name('campana');
	Route::get('/campaigns', 'HomeController@campaigns')->name('campaigns');

	/****************************************
	 * Fin Campañas
	 ****************************************/

	/******************************************
	 * Graficas
	 ****************************************/
	/*Route::get('/', function () {
	    return view('template.reporting',[
	    	'title' 		=>	'reporting',
	    	'newElement'	=>	'New user',
	    	'columns' 		=>	[ 'id', 'name','email', 'rol', 'Actions' ],
	    	'api'			=>	'/api/users',
	    	'base' 			=>	'users',
	    	'entity'		=>	'Usuario'
	    ]);
	})->name('index');	*/
	
	Route::get('/reporting', function () {

        $user_id = Auth::user()->id;

        $dashboard = Dashboard::where('user_id', $user_id)->get();

	    return view('template.reporting',[
	    	'title' 		=>	'reporting',
	    	'newElement'	=>	'New user',
	    	'columns' 		=>	[ 'id', 'name','email', 'rol', 'Actions' ],
	    	'api'			=>	'/api/users',
	    	'base' 			=>	'users',
	    	'entity'		=>	'Usuario',
            'dashboards' => $dashboard
	    ]);
	})->name('reporting');
	
	Route::get('/reporting2', function () {
	    return view('template.reporting2',[
	    	'title' 		=>	'reporting 2',
	    	'newElement'	=>	'New user',
	    	'columns' 		=>	[ 'id', 'name','email', 'rol', 'Actions' ],
	    	'api'			=>	'/api/users',
	    	'base' 			=>	'users',
	    	'entity'		=>	'Usuario'
	    ]);
	})->name('reporting2');

	Route::get('/reporting2/{idGraph}', function ($idGraph) {
	    return view('template.reporting2',[
	    	'title' 		=>	'reporting 2 b',
	    	'newElement'	=>	'New user',
	    	'columns' 		=>	[ 'id', 'name','email', 'rol', 'Actions' ],
	    	'api'			=>	'/api/users',
	    	'base' 			=>	'users',
	    	'entity'		=>	'Usuario',
	    	'idGraph'		=>  $idGraph
	    ]);
	})->name('reporting2');

	Route::get('/reporting3/{public_id?}', 'ReportController@reporting3')->name('reporting3');

	/****************************************
	 * Fin Graficas
	 ****************************************/

    /****************************************
     * Organic
     ****************************************/

    Route::prefix('/organic')->group( function ()
    {
        /*
         * POSTS
         */
        Route::get('/', 'OrganicController@index')->name('organic_home');
        Route::post('/getPostsByPeriod', 'OrganicController@postsByPeriod')->name('organic_posts_by_period');
        Route::get('/getPosts', 'OrganicController@getPosts')->name('organic_get_posts');
		Route::get('/getPost/{id}', 'OrganicController@getPostElement')->name('organic_get_post');
        Route::get('/post', 'OrganicController@postForm')->name('organic_create_post_form');
        Route::post('/post', 'OrganicController@savePost')->name('organic_create_post');
        Route::get('/post/{id}', 'OrganicController@postForm')->name('organic_create_post_form');
        Route::post('/post/{id}', 'OrganicController@savePost')->name('organic_create_post');

        /*
         * TAGS
         */
        Route::get('/tags', 'OrganicController@tags')->name('organic_tags');
        Route::get('/getTags', 'OrganicController@getTags')->name('organic_get_tags');
        Route::get('/getTag/{id}', 'OrganicController@getTagElement')->name('organic_get_tag');
        Route::get('/tag', 'OrganicController@tagForm')->name('organic_create_tag_form');
        Route::post('/tag', 'OrganicController@saveTag')->name('organic_create_tag');
        Route::get('/tag/{id}', 'OrganicController@tagForm')->name('organic_create_tag_form');
        Route::post('/tag/{id}', 'OrganicController@saveTag')->name('organic_create_tag');

        /*
         * SETTINGS
         */
        Route::get('/settings', 'OrganicController@settings')->name('organic_settings');
        Route::get('/getSettings', 'OrganicController@getSettings')->name('organic_get_settings');
        Route::get('/getSetting/{id}', 'OrganicController@getSettingElement')->name('organic_get_setting');
        Route::get('/setting', 'OrganicController@settingForm')->name('organic_create_setting_form');
        Route::post('/setting', 'OrganicController@saveSetting')->name('organic_create_setting');
        Route::get('/setting/{id}', 'OrganicController@settingForm')->name('organic_create_setting_form');
        Route::post('/setting/{id}', 'OrganicController@saveSetting')->name('organic_create_setting');
    });

    /****************************************
     * Fin Organic
     ****************************************/
	
    //Route::get('/createSubscription', 'SubscriptionController@create')->name('createSubscription');
	// desactivado jr porque esto esta en /api
  //Route::get("/chargebee/events", "ChargeBeeController@handleRequest")->name("charge_bee_weebhook");	


});
Route::get('/callback/{platform}/', 'PlatformsCallback@callbackSocialApi');
Route::get('/callback/{platform}/new', 'PlatformsCallback@callbackSocialApi');
Route::get('/callbackSocialApi', 'PlatformsCallback@callbackSocialApi');
//Route::get('/callbackSocialApi', 'Api\FunctionsController@callbackSocialApi');

Route::get("/subscription", "HomeController@subscription")->name("subscription");
Route::get("/mysubscription", "HomeController@subscription")->name("mysubscription");



//TODO
/*Route::get('/mysubscription', function () { return "TO DO";})->name('mysubscription');*/
//Route::get("/subscribed", "SubscribeController@subscribedChargebee")->name("subscribedChargebee");
//Route::get("/cancelSubscription", "HomeController@cancelSubscription")->name("cancelSubscription");
//Route::get("/changeSubscriptionPlan/{plan}", "HomeController@changeSubscriptionPlan")->name("changeSubscriptionPlan");
//Route::get("/planChosen/{plan}", "HomeController@planChosen")->name("planChosen");

