<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/blank', function(){ return view('template.blank', [ 'title' => 'Página en blanco' ]); });

Route::group([ 'middleware' => [ 'auth' ] ], function() {

	/****************************************
	 *
	 ****************************************/
	//Route::get('/', function () { return view('template.general', [ 'title' => 'Home' ]); });
	//Route::get('/logintemplate', function () { return view('template.login'); });
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/', 'HomeController@index')->name('index');
	/****************************************
	 *
	 ****************************************/

	/****************************************
	 * User
	 ****************************************/
	Route::get('/users', function () {		
	    return view('template.users',[
	    	'title' 		=>	'Users',
	    	'newElement'	=>	'New user',
	    	'columns' 		=>	[ 'id', 'name','email', 'rol', 'Actions' ],
	    	'api'			=>	'/api/users',
	    	'base' 			=>	'users',
	    	'entity'		=>	'Usuario'
	    ]);
	})->name('userlist');
	Route::get('/users/new', 'UserController@new')->name('newuser');
	Route::get('/users/{id}/edit', 'UserController@edit')->name('edituser');
	Route::get('/users/{id}/delete', 'UserController@delete')->name('deleteuser');
	Route::post('/users/create', 'UserController@store')->name('createuser');
	Route::post('/users/update', 'UserController@update')->name('updateuser');
	/****************************************
	 * Fin User
	 ****************************************/
	
	/****************************************
	 * Customers
	 ****************************************/
	Route::get('/customers', 'CustomerController@show')->name('customerslist');
	Route::get('/customers/new', 'CustomerController@new')->name('newcustomers');
	Route::get('/customers/{id}/edit', 'CustomerController@edit')->name('editcustomers');
	Route::post('/customers/create', 'CustomerController@store')->name('createcustomer');
	Route::post('/customers/update', 'CustomerController@update')->name('updatecustomer');
	/****************************************
	 * END Customers
	 ****************************************/

	
	Route::get('/connections', 'HomeController@connections')->name('connections');
	Route::get('/ads_accounts/{id?}', 'HomeController@ads_accounts')->name('ads_accounts');
	Route::get('/properties/{id?}', 'HomeController@properties')->name('properties');
	
	
	/****************************************
	 * Campañas
	 ****************************************/
	Route::get('/campana', 'HomeController@campana')->name('campana');
	
	/****************************************
	 * Fin Campañas
	 ****************************************/

	/******************************************
	 * Graficas
	 ****************************************/	
	/*Route::get('/', function () {
	    return view('template.reporting',[
	    	'title' 		=>	'reporting',
	    	'newElement'	=>	'New user',
	    	'columns' 		=>	[ 'id', 'name','email', 'rol', 'Actions' ],
	    	'api'			=>	'/api/users',
	    	'base' 			=>	'users',
	    	'entity'		=>	'Usuario'
	    ]);
	})->name('index');	*/
	Route::get('/reporting', function () {
	    return view('template.reporting',[
	    	'title' 		=>	'reporting',
	    	'newElement'	=>	'New user',
	    	'columns' 		=>	[ 'id', 'name','email', 'rol', 'Actions' ],
	    	'api'			=>	'/api/users',
	    	'base' 			=>	'users',
	    	'entity'		=>	'Usuario'
	    ]);
	})->name('reporting');
	Route::get('/reporting2', function () {
	    return view('template.reporting2',[
	    	'title' 		=>	'reporting 2',
	    	'newElement'	=>	'New user',
	    	'columns' 		=>	[ 'id', 'name','email', 'rol', 'Actions' ],
	    	'api'			=>	'/api/users',
	    	'base' 			=>	'users',
	    	'entity'		=>	'Usuario'
	    ]);
	})->name('reporting2');
	Route::get('/reporting2/{idGraph}', function ($idGraph) {
	    return view('template.reporting2',[
	    	'title' 		=>	'reporting 2 b',
	    	'newElement'	=>	'New user',
	    	'columns' 		=>	[ 'id', 'name','email', 'rol', 'Actions' ],
	    	'api'			=>	'/api/users',
	    	'base' 			=>	'users',
	    	'entity'		=>	'Usuario',
	    	'idGraph'		=>  $idGraph
	    ]);
	})->name('reporting2');
	
	Route::get('/reporting3/{public_id?}', 'ReportController@reporting3')->name('reporting3');
	
	/****************************************
	 * Fin Graficas
	 ****************************************/

	//TODO
	Route::get('/mysubscription', function () {
	    return "TO DO";
	})->name('mysubscription');
	
	Route::get('/damepermiso/{permiso}', function($permiso){ 
		if (Auth::check()) {
			$result = Auth::user()->damepermisos($permiso);
			if (is_bool($result)){
				return $result ? 'true' : 'false';
			}else{
				return $result;
			}
		}else{
			return "No hay usuario logueado";
		}
		
	});
	
	Route::get('/clear-all', function() {
		Artisan::call('cache:clear');
		Artisan::call('route:clear');
		Artisan::call('config:clear');
		Artisan::call('view:clear');
		return "All cleared";
	});

});
