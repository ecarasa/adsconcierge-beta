<?php

use Illuminate\Http\Request;
use App\Customer;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/







//https://pre.adsconcierge.com/api/callback/facebook    ?token=asdfghjklasdfghjkl
//Route::get('/callback/{platform}/', 'Api\FunctionsController@callbackSocialApi');
Route::get('/callback/{platform}/', 'PlatformsCallback@callbackSocialApi');





//Route::get('/callback/{facebook}/{aa}', 'Api\FunctionsController@callbackSocialApi');
//Route::any( '/callback/{platform}/*', function( $page ){    dd($page);});
//Route::get('/callback/{platform}/?{token}', function(Request $request){ dd($request); })->where('token', '[A-Za-z]+');
//Route::when('/callback/{platform}/*', 'admin', array('post'));
//Route::any('/callback/{platform}/{token}', function(Request $request){ dd($request); });




Route::middleware('auth:api')->get('/user', function (Request $request) { return next($request->user()); });

//list de customers y stats
Route::get('/accounts',  [  'uses' =>  'Api\AccountController@customersList' ]);
Route::get('/campaigns',  [  'uses' =>  'Api\AccountController@campaignsList' ]);

//entitys get/update
Route::get('/customer/{id}',  [  'uses' =>  'Api\EntityController@getCustomer' ]);

Route::get('/campaign/{id}',  [  'uses' =>  'Api\EntityController@getCampaign' ]);
Route::post('/campaign',  [  'uses' =>  'Api\EntityController@updateCampaign' ]);

Route::get('/campaign_platform/{id}',  [  'uses' =>  'Api\EntityController@getCampaignPlatform' ]);
Route::post('/campaignPlatform/update',  [  'uses' =>  'Api\EntityController@updateCampaignPlatform' ]);

Route::get('/atom/{id}',  [  'uses' =>  'Api\EntityController@getAtom' ]);
Route::post('/atom/update',  [  'uses' =>  'Api\EntityController@updateAtom' ]);

Route::get('/creativity/{id}',  [  'uses' =>  'Api\EntityController@getCreativity' ]);
Route::post('/creativity',  [  'uses' =>  'Api\EntityController@updateCreativity' ]);

Route::get('/entity/duplicate', 'Api\EntityController@duplicateEntity');

//chart 30 days
Route::get('/dashboard/chart-data/{entity}/{id}/{platform?}',  [  'uses' =>  'Api\AccountController@chartData' ]);

//vista by customer
Route::get('/dashboard/customer/campaigns/{id}',  [  'uses' =>  'Api\AccountController@customerCampaignsList' ]); // customer's campaigns OK
Route::get('/dashboard/customer/campaigns/byPlatform/{id}',  [  'uses' =>  'Api\AccountController@customerCampaignsListByPlatform' ]); // customer's campaigns OK
Route::get('/dashboard/customer/campaigns/byPlatformList/{id}',  [  'uses' =>  'Api\AccountController@customerCampaignsListByPlatformList' ]); // customer's campaigns OK
Route::get('/dashboard/customer/atoms/{id}',  [  'uses' =>  'Api\AccountController@customerAtoms' ]); //customer's atoms 
Route::get('/dashboard/customer/creativity/{id}',  [  'uses' =>  'Api\AccountController@customerCreativities' ]); // customer's creas 
Route::get('/dashboard/atom/creativity/{id}',  [  'uses' =>  'Api\AccountController@atomCreativities' ]); // customer's creas 

//vista by campaign
Route::get('/dashboard/data-platform/{id}',  [  'uses' =>  'Api\AccountController@campaignListByPlatForm' ]); // campaign's platform detail
Route::get('/dashboard/data-segmentations/{id}',  [  'uses' =>  'Api\AccountController@data_campaigns_platform' ]);//segmentacion de la campaña
Route::get('/dashboard/data-atom/{id}',  [  'uses' =>  'Api\AccountController@data_atomo' ]); // campaign's atoms
Route::get('/dashboard/data-creativity-by-campaign/{id}/{platformGet?}',  [  'uses' =>  'Api\AccountController@data_creativity_by_campaign' ]); // customer's creas // NO HAY STATS?? O DE DONDE VIENEN LAS STATS ??

//view campaign by platform
Route::get('/dashboard/data-platform-atom-list/{id}/{platform}',  [  'uses' =>  'Api\AccountController@data_atomo_by_platform' ]);
Route::get('/dashboard/data-platform-campaign-list/{id}/{platform}',  [  'uses' =>  'Api\AccountController@data_campaign_by_platform' ]);
Route::get('/dashboard/data-atom-campaign-list/{id}',  [  'uses' =>  'Api\AccountController@data_atomo_by_campaign_platform' ]);

//update para cualquier entity
Route::post('/dashboard/entity/update', 'Api\EntityController@update');

Route::get('/users',  [  'uses' =>  'Api\UserController@index' ]);
Route::post('/users/create', 'Api\UserController@create');
Route::get('/dashboard/campaigns/{id}',  [  'uses' =>  'Api\AccountController@campaigns' ]);
Route::get('/dashboard/campaigns/list/{id}',  [  'uses' =>  'Api\AccountController@listCampaigns' ]);
    
Route::post('/dashboard/switch', 'Api\AccountController@switch_type');
Route::post('/dashboard/change_budget', 'Api\AccountController@change_budget');
Route::post('/dashboard/change_status', 'Api\AccountController@change_status');//Cambio de status
Route::post('/dashboard/change_name', 'Api\AccountController@change_name');//Cambio de nombre
Route::get('/dashboard/duplicate', 'Api\AccountController@duplicate');

/* ---------------------------   -------  */

/* Route::get('/dashboard/datos_a/{id}',  [  'uses' =>  'Api\AccountController@datos_a' ]);
Route::get('/dashboard/datos_aa/{id}',  [  'uses' =>  'Api\AccountController@datos_aa' ]);
Route::get('/dashboard/datos_aaa/{id}',  [  'uses' =>  'Api\AccountController@datos_aaa' ]);
Route::get('/dashboard/datos_aaaa/{id}',  [  'uses' =>  'Api\AccountController@datos_aaaa' ]);
Route::get('/dashboard/datos_b/{id}',  [  'uses' =>  'Api\AccountController@datos_b' ]);
Route::get('/dashboard/datos_c/{id}',  [  'uses' =>  'Api\AccountController@datos_c' ]);
Route::get('/dashboard/datos_d/{id}',  [  'uses' =>  'Api\AccountController@datos_d' ]);
Route::get('/dashboard/datos_dd/{id}',  [  'uses' =>  'Api\AccountController@datos_dd' ]);
Route::get('/dashboard/datos_ddd/{id}',  [  'uses' =>  'Api\AccountController@datos_ddd' ]);
Route::get('/dashboard/datos_dddd/{id}',  [  'uses' =>  'Api\AccountController@datos_dddd' ]);
Route::get('/dashboard/datos_ddddd/{id}',  [  'uses' =>  'Api\AccountController@datos_ddddd' ]);
 */
/* Route::post('/dashboard/table_first_edit/{id}',  [  'uses' =>  'Api\AccountController@table_first_edit' ]);
Route::post('/dashboard/table_second_edit/{id}',  [  'uses' =>  'Api\AccountController@table_second_edit' ]);
Route::post('/dashboard/table_third_edit/{id}',  [  'uses' =>  'Api\AccountController@table_third_edit' ]);
Route::post('/dashboard/table_fourth_edit/{id}',  [  'uses' =>  'Api\AccountController@table_fourth_edit' ]);
Route::post('/dashboard/table_fifth_edit/{id}',  [  'uses' =>  'Api\AccountController@table_fifth_edit' ]);
 */
/** desactivado es un agujero
Route::get('/permisos',  [  'uses' =>  'Api\PermisosController@index' ]);
**/

/*** Jose, esta info se resuelve reporting/data
Route::get('/datos/{id}/impresiones',  [  'uses' =>  'Api\DashboardController@impresiones' ]);
Route::get('/datos/{id}/ingresos',  [  'uses' =>  'Api\DashboardController@ingresos' ]);
Route::get('/datos/{id}/ecpm',  [  'uses' =>  'Api\DashboardController@ecpm' ]);
Route::get('/datos/{id}/clics',  [  'uses' =>  'Api\DashboardController@clics' ]);
Route::get('/datos/{id}/ctr',  [  'uses' =>  'Api\DashboardController@ctr' ]);
***

/*rutas para dashboard (grafica1)*/
Route::get('/dashboard/{id?}', [  'uses' =>  'Api\DashboardController@index' ]);
Route::post('/dashboard', [  'uses' =>  'Api\DashboardController@create' ]);
Route::post('/dashboard/save', [  'uses' =>  'Api\DashboardController@create' ]);
Route::post('/dashboard/save/{id}', [  'uses' =>  'Api\DashboardController@update' ]);

//Route::post('/reporting/data', 'Api\ReportingController@data');
//Route::get('/reporting/data', 'Api\Reporting3@data');
Route::post('/reporting/data/v2', 'Api\ReportingController@data_v2');
Route::get('/reporting/save', 'Api\ReportingController@save');

Route::get('/reporting/getreport/{public_id}', 'Api\ReportingController@getreport');
Route::get('/reporting/entidades/{entidad?}', 'Api\ReportingController@entidades');


Route::get('/functions', 'Api\FunctionsController@index');
Route::get('/call', 'Api\FunctionsController@call');
Route::get('/token/{var}', 'Api\FunctionsController@tokenCallback');

Route::post('/createCampaign', 'Api\FunctionsController@createCampaign');
Route::post('/images-save', 'Api\FunctionsController@post_upload_image');
Route::post('/videos-save', 'Api\FunctionsController@post_upload_video');

// new routes - emiliano task reporting 3
Route::post('/reporting/save', 'Api\Reporting3@save');
Route::post('/reporting/data', 'Api\Reporting3@data');
Route::get('/reporting/config', 'Api\Reporting3@config');



/* 
Route::get('/createAdset', 'Api\FunctionsController@createAdset');
Route::get('/importe_maximo_mes/{fechaInicio}/{fechaFin}/{budget}/{budgetdistribution}', 'Api\FunctionsController@importe_maximo_mes');
 */

//routes used mostly in step2
Route::post('/getInterest', 'Api\FunctionsController@getInterest_V2');
Route::post('/getCustomAudiences', 'Api\FunctionsController@getCustomAudiencesV2');
Route::post('/getLocation', 'Api\FunctionsController@getLocation');
Route::post('/getLanguage', 'Api\FunctionsController@getLanguage');

Route::get('/getPromotePages/{AdAccount}/{param?}', 'Api\FunctionsController@getPromotePages');

Route::get('/getAdAccount', 'Api\FunctionsController@getAdAccount');

Route::post('/createClient', 'Api\FunctionsController@createClient');
Route::post('/updateClient', 'Api\FunctionsController@updateClient')->middleware('setnull');;

Route::get('/connections',  'Api\FunctionsController@get_connections');
Route::get('/ads_accounts/{id?}',  'Api\FunctionsController@get_ads_accounts');
Route::get('/properties/{id?}',  'Api\FunctionsController@get_properties');

Route::post('/connection/updater', 'Api\FunctionsController@connection_updater');
Route::post('/connection/sync', 'Api\FunctionsController@connection_sync');


Route::get('/getClients',  'Api\FunctionsController@getCustomerDataTable');
Route::get('/getClients_select',  'Api\FunctionsController@getClients_select');
Route::get('/getClients_select2',  'Api\FunctionsController@getClients_select2');
Route::get('/getClient/{id}',  'Api\FunctionsController@getCustomer');
Route::post('/adAccountProperty', 'Api\FunctionsController@adAccountProperty');


Route::get('/auth_by_client/{id}',  'Api\FunctionsController@auth_by_client');
Route::get('/account_by_client/{id}/{platform}',  'Api\FunctionsController@account_by_client');

//Chargebee
Route::post("/chargebee/events", "Api\ChargeBeeController@handleRequest");//->name("charge_bee_webhook");
Route::post('/chargebee', 'Api\FunctionsController@log_chargebee');

Route::post('/save_Autocomplete', 'Api\FunctionsController@save_Autocomplete');

Route::post('set_default_client', 'Api\FunctionsController@set_default_client');

Route::post('usetClientConnection', 'Api\FunctionsController@usetClientConnection');


Route::post('ads_account/add-client', 'Api\FunctionsController@addClientToAdsAccount');
Route::post('properties/add-client', 'Api\FunctionsController@addClientToProperty');

Route::post('clients', 'Api\FunctionsController@clients');



Route::post('external-log', 'Api\FunctionsController@log_externo');