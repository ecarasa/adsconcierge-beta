$('.check').click(function() {
    //($(this).attr('rrss'));
    var rrss = $(this).attr('rrss');

    if ($('#rrss_' + rrss).is(':checked')) {
        $('#ads_' + rrss).prop('disabled', true).trigger("chosen:updated");
        $('#properties_' + rrss).prop('disabled', true).trigger("chosen:updated");
        $('#fee_' + rrss).prop('disabled', true);
        $('#settings_' + rrss).hide();
    } else {
        $('#ads_' + rrss).prop('disabled', false).trigger("chosen:updated");
        $('#properties_' + rrss).prop('disabled', false).trigger("chosen:updated");
        $('#fee_' + rrss).prop('disabled', false);
        $('#settings_' + rrss).show();
    }

});
 

$('#ads_facebook').prop('disabled', true).trigger("chosen:updated");
$('#properties_facebook').prop('disabled', true).trigger("chosen:updated");
$('#ads_twitter').prop('disabled', true).trigger("chosen:updated");
$('#properties_twitter').prop('disabled', true).trigger("chosen:updated");
$('#ads_linkedin').prop('disabled', true).trigger("chosen:updated");
$('#properties_linkedin').prop('disabled', true).trigger("chosen:updated");
$('#ads_instagram').prop('disabled', true).trigger("chosen:updated");
$('#properties_instagram').prop('disabled', true).trigger("chosen:updated");
$('#ads_snapchat').prop('disabled', true).trigger("chosen:updated");
$('#properties_snapchat').prop('disabled', true).trigger("chosen:updated");
$('#ads_amazon').prop('disabled', true).trigger("chosen:updated");
$('#properties_amazon').prop('disabled', true).trigger("chosen:updated");
$('#ads_adwords').prop('disabled', true).trigger("chosen:updated");
$('#properties_adwords').prop('disabled', true).trigger("chosen:updated");

$('#fee_facebook').prop('disabled', true);
$('#fee_twitter').prop('disabled', true);
$('#fee_linkedin').prop('disabled', true);
$('#fee_instagram').prop('disabled', true);
$('#fee_snapchat').prop('disabled', true);
$('#fee_amazon').prop('disabled', true);
$('#fee_adwords').prop('disabled', true);

/*script para calculos de budget*/
$('#budget').blur(function() {
    var longitud = arrayrs.length
    if ($(this).val() != '') {
        var total = parseFloat($(this).val());
        var acum = 0;
        var acump = 0;
        var porcion = total / longitud;
        var porcentaje = 100 / longitud;
        arrayrs.forEach(function(elemento, indice, array) {
            if (indice != (longitud - 1)) {
                $('#bi' + elemento).val(porcion.toFixed(2));
                $('#bp' + elemento).val(porcentaje.toFixed(2) + "%");
                acump = acump + parseFloat(porcentaje.toFixed(2));
                acum = acum + parseFloat(porcion.toFixed(2));
            } else {
                porcion = total - acum;
                porcentaje = 100 - acump;
                $('#bi' + elemento).val(porcion.toFixed(2));
                $('#bp' + elemento).val(porcentaje.toFixed(2) + "%");
            }
        });
        $('#budget_split').show(200);
    }
});

$('.bi_rrss').blur(function() {
    var total = 0;
    var longitud = arrayrs.length
    $(".bi_rrss").each(function(indice) {
        var num = parseFloat($(this).val());
        if (num > 0) {
            total = total + parseFloat($(this).val());
        }
    });
    arrayrs.forEach(function(elemento, indice, array) {
        var num = parseFloat($('#bi' + elemento).val());

        if (num > 0) {
            var porcentaje = num * 100 / total;
            $('#bp' + elemento).val(porcentaje.toFixed(2) + "%");
        }
    });
    $('#budget').val(total.toFixed(2));
});

$('.form-porcen').blur(function() {
    var total = 0;
    $(".form-porcen").each(function(indice) {
        var num = parseFloat($(this).val());
        if (num > 0) {
            total = total + parseFloat($(this).val());
        }
    });
    if (total.toFixed(2) > 100) {
        $('.error_bidding').html("The percentage must not be greater than 100%");
        $('.error_bidding').show();
    } else if (total.toFixed(2) < 100) {
        $('.error_bidding').html("The percentage should not be less than 100%");
        $('.error_bidding').show();
    } else {
        $('.error_bidding').html("");
        $('.error_bidding').hide();
    }
});


// customer

function editModalCustomer(id) {
    var ruta = '/api/customer/' + id;

    //console.log('modal customer');

    $.ajax({
            url: ruta,
            contentType: false,
            dataType: "json",
            processData: false,
            beforeSend: function() {
                //$('#save_new_customer').html( '<img src="/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">' );
            },
        })
        .done(function(response) {

            //console.log(response);

            $('#modalCustomer').modal('show');

            $('#name').val(response.customer.name);
            $('#client_id').val(response.customer.public_id)

            if (response.customer.isdefault == 'Y') {
                $("#edit_default_customer").attr("checked", "checked");
            }

            let adsCustomer = JSON.parse(response.customer.ads_accounts);
            let propsCustomer = JSON.parse(response.customer.properties);
            let feesCustomer = JSON.parse(response.customer.fees);


            $.each(response.platforms, function(i, platform) {

                var platUpper = platform.name.toUpperCase();
                var platLower = platform.name.toLowerCase();

                $.each(response.ads_accounts[platUpper], function(j, ads) {

                    var selected = '';

                    $.each(adsCustomer[platUpper], function(p, adsCus) {
                        if (adsCus == ads.public_id) {
                            selected = ' selected ';

                            $("#rrss_" + platLower).attr("checked", "checked");


                        } else {
                            selected = '';
                        }
                    });

                    option = `<option value="${ads.public_id}" ${selected}> ${ads.name} </option>`
                    $('#ads_' + platLower).append(option);
                });

                $.each(response.properties_accounts[platUpper], function(h, prop) {

                    var selected = '';

                    $.each(propsCustomer[platUpper], function(p, propCus) {
                        if (propCus == prop.public_id) {
                            selected = ' selected ';
                        } else {
                            selected = '';
                        }
                    });

                    option = `<option value="${prop.public_id}" ${selected}> ${prop.name} </option>`
                    $('#properties_' + platLower).append(option);
                });






                $('.chosen-select').chosen();

                if (!$('#rrss_' + platLower).is(':checked')) {
                    $('#ads_' + platLower).prop('disabled', true).trigger("chosen:updated");
                    $('#properties_' + platLower).prop('disabled', true).trigger("chosen:updated");
                    $('#fee_' + platLower).prop('disabled', true);
                    $('#settings_' + platLower).hide();
                } else {
                    $('#ads_' + platLower).prop('disabled', false).trigger("chosen:updated");
                    $('#properties_' + platLower).prop('disabled', false).trigger("chosen:updated");
                    $('#fee_' + platLower).prop('disabled', false);
                    $('#settings_' + platLower).show();
                }


                loading.hide();

            });



        });

}

function saveCustomer() {

    $.ajax({
            url: '/customers/update',
            data: new FormData(document.getElementById('formEdit_Customer')),
            type: 'POST',
            contentType: false,
            dataType: "json",
            processData: false,

            beforeSend: function() {
                $('#saveBtnCustomer').html('<img src="https://app.thesocialaudience.com/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">');
            },
        })
        .done(function(response) {
            $('#saveBtnCustomer').html('Save');

            if (response.status) {
                toastr.success(response.message, "Customers");
                $('#modalCustomer').modal('hide');
            } else {
                toastr.error(response.message, "Customers");
            }

        });


}

function capitalizeFirstLetter2(string) {
    if (string == null) {
        return '';
    }
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

var platformsUsed = null;

// campaign
function editCampaign(id) {
    var ruta = '/api/campaign/' + id;

    /**
    budgetdistribution: "{\"facebook\":{\"platform\":\"facebook\",\"budgetType\":\"2\",\"budgetTypeStr\":\"by_percentage\",\"fixed_amount\":\"4999.50\",\"percentaje\":\"70%\",\"optimize\":\"likes\",\"pay\":\"cpm\",\"bid_type\":{\"id\":\"2\",\"value\":\"\"}},\"instagram\":{\"platform\":\"instagram\",\"budgetType\":\"2\",\"budgetTypeStr\":\"by_percentage\",\"fixed_amount\":\"4999.50\",\"percentaje\":\"30%\",\"optimize\":null,\"pay\":null,\"bid_type\":{\"id\":\"2\",\"value\":\"\"}}}"
    budgets_x_plataforma: null
    name: "LA LIGA"
*/

    var ruta = '/api/campaign/' + id;

    $.ajax({
        url: ruta,
        contentType: false,
        dataType: "json",
        processData: false,
        beforeSend: function() {
            //$('#save_new_customer').html( '<img src="/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">' );
        },
    }).done(function(response) {

        $('#modalCampaign').modal('show');

        $('#campaign_name').val(response.campaign.name);
        $('#campaign_id').val(response.campaign.public_id);
        $('#customer_id').val(response.customer.public_id);

        $('#social_rrss').empty();
        $('#budget_split').empty();

        platformsUsed = response.plataformas;


        var budgetSplit = JSON.parse(response.campaign.budgetdistribution);

        var btdt = capitalizeFirstLetter2(response.campaign.budget_target_diary_type);
        var bttt = capitalizeFirstLetter2(response.campaign.budget_target_total_type);

        $('#budget_target_diary').val(response.campaign.budget_target_diary);
        $('#budget_target_total').val(response.campaign.budget_target_total);
        $("#budget_target_diary_type option:contains(" + btdt + ")").attr('selected', 'selected');
        $("#budget_target_total_type option:contains(" + bttt + ")").attr('selected', 'selected');
        $('#budget').val(response.campaign.budget);

        var horarios = JSON.parse(response.campaign.run_only_on_schedule);

        $('#spend_on_each_ad').val(response.campaign.spend_on_each_ad);
        $('#impressions_per_day').val(response.campaign.impressions_per_day);


        $.each(response.plataformas, function(p, rrssUsed) {

            console.log("budget aa", budgetSplit[rrssUsed])

            var redSocial = //'<div class="form-group row rad_btn">' +
                '<div class="col-4"><span class="switch switch-sm"><label class="switch">' +
                '<input type="checkbox" rrss="' + rrssUsed + '" name="rrss[' + rrssUsed + ']" id="rrss_' + rrssUsed + '" value="' + rrssUsed + '" class="redSocial" checked="checked">' +
                '<span rrss="' + rrssUsed + '"></span>' +
                '<i class="fab fabBig fa-' + rrssUsed + '"></i> ' + rrssUsed +
                '</label></span>' +
                //'</div>' +
                '</div>';

            $('#social_rrss').append(redSocial);

            var valores = budgetSplit[rrssUsed];

            /*
                bid_type: {id: "2", value: ""}
                budgetType: "2"
                budgetTypeStr: "by_percentage"
                fixed_amount: "4999.50"
                optimize: null
                pay: null
                percentaje: "30%"
                platform: "instagram"
            */
            var bid1 = "";
            var bid2 = "";

            if (valores.bid_type.id == 1) {
                // es Lowest cost
                bid1 = ' checked ';
            } else {
                // es Lowest cost with bid cap
                bid2 = ' checked ';
            }

            var cardBudget =
                '<div class="budgetrs" id="b' + rrssUsed + '">' +
                '<h2>' + rrssUsed + '</h2>' +
                '<div class="form-group row" style="background-color: ; padding: 15px; border - radius: 46px;">' +
                '<div class="input-group col-6 text-left">' +
                '<label class="col-form-label" style="margin-right: 6px; ">Price</label>' +
                '<input type="number" class="form-control bi_rrss" value="' + valores.fixed_amount + '" placeholder="" name="bi' + rrssUsed + '" id="bi' + rrssUsed + '" >' +
                '<label class="checkbox kt-checkbox--bold kt-checkbox--brand cbbadget" style="margin - left: 2px;">' +
                '<input type="radio" name="br' + rrssUsed + '"  value="1" checked/> Fixed amount' +
                '<span></span>' +
                '</label>' +
                '</div>' +

                '<div class="col-1 text-center">' +
                '<label class="col-form-label">or</label>' +
                '</div>' +

                '<div class="col-5 text-right">' +
                '<label class="col-form-label" style="margin-right: 6px; float: left;">% Value</label>' +
                '<input type="text" class="form-control form-porcen" value="' + valores.percentaje + '"  name="bp' + rrssUsed + '" id="bp' + rrssUsed + '">' +
                '<label class="checkbox kt-checkbox--bold kt-checkbox--brand cbbadget" style="float: right; margin-top: -25px; margin-right: -4px;">' +
                '<input type="radio" name="br' + rrssUsed + '"  value="2" />' +
                '<span style="margin-left: 18px; margin-top: 25px;"></span>' +
                '</label>' +
                '</div>' +
                '</div>' +
                '<div class="form-group row">' +
                '<label class="col-form-label col-lg-3">Bidding *</label>' +
                '<div class="kt-radio-list col-md-9">' +
                '<label class="kt-radio kt-radio--bold kt-radio--brand">' +
                '<input type="radio" ' + bid1 + ' val="lowest_cost" name="bid_' + rrssUsed + '" rrss="' + rrssUsed + '" class="low-top"><b> Lowest cost </b>Get the most link clicks for your budget' +
                '<span></span>' +
                '</label>' +
                '<label class="kt-radio kt-radio--bold kt-radio--brand">' +
                '<input type="radio" val="lowest_cost_bid_cap" name="bid_' + rrssUsed + '" rrss="' + rrssUsed + '"' +
                'class="low-top low_top_rad" ' + bid2 + '><b>Lowest cost with bid cap </b>Insert the target price you\'d like to pay for an action. <i>This price is not the cpm bid.</i>' +
                ' <span></span>' +
                '</label>' +
                //<!-- Campo dependiente radio "coste + bajo con tope"   -->
                '<div class="form-group row" id="low_cost_top_' + rrssUsed + '" style="display: ;">' +
                ' <label class="col-2">Bid</label>' + ' <div class="input-group col-4">' + ' <input type="text" class="form-control" name="low_cost_top_' + rrssUsed + '" id="input_low_cost_top_' + rrssUsed + '" placeholder="" required>' + '</div>' + '</div>' + '</div>' + '</div>' +
                '</div>' +



                $('#budget_split').append(cardBudget);

        });



        $("#weekly-schedule").dayScheduleSelector({
            interval: 60,
            enableHorizontalSelection: true,
            startTime: '00:00',
            endTime: '24:00'
        });


    });

}

function saveCampaign() {

    let dataPost = new FormData(document.getElementById('kt_form_edit'));

    var i;
    let budget_split = [];
    let tipoBudget;
    let tipoBudgetStr;

    $.each(platformsUsed, function(p, rrssUsed) {
        budget_split[rrssUsed] = [];

        tipoBudget = $('input[name=br' + rrssUsed + ']:checked').val();


        if ($('input[name=br' + rrssUsed + ']:checked').val() == 1) {

            tipoBudgetStr = 'by_fixed';
        } else {
            tipoBudgetStr = 'by_percentage';
        }

        var item = {
            platform: rrssUsed,
            budgetType: tipoBudget,
            budgetTypeStr: tipoBudgetStr,
            fixed_amount: $('#bi' + rrssUsed).val(),
            percentaje: $('#bp' + rrssUsed).val(),
            optimize: $('#optimize_' + rrssUsed).val(),
            pay: $('#pay_' + rrssUsed).val(),
            bid_type: { id: $('input[name=br' + rrssUsed + ']:checked').val(), value: $('#input_low_cost_top_' + rrssUsed).val() }
        };

        //formData.append("budget_split[]", JSON.stringify(item));
        budget_split.push(item);

    });

    dataPost.append("budget_split_all", JSON.stringify(budget_split));
    let run_only_on_schedule = $("#weekly-schedule").data('artsy.dayScheduleSelector').serialize();
    formData.append("run_only_on_schedule_serialized", JSON.stringify(run_only_on_schedule));

    $.ajax({
            url: '/campaign/update',
            data: dataPost,
            type: 'POST',
            contentType: false,
            dataType: "json",
            processData: false,

            beforeSend: function() {
                $('#saveBtnCustomer').html('<img src="https://app.thesocialaudience.com/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">');
            },
        })
        .done(function(response) {
            //$('#saveBtnCustomer').html('Save');

            if (response.status) {
                toastr.success(response.message, "Campaign");
                //$('#modalCustomer').modal('hide');
            } else {
                toastr.error(response.message, "Campaign");
            }

        });


}


// campaign
function editCampaignPlatform(id) {

    var ruta = '/api/campaign_platform/' + id;

    /**
     * {
	    "campaign": {
		"id": 1145,
		"user_id": 6,
		"platform": "FACEBOOK",
		"id_en_platform": "23847262003810692",
		"app_id": "2230398120518804",
		"ad_account": 23,
		"auth_id": 110,
		"activa": "Y",
		"property_id": null,
		"campana_root": 1,
		"name": "OC005735_TacoBell_AlcanceSTRAperturaAlisios_1105",
		"stats_ultimoupdate": null,
		"created_at": "2021-07-28 15:48:16",
		"updated_at": "2021-07-31 05:14:20",
		"start": null,
		"end": null,
		"budget": 0,
		"budget_total": 0,
		"ctr": 0,
		"cpc": 0,
		"clicks": 0,
		"impression": 0,
		"spent": 0,
		"conversion": 0,
		"cpm": 0,
		"cpa": 0,
		"customer_id": 32,
		"geo_include": null,
		"geo_exclude": null,
		"platform_placements": null,
		"genders": null,
		"ages": null,
		"languages": null,
		"platform_properties_ids": null,
		"devices": null,
		"conectivity": null,
		"metadata": "{\"id\": \"23847262003810692\", \"name\": \"OC005735_TacoBell_AlcanceSTRAperturaAlisios_1105\", \"status\": \"ACTIVE\", \"adbatch\": null, \"adlabels\": null, \"objective\": \"REACH\", \"spend_cap\": null, \"stop_time\": \"2021-05-12T22:07:30+0200\", \"account_id\": \"2043263135689577\", \"start_time\": \"2021-05-11T14:07:30+0200\", \"topline_id\": \"0\", \"buying_type\": \"AUCTION\", \"issues_info\": null, \"pacing_type\": null, \"bid_strategy\": null, \"created_time\": \"2021-05-07T17:09:52+0200\", \"daily_budget\": null, \"updated_time\": \"2021-07-27T12:21:41+0200\", \"ad_strategy_id\": null, \"lifetime_budget\": null, \"promoted_object\": null, \"recommendations\": null, \"source_campaign\": null, \"upstream_events\": null, \"budget_remaining\": \"0\", \"effective_status\": \"ACTIVE\", \"boosted_object_id\": null, \"can_use_spend_cap\": true, \"configured_status\": \"ACTIVE\", \"execution_options\": null, \"brand_lift_studies\": null, \"source_campaign_id\": \"0\", \"special_ad_category\": \"NONE\", \"smart_promotion_type\": \"GUIDED_CREATION\", \"budget_rebalance_flag\": false, \"special_ad_categories\": [], \"last_budget_toggling_time\": null, \"is_skadnetwork_attribution\": false, \"can_create_brand_lift_study\": false, \"special_ad_category_country\": null, \"iterative_split_test_configs\": null}",
		"targeting_include": null,
		"targeting_exclude": null,
		"platforma_budget": null,
		"creativities_ids": null,
		"objective": null,
		"bid_strategy": null,
		"currency": "EUR",
		"status": "INACTIVE",
		"process": null,
		"public_id": "3970cfc6-efbb-11eb-8d81-ac1f6b17ff4a",
		"source": "IMPORTED"
	    }
    }
    */


    $.ajax({
        url: ruta,
        contentType: false,
        dataType: "json",
        processData: false,
        beforeSend: function () {
            //$('#save_new_customer').html( '<img src="/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">' );
        },
    }).done(function (response) {

        $('#modalCampaignPlatform').modal('show');

        $('#campaign_platform_name').val(response.campaign.name);
        $('#campaign_platform_platform').html(response.campaign.platform);
        $('#campaign_platform_id').val(response.campaign.public_id);
        
        $('#campaign_platform_budget').val(response.campaign.budget);
        $('#campaign_platform_budget_total').val(response.campaign.budget_total);
        
        
/*      var budgetSplit = JSON.parse(response.campaign.budgetdistribution);

        var btdt = capitalizeFirstLetter2(response.campaign.budget_target_diary_type);
        var bttt = capitalizeFirstLetter2(response.campaign.budget_target_total_type);

        $('#budget_target_diary').val(response.campaign.budget_target_diary);
        $('#budget_target_total').val(response.campaign.budget_target_total);
        $("#budget_target_diary_type option:contains(" + btdt + ")").attr('selected', 'selected');
        $("#budget_target_total_type option:contains(" + bttt + ")").attr('selected', 'selected');
        $('#budget').val(response.campaign.budget);

        var horarios = JSON.parse(response.campaign.run_only_on_schedule);

        $('#spend_on_each_ad').val(response.campaign.spend_on_each_ad);
        $('#impressions_per_day').val(response.campaign.impressions_per_day);


        $.each(response.plataformas, function (p, rrssUsed) {

            console.log("budget aa", budgetSplit[rrssUsed])

            var redSocial = //'<div class="form-group row rad_btn">' +
                '<div class="col-4"><label class="switch">' +
                '<input type="checkbox" rrss="' + rrssUsed + '" name="rrss[' + rrssUsed + ']" id="rrss_' + rrssUsed + '" value="' + rrssUsed + '" class="redSocial" checked="checked">' +
                '<span class="slider round check" rrss="' + rrssUsed + '"></span>' +
                '<i class="fab fabBig fa-' + rrssUsed + '"></i> ' + rrssUsed +
                '</label>' +
                //'</div>' +
                '</div>';

            $('#social_rrss').append(redSocial);

            var valores = budgetSplit[rrssUsed];

         
            var bid1 = "";
            var bid2 = "";

            if (valores.bid_type.id == 1) {
                // es Lowest cost
                bid1 = ' checked ';
            } else {
                // es Lowest cost with bid cap
                bid2 = ' checked ';
            }

            var cardBudget =
                '<div class="budgetrs" id="b' + rrssUsed + '">' +
                '<h2>' + rrssUsed + '</h2>' +
                '<div class="form-group row" style="background-color: ; padding: 15px; border - radius: 46px;">' +
                '<div class="input-group col-6 text-left">' +
                '<label class="col-form-label" style="margin-right: 6px; ">Price</label>' +
                '<input type="number" class="form-control bi_rrss" value="' + valores.fixed_amount + '" placeholder="" name="bi' + rrssUsed + '" id="bi' + rrssUsed + '" >' +
                '<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand cbbadget" style="margin - left: 2px;">' +
                '<input type="radio" name="br' + rrssUsed + '"  value="1" checked/> Fixed amount' +
                '<span></span>' +
                '</label>' +
                '</div>' +

                '<div class="col-1 text-center">' +
                '<label class="col-form-label">or</label>' +
                '</div>' +

                '<div class="col-5 text-right">' +
                '<label class="col-form-label" style="margin-right: 6px; float: left;">% Value</label>' +
                '<input type="text" class="form-control form-porcen" value="' + valores.percentaje + '"  name="bp' + rrssUsed + '" id="bp' + rrssUsed + '">' +
                '<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand cbbadget" style="float: right; margin-top: -25px; margin-right: -4px;">' +
                '<input type="radio" name="br' + rrssUsed + '"  value="2" />' +
                '<span style="margin-left: 18px; margin-top: 25px;"></span>' +
                '</label>' +
                '</div>' +
                '</div>' +
                '<div class="form-group row">' +
                '<label class="col-form-label col-lg-3">Bidding *</label>' +
                '<div class="kt-radio-list col-md-9">' +
                '<label class="kt-radio kt-radio--bold kt-radio--brand">' +
                '<input type="radio" ' + bid1 + ' val="lowest_cost" name="bid_' + rrssUsed + '" rrss="' + rrssUsed + '" class="low-top"><b> Lowest cost </b>Get the most link clicks for your budget' +
                '<span></span>' +
                '</label>' +
                '<label class="kt-radio kt-radio--bold kt-radio--brand">' +
                '<input type="radio" val="lowest_cost_bid_cap" name="bid_' + rrssUsed + '" rrss="' + rrssUsed + '"' +
                'class="low-top low_top_rad" ' + bid2 + '><b>Lowest cost with bid cap </b>Insert the target price you\'d like to pay for an action. <i>This price is not the cpm bid.</i>' +
                ' <span></span>' +
                '</label>' +
                '<div class="form-group row" id="low_cost_top_' + rrssUsed + '" style="display: ;">' +
                ' <label class="col-2">Bid</label>' + ' <div class="input-group col-4">' + ' <input type="text" class="form-control" name="low_cost_top_' + rrssUsed + '" id="input_low_cost_top_' + rrssUsed + '" placeholder="" required>' + '</div>' + '</div>' + '</div>' + '</div>' +
                '</div>' +



                $('#budget_split').append(cardBudget);

        });
 */


 


    });

}





function saveCampaignPlatform() {

    let dataPost = new FormData(document.getElementById('kt_form_edit_campaign_platform'));


    $.ajax({
        url: 'api/campaignPlatform/update',
        data: dataPost,
        type: 'POST',
        contentType: false,
        dataType: "json",
        processData: false,

        beforeSend: function () {
            $('#saveBtnCampaignPlatform').html('<img src="https://app.thesocialaudience.com/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">');
        },
    })
        .done(function (response) {
            //$('#saveBtnCustomer').html('Save');

            if (response.status) {
                toastr.success(response.message, "Campaign Platform");
                //$('#modalCustomer').modal('hide');
            } else {
                toastr.error(response.message, "Campaign Platform");
            }

        });


}



function saveCampaign() {

    let dataPost = new FormData(document.getElementById('kt_form_edit'));

    var i;
    let budget_split = [];
    let tipoBudget;
    let tipoBudgetStr;

    $.each(platformsUsed, function (p, rrssUsed) {
        budget_split[rrssUsed] = [];

        tipoBudget = $('input[name=br' + rrssUsed + ']:checked').val();


        if ($('input[name=br' + rrssUsed + ']:checked').val() == 1) {

            tipoBudgetStr = 'by_fixed';
        } else {
            tipoBudgetStr = 'by_percentage';
        }

        var item = {
            platform: rrssUsed,
            budgetType: tipoBudget,
            budgetTypeStr: tipoBudgetStr,
            fixed_amount: $('#bi' + rrssUsed).val(),
            percentaje: $('#bp' + rrssUsed).val(),
            optimize: $('#optimize_' + rrssUsed).val(),
            pay: $('#pay_' + rrssUsed).val(),
            bid_type: { id: $('input[name=br' + rrssUsed + ']:checked').val(), value: $('#input_low_cost_top_' + rrssUsed).val() }
        };

        //formData.append("budget_split[]", JSON.stringify(item));
        budget_split.push(item);

    });

    dataPost.append("budget_split_all", JSON.stringify(budget_split));
    let run_only_on_schedule = $("#weekly-schedule").data('artsy.dayScheduleSelector').serialize();
    formData.append("run_only_on_schedule_serialized", JSON.stringify(run_only_on_schedule));

    $.ajax({
        url: '/campaign/update',
        data: dataPost,
        type: 'POST',
        contentType: false,
        dataType: "json",
        processData: false,

        beforeSend: function () {
            $('#saveBtnCustomer').html('<img src="https://app.thesocialaudience.com/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">');
        },
    })
        .done(function (response) {
            //$('#saveBtnCustomer').html('Save');

            if (response.status) {
                toastr.success(response.message, "Campaign");
                //$('#modalCustomer').modal('hide');
            } else {
                toastr.error(response.message, "Campaign");
            }

        });


}

var platformAtomSelected = null;
var platformCreasSelected = null;
//atomo
function editModalAtom(id) {

    var ruta = '/api/atom/' + id;

    //var ruta = '/api/atom/6c4733f4-b350-11eb-8d81-ac1f6b17ff4a';


    $.ajax({
            url: ruta,
            contentType: false,
            dataType: "json",
            processData: false,
            beforeSend: function() {
                //$('#save_new_customer').html( '<img src="/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">' );
            },
        })
        .done(function(response) {
            $('#modalAtom').modal('show');

            let platLower = response.atom.platform.toLowerCase();
            platformAtomSelected = platLower;

            $('#dt-' + platLower).show();
            $('#dt2-' + platLower).show();
            //$('#dt3-' + platLower).show();

            $('#atom_id').val(response.atom.public_id);
            $('#atom_name').val(response.atom.name);
            $('#atom_budget').val(response.atom.budget);

            //languege
            $.each(response.atom_language, function(p, item) {
                $('#language-list').append('<li data-id="' + item.uuid + '" data-name="' + item.nombre + '" class="IN">' + item.nombre + '<i class="flaticon-circle"></i></li>');
            });

            //genderd6bed0cf-96f7-11eb-8d81-ac1f6b17ff4a
            //conectivityd58eec12-96f7-11eb-8d81-ac1f6b17ff4a
            //devicesd58ee679-96f7-11eb-8d81-ac1f6b17ff4a
            $.each(JSON.parse(response.atom.gender), function(p, item) { $('#gender' + item).attr('checked', 'checked'); });
            $.each(JSON.parse(response.atom.conectivity), function(p, item) { $('#conectivity' + item).attr('checked', 'checked'); });
            $.each(JSON.parse(response.atom.device), function(p, item) { $('#devices' + item).attr('checked', 'checked'); });

            $.each(JSON.parse(response.atom.targeting_include), function(p, item) {
                $(".intereses-list." + platLower).append("<li data-id='" + item.id + "' data-name='" + item.name + "' data=\"" + item.id + "\" class='IN' >" + item.name + "<i class='flaticon-circle'></i></li>");
            });

            $.each(JSON.parse(response.atom.targeting_exclude), function(p, item) {
                $(".intereses-list." + platLower).append("<li data-id='" + item.id + "' data-name='" + item.name + "' data=\"" + item.id + "\" class='EX' >" + item.name + "<i class='flaticon-circle'></i></li>");
            });

            $.each(JSON.parse(response.atom.geo_include), function(p, item) {
                $(".location-list." + platLower).append("<li data-id='" + item.id + "' data-name='" + item.name + "' data=\"" + item.id + "\" class='IN' >" + item.name + "<i class='flaticon-circle'></i></li>");
            });

            $.each(JSON.parse(response.atom.geo_exclude), function(p, item) {
                $(".location-list." + platLower).append("<li data-id='" + item.id + "' data-name='" + item.name + "' data=\"" + item.id + "\" class='EX' >" + item.name + "<i class='flaticon-circle'></i></li>");
            });


            $.each(JSON.parse(response.atom.age), function(p, item) {

                var age = '<div data-repeater-item class="row mb-3 align-items-center">' +
                    '<div class="col-md-6">' +
                    '<div class="kt-form__group--inline">' +
                    '<div class="kt-form__label">' +
                    '</div>' +
                    '<div class="kt-form__control">' +
                    '<div class="input-daterange input-group"> <input type="number" class="form-control agepckr" name="start" value="' + item.start + '" /> <div class="input-group-append"> <span class="input-group-text"><i class="la la-ellipsis-h"></i></span></div> <input type="number" class="form-control agepckr" name="end"  value="' + item.end + '" /></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div data-repeater-delete="" class="btn-sm btn btn-danger btn-pill deledad">' +
                    '<span>' +
                    '<i class="la la-trash-o"></i>' +
                    '<span> </span>' +
                    '</span>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-lg-3">' +
                    '<div data-repeater-create="" class="btn btn-sm btn-brand btn-pill addedad">' +
                    '<span>' +
                    '<i class="la la-plus"></i>' +
                    '<span>Add </span>' +
                    '</span>' +
                    '</div>' + '</div>' + '</div>';

                $("#age-list").append(age);
            });



        });

}

function saveAtom() {

    let formData = new FormData();

    // location json
    var i;
    let location_in = [];
    let location_out = [];
    location_in[platformAtomSelected] = [];
    location_out[platformAtomSelected] = [];
    $("#location-list-" + platformAtomSelected + ">li").each(function(index) {
        var type = $(this).attr('class');
        var item = { id: $(this).attr('data-id'), name: $(this).attr('data-name'), platform: platformAtomSelected };

        if (type == 'IN') {
            formData.append("location_in[]", JSON.stringify(item));
        } else {
            formData.append("location_out[]", JSON.stringify(item));
        }
    });


    var i;
    let interest_list_in = [];
    let interest_list_ex = [];

    $(".intereses-list." + platformAtomSelected + ">li").each(function(index) {

        var item = { id: $(this).attr('data-id'), name: $(this).attr('data-name'), type: $(this).attr('class') };

        if ($(this).attr('class') == 'IN') {
            interest_list_in.push(item);
        }

        if ($(this).attr('class') == 'EX') {
            interest_list_ex.push(item);
        }

    });

    formData.append("interest_list_in[]", JSON.stringify(interest_list_in));
    formData.append("interest_list_ex[]", JSON.stringify(interest_list_ex));


    formData.append("atom_id", $('#atom_id').val());
    formData.append("atom_name", $('#atom_name').val());
    formData.append("atom_budget", $('#atom_budget').val());



    formData.append("gender", $('input[name="gender"]:checked').val());

    let devices = [];
    $('input[name="devices"]:checked').each(function(index) {
        devices.push($('input[name="devices"]:checked').val());
    });
    formData.append("devices", JSON.stringify(devices));

    let conectivity = [];
    $('input[name="conectivity"]:checked').each(function(index) {
        conectivity.push($('input[name="conectivity"]:checked').val());
    });
    formData.append("conectivity", JSON.stringify(conectivity));

    let language = [];
    $("#language-list>li").each(function(index) {
        language.push($(this).attr('data-id'));
    });
    formData.append("language", JSON.stringify(language));


    $.ajax({
            url: 'api/atom/update',
            data: formData,
            type: 'POST',
            contentType: false,
            dataType: "json",
            processData: false,

            beforeSend: function() {
                $('#saveBtnCustomer').html('<img src="https://app.thesocialaudience.com/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">');
            },
        })
        .done(function(response) {
            $('#saveBtnCustomer').html('Save');

            if (response.status) {
                toastr.success(response.message, "Atom");
                $('#modalCustomer').modal('hide');
            } else {
                toastr.error(response.message, "Atom");
            }

        });


}




function saveCreas() {

    var formData = new FormData();

    //imagenes and inputs tags
    for (var key in window.images_uploaded) {
        window.images_uploaded[key].tags = $("input[name='imagen[" + key + "][imagentags]']").tagsinput('items');
    }
    console.log('images_uploaded', images_uploaded)
    formData.append("images_uploaded", JSON.stringify(window.images_uploaded));

    var headlines = [];
    var flag = true;
    var i = 0;
    while (flag) {
        if ($('input[name="titulo[' + i + '][titulo]"]').val() != undefined) {
            var item = { value: $('input[name="titulo[' + i + '][titulo]"]').val(), tags: $('input[name="titulo[' + i + '][titulotags]"]').val() };
            headlines.push(item);
            formData.append("headlines[]", JSON.stringify(item));
            i++;
        } else {
            flag = false;
        }
    }

    var ad_texts = [];
    flag = true;
    i = 0;
    while (flag) {
        if ($('textarea[name="descripcion[' + i + '][descripcion]"]').val() != undefined) {
            var item = { value: $('textarea[name="descripcion[' + i + '][descripcion]"]').val(), tags: $('input[name="descripcion[' + i + '][descripciontags]"]').val() };
            formData.append("ad_texts[]", JSON.stringify(item));

            i++;
        } else {
            flag = false;
        }
    }

    var urls = [];
    flag = true;
    i = 0;
    while (flag) {
        if ($('input[name="url[' + i + '][url]"]').val() != undefined) {
            var item = { value: $('input[name="url[' + i + '][url]"]').val(), tags: $('input[name="url[' + i + '][urltags]"]').val() };
            formData.append("urls[]", JSON.stringify(item));
            i++;
        } else {
            flag = false;
        }
    }

    flag = true;
    i = 0;
    while (flag) {
        if ($('textarea[name="text_link[' + i + '][text_link]"]').val() != undefined) {
            var item = { value: $('textarea[name="text_link[' + i + '][text_link]"]').val(), tags: $('input[name="text_link[' + i + '][text_linktags]"]').val() };
            formData.append("text_links[]", JSON.stringify(item));
            console.log(item);
            i++;
        } else {
            flag = false;
        }
    }


    formData.append("link_display", $('input[name="link_display"]').val());


    var call_action = [];
    flag = true;
    i = 0;
    while (flag) {
        if ($('select[name="call_to_action[' + i + '][call_to_action]"]').val() != undefined) {
            var item = { value: $('select[name="call_to_action[' + i + '][call_to_action]"]').val(), tags: $('input[name="call_to_action[' + i + '][call_to_actiontags]"]').val() };
            call_action.push(item);
            formData.append("call_actions[]", JSON.stringify(item));

            i++;
        } else {
            flag = false;
        }
    }



    $.ajax({
            url: 'api/creativity/update',
            data: formData,
            type: 'POST',
            contentType: false,
            dataType: "json",
            processData: false,

            beforeSend: function() {
                $('#saveBtnCustomer').html('<img src="https://app.thesocialaudience.com/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">');
            },
        })
        .done(function(response) {
            $('#saveBtnCustomer').html('Save');

            if (response.status) {
                toastr.success(response.message, "Creativity");
                $('#modalCustomer').modal('hide');
            } else {
                toastr.error(response.message, "Creativity");
            }

        });

}


function editModalCreas(id) {


    var ruta = '/api/creativity/' + id;
    var ruta = '/api/creativity/' + id;


    $.ajax({
            url: ruta,
            contentType: false,
            dataType: "json",
            processData: false,
            beforeSend: function() {
                //$('#save_new_customer').html( '<img src="/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">' );
            },
        })
        .done(function(response) {
            $('#modalCreas').modal('show');


            /**
             * atomo_id: 3005
                banner: "[{\"id\":121452}]"
                budget: 900
                calltoaction: "[{\"value\":\"SUBSCRIBE\",\"tags\":\"\"}]"
                campana_platform_id: 1
                campana_root: 1
                clicks: null
                content: "[{\"titulo\":\"titulo 1\"}, {\"titulo\":\"titulo 2\"}]"
                conversion_costs: null
                costs_per_click: null
                ctr: null
                customer_id: 32
                darkpost_id: null
                description: "LINK DISPLAYYY"
                id: 876
                id_en_platform: "8769"
                impressions: null
                linkdescription: "[{\"tags\": \"\", \"value\": \"DESCRIPCION\"}]"
                macrotag: null
                metadata: null
                name: "Demo Creas Emiliooooano"
                platform: "FACEBOOK"
                platform_configuration: null
                platform_status: null
                public_id: "c8bad253-b355-11eb-8d81-ac1f6b17ff4a"
                size: null
                source: "NATIVE"
                status: "ACTIVE"
                title: "Demo Creas Emi"
                type: null
                type_platform: null
                url: "[{\"value\":\"url demo\",\"tags\":\"\"},{\"value\":\"url demo22\",\"tags\":\"\"}]"
                user_id: 6
             */

            var platLower = response.creativity.platform.toLowerCase();
            platformCreasSelected = platLower;


            $('#creas_id').val(response.creativity.public_id);
            $('#creas_name').val(response.creativity.name);
            $('#creas_budget').val(response.creativity.budget);
            $('#link_display').val(response.creativity.description);


            //languege
            $.each(JSON.parse(response.creativity.calltoaction), function(p, item) {
                //$("#call_to_action option:value(" + item .value + ")").attr('selected', 'selected');
                $("#call_to_action").val(item.value);
            });


            // click para duplicar btn btn btn-sm btn-brand btn-pill addurl
            //name="" name="url[0][urltags]"
            var i = 0;
            $.each(JSON.parse(response.creativity.url), function(p, item) {
                $('input[name="url[' + i + '][url]"]').val(item.value);
                $('input[name="url[' + i + '][urltags]"]').val(item.tags);
                $(".addurl").click();
                i++;
            });

            var i = 0;
            $.each(JSON.parse(response.creativity.linkdescription), function(p, item) {

                $('input[name="text_link[' + i + '][text_link]"]').val(item.value);
                $('input[name="text_link[' + i + '][text_linktags]"]').val(item.tags);
                $(".addtext_link").click();
                i++;
            });


            var i = 0;
            $.each(JSON.parse(response.creativity.content), function(p, item) {

                $('input[name="descripcion[' + i + '][descripcion]"]').val(item.value);
                $('input[name="descripcion[' + i + '][descripciontags]"]').val(item.tags);
                $(".adddescripcion").click();
                i++;
            });



            var i = 0;
            $.each(JSON.parse(response.creativity.banner), function(p, item) {
                $('input[name="titulo[' + i + '][titulo]"]').val(item.value);
                $('input[name="titulo[' + i + '][titulotags]"]').val(item.tags);
                $(".addtitulo").click();
                i++;
            });


        });





}