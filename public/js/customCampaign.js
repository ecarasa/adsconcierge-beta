function setValuesStep5(objetivos, platform) {
    //console.log(platform, objetivos);

    $('#optimize_' + platform).empty();
    $('#pay_' + platform).empty();

    $.each(objetivos, function(i, item) {
        //console.log(i, item);
        switch (i) {
            case 'optimize_by':
                $('#optimize_' + platform).append($('<option>', { value: item, text: capitalizeFirstLetter(item) })).trigger('change');;
                break;
            case 'pay_by':
                $('#pay_' + platform).append($('<option>', { value: item, text: capitalizeFirstLetter(item) })).trigger('change');;
                break;
        }
    });

    // $('#optimize_' + platform).select2({
    //     placeholder: "Select an option",
    //     minimumResultsForSearch: Infinity
    // });
    // $('#optimize_' + platform).selectpicker({ noneSelectedText: 'Select an option' });
    // $('#pay_' + platform).selectpicker({ noneSelectedText: 'Select an option' });
    // $('#pay_' + platform).select2({
    //     placeholder: "Select an option",
    //     minimumResultsForSearch: Infinity
    // });

}

/*tablasel2*/
$(document).ready(function() {

    var nonLinearSlider = document.getElementsByClassName('age-slider');
    for (var i = 0; i < nonLinearSlider.length; i++) {
        noUiSlider.create(nonLinearSlider[i], {
            connect: true,
            behaviour: 'tap',
            start: [1, 100],
            range: {
                'min': [0],
                'max': [100]
            }
        });

        nonLinearSlider[i].noUiSlider.on('update', function(values, handle, unencoded, isTap, positions) {
            let minage = document.getElementsByClassName('min-age');
            let maxage = document.getElementsByClassName('max-age');
            minage.value = values[0];
            maxage.value = values[1];
        });
    }


    $('#table_objetive_facebook tr.click').click(function() {
        $('#table_objetive_facebook tr.click').removeClass("selecc");
        $(this).addClass("selecc");
        $(this).find('input[type="radio"]').prop("checked", true);
        let objetivos = $(this).find('input[type="radio"]').attr("metadata");
        objetivos = JSON.parse(objetivos.replace(/'/g, '"'));
        setValuesStep5(objetivos.FACEBOOK, 'facebook');
    });
    $('#table_objetive_twitter tr.click').click(function() {
        $('#table_objetive_twitter tr.click').removeClass("selecc");
        $(this).addClass("selecc");
        $(this).find('input[type="radio"]').prop("checked", true);
        let objetivos = $(this).find('input[type="radio"]').attr("metadata");
        objetivos = JSON.parse(objetivos.replace(/'/g, '"'));
        setValuesStep5(objetivos.TWITTER, 'twitter');
    });
    $('#table_objetive_instagram tr.click').click(function() {
        $('#table_objetive_instagram tr.click').removeClass("selecc");
        $(this).addClass("selecc");
        $(this).find('input[type="radio"]').prop("checked", true);
        let objetivos = $(this).find('input[type="radio"]').attr("metadata");
        objetivos = JSON.parse(objetivos.replace(/'/g, '"'));
        setValuesStep5(objetivos.INSTAGRAM, 'instagram');
    });
    $('#table_objetive_adwords tr.click').click(function() {
        $('#table_objetive_adwords tr.click').removeClass("selecc");
        $(this).addClass("selecc");
        $(this).find('input[type="radio"]').prop("checked", true);
        let objetivos = $(this).find('input[type="radio"]').attr("metadata");
        objetivos = JSON.parse(objetivos.replace(/'/g, '"'));
        setValuesStep5(objetivos.ADWORDS, 'adwords');
    });
    $('#table_objetive_linkedin tr.click').click(function() {
        $('#table_objetive_linkedin tr.click').removeClass("selecc");
        $(this).addClass("selecc");
        $(this).find('input[type="radio"]').prop("checked", true);
        let objetivos = $(this).find('input[type="radio"]').attr("metadata");
        objetivos = JSON.parse(objetivos.replace(/'/g, '"'));
        setValuesStep5(objetivos.LINKEDIN, 'linkedin');
    });
    $('#table_objetive_snapchat tr.click').click(function() {
        $('#table_objetive_snapchat tr.click').removeClass("selecc");
        $(this).addClass("selecc");
        $(this).find('input[type="radio"]').prop("checked", true);
        let objetivos = $(this).find('input[type="radio"]').attr("metadata");
        objetivos = JSON.parse(objetivos.replace(/'/g, '"'));
        setValuesStep5(objetivos.SNAPCHAT, 'snapchat');
    });
    $('#table_objetive_amazon tr.click').click(function() {
        $('#table_objetive_amazon tr.click').removeClass("selecc");
        $(this).addClass("selecc");
        $(this).find('input[type="radio"]').prop("checked", true);
        let objetivos = $(this).find('input[type="radio"]').attr("metadata");
        objetivos = JSON.parse(objetivos.replace(/'/g, '"'));
        setValuesStep5(objetivos.AMAZON, 'amazon');
    });
});
//	$("#errorAge").css('display', 'none');
// $(document).on( 'blur', '[name^="age"]', function(){
//        //console.log($(this).val());
// 	var vedad=$(this).val();
// 	var prueba=$('[name^="age"]');
// 	//console.log(prueba.length);
// 	var tamano=prueba.length/2
// 	var error=false;
// 	for (i=0;i<tamano;i++){
// 		inicio=parseInt($('[name="age['+i+'][start]"').val());
// 		fin=parseInt($('[name="age['+i+'][end]"').val());
// 		if ((vedad>inicio)&&(vedad<fin)){
// 			error=true;
// 		}
// 		if((inicio!='')&&(fin!='')){
// 			if (fin<inicio){
// 				error=true;
// 				console.log('inicio '+inicio+ ' fin: '+fin);
// 			}
// 		}
// 		//console.log("inicio: "+inicio+" fin: "+fin);
// 	}
// 	if (error) {
// 		//console.log('error');
// 		$("#errorAge").css("opacity", "1");
// 		$("#errorAge").css('display', 'block');
// 		$("#errorAge").css('margin-top', '10px');

// 	}
// 	$('#closeAge').click(function(){
// 		$("#errorAge").css('display', 'none');
// 	});
//    });

/*Script para inicializar el campo repeater*/
/*$('#kt_repeater_1').repeater({
	isFirstItemUndeletable: true
});*/
/* Scripts de Campos dependientes  ------*/
/* Checkbox al final de pagina Step 2  */
$("#go_track1").click(function() {
    if ($(this).is(':checked')) {
        $("#go-track1-capa").css('display', 'block');
    } else {
        $("#go-track1-capa").css('display', 'none');
    }
});

$("#go_track2").click(function() {
    if ($(this).is(':checked')) {
        $("#go-track2-capa").css('display', 'block');
    } else {
        $("#go-track2-capa").css('display', 'none');
    }
});

$("#go-track-viewpixel").click(function() {
    if ($(this).is(':checked')) {
        $("#go-track-viewpixel-capa").css('display', 'block');
    } else {
        $("#go-track-viewpixel-capa").css('display', 'none');
    }
});

$("#go_track3").click(function() {
    if ($(this).is(':checked')) {
        $("#go-track3-capa").css('display', 'block');
    } else {
        $("#go-track3-capa").css('display', 'none');
    }
});

/* Campo dependiente boton checkbox cargar audiencia   */
$("#lod_aud").click(function() {
    if ($(this).is(':checked')) {
        $("#audinc_camp").css('display', 'block');
    } else {
        $("#audinc_camp").css('display', 'none');
    }
});

/* Campo dependiente de radio destino masivo destino manual */
$(".dest").click(function() {
    if ($(this).val() == "1") {
        $("#dest_1").css('display', 'block');
        $("#dest_2").css('display', 'none')
    } else {
        $("#dest_1").css('display', 'none');
        $("#dest_2").css('display', 'block');
    }
});

/*  Script de checkbox campo dependiente de CONEXIONES Y PARTICIPACIÓN   */
$("#conx_part1").click(function() {
    if ($(this).is(':checked')) {
        $("#conx_part_camp1").css('display', 'block');
    } else {
        $("#conx_part_camp1").css('display', 'none');
    }
});

$("#conx_part2").click(function() {
    if ($(this).is(':checked')) {
        $("#conx_part_camp2").css('display', 'block');
    } else {
        $("#conx_part_camp2").css('display', 'none');
    }
});

$("#conx_part3").click(function() {
    if ($(this).is(':checked')) {
        $("#conx_part_camp3").css('display', 'block');
    } else {
        $("#conx_part_camp3").css('display', 'none');
    }
});

/* Script de checkbox campo dependiente guardar audiencia   */
$("#sav_adnc").click(function() {
    if ($(this).is(':checked')) {
        $("#sav_adnc_camp").css('display', 'block');
    } else {
        $("#sav_adnc_camp").css('display', 'none');
    }
});

/* script para tagsinput */
var addtags1 = 0;
$(".deltitulo").prop('disabled', true);
$(document).on('click', '.addtitulo', function() {
    if (addtags1 == 0) {
        $(".deltitulo").prop('disabled', false);
    }
    addtags1++;
    $("input[name='titulo[" + addtags1 + "][titulotags]']").tagsinput();
});

$(document).on('click', '.deltitulo', function() {
    addtags1--;
    if (addtags1 == 0) {
        $(".deltitulo").prop('disabled', true);
    }
});

var addtags2 = 0;
$(".deldescripcion").prop('disabled', true);

$(document).on('click', '.adddescripcion', function() {
    if (addtags2 == 0) {
        $(".deldescripcion").prop('disabled', false);
    }
    addtags2++;
    $("input[name='descripcion[" + addtags2 + "][descripciontags]']").tagsinput();
});

$(document).on('click', '.deldescripcion', function() {
    addtags2--;
    if (addtags2 == 0) {
        $(".deldescripcion").prop('disabled', true);
    }
});

var addtags3 = 0;

$(".delurl").prop('disabled', true);

$(document).on('click', '.addurl', function() {
    if (addtags3 == 0) {
        $(".delurl").prop('disabled', false);
    }
    addtags3++;
    $("input[name='url[" + addtags3 + "][urltags]']").tagsinput();
});

$(document).on('click', '.delurl', function() {
    addtags3--;
    if (addtags3 == 0) {
        $(".delurl").prop('disabled', true);
    }
});

var addtags4 = 0;

$(".deltext_link").prop('disabled', true);

$(document).on('click', '.addtext_link', function() {
    if (addtags4 == 0) {
        $(".deltext_link").prop('disabled', false);
    }
    addtags4++;
    $("input[name='text_link[" + addtags4 + "][text_linktags]']").tagsinput();
});

$(document).on('click', '.deltext_link', function() {
    addtags4--;
    if (addtags4 == 0) {
        $(".deltext_link").prop('disabled', true);
    }
});

var addtags5 = 0;

$(".delcall_to_action").prop('disabled', true);

$(document).on('click', '.addcall_to_action', function() {
    if (addtags5 == 0) {
        $(".delcall_to_action").prop('disabled', false);
    }
    addtags5++;
    $("input[name='call_to_action[" + addtags5 + "][call_to_actiontags]']").tagsinput();
});
$(document).on('click', '.delcall_to_action', function() {
    addtags5--;
    if (addtags5 == 0) {
        $(".delcall_to_action").prop('disabled', true);
    }
});

var addtags6 = 0;
$(".delimagen").prop('disabled', true);

$(document).on('click', '.addimagen', function() {
    if (addtags6 == 0) {
        $(".delimagen").prop('disabled', false);
    }
    addtags6++;

    $("input[name='imagen[" + addtags6 + "][imagentags]']").tagsinput();
});

$(document).on('click', '.delimagen', function() {
    addtags6--;
    if (addtags6 == 0) {
        $(".delimagen").prop('disabled', true);
    }
});

var addedades = 0;
$(".deledad").prop('disabled', true);
$(document).on('click', '.addedad', function() {
    if (addedades == 0) {
        $(".deledad").prop('disabled', false);
    }
    addedades++;
    $('[name="age[' + addedades + '][start]"]').rules('add', {
        ageValid: true,
        ageBigger: true
    });

    var nonLinearSlider = document.querySelectorAll('.age-slider:not(.noUi-target)');
    for (var i = 0; i < nonLinearSlider.length; i++) {
        noUiSlider.create(nonLinearSlider[i], {
            connect: true,
            behaviour: 'tap',
            start: [1, 100],
            range: {
                'min': [0],
                'max': [100]
            }
        });

        nonLinearSlider[i].noUiSlider.on('update', function(values, handle, unencoded, isTap, positions) {
            let minage = document.getElementsByClassName('min-age');
            let maxage = document.getElementsByClassName('max-age');
            minage.value = values[0];
            maxage.value = values[1];
        });
    }

    $('[name="age[twitter][0][start]"]').rules('add', {
        ageBigger: true
    });
    $('[name="age[twitter][0][end]"]').rules('add', {
        ageBigger: true
    });
    $('[name="age[linkedin][0][start]"]').rules('add', {
        ageBigger: true
    });
    $('[name="age[linkedin][0][end]"]').rules('add', {
        ageBigger: true
    });
});

$("#go_track1").click(function() {
    if ($(this).is(':checked')) {
        $("#go-track1-capa").css('display', 'block');
    } else {
        $("#go-track1-capa").css('display', 'none');
    }
});

$("#go_track2").click(function() {
    if ($(this).is(':checked')) {
        $("#go-track2-capa").css('display', 'block');
    } else {
        $("#go-track2-capa").css('display', 'none');
    }
});

$("#go-track-viewpixel").click(function() {
    if ($(this).is(':checked')) {
        $("#go-track-viewpixel-capa").css('display', 'block');
    } else {
        $("#go-track-viewpixel-capa").css('display', 'none');
    }
});

$("#go_track3").click(function() {
    if ($(this).is(':checked')) {
        $("#go-track3-capa").css('display', 'block');
    } else {
        $("#go-track3-capa").css('display', 'none');
    }
});

/* Campo dependiente boton checkbox cargar audiencia   */
$("#lod_aud").click(function() {
    if ($(this).is(':checked')) {
        $("#audinc_camp").css('display', 'block');
    } else {
        $("#audinc_camp").css('display', 'none');
    }
});



/* Campo dependiente de radio destino masivo destino manual */
$(".dest").click(function() {
    if ($(this).val() == "1") {
        $("#dest_1").css('display', 'block');
        $("#dest_2").css('display', 'none')
    } else {
        $("#dest_1").css('display', 'none');
        $("#dest_2").css('display', 'block');
    }
});


/*  Script de checkbox campo dependiente de CONEXIONES Y PARTICIPACIÓN   */
$("#conx_part1").click(function() {
    if ($(this).is(':checked')) {
        $("#conx_part_camp1").css('display', 'block');
    } else {
        $("#conx_part_camp1").css('display', 'none');
    }
});

$("#conx_part2").click(function() {
    if ($(this).is(':checked')) {
        $("#conx_part_camp2").css('display', 'block');
    } else {
        $("#conx_part_camp2").css('display', 'none');
    }
});

$("#conx_part3").click(function() {
    if ($(this).is(':checked')) {
        $("#conx_part_camp3").css('display', 'block');
    } else {
        $("#conx_part_camp3").css('display', 'none');
    }
});

/* Script de checkbox campo dependiente guardar audiencia   */
$("#sav_adnc").click(function() {
    if ($(this).is(':checked')) {
        $("#sav_adnc_camp").css('display', 'block');
    } else {
        $("#sav_adnc_camp").css('display', 'none');
    }
});

/* script para tagsinput */
var addtags1 = 0;
$(".deltitulo").prop('disabled', true);

$(document).on('click', '.addtitulo', function() {
    if (addtags1 == 0) {
        $(".deltitulo").prop('disabled', false);
    }
    addtags1++;
    $("input[name='titulo[" + addtags1 + "][titulotags]']").tagsinput();
});

$(document).on('click', '.deltitulo', function() {
    addtags1--;
    if (addtags1 == 0) {
        $(".deltitulo").prop('disabled', true);
    }
});

var addtags2 = 0;
$(".deldescripcion").prop('disabled', true);

$(document).on('click', '.adddescripcion', function() {
    if (addtags2 == 0) {
        $(".deldescripcion").prop('disabled', false);
    }
    addtags2++;
    $("input[name='descripcion[" + addtags2 + "][descripciontags]']").tagsinput();
});

$(document).on('click', '.deldescripcion', function() {
    addtags2--;
    if (addtags2 == 0) {
        $(".deldescripcion").prop('disabled', true);
    }
});

var addtags3 = 0;
$(".delurl").prop('disabled', true);

$(document).on('click', '.addurl', function() {
    if (addtags3 == 0) {
        $(".delurl").prop('disabled', false);
    }
    addtags3++;
    $("input[name='url[" + addtags3 + "][urltags]']").tagsinput();
});

$(document).on('click', '.delurl', function() {
    addtags3--;
    if (addtags3 == 0) {
        $(".delurl").prop('disabled', true);
    }
});

var addtags4 = 0;
$(".deltext_link").prop('disabled', true);

$(document).on('click', '.addtext_link', function() {
    if (addtags4 == 0) {
        $(".deltext_link").prop('disabled', false);
    }
    addtags4++;
    $("input[name='text_link[" + addtags4 + "][text_linktags]']").tagsinput();
});

$(document).on('click', '.deltext_link', function() {
    addtags4--;
    if (addtags4 == 0) {
        $(".deltext_link").prop('disabled', true);
    }
});

var addtags5 = 0;
$(".delcall_to_action").prop('disabled', true);

$(document).on('click', '.addcall_to_action', function() {
    if (addtags5 == 0) {
        $(".delcall_to_action").prop('disabled', false);
    }
    addtags5++;
    $("input[name='call_to_action[" + addtags5 + "][call_to_actiontags]']").tagsinput();
});

$(document).on('click', '.delcall_to_action', function() {
    addtags5--;
    if (addtags5 == 0) {
        $(".delcall_to_action").prop('disabled', true);
    }
});

var addtags6 = 0;
$(".delimagen").prop('disabled', true);

$(document).on('click', '.addimagen', function() {
    if (addtags6 == 0) {
        $(".delimagen").prop('disabled', false);
    }
    addtags6++;
    $("input[name='imagen[" + addtags6 + "][imagentags]']").tagsinput();
});

$(document).on('click', '.delimagen', function() {
    addtags6--;
    if (addtags6 == 0) {
        $(".delimagen").prop('disabled', true);
    }
});

var addedades = 0;

$(".deledad").prop('disabled', true);

$(document).on('click', '.addedad', function() {
    if (addedades == 0) {
        $(".deledad").prop('disabled', false);
    }
    addedades++;
    $('[name="age[' + addedades + '][start]"]').rules('add', {
        ageValid: true,
        ageBigger: true
    });
    $('[name="age[' + addedades + '][end]"]').rules('add', {
        ageBigger: true
    });
});

$(document).on('click', '.deledad', function() {
    $('[name="age[' + addedades + '][start]"]').rules('remove');
    $('[name="age[' + addedades + '][end]"]').rules('remove');
    addedades--;
    if (addedades == 0) {
        $(".deledad").prop('disabled', true);
    } else {
        $(".deledad").prop('disabled', false);
    }
});

var addposti1 = 0;
$(".delposti").prop('disabled', true);

$(document).on('click', '.addposti', function() {
    if (addposti1 == 0) {
        $(".delposti").prop('disabled', false);
    }
    addposti1++;
});

$(document).on('click', '.delposti', function() {
    addposti1--;
    if (addposti1 == 0) {
        $(".delposti").prop('disabled', true);
    }
});

$(".budgetrs").hide();
$(".dt-rrss").hide();

/*script pr redes sociales*/
var arrayrs = new Array;

$("input[name='rrss[]']").click(function() {
    if ($(this).is(':checked')) {
        if (arrayrs.length == 0) {
            $("#btitulo").show();
        }
        arrayrs.push($(this).val());
        $("#b" + $(this).val()).show();
        $("#dt-" + $(this).val()).show();
        $("#dt2-" + $(this).val()).show();
        $("#dt3-" + $(this).val()).show();
        $(".placements_" + $(this).val()).show();
        $(".pixels-" + $(this).val()).show();

        $("#posts-" + $(this).val()).show();
        var currency = $("#adsaccount-" + $(this).val() + " option:selected").attr('currency');
        $("#price-" + $(this).val()).html(currency);
        $("#bid-" + $(this).val()).html(currency);
    } else {
        var pos = arrayrs.indexOf($(this).val());
        if (pos !== -1) {
            arrayrs.splice(pos, 1);
            if (arrayrs.length == 0) {
                $("#btitulo").hide();
            }
        }
        //crearbudget(arrayrs);
        $("#b" + $(this).val()).hide();
        $("#dt-" + $(this).val()).hide();
        $(".placements_" + $(this).val()).hide();
        $(".pixels-" + $(this).val()).hide();
        $("#posts-" + $(this).val()).hide();
    }
})

/*script para calculos de budget*/
$('#budget').blur(function() {
    var longitud = arrayrs.length
    if ($(this).val() != '') {
        var total = parseFloat($(this).val());
        var acum = 0;
        var acump = 0;
        var porcion = total / longitud;
        var porcentaje = 100 / longitud;
        arrayrs.forEach(function(elemento, indice, array) {
            if (indice != (longitud - 1)) {
                $('#bi' + elemento).val(porcion.toFixed(2));
                $('#bp' + elemento).val(porcentaje.toFixed(2) + "%");
                acump = acump + parseFloat(porcentaje.toFixed(2));
                acum = acum + parseFloat(porcion.toFixed(2));
            } else {
                porcion = total - acum;
                porcentaje = 100 - acump;
                $('#bi' + elemento).val(porcion.toFixed(2));
                $('#bp' + elemento).val(porcentaje.toFixed(2) + "%");
            }
        });
        $('#budget_split').show(200);
    }
});

$('.bi_rrss').blur(function() {
    var total = 0;
    var longitud = arrayrs.length
    $(".bi_rrss").each(function(indice) {
        var num = parseFloat($(this).val());
        if (num > 0) {
            total = total + parseFloat($(this).val());
        }
    });
    arrayrs.forEach(function(elemento, indice, array) {
        var num = parseFloat($('#bi' + elemento).val());

        if (num > 0) {
            var porcentaje = num * 100 / total;
            $('#bp' + elemento).val(porcentaje.toFixed(2) + "%");
        }
    });
    $('#budget').val(total.toFixed(2));
});

$('.form-porcen').blur(function() {
    var total = 0;
    $(".form-porcen").each(function(indice) {
        var num = parseFloat($(this).val());
        if (num > 0) {
            total = total + parseFloat($(this).val());
        }
    });
    if (total.toFixed(2) > 100) {
        $('.error_bidding').html("The percentage must not be greater than 100%");
        $('.error_bidding').show();
    } else if (total.toFixed(2) < 100) {
        $('.error_bidding').html("The percentage should not be less than 100%");
        $('.error_bidding').show();
    } else {
        $('.error_bidding').html("");
        $('.error_bidding').hide();
    }
});

$('#select_client').change(function() {
    change_client($(this).val());
});

let container_ads_property = {};

function change_client(client) {


    $('#loading').modal('show');
    $("input[name='rrss[]']").each(function(index) {
        $("label[for='" + $(this).attr('id') + "']").css('background', '#ececec');
        if ($(this).prop('checked')) {
            // console.log($(this).val());
            $("input[value='" + $(this).val() + "']").click();
        }
    });
    $("input[name='rrss[]']").prop('disabled', true);
    $("input[name='rrss[]']").prop("checked", false);

    if (client == undefined) {
        $('#loading').modal('hide');
        return false;
    }


    $.ajax({
        type: "GET",
        url: "/api/auth_by_client/" + client,
        success: function(data) {
            $.each(data, function(index, value) {
                $("input[name='rrss[]'][value='" + value.platform.toLowerCase() + "']").prop('disabled', false);
                var id = $("input[name='rrss[]'][value='" + value.platform.toLowerCase() + "']").attr('id');
                $("label[for='" + id + "']").css('background', '');

                ad_account_by_client($('#select_client').val(), value.platform);
            });
            $('#loading').modal('hide');
        }
    });
}

function ad_account_by_client(client, platform) {

    $("#adsaccount-" + platform.toLowerCase()).empty();
    $("#properties-" + platform.toLowerCase()).empty();
    
    $('#loading').modal('show');

    $.ajax({
        type: "GET",
        url: "/api/account_by_client/" + client + "/" + platform,
        success: function(data) {
            console.log("api call", data)
            
            if (data.accounts.length > 0) {
                $.each(data.accounts, function(index, value) {
                    $("#adsaccount-" + data.platform.toLowerCase()).append('<option value="' + value.public_id + '" currency="' + value.currency + '" account_id="' + value.account_id + '" account_user_id="' + value.id + '">' + value.name + '</option>');
                });
            } else {
                $("#adsaccount-" + data.platform.toLowerCase()).append('<option value="">No hay Ads accounts</option>');
            }

            container_ads_property['ads_accounts_' + data.platform.toLowerCase()] = data.accounts;
            container_ads_property['ads_accounts_properties_' + data.platform.toLowerCase()] = data.properties;
            container_ads_property['ads_accounts_pixels_' + data.platform.toLowerCase()] = data.pixels;

            let account_selected = $("#adsaccount-" + data.platform.toLowerCase()).val();
            $("#properties-" + data.platform.toLowerCase()).empty();
            $("#pixels-" + data.platform.toLowerCase()).empty();
            var currency = $("#adsaccount-" + data.platform.toLowerCase() + " option:selected").attr('currency');
            $("#price-" + data.platform.toLowerCase()).html(currency);
            $("#bid-" + data.platform.toLowerCase()).html(currency);

            //step1 properties
            var filteredArray = data.properties.filter(function(itm) { if (account_selected == itm.ad_account_public_id) { return itm; } });

            if (filteredArray.length > 0) {
                $.each(filteredArray, function(index, value) {
                    $("#properties-" + data.platform.toLowerCase()).append('<option value="' + value.property_publicid + '">' + value.name + '</option>');
                });
            } else {
                $("#properties-" + data.platform.toLowerCase()).append('<option value="">No hay properties</option>');
            }

            // step 4 pixels
            var filteredArraypixels = data.pixels.filter(function(itm) {
                if (account_selected == itm.ad_account_public_id) { return itm; }
            });

            if (filteredArraypixels.length > 0) {
                $.each(filteredArraypixels, function(index, value) {
                    $("#pixels-" + data.platform.toLowerCase()).append('<option value="' + value.id + '">' + value.name + '</option>');
                });
            } else {
                $("#pixels-" + data.platform.toLowerCase()).append('<option value="">No hay pixels</option>');
            }


            $('#loading').modal('hide');
        }
    });
}

function property_by_client(client) {

}

change_client($('#select_client').val());

$('[id^="checkbtn_place"]').change(function() {

    var id = this.id.split("_");
    var rrss = "";

    if (id[2] == "all") {
        rrss = id[3];
        if (this.checked) {
            $('[rrss="' + rrss + '"]').prop("checked", true);
        } else {
            $('[rrss="' + rrss + '"]').prop("checked", false);
        }
    } else {
        rrss = $(this).attr('rrss');
        if (this.checked) {
            var chk = true;
            $('[rrss="' + rrss + '"]').each(function(i) {
                if (this.checked == false) {
                    chk = false;
                }
            });
            if (chk) {
                $('#checkbtn_place_all_' + rrss).prop("checked", true);
            }
        } else {
            $('#checkbtn_place_all_' + rrss).prop("checked", false);
        }
    }
});

$("#d_aPromoted").css('display', 'none');




$("#radiobtn_peq1").click(function() {

    $("#d_promote").css('display', 'block');
    $("#d_aPromoted").css('display', 'none');

    $("#radiobtn_peq1").attr('checked', true);
    $("#radiobtn_peq2").attr('checked', false);

    console.log("radioo", $("#radiobtn_peq2").val());

});

$("#radiobtn_peq2").click(function() {

    $("#d_promote").css('display', 'none');
    $("#d_aPromoted").css('display', 'block');

    $("#radiobtn_peq1").attr('checked', false);
    $("#radiobtn_peq2").attr('checked', true);

    console.log("radioo", $("#radiobtn_peq1").val());

});

/* campos de ads design create or latest post */
$("#d_latest_adsdesign").css('display', 'none');

$("#radiobtn_create_ad").click(function() {
    //console.log('1');
    $("#d_create_adsdesign").css('display', 'block');
    $("#d_latest_adsdesign").css('display', 'none');

    $("#radiobtn_create_ad").attr('checked', true);
    $("#radiobtn_latest_ad").attr('checked', false);


});

$("#radiobtn_latest_ad").click(function() {
    $("#d_create_adsdesign").css('display', 'none');
    $("#d_latest_adsdesign").css('display', 'block');

    $("#radiobtn_create_ad").attr('checked', false);
    $("#radiobtn_latest_ad").attr('checked', true);
    //console.log('2');
});

/*campos del radio posts RSS (url)*/
$("#aposts1").click(function() {
    //console.log('1');
    $("#post_url").css('display', 'block');
    $("#select_latestposts_post").css('display', 'none');
    $(".promote_post_with_more_than").css('display', 'none');
    $("#select_ganalitycs").css('display', 'none');
    $('#se_promotePost_GA').hide();
    $('#se_promotePost').show();


    $("#aposts1").attr('checked', true);
    $("#aposts2").attr('checked', false);
    $("#aposts3").attr('checked', false);


});

$("#aposts2").click(function() {
    //console.log('1');
    $("#post_url").css('display', 'none');
    $("#select_latestposts_post").css('display', 'none');
    $(".promote_post_with_more_than").css('display', 'block');
    $("#select_ganalitycs").css('display', 'flex');
    $('#se_promotePost_GA').show();
    $('#se_promotePost').hide();
    $("#aposts1").attr('checked', false);
    $("#aposts2").attr('checked', true);
    $("#aposts3").attr('checked', false);
});

$("#aposts3").click(function() {
    //console.log('1');
    $("#post_url").css('display', 'none');
    $("#select_latestposts_post").css('display', 'block');
    $(".promote_post_with_more_than").css('display', 'block');
    $("#select_ganalitycs").css('display', 'none');
    $('#se_promotePost_GA').hide();
    $('#se_promotePost').show();
    $("#aposts1").attr('checked', true);
    $("#aposts2").attr('checked', false);
    $("#aposts3").attr('checked', false);
});

/* Filtros CB Latest Posts */
$("#t_postLink").attr('disabled', 'disabled');

$('#cb_postLink').click(function() {
    //var tab =$(this).attr('tab');
    if ($(this).is(':checked')) {
        $("#t_postLink").removeAttr('disabled');
    } else {
        $("#t_postLink").attr('disabled', 'disabled');
    }
});

$("#t_postText").attr('disabled', 'disabled');

$('#cb_postText').click(function() {
    //var tab =$(this).attr('tab');
    if ($(this).is(':checked')) {
        $("#t_postText").removeAttr('disabled');
        $("#t_postText").tagsinput();
    } else {
        $("#t_postText").tagsinput('destroy');
        $("#t_postText").attr('disabled', 'disabled');
    }
});

$("#kt_repeater_13 input").attr('disabled', 'disabled');

$("#kt_repeater_13 select").attr('disabled', 'disabled');

$('#cb_promotePost').click(function() {

    //var tab =$(this).attr('tab');
    if ($(this).is(':checked')) {
        $("#kt_repeater_13 input").removeAttr('disabled');
        $("#kt_repeater_13 select").removeAttr('disabled');
    } else {
        $("#kt_repeater_13 input").attr('disabled', 'disabled');
        $("#kt_repeater_13 select").attr('disabled', 'disabled');
    }
});

$('.addposti').click(function() {
    if ($('#cb_promotePost').is(':checked')) {
        $("#kt_repeater_13 input").removeAttr('disabled');
        $("#kt_repeater_13 select").removeAttr('disabled');
    } else {
        $("#kt_repeater_13 input").attr('disabled', 'disabled');
        $("#kt_repeater_13 select").attr('disabled', 'disabled');
    }
});



/* script para los campos emergentes de los botones **
	$(".radio_btn").click(function(){
		if( $( this ).val() == "1"  ){
			$("#link_stndr").css('display', 'block');
			$("#fb_ie-capa").css('display', 'none')
		}else{
			$("#link_stndr").css('display', 'none');
			$("#fb_ie-capa").css('display', 'block');
		}
	});
*/

/**********************/
/*  Script campos dependientes botones radio enlace estandar y facebook instant experience   */
function checkRedActive() {
    // console.log("checkRedActive");
    var band = true;
    $('.redSocial').each(function() {
        if ($(this).is(':checked') && band) {
            band = false;
            op = "a[href='" + $(this).attr('data-option') + "']";
            $(op).click();
        }
    });
    if (band) {
        $('#tab-social-details-audience .tab-pane').removeClass('active');
        $('.link-social-details-audience').removeClass('active');
        $('#tab-social-details .tab-pane').removeClass('active');
        $('.link-social-details').removeClass('active');
    }
}


$('.redSocial').click(function() {

    var is_block = $(this).attr('blocked');
    if (is_block) {
        //window.location.href = "{{route('mysubscription')}}";
        $('#modalSubscription').modal('show');
        return false;
    }
    var tab = $(this).attr('tab');
    if ($(this).is(':checked')) {
        $("a[href='#kt_tabs_" + tab + "']").parent().css('display', 'block');
        $("a[href='#kt_tabs_" + tab + "']").click();
        $("a[href='#kt_tabs_audience_" + tab + "']").parent().css('display', 'block');
        $("a[href='#kt_tabs_audience_" + tab + "']").click();
        checkRedActive();
        fcross_plataformas(tab, 1);
    } else {
        $("a[href='#kt_tabs_" + tab + "']").parent().css('display', 'none');
        $("a[href='#kt_tabs_audience" + tab + "']").parent().css('display', 'none');
        checkRedActive();
        fcross_plataformas(tab, 0);
    }
});

//Dalibrandi
function fcross_plataformas(tab, type) {
    //console.log("red crosss")

    if (cross_plataformas == false) {
        $('.redSocial.step1').each(function() {
            if ($(this).attr('tab') != tab) {
                if (type == 1) {
                    $(this).attr('blocked', true);
                    $("[for='checkbtn_gde" + $(this).attr('tab') + "']").addClass("block");
                    $("[for='checkbtn_gde" + $(this).attr('tab') + "'] .fa-ban").show();
                } else {
                    $(this).removeAttr("blocked");
                    $("[for='checkbtn_gde" + $(this).attr('tab') + "']").removeClass("block");
                    $("[for='checkbtn_gde" + $(this).attr('tab') + "'] .fa-ban").hide();
                }
            }
        });
    }
}

$(".radio-test").click(function() {
    if ($(this).val() == "x") {
        $("#link_stndr").css('display', 'block');
        $("#fb_ie-capa").css('display', 'none')
    } else {
        $("#link_stndr").css('display', 'none');
        $("#fb_ie-capa").css('display', 'block');
    }
});

/* Script campos dependientes botones radio audiencia estandar y multiples aud.   */

$(".mlt_aud").click(function() {
    if ($(this).val() == "1") {
        $("#tst_multi_aud1").css('display', 'block');
        $("#tst_multi_aud2").css('display', 'none')
    } else {
        $("#tst_multi_aud1").css('display', 'none');
        $("#tst_multi_aud2").css('display', 'block');
    }
});


/* Script campo dep. guardar o sobreescrib. aud.     */

$(".save_ovw").click(function() {
    if ($(this).val() == "1") {
        $("#save_new").css('display', 'block');
        $("#ovw_aud").css('display', 'none')
    } else {
        $("#save_new").css('display', 'none');
        $("#ovw_aud").css('display', 'block');
    }
});

/* script de campo dep. formato normal y carrusel  */

$(".norm_crsl").click(function() {
    if ($(this).val() == "1") {
        $("#formt_norm").css('display', 'block');
        $("#carrusel").css('display', 'none')
    } else {
        $("#formt_norm").css('display', 'none');
        $("#carrusel").css('display', 'block');
    }
});

/*  Script campo dependiente radio coste + bajo con tope de oferta  <---- NO FUNCIONA, REVISAR */

/*	$("#low_top_rad").click(function(){
		if( $( this ).is(':checked')  ){
			$("#low_cost_top").css('display', 'flex');
		}else{
			$("#low_cost_top").css('display', 'none');
		}
	});
*/

$(".low-top").change(function() {
    var rrss = $(this).attr('rrss');
    //console.log(rrss);
    if ($(this).hasClass('low_top_rad')) {
        $("#low_cost_top_" + rrss).css('display', 'flex');
    } else {
        $("#low_cost_top_" + rrss).css('display', 'none');
    }
});
/*$("#low_cost").click(function(){
	if( $( this ).is(':checked')  ){
		$("#low_cost_top").css('display', 'none');
	}
});*/

var _URL = window.URL || window.webkitURL;
var images_uploaded = [];


$(document).ready(function() {
    $("#weekly-schedule").dayScheduleSelector({
        interval: 60,
        enableHorizontalSelection: true,
        startTime: '00:00',
        endTime: '24:00'
    });
});


function previewImage(input, index, tipoVideo) {


    if (!tipoVideo) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
                $('div[name="imagen[' + index + '][imagenpreview]"]').css("background-image", "url(" + e.target.result + ")").show();
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string

        }
    } else {
        $('video[name="imagen[' + index + '][imagenpreview_video_player]"]').show();

        const reader = new FileReader();

        reader.onload = function(e) {
            $('video[name="imagen[' + index + '][imagenpreview_video_player]"]').attr("src", e.target.result).show();
        };

        reader.readAsDataURL(input.files[0]);

    }


}

function imgUpload(objj) {
    //console.log(objj)

    let elementName = objj.name;
    let elementValue = objj.value;
    //console.log('elementName',elementName)
    let file, img;
    let tipoFileVideo = false;
    let index = elementName.replace('imagen[', '');
    index = index.replace('][imagen]', '');

    // limpiamos los preview    
    $('video[name="imagen[' + index + '][imagenpreview_video_player]"]').attr("src", '');
    $('img[name="imagen[' + index + '][imagenpreview]"]').attr("src", '');
    // ocultamos response text
    $('div[name="imagen[' + index + '][imagenresponse]"]').removeClass('alert alert-success alert-danger').hide();


    // check file extension
    var extension_images_allowed = ['jpeg', 'jpg', 'png'];
    var extension_videos_allowed = ['mp4', 'gif'];

    var extensions_accepted = extension_images_allowed.concat(extension_videos_allowed);

    if ($.inArray(elementValue.split('.').pop().toLowerCase(), extensions_accepted) == -1) {
        $('div[name="imagen[' + index + '][imagenresponse]"]').addClass('alert alert-danger').html("Only formats are allowed : " + extensions_accepted.join(', ')).show();
        return false;
    } else {

        if ($.inArray(elementValue.split('.').pop().toLowerCase(), extension_videos_allowed) >= 0) {
            tipoFileVideo = true;
        } else {
            tipoFileVideo = false;
        }

        $('div[name="imagen[' + index + '][imagenresponse]"]').removeClass('alert alert-success alert-danger').html("");
    }

    // validacion tamano y upload
    if (file = objj.files[0]) {

        fileThis = objj;
        img = new Image();

        var objectUrl = _URL.createObjectURL(file);

        $('label[name="imagen[' + index + '][imagenlabel]"]').addClass("selected").html(objj.value);


        img.onload = function() {
            _URL.revokeObjectURL(objectUrl);
            if (!tipoFileVideo) {
                if (this.width < 1200 && this.height < 628) {
                    $('div[name="imagen[' + index + '][imagenresponse]"]').addClass('alert alert-danger').html("Min. image size allowed is 1200x628px.").show();
                    return false;
                }
            }
        };


        img.src = objectUrl;

        // start.ajax


        formDataImg = new FormData();
        formDataImg.append("fileUpload", file);
        formDataImg.append("fileInputTags", $("input[name='imagen[" + index + "][imagentags]']").tagsinput('items'));

        let url = !tipoFileVideo ? '/api/images-save/' : '/api/videos-save/';

        $.ajax({
            url: url,
            data: formDataImg,
            processData: false,
            contentType: false,
            async: true,
            beforeSend: function() {
                // Handle the beforeSend event
                $('div[name="imagen[' + index + '][imagenresponse]"]').removeClass('alert alert-danger alert-success').html("<img src='img/loaderIcon.gif'>").show();
            },
            type: 'POST',
            success: function(result) {

                var jsonString = JSON.parse(result);

                previewImage(fileThis, index, tipoFileVideo);

                $.each(jsonString, function(k, v) {
                    if (v.status == true) {
                        images_uploaded.push(v);
                        $('div[name="imagen[' + index + '][imagenresponse]"]').html("Succesfully uploaded.").addClass('alert alert-success').show();
                        $('div[name="imagen[' + index + '][imagen_inputs_tags]"]').show();
                        setTimeout(function() {
                            $('div[name="imagen[' + index + '][imagenresponse]"]').html("").removeClass('alert alert-success').hide();
                        }, 2500);

                    } else {
                        $('div[name="imagen[' + index + '][imagenresponse]"]').html(v.msj).addClass('alert alert-danger').show();
                    }
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                $('div[name="imagen[' + index + '][imagenresponse]"]').addClass('alert alert-danger').html(errorThrown).show();
            }
        });


        //      console.log('finnnn', images_uploaded)



        //end.ajax



    }


}

function checkAllRow(id) {

    $(".time-slot").each(function(index) {
        if ($(this).attr('data-day') == id) {
            $(this).click();
        }
    });

}

function formatStr(str) {
    // string to camel
    //return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) { return index === 0 ? word.toLowerCase() : word.toUpperCase(); }).replace(/\s+/g, '');
    //string tu upper + $$
    return '$$' + str.replace(" ", "").toUpperCase();
}

function appendPlaceHolder(destination, value) {
    let str = $('#' + destination).val();
    $('#' + destination).val(str + formatStr(value)).focus();
}

function sync_account(platform) {

    let ad_account = $("#adsaccount-" + platform).val();

    form = new FormData();
    form.append("ad_account", ad_account);

    $.ajax({
        url: 'api/conexion_updater',
        data: form,
        processData: false,
        contentType: false,
        async: true,
        beforeSend: function() {
            // $('div[name="imagen[' + index + '][imagenresponse]"]').removeClass('alert alert-danger alert-success').html("<img src='img/loaderIcon.gif'>").show();
        },
        type: 'POST',
        success: function(result) {

            console.log(result)

        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });

}


// AJAX call for autocomplete
var timeouts_location;

function delay(ms, callback) {
    var timer = 0;
    return function() {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function() {
            callback.apply(context, args);
        }, ms || 0);
    };
}

$(document).ready(function() {
    var location_cache = {};
    var location_cache_linkedin = {}
    var location_cache_twitter = {}
        /* autocomplete para locations */

    var getAjaxLocationFacebook;
    $("#location_facebook").keyup(delay(3000, function() {
        console.log('buscamos FBBB location', $(this).val());
        let location_name = $(this).val();
        var location_shown = {};

        if (location_name == '') {
            $("#location-box-facebook").empty();
            return;
        }
        if (location_name.length < 3) {
            $("#location-box-facebook").empty();
            return;
        }
        if (location_cache[location_name]) {
            $("#location-box-facebook").empty();
            $("#location-box-facebook").html(location_cache[location_name]);
            return;
        }
        var t0 = performance.now();

        getAjaxLocationFacebook && getAjaxLocationFacebook.readyState !== 4 && getAjaxLocationFacebook.abort();

        getAjaxLocationFacebook = $.ajax({
            type: "GET",
            url: 'https://graph.facebook.com/v8.0/search/?location_types=["country", "country_group", "region", "city", "zip", "geo_market"]&type=adgeolocation&q=' + location_name + '&access_token=' + acces_token,
            data: '',
            beforeSend: function() {
                $("#location_facebook").css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");
            },
            success: function(result) {
                let data = result.data;

                $("#location-box-facebook").empty();
                if (!data) {
                    $("#location-box-facebook").html('<li class="locationsOptions">No Results</li>');
                    return;
                }
                let valueResponse = $("#location-box-facebook").html();
                if (valueResponse.includes('</ul>')) {
                    valueResponse = valueResponse.replace('</ul>', '');
                } else {
                    valueResponse += "<ul>";
                }

                saveAutoComplete(data, 'FACEBOOK');

                data.map(function(obj) {
                    if (location_shown[data.key]) {

                        return;
                    }
                    let reference = obj.name;
                    if (typeof obj.region != 'undefined') {
                        reference += ', ' + obj.region;
                    }
                    if (typeof obj.country_name != 'undefined') {
                        reference += ', ' + obj.country_name;
                    }
                    valueResponse += '<li class="locationsOptions"  data-id=' + obj.key + ' data-name=\'' + reference + '\' data="' + (JSON.stringify(obj)).replace(/\"/g, "'") + '">' + reference + '<span>' + obj.type + '</span></li>';
                });
                valueResponse += "</ul>";
                $("#location-box-facebook").html(valueResponse);
                $("#location_facebook").css("background", "#FFF");
                location_cache[location_name] = valueResponse;
            },
            error: function() {
                $("#location_facebook").css("background", "#FFF");
            }
        });
    }));

    $(document).on("click", "#location-list-facebook i", function() { $(this).parent().remove(); });
    $(document).on("click", "#location-box-facebook ul li", function() {
        $('#location-box-facebook ul').empty();
        var text = "<li data-id='" + $(this).attr('data-id') + "' data=\"" + $(this).attr('data') + "\" data-name='" + $(this).attr('data-name') + "' class='" + $("#location-action-facebook").val() + "' data-location-option='" + $('#location_option_facebook').val() + "' >" + $(this).attr('data-name') + '<svg id="_27_Icon_close" data-name="27) Icon/close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M7.414,6l4.293-4.293A1,1,0,1,0,10.293.293L6,4.586,1.707.293A1,1,0,0,0,.293,1.707L4.586,6,.293,10.293a1,1,0,1,0,1.414,1.414L6,7.414l4.293,4.293a1,1,0,0,0,1.414-1.414Z" transform="translate(6 6)" fill="#222b45"/> </svg></li>';
        $("#location-list-facebook").append(text);
        $("#location-list-instagram").append(text);
        $('#location_facebook').val('');
    });
    $('#location-action').select2({ placeholder: "Select an option", minimumResultsForSearch: Infinity });



    var getAjaxLocationInstagram;
    $("#location_instagram").keyup(delay(3000, function() {
        let location_name = $(this).val();
        var location_shown = {};
        if (location_name == '') {
            $("#location-box-instagram").empty();
            return;
        }


        if (location_cache[location_name]) {
            $("#location-box-instagram").empty();
            $("#location-box-instagram").html(location_cache[location_name]);
            return;
        }
        var t0 = performance.now();

        getAjaxLocationInstagram && getAjaxLocationInstagram.readyState !== 4 && getAjaxLocationInstagram.abort();

        getAjaxLocationInstagram = $.ajax({
            type: "GET",
            url: 'https://graph.facebook.com/v8.0/search/?location_types=["country", "country_group", "region", "city", "zip", "geo_market"]&type=adgeolocation&q=' + location_name + '&access_token=' + acces_token,
            data: '',
            beforeSend: function() {
                $("#location_instagram").css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");
            },
            success: function(result) {
                let data = result.data;

                $("#location-box-instagram").empty();
                if (!data) {
                    $("#location-box-instagram").html('<li class="locationsOptions">No Results</li>');
                    return;
                }
                let valueResponse = $("#location-box-instagram").html();
                if (valueResponse.includes('</ul>')) {
                    valueResponse = valueResponse.replace('</ul>', '');
                } else {
                    valueResponse += "<ul>";
                }

                saveAutoComplete(data, 'INSTAGRAM');

                data.map(function(obj) {
                    if (location_shown[data.key]) {

                        return;
                    }
                    let reference = obj.name;
                    if (typeof obj.region != 'undefined') {
                        reference += ', ' + obj.region;
                    }
                    if (typeof obj.country_name != 'undefined') {
                        reference += ', ' + obj.country_name;
                    }
                    valueResponse += '<li class="locationsOptions"  data-id=' + obj.key + ' data-name=\'' + reference + '\' data="' + (JSON.stringify(obj)).replace(/\"/g, "'") + '">' + reference + '<span>' + obj.type + '</span></li>';
                });
                valueResponse += "</ul>";
                $("#location-box-instagram").html(valueResponse);
                $("#location_instagram").css("background", "#FFF");
                location_cache[location_name] = valueResponse;
            },
            error: function() {
                $("#location_instagram").css("background", "#FFF");
            }
        });
    }));

    $(document).on("click", "#location-list-instagram i", function() { $(this).parent().remove(); });
    $(document).on("click", "#location-box-instagram ul li", function() {
        $('#location-box-instagram ul').empty();
        var text = "<li data-id='" + $(this).attr('data-id') + "' data=\"" + $(this).attr('data') + "\" data-name='" + $(this).attr('data-name') + "' class='" + $("#location-action-instagram").val() + "' data-location-option='" + $('#location_option_instagram').val() + "' >" + $(this).attr('data-name') + '<svg id="_27_Icon_close" data-name="27) Icon/close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M7.414,6l4.293-4.293A1,1,0,1,0,10.293.293L6,4.586,1.707.293A1,1,0,0,0,.293,1.707L4.586,6,.293,10.293a1,1,0,1,0,1.414,1.414L6,7.414l4.293,4.293a1,1,0,0,0,1.414-1.414Z" transform="translate(6 6)" fill="#222b45"/> </svg></li>';
        $("#location-list-instagram").append(text);
        $("#location-list-facebook").append(text);
        $('#location_instagram').val('');
    });
    $('#location-action').select2({ placeholder: "Select an option", minimumResultsForSearch: Infinity });


    var getAjaxLocationLinkedin;
    $("#location_linkedin").keyup(delay(3000, function() {
        let location_name = $(this).val();
        var location_shown = {};
        if (location_name === '') {
            $("#location-box-linkedin").empty();
            return;
        }
        /*if(location_name.length < 3 ){
        	$("#location-box-linkedin").empty();
        	return;
        }*/

        if (location_cache_linkedin[location_name]) {
            $("#location-box-linkedin").empty();
            $("#location-box-linkedin").html(location_cache_linkedin[location_name]);
            return;
        }
        var t0 = performance.now();
        var user_account_id = $("#adsaccount-linkedin option:selected").attr('account_user_id');

        getAjaxLocationLinkedin && getAjaxLocationLinkedin.readyState !== 4 && getAjaxLocationLinkedin.abort();

        data = { "query": location_name };

        getAjaxLocationLinkedin = jQuery.ajax({
            url: 'api/getLocation',
            type: 'POST',
            data: data,
            accept: "application/json",
            beforeSend: function() {
                $("#location_linkedin").css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");
            },
            success: function(data) {

                $("#location-box-linkedin").empty();
                if (!data) {
                    $("#location-box-linkedin").html('<li class="locationsOptions">No Results</li>');
                    return;
                }
                let valueResponse = $("#location-box-linkedin").html();
                if (valueResponse.includes('</ul>')) {
                    valueResponse = valueResponse.replace('</ul>', '');
                } else {
                    valueResponse += "<ul>";
                }

                saveAutoComplete(data, 'LINKEDIN');

                data.map(function(obj) {

                    if (location_shown[data.name]) {

                        return;
                    }
                    let reference = obj.name;

                    valueResponse += '<li class="locationsOptions" data-id=' + obj.uuid + ' data-name=\'' + reference + '\' data="' + (JSON.stringify(obj)).replace(/\"/g, "'") + '">' + reference + '</li>';
                });
                valueResponse += "</ul>";
                $("#location-box-linkedin").html(valueResponse);
                $("#location_linkedin").css("background", "#FFF");
                location_cache_linkedin[location_name] = valueResponse;
            },
            error: function() {
                $("#location_linkedin").css("background", "#FFF");
            }
        });
    }));

    $(document).on("click", "#location-list-linkedin i", function() { $(this).parent().remove(); });
    $(document).on("click", "#location-box-linkedin ul li", function() {
        $('#location-box-linkedin ul').empty();
        $("#location-list-linkedin").append("<li data-id='" + $(this).attr('data-id') + "' data=\"" + $(this).attr('data') + "\" data-name='" + $(this).attr('data-name') + "' class='" + $("#location-action-linkedin").val() + "'  data-location-option='" + $('#location_option_linkedin').val() + "' >" + $(this).attr('data-name') + '<svg id="_27_Icon_close" data-name="27) Icon/close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M7.414,6l4.293-4.293A1,1,0,1,0,10.293.293L6,4.586,1.707.293A1,1,0,0,0,.293,1.707L4.586,6,.293,10.293a1,1,0,1,0,1.414,1.414L6,7.414l4.293,4.293a1,1,0,0,0,1.414-1.414Z" transform="translate(6 6)" fill="#222b45"/> </svg></li>');
        $('#location_linkedin').val('');
    });
    $('#location-action-linkedin').select2({ placeholder: "Select an option", minimumResultsForSearch: Infinity });


    var getAjaxLocationTwitter;

    $("#location_twitter").keyup(delay(3000, function() {
        let location_name = $(this).val();
        var location_shown = {};
        if (location_name === '') {
            $("#location-box-twitter").empty();
            return;
        }


        if (location_cache_twitter[location_name]) {
            $("#location-box-twitter").empty();
            $("#location-box-twitter").html(location_cache_twitter[location_name]);
            return;
        }
        var t0 = performance.now();
        var user_account_id = $("#adsaccount-twitter option:selected").attr('account_user_id');

        getAjaxLocationTwitter && getAjaxLocationTwitter.readyState !== 4 && getAjaxLocationTwitter.abort();

        data = { "query": location_name };

        getAjaxLocationTwitter = jQuery.ajax({
            url: 'api/getLocation',
            type: 'POST',
            data: data,
            accept: "application/json",
            beforeSend: function() {
                $("#location_twitter").css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");
            },
            success: function(data) {

                $("#location-box-twitter").empty();
                if (!data) {
                    $("#location-box-twitter").html('<li class="locationsOptions">No Results</li>');
                    return;
                }
                let valueResponse = $("#location-box-twitter").html();

                if (valueResponse.includes('</ul>')) {
                    valueResponse = valueResponse.replace('</ul>', '');
                } else {
                    valueResponse += "<ul>";
                }

                saveAutoComplete(data, 'TWITTER');

                data.map(function(obj) {

                    if (location_shown[data.name]) {

                        return;
                    }
                    let reference = obj.name;

                    valueResponse += '<li class="locationsOptions" data-id=' + obj.uuid + ' data-name=\'' + reference + '\' data="' + (JSON.stringify(obj)).replace(/\"/g, "'") + '">' + reference + '</li>';
                });
                valueResponse += "</ul>";
                $("#location-box-twitter").html(valueResponse);
                $("#location_twitter").css("background", "#FFF");
                location_cache_twitter[location_name] = valueResponse;
            },
            error: function() {
                $("#location_twitter").css("background", "#FFF");
            }
        });
    }));

    $(document).on("click", "#location-list-twitter i", function() { $(this).parent().remove(); });
    $(document).on("click", "#location-box-twitter ul li", function() {
        $('#location-box-twitter ul').empty();
        $("#location-list-twitter").append("<li data-id='" + $(this).attr('data-id') + "' data=\"" + $(this).attr('data') + "\" data-name='" + $(this).attr('data-name') + "' class='" + $("#location-action-twitter").val() + "'  data-location-option='" + $('#location_option_twitter').val() + "' >" + $(this).attr('data-name') + '<svg id="_27_Icon_close" data-name="27) Icon/close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M7.414,6l4.293-4.293A1,1,0,1,0,10.293.293L6,4.586,1.707.293A1,1,0,0,0,.293,1.707L4.586,6,.293,10.293a1,1,0,1,0,1.414,1.414L6,7.414l4.293,4.293a1,1,0,0,0,1.414-1.414Z" transform="translate(6 6)" fill="#222b45"/> </svg></li>');
        $('#location_twitter').val('');
    });
    // $('#location-action-twitter').select2({ placeholder: "Select an option", minimumResultsForSearch: Infinity });


    // mostramos el loader antes de los 3 segundos para transparencia al usuario.
    $("#location_linkedin").keyup(function() { $("#location_linkedin").css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px"); });
    $("#location_twitter").keyup(function() { $("#location_twitter").css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px"); });
    $("#location_facebook").keyup(function() { $("#location_facebook").css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px"); });
    $("#location_instagram").keyup(function() { $("#location_instagram").css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px"); });
    $("#language").keyup(function() { $("#language").css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px"); });

    $(".intereses").keyup(function() {
        var element = $(this);
        element.css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");
    });

    $(".audiencia").keyup(function() {
        var element = $(this);
        element.css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");
    });

    /** start languages  ------------------------------------------------------------------------------------------------------------------------------ */

    var getAjaxLanguage;

    $("#language").keyup(delay(3000, function() {
        $("#language").css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");

        let queri_language = $(this).val();

        if (queri_language == '') {
            $("#language-box").empty();
            return;
        }

        if (queri_language.length < 3) {
            $(".intereses-box." + rrss).empty();
            return;
        }

        getAjaxLanguage && getAjaxLanguage.readyState !== 4 && getAjaxLanguage.abort();

        data = { "query": queri_language };

        getAjaxLanguage = jQuery.ajax({
            url: 'api/getLanguage',
            type: 'POST',
            data: data,
            accept: "application/json",

            beforeSend: function() {
                $("#language").css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");
            },
            success: function(data) {

                $("#language-box").empty();
                if (!data) {
                    $("#language-box").html('<li class="languageOptions">No Results</li>');
                    return;
                }

                var valueResponse = "<ul>";
                let i = 0;

                data.map(function(obj) {
                    i++;
                    if (i < 5) {
                        let reference = obj.name;
                        valueResponse += '<li class="languageOptions" data-id=' + obj.uuid + ' data-name="' + reference + '" data="' + (JSON.stringify(obj)).replace(/\"/g, "'") + '">' + reference + '</li>';
                    }
                });
                valueResponse += "</ul>";
                $("#language-box").show();
                $("#language-box").html(valueResponse);
                $("#language").css("background", "#FFF");
            }
        });
    }));

    $(document).on("click", "#language-list i", function() { $(this).parent().remove(); });
    $(document).on("click", "#language-box ul li", function() {
        $('#language-box ul').empty();
        //$("#language-list").append("<li data-id='"+$(this).attr('data-id')+"' data-name='"+$(this).attr('data-name')+"' data=\""+$(this).attr('data')+"\" class='IN' >"+$(this).attr('data-name')+'<svg id="_27_Icon_close" data-name="27) Icon/close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M7.414,6l4.293-4.293A1,1,0,1,0,10.293.293L6,4.586,1.707.293A1,1,0,0,0,.293,1.707L4.586,6,.293,10.293a1,1,0,1,0,1.414,1.414L6,7.414l4.293,4.293a1,1,0,0,0,1.414-1.414Z" transform="translate(6 6)" fill="#222b45"/> </svg></li>');
        $("#language-list").append("<li data-id='" + $(this).attr('data-id') + "' data-name='" + $(this).attr('data-name') + "' class='IN' >" + $(this).attr('data-name') + '<svg id="_27_Icon_close" data-name="27) Icon/close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M7.414,6l4.293-4.293A1,1,0,1,0,10.293.293L6,4.586,1.707.293A1,1,0,0,0,.293,1.707L4.586,6,.293,10.293a1,1,0,1,0,1.414,1.414L6,7.414l4.293,4.293a1,1,0,0,0,1.414-1.414Z" transform="translate(6 6)" fill="#222b45"/> </svg></li>');
        $('#language').val('');
    });

    /** end languages  ------------------------------------------------------------------------------------------------------------------------------ */

    /* autocomplete para interes (faceboook)*/
    $(".intereses").keyup(delay(3000, function() {

        var rrss = $(this).attr('rrss');
        var element = $(this);
        let q = $(this).val();

        if (q == '') {
            $(".intereses-box." + rrss).empty();
            return;
        }

        if (q.length < 3) {
            $(".intereses-box." + rrss).empty();
            return;
        }

        element.css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");

        var data = [];

        switch (rrss) {
            case "facebook":
                $.when(
                        interest(q),
                        behaviors(q),
                        demographics(q),
                        life_events(q)
                    )
                    .done(function(interestResults, behaviorsResults, demographicsResults, life_eventsResults) {

                        if (behaviorsResults[0].data != undefined) { data = $.merge(data, behaviorsResults[0].data); }
                        if (life_eventsResults[0].data != undefined) { data = $.merge(data, life_eventsResults[0].data); }
                        if (demographicsResults[0].data != undefined) { data = $.merge(data, demographicsResults[0].data); }

                        //** filtramos los datos, intereses lo mergeamos debajo por que viene filtrado */
                        data = data.filter(function(element) {
                            //console.log(element.name.toLowerCase() + ' - ' + q.toLowerCase() + ' -- > ' + element.name.toLowerCase().indexOf(q.toLowerCase()))
                            if (element.name.toLowerCase().indexOf(q.toLowerCase()) >= 0) {
                                return element;
                            }
                        });

                        if (interestResults[0].data != undefined) { data = $.merge(data, interestResults[0].data); }

                        if (!data) {
                            $(".intereses-box." + rrss).html('<ul><li class="interesOptions">No Results</li></ul>');
                            return;
                        }


                        saveAutoComplete(data, rrss);

                        data = data.sort(SortByAudienceSize);
                        var valueResponse = "<ul>";
                        let i = 0;

                        data.map(function(obj) {
                            let reference = obj.name;
                            var type = ""
                            if (obj.type != undefined) {
                                type = obj.type;
                            } else if (obj.topic != undefined) {
                                type = obj.topic;
                            }
                            valueResponse += '<li class="interesOptions" rrss=' + rrss + ' data-id=' + obj.id + ' data-name="' + reference + '" data-type="' + type + '"  data="' + (JSON.stringify(obj)).replace(/\"/g, "'") + '" >' + reference + '<span>' + type + ' (' + addCommas(obj.audience_size) + ')' + '</span></li>';
                        });

                        valueResponse += "</ul>";
                        $(".intereses-box." + rrss).show();
                        $(".intereses-box." + rrss).html(valueResponse);


                        element.css("background", "#FFF");
                    });
                break;

            case "instagram":
                $.when(interest(q),
                        behaviors(q),
                        demographics(q),
                        life_events(q))
                    .done(function(interestResults, behaviorsResults, demographicsResults, life_eventsResults) {

                        if (interestResults[0].data != undefined) { data = $.merge(data, interestResults[0].data); }
                        if (behaviorsResults[0].data != undefined) { data = $.merge(data, behaviorsResults[0].data); }
                        if (life_eventsResults[0].data != undefined) { data = $.merge(data, life_eventsResults[0].data); }
                        if (demographicsResults[0].data != undefined) { data = $.merge(data, demographicsResults[0].data); }

                        //** filtramos los datos */


                        data = data.filter(function(element) {
                            console.log(element.name.toLowerCase() + ' - ' + q.toLowerCase() + ' -- > ' + element.name.toLowerCase().indexOf(q.toLowerCase()))
                            if (element.name.toLowerCase().indexOf(q.toLowerCase()) >= 0) {
                                return element;
                            }
                        });

                        if (!data) {
                            $(".intereses-box." + rrss).html('<ul><li class="interesOptions">No Results</li></ul>');
                            return;
                        }

                        saveAutoComplete(data, rrss);
                        data = data.sort(SortByAudienceSize);
                        var valueResponse = "<ul>";
                        let i = 0;

                        data.map(function(obj) {
                            let reference = obj.name;
                            var type = ""
                            if (obj.type != undefined) {
                                type = obj.type;
                            } else if (obj.topic != undefined) {
                                type = obj.topic;
                            }
                            valueResponse += '<li class="interesOptions" rrss=' + rrss + ' data-id=' + obj.id + ' data-name="' + reference + '" data-type="' + type + '"  data="' + (JSON.stringify(obj)).replace(/\"/g, "'") + '" >' + reference + '<span>' + type + ' (' + addCommas(obj.audience_size) + ')' + '</span></li>';
                        });

                        valueResponse += "</ul>";
                        $(".intereses-box." + rrss).show();
                        $(".intereses-box." + rrss).html(valueResponse);
                        element.css("background", "#FFF");
                    });
                break;

            case "twitter":
                $.when(interest_twitter(q)).done(function(interestResults) {

                    interestResults = JSON.parse(interestResults);
                    if (interestResults.data != undefined) {
                        data = $.merge(data, interestResults.data);
                        if (data.length > 0) {
                            saveAutoComplete(data, rrss);
                        }
                    }


                    if (interestResults.data.length == 0) {
                        $(".intereses-box." + rrss).show();
                        element.css("background", "#FFF");
                        $(".intereses-box." + rrss).html('<ul><li class="interesOptions">No Results</li></ul>');
                        return;
                    }

                    data = data.sort(SortByName);
                    var valueResponse = "<ul>";
                    let i = 0;
                    data.map(function(obj) {
                        let reference = obj.name;
                        valueResponse += '<li class="interesOptions" rrss=' + rrss + ' data-id=' + obj.targeting_value + ' data-name="' + reference + '" data-type="' + obj.targeting_type + '"  data="' + (JSON.stringify(obj)).replace(/\"/g, "'") + '" >' + reference + '</li>';
                    });
                    valueResponse += "</ul>";
                    $(".intereses-box." + rrss).show();
                    $(".intereses-box." + rrss).html(valueResponse);
                    element.css("background", "#FFF");
                });
                break;

            case "linkedin":
                $.when(interest_linkedin(q)).done(function(interestResults) {
                    interestResults = JSON.parse(interestResults);
                    if (interestResults.data != undefined) {
                        data = $.merge(data, interestResults.data);
                        if (data.length > 0) {
                            saveAutoComplete(data, rrss);
                        }
                    }


                    if (interestResults.data.length == 0) {
                        $(".intereses-box." + rrss).show();
                        element.css("background", "#FFF");
                        $(".intereses-box." + rrss).html('<ul><li class="interesOptions">No Results</li></ul>');
                        return;
                    }
                    data = data.sort(SortByName);
                    var valueResponse = "<ul>";
                    let i = 0;
                    data.map(function(obj) {
                        let reference = obj.name;
                        valueResponse += '<li class="interesOptions" rrss=' + rrss + ' data-id=' + obj.urn + ' data-name="' + reference + '" data-type="' + obj.facetUrn + '"  data="' + (JSON.stringify(obj)).replace(/\"/g, "'") + '" >' + reference + '<span>' + obj.facetUrn + '</span></li>';
                    });
                    valueResponse += "</ul>";
                    $(".intereses-box." + rrss).show();
                    $(".intereses-box." + rrss).html(valueResponse);
                    element.css("background", "#FFF");
                });
                break;
        }

    }));

    function SortByName(a, b) {
        var aName = a.name.toLowerCase();
        var bName = b.name.toLowerCase();
        return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    }

    function SortByAudienceSize(a, b) {
        var a_audience_size = a.audience_size;
        var b_audience_size = b.audience_size;
        return ((a_audience_size > b_audience_size) ? -1 : ((a_audience_size < b_audience_size) ? 1 : 0));
    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    function interest_linkedin(q) {

        data = { "platform": "LINKEDIN", "query": q };

        currentRequest = jQuery.ajax({
            url: 'api/getInterest',
            type: 'POST',
            data: data,
            accept: "application/json",
            beforeSend: function() {
                if (currentRequest != null) {
                    currentRequest.abort();
                }
            },
            success: function(data) {
                // Success
            },
            error: function(e) {
                // Error
            }
        });

        return currentRequest;
    }

    function interest_twitter(q) {

        data = { "platform": "TWITTER", "query": q }

        currentRequest = jQuery.ajax({
            url: 'api/getInterest',
            type: 'POST',
            data: data,
            accept: "application/json",
            beforeSend: function() {
                if (currentRequest != null) {
                    currentRequest.abort();
                }
            },
            success: function(data) {
                // Success
            },
            error: function(e) {
                // Error
            }
        });

        return currentRequest;

    }

    var currentRequest = null;

    function interest(q) {
        return $.ajax({
            type: "GET",
            url: 'https://graph.facebook.com/v10.0/search/?type=adinterest&limit=100&q=' + q + '&access_token=' + acces_token,
            data: '',
            success: function(result) {
                //console.log(result.data);
            }
        });
    }

    function behaviors(q) {
        return $.ajax({
            type: "GET",
            url: 'https://graph.facebook.com/v10.0/search/?type=adTargetingCategory&class=behaviors&limit=100&q=' + q + '&access_token=' + acces_token,
            data: '',
            success: function(result) {
                //console.log(result.data);
            }
        });
    }

    function demographics(q) {
        return $.ajax({
            type: "GET",
            url: 'https://graph.facebook.com/v10.0/search/?type=adTargetingCategory&class=demographics&limit=100&q=' + q + '&access_token=' + acces_token,
            data: '',
            success: function(result) {
                //console.log(result.data);
            }
        });
    }

    function life_events(q) {
        return $.ajax({
            type: "GET",
            url: 'https://graph.facebook.com/v10.0/search/?type=adTargetingCategory&class=life_events&limit=100&q=' + q + '&access_token=' + acces_token,
            data: '',
            success: function(result) {
                //console.log(result.data);
            }
        });
    }

    function saveAutoComplete(data, rrss, type = "") {

        data = { "data": data, "rrss": rrss, "type": type }

        $.ajax({
            url: '/api/save_Autocomplete',
            type: 'POST',
            data: data,
            accept: "application/json",
        }).done(function(data) {
            //console.log(data);
        });
    }


    $(document).on("click", ".intereses-list i", function() { $(this).parent().remove(); });
    $(document).on("click", ".intereses-box ul li", function() {
        var rrss = $(this).attr('rrss');
        $(this).parent().empty();
        $(".intereses-list." + rrss).append("<li data-id='" + $(this).attr('data-id') + "' data-name='" + $(this).attr('data-name') + "' data=\"" + $(this).attr('data') + "\" class='" + $(".intereses-action." + rrss).val() + "' >" + $(this).attr('data-name') + ' (' + $(this).attr('data-type') + ")<i class='flaticon-circle'></i></li>");

        if (rrss == 'facebook') {
            $(".intereses-list.instagram").append("<li data-id='" + $(this).attr('data-id') + "' data-name='" + $(this).attr('data-name') + "' data=\"" + $(this).attr('data') + "\" class='" + $(".intereses-action." + rrss).val() + "' >" + $(this).attr('data-name') + ' (' + $(this).attr('data-type') + ")<i class='flaticon-circle'></i></li>");
        }
        if (rrss == 'instagram') {
            $(".intereses-list.facebook").append("<li data-id='" + $(this).attr('data-id') + "' data-name='" + $(this).attr('data-name') + "' data=\"" + $(this).attr('data') + "\" class='" + $(".intereses-action." + rrss).val() + "' >" + $(this).attr('data-name') + ' (' + $(this).attr('data-type') + ")<i class='flaticon-circle'></i></li>");
        }

        $('.intereses.' + rrss).val('');
    });

    /* fin autocomplete para interes (faceboook)*/

    /* autocomplete para interes */

    $(".audiencia").keyup(delay(3000, function() {
        var rrss = $(this).attr('rrss');
        var adaccount = $('#adsaccount-' + rrss + ' option:selected').attr('account_id');
        var search = $(this).val();
        var element = $(this);

        if (search == '') {
            $(".audiencia-box." + rrss).empty();
            return;
        }

        switch (rrss) {
            /*case "facebook":
            	$.ajax({
            		type: "GET",
            		url: "https://graph.facebook.com/v10.0/"+ adaccount +"/customaudiences?fields=id,name,approximate_count,description,subtype,data_source&access_token=" + acces_token,
            		data:'',
            		beforeSend: function(){
            			element.css("background","#FFF url(/img/loaderIcon.gif) no-repeat 165px");
            		},
            		success: function(result){
            			let data = result.data;

            			$(".audiencia-box."+rrss).empty();
            			if(!data){
            				$(".audiencia-box."+rrss).html('<li class="languageOptions">No Results</li>');
            				element.css("background","#FFF");
            				return;
            			}
            			saveAutoComplete(data, rrss);
            			data = data.sort(SortByName);
            			var array = [];
            			$.each( data, function( index, value ) {
            				var value = value.name.toLowerCase();
            				if (value.indexOf(search.toLowerCase()) >= 0){
            					array.push(data[ index ]);
            				}
            			});

            			data = array;

            			var valueResponse = "<ul>";
            			let i = 0;
            			data.map(function( obj ){
            				let reference = obj.name;
            				valueResponse += '<li class="audienciaOptions" rrss='+rrss+' data-id='+obj.key+' data-name="'+reference+'" data="'+(JSON.stringify(obj)).replace(/\"/g, "'")+'" >'+reference+'<span>'+obj.approximate_count+'</span></li>';
            			});
            			valueResponse += "</ul>";
            			$(".audiencia-box."+rrss).show();
            			$(".audiencia-box."+rrss).html(valueResponse);
            			element.css("background","#FFF");
            		}
            	});
            	break;*/

            default:

                element.css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");

            data = { "adAccount": adaccount, "customAudiences": search }

            $.ajax({
                url: '/api/getCustomAudiences',
                type: 'POST',
                data: data,
                accept: "application/json",
                beforeSend: function() {
                    element.css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");
                },
                success: function(result) {

                    let data = JSON.parse(result)
                    data = data.data;

                    //console.log(result);
                    //console.log(data);

                    $(".audiencia-box." + rrss).empty();

                    if (!data) {
                        $(".audiencia-box." + rrss).html('<li class="audienciaOptions">No Results</li>');
                        element.css("background", "#FFF");
                        return;
                    }

                    saveAutoComplete(data, rrss);

                    var valueResponse = "<ul>";
                    let i = 0;

                    data.map(function(obj) {
                        let reference = obj.name;
                        console.log(obj);
                        valueResponse += '<li class="audienciaOptions" rrss=' + rrss + ' data-id=' + obj.public_id + ' data-name="' + reference + '" data="' + (JSON.stringify(obj)).replace(/\"/g, "'") + '" >' + reference + '<span>(' + obj.category + ')</span></li>';
                    });

                    valueResponse += "</ul>";

                    $(".audiencia-box." + rrss).show();
                    $(".audiencia-box." + rrss).html(valueResponse);

                    element.css("background", "#FFF");

                }
            });
            break;
        }

    }));

    $(document).on("click", ".audiencia-list i", function() { $(this).parent().remove(); });
    $(document).on("click", ".audiencia-box ul li", function() {
        var rrss = $(this).attr('rrss');
        $(this).parent().empty();
        $(".audiencia-list." + rrss).append("<li data-id='" + $(this).attr('data-id') + "' data-name='" + $(this).attr('data-name') + "' data=\"" + $(this).attr('data') + "\" class='" + $(".audiencia-action." + rrss).val() + "' >" + $(this).attr('data-name') + '<svg id="_27_Icon_close" data-name="27) Icon/close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M7.414,6l4.293-4.293A1,1,0,1,0,10.293.293L6,4.586,1.707.293A1,1,0,0,0,.293,1.707L4.586,6,.293,10.293a1,1,0,1,0,1.414,1.414L6,7.414l4.293,4.293a1,1,0,0,0,1.414-1.414Z" transform="translate(6 6)" fill="#222b45"/> </svg></li>');
        $(".audiencia." + rrss).val('');


        if (rrss == 'facebook') {
            $(".audiencia-list.instagram").append("<li data-id='" + $(this).attr('data-id') + "' data-name='" + $(this).attr('data-name') + "' data=\"" + $(this).attr('data') + "\" class='" + $(".intereses-action." + rrss).val() + "' >" + $(this).attr('data-name') + ' (' + $(this).attr('data-type') + ")<i class='flaticon-circle'></i></li>");
        }
        if (rrss == 'instagram') {
            $(".audiencia-list.facebook").append("<li data-id='" + $(this).attr('data-id') + "' data-name='" + $(this).attr('data-name') + "' data=\"" + $(this).attr('data') + "\" class='" + $(".intereses-action." + rrss).val() + "' >" + $(this).attr('data-name') + ' (' + $(this).attr('data-type') + ")<i class='flaticon-circle'></i></li>");
        }


    });

    /* fin autocomplete para interes */

    /* post para interes */
    $(".posts").keyup(delay(500, function() {
        var rrss = $(this).attr('rrss');
        var adaccount = $('#adsaccount-' + rrss).val();
        var element = $(this);
        $.ajax({
            type: "GET",
            url: "/api/getPromotePages/" + adaccount + "/" + $(this).val(),
            data: '',
            beforeSend: function() {
                element.css("background", "#FFF url(/img/loaderIcon.gif) no-repeat 165px");
            },
            success: function(data) {
                var valueResponse = "<ul>";
                let i = 0;
                data.map(function(obj) {
                    let reference = obj.name;
                    valueResponse += '<li class="postsOptions" rrss=' + rrss + ' data-id=' + obj.key + ' data-name="' + reference + '" data="' + (JSON.stringify(obj)).replace(/\"/g, "'") + '" >' + reference + '<span>' + obj.approximate_count + '</span></li>';
                });
                valueResponse += "</ul>";
                $(".posts-box." + rrss).show();
                $(".posts-box." + rrss).html(valueResponse);
                element.css("background", "#FFF");
            }
        });
    }));

    $(document).on("click", ".posts-list i", function() { $(this).parent().remove(); });
    $(document).on("click", ".posts-box ul li", function() {
        var rrss = $(this).attr('rrss');
        $(this).parent().empty();
        $(".posts-list." + rrss).append("<li data-id='" + $(this).attr('data-id') + "' data-name='" + $(this).attr('data-name') + "' data=\"" + $(this).attr('data') + "\" class='IN' >" + $(this).attr('data-name') + '<svg id="_27_Icon_close" data-name="27) Icon/close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M7.414,6l4.293-4.293A1,1,0,1,0,10.293.293L6,4.586,1.707.293A1,1,0,0,0,.293,1.707L4.586,6,.293,10.293a1,1,0,1,0,1.414,1.414L6,7.414l4.293,4.293a1,1,0,0,0,1.414-1.414Z" transform="translate(6 6)" fill="#222b45"/> </svg></li>');
        $(".posts." + rrss).val('');
    });




    $('#btnSubscription').on("click", function() {
        alert('mostramos');
        window.location = '/mysubscription'
    });


    /* fin autocomplete para post */


    //To select country name
    /*
    function selectCountry(val) {
    	$("#location").val(val);
    	$("#location-box").hide();
    }
    */

    /*
    $.ajax({
    	type: "GET",
    	url: "/api/getAdAccount/",
    	data:'',
    	success: function(data){
    		var valueResponse = "<ul>";
    		let i = 0;
    					$.each(data, function( k, v ) {
    						valueResponse += "<option value=\""+v.id+"\">"+v.name+"</option>";
    						if(i==0){
    								getPage(v.id);
    						}
    					});
    		valueResponse += "</ul>";

    					$("#select_adaccount").html(valueResponse);
    	}
    });*/


    function getPage(AdAccount) {
        $.ajax({
            type: "GET",
            url: "/api/getPromotePages/" + AdAccount,
            data: '',
            success: function(data) {
                var valueResponse = "<option >Please select a Page</option>";
                let i = 0;
                $.each(data, function(k, v) {
                    valueResponse += "<option value=\"" + v.id + "\" style=\"background-image:url(" + v.picture.data.url + ");\">" + v.name + "</option>"
                });
                $("#select_page").html(valueResponse);
            }
        });
    }

});

(function($) {
    'use strict';

    var DayScheduleSelector = function(el, options) {
        this.$el = $(el);
        this.options = $.extend({}, DayScheduleSelector.DEFAULTS, options);
        this.render();
        this.attachEvents();
        this.$selectingStart = null;
    }

    DayScheduleSelector.DEFAULTS = {
        days: [0, 1, 2, 3, 4, 5, 6], // Sun - Sat
        startTime: '08:00', // HH:mm format
        endTime: '20:00', // HH:mm format
        interval: 30, // minutes
        stringDays: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        template: '<div class="day-schedule-selector">' +
            '<table class="schedule-table" style="font-size: 12px;">' +
            '<thead class="schedule-header"></thead>' +
            '<tbody class="schedule-rows"></tbody>' +
            '</table>' +
            '<div>'
    };

    /**
     * Render the calendar UI
     * @public
     */
    DayScheduleSelector.prototype.render = function() {
        this.$el.html(this.options.template);
        this.renderHeader();
        this.renderRows();
    };

    /**
     * Render the calendar header
     * @public
     */
    DayScheduleSelector.prototype.renderHeader = function() {
        var start = this.options.startTime,
            end = this.options.endTime,
            interval = this.options.interval,
            stringDays = this.options.stringDays,
            days = this.options.days,
            html = '';

        //$.each(days, function (i, _) { html += '<th>' + (stringDays[i] || '') + '</th>'; });
        $.each(generateDates(start, end, interval), function(i, d) { html += '<td data-time="' + hhmm(d) + '">' + hhmm(d) + '</td>'; });
        this.$el.find('.schedule-header').html('<tr style="text-align: center;"><td></td>' + html + '</tr>');
    };

    /**
     * Render the calendar rows, including the time slots and labels
     * @public
     */
    DayScheduleSelector.prototype.renderRows = function() {
        var start = this.options.startTime,
            end = this.options.endTime,
            interval = this.options.interval,
            stringDays = this.options.stringDays,
            days = this.options.days,
            $el = this.$el.find('.schedule-rows');

        $.each(days, function(i, _) {
            var daysInARow = "";
            var days_id = i;
            $.each(generateDates(start, end, interval), function(i, d) {
                daysInARow += '<td class="time-slot" data-time="' + hhmm(d) + '" data-day="' + days_id + '"></td>';
            });

            $el.append('<tr><th class="time-label"><label class="checkbox kt-checkbox--bold kt-checkbox--brand cbbadget"><input type="checkbox" id="' + i + '" onClick="javascript:checkAllRow(' + i + ');"><span></span>' + (stringDays[i] || '') + '&ensp;</label></th>' + daysInARow + '</tr>');
        });
    };


    /**
     * Is the day schedule selector in selecting mode?
     * @public
     */
    DayScheduleSelector.prototype.isSelecting = function() {
        return !!this.$selectingStart;
    }

    DayScheduleSelector.prototype.select = function($slot) { $slot.attr('data-selected', 'selected'); }
    DayScheduleSelector.prototype.deselect = function($slot) { $slot.removeAttr('data-selected'); }

    function isSlotSelected($slot) { return $slot.is('[data-selected]'); }

    function isSlotSelecting($slot) { return $slot.is('[data-selecting]'); }

    /**
     * Get the selected time slots given a starting and a ending slot
     * @private
     * @returns {Array} An array of selected time slots
     */
    function getSelection(plugin, $a, $b) {
        var $slots, small, large, temp;
        if (!$a.hasClass('time-slot') || !$b.hasClass('time-slot') ||
            ($a.data('day') != $b.data('day'))) { return []; }
        $slots = plugin.$el.find('.time-slot[data-day="' + $a.data('day') + '"]');
        small = $slots.index($a);
        large = $slots.index($b);
        if (small > large) {
            temp = small;
            small = large;
            large = temp;
        }
        return $slots.slice(small, large + 1);
    }

    DayScheduleSelector.prototype.attachEvents = function() {
        var plugin = this,
            options = this.options,
            $slots;

        this.$el.on('click', '.time-slot', function() {
            var day = $(this).data('day');
            /*if (!plugin.isSelecting()) {  // if we are not in selecting mode
              if (isSlotSelected($(this))) { plugin.deselect($(this)); }
              else {  // then start selecting
            	plugin.$selectingStart = $(this);
            	$(this).attr('data-selecting', 'selecting');
            	plugin.$el.find('.time-slot').attr('data-disabled', 'disabled');
            	plugin.$el.find('.time-slot[data-day="' + day + '"]').removeAttr('data-disabled');
              }
            } else {  // if we are in selecting mode
              if (day == plugin.$selectingStart.data('day')) {  // if clicking on the same day column
            	// then end of selection
            	plugin.$el.find('.time-slot[data-day="' + day + '"]').filter('[data-selecting]')
            	  .attr('data-selected', 'selected').removeAttr('data-selecting');
            	plugin.$el.find('.time-slot').removeAttr('data-disabled');
            	plugin.$el.trigger('selected.artsy.dayScheduleSelector', [getSelection(plugin, plugin.$selectingStart, $(this))]);
            	plugin.$selectingStart = null;
              }
            }*/
            var attr = $(this).attr('data-selected');

            if (typeof attr !== typeof undefined && attr !== false) {
                $(this).removeAttr('data-selected');
            } else {
                $(this).attr('data-selected', 'selected');
            }

        });


    };

    /**
     * Serialize the selections
     * @public
     * @returns {Object} An object containing the selections of each day, e.g.
     *    {
     *      0: [],
     *      1: [["15:00", "16:30"]],
     *      2: [],
     *      3: [],
     *      5: [["09:00", "12:30"], ["15:00", "16:30"]],
     *      6: []
     *    }
     */
    DayScheduleSelector.prototype.serialize = function() {
        var plugin = this,
            selections = {};

        $.each(this.options.days, function(_, v) {
            var start, end;
            start = end = false;
            selections[v] = [];
            plugin.$el.find(".time-slot[data-day='" + v + "']").each(function() {
                // Start of selection
                if (isSlotSelected($(this)) && !start) {
                    start = $(this).data('time');
                }

                // End of selection (I am not selected, so select until my previous one.)
                if (!isSlotSelected($(this)) && !!start) {
                    end = $(this).data('time');
                }

                // End of selection (I am the last one :) .)
                if (isSlotSelected($(this)) && !!start && $(this).is(".time-slot[data-day='" + v + "']:last")) {
                    end = secondsSinceMidnightToHhmm(
                        hhmmToSecondsSinceMidnight($(this).data('time')) + plugin.options.interval * 60);
                }

                if (!!end) {
                    selections[v].push([start, end]);
                    start = end = false;
                }
            });
        })
        return selections;
    };

    /**
     * Deserialize the schedule and render on the UI
     * @public
     * @param {Object} schedule An object containing the schedule of each day, e.g.
     *    {
     *      0: [],
     *      1: [["15:00", "16:30"]],
     *      2: [],
     *      3: [],
     *      5: [["09:00", "12:30"], ["15:00", "16:30"]],
     *      6: []
     *    }
     */
    DayScheduleSelector.prototype.deserialize = function(schedule) {
        var plugin = this,
            i;
        $.each(schedule, function(d, ds) {
            var $slots = plugin.$el.find('.time-slot[data-day="' + d + '"]');
            $.each(ds, function(_, s) {
                for (i = 0; i < $slots.length; i++) {
                    if ($slots.eq(i).data('time') >= s[1]) { break; }
                    if ($slots.eq(i).data('time') >= s[0]) { plugin.select($slots.eq(i)); }
                }
            })
        });
    };

    // DayScheduleSelector Plugin Definition
    // =====================================

    function Plugin(option) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('artsy.dayScheduleSelector'),
                options = typeof option == 'object' && option;

            if (!data) {
                $this.data('artsy.dayScheduleSelector', (data = new DayScheduleSelector(this, options)));
            }
        })
    }

    $.fn.dayScheduleSelector = Plugin;

    /**
     * Generate Date objects for each time slot in a day
     * @private
     * @param {String} start Start time in HH:mm format, e.g. "08:00"
     * @param {String} end End time in HH:mm format, e.g. "21:00"
     * @param {Number} interval Interval of each time slot in minutes, e.g. 30 (minutes)
     * @returns {Array} An array of Date objects representing the start time of the time slots
     */
    function generateDates(start, end, interval) {
        var numOfRows = Math.ceil(timeDiff(start, end) / interval);
        return $.map(new Array(numOfRows), function(_, i) {
            // need a dummy date to utilize the Date object
            return new Date(new Date(2000, 0, 1, start.split(':')[0], start.split(':')[1]).getTime() + i * interval * 60000);
        });
    }

    /**
     * Return time difference in minutes
     * @private
     */
    function timeDiff(start, end) { // time in HH:mm format
        // need a dummy date to utilize the Date object
        return (new Date(2000, 0, 1, end.split(':')[0], end.split(':')[1]).getTime() -
            new Date(2000, 0, 1, start.split(':')[0], start.split(':')[1]).getTime()) / 60000;
    }

    /**
     * Convert a Date object to time in H:mm format with am/pm
     * @private
     * @returns {String} Time in H:mm format with am/pm, e.g. '9:30am'
     */
    function hmmAmPm(date) {
        var hours = date.getHours(),
            minutes = date.getMinutes(),
            ampm = hours >= 12 ? 'pm' : 'am';
        return hours + ':' + ('0' + minutes).slice(-2) + ampm;
    }

    /**
     * Convert a Date object to time in HH:mm format
     * @private
     * @returns {String} Time in HH:mm format, e.g. '09:30'
     */
    function hhmm(date) {
        var hours = date.getHours(),
            minutes = date.getMinutes();
        return ('0' + hours).slice(-2) + ':' + ('0' + minutes).slice(-2);
    }

    function hhmmToSecondsSinceMidnight(hhmm) {
        var h = hhmm.split(':')[0],
            m = hhmm.split(':')[1];
        return parseInt(h, 10) * 60 * 60 + parseInt(m, 10) * 60;
    }

    /**
     * Convert seconds since midnight to HH:mm string, and simply
     * ignore the seconds.
     */
    function secondsSinceMidnightToHhmm(seconds) {
        var minutes = Math.floor(seconds / 60);
        return ('0' + Math.floor(minutes / 60)).slice(-2) + ':' +
            ('0' + (minutes % 60)).slice(-2);
    }

    // Expose some utility functions
    window.DayScheduleSelector = {
        ssmToHhmm: secondsSinceMidnightToHhmm,
        hhmmToSsm: hhmmToSecondsSinceMidnight
    };

})(jQuery);