<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once '../crons/helper/config.php';
require_once  __DIR__ .'/../crons/helper/db.php';

$db = new db();

/*
$sql = "SELECT * FROM app_thesoci_9c37.organic_settings where id = 1";

$jobs = $db->getRecordSet($sql);
  echo $arg_json = json_encode($jobs);

die();*/

$arg = json_decode('{"id":"1","name":"vozpopuli economia","sourcetype":"RSS","Source_url":"https:\/\/www.vozpopuli.com\/rss\/economia-y-finanzas\/","readtypetime":"Interval","readintervalminutes":"60","itemstoread":"15","itemschoose":"Old First","postingtypetime":"Interval","postingintervalminutes":"30","public_id":"91aab5a6-1dcb-11eb-a49a-ac1f6b17ff4a","tagsdictionary":"[]","user_id":"6","filter":null,"destination_profiles":"[\"71bfc81b-ca8d-11ea-86d6-de2e501f7018\", \"7205354a-ca8d-11ea-86d6-de2e501f7018\", \"99b23474-ca85-11ea-86d6-de2e501f7018\"]"}');


$destination_profiles = str_replace('[', '(', $arg->destination_profiles);
$destination_profiles = str_replace(']', ')', $destination_profiles);

$sql = "SELECT id, name, platform FROM app_thesoci_9c37.properties_accounts where public_id in ".$destination_profiles;
$profiles = $db->getRecordSet($sql);

$xml = simplexml_load_file($arg->Source_url, 'SimpleXMLElement', LIBXML_NOCDATA);
$json = json_decode(json_encode($xml), true);

$sql = "SELECT max(posted_time) as max_time FROM app_thesoci_9c37.organic_post WHERE user_id = ".$arg->user_id;

$jobs = $db->getRecordSet($sql);


if(time() > strtotime($jobs[0]['max_time'])){
	$max_time = date("Y-m-d H:i:s");
}else{
	$max_time = $jobs[0]['max_time'];
}

$i=0;
$time = 0;
if(!empty($json)){
	$arr = array();
	switch($arg->itemschoose){
		case "Old First":
			$arr = $json["channel"]["item"];

			usort($arr, function($a, $b) {
				return strtotime($a['pubDate']) < strtotime($b['pubDate']);
			});
		break;
		case "Recent First":
			$arr = $json["channel"]["item"];

			usort($arr, function($a, $b) {
				return strtotime($a['pubDate']) > strtotime($b['pubDate']);
			});
		break;
		case "Random":
			$arr = shuffle($json["channel"]["item"]);
		break;
		case "Top to bottom":
		default:
			$arr = $json["channel"]["item"];
		break;
	}

	foreach ($arr as $item) {
		if($i < $arg->itemstoread){
			$time = $time + $arg->postingintervalminutes;
			insert_post($item, $time);
			$i++;
		}else{
			break;
		}
	}

 }else{
     echo "<h2>No item found</h2>";
 }

function insert_post($item, $time){
	GLOBAL $db, $arg, $max_time, $profiles;

	//echo date("Y-m-d H:i:s", strtotime($max_time." +".$time." minutes"))."<br>";
	$sql = "INSERT INTO `app_thesoci_9c37`.`organic_sources_posts`
						(`setting_id`, `organic_souce_post_md5`, `destination_url`, `thumbnail_url`, `post_name`, `post_description`, `user_id`, properties)
						VALUES (1, '".md5($item["enclosure"]["@attributes"]["url"])."', '".$db->real_escape_string($item["link"])."',
						'".$db->real_escape_string($item["enclosure"]["@attributes"]["url"])."', '".$db->real_escape_string($item["title"])."',
						'".$db->real_escape_string($item["description"])."', ".$arg->user_id.", '".json_encode($profiles)."')
						ON DUPLICATE KEY UPDATE thumbnail_url = '".$db->real_escape_string($item["enclosure"]["@attributes"]["url"])."';";

	$db->query($sql);

	$id_content = $db->insert_id();
	if($id_content != 0){
		foreach ($profiles as $value) {
			$sql = "INSERT INTO `app_thesoci_9c37`.`organic_post`
				(`posted_time`, `setting_id`, `source_content_id`, `content`, `user_id`, `destination_url`,
					`property_id`, `platform`)
			VALUES
				('".date("Y-m-d H:i:s", strtotime($max_time." +".$time." minutes"))."', 1, ".$id_content.",
				'".$db->real_escape_string($item["title"])."', $arg->user_id, '".$db->real_escape_string($item["link"])."',
				".$value["id"].", '".$value["platform"]."');";

			$db->query($sql);
		}
	}
}

echo "fin";
