<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require 'configs/general.php';
require 'configs/linkedin.php';
require 'apis_curls/LinkedInApi.php';



//print_r($client_id );
//session_start();

$code = $_GET['code'];
$error = $_GET['error'];
$error_description = $_GET['error_description'];

//$access_token = 'AQU4KJ_5cYwHWCG0VAJvP7xMWpGeFKXnEmZQ8bifIgfGb1XvC86aiyAkkv6NxxS8v5qFUp2521XY0dJHnRhJ7f9_fbdcbE8PcxSzSvfoReo1rUsBt8UJzvGJIJk3xjYoHdx2xuXHk7B8Yah1WG4sokooLYu_MEkGAPlJqLbGvCV013dfWeQTu8CjuLDUy02zxBcjDfm0n97wnqLopOrvKH7ffkuGOb0ap7Q2GtDjU9rnaibPciW0GsLI08btR7ilvJN_IjHiH7dPIpCKUzcNVLD61XDf4QcOSLZd0DiPDADTWfYP-w8XNZnKE-n4Hvb6j4TNlR0upT6Fsw6nU62Rvsky_rCvrw';
//{"access_token":"AQU4KJ_5cYwHWCG0VAJvP7xMWpGeFKXnEmZQ8bifIgfGb1XvC86aiyAkkv6NxxS8v5qFUp2521XY0dJHnRhJ7f9_fbdcbE8PcxSzSvfoReo1rUsBt8UJzvGJIJk3xjYoHdx2xuXHk7B8Yah1WG4sokooLYu_MEkGAPlJqLbGvCV013dfWeQTu8CjuLDUy02zxBcjDfm0n97wnqLopOrvKH7ffkuGOb0ap7Q2GtDjU9rnaibPciW0GsLI08btR7ilvJN_IjHiH7dPIpCKUzcNVLD61XDf4QcOSLZd0DiPDADTWfYP-w8XNZnKE-n4Hvb6j4TNlR0upT6Fsw6nU62Rvsky_rCvrw","expires_in":5183999}
if(isset($code)){
    //TODO

    $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.linkedin.com/oauth/v2/accessToken",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=authorization_code&code=".$code."&redirect_uri=".$linkedin_redirect_uri."&client_id=".$client_id."&client_secret=".$client_secret."&prueba=id",
            CURLOPT_HTTPHEADER => array('Content-Type: application/x-www-form-urlencoded'),
        ));

        $retorno = json_decode(curl_exec($curl));
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "Error:" . $err;
        } else {
            if(isset($retorno->error)){
                //   header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $retorno->error . "\n";
                echo "Error Description: " . $retorno->error_description . "\n";
            }else{
                $access_token = $retorno->access_token;
                $refresh_token = $retorno->refresh_token;
            }
        }



}else if (isset($error)){
    //   header('HTTP/1.0 401 Unauthorized');
    echo "Error: " . $error . "\n";
    echo "Error Description: " . $error_description . "\n";
}else{
    $url = 'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id='.$client_id.'&redirect_uri='. urlencode( $linkedin_redirect_uri).'&scope='.$scope;
    echo '<a href="'.htmlspecialchars($url).'">login</a>';
    header('Location: '.$url);
}



if(isset($access_token)){
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.linkedin.com/v2/me",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "authorization: Bearer ".$access_token,
            "cache-control: no-cache",
        ),
    ));

    $response = json_decode(curl_exec($curl));
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL".$err;
    } else {

        $email = json_decode(file_get_contents('https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))&oauth2_access_token='.$access_token), true);

        $email = $email["elements"][0]["handle~"]["emailAddress"];

        //echo $email->elements->elements->handle~->emailAddress;

        $userid = get_user_id();

        $stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`auths_user_platform`
        (`user_id`, `platform`, `app_id`, `app_secret`, `platform_user_id`, `platform_email`, `access_token`,`expiretime`, `retorno`, `refresh_token`)
        VALUES (?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE   activa = 'Y', `expiretime`= ?,  `access_token`= ?,`platform_user_id`=?,  `retorno`= ?, `platform_email` = ?, `refresh_token` = ? ");
        $stmt->bind_param("ssssssssssssssss",...[ $userid, $platformid, $client_id, $client_secret, $response->id, $email, $access_token,time()+(3600*59), json_encode($retorno), $refresh_token, time()+(3600*59),$access_token, $response->id, json_encode($retorno), $email, $refresh_token]);


        $stmt->execute();

        exec('php /home/app.thesocialaudience.com/public_html/www/crons/auth_account_update.php '.$stmt->insert_id);

        $stmt->close();
        //crear metodo en general que llame traiga todos los items de la cuenta
    }
    header('Location: https://app.thesocialaudience.com/connections/success');
}
