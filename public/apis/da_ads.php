<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require __DIR__ . '/../../vendor/autoload.php';
use Google\Auth\OAuth2;
require 'configs/general.php';
require 'configs/googleads.php';


session_start();


$oauth2 = new OAuth2([
    'authorizationUri' => 'https://accounts.google.com/o/oauth2/v2/auth',
    'tokenCredentialUri' => 'https://www.googleapis.com/oauth2/v4/token',
    'redirectUri' => $callbackUrl,
    'clientId' => $clientId,
    'clientSecret' => $clientSecret,
    'scope' => ['https://www.googleapis.com/auth/adwords', 'https://www.googleapis.com/auth/userinfo.email']
]);


if (!isset($_GET['code'])) {
  // Create a 'state' token to prevent request forgery.
  // Store it in the session for later validation.
  $oauth2->setState(sha1(openssl_random_pseudo_bytes(1024)));
  $_SESSION['oauth2state'] = $oauth2->getState();

  // Redirect the user to the authorization URL.
  $config = [
    // Set to 'offline' if you require offline access.
    'access_type' => 'offline',
    'approval_prompt' => 'force'
  ];

  //$oauth2->buildFullAuthorizationUri($config);

  header('Location: ' . $oauth2->buildFullAuthorizationUri($config));
  exit;
} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

  unset($_SESSION['oauth2state']);
  exit('Invalid state.');

} else {
  $oauth2->setCode($_GET['code']);
  $authToken = $oauth2->fetchAuthToken();

  // Store the refresh token for your user in your local storage if you
  // requested offline access.
  if(isset($authToken['refresh_token'])){
    $refresh_token = $authToken['refresh_token'];
  }

  $jwt = explode('.', $authToken['id_token']);

  // Extract the middle part, base64 decode, then json_decode it
  $userinfo = json_decode(base64_decode($jwt[1]), true);

  $access_token = $authToken['access_token'];
  $platform_user_id = $userinfo['sub'];
  $email = $userinfo['email'];

  $userid = get_user_id();

  if(isset($authToken['refresh_token'])){
    $stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`auths_user_platform`
    (`user_id`, `platform`, `app_id`, `app_secret`, `platform_user_id`, `platform_email`,
      `access_token`,`expiretime`, `retorno`, `refresh_token`)
    VALUES (?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE  activa = 'Y', `expiretime`= ?,  `access_token`= ?,`platform_user_id`=?,
    `retorno`= ?, `platform_email` = ?, `refresh_token` = ? ");
    $stmt->bind_param("ssssssssssssssss",...[ $userid, $platformid, $client_id, $client_secret, $platform_user_id, $email,
    $access_token,time()+(3600*59), json_encode($retorno), $refresh_token,
    time()+(3600*59),$access_token, $platform_user_id, json_encode($authToken), $email, $refresh_token]);
  }else{
    $stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`auths_user_platform`
    (`user_id`, `platform`, `app_id`, `app_secret`, `platform_user_id`, `platform_email`,
      `access_token`,`expiretime`, `retorno`)
    VALUES (?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE  activa = 'Y', `expiretime`= ?,  `access_token`= ?,`platform_user_id`=?,
    `retorno`= ?, `platform_email` = ?");
    $stmt->bind_param("ssssssssssssss",...[ $userid, $platformid, $client_id, $client_secret, $platform_user_id, $email,
    $access_token,time()+(3600*59), json_encode($retorno),
    time()+(3600*59),$access_token, $platform_user_id, json_encode($authToken), $email]);
  }

  $stmt->execute();

  exec('php /home/app.thesocialaudience.com/public_html/www/crons/auth_account_update.php '.$stmt->insert_id);

  $stmt->close();
  //crear metodo en general que llame traiga todos los items de la cuenta
}
header('Location: https://app.thesocialaudience.com/connections/success');
