<?php

if($_GET['key'] !='2904'){
   echo 'no access';
   return '';
}

//CODE ORGANIZED
use App\settings_matrix;

require_once('config.php');
require_once('db.php');
require_once('functions.php');
//require_once('twitter_func.php');
ini_set('display_errors', 1);
require_once('TwitterAPIExchange.php');

$db = new db();
//--------------------------------------START
$user_id = 6;
$auth = get_auth($user_id, 'TWITTER');

$twitter = new TwitterAPIExchange($auth['settings']); //connect to twitter API

$url = 'https://ads-api.twitter.com/7/targeting_criteria/locations';
$requestMethod = 'GET';
$getfields = '?location_type=COUNTRIES&lang=en&count=1000';
$requestMethod = 'GET';
$countries = $twitter->setGetfield($getfields)->buildOauth($url, $requestMethod)->performRequest();
$countries =  json_decode($countries,true);
$pass = true;
foreach($countries['data'] as $country){
    // if($pass){
    //     if($country['country_code'] == 'AT')
    //         $pass = false;
    //     continue;
    // }
   //var_dump($country);
   saveCountry($country);
   handleRegions($country);
   handleCities($country);
   sleep(1);
}

function saveCountry($country){
    global $db;
    $type = "GEO";
    $name_clean = $db->real_escape_string($country['localized_name']);
    echo 'Country:';
    var_dump($name_clean);
    $sql = "select * from settings_matrix where name ='{$name_clean}' and type = 'GEO' ";
    $country_db= $db->getRow($sql);
    $metadata = [];
    $update = false;
    if($country_db){
        $metadata = json_decode($country_db['metadata'], true);
        if($metadata['facebook']['type'] == 'country' || $metadata['facebook']['country_code'] == $country['country_code']){
            $update = true;
        }
    } 
    if($update){
        $metadata = json_decode($country_db['metadata'], true);
        $metadata['twitter'] = $country;
        $metadata_clear =  $db->real_escape_string(json_encode($metadata ));
        $sql = "UPDATE settings_matrix SET 
                  name_tw = '{$name_clean}' ,
                  valor_tw = '{$country['targeting_value']}',
                  metadata = '{$metadata_clear}' 
                where id = '{$country_db['id']}'";
        //var_dump($sql);
    } else{
        $metadata['twitter'] =$country;
        $metadata_clear =  $db->real_escape_string(json_encode($metadata ));
        $sql = "INSERT INTO settings_matrix SET 
                  name = '{$name_clean}' ,
                  nombre = '{$name_clean}',
                  type = '{$type}',
                  uuid = REPLACE(UUID(),'-',''),
                  name_tw = '{$name_clean}' ,
                  valor_tw = '{$country['targeting_value']}',
                  metadata = '{$metadata_clear}' ";
        //var_dump($sql);
    }
    $db->query($sql);
}

function handleRegions($country){
    global $twitter;
    $getfields = '?location_type=REGIONS&lang=en&country_code='.$country['country_code'].'&count=1000';
    $url = 'https://ads-api.twitter.com/7/targeting_criteria/locations';
    $cursor = '';
    $requestMethod = 'GET';
    $skip_code = ['AF', 'AX','AL','DZ','AS', 'AD','AO','AI','AG', 'AM', 'AW','AT', 'AZ',
                'BD','BB','BY', 'BE','BZ','BJ','BM','BT','BO','BQ','BA','BW','BV','IO','BS',
                'VG','BN', 'BG','BF','BI','CM','KH','CV','KY','CF','TD','CN','CX','CC','BH',
                'KM','KM','CR','CI','HR','CW','CY','CZ','CD','DK','DJ','DM','DO','EC','SV',
                'GQ','EE','ET','FK','FO','FJ','FI','GF','PF','TF','GA','GM','GE','GH','GI',
                'GR','GL','GD','MK','GU','GT','GG','GN','GY','GW','GY','HT','HK','HU','IS',
                'NI','NE','NG','NU','NF','MP','NO','OM','PK','PW','PS','PA','PG','PY','PE',
                'PN','PL','PT','CG','RE','RO','RU','RW','BL','SH','KN','LC','MF','PM','VC',
                'WS','SM','ST','SN','SC','SL','SX','SI','SK','SB','SO','ZA','GS','LK','SR',
                'SJ','SZ','SE', 'CH','TW','TJ','TZ','VA','VE','VN','WF','EH','YE','ZM','ZW'
    ];
    if(in_array($country['country_code'],$skip_code)) return;
    while(true){
        $region = $twitter->setGetfield($getfields . $cursor )->buildOauth($url, $requestMethod)->performRequest();
        $region =  json_decode($region,true);
        if(!isset($country['data'])) break;
        $log['type'] = 'region';
        $log['code'] = $country['country_code'];
        $log['number'] = count($country['data']);
        logVarDebugApend($log);
        foreach ($region['data'] as $region_data){
            saveRegion($region_data);
        }
        if(isset($region['next_cursor']) && $region['next_cursor'] != ''){
            $cursor = '&cursor=' .$region['next_cursor'];
        }
        else {
            break;
        }
    }
}

function saveRegion($region){
    global $db;
    $type = "GEO";
    $name_clean = $db->real_escape_string($region['localized_name']);
    echo 'Region:';
    var_dump($name_clean);
    $sql = "select * from settings_matrix where name ='{$name_clean}' and type = 'GEO' ";
    $country_db= $db->getRow($sql);
    $metadata = [];
    $update = false;
    if($country_db){
        $metadata = json_decode($country_db['metadata'], true);
        if(isset($metadata['facebook']['region']) && $metadata['facebook']['type'] == 'city' && $metadata['facebook']['country_code'] == $region['country_code']){
            $update = true;
        }
    }
    if($update){
        $metadata = json_decode($country_db['metadata'], true);
        $metadata['twitter'] = $region;
        $metadata_clear =  $db->real_escape_string(json_encode($metadata ));
        $sql = "UPDATE settings_matrix SET 
                  name_tw = '{$name_clean}' ,
                  valor_tw = '{$region['targeting_value']}',
                  metadata = '{$metadata_clear}' 
                where id = '{$country_db['id']}'";
        //var_dump($sql);
    } else{
        $metadata['twitter'] =$region;
        $metadata_clear =  $db->real_escape_string(json_encode($metadata ));
        $sql = "INSERT INTO settings_matrix SET 
                  name = '{$name_clean}' ,
                  nombre = '{$name_clean}',
                  type = '{$type}',
                  uuid = REPLACE(UUID(),'-',''),
                  name_tw = '{$name_clean}' ,
                  valor_tw = '{$region['targeting_value']}',
                  metadata = '{$metadata_clear}' ";
       // var_dump($sql);
    }
    $db->query($sql);
}
$pass = true;
function handleCities($country){
    global $twitter;
    $getfields = '?location_type=CITIES&lang=en&country_code='.$country['country_code'].'&count=1000';
    $url = 'https://ads-api.twitter.com/7/targeting_criteria/locations';
    $cursor = '';
    $requestMethod = 'GET';
    while(true){       
        sleep(1);         
        $cities = $twitter->setGetfield($getfields . $cursor )->buildOauth($url, $requestMethod)->performRequest();
        $cities =  json_decode($cities,true);
        $log['type'] = 'city';
        $log['code'] = $country['country_code'];
        $log['number'] =count($cities['data']);
        logVarDebugApend($log);
        if (!empty($cities['data'])) {
            foreach ($cities['data'] as $city_data){
                saveCity($city_data);
            }
        }
        if(isset($cities['next_cursor']) && $cities['next_cursor'] != ''){
            $cursor = '&cursor=' .$cities['next_cursor'];
        } else {
            break;
        }
    }
}

function saveCity($city){
    global $db;
    $type = "GEO";
    $city['localized_name'] = preg_replace('/\s+/', ' ', $city['localized_name']);
    $name_clean = $db->real_escape_string($city['localized_name']);
    $metadata = [];
    $metadata['twitter'] =$city;
    $metadata_clear =  $db->real_escape_string(json_encode($metadata ));
    $sql = "INSERT INTO settings_matrix SET 
                  name = '{$name_clean}' ,
                  nombre = '{$name_clean}',
                  type = '{$type}',
                  uuid = REPLACE(UUID(),'-',''),
                  name_tw = '{$name_clean}' ,
                  valor_tw = '{$city['targeting_value']}',
                  metadata = '{$metadata_clear}'";
    $db->query($sql);
}

function logVarDebugApend($var, $file = 'bla.html') {
    ob_start();
    var_dump($var);
    $data = "<pre>" . ob_get_clean() . "</pre>";
    $fp = fopen( $file, "a");
    fwrite($fp, $data);
    fclose($fp);
}