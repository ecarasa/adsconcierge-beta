<?php

/**
 * Description of db
 *
 * @author Goran.Jordanoski
 */
class db
{
    private $cn;

    public function __construct()
    {
        global $config;
        $this->cn = mysqli_connect($config['db_host'], $config['db_username'], $config['db_password'], $config['db_database_name'])
                        or die(mysqli_error($this->cn));

        $this->cn->set_charset("utf8");
    }

    function getCN()
    {
        return $this->cn;
    }

    function getRecordSet($sql)
    {
        $rs = $this->cn->query($sql) or die(mysqli_error($this->cn). " " . $sql);

        $recordSet = array();
        $result = array();
        while ($row = mysqli_fetch_assoc($rs)) {
            $result[] = $row;
        }
        mysqli_free_result($rs);

        return $result;
    }

    function getRow($sql)
    {
        $rs = $this->cn->query($sql) or die(mysqli_error($this->cn). " " . $sql);

        $row = mysqli_fetch_assoc($rs);
        mysqli_free_result($rs);

        return $row;
    }

    function query($sql)
    {
        if (isset($_REQUEST['debug']) && $_REQUEST['debug'] == '1') {
            echo $sql . '<br><br>';
        }
        $rs = $this->cn->query($sql) or die(mysqli_error($this->cn). " " . $sql);

        return $rs;
    }

    function queryApi($sql){
        if(isset($_REQUEST['debug']) && $_REQUEST['debug'] == '1'){
            echo $sql . '<br><br>';
        }
        $rs = $this->cn->query($sql) or $this->returnFalse();

        return $rs;
    }

    function returnFalse(){
        return false;
    }

    function returnError($error){
        $rt['error'] = $error;
        return $rt;
    }

    function real_escape_string($str)
    {
        return mysqli_real_escape_string($this->cn, $str);
    }
}
