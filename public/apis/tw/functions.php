<?php
function get_auth($user_id, $media){
    global $db;
    $sql = "SELECT 
            a.*, 
            u.customer_id_default,
            u.campaign_root_default
        FROM auths_user_platform a 
            LEFT JOIN users u ON u.id= a.user_id
        WHERE user_id={$user_id} AND platform='{$media}'";

    $auth = $db->getRow($sql);
    if($auth['platform'] == 'TWITTER'){
        $auth['settings'] = [];
        $dataArray = json_decode($auth['app_secret']);
        $auth['settings']['oauth_access_token'] = $auth['app_id'];
        $auth['app_id'] =  $dataArray->consumer_key;
        $auth['settings']['oauth_access_token_secret'] = $auth['access_token'];
        $auth['settings']['consumer_key'] = $dataArray->consumer_key;
        $auth['settings']['consumer_secret'] = $dataArray->consumer_secret;
    }

    return $auth;
}

function get_ads_accounts($user_id, $media){
    global $db;
    $sql = "SELECT * 
            FROM ads_accounts  
            WHERE user_id={$user_id} AND platform='{$media}'";
    $rs = $db->getRecordSet($sql);

    foreach($rs as $val){
        $ads_accounts[$val['account_id']] = $val;
    }
    return $ads_accounts;
}

function get_campaign_platform_by_urn($campaignGroup){
    $cpgArray = explode(':', $campaignGroup);
    $row = get_campaign_platform_by_id($cpgArray[3]);

    return $row;
}

function get_campaign_platform_by_id($id_en_platform){
    global $db;
    $sql = "SELECT * from campaigns_platform WHERE id_en_platform = '{$id_en_platform}'";
    $row = $db->getRow($sql);//echo "<pre>";print_r($row);die;

    return $row;

}