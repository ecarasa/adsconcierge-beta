<?php

function snapchatRefreshToken($auth){
    $curl = curl_init();
    $url = "https://accounts.snapchat.com/login/oauth2/access_token" ;

    $postfields[] = 'client_id=' . $auth['app_id'];
    $postfields[] = 'client_secret=' . $auth['app_secret'];
    $postfields[] = 'grant_type=refresh_token';
    $postfields[] = 'refresh_token='. $auth['refresh_token'];

    $queryString = implode('&', $postfields);

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url.'?'.$queryString ,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_HTTPHEADER => array(
            "Content-Length: 0"
        )
    ));

    curl_setopt($curl, CURLOPT_VERBOSE, 1);
    curl_setopt($curl, CURLOPT_HEADER, 1);

    $response = curl_exec($curl);

    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

    curl_close($curl);

    $header = substr($response, 0, $header_size);
    $body = substr($response, $header_size);

    $rt['headers'] = getHeaders($header);
    $rt['body'] = $body;

    return $rt;

}

function snapchatGet($method, $token, $params, $nextUrl = ''){
    //https://adsapi.snapchat.com/v1/me/organizations?with_ad_accounts=true
    $queryString = http_build_query($params);//echo($queryString);die;
    $url = "https://adsapi.snapchat.com/v1/" . $method . "?" . $queryString;
    if(count($params) < 1) {
        $url = "https://adsapi.snapchat.com/v1/" . $method ;
    }
    if($nextUrl != ''){
        $url = $nextUrl;
    }
    //echo $url;

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url ,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer {$token}"
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function snapchatPost($method, $token, $campaign_json){
    $curl = curl_init();
    $url = "https://adsapi.snapchat.com/v1/" . $method ;

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url ,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $campaign_json,
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer {$token}",
            "Content-Type: application/json"
        ),
    ));

    curl_setopt($curl, CURLOPT_VERBOSE, 1);
    curl_setopt($curl, CURLOPT_HEADER, 1);

    $response = curl_exec($curl);

    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

    curl_close($curl);

    $header = substr($response, 0, $header_size);
    $body = substr($response, $header_size);

    $rt['headers'] = getHeaders($header);
    $rt['body'] = $body;

    return $rt;

}

function snapchatHandleNext($paging){
    $url = '';
    if(isset($paging['next_link'])){
        $url = $paging['next_link'];
    }
    return $url;
}

function getHeaders($header){
    $data = explode("\n",$header);
    $headers = array();
    foreach($data as $part){

        //some headers will contain ":" character (Location for example), and the part after ":" will be lost, Thanks to @Emanuele
        $middle = explode(":",$part,2);

        //Supress warning message if $middle[1] does not exist, Thanks to @crayons
        if ( !isset($middle[1]) ) { $middle[1] = null; }

        $headers[trim($middle[0])] = trim($middle[1]);
    }
    return $headers;
}

