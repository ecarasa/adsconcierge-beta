<?php
function get_accounts_twitter(){
    global $twitter;
//========================================================
//Get all accounts
    $url = 'https://ads-api.twitter.com/7/accounts/';
    $requestMethod = 'GET';
    $accounts = $twitter->buildOauth($url, $requestMethod)
        ->performRequest();
    $accounts =  json_decode($accounts,true);
    return $accounts;
//Get all accounts
//========================================================
}

function get_funding_instruments_twitter($account_id){
    global $twitter;
    //==============================================
    //GET funding instruments
    $url = 'https://ads-api.twitter.com/7/accounts/'.$account_id.'/funding_instruments';
    $requestMethod = 'GET';
    $fundingInstruments = $twitter->buildOauth($url, $requestMethod)
        ->performRequest();
    $fundingInstruments = json_decode($fundingInstruments,true);
    //GET funding instruments
    //==============================================
    return  $fundingInstruments ;
}


function get_promotable_users_twitter($account_id){
    global $twitter;
    //==============================================
// Get Promotable Users
    $url = 'https://ads-api.twitter.com/7/accounts/'.$account_id.'/promotable_users';
    $requestMethod = 'GET';
    $promotableUsers = $twitter->buildOauth($url, $requestMethod)
        ->performRequest();
    $promotableUsers = json_decode($promotableUsers,true);
// Get Promotable Users
//==============================================
    return  $promotableUsers ;
}

function get_user_look_up_twitter($user_id){
    global $twitter;
    //==============================================
    // Get user Lookup
    $url = 'https://api.twitter.com/1.1/users/lookup.json';
    $getfield = '?user_id='.$user_id;
    $requestMethod = 'GET';
    $userData = $twitter->setGetfield($getfield)->buildOauth($url, $requestMethod)
        ->performRequest();
    $userData = json_decode($userData,true);
    $userData = $userData[0];

    return $userData;
    // Get user Lookup
    //==============================================
}

function get_promoted_tweets($account_id){
    global $twitter;
    //==============================================
    //GET Promoted Tweets
    $url = 'https://ads-api.twitter.com/7/accounts/'.$account_id.'/promoted_tweets';
    $requestMethod = 'GET';
    $promotedTweets = $twitter->buildOauth($url, $requestMethod)
        ->performRequest();
    $promotedTweets = json_decode($promotedTweets,true);

    return $promotedTweets;
    //GET Promoted Tweets
    //==============================================
}
function get_tweet_data($account_id, $tweet_id, $tweet_type){
    global $twitter;
    //==============================================
    // Get Tweet Data
    $url = 'https://ads-api.twitter.com/7/accounts/'.$account_id.'/tweets';
    $requestMethod = 'GET';
    $getfields = "?tweet_type=" . $tweet_type . "&tweet_ids=" . $tweet_id . "&timeline_type=ALL";
    $tweets = $twitter->setGetfield($getfields)->buildOauth($url, $requestMethod)
        ->performRequest();
    $tweets = json_decode($tweets,true);

    return $tweets;
    // Get Tweet Data
    //==============================================
}