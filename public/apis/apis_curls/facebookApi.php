<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/../../../vendor/autoload.php';
require_once __DIR__ . '/../configs/general.php';
require_once __DIR__ . '/../configs/facebook.php';

use FacebookAds\Api;
use FacebookAds\Cursor;
Cursor::setDefaultUseImplicitFetch(false);
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\User;

/*
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Cursor;
Cursor::setDefaultUseImplicitFetch(false);
use FacebookAds\Object\User;
use FacebookAds\Object\Page;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Values\AdSetBillingEventValues;
use FacebookAds\Object\Values\AdSetOptimizationGoalValues;
use FacebookAds\Object\Fields\TargetingFields;
use FacebookAds\Object\Targeting;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Fields\AdsInsightsFields;
use FacebookAds\Object\AdsInsights;
*/

$pos =  isset($argv[0]) && strpos($argv[0], "facebookApi.php");

if ($pos !== false) {
    if (isset($argv[1])) {

        $args = json_decode($argv[1], true);

        if (is_array($args)) {

            $query = "SELECT AUP.id, AUP.auths_user_id, AUP.user_id, AUP.platform, AUP.app_id, AUP.app_secret, AUP.platform_user_id, AUP.access_token, AUP.refresh_token, U.name, U.email, U.customer_id_default, U.campaign_root_default 
            FROM auths_user_platform AUP JOIN users U ON (AUP.user_id = U.id) WHERE AUP.id = " . $args["platform_account_id"];
            $resultado = $dbconn->query($query);
            
            //print_r($query);

            $info = $resultado->fetch_assoc();

            $api = Api::init($info['app_id'], $info['app_secret'], $info['access_token']);

            switch ($args["function"]) {
                case "get_atomo":
                    echo "begin get_atomo" . "\n";
                    getAdSets($args["id_en_platform"], $info);
                    echo "end get_atomo" . "\n";
                    break;
                case "get_creatividad":
                    echo "begin get_creatividad" . "\n";
                    getAds($args["id_en_platform"], $info);
                    echo "end get_creatividad" . "\n";
                    break;
            }

            $sql = "UPDATE adsconcierge_stats.background_job SET status = 'finished' WHERE id = " . $args["job"]["id"];
            $dbconn->query($sql);
        }
    }
}

function facebook_account_retrieve_all($credentials=['app_id'=>'', 'app_secret'=>'', 'access_token'=>'', 'platform_user_id'=>''], $user_id, $rowdata) {
    Global $dbconn;
    $auths=$rowdata;
    print_r($auths);
    
    try {

        $api = Api::init($credentials['app_id'], $credentials['app_secret'], $credentials['access_token']);

      /*  $fields = array('account_status','name ','id','age','amount_spent','attribution_spec', 'account_id','balance', 'business_name' ,
            'business_city','business_country_code', 'currency','owner',
            'partner','user_tos_accepted',	
            'spend_cap', 'tos_accepted','offsite_pixels_tos_accepted',
            'end_advertiser','end_advertiser_name','media_agency','partner',
            'user_tasks', 'disable_reason','has_migrated_permissions','users',
            'is_prepay_account','media_agency','can_create_brand_lift_study','is_direct_deals_enabled','is_in_middle_of_local_entity_migration'
        );  */
       
        $fields = array(
            'account_status', 'name', 'id', 'attribution_spec',  'account_id',
           // 'amount_spent',
           // 'spend_cap',
            'business_name' , 'business_city','business_country_code', 'currency',
            'owner', 'partner', 'user_tos_accepted',	
            'tos_accepted', 'offsite_pixels_tos_accepted', 'end_advertiser', 'end_advertiser_name',
            'media_agency', 'partner', 'user_tasks', 'disable_reason',
            'has_migrated_permissions', 'users', 'is_prepay_account',
            'media_agency',
           // 'can_create_brand_lift_study',
            'is_direct_deals_enabled',
            'is_in_middle_of_local_entity_migration'
        );

        //$params = array('summary' => true, 'limit'=>10);
        $params = array('summary' => true, 'limit'=> "300");
        
        echo 'Parametros de busqueda -> ' . serialize($params).PHP_EOL;
        $adsaccounts=(new User($credentials['platform_user_id']))->getAdAccounts($fields, $params);
      
        //var_dump($adsaccounts);
        //die();
        //$arrayadsaccounts=[];

        $adsaccounts->setDefaultUseImplicitFetch(true);
        $stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`ads_accounts`
            (`user_id`, `platform`, `app_id`, `account_id`, `name`, `platform_user_id`, `status`, `currency`, `metadata`, `customer_id`, `auth_id`)
            VALUES (?,?,?,?,?,?,?,?,?,?,?) 	ON DUPLICATE KEY UPDATE `name`= ?,  `status`= ?, `platform_user_id`=?,  `metadata`= ? ");

        $fields=array('id', 'name','category');
        $params = array( 'summary' => true);
        echo 'RECORREMOS AdsAccounts...'.count($adsaccounts);
        $i = 0;
        
        foreach($adsaccounts as $sitem) {
            $i++;
              $item=$sitem->getData();
            echo PHP_EOL. $item['account_id'] .' --- '.$item['name'] . ','.PHP_EOL;
        
            $ad_accountid=$item['account_id'];
            $arrayadsaccounts=[ 'ad_account'=> $ad_accountid, 'currency' =>$item['currency']  ];
            $adaccount_name=$item['name'];
            $currency=$item['currency'];
            $status=$item['account_status'].'|'.$item['disable_reason'];
            $metadata=json_encode($item, true);

            $stmt->bind_param("isiisisssiissis", $user_id, $auths["platform"], $auths["app_id"], $ad_accountid, $adaccount_name,
                $credentials['platform_user_id'], $status, $currency, $metadata, $auths["customer_id_default"], $auths["id"], $adaccount_name,
                $status, $credentials['platform_user_id'], $metadata);
            $stmt->execute();
          
                $query = "SELECT id as adacc_id, public_id as adaccount_public_id FROM `app_thesoci_9c37`.`ads_accounts` WHERE auth_id = '{$auths["id"]}' and platform = '{$auths["platform"]}' and platform_user_id= '{$credentials['platform_user_id']}' and account_id = '{$ad_accountid}' limit 1    ";
                $res_ac = $dbconn->query($query);
                $id_ac = $res_ac->fetch_assoc();
            //   print_R( $id_ac );
          //     print_R( $query );
            $fff =0;
          if ($ad_accountid=='704144226452539'){
            print_r( $sitem->getConnectedInstagramAccounts( ) );
             foreach( $sitem->getConnectedInstagramAccounts( ) as $instagramacc ) {
   
            // print_r($instagramacc );
            print_r($instagramacc->getData());
            
            }
          }
            foreach ($sitem->getPromotePages($fields,  $params) as $p){
               
                $page= $p->getData();
                 //print_r($page );
           
                $fff++;
                echo  $fff .$page['name']. ','.PHP_EOL;

                $page_id=$page['id'];
                $pagename=$page['name'];
                $category=$page['category'];
                $token= $credentials['access_token'];

                $status=0;
                $metadata=json_encode($item, true);
                $stmt2 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` (`user_id`, `platform`, `app_id`,`platform_user_id`, `id_en_platform`, `token`,`name`,`status`,`category`, `metadata`, `adaccount_id`, `auth_id`)
								VALUES (?,?,?,?,?,?,?,?,?,?,?,?)
								ON DUPLICATE KEY UPDATE    `name`= ?,  `status`= ?,`platform_user_id`=?,  `metadata`= ? ");
                $stmt2->bind_param("ssssssssssssssss", $user_id, $auths["platform"], $auths["app_id"], $credentials['platform_user_id'], $page_id, $token, $pagename, $status, $category, $metadata, $ad_accountid, $auths["id"], $pagename, $status, $credentials['platform_user_id'], $metadata);
                $stmt2->execute();
 
                $query = "SELECT id as pageid, public_id as property_public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE  auth_id = '{$auths["id"]}' and platform = '{$auths["platform"]}' and platform_user_id= '{$credentials['platform_user_id']}' and id_en_platform = '{$page_id}'";
                $res_pa = $dbconn->query($query);
                $id_pa = $res_pa->fetch_assoc();
            //    print_r( $id_pa);
              /**
                $query = "SELECT public_id FROM `app_thesoci_9c37`.`users` WHERE id = " .$user_id;
                $res_u = $dbconn->query($query);
                $id_u = $res_u->fetch_assoc();
              **/
        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` 
        (`type`,`auth_id`, `ad_account`,  `ad_account_name`,`property_name`, `property_id`, `user_id`,`platform`,`auth_publicid`,   `user_publicid`,  `ad_account_publicid`,`property_publicid`) 
        VALUES ('PAGE', ?,?,?,?,?,?,?,?,?,?,?)
        ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");              
                        $stmt3->bind_param("sssssssssssss", 
                        $auths["id"], 
                        $id_ac["adacc_id"] ,
                        $adaccount_name,
                        $pagename, 
                        $id_pa["pageid"],      
                        $user_id,
                        $auths["platform"],                   
                        $auths["auths_user_id"],
                        $auths["user_public_id"],
                        $id_ac["adaccount_public_id"], 
                        $id_pa["property_public_id"],  
                        $adaccount_name, 
                        $pagename);
        $stmt3->execute();
              
        /**
        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` (`auth_id`, `auth_publicid`, `ad_account`, `ad_account_publicid`, `ad_account_name`, `property_id`, `property_publicid`, `property_name`, `user_id`, `user_publicid`) VALUES (?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");
        $stmt3->bind_param("isssssssisss", $auths["id"], $id_aup["auths_user_id"], $ad_accountid, $id_ac["public_id"], $item["name"], $page_id, $id_pa["public_id"], $name, $user_id, $id_u["public_id"], $item["name"], $name);
        $stmt3->execute();
        **/
              
          //    exit;       
        try {
            print_r( $p->getInstagramAccounts( ) );
        } catch (Exception $e) {
                
        }
   
    }
          
        foreach( $sitem->getAdsPixels( array('id', 'name','is_unavailable','code','owner_ad_account','first_party_cookie_status','automatic_matching_fields')) as $pixel ) {
                /**Array
                (
                    [automatic_matching_fields] =>
                    [can_proxy] =>
                    [code] => <!-- Facebook Pixel Code -->
                <script nonce="mCudTrxd">
                !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','https://connect.facebook.net/en_US/fbevents.js');

                fbq('init', '1567559540192035');
                fbq('track', "PageView");</script>
                <noscript><img height="1" width="1" style="display:none"
                src="https://www.facebook.com/tr?id=1567559540192035&ev=PageView&noscript=1"
                /></noscript>
                <!-- End Facebook Pixel Code -->
                    [creation_time] =>
                    [creator] =>
                    [data_use_setting] =>
                    [enable_automatic_matching] =>
                    [first_party_cookie_status] => first_party_cookie_enabled
                    [id] => 1567559540192035
                    [is_created_by_business] =>
                    [is_unavailable] =>
                    [last_fired_time] =>
                    [name] => Jr II's Pixel
                    [owner_ad_account] => Array
                        (
                            [account_id] => 365485994
                            [id] => act_365485994
                        )

                    [owner_business] =>
                )
                **/
    
        $itempixel = $pixel->getData();
            $status=0;
                    $metadata=json_encode($itempixel, true);
                    $itemid=$itempixel['id'];
                    $itemname=$itempixel['name'];
                    $category="-";
                    $token= "-";
                    $tipoitem="PIXEL";
                    $stmt2 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` (`type`,`user_id`, `platform`, `app_id`,`platform_user_id`, `id_en_platform`, `token`,`name`,`status`,`category`, `metadata`, `adaccount_id`, `auth_id`)
                                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)
                                    ON DUPLICATE KEY UPDATE    `name`= ?,  `status`= ?,`platform_user_id`=?,  `metadata`= ? ");
        
        //print_R([$tipoitem, $user_id, $auths["platform"], $auths["app_id"], $credentials['platform_user_id'], $itemid, $token, $itemname, $status, $category, $metadata, $ad_accountid, $auths["id"], $itemname, $status, $credentials['platform_user_id'], $metadata] );
        
                    $stmt2->bind_param("sssssssssssssssss", $tipoitem, $user_id, $auths["platform"], $auths["app_id"], $credentials['platform_user_id'], $itemid, $token, $itemname, $status, $category, $metadata, $ad_accountid, $auths["id"], $itemname, $status, $credentials['platform_user_id'], $metadata);
                    $stmt2->execute();
        

            $query = "SELECT id as propertyid, public_id as property_public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE type='PIXEL' and
            auth_id = '{$auths["id"]}' and platform = '{$auths["platform"]}' and platform_user_id= '{$credentials['platform_user_id']}' and id_en_platform = '{$itemid}'";
                    $res_pa = $dbconn->query($query);
                    $id_pa = $res_pa->fetch_assoc();
        //print_R( $id_pa); 
        
        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` 
            (`type`,`auth_id`, `ad_account`,  `ad_account_name`,`property_name`, `property_id`, `user_id`,`platform`,`auth_publicid`,   `user_publicid`,  `ad_account_publicid`,`property_publicid`) 
            VALUES ('PIXEL',?,?,?,?,?,?,?,?,?,?,?)
            ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");
            $stmt3->bind_param("sssssssssssss", 
                            $auths["id"], 
                            $id_ac["adacc_id"] ,
                            $adaccount_name,
                            $itemname, 
                            $id_pa["propertyid"],      
                            $user_id,
                            $auths["platform"],                   
                            $auths["auths_user_id"],
                            $auths["user_public_id"],
                            $id_ac["adaccount_public_id"], 
                            $id_pa["property_public_id"],  
                            $adaccount_name, 
                            $itemname);
            $stmt3->execute();
        
                
        
        }

        
            
 
            $campaigns= $sitem->getCampaigns(CampaignFields::getInstance()->getvalues(),   array('summary' => true));
            $campaigns->setDefaultUseImplicitFetch(true);
            $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (user_id, platform, app_id, auth_id, name, id_en_platform, status, currency, metadata, ad_account, campana_root, customer_id, source)
											values (?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id =? and ads_accounts.user_id=?), ?,?, 'IMPORTED')
											on duplicate key update status=?, metadata=?, campana_root=?, customer_id=?");

            $cc =0;

            foreach( $campaigns as $citem) {
                $cc++;
                echo  $cc . ' --- '. $citem['name']. ','.PHP_EOL;
                $citem=$citem->getData();
                $stmtcampana->bind_param("isssssssssissssss",...[ $user_id, $rowdata['platform'],$credentials['app_id'] , $rowdata['id'],  $citem['name'],  $citem['id'], unificastatus('campana', $rowdata['platform'],   $citem['status'], ['estado'=>  $citem['status'], 'otros'=>   $citem['effective_status'] , 'record'=>$item ] ) , $currency, json_encode(  $citem ),  'act_'. $citem['account_id']  ,$user_id, $rowdata["campaign_root_default"], $rowdata["customer_id_default"],  unificastatus('campana', $rowdata['platform'],     $citem['status'], ['estado'=>  $citem['status'], 'otros'=>  $citem['effective_status'], 'record'=>$citem ] )   , json_encode($citem ), $rowdata["campaign_root_default"], $rowdata["customer_id_default"]]);

                $stmtcampana->execute();
                if($stmtcampana->error != ""){
                    printf("Error: %s.\n", $stmtcampana->error);
                }

                if($stmtcampana->affected_rows > 0){
                    $dbconn->query("INSERT INTO adsconcierge_stats.background_job (type, subject_id, next_execution) VALUES('get_atomo', '".$stmtcampana->insert_id."', '".date("Y-m-d H:i:s")."')");
                }
            }
 
           // die();
        }


    } catch (Exception $e) {
        echo 'FACEBOOK API ERROR. Caught exception: ',  $e->getMessage(), "\n";
     //   print_r($rowdata);
    }



}

function getCampaigns($ad_accountid, $user_id, $rowdata)
{
    global $dbconn, $credentials, $item;

    $params = array('summary' => true);

    $campanafields = CampaignFields::getInstance();

    $campaigns = ((new AdAccount($ad_accountid['ad_account']))->getCampaigns($campanafields->getvalues(), $params));
    $campaigns->setDefaultUseImplicitFetch(true);
    // $campaigns->setDefaultUseImplicitFetch(true);
    /***
    {
        "account_id": "2848983818507558",
        "budget_rebalance_flag": false,
        "budget_remaining": "0",
        "buying_type": "AUCTION",
        "can_create_brand_lift_study": false,
        "can_use_spend_cap": true,
        "configured_status": "PAUSED",
        "created_time": "2019-08-06T10:00:06+0200",
        "effective_status": "PAUSED",
        "id": "23843687268390653",
        "name": "Lady Bug_ES 2",
        "objective": "CONVERSIONS",
        "source_campaign_id": "0",
        "special_ad_category": "NONE",
        "spend_cap": "100000",
        "start_time": "2019-08-06T10:00:10+0200",
        "status": "PAUSED",
        "topline_id": "0",
        "updated_time": "2019-09-12T15:55:51+0200"
    }
     **/
    $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (user_id, platform, app_id, auth_id, name, id_en_platform, status, currency, metadata, ad_account, campana_root, customer_id, source)
											values (?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id =? and ads_accounts.user_id=?), ?,?, 'IMPORTED')
											on duplicate key update status=?, metadata=?, campana_root=?, customer_id=?");

    echo 'summary total_account ' . count($campaigns) . PHP_EOL;
    foreach ($campaigns as $citem) {
        $citem = $citem->getData();

        //$budget = ($citem['daily_budget'] == "")? 0:$citem['daily_budget']/100;
        //$budget_total = ($citem['lifetime_budget'] == "")? 0:$citem['lifetime_budget']/100;

        $stmtcampana->bind_param("isssssssssissssss", ...[$user_id, $rowdata['platform'], $credentials['app_id'], $rowdata['id'], $citem['name'], $citem['id'], unificastatus('campana', $rowdata['platform'], $citem['status'], ['estado' => $citem['status'], 'otros' => $citem['effective_status'], 'record' => $item]), $ad_accountid['currency'], json_encode($citem), 'act_' . $citem['account_id'], $user_id, $rowdata["campaign_root_default"], $rowdata["customer_id_default"], unificastatus('campana', $rowdata['platform'], $citem['status'], ['estado' => $citem['status'], 'otros' => $citem['effective_status'], 'record' => $citem]), json_encode($citem), $rowdata["campaign_root_default"], $rowdata["customer_id_default"]]);

        $stmtcampana->execute();
        if ($stmtcampana->error != "") {
            printf("Error: %s.\n", $stmtcampana->error);
        }

        if ($stmtcampana->affected_rows > 0) {
            $dbconn->query("INSERT INTO adsconcierge_stats.background_job (type, subject_id, next_execution) VALUES('get_atomo', '" . $stmtcampana->insert_id . "', '" . date("Y-m-d H:i:s") . "')");
        }
    }
}

function getAdSets($id_campaing_platform, $auths)
{
    global $dbconn;

    $AdSetFields = AdSetFields::getInstance();
    $fields = $AdSetFields->getvalues();

    unset($fields[24]);
    unset($fields[54]);
    unset($fields[57]);
    unset($fields[61]);

    $campaigns = new Campaign($id_campaing_platform);
    $adsets = $campaigns->getAdSets($fields);

    $stmtadset = $dbconn->prepare("INSERT INTO campaigns_platform_atomo (user_id, id_en_platform, platform, app_id, campana_root_id, campana_platform_id, auth_id, name, status, customer_id, metadata, ad_account, source)
									values (?,?,?,?,?, (SELECT id FROM campaigns_platform WHERE campaigns_platform.id_en_platform = ? and campaigns_platform.user_id = ?),?,?,?,?,?, (SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id =? and ads_accounts.user_id=?), 'IMPORTED')
									on duplicate key update status=?, metadata=?");

    foreach ($adsets as $adset) {
        $adset = $adset->getData();

        $stmtadset->bind_param("isssssissssssiss", ...[$auths['user_id'], $adset['id'], $auths['platform'], $auths['app_id'], $auths["campaign_root_default"], $adset['campaign_id'], $auths['user_id'], $auths['id'], $adset['name'], $adset['status'], $auths["customer_id_default"], json_encode($adset), 'act_' . $adset['account_id'], $auths['user_id'], $adset['status'], json_encode($adset)]);

        $stmtadset->execute();
        if ($stmtadset->error != "") {
            printf("Error: %s.\n", $stmtadset->error);
        }

        if ($stmtadset->affected_rows > 0) {
            $dbconn->query("INSERT INTO adsconcierge_stats.background_job (type, subject_id, next_execution) VALUES('get_creatividad', '" . $stmtadset->insert_id . "', '" . date("Y-m-d H:i:s") . "')");
        }
    }
}

function getAds($id_atomo_platform, $auths)
{

    global $dbconn;

    $AdFields = AdFields::getInstance();
    $fields = $AdFields->getvalues();

    $AdSet = new AdSet($id_atomo_platform);
    $ads = $AdSet->getAds($fields);

    $stmtadset = $dbconn->prepare("INSERT INTO creatividades ( id_en_platform, user_id, customer_id,  name, 
                                                    platform, campana_root, campana_platform_id, atomo_id,
                                                    platform_status, metadata, source )
									values (?,?,?,?,?,?,(SELECT id FROM campaigns_platform WHERE campaigns_platform.id_en_platform = ? and campaigns_platform.user_id = ? LIMIT 1), (SELECT id FROM campaigns_platform_atomo WHERE id_en_platform = ? and user_id = ? LIMIT 1),?,?, 'IMPORTED') on duplicate key update platform_status=?, metadata=?");

    echo "Creatividades -> " . count($ads) . "\n";
    
    foreach ($ads as $ad) {
    
        $ad = $ad->getData();
  
        $stmtadset->bind_param("sisssssisissss", ...[   $ad['id'],
                                                        $auths['user_id'],
                                                        $auths["customer_id_default"],
                                                        $ad['name'],
                                                        $auths['platform'],
                                                        $auths["campaign_root_default"],
                                                        $ad['adset_id'],
                                                        $auths['user_id'],
                                                        $ad['adset_id'],
                                                        $auths['user_id'],
                                                        $ad['status'],
                                                        json_encode($ad),
                                                        $ad['status'],
                                                        json_encode($ad) ]);

        var_dump($stmtadset);
        
        $stmtadset->execute();
        if ($stmtadset->error != "") {
            printf("Error: %s.\n", $stmtadset->error);
        }
    }

}
