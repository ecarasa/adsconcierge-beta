<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require __DIR__ . '/../../../vendor/autoload.php';

require_once __DIR__ . '/../configs/general.php';
require_once __DIR__ . '/../configs/facebook.php';
require_once __DIR__ . '/facebookApi.php';
use FacebookAds\Api;
use FacebookAds\Cursor;
Cursor::setDefaultUseImplicitFetch(false);
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Page;
//facebook_stats_from_adaccount


function facebook_stats_from_campaign($credentials = ['app_id' => '', 'app_secret' => '', 'access_token' => ''], $userid, $ad_accountdata, $startdate, $enddate, $timeinterval, $pivot)
{

    try {

        $api = Api::init($credentials['app_id'], $credentials['app_secret'], $credentials['access_token']);

        $fields = ['impressions', 
                    'campaign_id', 
                    'account_id', 
                    'account_currency', 
                    'buying_type', 
                    'campaign_name', 
                    'clicks', 
                    'conversions', 
                    'date_start', 
                    'date_stop', 
                    'objective', 
                    'reach', 
                    'spend', 
                    'inline_post_engagement', 
                    'actions', 
                    'wish_bid', 
                    'ad_bid_type', 
                    'social_spend', 
                    'video_thruplay_watched_actions', 
                    'video_play_actions', 
                    'ad_bid_value'];

        $params = ['level' => 'campaign', 'time_range' => array('since' => substr($startdate, 0, 10), 'until' => substr($enddate, 0, 10)), 'time_increment' => '1', 'breakdowns' => 'publisher_platform'];

        echo "GET AdAccount Insights - " . $ad_accountdata . PHP_EOL;

        $datos = (new AdAccount("act_".$ad_accountdata))->getInsights($fields, $params);

        // var_dump($account);
        // $datos = (new AdAccount($ad_accountdata))->getInsights($fields, $params);

        $datos->setDefaultUseImplicitFetch(true);
        $retorno = [];
        foreach ($datos as $citem) {
            $retorno[] = $citem->getData();

        }
        return $retorno;
    } catch (Exception $e) {
        echo 'Caught exception: ', $e->getMessage(), "\n";
    }

}

function facebook_stats_from_creative_ad($credentials = ['app_id' => '', 'app_secret' => '', 'access_token' => ''], $userid, $ad_accountdata, $startdate, $enddate, $timeinterval, $pivot)
{

    //print_r(func_get_args());

    $api = Api::init($credentials['app_id'], $credentials['app_secret'], $credentials['access_token']);
    $fields = ['ad_id, adset_id, impressions', 'campaign_id', 'account_id', 'account_currency', 'buying_type', 'campaign_name', 'clicks', 'conversions', 'date_start', 'date_stop', 'objective', 'reach', 'spend', 'inline_post_engagement', 'actions', 'wish_bid', 'ad_bid_type', 'social_spend', 'video_thruplay_watched_actions', 'video_play_actions', 'ad_bid_value'];

    $params = [
        'level' => 'ad',
        'time_range' => array('since' => substr($startdate, 0, 10), 
        'until' => substr($enddate, 0, 10)),
        'time_increment' => '1',
        'breakdowns' => 'publisher_platform',
    ];

    $datos = (new AdSet($ad_accountdata))->getInsights($fields, $params);

    $datos->setDefaultUseImplicitFetch(true);
    $retorno = [];
    foreach ($datos as $citem) {
        $retorno[] = $citem->getData();
    }
    //print_r($retorno);die;
    return $retorno;

}
//https://developers.facebook.com/docs/marketing-api/reference/ad-account/adsets/
function facebook_stats_from_adset($credentials = ['app_id' => '', 'app_secret' => '', 'access_token' => ''], $userid, $ad_accountdata, $startdate, $enddate, $timeinterval, $pivot)
{

    $ad_accountdata = '6092835225832';
    $api = Api::init($credentials['app_id'], $credentials['app_secret'], $credentials['access_token']);
    $fields = ['adset_id, impressions', 'campaign_id', 'account_id', 
    'account_currency', 'buying_type', 'campaign_name', 'clicks', 
    'conversions', 'date_start', 'date_stop', 'objective', 'reach', 
    'spend', 'inline_post_engagement', 'actions', 'wish_bid', 'ad_bid_type',
     'social_spend', 'video_thruplay_watched_actions', 'video_play_actions', 'ad_bid_value'];
   
    $params = [
        'level' => 'adset',
        'time_range' => array('since' => substr($startdate, 0, 10), 'until' => substr($enddate, 0, 10)),
        'time_increment' => '1',
        'breakdowns' => 'publisher_platform',

    ];

    $datos = (new AdSet($ad_accountdata))->getInsights($fields, $params);

    $datos->setDefaultUseImplicitFetch(true);
    $retorno = [];
    foreach ($datos as $citem) {
        $retorno[] = $citem->getData();

    }
    return $retorno;
}
//https://developers.facebook.com/docs/marketing-api/insights/parameters/v5.0
//pivot values enum {ad, adset, campaign, account}
function facebook_stats_from_adaccount($credentials = ['app_id' => '', 'app_secret' => '', 'access_token' => ''], $userid, $ad_accountdata, $startdate, $enddate, $timeinterval, $pivot = 'campaign')
{

    //print_r(func_get_args());


    $api = Api::init($credentials['app_id'], $credentials['app_secret'], $credentials['access_token']);
    $fields = ['impressions', 'campaign_id', 'account_id', 'account_currency', 'buying_type', 'campaign_name', 'clicks', 'conversions', 'date_start', 'date_stop', 'objective', 'reach', 'spend', 'inline_post_engagement', 'actions', 'wish_bid', 'ad_bid_type', 'social_spend', 'video_thruplay_watched_actions', 'video_play_actions', 'ad_bid_value'];
    
    switch ($pivot) {
        case 'ad':
            $fields = array_merge($fields, ['ad_name', 'ad_id', 'adset_name', 'adset_id']);
            break;
        case 'adset':
            $fields = array_merge($fields, ['adset_name', 'adset_id']);
            break;
    }

    //$fields=[  "objective","reach","impressions"];
    $params = [

        'level' => $pivot,
        'date_preset' => 'last_7d',
//     'date_preset' => 'yesterday',
        'time_increment' => '1',
        'breakdowns' => 'publisher_platform',

    ];

//    print_r($ad_accountdata);
    $datos = (new AdAccount($ad_accountdata))->getInsights($fields, $params);
    /**
    [Breakdowns] => Array
    (
    [0] => ad_format_asset
    [1] => age
    [2] => body_asset
    [3] => call_to_action_asset
    [4] => country
    [5] => description_asset
    [6] => device_platform
    [7] => dma
    [8] => frequency_value
    [9] => gender
    [10] => hourly_stats_aggregated_by_advertiser_time_zone
    [11] => hourly_stats_aggregated_by_audience_time_zone
    [12] => image_asset
    [13] => impression_device
    [14] => link_url_asset
    [15] => place_page_id
    [16] => platform_position
    [17] => product_id
    [18] => publisher_platform
    [19] => region
    [20] => title_asset
    [21] => video_asset
    )

     */
    //$datos = (new AdSet($ad_accountdata))->getInsights($fields ,  $params);
    //print_r( (  $datos));

    $datos->setDefaultUseImplicitFetch(true);

    $retorno = [];
    foreach ($datos as $citem) {
        $retorno[] = $citem->getData();
        break;
    }
    return $retorno;

}
