<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once __DIR__ . '/../../../vendor/autoload.php';
require_once __DIR__ . '/../configs/general.php';
require_once __DIR__ . '/../configs/twitter.php';

use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;
use Hborras\TwitterAdsSDK\TwitterAds\Enumerations;
use Hborras\TwitterAdsSDK\TwitterAds\Fields\AnalyticsFields;


function twitter_stats_campana_xaccount($credentials, $platform_account_id, $userid, $itemsid_en_pl = null, $startDate, $endDate)
{

    try {
        TwitterAds::init($credentials['consumer_key'], $credentials['consumer_secret'], $credentials['oauth_token'], $credentials['oauth_token_secret']);
        $accountw = new Account($platform_account_id);
        $accountw->read();

        $getFundingInstruments = $accountw->getFundingInstruments();

        $campaigns = $accountw->getCampaigns('');

        $campaigns->setUseImplicitFetch(true);

        $campaignsIds = array();
        $campaignsData = array();
        $campStruct = array();

        foreach ($campaigns as $campaign) {
            if ($campaign->effective_status != 'RUNNING') {
                //continue;
            }
            $campaignsIds[] = $campaign->getId();
            $campaignsData[$campaign->getId()] = array(
                'id' => $campaign->getId(),
                'name' => $campaign->getName(),
                'currency' => $campaign->getCurrency(),
                'status' => $campaign->effective_status
            );

            if (count($campaignsIds) == 20) {
                $campStruct[] = array(
                    'campaignsIds' => $campaignsIds,
                    'campaignsData' => $campaignsData
                );
                $campaignsIds = array();
                $campaignsData = array();
            }
        }
        if (count($campaignsIds) > 0) {
            $campStruct[] = array(
                'campaignsIds' => $campaignsIds,
                'campaignsData' => $campaignsData
            );
        }

        $datos = array();


        foreach ($campStruct as $val) {

            $temp = twitter_stats_generic($userid, $accountw, AnalyticsFields::CAMPAIGN, $val['campaignsIds'], 'P1D', $startDate, $endDate, $val['campaignsData']);


            if (count($datos) > 0) {
                $datos = array_merge($datos, $temp);
            } else {
                $datos = $temp;
            }
        }


        return $datos;
    } catch (exception $e) {
        echo "Error api twitter: " . $e->getMessage();
        die();
    }
}


function twitter_stats_atomo_xaccount($userid, $accountw, $itemsid_en_pl = null, $startDate, $endDate, $lineitemsStruct)
{

    $datos = array();
    foreach ($lineitemsStruct as $val) {
        $temp = twitter_stats_generic($userid, $accountw, AnalyticsFields::LINE_ITEM, $val, 'P1D', $startDate, $endDate);
        if (count($datos) > 0) {
            $datos = array_merge($datos, $temp);
        } else {
            $datos = $temp;
        }
    }

    return $datos;
}

function twitter_stats_ads_xaccount($userid, $accountw, $itemsid_en_pl = null, $startDate, $endDate, $adsStruct)
{

    $datos = array();
    foreach ($adsStruct as $val) {
        $temp = twitter_stats_generic($userid, $accountw, AnalyticsFields::PROMOTED_TWEET, $val, 'P1D', $startDate, $endDate);
        if (count($datos) > 0) {
            $datos = array_merge($datos, $temp);
        } else {
            $datos = $temp;
        }
    }

    return $datos;
}

function twitter_stats_ad_xaccount($userid, $accountw, $itemsid_en_pl, $startDate, $endDate)
{
    $getFundingInstruments = $accountw->getFundingInstruments();

    $lineitems = $accountw->getLineItems('');
    $lineitems->setUseImplicitFetch(true);
    foreach ($lineitems as $lineitem) {
        $lineitemid = $lineitem->getId();
        $CampaignId = $lineitem->getCampaignId();

        foreach ($lineitem->getPromotedTweets() as $item) {
            $itemsIds[] = $item->getId();
            $itemsData[$item->getId()] = ['id' => $item->getId(), 'name' => $item->getTweetId(), 'lineitem_id' => $lineitem->getId(), 'campaign_id' => $lineitem->getCampaignId(), 'currency' => $lineitem->getCurrency()];
        }
    }

    $metricas = twitter_stats_generic($userid, $accountw, AnalyticsFields::PROMOTED_TWEET, $itemsIds, 'P1D', $startDate, $endDate, $itemsData);


    return $metricas;
}


function twitter_stats_generic($userid, $accountw, $kindentity, $itemsid_en_pl, $granular, $startDate, $endDate, $campaignsData = null)
{

    $dates = dateRanges($startDate, $endDate, $granular); //print_r($dates);die;

    $metricas = [];

    foreach ($dates as $date) {

        if (is_string($itemsid_en_pl)) {
            $items = [$itemsid_en_pl];
        } else {
            $items = $itemsid_en_pl;
        }

        try {
            $stats = $accountw->all_stats(
                $items,
                array(
                    AnalyticsFields::METRIC_GROUPS_BILLING,
                    AnalyticsFields::METRIC_GROUPS_VIDEO,
                    AnalyticsFields::METRIC_GROUPS_MEDIA,
                    AnalyticsFields::METRIC_GROUPS_WEB_CONVERSIONS,
                    AnalyticsFields::METRIC_GROUPS_MOBILE_CONVERSION,
                    AnalyticsFields::METRIC_GROUPS_ENGAGEMENT
                ),
                array(
                    AnalyticsFields::ENTITY => $kindentity,
                    AnalyticsFields::START_TIME => $date[0],
                    AnalyticsFields::END_TIME => $date[1],
                    AnalyticsFields::GRANULARITY => Enumerations::GRANULARITY_TOTAL
                )
            );

        } catch (exception $e) {
            echo "Error: " . $e->getMessage();
            continue;
        }

        foreach ($stats as $statsitem) {

            $statsData = cleanConversions($statsitem->id_data[0]->metrics);
            $name = $campaignsData[$statsitem->id]['name'];
            if ($kindentity == AnalyticsFields::PROMOTED_TWEET)
                $name = $campaignsData[$statsitem->id]['ad_name'];

            $metricas[] = array_merge([
                'entity' => $kindentity,
                'startdate' => $date[0]->format('Y-m-d H:i:s'),
                'enddate' => $date[1]->format('Y-m-d H:i:s'),
                'id_in_platform' => $statsitem->id,
                'id' => null,
                strtolower($kindentity) . '_name' => $name,
                'currency' => (isset($campaignsData[$statsitem->id]['currency']) ? $campaignsData[$statsitem->id]['currency'] : null),
            ], (array)$statsData);
        }

    }

    return $metricas;
}

function cleanConversions($stat)
{
    foreach ($stat as $key => $value) {
        if (strpos($key, 'conversion') !== false) {
            $checkEmpty = (object)array_filter((array)$stat->{$key});
            if ((array)$checkEmpty) {
                $stat->{$key} = $value;
            } else {
                unset ($stat->{$key});
            }
        }
    }
    return $stat;
}

