<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require __DIR__ . '/../../../vendor/autoload.php';
require_once __DIR__ . '/../configs/general.php';
require_once __DIR__ . '/../configs/linkedin.php';


function linkedin_stats_creative_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate)
{
    return linkedin_stats_account_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, 'CREATIVE', 'DAILY');
}

function linkedin_stats_ad_group_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate)
{
    return linkedin_stats_account_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, 'AD_GROUP', 'DAILY');
}

function linkedin_stats_campana_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate)
{
    $entity_filter = trim("accounts[0]=urn:li:sponsoredAccount:{$rowdata['platform_account_id']}");
    return linkedin_stats_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, 'CAMPAIGN_GROUP', 'DAILY', $entity_filter);
}

function linkedin_stats_atomo_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate)
{
    $entity_filter = "campaignGroups[0]=urn:li:sponsoredCampaignGroup:{$rowdata['id_en_platform']}";
    return linkedin_stats_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, 'CAMPAIGN', 'DAILY', $entity_filter);
}

function linkedin_stats_ads_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate)
{
    $entity_filter = "campaigns[0]=urn:li:sponsoredCampaign:{$rowdata['id_en_platform']}";
    return linkedin_stats_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, 'CAMPAIGN', 'DAILY', $entity_filter);
}

function linkedin_stats_account_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, $pivot, $timegranularity)
{
    $entity_filter = "accounts[0]=urn:li:sponsoredAccount:{$rowdata['platform_account_id']}";
    return linkedin_stats_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, $pivot, $timegranularity, $entity_filter);
}


function linkedin_stats_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, $pivot, $timegranularity, $entity_filter)
{

    $startdate = strtotime($startdate);
    $enddate = strtotime($enddate);

    $startyear = date('Y', $startdate);
    $startmonth = date('m', $startdate);
    $startday = date('j', $startdate);

    $endyear = date('Y', $enddate);
    $endmonth = date('m', $enddate);
    $endday = date('j', $enddate);

    $url = "https://api.linkedin.com/v2/adAnalyticsV2?q=analytics&pivot={$pivot}&timeGranularity={$timegranularity}&dateRange.start.month={$startmonth}&dateRange.start.day={$startday}&dateRange.start.year={$startyear}&dateRange.end.month={$endmonth}&dateRange.end.day={$endday}&dateRange.end.year={$endyear}&fields=clicks,dateRange,impressions,likes,shares,costInLocalCurrency,pivot,pivotValue,costInUsd,approximateUniqueImpressions,videoStarts,videoCompletions,videoFirstQuartileCompletions,videoMidpointCompletions,videoThirdQuartileCompletions,totalEngagements&" . $entity_filter;
    $headers = array();
    $headers[] = 'Authorization: Bearer ' . $access_token;;
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer {$access_token}"
        ),
    ));

    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);


    if ($err) {
        var_dump($err);
    }

    $response = json_decode($response, true);

    if (isset($response['elements'])) {
        $response = $response['elements'];
    } else {
        print_r($response);
    }
    return $response;

}
