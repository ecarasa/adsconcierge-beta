<?php
//https://developers.facebook.com/apps/2230398120518804/app-review/my-permissions/?business_id=1679040252344960
//https://developers.facebook.com/docs/marketing-apis#newapp
//https://github.com/facebook/facebook-php-business-sdk?fbclid=IwAR2IUIRresFa2PgNUxDwT_Wx3T5re8dDdZB_kJnI4Xgib32gLcdd_ytI7jc

/**
meter la gestion de errores en todas las llamadas, porque son informativos y afectan
Campana: get, crea x, update
Adset:  get, crea x, update 
Creatividad:  get, crea, update
pixel: traer, crear  https://developers.facebook.com/docs/marketing-api/reference/ad-account/adspixels/#campos
audiencia: get, crea, update  https://developers.facebook.com/docs/marketing-api/audiences-api

set si los ads son politicos

leer urls https://developers.facebook.com/docs/graph-api/reference/v4.0/url

Campana tipo "post automatico": como adespresso pero con excluyentes, cruces, 
https://developers.facebook.com/docs/graph-api/reference/user/posts/


Campana tipo "rtb dinamico"  https://developers.facebook.com/docs/graph-api/reference/rtb-dynamic-post/#parameters
https://developers.facebook.com/docs/marketing-api/dynamic-product-ads/ads-management#categories

Creatividad tipo travel, 
catalogo https://developers.facebook.com/docs/marketing-api/reference/product-catalog
travel https://developers.facebook.com/docs/marketing-api/travel-ads
Leads https://developers.facebook.com/docs/marketing-api/dynamic-ads-for-leadgen


https://developers.facebook.com/docs/graph-api/photo-uploads
 

https://developers.facebook.com/docs/graph-api/reference/v4.0/targeting

**/

require __DIR__.'/configs/general.php';
require __DIR__.'/configs/facebook.php';

require __DIR__ . '/../../vendor/autoload.php';

use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;	 	 
use FacebookAds\Cursor;
Cursor::setDefaultUseImplicitFetch(false);
use FacebookAds\Object\User;
use FacebookAds\Object\Page;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Values\AdSetBillingEventValues;
use FacebookAds\Object\Values\AdSetOptimizationGoalValues;
use FacebookAds\Object\Fields\TargetingFields;
use FacebookAds\Object\Targeting;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdCreativeFields;
//echo json_encode((new User($platform_user_id))->getAccounts(  $fields,  $params)->getResponse()->getContent(), JSON_PRETTY_PRINT);
//$salida=(new User($platform_user_id))->getAdAccounts(  $fields,  $params)->getResponse()->getContent();
	/**
	probar esta opcion
	use FacebookAds\Object\AdUser;

// Add after Api::init()
$me = new AdUser('me');
$my_adaccount = $me->getAdAccounts()->current();

***/

function inicializacion($userid) {
Global $dbconn,$app_id_fb,$app_secret,$platformid_facebook;
$sql = "SELECT * FROM auths_user_platform WHERE user_id='{$userid}' AND platform='{$platformid_facebook}' AND appid='{$app_id_fb}' limit 1";
$result = $dbconn->query($sql);
$row= $result->fetch_assoc();
$access_token=$row['access_token'];
$platform_user_id=$row['platform_user_id'];
$app_id=$row['appid'];



$api = Api::init($app_id, $app_secret, $access_token);
$api->setLogger(new CurlLogger());

$fields = array('account_status','name ','id','age','amount_spent','attribution_spec', 'account_id','balance', 'business_name' , 'business_city','business_country_code', 'currency','owner', 
				'partner','user_tos_accepted',	'spend_cap', 'tos_accepted','offsite_pixels_tos_accepted','user_role','user_tasks', 'disable_reason','has_migrated_permissions'
				, 'is_prepay_account','media_agency','can_create_brand_lift_study','is_direct_deals_enabled','is_in_middle_of_local_entity_migration'
				//,		'business', 	 para permiso business managment
				//,'direct_deals_tos_accepted' para produccion
				// ,'rf_spec' para investigar
				
			   );
$params = array( 'summary' => true, 'limit'=>80);

$salida=(new User($platform_user_id))->getAdAccounts(  $fields,  $params);

	$salida->setDefaultUseImplicitFetch(true);
	$stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`ads_accounts` (`user_id`, `platform`, `app_id`,`platform_user_id`, `account_id`, `currency`,`name`,`status`, `metadata`) 
	VALUES (?,?,?,?,?,?,?,?,?) 	ON DUPLICATE KEY UPDATE    `name`= ?,  `status`= ?,`platform_user_id`=?,  `metadata`= ? ");
$stmt->bind_param("sssssssssssss", $userid, $platformid_facebook, $app_id, $platform_user_id, $ad_accountid, $currency,$name,  $status, $metadata,$name, $status, $platform_user_id, $metadata);
foreach($salida as $sitem) {
	$item=$sitem->getData();
	$ad_accountid=$item['id'];
	$name=$item['name'];
	$currency=$item['currency'];
		$status=$item['account_status'].'|'.$item['disable_reason'];
		$metadata=print_r($item, true);
		$stmt->execute();
}

//$stmt->close();

	/*******************************************************/
	
	$fields=array();
	$params = array( 'summary' => true, 'limit'=>80);
	$salida=(new User($platform_user_id))->getAccounts(  $fields,  $params) ;
	$salida->setDefaultUseImplicitFetch(true);
		
 
	$stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` (`user_id`, `platform`, `app_id`,`platform_user_id`, `id_en_platform`, `token`,`name`,`status`,`category`, `metadata`) 	VALUES (?,?,?,?,?,?,?,?,?,?) 	ON DUPLICATE KEY UPDATE    `name`= ?,  `status`= ?,`platform_user_id`=?,  `metadata`= ? ");
$stmt->bind_param("ssssssssssssss", $userid, $platformid_facebook, $app_id, $platform_user_id, $ad_accountid, $token ,$name,  $status,  $category, $metadata,$name, $status, $platform_user_id, $metadata);
foreach($salida as $sitem) {
	$item=$sitem->getData();
	 //print_r($item);
	 
	$ad_accountid=$item['id'];
	$name=$item['name'];
	$category=$item['category'];
	$token=$item['access_token'];
		$status=0;
		$metadata=print_r($item, true);
		$stmt->execute();
}

$stmt->close();
	
	
}
/**

$rowdata = Array
(
    [id] => 1
    [user_id] => 1
    [id_en_platform] => 
    [app_id] => 
    [ad_account] => 10
    [property_id] => 16
    [platform] => fac
    [campana_root] => 1
    [name] => Nombre de capaña
    [created_at] => 2019-01-01 00:00:00
    [updated_at] => 2019-01-01 00:00:00
    [start] => 2019-01-01 00:00:00
    [end] => 2020-01-01 00:00:00
    [budget] => 5000
    [ctr] => 
    [cpc] => 
    [clicks] => 
    [impression] => 
    [spent] => 
    [conversion] => 
    [cpm] => 
    [cpa] => 
    [customer_id] => 2
    [geo_include] => [{"id":"1659391","name":"Espa, Hordaland, Noruega"},{"id":"689416","name":"Manzanares El Real, Madrid, Spain, Comunidad de Madrid, España"}]
    [geo_exclude] => [{"id":"1696855","name":"Lima, Lima Region, Perú"}]
    [platform_placements] => ['mobile', 'desktop']
    [genders] => 1
    [ages] => [{"start":"20","end":"30"},{"start":"50","end":"60"}]
    [languages] => []
    [platform_properties_ids] => ['feed']
    [devices] => [1,3]
    [conectivity] => wifi
    [targeting_include] => [{"id":"6002971085794","name":"Teléfonos móviles"},{"id":"6003237743724","name":"Reflexión"}]
    [targeting_exclude] => [{"id":"6003261367957","name":"Fuerzas armadas de los Estados Unidos"},{"id":"6003287282400","name":"Mundo árabe"}]
    [platforma_budget] => 1000
    [creativities_ids] => [15,16]
    [objective] => 
    [bid_strategy] => 
    [adacc_app_id] => 2230398120518804
    [adacc_account_id] => act_1711974962384822
    [adacc_platform_user_id] => 10215767923927052
    [appid_access_token] => EAAfsiQeuZAJQBACysL6fUyHyyZCwSMbhzf8ShzBNIxsJT55dvg2N8ITEAmVh7dqH8bvZCFqCdq4TZAgzwZCMvFxfEEBESZCqP3vSL3Ycxug96qNCuHf67CgXx5P44f0GeeDOBYCWaUEOKYyUkku8lOmGFPGPjhclCS56Kb2VNnUAZDZD
    [appid_app_secret] => 7fdb89a242acff2678080d01b80bbba5
)

***/
function fb_creacampana($app_id, $app_secret,$access_token,$userid, $add_account_id,$rowdata) {
$api = Api::init($app_id, $app_secret ,$access_token);
$api->setLogger(new CurlLogger());
	$fields=array();
$params = array(
  'name' =>$userid.'|'.$rowdata['id'].':'. $rowdata['name'].' '.date('Y/m/d h:i:s a', time()) ,
  'objective' => 'REACH', //ver esto https://developers.facebook.com/docs/reference/ads-api/adcampaign#create
	//enum{APP_INSTALLS, BRAND_AWARENESS, CONVERSIONS, EVENT_RESPONSES, LEAD_GENERATION, LINK_CLICKS, LOCAL_AWARENESS, MESSAGES, OFFER_CLAIMS, PAGE_LIKES, POST_ENGAGEMENT, PRODUCT_CATALOG_SALES, REACH, VIDEO_VIEWS}
  'status' => 'PAUSED',
);
$campananueva=(new AdAccount($add_account_id))->createCampaign(  $fields,  $params)->exportAllData();
	
return $campananueva;	
}
/**
$rowdata= Array
(
    [id] => 7
    [user_id] => 1
    [id_en_platform] => 
    [platform] => fac
    [campana_root_id] => 1
    [campana_platform_id] => 1
    [ads_accounts] => 
    [name] => Nombre de capaña
    [created_at] => 2019-01-01 00:00:00
    [updated_at] => 2019-01-01 00:00:00
    [start] => 2019-01-01 00:00:00
    [end] => 2020-01-01 00:00:00
    [budget] => 5000
    [ctr] => 
    [cpc] => 
    [clicks] => 
    [impression] => 
    [spent] => 
    [conversion] => 
    [cpm] => 
    [cpa] => 
    [customer_id] => 2
    [geo_include] => [{"id":"1659391","name":"Espa, Hordaland, Noruega"}]
    [geo_exclude] => [{"id":"1696855","name":"Lima, Lima Region, Perú"}]
    [ad_account] => 10
    [property_id] => 
    [platform_placements] => mobile
    [gender] => 1
    [age] => [{"start":"20","end":"30"}]
    [language] => []
    [platform_propertie_id] => ['feed']
    [targeting_include] => [{"id":"6003237743724","name":"Reflexión"}]
    [targeting_exclude] => [{"id":"6003287282400","name":"Mundo árabe"}]
    [device] => 1
    [conectivity] => wifi
    [platforma_budget] => 1000
    [creativities_ids] => [15,16]
    [campana_id_en_platform] => 6139846553315
    [adacc_app_id] => 2230398120518804
    [adacc_account_id] => act_1711974962384822
    [adacc_platform_user_id] => 10215767923927052
    [appid_access_token] => EAAfsiQeuZAJQBACysL6fUyHyyZCwSMbhzf8ShzBNIxsJT55dvg2N8ITEAmVh7dqH8bvZCFqCdq4TZAgzwZCMvFxfEEBESZCqP3vSL3Ycxug96qNCuHf67CgXx5P44f0GeeDOBYCWaUEOKYyUkku8lOmGFPGPjhclCS56Kb2VNnUAZDZD
    [appid_app_secret] => 7fdb89a242acff2678080d01b80bbba5
)

**/
function facebook_adset_crea($app_id, $app_secret,$access_token,$userid, $add_account_id,$campaignid, $rowdata) {
		$api = Api::init($app_id, $app_secret ,$access_token);
$api->setLogger(new CurlLogger());
	$fields=array();
	/**
	$adset = new AdSet(null, $add_account_id);
$adset->setData(array(
  AdSetFields::NAME => $userid.'|'.$rowdata['campana_root_id'].'|adset '. $rowdata['id'],
  AdSetFields::OPTIMIZATION_GOAL => AdSetOptimizationGoalValues::REACH,
  AdSetFields::BILLING_EVENT => AdSetBillingEventValues::IMPRESSIONS,
  AdSetFields::BID_AMOUNT => 2,
  AdSetFields::DAILY_BUDGET => 1000,
  AdSetFields::CAMPAIGN_ID => $campaignid,
  AdSetFields::TARGETING => (new Targeting())->setData(array(
    TargetingFields::GEO_LOCATIONS => array(
      'countries' => array(
        'US',
      ),
    ),
    TargetingFields::USER_DEVICE => array(
      'Galaxy S6',
      'One m9',
    ),
    TargetingFields::USER_OS => array('android'),
  )),
));
	$adset->setData(		AdSetFields::TARGETING => (new Targeting())->setData(array(
  					TargetingFields::AGE_MIN => 18,
    				TargetingFields::AGE_MAX => 43
							))
		);			
return $adset->create(array(  AdSet::STATUS_PARAM_NAME => AdSet::STATUS_PAUSED));
**/	
 
$params = array(
  'name' => $userid.'|'.$rowdata['campana_root_id'].'|adset '. $rowdata['id'],
  'daily_budget' => '5000',
//  'start_time' => '2019-08-14T02:17:24-0700',
//  'end_time' => '2019-08-21T02:17:24-0700',
  'campaign_id' => $campaignid,
  'bid_amount' => '15',
'billing_event' => 'IMPRESSIONS',
  'optimization_goal' => 'REACH',
'status' => 'PAUSED'	,
'targeting' =>	array(
//	'age_min' => 20,'age_max' => 24,	
	//'behaviors' => array(array('id' => 6002714895372,'name' => 'All travelers')),
	//'genders' => array(1),	
	'geo_locations' => array('countries' => array('US')),
//							 ,'regions' => array(array('key' => '4081')),'cities' => array(array('key' => '777934','radius' => 10,'distance_unit' => 'mile'))),
//	'interests' => array(array('id' => '<adsInterestID>','name' => '<adsInterestName>')),
	//'life_events' => array(array('id' => 6002714398172,'name' => 'Newlywed (1 year)')),
//	'publisher_platforms' => array('facebook')
	)
	);
 //return (new AdAccount($add_account_id))->createAdSet(  $fields,  $params)->exportAllData();
	 
foreach (array_keys( $rowdata) as $item) {	
			//print_r($item);
switch($item){
	case 'age':		
		$tmpdata=json_decode($rowdata[$item])[0]; 
		$params['targeting']['age_min']= $tmpdata->start;
		$params['targeting']['age_max']= $tmpdata->end ;
		break;		
	case 'gender':		
		//print_r($rowdata[$item]);
		 $params['targeting']['genders']=array($rowdata[$item]);
		break;		
	case 'geo_include':		
		print_r($rowdata[$item]);
		break;		
	case 'platform_placements':				
		$params['targeting']['publisher_platforms']= array('facebook');
		$params['targeting']['device_platforms']=array($rowdata[$item] );
		break;	
	case 'platform_propertie_id':						
		echo '-$item--'.print_r($rowdata[$item], true);
		//	$params['targeting']['facebook_positions']=$rowdata[$item];
		break;			
}
	
	
}
//	print_r($params ) ;
	


	
	try {
//$salida=(new AdAccount($add_account_id))->createAdSet(  $fields,  $params)->getResponse();
$salida=(new AdAccount($add_account_id))->createAdSet(  $fields,  $params)->exportAllData();

		//asignarlo creas al adset $rowdata['creativities_ids']
	/**
$params = array(
  'name' => 'My Ad',
  'adset_id' => $adsetid,
  'creative' => array('creative_id' => '<adCreativeID>'),
  'status' => 'PAUSED',
);
	
return (new AdAccount($id))->createAd(  $fields,  $params)->exportAllData();
***/
		
} catch (Exception $e) {
  echo 'Messagsddddddddddde: ' . $e->getErrorUserMessage();
 // echo 'Messagsddddddddddde: ' . $e->getMessage();
		/**
		print_r($e );
		"error": {
            "message": "Invalid parameter",
            "type": "OAuthException",
            "code": 100,
            "error_subcode": 1487212,
            "is_transient": false,
            "error_user_title": "Missing Image",
            "error_user_msg": "Please specify an image to run with this ad.",
            "fbtrace_id": "AycaOOEuDbg"
}
***/
  $previousException = $e->getPrevious();
		//print_r($previousException );
  // Do some further processing on $previousException
  exit;
}
	
return $salida;

	
}
/****
$creadata= Array
(
    [id] => 16
    [id_en_platform] => 
    [user_id] => 1
    [title] => titulo 2
    [content] => texto
    [banner] => 12413
    [macrotag] => tag
    [type] => 
    [size] => 
    [printouts] => 
    [clicks] => 
    [ctr] => 
    [costs_per_click] => 
    [conversion_costs] => 
    [platform] => 
    [campana_root] => 0
    [campana_platform_id] => 1
    [atomo_id] => 0
    [description] => 
    [linkdescription] => 
    [calltoaction] => 
    [page_id_en_platform] => 126069094110167
    [campana_id_en_platform] => 6139846553315
    [adacc_app_id] => 2230398120518804
    [adacc_account_id] => act_1711974962384822
    [adacc_platform_user_id] => 10215767923927052
    [appid_access_token] => 
    [appid_app_secret] => 
)



***/
//https://developers.facebook.com/docs/marketing-api/reference/ad-creative
function facebook_ad_creatividad($app_id, $app_secret,$access_token,$userid, $add_account_id,$campaignid, $creadata) {
	/**
		$api = Api::init($app_id, $app_secret ,$access_token);
$api->setLogger(new CurlLogger());
	$fields=array();
$creative = new AdCreative(null, $add_account_id);
	switch($rowdata['type'])
		case 'img'	
$creative->setData(array(
AdCreativeFields::OBJECT_STORY_SPEC => array('page_id' => $rowdata['property_id'], 'photo_data' => array('url' => $rowdata['banner'], 'caption' =>  $rowdata['content']))
));

case 'post'
curl \  
-F 'message=Book your trip to Alaska, http://bit.ly/alaska'\  
-F 'source=@alaska.jpg'\
-F 'published=0'\
-F 'access_token=<PAGE_TOKEN>'\
https://graph.facebook.com/<PAGE_ID>/photos

// Then create the ad creative and reference the page post ID
curl \
-F "object_story_id=<PAGE_ID>_<POST_ID>" \
-F "access_token=<ADS_ACCESS_TOKEN>" \
"https://graph.facebook.com/act_<AD_ACCOUNT_ID>/adcreatives"



$creative->create();	
	case 'externallink'
$link_data = new AdCreativeLinkData();
$link_data->setData(array(
  AdCreativeLinkDataFields::MESSAGE => 'try it out',
  AdCreativeLinkDataFields::LINK => '<URL>',
  AdCreativeLinkDataFields::CALL_TO_ACTION => array(
    'type' => AdCreativeCallToActionTypeValues::SIGN_UP,
    'value' => array(
      'link' => '<URL>',
    ),
  ),
));

$object_story_spec = new AdCreativeObjectStorySpec();
$object_story_spec->setData(array(
  AdCreativeObjectStorySpecFields::PAGE_ID => <PAGE_ID>,
  AdCreativeObjectStorySpecFields::LINK_DATA => $link_data,
));

$creative = new AdCreative(null, 'act_<AD_ACCOUNT_ID>');
$creative->setData(array(
  AdCreativeFields::NAME => 'Sample Creative',
  AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec,
));

$creative->create();

case 'carrouselad'



$product1 = (new AdCreativeLinkDataChildAttachment())->setData(array(
  AdCreativeLinkDataChildAttachmentFields::LINK =>
    'https://www.link.com/product1',
  AdCreativeLinkDataChildAttachmentFields::NAME => 'Product 1',
  AdCreativeLinkDataChildAttachmentFields::DESCRIPTION => '$8.99',
  AdCreativeLinkDataChildAttachmentFields::IMAGE_HASH => '<IMAGE_HASH>',
  AdCreativeLinkDataChildAttachmentFields::VIDEO_ID => '<VIDEO_ID>',
));

$product2 = (new AdCreativeLinkDataChildAttachment())->setData(array(
  AdCreativeLinkDataChildAttachmentFields::LINK =>
    'https://www.link.com/product2',
  AdCreativeLinkDataChildAttachmentFields::NAME => 'Product 2',
  AdCreativeLinkDataChildAttachmentFields::DESCRIPTION => '$9.99',
  AdCreativeLinkDataChildAttachmentFields::IMAGE_HASH => '<IMAGE_HASH>',
  AdCreativeLinkDataChildAttachmentFields::VIDEO_ID => '<VIDEO_ID>',
));

$product3 = (new AdCreativeLinkDataChildAttachment())->setData(array(
  AdCreativeLinkDataChildAttachmentFields::LINK =>
    'https://www.link.com/product3',
  AdCreativeLinkDataChildAttachmentFields::NAME => 'Product 3',
  AdCreativeLinkDataChildAttachmentFields::DESCRIPTION => '$10.99',
  AdCreativeLinkDataChildAttachmentFields::IMAGE_HASH => '<IMAGE_HASH>',
));

$link_data = new AdCreativeLinkData();
$link_data->setData(array(
  AdCreativeLinkDataFields::LINK => '<URL>',
  AdCreativeLinkDataFields::CHILD_ATTACHMENTS => array(
    $product1, $product2, $product3,
  ),
));

$object_story_spec = new AdCreativeObjectStorySpec();
$object_story_spec->setData(array(
  AdCreativeObjectStorySpecFields::PAGE_ID => <PAGE_ID>,
  AdCreativeObjectStorySpecFields::LINK_DATA => $link_data,
));

$creative = new AdCreative(null, 'act_<AD_ACCOUNT_ID>');
$creative->setData(array(
  AdCreativeFields::NAME => 'Sample Creative',
  AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec,
));

$creative->create();



	
	case 'videopage'
	$video_data = new AdCreativeVideoData();
$video_data->setData(array(
  AdCreativeVideoDataFields::IMAGE_URL => '<THUMBNAIL_URL>',
  AdCreativeVideoDataFields::VIDEO_ID => <VIDEO_ID>,
  AdCreativeVideoDataFields::CALL_TO_ACTION => array(
    'type' => AdCreativeCallToActionTypeValues::LIKE_PAGE,
    'value' => array(
      'page' => <PAGE_ID>,
    ),
  ),
));

$object_story_spec = new AdCreativeObjectStorySpec();
$object_story_spec->setData(array(
  AdCreativeObjectStorySpecFields::PAGE_ID => <PAGE_ID>,
  AdCreativeObjectStorySpecFields::VIDEO_DATA => $video_data,
));

$creative = new AdCreative(null, 'act_<AD_ACCOUNT_ID>');

$creative->setData(array(
  AdCreativeFields::NAME => 'Sample Creative',
  AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec,
));


case 'promotedpost'

$creative->setData(array(
  AdCreativeFields::NAME => 'Sample Promoted Post',
  AdCreativeFields::OBJECT_STORY_ID => <POST_ID>,
));


endswitch;



***/
}
