<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require __DIR__ . '/../../vendor/autoload.php';
 

require 'configs/general.php';
require 'configs/twitter.php';
use Hborras\TwitterAdsSDK\TwitterAds;
 use Hborras\TwitterAdsSDK\TwitterAds\Account;
use Hborras\TwitterAdsSDK\TwitterAds\Campaign\Campaign;
use Hborras\TwitterAdsSDK\TwitterAds\Campaign\LineItem;
use Hborras\TwitterAdsSDK\TwitterAds\Enumerations;
use Hborras\TwitterAdsSDK\TwitterAds\Fields\AnalyticsFields;
use Hborras\TwitterAdsSDK\TwitterAdsException;

function twitter_stats_lineitem($userid, $accountw, $lineitemid_pl,$startDate,$endDate) {
	global $dbconn_stats,$userid, $platformid,$TWITTER_CONSUMER_KEY;
	$accountw->read();
 
$metricskeys=['metrics_engagement', 'metrics_conversion', 'metrics_video','metrics_costs','metrics_delivery'] ;
	
$metrics['metrics_delivery']=['impressions'=>0,'qualified_impressions'=>0,'clicks'=>0,'url_clicks'=>0,'media_views'=>0,'app_clicks'=>0];
$metrics['metrics_engagement']=['tweets_send'=>0,'media_engagements'=>0,'follows'=>0,'retweets'=>0,'unfollows'=>0,'likes'=>0,'unfollows'=>0,'engagements'=>0,'card_engagements'=>0,'replies'=>0,'billed_engagements'=>0,'carousel_swipes'=>0];
$metrics['metrics_conversion']=['conversion_purchases'=>0,'conversion_sign_ups'=>0];
 
$metrics['metrics_video']=['video_views_50'=>0,'video_views_75'=>0,'video_3s100pct_views'=>0,'video_cta_clicks'=>0,'video_content_starts'=>0,'video_views_25'=>0,'video_views_100'=>0,'video_6s_views'=>0,'video_total_views'=>0,'video_mrc_views'=>0];
$metrics['metrics_costs']=['billed_charge_local_micro'=>0,'billed_engagements'=>0];
 
   $dates = dateRanges($startDate, $endDate, 'P1D'); 
		  foreach ($dates as $date) {
			  	  echo "\t\t\t Start: " . $date[0]->format('Y-m-d H:i:s') . PHP_EOL; 
                    echo "\t\t\t End: " . $date[0]->format('Y-m-d H:i:s') . PHP_EOL; 
			  if (is_string($lineitemid_pl )) { $lineitemid_pl=[$lineitemid_pl];   }
				$stats=	$accountw->all_stats($lineitemid_pl, [  
				 	TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_BILLING,
                       TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_VIDEO,
                     TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_MEDIA,
                        TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_WEB_CONVERSIONS,
                       TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_MOBILE_CONVERSION,
                        TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_ENGAGEMENT
						], 
						[
				 		 AnalyticsFields::ENTITY => AnalyticsFields::LINE_ITEM ,    
                        TwitterAds\Fields\AnalyticsFields::START_TIME => $date[0],
                        AnalyticsFields::END_TIME =>$date[1],
                        AnalyticsFields::GRANULARITY => Enumerations::GRANULARITY_TOTAL
						], false);
			//  print_r( $stats);
			//  print_r( $stats[0]->id_data[0]->metrics );
			  $property_name='impressions';
			//  print_r( $stats[0]->id_data[0]->metrics->{$property_name}[0]  );
//$item=[$stats[0]->id   ]	;
			  
			  foreach ($metricskeys as $item) {				  
				 //echo '---'. print_r($item, true).PHP_EOL;
				 foreach (array_keys($metrics[$item]) as $property_name) {
			//	  echo '--$property_name-'. print_r($property_name, true).PHP_EOL;
			//	  echo '--$stats--'.  isset($stats[0]->id_data[0]->metrics->{$property_name})   .PHP_EOL;
					 
					  $metrics[$item][$property_name]=isset($stats[0]->id_data[0]->metrics->{$property_name}) ? is_array( $stats[0]->id_data[0]->metrics->{$property_name}) ? $stats[0]->id_data[0]->metrics->{$property_name}[0]  : null : null ;
					 					 
				   }
 				}
			  echo '--salida--'. print_r($metrics, true).PHP_EOL;
print_r( $dbconn_stats);
			  
$stmt = $dbconn_stats->prepare("INSERT INTO platform_atomo_day (user_id,platformid,idenplatform,dia,unico, metrics_delivery,metrics_costs,metrics_engagement,metrics_video) values (?,?,?,?,?,?,?,?,?)  ");
//			    printf("Error: %s.\n", $stmt->error);	
		  
$stmt->bind_param("iisssssss",...[ $userid, $platformid, $stats[0]->id,$date[0]->format('Y-m-d') , md5($lineitemid_pl. $date[0]->format('Y-m-d')) , 
								json_encode( $metrics['metrics_delivery'] ),  json_encode( $metrics['metrics_costs'] ) ,	json_encode( $metrics['metrics_engagement'] ) ,
								  json_encode( $metrics['metrics_video'] ) 						  
							  ]) ;
   printf("Error: %s.\n", $stmt->error);	
   $salida=$stmt->execute();		
			  print_r( $salida);
			     printf("Error: %s.\n", $stmt->error);	
			  exit;
	  }
	
   }
$userid = 1;

 

 $stmt = $dbconn->prepare("select * from  `app_thesoci_9c37`.`auths_user_platform` where platform=? and `user_id`= ? limit 1");
		$stmt->bind_param("is",$platformid, $userid);
   $stmt->execute();
  //$stmt->bind_result($userauth);
  $resultado = $stmt->get_result();
     $userauth = $resultado->fetch_array(MYSQLI_ASSOC);
 
$user_credentials=json_decode($userauth['retorno']);
 //  print_r($user_credentials)	;
    $stmt->close();	  
 
//$connection = new TwitterOAuth($settings['consumer_key'], $settings['consumer_secret'], $user_credentials->oauth_token, $user_credentials->oauth_token_secret);
 
 $twitterapi = TwitterAds::init($TWITTER_CONSUMER_KEY, $TWITTER_CONSUMER_SECRET, $user_credentials->oauth_token, $user_credentials->oauth_token_secret);
 
$accountw = new Account('18ce550icv8');
//$accountw->setDefaultUseImplicitFetch(true);


// twitter_stats_lineitem($userid, $accountw, ['g9k90'],new DateTime('today -3 days'),new DateTime('today'));
// twitter_stats_lineitem($userid, $accountw, ['g9k90'],new DateTime('today -3 days'),new DateTime('today'));

 
exit;



//$content = $connection->get("5/accounts");

 $accounts = $twitterapi->getAccounts();
$accounts->setDefaultUseImplicitFetch(true);
 //print_r($accounts);
  echo '**********************'.PHP_EOL;
 foreach ($accounts as $account) {
	 $accountmeta=[];
	 $accountmeta['getId']= $account->getId()  ;
	 $accountmeta['getName']= $account->getName()  ;
	 $accountmeta['getApprovalStatus']= $account->getApprovalStatus()  ;
	 $accountmeta['getBusinessId']= $account->getBusinessId()  ;
	 $accountmeta['getBusinessId']= $account->getBusinessId()  ;
	 $accountmeta['getBusinessName']= $account->getBusinessName()  ;
	 $accountmeta['getIndustryType']= $account->getIndustryType()  ;
	 
$topaccount=$account;
	 /*
	 
	 `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NULL DEFAULT '0',
	`platform` CHAR(3) NULL DEFAULT '0',
	`user_id` INT(11) NULL DEFAULT 0,
	`app_id` VARCHAR(50) NULL DEFAULT '0',
	`platform_user_id` VARCHAR(50) NULL DEFAULT '0',
	`account_id` VARCHAR(128) NULL DEFAULT '0',
	`currency` CHAR(3) NULL DEFAULT '0',
	`isdefault` TINYINT(4) NULL DEFAULT 0,
	`metadata` LONGTEXT NULL DEFAULT '0' COLLATE 'utf8mb4_bin',
	`status` VARCHAR(8) NULL DEFAULT '0',
	 
	 **/
	$metadata=json_encode($accountmeta );
	$stmt = $dbconn->prepare("INSERT INTO ads_accounts (user_id,platform,app_id,account_id,name,platform_user_id,status,metadata) values (?,?,?,?,?,?,?,?) on duplicate key update status=?, metadata=?  ");
	$stmt->bind_param("iissssssss", $userid, $platformid,$TWITTER_CONSUMER_KEY, $accountmeta['getId'] , $accountmeta['getName'],$userauth['platform_user_id'],  $accountmeta['getApprovalStatus'], $metadata, $accountmeta['getApprovalStatus'] ,$metadata );
	$salida=$stmt->execute();

	$account = new Account($accountmeta['getId'] );
	$account->read();
	$campaigns = $account->getCampaigns('', [TwitterAds\Fields\CampaignFields::COUNT => 100]);
	$campaigns->setUseImplicitFetch(false);
	$campaignsData = [];

	$campaignLineitems=[];	 
	foreach ($campaigns as $campaign) {
		$campaignsData[] = $campaign;
	 	 

		echo '**********************'.PHP_EOL;
			if ('danbg'!= $campaign->getId()) continue; 
		echo print_r( $campaign->getId(), true).PHP_EOL;
			
		echo print_r( $campaign->getName(), true).PHP_EOL;
		echo print_r( $campaign->getFundingInstrumentId(), true).PHP_EOL;
		echo print_r( $campaign->getReasonsNotServable(), true).PHP_EOL;
		//echo print_r( $campaign->getProperties(), true).PHP_EOL;
		echo print_r( $campaign->getTotalBudgetAmountLocalMicro(), true).PHP_EOL;
		echo print_r( $campaign->getStandardDelivery(), true).PHP_EOL;
		echo print_r( $campaign->getDailyBudgetAmountLocalMicro(), true).PHP_EOL;
		

		$stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (user_id, platform, app_id, name, id_en_platform, budget, status, currency, metadata, ad_account  ) values (?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id 
		FROM ads_accounts WHERE ads_accounts.account_id =? and ads_accounts.user_id=?) ) on duplicate key update status=?, metadata=?  ");

		printf("Error: %s.\n", $stmt->error);
		$i = 1;
		$j = 1;
		$l = 1;	 
		
		$stmtcampana->bind_param("iissssssssiss",...[ $userid, $platformid,$TWITTER_CONSUMER_KEY,$campaign->getName(), $campaign->getId(),$campaign->getDailyBudgetAmountLocalMicro(), unificastatus('campana', $platformid,  $campaign->getEntityStatus(), ['estado'=> $campaign->getEntityStatus(), 'otros'=> $campaign->getReasonsNotServable() , 'record'=>$campaign ] ) , $campaign->getCurrency(), json_encode($campaign ),$account->getId(),$userid,  unificastatus('campana', $platformid,  $campaign->getEntityStatus(), ['estado'=> $campaign->getEntityStatus(), 'otros'=> $campaign->getReasonsNotServable() , 'record'=>$campaign ] )   , json_encode($campaign ) ]);
   		$stmtcampana->execute();	 
	  	printf("Error: %s.\n", $stmtcampana->error);
	 
	 	$campaignLineitems[$campaign->getId()] = $campaign->getLineItems([TwitterAds\Fields\LineItemFields::COUNT => 200]);
 
    	foreach ( $campaignLineitems[$campaign->getId()] as $lineItem) {


			$stmt_atomo = $dbconn->prepare("INSERT INTO campaigns_platform_atomo (user_id, platform, app_id, id_en_platform, name, objetivo, platform_placements, status, chargeby, bid_unit, puja_valor, optimizacion, bid_type, product_type, budget_total, targeting_include, metadata, campana_platform_id, ads_accounts)
			values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
			(SELECT campaigns_platform.id FROM campaigns_platform WHERE campaigns_platform.id_en_platform=? and   campaigns_platform.user_id=? ), 
			(SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id =? and ads_accounts.user_id=?) ) on duplicate key update  metadata=?  ");
		 	 
			print_r( $account->getId() );
			print_r( $campaign->getId() );
			printf("Error: %s.\n", $stmt_atomo->error);	
		 
			$stmt_atomo->bind_param("iissssssssssssssssssss",...[ $userid, $platformid,$TWITTER_CONSUMER_KEY,$lineItem->getId(),$lineItem->getName(), $lineItem->getObjective(), json_encode($lineItem->getPlacements()), $lineItem->getEntityStatus(), $lineItem->getChargeBy(), $lineItem->getBidUnit(),($lineItem->getBidAmountLocalMicro() / 1000000), 
			$lineItem->getOptimization(), $lineItem->getBidType(),$lineItem->getProductType(),($lineItem->getTotalBudgetAmountLocalMicro()/ 1000000), json_encode($lineItem->getTargetingCriteria()), json_encode($lineItem ), $campaign->getId(),  $userid ,$account->getId() ,$userid,json_encode($lineItem )]);
			$stmt_atomo->execute();	  
			
			printf("Error: %s.\n", $stmt_atomo->error);		
		   
			echo "\t" . $j . ': ' . $lineItem->getId() . ' ' . $lineItem->getName() . ' ' . PHP_EOL;
			echo "\t\tBid: " . ($lineItem->getBidAmountLocalMicro() / 1000000) . PHP_EOL;
			echo "\t\tObjective: " . $lineItem->getObjective() . PHP_EOL;
			echo "\t\tCharge By: " . $lineItem->getChargeBy() . PHP_EOL;
			echo "\t\tBid Unit: " . $lineItem->getBidUnit() . PHP_EOL;
			echo "\t\tOptimization: " . $lineItem->getOptimization() . PHP_EOL;
			echo "\t\tBid Type: " . $lineItem->getBidType() . PHP_EOL;
			echo "\t\getProductType: " . print_r($lineItem->getProductType(), true) . PHP_EOL;
			echo "\t\tgetPlacements: " . print_r($lineItem->getPlacements(), true) . PHP_EOL;
			echo "\t\t getEntityStatus: " . print_r($lineItem->getEntityStatus(), true) . PHP_EOL;
			echo "\t\t getIncludeSentiment: " . print_r($lineItem->getIncludeSentiment(), true) . PHP_EOL;
			echo "\t\t getAdvertiserDomain: " . print_r($lineItem->getAdvertiserDomain(), true) . PHP_EOL;
			echo "\t\t getAdvertiserUserId: " . print_r($lineItem->getAdvertiserUserId(), true) . PHP_EOL;
			
			// echo "\t\t getPromotedTweets: " . print_r($lineItem->getPromotedTweets(), true) . PHP_EOL;
			// echo "\t\t DATES: " . print_r($dates, true) . PHP_EOL;
		 
 			$startDate = new DateTime($campaign->getStartTime()->format('Y-m-d 00:00:00'));
	 
            $endDate = new DateTime('now');
            $endDate = new DateTime('now - 1 day');
	
            $dates = dateRanges($startDate, $endDate, 'P1D');
	
		  	foreach ($dates as $date) {
 
				echo "\t\t\t Start: " . $date[0]->format('Y-m-d H:i:s') . PHP_EOL;
				echo "\t\t\t End: " .   $date[1]->format('Y-m-d H:i:s') . PHP_EOL;
	
				$stats = $lineItem->stats([ TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_BILLING,
							TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_VIDEO,
							TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_MEDIA,
							TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_WEB_CONVERSIONS,
							// TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_MOBILE_CONVERSION,
							TwitterAds\Fields\AnalyticsFields::METRIC_GROUPS_ENGAGEMENT
							], [
						TwitterAds\Fields\AnalyticsFields::START_TIME => $date[0], 
						AnalyticsFields::END_TIME => $date[1],
							AnalyticsFields::GRANULARITY => Enumerations::GRANULARITY_TOTAL,
						// AnalyticsFields::PLACEMENT => Enumerations::PLACEMENT_ALL_ON_TWITTER
						], false );
			//  echo "\t\t stats: " . print_r(		$stats		, true) . PHP_EOL; 
		
				break;
	    	}
				print_r('/////////////////////////////'.PHP_EOL);
			twitter_stats_lineitem($userid, $accountw, ['g9k90'],new DateTime('today -5 days'),new DateTime('today'));
			exit;
		 
        	echo "\t\t getCategories: " . print_r($lineItem->getCategories(), true) . PHP_EOL;
        	$targetingCriterias = $lineItem->getTargetingCriteria();
        	/** @var TwitterAds\Campaign\TargetingCriteria $targetingCriteria */
        	
			foreach ($targetingCriterias as $targetingCriteria) {
            	echo "\t\t" . $l . ': ' . $targetingCriteria->getId() . ' ' . $targetingCriteria->getName() . ' ' .
                $targetingCriteria->getTargetingType() . ' ' . $targetingCriteria->getTargetingValue() . PHP_EOL;
            	$l++;
        	}
	 	}
	}
}

//https://ads-api.twitter.com/6/accounts


?>