<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set('UTC');
require 'configs/general.php';
require 'configs/googleanalytics.php';
require 'jr_google_realtime.php';
require __DIR__ . '/../../vendor/autoload.php';

$KEY_FILE_LOCATION = 'configs/client_secret_30164914022-gln0hfnp66r7im2sccenlnm3m1j4ud5i.apps.googleusercontent.com.json';

//$REDIRECT_URI = 'https://sunpanel.tsapx.net/newsreader/newsretrieve-offline.php';

//$analytics = initializeAnalytics($KEY_FILE_LOCATION);

$altanueva = false;

$userid = get_user_id();

$client = new Google_Client();
$client->setApplicationName("Hello Analytics Reporting");
// $client->setAccessType('offline');
$client->setIncludeGrantedScopes(true);
$client->setAuthConfig($KEY_FILE_LOCATION);
$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly', 'https://www.googleapis.com/auth/userinfo.email']);
$client->setRedirectUri($googleanalytics_redirect_uri);
$client->setAccessType('offline');
$client->setApprovalPrompt('force');

$sql = "SELECT * FROM auths_user_platform WHERE user_id='{$userid}' AND platform='{$platformid}' AND app_id='{$googleanalytics_appid}' limit 1";

$result = $dbconn->query($sql);

$token = null;
if ($result) {
    $row = $result->fetch_assoc();
}

if (isset($_GET['code']) && !empty($_GET['code'])) {
    try {
        // Exchange the one-time authorization code for an access token
        $retorno = $client->fetchAccessTokenWithAuthCode($_GET['code']);

        if (isset($retorno['access_token'])) {
            $google_app_secret = null;
            //   $accessToken_value =$retorno->getValue()  ;

            $result = json_encode($retorno);
            $expiretime = time() + $retorno['expires_in'];

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://www.googleapis.com/oauth2/v1/userinfo?alt=json",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer " . $retorno['access_token'],
                    "cache-control: no-cache",
                ),
            ));

            $response = json_decode(curl_exec($curl));
            $err = curl_error($curl);

            curl_close($curl);

            $stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`auths_user_platform` (`user_id`, `platform`, `app_id`, `app_secret`, `platform_user_id`, `platform_email`, `access_token`,`expiretime`, `retorno`) VALUES (?,?,?,?,?,?,?,?,?)    ON DUPLICATE KEY UPDATE	activa = 'Y', `expiretime`= ?,  `access_token`= ?,`platform_user_id`=?,  `retorno`= ?, `platform_email` = ? ");
            echo $stmt->bind_param("ssssssssssssss", ...[$userid, $platformid_googleanalytics, $googleanalytics_appid, $google_app_secret, $response->id, $response->email, $retorno['access_token'], $expiretime, $result, time() + (3600 * 59), $retorno['access_token'], $response->id, $result, $response->email]);

            $stmt->execute();
            exec('php /home/app.thesocialaudience.com/public_html/www/crons/auth_account_update.php ' . $stmt->insert_id);

            $stmt->close();

            // lanzar esto por ssh inicializacion_google($userid);
            shell_exec("php " . __DIR__ . "/jr_google_realtime.php inicializacion_google {$userid} &> /dev/null &");

            header('Location: https://app.thesocialaudience.com/connections/success');

            exit;
        }
    } catch (\Google_Service_Exception $e) {
        print_r($e);
    }
}

if (!isset($_SESSION['accessToken'])) {

    //    $token = @file_get_contents($TOKEN_FILE);

    if ($token == null) {
        // Generate a URL to request access from Google's OAuth 2.0 server:
        $authUrl = $client->createAuthUrl();

        // Redirect the user to Google's OAuth server
        header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));
        exit();
    } else {
        $_SESSION['accessToken'] = json_decode($token, true);
    }
}

$client->setAccessToken($_SESSION['accessToken']);

if ($client->isAccessTokenExpired()) {

    // the new access token comes with a refresh token as well
    $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());

    file_put_contents($TOKEN_FILE, json_encode($client->getAccessToken()));
}
$service = new Google_Service_Oauth2($client);

var_dump($service->userinfo);
$analytics = new Google_Service_Analytics($client);
$accounts = $analytics->management_accounts->listManagementAccounts();
echo 'abc';
//var_dump ( $analytics);
if ($accounts->username != null && $altanueva) {
    file_put_contents('token_' . $accounts->username, json_encode($accessToken));
    file_put_contents('req' . $accounts->username, json_encode($_GET));
    header('Location: ' . filter_var($REDIRECT_URI, FILTER_SANITIZE_URL));
}
exit;

/**


$accounts = $analytics->management_accounts->listManagementAccounts();
print_r($accounts[3]->getId());

$properties = $analytics->management_webproperties
->listManagementWebproperties(123399689);
//print_r($properties);

$profiles = $analytics->management_profiles
->listManagementProfiles($accounts[3]->getId(), $properties[0]->getId());
print_r($profiles);
exit;
 **/
/**
$fichero=' $paginas=[]; '.PHP_EOL;
foreach($profiles as $profile )  {
print_r($profile);
try {
$resultados = getResults($analytics, $profile);
$fichero.=mapearesultados($resultados, $profile, $mapas);
} catch(Error $e) {

print_r($e);
}
}
 ***/

function initializeAnalytics($KEY_FILE_LOCATION)
{
    global $googleanalytics_redirect_uri;
    // Creates and returns the Analytics Reporting service object.

    // Use the developers console and download your service account
    // credentials in JSON format. Place them in this directory or
    // change the key file location if necessary.

    // Create and configure a new client object.
    $client = new Google_Client();
    $client->setApplicationName("Hello Analytics Reporting");
    $client->setAccessType('offline');
    $client->setIncludeGrantedScopes(true);
    $client->setAuthConfig($KEY_FILE_LOCATION);
    $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
    $client->setRedirectUri($googleanalytics_redirect_uri);
    $analytics = new Google_Service_Analytics($client);

    return $analytics;

}
