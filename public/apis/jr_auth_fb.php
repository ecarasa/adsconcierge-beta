<?php
//https://developers.facebook.com/apps/2230398120518804/app-review/my-permissions/?business_id=1679040252344960
//https://developers.facebook.com/docs/marketing-apis#newapp
//https://github.com/facebook/facebook-php-business-sdk?fbclid=IwAR2IUIRresFa2PgNUxDwT_Wx3T5re8dDdZB_kJnI4Xgib32gLcdd_ytI7jc

if (!session_id()) {
    session_start();
}

//https://app.thesocialaudience.com/apis/callback?code=AQC1gyMBPIWTKr2-yPun81FGlApGiOHQWiahaNIq2mIIgZlz-6H3YjxJWA7tFAIEWYxltpLkH2uzuk6lhI5OhF8qwGeEBX6QMyzrADgO5L3JptyQw8mV3BB6HmthQDPWDb9DLPK3VjLndno1jVMx8SidycL1FzT05yd0yjU23xg30yx8YuLn4ATyyVqZkgdp8QNkZji2Q81AVvJlKm41X409K3ndGrXsuVzKlS8RXks4BRlQz9RHTfuCFDwl8sb8zWr52Zeu0JsEJNgnU9XcF3hKr1boly3WM_QmlpN1BqTs320ClU5K6SK5iKRrSL-EYsk&state=76e24b14cfd74267b7286f6b5805f404#_=_

require 'configs/general.php';
require 'configs/facebook.php';

require __DIR__ . '/../../vendor/autoload.php';


$fb = new Facebook\Facebook(['app_id' => $app_id, 'app_secret' => $app_secret, 'default_graph_version' => 'v4.0']);
$helper = $fb->getRedirectLoginHelper();

$permissions = ['email', 'leads_retrieval', 'pages_read_engagement','pages_read_user_content', 'leads_retrieval', 'pages_manage_ads', 'instagram_manage_comments',   'read_insights', 'ads_management', 'instagram_manage_insights']; // Optional permissions
//$loginUrl = $helper->getLoginUrl('https://app.thesocialaudience.com/apis/jr_auth_fb.php', $permissions);
//$loginUrl = $helper->getLoginUrl('https://pre.thesocialaudience.com/apis/jr_auth_fb.php', $permissions);
$loginUrl = $helper->getLoginUrl('https://pre.adsconcierge.com/callback/facebook', $permissions);

try {
    $accessToken = $helper->getAccessToken();
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    // exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    // exit;
}

if (!isset($accessToken)) {
    if ($helper->getError()) {
        //   header('HTTP/1.0 401 Unauthorized');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
    } else {
        //   header('HTTP/1.0 400 Bad Request');
        header('Location: ' . $loginUrl);
        echo '<a href="' . htmlspecialchars($loginUrl) . '" target="_blank">Log in with Facebook!</a>';
        
    }
    exit;
}


$accessToken_value = $accessToken->getValue();
// The OAuth 2.0 client handler helps us manage access tokens
$oAuth2Client = $fb->getOAuth2Client();

// Get the access token metadata from /debug_token
$tokenMetadata = $oAuth2Client->debugToken($accessToken);
//$tokenMetadata;

// Validation (these will throw FacebookSDKException's when they fail)
$tokenMetadata->validateAppId($app_id); // Replace {app-id} with your app id
// If you know the user ID this access token belongs to, you can validate it here
//$tokenMetadata->validateUserId('123');
$tokenMetadata->validateExpiration();

if (!$accessToken->isLongLived()) {
    // Exchanges a short-lived access token for a long-lived one
    try {
        $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
        exit;
    }
}

$platform_user_id = $tokenMetadata->getUserId();
$userid = get_user_id();
$accessToken_value = $accessToken->getValue();

$retorno = print_r($tokenMetadata, true);

$email = json_decode(file_get_contents("https://graph.facebook.com/v7.0/me?fields=email&access_token=" . $accessToken_value))->email;

$stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`auths_user_platform` (`user_id`, `platform`, `app_id`, `app_secret`,`platform_user_id`, `platform_email`, `access_token`, `retorno`, `expiretime`) VALUES (?,?,?,?,?,?,?,?,?) 
                            ON DUPLICATE KEY UPDATE activa = 'Y', `access_token`= ?, `platform_user_id`=?, `platform_email`=?, `retorno`= ?, `expiretime`= ? ");

$stmt->bind_param("ssssssssssssss", ...[$userid, $platformid_facebook, $app_id, $app_secret, $platform_user_id, $email, $accessToken_value, $retorno, time() + (3600 * 59), $accessToken_value, $platform_user_id, $email, $retorno, time() + (3600 * 59)]);

$stmt->execute();

// Obtenemos adsaccounts 
exec('php /home/app.thesocialaudience.com/public_html/www/crons/auth_account_update.php ' . $stmt->insert_id);


// aca viene lo nuevo
// revisamos en bbdd que no tengamos ninguna task de este tipo para este usuario y la lanzamos en la nube
$data = array( 'subject_id'=> $stmt->insert_id, 'type' => 'auth_callback' );
event(new TaskCreate($data));

/* $dbconn="INSERT INTO `app_thesoci_9c37`.`gcloud_tasks` ( `user_id`, `auth_id`, `google_client_id`, `date`, `interval`, `action`, `json`, `task_id`) VALUES (?,?,?,?,?,?,?,?,?);";
$stmt->bind_param("sssssssss", ...[$userid, $stmt->insert_id, $ggoogleID , 'now()', '6H', 'retrieve_all', $payload, $taskid]);
$stmt->execute(); */

$stmt->close();

header('Location: https://app.thesocialaudience.com/connections/success');
