<?php

require 'configs/general.php';

require __DIR__ . '/../../vendor/autoload.php';

use Abraham\TwitterOAuth\TwitterOAuth;

$id = (is_null($_POST["id"]))? "" : $_POST["id"];

if($id != ""){
    
    $query = "SELECT platform, app_id, app_secret, platform_user_id, access_token, refresh_token 
                      FROM auths_user_platform WHERE id = ". $id;
    if ($result = $dbconn->query($query)){
        $obj = $result->fetch_object();
        $result->free_result();
    }
    
    switch ($obj->platform){
        case "FACEBOOK":            
        case "FBINSIGHTS":
            require 'configs/facebook.php';
            
            
            $url = "https://graph.facebook.com/v7.0/oauth/client_code?client_id=".$app_id."&client_secret=".$app_secret."&access_token=".$obj->access_token."&redirect_uri=".$redirect_uri;
            $result = json_decode(file_get_contents($url));
            
            if($result->code != ""){
                
                $url = "https://graph.facebook.com/v7.0/oauth/access_token?code=".$result->code."&client_id=".$app_id."&redirect_uri=".$redirect_uri;
                $result = json_decode(file_get_contents($url));
                
                if($result->access_token != ""){
                    $email = json_decode(file_get_contents("https://graph.facebook.com/v7.0/me?fields=email&access_token=".$accessToken_value))->email;


                    $retorno = print_r($result, true);
                    $stmt = $dbconn->prepare("UPDATE `app_thesoci_9c37`.`auths_user_platform` 
                                SET `access_token`= ?, `retorno`= ?, `expiretime`= ?
                                WHERE id = ?");
                    $stmt->bind_param("ssss", ...[$result->access_token, $retorno, time()+(3600*59), $id]);        
                    
                    $result = $stmt->execute();
                    $stmt->close();       
                    
                    if($result){
                        $response = array(
                            "result" => true,
                            "message" => "OK refresh ".$obj->platform
                        );
                    }else{
                        $response = array(
                            "result" => false,
                            "redirect" => $redirect_uri,
                            "message" => "Error save token ".$obj->platform
                        );
                    }
                    
                }else{
                    $response = array(
                        "result" => false,
                        "redirect" => $redirect_uri,
                        "message" => "Error get access token in refresh ".$obj->platform
                    );
                }
            }else{
                $response = array(
                    "result" => false,
                    "redirect" => $redirect_uri,
                    "message" => "Error get code in refresh ".$obj->platform
                );
            }            
            break;
        case "TWITTER":
            
            require 'configs/twitter.php';

            $connection = new TwitterOAuth($TWITTER_CONSUMER_KEY, $TWITTER_CONSUMER_SECRET, $obj->app_id, $obj->access_token);
            $content = $connection->get("account/verify_credentials");
            
            if($content->id == $obj->platform_user_id){
                $stmt = $dbconn->prepare("UPDATE `app_thesoci_9c37`.`auths_user_platform` 
                                SET `expiretime`= ?
                                WHERE id = ?");
                $stmt->bind_param("ss", ...[time()+(3600*59), $id]);        

                $result = $stmt->execute();
                $stmt->close();       
                
                if($result){
                    $response = array(
                        "result" => true,
                        "message" => "OK refresh ".$obj->platform
                    );
                }else{
                    $response = array(
                        "result" => false,
                        "redirect" => $redirect_uri,
                        "message" => "Error save token ".$obj->platform
                    );
                }                
            }else{
                $response = array(
                    "result" => false,
                    "redirect" => $twitter_redirect_uri,
                    "message" => "Error get access token in refresh ".$obj->platform
                );
            }
            break;
        case "LINKEDIN":
            //TODO LINKEDIN
            break;
        case "GANALYTICS":
            //TODO GOOGLE
            break;
        case "SNAPCHAT":
            //TODO SNAPCHAT
            break;
        default:
            $response = array(
                "result" => false,
                "message" => "Plataform not found"
            );
            break;
    }
    
}else{
    $response = array(
        "result" => false,
        "message" => "Id not found"
    );
}


echo json_encode($response);