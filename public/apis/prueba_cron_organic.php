<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once '../../crons/helper/config.php';
require_once  __DIR__ .'/../../crons/helper/db.php';
require __DIR__ . '/../../vendor/autoload.php';
require 'configs/googleanalytics.php';

$db = new db();


/*
$analytics = inicializacion_google(6);
$accounts = $analytics->management_accounts->listManagementAccounts();
foreach ($accounts as $value_accounts) {
  echo $value_accounts->getId()."<br><br><br>";

  $properties = $analytics->management_webproperties->listManagementWebproperties($value_accounts->getId());

  foreach ($properties as $value_properties) {

    $profiles = $analytics->management_profiles->listManagementProfiles($value_accounts->getId(), $value_properties->getId());

    foreach ($profiles as $value_profiles) {
      $results = getResults($analytics, 39922496);

      if($results->totalResults > 0){
        echo $value_accounts->getId();echo "<br>";
        echo $value_properties->getId();echo "<br>";
        echo "<pre>";
        print_r($value_properties);
        echo "<br><br><br>";
        echo $value_profiles->getId();echo "<br>";
        echo "<pre>";
        print_r($value_profiles);
        echo "<br><br><br>";
      }
      print_r($results->totalResults);
      echo "<br>";echo "<br>";


    }

  }


}
die();
*/

function inicializacion_google($userid) {
  Global $db,$platformid,$KEY_FILE_LOCATION,$googleanalytics_redirect_uri;

    $sql = "SELECT * FROM app_thesoci_9c37.auths_user_platform WHERE user_id='{$userid}' AND platform='{$platformid}'  limit 1";
    $result = $db->query($sql);
    $row= $result->fetch_assoc();
  	$userid=$row['user_id'];
  	$app_id=$row['app_id'];
  	$platformid=$row['platform'];
    $access_token= json_decode( $row['retorno'], true);

    $client = new Google_Client();
    $client->setApplicationName("Hello Analytics Reporting");
    $client->setAuthConfig( $KEY_FILE_LOCATION );
    $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly', 'https://www.googleapis.com/auth/userinfo.email']);
    $client->setRedirectUri($googleanalytics_redirect_uri);
  	$client->setAccessType('offline');
    $client->setApprovalPrompt('force');
  	$client->setAccessToken($access_token);


  	if ($client->isAccessTokenExpired()) {

  	   ECHO 'TOKEN EXPIRED';

       $refreshTokenSaved = $client->getRefreshToken();

       $client->fetchAccessTokenWithRefreshToken($refreshTokenSaved);

  		 $accessTokenUpdated=$client->getAccessToken();
       $accessTokenUpdated['refresh_token'] = $refreshTokenSaved;
  		 $result= json_encode($accessTokenUpdated);

  		 $stmt = $db->query("UPDATE `app_thesoci_9c37`.`auths_user_platform`
                  SET access_token = '".$db->real_escape_string($accessTokenUpdated['access_token'])."',
                  retorno = '".$db->real_escape_string($result)."'
                  WHERE  user_id = ".$userid." AND platform = '".$platformid."' ");

       $accessToken = $refreshTokenSaved;
       $client->setAccessToken($accessTokenUpdated);
    }

    $analytics = new Google_Service_Analytics($client);

    return $analytics;
}

/*
$sql = "SELECT * FROM app_thesoci_9c37.organic_settings where id = 1";

$jobs = $db->getRecordSet($sql);
  echo $arg_json = json_encode($jobs);

die();*/

/*
$client = new Google_Client();
$client->setApplicationName("Hello Analytics Reporting");
$client->setAuthConfig( $KEY_FILE_LOCATION );

//$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly', 'https://www.googleapis.com/auth/userinfo.email']);
$client->setAccessType('offline');
$client->setAccessToken("ya29.a0AfH6SMBfyRo8VWxsJpVmUJbivDLlug3Ow_Alt2ybHy9hjiT-5Izb-7ZB4-tSiQ9XrpkLG4RgvZwI4CHHMkW915ebbq1yQzs0nW5CEirMnezgDxUWVzBm91S2JcVQE850y7s9pRK4ZL0hwf77rvqjbbB1IqKaq-kZ_kM");


$analytics = new Google_Service_Analytics($client);



$results = getResults($analytics, 26174152);


print_r($results);
*/

function getResults($analytics, $profileId) {
  // Calls the Core Reporting API and queries for the number of sessions
  // for the last seven days.
   return $analytics->data_realtime->get(
 'ga:' . $profileId, array('metrics'=> 'rt:activeUsers'), array('dimensions'=> 'rt:pagepath,rt:pageTitle', 'sort'=> '-rt:activeUsers' ,'max-results'=>10 )  );
}

function getProfile($analytics, $account_id, $profile_id){
  $properties = $analytics->management_webproperties->listManagementWebproperties($account_id);
  foreach ($properties as $value_properties) {
    $profiles = $analytics->management_profiles->listManagementProfiles($account_id, $value_properties->getId());
    foreach ($profiles as $value_profiles) {
      if($value_profiles->getId() == $profile_id){
          return $analytics->management_profiles->get($account_id, $value_properties->getId(), $profile_id);
      }
    }
  }
  return false;
}

function getMetaOg($url){
  libxml_use_internal_errors(true);
  $doc = new DomDocument();
  $doc->loadHTML(file_get_contents($url));
  $xpath = new DOMXPath($doc);
  $query = '//*/meta[starts-with(@property, \'og:\')]';
  $metas = $xpath->query($query);

  $array = array();

  foreach ($metas as $meta) {
      $property = $meta->getAttribute('property');
      $content = $meta->getAttribute('content');
      $array[$property] = $content;
  }

  return $array;
}

//ejecuta
//esto no puede ir en codigo
$arg = json_decode('{"id":"1", "accountId":"20159952", "profileId":"39922496", "setting_id":"3", "user_from":"0","name":"vozpopuli economia","sourcetype":"RSS","Source_url":"https:\/\/www.vozpopuli.com\/rss\/economia-y-finanzas\/","readtypetime":"Interval","readintervalminutes":"60","itemstoread":"15","itemschoose":"Old First","postingtypetime":"Interval","postingintervalminutes":"30","public_id":"91aab5a6-1dcb-11eb-a49a-ac1f6b17ff4a","tagsdictionary":"[]","user_id":"6","filter":null,"destination_profiles":"[\"71bfc81b-ca8d-11ea-86d6-de2e501f7018\", \"7205354a-ca8d-11ea-86d6-de2e501f7018\", \"99b23474-ca85-11ea-86d6-de2e501f7018\"]"}');


$destination_profiles = str_replace('[', '(', $arg->destination_profiles);
$destination_profiles = str_replace(']', ')', $destination_profiles);

$sql = "SELECT id, name, platform FROM app_thesoci_9c37.properties_accounts where public_id in ".$destination_profiles;
$profiles = $db->getRecordSet($sql);




$analytics = inicializacion_google($arg->user_id);


$profile = getProfile($analytics, $arg->accountId, $arg->profileId);

if($profile !== false){
  $base_url = $profile->websiteUrl;
}

$results = getResults($analytics, $arg->profileId);




//y aqui es donde debes hacer el filtro, antes de los insert
$sql = "SELECT max(posted_time) as max_time FROM app_thesoci_9c37.organic_post WHERE user_id = ".$arg->user_id;

$jobs = $db->getRecordSet($sql);


if(time() > strtotime($jobs[0]['max_time'])){
	$max_time = date("Y-m-d H:i:s");
}else{
	$max_time = $jobs[0]['max_time'];
}


if(!empty($results)){
  $arr = array();
	switch($arg->itemschoose){
		case "Random":
			$arr = shuffle($results);
		break;
    case "Old First":
    case "Recent First":
		case "Top to bottom":
		default:
			$arr = $results;
		break;
	}

  $i = 0;
  $time = 0;
	foreach ($arr as $item) {
		if($i < $arg->itemstoread){
			$time = $time + $arg->postingintervalminutes;
      if($item[2] > $arg->user_from){
        $url = $base_url.$item[0];
  			insert_post($item, $time, $url);
        $i++;
      }
		}else{
			break;
		}
	}

 }else{
     echo "<h2>No item found</h2>";
 }


function insert_post($item, $time, $url){
	GLOBAL $db, $arg, $max_time, $profiles;

  $meta = getMetaOg($url);

  $thumbnail_url = (isset($meta['og:image']))?$meta['og:image']:"";
  $description = (isset($meta['og:description']))?$meta['og:description']:"";

  $sql = "INSERT INTO `app_thesoci_9c37`.`organic_sources_posts`
						(`setting_id`, `organic_souce_post_md5`, `destination_url`, `thumbnail_url`, `post_name`, `post_description`, `user_id`, properties)
						VALUES ($arg->setting_id, '".md5($url)."', '".$db->real_escape_string($url)."',
						'".$db->real_escape_string($thumbnail_url)."', '".$db->real_escape_string($item[1])."',
						'".$db->real_escape_string($description)."', ".$arg->user_id.", '".json_encode($profiles)."')
						ON DUPLICATE KEY UPDATE thumbnail_url = '".$db->real_escape_string($url)."';";


	$db->query($sql);

	$id_content = $db->insert_id();
	if($id_content != 0){
		foreach ($profiles as $value) {
      $sql = "INSERT INTO `app_thesoci_9c37`.`organic_post`
				(`posted_time`, `setting_id`, `source_content_id`, `content`, `user_id`, `destination_url`,
					`property_id`, `platform`)
			VALUES
				('".date("Y-m-d H:i:s", strtotime($max_time." +".$time." minutes"))."', $arg->setting_id, ".$id_content.",
				'".$db->real_escape_string($item[1])."', $arg->user_id, '".$db->real_escape_string($url)."',
				".$value["id"].", '".$value["platform"]."');";

			$db->query($sql);
		}
	}
}


echo "fin";
