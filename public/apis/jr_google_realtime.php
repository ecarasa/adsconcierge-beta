<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set('UTC');
require 'configs/general.php';
require 'configs/googleanalytics.php';
require __DIR__ . '/../../vendor/autoload.php';

if (isset($argv)) {
    switch ($argv[1]) {
        case 'inicializacion_google':
            inicializacion_google($argv[2]);

    }
    exit;
}

function inicializacion_google($userid)
{

    global $dbconn, $platformid_googleanalytics, $KEY_FILE_LOCATION;

    $sql = "SELECT * FROM auths_user_platform WHERE user_id={$userid} AND platform='GANALYTICS' limit 0,1";
    $result = $dbconn->query($sql);
    $row = $result->fetch_assoc();

    
    $userid = $row['user_id'];
    $app_id = $row['app_id'];
    $platformid = $row['platform'];
    $access_token = json_decode($row['retorno'], true);

    $analytics_offline = initializeAnalytics_offline($userid, $access_token);

    $stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` (`user_id`, `platform`, `app_id`,`platform_user_id`, `id_en_platform`, `token`,`name`,`status`,`category`, `metadata`) 	VALUES (?,?,?,?,?,?,?,?,?,?) 	ON DUPLICATE KEY UPDATE    `name`= ?,  `status`= ?,`platform_user_id`=?,  `metadata`= ? ");
    $category = 'ganalytics';
    $abc = 0;
    $accounts = $analytics_offline->management_accounts->listManagementAccounts();

    foreach ($accounts->getItems() as $account) {
        print_r('ACCOUNT' . PHP_EOL . $account->getId());
        print_r($account->getName() . PHP_EOL . '********' . PHP_EOL);
        $properties = $analytics_offline->management_webproperties->listManagementWebproperties($account->getId());
        foreach ($properties->getItems() as $property) {

            //print_r( $property->getId());
            //    print_r( $property->getName().PHP_EOL);
            usleep(200); //usleep(2000000); 2 segundos
            $profiles = $analytics_offline->management_profiles->listManagementProfiles($account->getId(), $property->getId());

            foreach ($profiles->getItems() as $item) {

                $metadata = json_encode($item);
                $token = null;
                $status = 'Active';
                $name = $property->getName() . ' ->' . $item->websiteUrl . ' ->' . $item->getName();
                $propertyid = $item->getId();
                $userid_en_platfomr = $account->getId();
                print_r($item->getId());
                print_r($name . PHP_EOL . '--' . PHP_EOL);

                $stmt->bind_param("ssssssssssssss", $userid, $platformid, $app_id, $userid_en_platfomr, $propertyid, $token, $name, $status, $category, $metadata, $name, $status, $userid_en_platfomr, $metadata);
                $stmt->execute();
                if (!$stmt) {
                    echo "\nPDO::errorInfo():\n";
                    print_r($dbh->errorInfo());
                }
            }

        }

    }
    $stmt->close();

}

/***
	$accounts = $analytics->management_accounts->listManagementAccounts();
	print_r($accounts[3]->getId());

	$properties = $analytics->management_webproperties
	->listManagementWebproperties(123399689);
	//print_r($properties);

	$profiles = $analytics->management_profiles
	->listManagementProfiles($accounts[3]->getId(), $properties[0]->getId());
	print_r($profiles);
	exit;
 **/

function ___a()
{
    $fichero = ' $paginas=[]; ' . PHP_EOL;
    foreach ($profiles as $profile) {
        print_r($profile);
        $analytics = $analytics_offline;
        try {
            if (isset($offline[$profile])) {
                $analytics = initializeAnalytics_offline($offline[$profile]);
            }
            $resultados = getResults($analytics, $profile);
            $fichero .= mapearesultados($resultados, $profile, $mapas);
        } catch (Error $e) {

            print_r($e);
        }
    }

    $fichero .= '$v="' . date('Ymd-H.i.s') . '";' . PHP_EOL;
}
function mapearesultados($results, $profile, $mapas)
{
    $fichero = '';
    $fichero .= $mapas[$profile] . PHP_EOL;
    $i = 0;
    foreach ($results->rows as $linea) {
        if (strpos($linea[0], 'marfeel')) {continue;}
        if (strpos($linea[0], 'amp')) {continue;}
        $fichero .= '$paginas["' . $profile . '"][]=["path"=>"' . $linea[0] . '", "title"=>"' . htmlspecialchars(str_replace('(not set)', '', $linea[1])) . '",  "pv"=>' . $linea[2] . '];' . PHP_EOL;

        if ($i > 50) {break;}
        $i++;

    }

    $fichero .= '$conteo["' . $profile . '"]=' . $i . ';' . PHP_EOL;
    return $fichero;
}

function initializeAnalytics_offline($userid, $access_token)
{
    global $KEY_FILE_LOCATION, $dbconn;

    $client = new Google_Client();
    $client->setApplicationName("Hello Analytics Reporting");
    $client->setAuthConfig($KEY_FILE_LOCATION);

    //$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
    $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly', 'https://www.googleapis.com/auth/userinfo.email']);
    $client->setAccessType('offline');
    $client->setAccessToken($access_token);

    if ($client->isAccessTokenExpired()) {
        echo 'TOKEN EXPIRED';
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        $retorno = $client->getAccessToken();
        $result = json_encode($retorno);
        $stmt = $dbconn->prepare("UPDATE `app_thesoci_9c37`.`auths_user_platform` SET `access_token`=?, `retorno`=?  WHERE  `user_id`=1 AND `platform`=6");

        $salida = $stmt->bind_param("ss", $retorno['access_token'], $result);

        $stmt->execute();
        $stmt->close();

    }
    $analytics = new Google_Service_Analytics($client);

    return $analytics;

}

function getResults($analytics, $profileId)
{
    // Calls the Core Reporting API and queries for the number of sessions
    // for the last seven days.
    return $analytics->data_realtime->get(
        'ga:' . $profileId, array('metrics' => 'rt:pageviews'), array('dimensions' => 'rt:pagepath,rt:pageTitle', 'sort' => '-rt:pageviews', 'max-results' => 500));
}