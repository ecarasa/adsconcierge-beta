<?php

//print_r($_POST);

$curl = curl_init();

curl_setopt_array($curl, [
  CURLOPT_URL => $_POST['url'],
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => $_POST['method'],
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => ["Authorization: ".$_POST['oauth']['Authorization']],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
header('Content-Type: application/json');

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}