"use strict";
var KTDatatablesExtensionsScroller = function() {

	var initTable1 = function( parameters ) { 
		var table = parameters.table;
		// begin first table
		table.DataTable({
			responsive		: true,
			ajax			: typeof parameters.url === 'undefined' ? parameters.ajax : parameters.url,
			deferRender		: true,
			scrollY			: '500px',
			scrollCollapse	: true,
			scroller  		: true,
			columns 		: parameters.columns,
			language: {
					        "decimal"			: "",
					        "emptyTable"		: "Not Found...",
					        "info"				: "Showing _START_ to _END_ of _TOTAL_ inputs",
					        "infoEmpty"			: "Showing 0 to 0 of 0 inputs",
					        "infoFiltered"		: "(Filtering of _MAX_ total inputs)",
					        "infoPostFix"		: "",
					        "thousands"			: ",",
					        "lengthMenu"		: "Show _MENU_ entries",
					        "loadingRecords"	: "Loading...",
					        "processing"		: "Processing...",
					        "search"			: "Search:",
					        "zeroRecords"		: "No results found",
					        "paginate"			: {
											            "first"		: "first",
											            "last"		: "last",
											            "next"		: "next",
											            "previous"	: "previous"
											        }
					    },
			columnDefs: [
				{
					targets: -1,
					title: 'Actions',
					orderable: false,
					render: function(data, type, full, meta){
						return `
                        <ul class="list-group list-group-horizontal list-inline" >
	                        <!--
	                        <li class="list-inline-item" >
		                        <span class="dropdown">
		                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
		                              <i class="la la-ellipsis-h"></i>
		                            </a>
		                            <div class="dropdown-menu dropdown-menu-right">
		                                <a class="dropdown-item" href="/`+parameters.base+`/`+full.id+`/edit" title="Editar `+parameters.base+`" ><i class="la la-edit"></i></a>
		                                <a class="dropdown-item" href="/`+parameters.base+`/`+full.id+`/delete" title="Eliminar `+parameters.base+`" ><i class="la la-trash"></i></a>
		                            </div>
		                        </span>
	                        </li>
	                        -->
	                        <li class="list-inline-item" >
		                        <a class="dropdown-item" href="/`+parameters.base+`/`+full.id+`/edit" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Editar `+parameters.base+`" ><i class="la la-edit"></i></a>
		                    </li>
		                    <li class="list-inline-item" >
		                        <a class="dropdown-item" href="/`+parameters.base+`/`+full.id+`/delete" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Eliminar `+parameters.base+`" ><i class="la la-trash"></i></a>
	                        </li>	
                        </ul>
                        `;
					},
				},
				{
					targets: -3,
					render: function(data, type, full, meta) {
						//console.log('render columns def 2');
						var status = {
							1: {'title': 'Pending'	, 'class': 'kt-badge--brand'},
							2: {'title': 'Delivered', 'class': ' kt-badge--danger'},
							3: {'title': 'Canceled'	, 'class': ' kt-badge--primary'},
							4: {'title': 'Success'	, 'class': ' kt-badge--success'},
							5: {'title': 'Info'		, 'class': ' kt-badge--info'},
							6: {'title': 'Danger'	, 'class': ' kt-badge--danger'},
							7: {'title': 'Warning'	, 'class': ' kt-badge--warning'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
					},
				},
				{
					targets: -2,
					render: function(data, type, full, meta) {
						//console.log('render columns def 3');
						var status = {
							1: {'title': 'Online', 'state': 'danger'},
							2: {'title': 'Retail', 'state': 'primary'},
							3: {'title': 'Direct', 'state': 'success'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="kt-badge kt-badge--' + status[data].state + ' kt-badge--dot"></span>&nbsp;' +
							'<span class="kt-font-bold kt-font-' + status[data].state + '">' + status[data].title + '</span>';
					},
				},
			],
		});
	};

	return {

		//main function to initiate the module
		init: function( datatable ) {
			initTable1( datatable );
		},

	};

}();
