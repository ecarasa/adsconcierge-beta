// Class definition
var KTFormRepeater = function() {

    // Private functions
    var demo1 = function() {
        $('#kt_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo2 = function() {
        $('#kt_repeater_2').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });
    }


    var demo3 = function() {
        $('#kt_repeater_3').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });
    }

    var demo4 = function() {
        $('#kt_repeater_4').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo5 = function() {
        $('#kt_repeater_5').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo6 = function() {
        $('#kt_repeater_6').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo7 = function() {
        $('#kt_repeater_7').repeater({
            initEmpty: false,

            defaultValues: {
                'textarea': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo8 = function() {
        $('#kt_repeater_8').repeater({
            initEmpty: false,

            defaultValues: {
                'textarea': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo9 = function() {
        $('#kt_repeater_9').repeater({
            initEmpty: false,

            defaultValues: {
                'textarea': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo10 = function() {
        $('#kt_repeater_10').repeater({
            initEmpty: false,

            defaultValues: {
                'textarea': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo11 = function() {
        //age
        $('#kt_repeater_11').repeater({
            initEmpty: false,

            defaultValues: {
                'textarea': 'foo'
            },

            show: function() {
                $(this).slideDown();
                /*$(this).children(".add").remove();
                $(this).append('<div class="col-md-6"><div data-repeater-delete="" class="btn-sm btn btn-danger btn-pill"><span><i class="la la-trash-o"></i><span>Delete</span></span></div></div>');*/
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }


    var demo12 = function() {
        $('#kt_repeater_12').repeater({
            initEmpty: false,

            defaultValues: {
                'textarea': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo13 = function() {
        $('#kt_repeater_13').repeater({
            initEmpty: false,

            defaultValues: {
                'textarea': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo14 = function() {
        $('#kt_repeater_14').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': ''
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo15 = function() {
        $('#kt_repeater_15').repeater({
            initEmpty: false,

            defaultValues: {
                'textarea': 'foo'
            },

            show: function() {
                $(this).slideDown();
                /*$(this).children(".add").remove();
                $(this).append('<div class="col-md-6"><div data-repeater-delete="" class="btn-sm btn btn-danger btn-pill"><span><i class="la la-trash-o"></i><span>Delete</span></span></div></div>');*/
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var demo16 = function() {
        $('#kt_repeater_16').repeater({
            initEmpty: false,

            defaultValues: {
                'textarea': 'foo'
            },

            show: function() {
                $(this).slideDown();
                /*$(this).children(".add").remove();
                $(this).append('<div class="col-md-6"><div data-repeater-delete="" class="btn-sm btn btn-danger btn-pill"><span><i class="la la-trash-o"></i><span>Delete</span></span></div></div>');*/
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
            demo2();
            demo3();
            demo4();
            demo5();
            demo6();
            demo7();
            demo8();
            demo10();
            demo11();
            demo13();
            demo14();
            demo15();
            demo16();
        }
    };
}();


jQuery(document).ready(function() {
    KTFormRepeater.init();
});
