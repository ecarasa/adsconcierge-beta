"use strict";

// Class definition
var KTWizard1 = function() {
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;
    var indexImg, flag;

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    var initWizard = function() {
        // Initialize form wizard

        wizard = new KTWizard('kt_wizard_v1', {
            startStep: 1
        });

        // Validation before going to next page
        wizard.on('beforeNext', function(wizardObj) {

            var respuesta1 = validator.form();
            var respuesta2 = ValidacionesEspeciales(respuesta1);
            if ((respuesta1 !== true) || (respuesta2)) {
                //wizardObj.stop();  // don't go to the next step
            }

            if (wizardObj.currentStep == 5) {

                $("#cards_redes_sociales").empty();
                $("#audience_summary").empty();
                $("#step1_summary").empty();
                $("#placement_summary").empty();
                $("#objective_summary").empty();
                $("#demographics_summary").empty();
                $("#budget_summary").empty();
                $("#audience_summary").empty();



                if ($('input[name="campaign_name"]').val() != "") {
                    //$("#resumen_step1").append("<b>Campaign name: </b>"+ +"<br>");
                    $('#step1_summary').append('<li><strong>Campaign Name:</strong> ' + $('input[name="campaign_name"]').val() + '</li>');
                }

                if ($('select[name="client"]').val() != "") {
                    //$("#resumen_step1").append("<b>Client: </b>"+ $('select[name="client"] option:selected').html()+"<br>");
                    $('#step1_summary').append('<li><strong>Client:</strong> ' + $('select[name="client"] option:selected').html() + '</li>');
                }

                var currency = [];

                $('input[name="rrss[]"]:checked').each(function() {

                    //objective
                    $('input[name="objetive[' + $(this).val() + ']"]:checked').each(function() {
                        $('#objective_summary').append('<li>' + $(this).attr('valor') + '</li>');
                    });

                    $('input[name="placement[' + $(this).val() + '][]"]:checked').each(function(index) {
                        $('#placement_summary').append('<li>' + $(this).attr('valor') + '</li>');
                    });

                    currency.push($('#price-' + $(this).val()).html());

                });

                var uniqueCurrency = currency.filter(onlyUnique);

                if (uniqueCurrency.length > 1) {
                    uniqueCurrency = 'MIX';
                } else {
                    uniqueCurrency = uniqueCurrency[0];
                }


                let target_in = [];
                let target_out = [];
                $('input[name="target"]').each(function() {
                    if ($(this).is(':checked')) {
                        target_in.push($(this).attr('data-id'));
                    } else {
                        target_out.push($(this).attr('data-id'));
                    }
                });


                $('#targets_summary').append('<svg id="_27_Icon_all-done" data-name="27) Icon/all-done" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M10.169,12a1,1,0,0,1-.779-.373l-1.1-1.367L9.554,8.644l.6.749L17.213.384a1,1,0,1,1,1.574,1.232l-7.831,10a1,1,0,0,1-.78.384Zm-5,0a1,1,0,0,1-.78-.373L.221,6.449A1,1,0,0,1,1.779,5.2l3.379,4.2L12.213.384a1,1,0,0,1,1.574,1.232l-7.831,10a1,1,0,0,1-.78.384Zm.051-5.55A1,1,0,1,1,6.779,5.2l.2.249L5.713,7.061Z" transform="translate(3 6)" fill="#222b45"/> </svg> Included: ' + target_in.join(' - '));
                $('#targets_summary').append('<br><svg id="_27_Icon_close-circle" data-name="27) Icon/close-circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M10,20A10,10,0,1,1,20,10,10.011,10.011,0,0,1,10,20ZM10,2a8,8,0,1,0,8,8A8.009,8.009,0,0,0,10,2Zm2,11a.99.99,0,0,1-.707-.292L10,11.414,8.707,12.707a1,1,0,0,1-1.414,0,1,1,0,0,1,0-1.414L8.586,10,7.293,8.707A1,1,0,1,1,8.707,7.293L10,8.586l1.293-1.293a1,1,0,1,1,1.414,1.414L11.414,10l1.293,1.293A1,1,0,0,1,12,13Z" transform="translate(2 2)" fill="#222b45"/> </svg> Exclude: ' + target_out.join(' - '));


                // demograpichs / Audience
                var str = "";
                str = str + "<b>Gender: </b>" + $('input[name="gender"]:checked').attr('valor') + "<br>";
                str = str + "<b>Device: </b>" + $('input[name="devices"]:checked').attr('valor') + "<br>";
                str = str + "<b>Conectivity: </b>" + $('input[name="conectivity"]:checked').attr('valor') + "<br>";
                str = str + "<b>Languages: </b>" + "<br>";
                // ages
                str = "";
                var AGE_RANGES = [];
                $(".input-daterange.input-group").each(function(index) {
                    if ($('input[name="age[' + index + '][start]"]').val()) {
                        AGE_RANGES.push([$('input[name="age[' + index + '][start]"]').val(), $('input[name="age[' + index + '][end]"]').val()]);
                    }
                });

                if (AGE_RANGES.length != 0) {
                    str = str + "<b>Age Range: </b>";
                    AGE_RANGES.map((range) => str = str + range.join('-') + ", ");
                }

                //$('#demographics_summary').append(str);

                // LANGUAGES
                var LANGUAGES = [];
                str = str + "<ul style='margin-left: 30px;'>";
                $("#language-list>li").each(function(index) {
                    str = str + "<li>" + $(this).attr('data-name') + "</li>";
                });
                str = str + "</ul><br>";

                $('#demographics_summary').append(str);




                //budget
                if ($('input[name="budget"]').val() != "") {
                    var budgetdistribution = "";
                    if ($('input[name="budgetdistribution"]:checked').val() == 1) {
                        budgetdistribution = "Per day";
                    } else {
                        budgetdistribution = "Total";
                    }
                    //$("#resumen_step5").append("<b>Budget: </b>"+ $('input[name="budget"]').val()+" " +budgetdistribution+ "<br>");
                    $('#budget_summary').append('<strong>Total Budget:</strong> ' + uniqueCurrency + $('input[name="budget"]').val() + " - " + budgetdistribution);
                }

                $("#budget_summary").append("<br><i class='icon-2x text-dark-50 flaticon2-calendar-9'></i> <b>Date From: </b>" + $('input[name="start_date"]').val() + " <b> to </b>" + $('input[name="end_date"]').val() + "<br>");

                $("#budget_summary").append("<hr/>");

                if ($('input[name="budget_target_diary"]').val() != "") {
                    $("#budget_summary").append("<b>Budget target diary: </b>" + $('input[name="budget_target_diary"]').val() + " " + $('select[name="budget_target_diary_type"] option:selected').html() + "<br>");
                }

                if ($('input[name="budget_target_total"]').val() != "") {
                    $("#budget_summary").append("<b>Budget target total: </b>" + $('input[name="budget_target_total"]').val() + " " + $('select[name="budget_target_total_type"] option:selected').html() + "<br>");
                }

                $("#budget_summary").append("<hr/>");

                var schedule = $("#weekly-schedule").data('artsy.dayScheduleSelector').serialize();

                var days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
                var str_schedule = "";
                $.each(schedule, function(index, value) {
                    if (value.length > 0) {
                        var i = 0;
                        jQuery.map(value, function(a) {
                            if (i == 0) {
                                str_schedule = str_schedule + "<tr><td style='padding: 5px;'><b>" + days[index] + "</b></td><td style='padding: 5px;'> From " + a[0] + " to " + a[1] + "</td></tr>";
                            } else {
                                str_schedule = str_schedule + "<tr><td></td><td style='padding: 5px;'> From " + a[0] + " to " + a[1] + "</td></tr>";
                            }
                            i++;
                        });
                    }
                });

                if (str_schedule != "") {
                    $("#budget_summary").append("<b>Run only on:</b><br> <table style='font-size: 14px; margin-left: 30px;'>" + str_schedule + "</table>");
                }

                if ($('input[name="spend_on_each_ad"]').val() != "") {
                    $("#budget_summary").append("<b>Spend on each ad: </b>" + uniqueCurrency + $('input[name="spend_on_each_ad"]').val() + "<br>");
                }

                if ($('input[name="impressions_per_day"]').val() != "") {
                    $("#budget_summary").append("<b>Impressions per day: </b>" + uniqueCurrency + $('input[name="impressions_per_day"]').val() + "<br>");
                }



                str = '';
                // INTEREST Y AUDIENCE
                var Interests_IN = [],
                    Interests_OUT = [];
                var Audience_IN = [],
                    Audience_OUT = [];

                $('input[name="rrss[]"]:checked').each(function() {

                    $(".intereses-list." + $(this).val() + ">li").each(function(index) {
                        if ($(this).attr('class') == 'IN') {
                            Interests_IN.push($(this).attr('data-name'));
                        } else {
                            Interests_OUT.push($(this).attr('data-name'));
                        }
                    });

                    $(".audiencia-list." + $(this).val() + ">li").each(function(index) {
                        if ($(this).attr('class') == 'IN') {
                            Audience_IN.push($(this).attr('data-name'));
                        } else {
                            Audience_OUT.push($(this).attr('data-name'));
                        }
                    });

                });

                if (Interests_IN.length != 0) {
                    str = str + '<b><svg id="_27_Icon_all-done" data-name="27) Icon/all-done" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M10.169,12a1,1,0,0,1-.779-.373l-1.1-1.367L9.554,8.644l.6.749L17.213.384a1,1,0,1,1,1.574,1.232l-7.831,10a1,1,0,0,1-.78.384Zm-5,0a1,1,0,0,1-.78-.373L.221,6.449A1,1,0,0,1,1.779,5.2l3.379,4.2L12.213.384a1,1,0,0,1,1.574,1.232l-7.831,10a1,1,0,0,1-.78.384Zm.051-5.55A1,1,0,1,1,6.779,5.2l.2.249L5.713,7.061Z" transform="translate(3 6)" fill="#222b45"/> </svg> Include Interests: </b>' + Interests_IN.join(', ') + "<br>";
                }
                if (Interests_OUT.length != 0) {
                    str = str + '<b><svg id="_27_Icon_close-circle" data-name="27) Icon/close-circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M10,20A10,10,0,1,1,20,10,10.011,10.011,0,0,1,10,20ZM10,2a8,8,0,1,0,8,8A8.009,8.009,0,0,0,10,2Zm2,11a.99.99,0,0,1-.707-.292L10,11.414,8.707,12.707a1,1,0,0,1-1.414,0,1,1,0,0,1,0-1.414L8.586,10,7.293,8.707A1,1,0,1,1,8.707,7.293L10,8.586l1.293-1.293a1,1,0,1,1,1.414,1.414L11.414,10l1.293,1.293A1,1,0,0,1,12,13Z" transform="translate(2 2)" fill="#222b45"/> </svg> Exclude Interests: </b>' + Interests_OUT.join(', ') + "<br>";
                }

                if (Audience_IN.length != 0) {
                    str = str + '<b><svg id="_27_Icon_all-done" data-name="27) Icon/all-done" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M10.169,12a1,1,0,0,1-.779-.373l-1.1-1.367L9.554,8.644l.6.749L17.213.384a1,1,0,1,1,1.574,1.232l-7.831,10a1,1,0,0,1-.78.384Zm-5,0a1,1,0,0,1-.78-.373L.221,6.449A1,1,0,0,1,1.779,5.2l3.379,4.2L12.213.384a1,1,0,0,1,1.574,1.232l-7.831,10a1,1,0,0,1-.78.384Zm.051-5.55A1,1,0,1,1,6.779,5.2l.2.249L5.713,7.061Z" transform="translate(3 6)" fill="#222b45"/> </svg> Include Audience: </b>' + Audience_IN.join(', ') + "<br>";
                }
                if (Audience_OUT.length != 0) {
                    str = str + '<b><svg id="_27_Icon_close-circle" data-name="27) Icon/close-circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M10,20A10,10,0,1,1,20,10,10.011,10.011,0,0,1,10,20ZM10,2a8,8,0,1,0,8,8A8.009,8.009,0,0,0,10,2Zm2,11a.99.99,0,0,1-.707-.292L10,11.414,8.707,12.707a1,1,0,0,1-1.414,0,1,1,0,0,1,0-1.414L8.586,10,7.293,8.707A1,1,0,1,1,8.707,7.293L10,8.586l1.293-1.293a1,1,0,1,1,1.414,1.414L11.414,10l1.293,1.293A1,1,0,0,1,12,13Z" transform="translate(2 2)" fill="#222b45"/> </svg> Exclude Audience: </b>' + Audience_OUT.join(', ') + "<br>";
                }

                $("#audience_summary").append(str);





                $("#creas_summary").html('');

                if ($('input[name="rad__adsdesign"]:checked').val() == 1) {
                    $("#creas_summary").append("<h4>Create</h4>");
                } else {
                    $("#creas_summary").append("<h4>Latest Post</h4>");
                }

                //  **** Begin Summary: Step 4.1 - Ads Design ***
                //start.headline
                $("#creas_summary").append('<b>Headlines</b>');
                str = "<ul style='margin-left: 30px;' class='summarylist'>";
                var flag = true;
                var i = 0;
                while (flag) {
                    if ($('input[name="titulo[' + i + '][titulo]"]').val() != undefined) {
                        str = str + "<li>";
                        str = str + $('input[name="titulo[' + i + '][titulo]"]').val();
                        str = str + ' | Tags: ';
                        if ($('input[name="titulo[' + i + '][titulotags]"]').val() != "") {
                            str = str + $('input[name="titulo[' + i + '][titulotags]"]').val();
                        }
                        i++;
                        str = str + '</li>';
                    } else {
                        flag = false;
                    }
                }
                $("#creas_summary").append(str);
                $("#creas_summary").append("<br>");
                //FIN.


                //start.adtext		
                $("#creas_summary").append('<b>Ad Texts</b><br>');
                str = "<ul style='margin-left: 30px;' class='summarylist'>";

                flag = true;
                i = 0;
                while (flag) {
                    if ($('textarea[name="descripcion[' + i + '][descripcion]"]').val() != undefined) {
                        str = str + "<li>";
                        str = str + $('textarea[name="descripcion[' + i + '][descripcion]"]').val();
                        str = str + ' | Tags: ';
                        if ($('input[name="descripcion[' + i + '][descripciontags]"]').val() != "") {
                            str = str + $('input[name="descripcion[' + i + '][descripciontags]"]').val();
                        }
                        i++;
                        str = str + '</li>';


                    } else {
                        flag = false;
                    }
                }
                $("#creas_summary").append(str);
                $("#creas_summary").append("<br>");
                //FIN.

                //start.img
                $("#creas_summary").append('<b>Upload new images</b><br>');

                flag = true;
                i = 0;

                $("#creas_summary").append('<div id="showImagenes" class="imagenessummary"></div>');
                $("#showImagenes").append($('.img_preview').clone());

                while (flag) {
                    if ($('input[name="imagen[' + i + '][imagen]"]').val() != undefined) {

                        if ($('input[name="imagen[' + i + '][imagentags]"]').val() != "") {
                            $("#creas_summary").append("Tags:" + $('input[name="imagen[' + i + '][imagentags]"]').val());
                        }
                        i++;
                        $("#creas_summary").append("<br>");
                    } else {
                        flag = false;
                    }
                }
                $("#creas_summary").append("<br>");
                //FIN.


                //start.urls
                $("#creas_summary").append('<b>URLs</b><br>');
                str = "<ul style='margin-left: 30px;' class='summarylist'>";
                flag = true;
                i = 0;
                while (flag) {
                    if ($('input[name="url[' + i + '][url]"]').val() != undefined) {


                        str = str + "<li>";
                        str = str + $('input[name="url[' + i + '][url]"]').val();
                        str = str + ' | Tags: ';
                        if ($('input[name="url[' + i + '][urltags]"]').val() != "") {
                            str = str + $('input[name="url[' + i + '][urltags]"]').val();
                        }
                        i++;
                        str = str + '</li>';

                    } else {
                        flag = false;
                    }

                }
                $("#creas_summary").append(str);
                $("#creas_summary").append("<br>");
                //FIN.

                //start.link display
                if ($('input[name="link_display"]').val() != "") {
                    $("#resumen_step4").append("<b>Display Link: </b>" + $('input[name="link_display"]').val() + "<br>");
                }
                //FIN.


                //start.description
                $("#creas_summary").append('<b>Link Description</b><br>');
                str = "<ul style='margin-left: 30px;' class='summarylist'>";
                flag = true;
                i = 0;
                while (flag) {
                    if ($('textarea[name="text_link[' + i + '][text_link]"]').val() != undefined) {
                        //$("#resumen_step4").append($('textarea[name="text_link['+i+'][text_link]"]').val());
                        str = str + "<li>";
                        str = str + $('textarea[name="text_link[' + i + '][text_link]"]').val();
                        str = str + ' | Tags: ';
                        if ($('input[name="text_link[' + i + '][text_linktags]"]').val() != "") {
                            str = str + $('input[name="text_link[' + i + '][text_linktags]"]').val();
                        }
                        i++;
                        str = str + '</li>';


                    } else {
                        flag = false;
                    }
                }

                $("#creas_summary").append(str);
                $("#creas_summary").append("<br>");
                //FIN.

                //CALL TO ACTION
                $("#creas_summary").append('<b>Call to Action</b><br>');
                str = "<ul style='margin-left: 30px;' class='summarylist'>";
                flag = true;
                i = 0;
                while (flag) {
                    if ($('select[name="call_to_action[' + i + '][call_to_action]"]').val() != undefined) {
                        str = str + "<li>";
                        str = str + $('select[name="call_to_action[' + i + '][call_to_action]"]').val();
                        str = str + ' | Tags: ';
                        if ($('input[name="call_to_action[' + i + '][call_to_actiontags]"]').val() != "") {
                            str = str + $('input[name="call_to_action[' + i + '][call_to_actiontags]').val();
                        }
                        i++;
                        str = str + '</li>';


                    } else {
                        flag = false;
                    }
                }
                $("#creas_summary").append(str);
                $("#creas_summary").append("<br>");
                //FIN.CALL TO ACTION	


                if ($('input[name=rad__promote]:checked').val() == 2) {

                    $("#creas_summary").append('<b>AutoPost</b><br>');





                    // creamos o latest post
                    /* 	
						formData.append("action_creas", $('input[name=rad__promote]:checked').val()); // 1 al 2
	
						formData.append("howPromoted", $('input[name=rad__promote]:checked').val()); // 1 al 2
						formData.append("typePromoted", $('input[name=aPromoted]:checked').val()); // 1 al 3
						formData.append("postPromoted", $('input[name=aPosts]:checked').val()); // 1 al 5 */
                    $("#creas_summary").append('<b>' + $('#promoteForValue').val() + ' ' + $('#promoteForValue').val() + '<br>');

                    if ($('input[name=aPromoted]:checked').val() == 2) {
                        $("#creas_summary").append('<b>GAnalytics Property: ' + $('#ganalitycs').val() + '<br>');
                    }

                    $("#creas_summary").append('<b>URL: ' + $('#t_postLink').val() + '<br>');
                    $("#creas_summary").append('<b>Texts: ' + $('#t_postText').val() + '<br>');

                    if ($('input[name=aPromoted]:checked').val() == 2) {
                        // es google analitycs
                        var flag = true;
                        var i = 0;
                        while (flag) {
                            if ($('input[name="posti[' + i + '][t_promotePost]"]').val() != undefined) {
                                var item = { value: $('input[name="posti[' + i + '][t_promotePost]"]').val(), typeFilter: $('input[name="posti[' + i + '][se_promotePost_GA]"]').val() };
                                $("#creas_summary").append('<b>GA Post Filter: ' + $('input[name="posti[' + i + '][t_promotePost]"]').val() + ' ' + $('input[name="posti[' + i + '][se_promotePost_GA]"]').val() + '<br>');
                                i++;
                            } else {
                                flag = false;
                            }
                        }
                    }

                    if ($('input[name=aPromoted]:checked').val() == 3) {
                        var flag = true;
                        var i = 0;
                        while (flag) {
                            if ($('input[name="posti[' + i + '][t_promotePost]"]').val() != undefined) {
                                var item = { value: $('input[name="posti[' + i + '][t_promotePost]"]').val(), typeFilter: $('input[name="posti[' + i + '][se_promotePost]"]').val() };
                                $("#creas_summary").append('<b>Latest Post Filter: ' + $('input[name="posti[' + i + '][t_promotePost]"]').val() + ' ' + $('input[name="posti[' + i + '][se_promotePost]"]').val() + '<br>');
                                i++;
                            } else {
                                flag = false;
                            }
                        }
                    }


                }
                $("#creas_summary").append("<br>");
                $("#creas_summary").append("<br>");

                // end.

                // TARJETAS REDES SOCIALES
                $('input[name="rrss[]"]:checked').each(function() {

                    var cardSocial = '';
                    var rrss = $(this).val();
                    var linea = '<hr style="color: black;width: 100%;"></hr>';

                    cardSocial = cardSocial + '<div class="col-sm-12 col-md-4">';
                    cardSocial = cardSocial + '<div class="card">';
                    cardSocial = cardSocial + '<div class="card-header flex-nowrap border-0 pt-9">';
                    cardSocial = cardSocial + '<div class="card-title m-0">';
                    cardSocial = cardSocial + '<div class="symbol symbol-45px w-45px bg-light me-5" style="font-size: 21px;font-weight: bold;"><i class="fab fa-' + rrss + '' + (rrss != 'instagram' ? '-square' : '') + '"></i> ' + rrss + '</div></div>';
                    cardSocial = cardSocial + '<div class="card-body d-flex flex-column" style="padding: 0.05rem;">';
                    cardSocial = cardSocial + '<div class="fs-2tx fw-bolder mb-2">Budget $000.00</div>';

                    cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '<div class="fw-bolder text-gray-800 me-2"><strong>Ad Account:&nbsp;</strong> </div>';
                    cardSocial = cardSocial + '<div class="fw-bold text-gray-400">' + $('select[name="adsaccount[' + $(this).val() + ']"] option:selected').html() + '</div>';
                    cardSocial = cardSocial + '</div>';

                    cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '<div class="fw-bolder text-gray-800 me-2"><strong>Property:&nbsp;</strong> </div>';
                    cardSocial = cardSocial + '	<div class="fw-bold text-gray-400">' + $('select[name="properties[' + $(this).val() + ']"] option:selected').html() + '</div>';
                    cardSocial = cardSocial + '	</div>';
                    cardSocial = cardSocial + linea;


                    $('input[name="objetive[' + $(this).val() + ']"]:checked').each(function() {
                        //str = str + "<li><b>Objetive: </b>" + $(this).val() + "</li>";
                        cardSocial = cardSocial + '	<div class="d-flex align-items-center flex-wrap">';
                        cardSocial = cardSocial + '		<div class="fw-bolder text-gray-800 me-2"><strong>Objective:&nbsp;</strong> </div>';
                        cardSocial = cardSocial + '		<div class="fw-bold text-gray-400">' + $(this).attr('valor') + '</div>';
                        cardSocial = cardSocial + '	</div>';
                    });

                    cardSocial = cardSocial + '	<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '		<div class="fw-bolder text-gray-800 me-2"><strong>Audience</strong></div>';
                    cardSocial = cardSocial + '		<hr/>	';
                    cardSocial = cardSocial + '	</div>';

                    cardSocial = cardSocial + linea;
                    cardSocial = cardSocial + '<div class="fw-bolder text-gray-800 me-2"><strong>Interest</strong></div>';

                    cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '<div class="fw-bold text-gray-800 titulo" style="padding-left: 5px;">Include</div>';
                    cardSocial = cardSocial + '</div>';
                    $(".intereses-list." + rrss + ">li").each(function(index) {
                        if ($(this).attr('class') == 'IN') {
                            //Interests_IN.push($(this).attr('data-name'));
                            cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                            cardSocial = cardSocial + '<div class="fw-bold text-gray-400 tituloItem">* ' + $(this).attr('data-name') + '</div>';
                            cardSocial = cardSocial + '</div>';
                        }
                    });


                    cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '<div class="fw-bold text-danger" style="padding-left: 5px;">Exclude</div>';
                    cardSocial = cardSocial + '</div>';
                    $(".intereses-list." + rrss + ">li").each(function(index) {
                        if ($(this).attr('class') == 'EX') {
                            //Interests_IN.push($(this).attr('data-name'));
                            cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                            cardSocial = cardSocial + '<div class="fw-bold text-danger tituloItem">* ' + $(this).attr('data-name') + '</div>';
                            cardSocial = cardSocial + '</div>';
                        }
                    });



                    cardSocial = cardSocial + linea;

                    cardSocial = cardSocial + '<div class="fw-bolder text-gray-800 me-2"><strong>Custom Audience</strong></div>';

                    cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '<div class="fw-bold text-gray-800 titulo" style="padding-left: 5px;">Include</div>';
                    cardSocial = cardSocial + '</div>';
                    $(".audiencia-list." + rrss + ">li").each(function(index) {
                        if ($(this).attr('class') == 'IN') {
                            cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                            cardSocial = cardSocial + '<div class="fw-bold text-gray-400 tituloItem">* ' + $(this).attr('data-name') + '</div>';
                            cardSocial = cardSocial + '</div>';
                        }
                    });

                    cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '<div class="fw-bold text-danger" style="padding-left: 5px;">Exclude</div>';
                    cardSocial = cardSocial + '</div>';
                    $(".audiencia-list." + rrss + ">li").each(function(index) {
                        if ($(this).attr('class') == 'EX') {
                            cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                            cardSocial = cardSocial + '<div class="fw-bold text-danger tituloItem">* ' + $(this).attr('data-name') + '</div>';
                            cardSocial = cardSocial + '</div>';
                        }
                    });



                    cardSocial = cardSocial + linea;
                    cardSocial = cardSocial + '<div class="fw-bolder text-gray-800 me-2"><strong>Location</strong></div>';
                    cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '<div class="fw-bold text-gray-800 titulo" style="padding-left: 5px;">Include</div>';
                    cardSocial = cardSocial + '</div>';
                    $("#location-list-" + rrss + ">li").each(function(index) {
                        if ($(this).attr('class') == 'IN') {
                            cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                            cardSocial = cardSocial + '<div class="fw-bold text-gray-400 tituloItem">* ' + $(this).attr('data-location-option') + '</div>';
                            cardSocial = cardSocial + '</div>';
                        }
                    });

                    cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '<div class="fw-bold text-danger" style="padding-left: 5px;">Exclude</div>';
                    cardSocial = cardSocial + '</div>';
                    $("#location-list-" + rrss + ">li").each(function(index) {
                        if ($(this).attr('class') == 'EX') {
                            cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                            cardSocial = cardSocial + '<div class="fw-bold text-danger tituloItem">* ' + $(this).attr('data-location-option') + '</div>';
                            cardSocial = cardSocial + '</div>';
                        }
                    });


                    cardSocial = cardSocial + linea;
                    cardSocial = cardSocial + '<div class="fw-bolder text-gray-800 me-2"><strong>Placements</strong></div>';
                    $('input[name="placement[' + rrss + '][]"]:checked').each(function(index) {
                        cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                        cardSocial = cardSocial + '<div class="fw-bold text-gray-800 titulo" style="padding-left: 5px;">' + $(this).attr('valor') + '</div>';
                        cardSocial = cardSocial + '</div>';
                    });


                    cardSocial = cardSocial + linea;
                    cardSocial = cardSocial + '<div class="fw-bolder text-gray-800 me-2"><strong>Budget & Bidding</strong></div>';

                    if ($('input[name="br' + rrss + '"]:checked').val() == 1) {
                        var bbStr = "<b>Price: </b>" + $('#price-' + rrss).html() + $('input[name="bi' + rrss + '"]').val() + "<br>";
                    } else {
                        var bbStr = "<b>Price: </b>" + $('#price-' + rrss).html() + $('input[name="bp' + rrss + '"]').val() + "<br>";
                    }

                    cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '<div class="fw-bold text-gray-800 titulo" style="padding-left: 5px;">' + bbStr + '</div>';
                    cardSocial = cardSocial + '</div>';

                    cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '<div class="fw-bold text-gray-800 titulo" style="padding-left: 5px;">' + '<b>Optimize for: </b>' + $('select[name="optimize_' + rrss + '"] option:selected').html() + '</div>';
                    cardSocial = cardSocial + '</div>';


                    cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                    cardSocial = cardSocial + '<div class="fw-bold text-gray-800 titulo" style="padding-left: 5px;">' + '<b>Pay for: </b>' + $('select[name="pay_' + rrss + '"] option:selected').html() + '</div>';
                    cardSocial = cardSocial + '</div>';


                    if ($('input[name="bid_' + rrss + '"]:checked').val() == 1) {
                        cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                        cardSocial = cardSocial + '<div class="fw-bold text-gray-800 titulo" style="padding-left: 5px;"><b>Bidding: </b> Lowest cost <br></div>';
                        cardSocial = cardSocial + '</div>';
                    } else {
                        cardSocial = cardSocial + '<div class="d-flex align-items-center flex-wrap">';
                        cardSocial = cardSocial + '<div class="fw-bold text-gray-800 titulo" style="padding-left: 5px;">' + '<b>Bidding: </b>Lowest cost with bid cap ' + $('#price-' + rrss).html() + $('input[name="low_cost_top_' + rrss + '"]').val() + '</div>';
                        cardSocial = cardSocial + '</div>';
                    }

                    cardSocial = cardSocial + '</div>';
                    cardSocial = cardSocial + '</div>';
                    cardSocial = cardSocial + '</div>';
                    cardSocial = cardSocial + '</div>';
                    cardSocial = cardSocial + '</div>';


                    $("#cards_redes_sociales").append(cardSocial);

                });




                //  **** Start.Begin Summary: Step 1 - Create Campaign ***

                /*		$("#resumen_step1").html('');
		
						if($('input[name="campaign_name"]').val() != ""){
							$("#resumen_step1").append("<b>Campaign name: </b>"+ $('input[name="campaign_name"]').val()+"<br>");
						}
		
						if($('select[name="client"]').val() != ""){
							$("#resumen_step1").append("<b>Client: </b>"+ $('select[name="client"] option:selected').html()+"<br>");
						}
		
						$('input[name="rrss[]"]:checked').each( function() {
		
							var str = "";
		
							if($('select[name="adsaccount[' + $(this).val() + ']"]').val() != ""){
								str = str + "<li><b>Ad Account: </b>" + $('select[name="adsaccount[' + $(this).val() + ']"] option:selected').html() + "</li>";
							}
							if($('select[name="properties[' + $(this).val() + ']"]').val() != ""){
								str = str + "<li><b>Property: </b>" + $('select[name="properties[' + $(this).val() + ']"] option:selected').html() + "</li>";
							}
							$('input[name="objetive[' + $(this).val() + ']"]:checked').each(function() {
								str = str + "<li><b>Objetive: </b>" + $(this).val() + "</li>";
							});
		
							$("#resumen_step1").append("<b>RRSS: </b>" + $(this).val() + "<ul>" + str + "</ul>");
						});
						
						//  **** end.Begin Summary: Step 1 - Create Campaign ***
		
						//  **** Begin Summary: Step 2 - Audience ***
		
						$("#resumen_step2").html('');
		
		
			
		
		
						var AGE_RANGES = [];
						$( ".input-daterange.input-group" ).each(function( index ) {
							if($('input[name="age[' + index + '][start]"]').val()) {
								AGE_RANGES.push([$('input[name="age[' + index + '][start]"]').val(), $('input[name="age[' + index + '][end]"]').val()] );
							}
						});
		
						if( AGE_RANGES.length != 0) {
							str = str + "<b>Age Range: </b>";
							AGE_RANGES.map( (range) => str = str + range.join('-') + ", " );
						}
		
						$("#resumen_step2").append("<b>Demographics</b><div style='margin-left:20px;'>"+ str +"</div>");
		
		
		
						$('input[name="rrss[]"]:checked').each( function() {
							str = "";
								var rrss = $(this).val();
								//str = str + "<b>"+'Location' + ": </b><br>";
		
								var LOCATION_IN = [], LOCATION_OUT = [];
								$( "#location-list-"+rrss+" >li" ).each(function( index ) {
		
									if($(this).attr('class') == 'IN'){
										LOCATION_IN.push($(this).attr('data-name') + ' (' +$(this).attr('data-location-option')  + ')');
									}else{
										LOCATION_OUT.push($(this).attr('data-name') + ' (' +$(this).attr('data-location-option')  + ')');
									}
		
								});
								if(LOCATION_IN.length != 0) {
									str = str + "<b>Include Location: </b>" + LOCATION_IN.join(', ') + "<br>";
								}
								if(LOCATION_OUT.length != 0) {
									str = str + "<b>Exclude Location: </b>" + LOCATION_OUT.join(', ') + "<br>";
								}
		
		
							//location_option
		
								var LANGUAGES = [];
								$( "#language-list>li" ).each(function( index ) {
									LANGUAGES.push($(this).attr('data-name'));
								});
		
								if(LANGUAGES.length != 0) {
									str = str + "<b>Language: </b>" + LANGUAGES.join(', ') + "<br>";
								}
		
		
								$("#resumen_step2").append("<b>"+rrss.toUpperCase()+"</b><div style='margin-left:20px;'>"+ str +"</div>");
		
		
								str="";
		
		
							str = "";
							var DEVICE = 'All';
							if ($('input[name="devices[' +rrss+ ']"]:checked').val() == '5'){
								str = str + "<b>Devices: </b>" + DEVICE + "<br>";
							}
							else {
								str = str + "<b>Devices: </b>" + $('input[name="devices[' +rrss+ ']"]:checked').val() + "<br>";
							}
		
							var CONECTIVY = 'All'
							if ($('input[name="conectivity[' +rrss+ ']"]:checked').val() == '5'){
								str = str + "<b>Conectivity: </b>" + CONECTIVY + "<br>";
							}else{
								str = str + "<b>Conectivity: </b>" + $('input[name="conectivity[' +rrss+ ']"]:checked').val() + "<br>";
							}
		
							$("#resumen_step2").append("<b>Technology</b><div style='margin-left:20px;'>"+ str +"</div>");
		
		
							str='';
								var Interests_IN = [], Interests_OUT = [];
								$( ".intereses-list."+rrss+">li" ).each(function( index ) {
									if($(this).attr('class') == 'IN'){
										Interests_IN.push($(this).attr('data-name'));
									}else{
										Interests_OUT.push($(this).attr('data-name'));
									}
								});
								if(Interests_IN.length != 0) {
									str = str + "<b>Include Interests: </b>" + Interests_IN.join(', ') + "<br>";
								}
								if(Interests_OUT.length != 0) {
									str = str + "<b>Exclude Interests: </b>" + Interests_OUT.join(', ') + "<br>";
								}
		
		
								var Audience_IN = [], Audience_OUT = [];
								$( ".audiencia-list."+rrss+">li" ).each(function( index ) {
		
									if($(this).attr('class') == 'IN'){
										Audience_IN.push($(this).attr('data-name'));
									}else{
										Audience_OUT.push($(this).attr('data-name'));
									}
		
								});
								if(Audience_IN.length != 0) {
									str = str + "<b>Include Audience: </b>" + Audience_IN.join(', ') + "<br>";
								}
								if(Audience_OUT.length != 0) {
									str = str + "<b>Exclude Audience: </b>" + Audience_OUT.join(', ') + "<br>";
								}
							}
						);
		
						$("#resumen_step2").append("<b>Detailed targetin</b><div style='margin-left:20px;'>"+ str +"</div>");
		
		
						//  **** End Summary: Step 2 - Audience ***
		
						//  **** Begin Summary: Step 3 - Placement ***
						//$("#resumen_step3").html('<h3>Placement</h3>');
						$("#resumen_step3").html('');
		
						$('input[name="rrss[]"]:checked').each( function() {
							var rrss = $(this).val();
		
							var placement = [];
							$( 'input[name="placement['+rrss+'][]"]:checked' ).each(function( index ) {
								placement.push($(this).val());
							});
							if(placement.length != 0) {
								$("#resumen_step3").append("<b>"+rrss+": </b>" + placement.join(', ') + "<br>");
							}
						});
						//  **** End Summary: Step 3 - Placement ***
						//  **** Begin Summary: Step 4 - Ads Design ***
		
						//$("#resumen_step4").html('<h3>Ads Design</h3>');
						$("#resumen_step4").html('');
		
						if($( 'input[name="rad__adsdesign"]:checked' ).val() == 1){
							$("#resumen_step4").append("<h4>Create</h4>");
						}else{
							$("#resumen_step4").append("<h4>Latest Post</h4>");
						}
		
						//  **** Begin Summary: Step 4.1 - Ads Design ***
		
						$("#resumen_step4").append('<b>Headlines</b><br>');
		
						var flag = true;
						var i = 0;
						while (flag) {
							if($('input[name="titulo['+i+'][titulo]"]').val() != undefined){
								$("#resumen_step4").append($('input[name="titulo['+i+'][titulo]"]').val());
								if($('input[name="titulo['+i+'][titulotags]"]').val() != ""){
									$("#resumen_step4").append(" Tags:"+$('input[name="titulo['+i+'][titulotags]"]').val());
								}
								i++;
		
								$("#resumen_step4").append("<br>");
							}else{
								flag = false;
							}
						}
		
						$("#resumen_step4").append('<b>Ad Texts</b><br>');
		
						flag = true;
						i = 0;
						while (flag) {
							if($('textarea[name="descripcion['+i+'][descripcion]"]').val() != undefined){
								$("#resumen_step4").append($('textarea[name="descripcion['+i+'][descripcion]"]').val());
								if($('input[name="descripcion['+i+'][descripciontags]"]').val() != ""){
									$("#resumen_step4").append(" Tags:"+$('input[name="descripcion['+i+'][descripciontags]"]').val());
								}
								i++;
								$("#resumen_step4").append("<br>");
							}else{
								flag = false;
							}
						}
		
		
						$("#resumen_step4").append('<b>Upload new images</b><br>');
		
						flag = true;
						i = 0;
		
						while (flag) {
							if($('input[name="imagen['+i+'][imagen]"]').val() != undefined){
								$("#resumen_step4").append($('input[name="imagen['+i+'][imagen]"]').val());
								if($('input[name="imagen['+i+'][imagentags]"]').val() != ""){
									$("#resumen_step4").append(" Tags:"+$('input[name="imagen['+i+'][imagentags]"]').val());
								}
								i++;
								$("#resumen_step4").append("<br>");
							}else{
								flag = false;
							}
						}
		
						$("#resumen_step4").append('<b>URLs</b><br>');
		
						flag = true;
						i = 0;
						while (flag) {
							if($('input[name="url['+i+'][url]"]').val() != undefined){
								$("#resumen_step4").append($('input[name="url['+i+'][url]"]').val());
								if($('input[name="url['+i+'][urltags]"]').val() != ""){
									$("#resumen_step4").append(" Tags:"+$('input[name="url['+i+'][urltags]').val());
								}
								i++;
								$("#resumen_step4").append("<br>");
							}else{
								flag = false;
							}
		
						}
		
						if($('input[name="link_display"]').val() != ""){
							$("#resumen_step4").append("<b>Display Link: </b>"+ $('input[name="link_display"]').val()+"<br>");
						}
		
						$("#resumen_step4").append('<b>Link Description</b><br>');
		
						flag = true;
						i = 0;
						while (flag) {
							if($('textarea[name="text_link['+i+'][text_link]"]').val() != undefined){
								$("#resumen_step4").append($('textarea[name="text_link['+i+'][text_link]"]').val());
								if($('input[name="text_link['+i+'][text_linktags]"]').val() != ""){
									$("#resumen_step4").append(" Tags:"+$('input[name="text_link['+i+'][text_linktags]').val());
								}
								i++;
							}else{
								flag = false;
							}
							$("#resumen_step4").append("<br>");
						}
		
						$("#resumen_step4").append('<b>Call to Action</b><br>');
		
						flag = true;
						i = 0;
						while (flag) {
							if($('select[name="call_to_action['+i+'][call_to_action]"]').val() != undefined){
								$("#resumen_step4").append($('select[name="call_to_action['+i+'][call_to_action]"] option:selected').html());
								if($('input[name="call_to_action['+i+'][call_to_actiontags]"]').val() != ""){
									$("#resumen_step4").append(" Tags:"+$('input[name="call_to_action['+i+'][call_to_actiontags]').val());
								}
								i++;
								$("#resumen_step4").append("<br>");
							}else{
								flag = false;
							}
						}
						//  **** End Summary: Step 4.1- Ads Design ***
		
						//  **** Begin Summary: Step 4.2 - Ads Design ***
		
						//  **** End Summary: Step 4.2 - Ads Design ***
		
						//  **** Begin Summary: Step 4.3 - Ads Design ***
		
						//  **** End Summary: Step 4.3 - Ads Design ***
		
						//  **** End Summary: Step 4 - Ads Design ***
						//  **** Begin Summary: Step 5 - Budget & Bidding ***
						//$("#resumen_step5").html('<h3>Budget & Bidding</h3>');
						$("#resumen_step5").html('');
		
						if($('input[name="budget"]').val() != ""){
							var budgetdistribution = "";
							if($( 'input[name="budgetdistribution"]:checked' ).val() == 1){
								budgetdistribution = "Per day";
							}else{
								budgetdistribution = "Total";
							}
							$("#resumen_step5").append("<b>Budget: </b>"+ $('input[name="budget"]').val()+" " +budgetdistribution+ "<br>");
						}
		
						$('input[name="rrss[]"]:checked').each( function() {
							var rrss = $(this).val();
		
							var str = "";
		
							if( $('input[name="br'+rrss+'"]:checked').val() == 1){
								str = str + "<b>Price: </b>" + $('input[name="bi'+rrss+'"]').val() + "<br>";
							}else{
								str = str + "<b>Price: </b>" + $('input[name="bp'+rrss+'"]').val() + "<br>";
							}
		
							str = str + "<b>Optimize for: </b>"+ $('select[name="optimize_'+rrss+'"] option:selected').html()+"<br>";
							str = str + "<b>Pay for: </b>"+ $('select[name="pay_'+rrss+'"] option:selected').html()+"<br>";
		
							if( $('input[name="bid_'+rrss+'"]:checked').val() == 1){
								str = str + "<b>Bidding: </b> Lowest cost <br>";
							}else{
								str = str + "<b>Bidding: </b>Lowest cost with bid cap "+$('input[name="low_cost_top_'+rrss+'"]').val()+"<br>";
							}
		
		
							$("#resumen_step5").append("<b>"+rrss+":</b><div style='margin-left:20px;'>"+ str +"</div>");
		
		
						});
		
						$("#resumen_step5").append("<b>Start Date: </b>"+$('input[name="start_date"]').val()+"<br>");
						$("#resumen_step5").append("<b>End Date: </b>"+$('input[name="end_date"]').val()+"<br>");
		
						if( $('input[name="budget_target_diary"]').val() != ""){
							$("#resumen_step5").append("<b>Budget target diary: </b>"+$('input[name="budget_target_diary"]').val()+" "+$('select[name="budget_target_diary_type"] option:selected').html()+"<br>");
						}
		
						if( $('input[name="budget_target_total"]').val() != ""){
							$("#resumen_step5").append("<b>Budget target total: </b>"+$('input[name="budget_target_total"]').val()+" "+$('select[name="budget_target_total_type"] option:selected').html()+"<br>");
						}
		
						var schedule = $("#weekly-schedule").data('artsy.dayScheduleSelector').serialize();
		
						var days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
						var str_schedule = "";
						$.each(schedule, function( index, value ) {
							if(value.length>0){
								var i = 0;
								jQuery.map( value, function( a ) {
									if(i == 0){
										str_schedule=str_schedule +"<tr><td style='padding: 5px;'><b>"+days[index] + "</b></td><td style='padding: 5px;'> From "+a[0]+" to "+a[1]+"</td></tr>";
									}else{
										str_schedule=str_schedule +"<tr><td></td><td style='padding: 5px;'> From "+a[0]+" to "+a[1]+"</td></tr>";
									}
									i++;
								});
							}
						});
						if(str_schedule != ""){
							$("#resumen_step5").append("<b>Run only on:</b><br> <table style='font-size: 14px;'>"+str_schedule+"</table>");
						}
		
						if( $('input[name="spend_on_each_ad"]').val() != ""){
							$("#resumen_step5").append("<b>Spend on each ad: </b>"+$('input[name="spend_on_each_ad"]').val()+"<br>");
						}
		
						if( $('input[name="impressions_per_day"]').val() != ""){
							$("#resumen_step5").append("<b>Impressions per day: </b>"+$('input[name="impressions_per_day"]').val()+"<br>");
						}
						
						
						$('input[name="budget_target_diary"]').val()
						$('select[name="budget_target_diary_type"] option:selected').html()
		
						$('input[name="budget_target_total"]').val()
						$('select[name="budget_target_total_type"] option:selected').html()
		
						$('input[name="spend_on_each_ad"]').val()
						$('input[name="impressions_per_day"]').val()
		
						/*if( $('input[name="spend_cap"]').val() != ""){
							$("#resumen_step5").append("<b>Spend Cap: </b>"+$('input[name="spend_cap"]').val()+"<br>");
						}*/

                //  **** End Summary: Step 5 - Budget & Bidding ***


            }

        });

        // Change event
        wizard.on('change', function(wizard) {
            setTimeout(function() {
                KTUtil.scrollTop();
            }, 500);
        });
    }

    var initValidation = function() {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {

                'campaign_name': {
                    required: true
                },
                // Step 4.a
                'titulo[0][titulo]': {
                    required: true
                },
                'descripcion[0][descripcion]': {
                    required: true
                },
                'url[0][url]': {
                    required: true
                },
                'link_display': {
                    required: true
                },
                'text_link[0][text_link]': {
                    required: true
                },
                'call_to_action[0][call_to_action]': {
                    required: true
                },
                'GA_source': {
                    required: true
                },
                'GA_medium': {
                    required: true
                },
                'GA_campaign': {
                    required: true
                },
                'GA_content': {
                    required: true
                },
                'url_parameter_extra': {
                    required: true
                },
                'pixelview': {
                    required: true
                },
                'pixel[0][fbpixel]': {
                    required: true
                },
                // Step 4.b
                'page': {
                    required: true
                },

                // Step 4.b.a
                'posts': {
                    required: true
                },

                // Step 4.b.b
                'url': {
                    required: true
                },
                't_postLink': {
                    required: true
                },
                'posti[0][t_promotePost]': {
                    required: true
                },
                'posti[0][se_promotePost]': {
                    required: true
                },

                //Step 5
                'budget': {
                    required: true
                },
                'start_date': {
                    required: false
                },
                'end_date': {
                    required: true
                }
            },

            errorPlacement: function(error, element) {
                var name = $(element).attr("name");
                var $obj = $("#" + name + "_error_validate");
                if ($obj.length) {
                    error.appendTo($obj).addClass('invalid-feedback');
                } else {
                    error.insertAfter(element).addClass('invalid-feedback');

                }
            },
            //<div id="campaign_name-error" class="error invalid-feedback ">This field is required.</div>

            // Display error
            invalidHandler: function(event, validator) {
                KTUtil.scrollTop();

                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary"
                });
            },

            // Submit valid form
            submitHandler: function(form) {

            }
        });
    }

    $.validator.addMethod("ageValid", function(value, element) {
        let name = element.name;
        let i = parseInt(name.replace('age[', '').replace('][start]', ''));
        let previous_value_int = 0;
        if (i >= 1) {
            i--;
            previous_value_int = $('[name="age[' + i + '][end]"]').val();
        }
        if (value > previous_value_int) {
            return true
        }
        return false;
    }, 'Number must be greater than the previous');

    $.validator.addMethod("ageBigger", function(value, element) {
        let name = element.name;
        let i = parseInt(name.replace('age[', '').replace('][start]', '').replace('][end]', ''));
        let start_value_int = $('[name="age[' + i + '][start]"]').val();
        let end_value_int = $('[name="age[' + i + '][end]"]').val();
        if (end_value_int == '' || start_value_int == '') {
            return true;
        }
        if (end_value_int < start_value_int) {
            return false
        }
        return true;
    }, 'First number must be lower or same as the last');

    $.validator.addMethod("valueNotEquals", function(value, element, arg) {
        return arg !== value;
    }, "This field is required");

    const sleep = (milliseconds) => {
        return new Promise(resolve => setTimeout(resolve, milliseconds))
    }


    var initSubmit = function() {

        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on('click', function(e) {

            var status_campaign = $(this).attr('type_btn');

            e.preventDefault();

            if ($("[data-ktwizard-type='action-submit']").hasClass("block")) {
                var getUrl = window.location;
                window.location.href = getUrl.protocol + "//" + getUrl.host + "/mysubscription";
                return false;
            }


            if (status_campaign != 'draft') {
                if (!validator.form()) {
                    return false;
                }
            }

            // See: src\js\framework\base\app.js
            //KTApp.block(formEl);
            KTApp.progress(btn);

            let formData = new FormData();

            var processData = new Promise((resolve, reject) => {
                formData.append("client", $('#select_client').val());
                formData.append("gender", $('input[name="gender"]:checked').val());
                formData.append("campaign_name", $('input[name="campaign_name"]').val());


                let ages = [];
                $(".input-daterange.input-group").each(function(index) {
                    if ($('input[name="age[' + index + '][start]"]').val()) {
                        var item = { start: $('input[name="age[' + index + '][start]"]').val(), end: $('input[name="age[' + index + '][end]"]').val() };
                        ages.push(item);
                    }
                });
                formData.append("ages", JSON.stringify(ages));

                let devices = [];
                $('input[name="devices"]:checked').each(function(index) {
                    devices.push($('input[name="devices"]:checked').val());
                });
                formData.append("devices", JSON.stringify(devices));

                let conectivity = [];
                $('input[name="conectivity"]:checked').each(function(index) {
                    conectivity.push($('input[name="conectivity"]:checked').val());
                });
                formData.append("conectivity", JSON.stringify(conectivity));

                let language = [];
                $("#language-list>li").each(function(index) {
                    language.push($(this).attr('data-id'));
                });
                formData.append("language", JSON.stringify(language));

                let target = [];
                $('input[name="target"]').each(function() {
                    var item = { id: $(this).attr('data-id'), name: $(this).is(':checked') };
                    target.push(item);
                });
                formData.append("target", JSON.stringify(target));


                let run_only_on_schedule = $("#weekly-schedule").data('artsy.dayScheduleSelector').serialize();
                formData.append("run_only_on_schedule", JSON.stringify(run_only_on_schedule));


                //imagenes and inputs tags
                for (var key in window.images_uploaded) {
                    window.images_uploaded[key].tags = $("input[name='imagen[" + key + "][imagentags]']").tagsinput('items');
                }
                formData.append("images_uploaded", JSON.stringify(window.images_uploaded));

                //start.autopost
                // creamos o latest post
                $('input[name=rad__adsdesign]:checked').val() // 1 creamos 2 latest
                formData.append("action_creas", $('input[name=rad__promote]:checked').val()); // 1 al 2

                formData.append("howPromoted", $('input[name=rad__promote]:checked').val()); // 1 al 2
                formData.append("typePromoted", $('input[name=aPromoted]:checked').val()); // 1 al 3
                formData.append("postPromoted", $('input[name=aPosts]:checked').val()); // 1 al 5

                formData.append("ganalitycs", $('#ganalitycs').val()); // 1 al 5

                formData.append("promoteForValue", $('#promoteForType').val()); // cantidad de dais o dinero
                formData.append("promoteForType", $('#promoteForType').val()); // si es dias o dinero

                formData.append("t_postLink", $('#t_postLink').val()); // cantidad de dais o dinero
                formData.append("t_postText", $('#t_postText').val()); // si es dias o dinero


                if ($('input[name=aPromoted]:checked').val() == 2) {
                    // es google analitycs
                    var flag = true;
                    var i = 0;
                    while (flag) {
                        if ($('input[name="posti[' + i + '][t_promotePost]"]').val() != undefined) {
                            var item = { value: $('input[name="posti[' + i + '][t_promotePost]"]').val(), tipo: $('input[name="posti[' + i + '][se_promotePost_GA]"]').val() };
                            formData.append("ga_filter_post[]", JSON.stringify(item));
                            i++;
                        } else {
                            flag = false;
                        }
                    }
                }

                if ($('input[name=aPromoted]:checked').val() == 3) {
                    var flag = true;
                    var i = 0;
                    while (flag) {
                        if ($('input[name="posti[' + i + '][t_promotePost]"]').val() != undefined) {
                            var item = { value: $('input[name="posti[' + i + '][t_promotePost]"]').val(), tipo: $('input[name="posti[' + i + '][se_promotePost]"]').val() };
                            formData.append("latestpost_filter_post[]", JSON.stringify(item));
                            i++;
                        } else {
                            flag = false;
                        }
                    }
                }

                //end.autopost







                var headlines = [];
                var flag = true;
                var i = 0;
                while (flag) {
                    if ($('input[name="titulo[' + i + '][titulo]"]').val() != undefined) {
                        var item = { value: $('input[name="titulo[' + i + '][titulo]"]').val(), tags: $('input[name="titulo[' + i + '][titulotags]"]').val() };
                        headlines.push(item);
                        formData.append("headlines[]", JSON.stringify(item));
                        i++;
                    } else {
                        flag = false;
                    }
                }


                var ad_texts = [];
                flag = true;
                i = 0;
                while (flag) {
                    if ($('textarea[name="descripcion[' + i + '][descripcion]"]').val() != undefined) {
                        var item = { value: $('textarea[name="descripcion[' + i + '][descripcion]"]').val(), tags: $('input[name="descripcion[' + i + '][descripciontags]"]').val() };
                        formData.append("ad_texts[]", JSON.stringify(item));

                        i++;
                    } else {
                        flag = false;
                    }
                }

                var urls = [];
                flag = true;
                i = 0;
                while (flag) {
                    if ($('input[name="url[' + i + '][url]"]').val() != undefined) {
                        var item = { value: $('input[name="url[' + i + '][url]"]').val(), tags: $('input[name="url[' + i + '][urltags]"]').val() };
                        formData.append("urls[]", JSON.stringify(item));
                        i++;
                    } else {
                        flag = false;
                    }
                }

                flag = true;
                i = 0;
                while (flag) {
                    if ($('textarea[name="text_link[' + i + '][text_link]"]').val() != undefined) {
                        var item = { value: $('textarea[name="text_link[' + i + '][text_link]"]').val(), tags: $('input[name="text_link[' + i + '][text_linktags]"]').val() };
                        formData.append("text_links[]", JSON.stringify(item));
                        console.log(item);
                        i++;
                    } else {
                        flag = false;
                    }
                }


                formData.append("link_display", $('input[name="link_display"]').val());


                var call_action = [];
                flag = true;
                i = 0;
                while (flag) {
                    if ($('select[name="call_to_action[' + i + '][call_to_action]"]').val() != undefined) {
                        var item = { value: $('select[name="call_to_action[' + i + '][call_to_action]"]').val(), tags: $('input[name="call_to_action[' + i + '][call_to_actiontags]"]').val() };
                        call_action.push(item);
                        formData.append("call_actions[]", JSON.stringify(item));

                        i++;
                    } else {
                        flag = false;
                    }
                }

                let rrss = null;
                let rrss_list = [];


                $('input[name="rrss[]"]:checked').each(function() {
                    rrss_list.push($(this).val());
                });

                formData.append("rrss_list", JSON.stringify(rrss_list));

                let ads_accounts_list = [];
                let ads_accounts_properties_list = [];

                for (i = 0; i < rrss_list.length; i++) {
                    ads_accounts_list.push($('select[name="adsaccount[' + rrss_list[i] + ']"]').val());
                    ads_accounts_properties_list.push($('select[name="properties[' + rrss_list[i] + ']"]').val());
                }

                formData.append("ads_accounts", JSON.stringify(ads_accounts_list));

                formData.append("ads_accounts_properties", JSON.stringify(ads_accounts_properties_list));


                var i;
                let placement = [];
                for (i = 0; i < rrss_list.length; i++) {
                    $('input[name="placement[' + rrss_list[i] + '][]"]:checked').each(function(index) {
                        placement.push($(this).val());
                    });
                }
                formData.append("placement", JSON.stringify(placement));



                var i;
                let location_in = [];
                let location_out = [];

                for (i = 0; i < rrss_list.length; i++) {
                    location_in[rrss_list[i]] = [];
                    location_out[rrss_list[i]] = [];
                    $("#location-list-" + rrss_list[i] + ">li").each(function(index) {
                        var type = $(this).attr('class');
                        var item = { id: $(this).attr('data-id'), name: $(this).attr('data-name'), platform: rrss_list[i] };

                        if (type == 'IN') {
                            formData.append("location_in[]", JSON.stringify(item));
                            console.log('location_in', JSON.stringify(item))
                        } else {
                            formData.append("location_out[]", JSON.stringify(item));
                            console.log('location_out', JSON.stringify(item))
                                //location_out[rrss_list[i]].push($(this).attr('data-id'));
                        }
                    });
                }

                var i;
                let interest_list = [];

                for (i = 0; i < rrss_list.length; i++) {

                    $(".intereses-list." + rrss_list[i] + ">li").each(function(index) {
                        var type = $(this).attr('class');
                        var item = { id: $(this).attr('data-id'), name: $(this).attr('data-name'), type: $(this).attr('class') };
                        formData.append("interest_list_" + rrss_list[i] + "[]", JSON.stringify(item));

                    });
                }

                var i;
                let audiencie_list = [];

                for (i = 0; i < rrss_list.length; i++) {

                    $(".audiencia-list." + rrss_list[i] + ">li").each(function(index) {
                        var type = $(this).attr('class');
                        var item = { id: $(this).attr('data-id'), name: $(this).attr('data-name'), type: $(this).attr('class') };
                        formData.append("audiencia_list_" + rrss_list[i] + "[]", JSON.stringify(item));
                        console.log('audiencie_list', JSON.stringify(item));

                        /*if(type == 'IN'){
                        	audiencie_list[rrss_list[i]]['IN'].push(item);
                        }else{
                        	audiencie_list[rrss_list[i]]['EX'].push(item);
                        }*/
                    });
                }


                var i;
                let pixeles = [];

                for (i = 0; i < rrss_list.length; i++) {
                    $('select[name="pixels[' + rrss_list[i] + ']"]').each(function(index) {
                        if ($(this).val() != '') {
                            pixeles.push($(this).val());
                        }
                    });
                }
                //console.log('pixeles', pixeles)
                formData.append("pixeles", JSON.stringify(pixeles));

                var i;
                let budget_split = [];
                let tipoBudget;
                let tipoBudgetStr;

                for (i = 0; i < rrss_list.length; i++) {
                    budget_split[rrss_list[i]] = [];

                    tipoBudget = $('input[name=br' + rrss_list[i] + ']:checked').val();


                    if ($('input[name=br' + rrss_list[i] + ']:checked').val() == 1) {

                        tipoBudgetStr = 'by_fixed';
                    } else {
                        tipoBudgetStr = 'by_percentage';
                    }

                    var item = {
                        platform: rrss_list[i],
                        budgetType: tipoBudget,
                        budgetTypeStr: tipoBudgetStr,
                        fixed_amount: $('#bi' + rrss_list[i]).val(),
                        percentaje: $('#bp' + rrss_list[i]).val(),
                        optimize: $('#optimize_' + rrss_list[i]).val(),
                        pay: $('#pay_' + rrss_list[i]).val(),
                        bid_type: { id: $('input[name=br' + rrss_list[i] + ']:checked').val(), value: $('#input_low_cost_top_' + rrss_list[i]).val() }
                    };

                    formData.append("budget_split[]", JSON.stringify(item));
                    budget_split.push(item);

                }
                //console.log('budget_split_all', budget_split)
                formData.append("budget_split_all", JSON.stringify(budget_split));


                if ($('#GA_source').val() != '' && $('#GA_medium').val() != '') {
                    let itm_googleAnalytics = { 'GA_source': $('#GA_source').val(), 'GA_medium': $('#GA_medium').val(), 'GA_campaign': $('#GA_campaign').val(), 'GA_content_placeholder': $('#GA_content_placeholder').val() }
                        //console.log('itm_googleAnalytics', JSON.stringify(itm_googleAnalytics));
                    formData.append("itm_googleAnalytics", JSON.stringify(itm_googleAnalytics));
                }

                formData.append("url_parameter_extra", $('#url_parameter_extra').val());
                formData.append("pixelview", $('#pixelview').val());
                formData.append("budget", $('#budget').val());
                formData.append("budget_target_diary", $('#budget_target_diary').val());
                formData.append("budget_target_diary_type", $('#budget_target_diary_type').val());
                formData.append("budget_target_total", $('#budget_target_total').val());
                formData.append("budget_target_total_type", $('#budget_target_total_type').val());
                formData.append("spend_on_each_ad", $('#spend_on_each_ad').val());
                formData.append("impressions_per_day", $('#impressions_per_day').val());

                formData.append("internal_holder", $('input[name=IO]').val());
                formData.append("brand_name", $('input[name=brand_name]').val());

                let startDate = $('#start_date').val() == '' ? false : $('#start_date').val();
                let endDate = $('#end_date').val() == '' ? false : $('#end_date').val();

                formData.append("start_date", startDate);
                formData.append("end_date", endDate);

                formData.append("status", status_campaign);

                //sleep(400);

                resolve();
            });

            processData.then(() => {

                $.ajax({
                    url: '/api/createCampaign/',
                    data: formData,
                    contentType: false,
                    dataType: "json",
                    processData: false,
                    type: 'POST',
                    success: function(result) {

                        KTApp.unprogress(btn);

                        if (result.response.status) {
                            //KTApp.unblock(formEl);
                            swal.fire({
                                "title": "",
                                "text": "The application has been successfully submitted ! " + result.response.message,
                                "type": "success",
                                "confirmButtonClass": "btn btn-secondary"
                            });
                        } else {

                            var str_mss = '<ul>';

                            $.each(result.response.message, function(i, item) {
                                str_mss = str_mss + '<li>' + item + '</li>'
                            });
                            str_mss = str_mss + '</ul>'

                            KTUtil.scrollTop();
                            swal.fire({
                                "title": "",
                                "text": str_mss,
                                //"text": "There are some errors in your submission. Please correct them.",
                                "type": "error",
                                "confirmButtonClass": "btn btn-secondary"
                            });
                        }
                    },
                    error: function() {
                        KTApp.unprogress(btn);
                        KTUtil.scrollTop();
                        swal.fire({
                            "title": "",
                            "text": "There are some errors in your submission. Please correct them.",
                            "type": "error",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                    }
                });


            });




        });
    }


    return {
        // public functions
        init: function() {
            wizardEl = KTUtil.get('kt_wizard_v1');
            formEl = $('#kt_form');

            initWizard();
            initValidation();
            initSubmit();
        }
    };
}();

function ValidacionesEspeciales(display) {

    var error = false;
    //Step 1
    //Validamos las RRSS del paso 1
    if ($('input[name="rrss[]"]:checked').length == 0) {
        $('input[name="rrss[]"]').click(function() {
            if ($('input[name="rrss[]"]:checked').length == 0) {
                $('#ulrrss').css('border', "2px solid red");
                $('.error_rrss').show();
            } else {
                $('#ulrrss').css('border', "");
                $('.error_rrss').hide();
            }
        });

        $('#ulrrss').css('border', "2px solid red");
        $('.error_rrss').show();
        error = true;
    }

    //Validamos Objetivos Facebook
    if ($('input[name="objetive[facebook]"]:checked').length == 0 &&
        $('input[name="objetive[facebook]"]').is(':visible')) {
        $('input[name="objetive[facebook]"]').click(function() {
            if ($('input[name="objetive[facebook]"]:checked').length == 0) {
                $('#table_objetive_facebook').css('border', "2px solid red");
                $('.error_objetive_facebook').show();
            } else {
                $('#table_objetive_facebook').css('border', "");
                $('.error_objetive_facebook').hide();
            }
        });

        $('#table_objetive_facebook').css('border', "2px solid red");
        $('.error_objetive_facebook').show();
        error = true;
    }

    //Step 2
    //Validamos al menos 1 localidad incluida
    if ($("#location-list-facebook>li.IN").length == 0 &&
        $("#location-list-facebook").is(':visible')) {

        $('#location-list-facebook').bind('DOMSubtreeModified', function(event) {
            if ($("#location-list-facebook>li.IN").length == 0) {
                $('#divlocation').css('border', "2px solid red");
                $('.error_location').show();
            } else {
                $('#divlocation').css('border', "");
                $('.error_location').hide();
            }
        });

        $('#divlocation').css('border', "2px solid red");
        $('.error_location').show();
        error = true;
    }
    if ($("#location-list-linkedin>li.IN").length == 0 &&
        $("#location-list-linkedin").is(':visible')) {

        $('#location-list-linkedin').bind('DOMSubtreeModified', function(event) {
            if ($("#location-list-linkedin>li.IN").length == 0) {
                $('#divlocation').css('border', "2px solid red");
                $('.error_location').show();
            } else {
                $('#divlocation').css('border', "");
                $('.error_location').hide();
            }
        });

        $('#divlocation').css('border', "2px solid red");
        $('.error_location').show();
        error = true;
    }
    if ($("#location-list-twitter>li.IN").length == 0 &&
        $("#location-list-twittwe").is(':visible')) {

        $('#location-list-twitter').bind('DOMSubtreeModified', function(event) {
            if ($("#location-list-twitter>li.IN").length == 0) {
                $('#divlocation').css('border', "2px solid red");
                $('.error_location').show();
            } else {
                $('#divlocation').css('border', "");
                $('.error_location').hide();
            }
        });

        $('#divlocation').css('border', "2px solid red");
        $('.error_location').show();
        error = true;
    }

    //Validamos al menos 1 interes incluido
    if ($("#interes-list>li.IN").length == 0 &&
        $("#interes-list").is(':visible')) {

        $('#interes-list').bind('DOMSubtreeModified', function(event) {
            if ($("#interes-list>li.IN").length == 0) {
                $('#dt-facebook').css('border', "2px solid red");
                $('.error_interes').show();
            } else {
                $('#dt-facebook').css('border', "");
                $('.error_interes').hide();
            }
        });

        $('#dt-facebook').css('border', "2px solid red");
        $('.error_interes').show();
        error = true;
    }

    //Step 3
    //Validamos los placement facebook
    if ($('input[name="placement[facebook][]"]:checked').length == 0 &&
        $(".placements_facebook").is(':visible')) {

        $('input[name="placement[facebook][]"]').click(function() {
            if ($('input[name="placement[facebook][]"]:checked').length == 0) {
                $('.placements_facebook').css('border', "2px solid red");
                $('.error_placements_facebook').css('display', "inline-block");
            } else {
                $('.placements_facebook').css('border', "");
                $('.error_placements_facebook').css('display', "none");
            }
        });

        $('.placements_facebook').css('border', "2px solid red");
        $('.error_placements_facebook').css('display', "inline-block");
        error = true;
    }

    //Validamos los placement linkedin
    if ($('input[name="placement[linkedin][]"]:checked').length == 0 &&
        $(".placements_linkedin").is(':visible')) {

        $('input[name="placement[linkedin][]"]').click(function() {
            if ($('input[name="placement[linkedin][]"]:checked').length == 0) {
                $('.placements_linkedin').css('border', "2px solid red");
                $('.error_placements_linkedin').css('display', "inline-block");
            } else {
                $('.placements_linkedin').css('border', "");
                $('.error_placements_linkedin').css('display', "none");
            }
        });

        $('.placements_linkedin').css('border', "2px solid red");
        $('.error_placements_linkedin').css('display', "inline-block");
        error = true;
    }

    //Validamos los placement instagram
    if ($('input[name="placement[instagram][]"]:checked').length == 0 &&
        $(".placements_instagram").is(':visible')) {

        $('input[name="placement[instagram][]"]').click(function() {
            if ($('input[name="placement[instagram][]"]:checked').length == 0) {
                $('.placements_instagram').css('border', "2px solid red");
                $('.error_placements_instagram').css('display', "inline-block");
            } else {
                $('.placements_instagram').css('border', "");
                $('.error_placements_instagram').css('display', "none");
            }
        });

        $('.placements_instagram').css('border', "2px solid red");
        $('.error_placements_instagram').css('display', "inline-block");
        error = true;
    }


    //Step 4
    //Validamos que tengamos imagenes
    if ($('.dz-preview.dz-processing.dz-image-preview.dz-success.dz-complete').length == 0 &&
        $("#m-dropzone-three").is(':visible')) {

        $('#m-dropzone-three').on("addedfile", function(file) {
            if ($('.dz-preview.dz-processing.dz-image-preview.dz-success.dz-complete').length == 0) {
                $('#m-dropzone-three').css('border', "2px solid red");
                $('.error_dropzone').css('display', "inline-block");
            } else {
                $('#m-dropzone-three').css('border', "");
                $('.error_dropzone').css('display', "none");
            }
        });
        $('#m-dropzone-three').on("removedfile", function(file) {
            if ($('.dz-preview.dz-processing.dz-image-preview.dz-success.dz-complete').length == 0) {
                $('#m-dropzone-three').css('border', "2px solid red");
                $('.error_dropzone').css('display', "inline-block");
            } else {
                $('#m-dropzone-three').css('border', "");
                $('.error_dropzone').css('display', "none");
            }
        });



        $('#m-dropzone-three').css('border', "2px solid red");
        $('.error_dropzone').css('display', "inline-block");
        error = true;
    }

    if ($('#t_postText').val() == "" &&
        $("#t_postText").is(':visible')) {


        $('#t_postText').on('itemAdded', function(event) {
            $('#t_postText').css('border', "");
            $('.error_t_postText').css('display', "none");
        });

        $('#t_postText').on('itemRemoved', function(event) {
            if ($('#t_postText').val() == "") {
                $('#t_postText').css('border', "");
                $('.error_t_postText').css('display', "none");
            } else {
                $('#t_postText').css('border', "2px solid red");
                $('.error_t_postText').css('display', "inline-block");
            }
        });

        $('#t_postText').css('border', "2px solid red");
        $('.error_t_postText').css('display', "inline-block");
        error = true;
    }

    if (error) {
        if (display == true) {
            console.log("display error");
            displayerror();
        }
        return true;
    } else {
        return false;
    }
}

function displayerror(text = "") {
    KTUtil.scrollTop();
    if (text == "") {
        text = "There are some errors in your submission. Please correct them."
    }
    swal.fire({
        "title": "",
        "text": text,
        "type": "error",
        "confirmButtonClass": "btn btn-secondary"
    });
}

$(document).ready(function() {
    KTWizard1.init();
});