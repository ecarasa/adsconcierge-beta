<?php

use App\Post;
use App\PostSource;
use Faker\Factory;
use Illuminate\Database\Seeder;

class SeedPostsAndPostSources extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for($i=0; $i<=15; $i++)
        {
            $jsonPost = [];
            $jsonPost['prop-one'] = $faker->words(10);
            $jsonPost['prop-two'] = $faker->words(10);

            $post = new Post();

            $post->setting_id = $faker->randomNumber(2);
            $post->organic_posts = json_encode($jsonPost);
            $post->destination_url = $faker->url();
            $post->post_name = $faker->sentence(5);

            $post->save();

            for($j=0; $j<=3; $j++)
            {
                $postSource = new PostSource();

                $jsonSocial = [];
                $jsonSocial['engagements'] = $faker->words(5);
                $jsonSocial['reactions'] = $faker->words(5);

                $json = [];
                $json['tags'] = $faker->words(10);

                $postSource->posted_time = $faker->dateTimeBetween('-1 month', '+3 months');
                $postSource->status = $faker->randomElement(['POSTED', 'CREATED', 'SCHEDULED']); // $postSource->status = $faker->randomElement(['POSTED', 'CREATED', 'SCHEDULED', 'EXPIRED', 'DELETED']);
                $postSource->platform = $faker->randomElement(['FACEBOOK', 'LINKEDIN', 'TWITTER', 'INSTAGRAM', 'SNAPCHAT', 'GANALYTICS', 'SUNNATIVO', 'FBINSIGHTS', 'PINTEREST', 'ADWORDS']);
                $postSource->setting_id = $post->setting_id;
                $postSource->promoted = $faker->randomElement(['Y', 'N']);
                $postSource->creatividad_ad_id = $faker->randomNumber(5);
                $postSource->destination_url = $faker->url();
                $postSource->post_url = $faker->url();
                $postSource->reach = $faker->randomNumber(5);
                $postSource->impresions = $faker->randomNumber(5);
                $postSource->clicks = $faker->randomNumber(3);
                $postSource->reactions = $faker->randomNumber(3);
                $postSource->engagements = $faker->randomNumber(3);
                $postSource->reactions_data = json_encode($jsonSocial);
                $postSource->engagements_data = json_encode($jsonSocial);
                $postSource->tags = json_encode($json);
                $postSource->public_id = $faker->uuid();
                $postSource->user_id = 6;

                // Relation Field
                $postSource->content = $post->id;

                $postSource->save();
            }
        }
    }
}
