<?php

use Faker\Factory;
use App\PostSetting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for($i=0; $i<=50; $i++)
        {
            $json = [];
            $json['prop-one'] = $faker->words(10);
            $json['prop-two'] = $faker->words(10);

            $postSetting = new PostSetting();

            $postSetting->name = $faker->sentence(5);
            $postSetting->sourcetype = $faker->randomElement(['RSS', 'Api GET']);
            $postSetting->Source_url = $faker->url();
            $postSetting->readtypetime = $faker->randomElement(['Interval', 'Schedule']);
            $postSetting->readintervalminutes = $faker->numberBetween(1, 60);
            $postSetting->itemstoread = $faker->numberBetween(1, 99);
            $postSetting->itemschoose = $faker->randomElement(['Random', 'Top to bottom', 'Recent First', 'Old First']);
            $postSetting->postingtypetime = $postSetting->readtypetime;
            $postSetting->postingintervalminutes = $postSetting->readintervalminutes;
            $postSetting->public_id = $faker->uuid();
            //$postSetting->tagsdictionary = ;
            $postSetting->user_id = 6;
            $postSetting->filter = json_encode($json);;

            $postSetting->save();
        }
    }
}
