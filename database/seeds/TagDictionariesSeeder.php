<?php

use App\TagDictionary;
use Faker\Factory;
use Illuminate\Database\Seeder;

class TagDictionariesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for($i=0; $i<=50; $i++)
        {
            $tagDic = new TagDictionary();

            $tagDic->name = $faker->sentence(1);
            $tagDic->platform = $faker->randomElement(['FACEBOOK', 'LINKEDIN', 'TWITTER', 'INSTAGRAM', 'SNAPCHAT', 'GANALYTICS', 'SUNNATIVO', 'FBINSIGHTS', 'PINTEREST', 'ADWORDS']);
            $tagDic->profilesource = $faker->url();
            $tagDic->public_id = $faker->uuid();

            $tagDic->save();
        }
    }
}
