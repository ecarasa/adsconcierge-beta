<?php

//test
use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;
use Hborras\TwitterAdsSDK\TwitterAds\LineItem\LineItem;
use Hborras\TwitterAdsSDK\TwitterAds\Campaign\FundingInstrument;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\BadRequest;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\Forbidden;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotAuthorized;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotFound;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\RateLimit;
use Hborras\TwitterAdsSDK\TwitterAds\Creative\PromotedTweet;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServerError;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServiceUnavailable;
use Hborras\TwitterAdsSDK\TwitterAdsException;

use Hborras\TwitterAdsSDK\TwitterAds\Enumerations;
use Hborras\TwitterAdsSDK\TwitterAds\Fields\AnalyticsFields;
use Hborras\TwitterAdsSDK\TwitterAds\Fields\LineItemFields;
use Hborras\TwitterAdsSDK\TwitterAds\Analytics;
use Hborras\TwitterAdsSDK\TwitterAds\Analytics\Job;
use Hborras\TwitterAdsSDK\TwitterAds\Cursor;

Cursor::setDefaultUseImplicitFetch(true);
 
const MAPA_DEVICES=['Android devices'=>'MOBILE', 'Desktop and laptop computers'=>'DESKTOP', 'iOS devices'=>'MOBILE'  ];
function getStats($stat)
{
    if ($stat instanceof stdClass) {
        foreach (get_object_vars($stat) as $key => $val) {
            if (is_array($val)) {
                echo "\t\t\t\t\t " . $key . ': ' . $val[0] . PHP_EOL;
            } else {
                echo "\t\t\t\t\t " . $key . ' 0' . PHP_EOL;
            }
        }
    } else if (is_array($stat)) {
        foreach ($stat as $s) {
            echo $s . PHP_EOL;
        }
    }

}

function get_stats_global($parentobject, $infoCredentials, $params,  $parentevent = '',$twitterapi=false, $apibasic=FALSE){
 
 
 
  
      $fields = ['account_id','impressions','clicks','campaign_id','campaign_name','account_currency','buying_type','date_start','objective','reach','spend','inline_post_engagement','cost_per_action_type','cpc','cpm','website_ctr'];
  
  $metrics= array(
                    AnalyticsFields::METRIC_GROUPS_BILLING,
                     AnalyticsFields::METRIC_GROUPS_VIDEO,
                     AnalyticsFields::METRIC_GROUPS_MEDIA,
                    AnalyticsFields::METRIC_GROUPS_WEB_CONVERSIONS,
                 //   AnalyticsFields::METRIC_GROUPS_MOBILE_CONVERSION,
                    AnalyticsFields::METRIC_GROUPS_ENGAGEMENT
                );
 
    switch ($parentevent) {
        case 'get_stats_campaigns':
        case 'get_account_stats_campaigns':
            $fields = ['account_id','impressions','clicks','campaign_id','campaign_name','account_currency','buying_type','date_start','objective','reach','spend','inline_post_engagement','cost_per_action_type','cpc','cpm','website_ctr'];
            $entity='CAMPAIGN';
            break;
        case 'get_stats_ad':
            //$fields = ['ad_id, adset_id, impressions', 'campaign_id', 'account_id', 'account_currency', 'buying_type', 'campaign_name', 'clicks', 'conversions', 'date_start', 'date_stop', 'objective', 'reach', 'spend', 'inline_post_engagement', 'actions', 'wish_bid', 'ad_bid_type', 'social_spend', 'video_thruplay_watched_actions', 'video_play_actions', 'ad_bid_value'];
            $fields[]= 'ad_id';
            $fields[]= 'ad_name';
            $fields[]= 'adset_id';
            $fields[]= 'campaign_id';
            $entity='PROMOTED_TWEET';        
            break;
        
        case 'get_stats_adSet':
            $fields[]= 'adset_name';
            $fields[]= 'adset_id';
            $entity='LINE_ITEM';
        // ['impressions', 'campaign_id', 'account_id', 'account_currency', 'clicks', 'conversions', 'date_start', 'date_stop', 'objective', 'reach', 'spend',  'ad_bid_type', 'video_play_actions', 'ad_bid_value', 'adset_name', 'adset_id'];
            break;
    }

 
    try {
$params['start_time']->setTimezone(new DateTimeZone($parentobject->getTimezone()));
$params['end_time']->setTimezone(new DateTimeZone($parentobject->getTimezone()));
            $interval = new DateInterval('P1D');
$daterange = new DatePeriod($params['start_time'], $interval ,$params['end_time']);
      $rows=[];
      $statbase = ['id_in_platform' => 0, 'ad_id'=>'', 'adset_id'=>'', 'ad_name'=>'', 'campananame' => '', 'date' => '', 
											'metrics_delivery' => [], 'metrics_costs' => [], 'metrics_engagement' => [], 'metrics_video' => [], 'metrics_conversion' => [],
											'metrics_rest' => [], 'cost' => 0, 'impressions' => 0, 'reach' => 0, 'clicks' => 0, 'engagements' => 0, 'cpc'=>0, 'cpm'=>0, 'ctr'=>0,
											'video_views' => 0, 'video_starts' => 0,'video_completes' => 0, 'currency' => '', 'conversions' => 0, 'objective'=>'', 'device'=>'', 'placement'=>'', 'platform_position'=>'',
'video_content_starts'=>0,'video_views_100'=>0,      'video_views_25'=>0,      'video_views_50'=>0,      'video_views_75'=>0                
                  ];
       $rows=[];
foreach ($daterange as $date){
  $dias[] = $date->format('Y-m-d');
}
  
 
   $job=[];
      foreach($params['placements'] as $placement  ) {
        $job[$placement] = $parentobject->all_stats($params['entity_ids'],
               $metrics,
                array(
                    "entity" => $entity,
                   // "entity_ids" => $params['entity_ids'],
                  "segmentation_type" => "PLATFORMS",
                    "placement" => $placement,
                    AnalyticsFields::START_TIME =>$params['start_time']->setTime(0, 0, 0),
                    AnalyticsFields::END_TIME =>$params['end_time']->setTime(0, 0, 0) ,
                    AnalyticsFields::GRANULARITY => Enumerations::GRANULARITY_DAY
//                    AnalyticsFields::GRANULARITY => Enumerations::GRANULARITY_TOTAL
                ),
              true
            );
        
      }
     // print_r($job);
$datos=[];
      foreach($params['placements'] as $placement  ) {
         while($job[$placement]->getStatus() == TwitterAds\Fields\JobFields::PROCESSING){
               //     echo 'Job is still processing. Waiting 5 more seconds' .PHP_EOL;
                    $job[$placement]->read();
                    sleep(5);
                }
    // echo 'finjob '. $placement ;
        
                   if($job[$placement]->getStatus() == TwitterAds\Fields\JobFields::SUCCESS){
                    $result = gzfile($job[$placement]->getUrl());
                    $result = implode('', $result);
                  
                    $entityStats=  json_decode($result)->data;
                    $metricKeys=['impressions', 'billed_charge_local_micro', 'clicks', 'url_clicks','engagements' ];              
                     foreach($entityStats as $statGroup) {
                       
                       $id_en_platform= $statGroup->id;

                       foreach ($statGroup->id_data  as $stat ) {
                         $device= $stat->segment->segment_name;
                      //echo 'finjob '. $id_en_platform  .' - '. $placement . ' - '. $device.PHP_EOL;
                         foreach($dias as $idx=>  $dia) {
                           $sufix=$id_en_platform.$placement.$device ;
                           $datos[$idx.$sufix ]=$statbase;
                           $datos[$idx.$sufix ]['date']=$dia;
                           $datos[$idx.$sufix ]['startdate']=$dia;
                           $datos[$idx.$sufix ]['placement']=$placement;
                           $datos[$idx.$sufix ]['device']=MAPA_DEVICES[$device];
                           $datos[$idx.$sufix ]['platform_position']=$device;
                           $datos[$idx.$sufix ]['id_in_platform']=$id_en_platform;
                           $datos[$idx.$sufix ]['ad_id']=$id_en_platform;
                           

                         }
 
                           foreach ($metricKeys  as $metrica) {
                              $imetrica=0;
                             if (is_array($stat->metrics->{$metrica})) {
                             foreach ($stat->metrics->{$metrica} as $key => $value) {
                               switch($metrica ){
                                 case 'billed_charge_local_micro':
                                   $datos[$key.$sufix][$metrica]=$value ;
                                   break;
                                 default:
                                   $datos[$key.$sufix][$metrica]=$value;
                                   break; 
                               }
                                
                             }
                             }
                            
                           }
 
                       }
                       
                    //$stats = $stat->id_data[0]->metrics;
   
                     }

                }
      
    }
     //   $datos->setUseImplicitFetch(true);
//       print_r(get_class_methods($parentobject));
///       print_r(get_class_methods($datos));
  
      
        $retorno = [];
    //  echo 'datos count '.count($datos).PHP_EOL;
        return $datos;
        foreach ($datos as $citem) {
            $retorno[] = $citem->getData();
        } 
        return $retorno;
    } catch (Exception $e) {
        print_r($e->getMessage());      
        print_r($e->getErrors());      
        print_r($e->getTrace()[0] );      
      $loggermessage = array( 'level'=> 'Exception', 'category'=>'FbapiError', 'message'=> $e->getMessage()  , 'params'=> json_encode($params),  'infoCredentials' => json_encode(  $infoCredentials ) );
          extLogger($loggermessage );
    }
}






function get_account_stats_ads( $AdAccountObj, $data , $period='last_3d', $infoCredentials){ 
 
	//echo 'get_account_stats_ads AdAccountObj '. PRINT_R( $AdAccountObj , true); 
	echo 'get_account_stats_ads data '. PRINT_R( $data , true); 
 
    if (!isset($AdAccountObj['account_platform_id'] )) { return false; }
  $adAccountPlatformId= $AdAccountObj['account_platform_id'];

  try { 
 
    $twitterapi=loginApi($infoCredentials, $infoCredentials['app_id'], $infoCredentials['app_secret']);
 
  $account = new Account($AdAccountObj['account_platform_id']);
  $account->read();
 // print_r(get_class_methods($account));
 //   exit;
   // $parentobject =  new AdAccount("act_" . $AdAccountObj['account_platform_id']  );
    $params = [
        'level' => 'ad',
        'date_preset' => 'last_3d',
        'time_increment' => '1',
        'breakdowns' => 'publisher_platform,platform_position,device_platform'
    ];
  //  'time_range' => array('since' => substr($data['start_date'], 0, 10), 'until' => substr($data['end_date'], 0, 10)),
 if (isset($period)  ) { $params['date_preset']= $period;    }
    $message = 'Stats adset ad_account: '. $AdAccountObj['account_platform_id'] .' from '. $data['start_date'].' to '. $data['end_date'] . PHP_EOL;
    $error_message = ['add_acount' => $AdAccountObj];
          $startTime= new DateTime('90 Days ago');
          $startTime->setTimezone(new DateTimeZone($account->getTimezone()   ));
          $startTime->setTime(0, 0, 0);
    
        $endTime=  new DateTime('80 Days ago');
      $endTime->setTimezone(new DateTimeZone($account->getTimezone()));
      $endTime->setTime(0, 0, 0);
  
        $paramsActive= ['start_time'=>$startTime->format('Y-m-d'),
                        'end_time' => $endTime->format('Y-m-d'),
                        'entity' =>'PROMOTED_TWEET',
                        'entity' =>'PROMOTED_TWEET'

        ];
 
  
      $activeTweets = $twitterapi->get('stats/accounts/'.$AdAccountObj['account_platform_id'].'/active_entities', $paramsActive); 
 	 
    
      $items=$activeTweets->getBody()->data;
		 echo '$activeTweets '.   count($items).PHP_EOL;
      $oncourseIds=[];
      $placements=[];
    /*
    [0] => stdClass Object
        (
            [entity_id] => 6qxeap
            [activity_start_time] => 2021-06-11T05:55:13Z
            [activity_end_time] => 2021-06-21T14:30:06Z
            [placements] => Array
                (
                    [0] => ALL_ON_TWITTER
                    [1] => PUBLISHER_NETWORK
                )

        )

    */

		if (count($items )<1) { return true; }
       $i=0;
      foreach($items as $item){
        $oncourseIds[]=$item->entity_id;
        foreach($item->placements as $placement ){
        $placements[$placement ]=$placement;  
        }
        if ($i==20) {
         // get_stats_global
          $i=0;
          $oncourseIds=[];
        }
      }

		$params=['entity_ids'=>  $oncourseIds, 'placements'=>$placements, 'entity'=> $paramsActive['entity'], 'start_time'=> $startTime   , 'end_time'=>  $endTime];
    $bulkdata = get_stats_global($account, $infoCredentials, $params,  'get_stats_ad',$twitterapi);  
   echo PHP_EOL . PHP_EOL . 'get_account_stats_ads  Qty items - tw API  '. count($bulkdata) . PHP_EOL;    

   persistStats_platform_Ads_day($infoCredentials, $infoCredentials['platform'], $AdAccountObj, $bulkdata );
       } catch (exception $e) {
        echo "Error api twitter: " . $e->getMessage();
        //die();
    }
}





















function twitter_stats_generic($userid, $accountw, $kindentity, $itemsid_en_pl, $granular, $startDate, $endDate, $campaignsData = null)
{

    $dates = dateRanges($startDate, $endDate, $granular);
    //print_r($dates);die;

    $metricas = [];


    foreach ($dates as $date) {

        if (is_string($itemsid_en_pl)) {
            $items = [$itemsid_en_pl];
        } else {
            $items = $itemsid_en_pl;
        }

        try {
            $stats = $accountw->all_stats(
                $items,
                array(
                    AnalyticsFields::METRIC_GROUPS_BILLING,
                    AnalyticsFields::METRIC_GROUPS_VIDEO,
                    AnalyticsFields::METRIC_GROUPS_MEDIA,
                    AnalyticsFields::METRIC_GROUPS_WEB_CONVERSIONS,
                    AnalyticsFields::METRIC_GROUPS_MOBILE_CONVERSION,
                    AnalyticsFields::METRIC_GROUPS_ENGAGEMENT
                ),
                array(
                    AnalyticsFields::ENTITY => $kindentity,
                    AnalyticsFields::START_TIME => $date[0],
                    AnalyticsFields::END_TIME => $date[1],
                    AnalyticsFields::GRANULARITY => Enumerations::GRANULARITY_TOTAL
                )
            );

        } catch (exception $e) {
            echo "Error: " . $e->getMessage();
            continue;
        }

        foreach ($stats as $statsitem) {

            $statsData = cleanConversions($statsitem->id_data[0]->metrics);
            $name = $campaignsData[$statsitem->id]['name'];
            if ($kindentity == AnalyticsFields::PROMOTED_TWEET)
                $name = $campaignsData[$statsitem->id]['ad_name'];

            $metricas[] = array_merge([
                'entity' => $kindentity,
                'startdate' => $date[0]->format('Y-m-d H:i:s'),
                'enddate' => $date[1]->format('Y-m-d H:i:s'),
                'id_in_platform' => $statsitem->id,
                'id' => null,
                strtolower($kindentity) . '_name' => $name,
                'currency' => (isset($campaignsData[$statsitem->id]['currency']) ? $campaignsData[$statsitem->id]['currency'] : null),
            ], (array)$statsData);
        }
    }
    return $metricas;
}

// STATS functions
function stats_campana_xaccount($twiiterApi, $infoCredentials, $data){

    try {

        $cuentaTwBBDD = getAdAccount_Element($data['ad_account_id']);

        echo 'AdAccount Twitter - ' . $data['ad_account_id'] . PHP_EOL;

        $accountw = new Account($cuentaTwBBDD['account_id']);
        $accountw->read();

        $getFundingInstruments = $accountw->getFundingInstruments();

        $campaigns = $accountw->getCampaigns('');

        $campaigns->setUseImplicitFetch(true);

        $campaignsIds = array();
        $campaignsData = array();
        $campStruct = array();

        foreach ($campaigns as $campaign) {

            if ($campaign->effective_status != 'RUNNING') {
                //continue;
            }

            $campaignsIds[] = $campaign->getId();

            $campaignsData[$campaign->getId()] = array(
                'id' => $campaign->getId(),
                'name' => $campaign->getName(),
                'currency' => $campaign->getCurrency(),
                'status' => $campaign->effective_status
            );

            if (count($campaignsIds) == 20) {
                $campStruct[] = array(
                    'campaignsIds' => $campaignsIds,
                    'campaignsData' => $campaignsData
                );
                $campaignsIds = array();
                $campaignsData = array();
            }
        }

        if (count($campaignsIds) > 0) {
            $campStruct[] = array(
                'campaignsIds' => $campaignsIds,
                'campaignsData' => $campaignsData
            );
        }

        $datos = array();

        foreach ($campStruct as $val) {

            $temp = twitter_stats_generic($infoCredentials['user_id'], $accountw, AnalyticsFields::CAMPAIGN, $val['campaignsIds'], 'P1D', $data['start_date'], $data['end_date'], $val['campaignsData']);

            if (count($datos) > 0) {
                $datos = array_merge($datos, $temp);
            } else {
                $datos = $temp;
            }
        }

        helper_metrics_campana_day( $infoCredentials['platform'],
            $infoCredentials['user_id'],
            $infoCredentials['customer_id_default'],
            $datos,
            $cuentaTwBBDD['id'],
            $cuentaTwBBDD['account_id'],
            $infoCredentials );

    } catch (exception $e) {
        echo "Error api twitter: " . $e->getMessage();
        //die();
    }

}
 
?>