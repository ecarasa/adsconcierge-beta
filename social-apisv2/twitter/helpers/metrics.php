<?php
function helper_metrics_keytranslator($platformid, $tipoinput, $inputraw, $outputrow, $metadata = null)
{
    global $mapeometricas;

    //var_dump($inputraw);
    //echo PHP_EOL;
    // die();

    switch ($platformid) {
        case 1:
        case "FACEBOOK":
            switch ($tipoinput) {
                case 'campana':
                    $outputrow['id_in_platform'] = $inputraw['campaign_id'];
                    break;
                case 'ad':
                    $outputrow['id_in_platform'] = $inputraw['ad_id'];
                    $outputrow['lineitem_id'] = $inputraw['adset_id'];

                    break;
                case 'lineitem':
                    $outputrow['id_in_platform'] = $inputraw['adset_id'];
                    $outputrow['lineitem_id'] = $inputraw['adset_id'];

                    break;

            }
            $outputrow['date'] = $inputraw['date_start'];
            $outputrow['yearmonth'] = date('Y-m-01', strtotime($inputraw['date_start']));
            $outputrow['platformid'] = 1;
            switch ($inputraw['publisher_platform']) {
                case 'facebook':
                    $outputrow['platformid'] = 1;
                    break;
                case 'instagram':
                    $outputrow['platformid'] = 4;
                    break;
            }
            //https://developers.facebook.com/docs/marketing-api/insights/parameters/v9.0
            $outputrow['video_starts'] = $inputraw['video_play_actions'];
            $outputrow['video_views'] = $inputraw['video_play_actions'];
            $outputrow['video_completes'] = $inputraw['video_p100_watched_actions'];
            $outputrow['video_25'] = $inputraw['video_p25_watched_actions'];
            $outputrow['video_50'] = $inputraw['video_p50_watched_actions'];
            $outputrow['video_75'] = $inputraw['video_p75_watched_actions'];

            foreach ($inputraw as $clave => $valor) {
                if ($valor == null) {
                    continue;
                }
                $clave = strtolower($clave);
                $clavemapeo = isset($mapeometricas[$platformid][$clave]) ? $mapeometricas[$platformid][$clave] : (isset($mapeometricas['default'][$clave]) ? $mapeometricas['default'][$clave] : ['metrics_rest']);

                foreach ($clavemapeo as $claveout) {
                    if (is_numeric($outputrow[$claveout])) {
                        $outputrow[$claveout] = $outputrow[$claveout] + $valor;
                    } elseif (is_string($outputrow[$claveout])) {
                        $outputrow[$claveout] = $valor;
                    } else {
                        $outputrow[$claveout][$clave] = $valor;
                    }
                }

            }
            //die();
            break;
        case 2:
        case "LINKEDIN":

            $outputrow['id_in_platform'] = explode(':', $inputraw['pivotValue']);
            $outputrow['id_in_platform'] = $outputrow['id_in_platform'][3];
            $outputrow['platformid'] = 2;
            $outputrow['date'] = $inputraw['dateRange']['start']['year'] . '-' . $inputraw['dateRange']['start']['month'] . '-' . $inputraw['dateRange']['start']['day'];
            $outputrow['yearmonth'] = $inputraw['dateRange']['start']['year'] . '-' . $inputraw['dateRange']['start']['month'] . '-01';
            $outputrow['campananame'] = (isset($metadata['campaignsData'][$inputraw['pivotValue']]) ? $metadata['campaignsData'][$inputraw['pivotValue']]['name'] : null);
            $outputrow['currency'] = (isset($metadata['campaignsData'][$inputraw['pivotValue']]['totalBudget']) ? $metadata['campaignsData'][$inputraw['pivotValue']]['totalBudget']['currencyCode'] : $metadata['currency']);
            $outputrow['impressions'] = $inputraw['impressions'];
            $outputrow['clicks'] = $inputraw['clicks'];
            $outputrow['cost'] = $inputraw['costInLocalCurrency'];
            $outputrow['reach'] = $inputraw['approximateUniqueImpressions'];
            $outputrow['video_starts'] = $inputraw['videoStarts'];
            $outputrow['video_views'] = $inputraw['videoStarts'];
            $outputrow['video_completes'] = $inputraw['videoCompletions'];
            $outputrow['video_25'] = $inputraw['videoFirstQuartileCompletions'];
            $outputrow['video_50'] = $inputraw['videoMidpointCompletions'];
            $outputrow['video_75'] = $inputraw['videoThirdQuartileCompletions'];
            $outputrow['engagements'] = $inputraw['totalEngagements'] - $inputraw['clicks'];

            foreach ($inputraw as $clave => $valor) {
                $clave = strtolower($clave);
                $clavemapeo = isset($mapeometricas[$platformid][$clave]) ? $mapeometricas[$platformid][$clave] : (isset($mapeometricas['default'][$clave]) ? $mapeometricas['default'][$clave] : ['metrics_rest']);
                foreach ($clavemapeo as $claveout) {
                    if (is_numeric($outputrow[$claveout])) {
                        $outputrow[$claveout] = $outputrow[$claveout] + $valor;
                    } else {
                        $outputrow[$claveout][$clave] = $valor;
                    }
                }
            }

            break;
        case 3:
        case "TWITTER":

            $outputrow['date'] = date('Y-m-d', strtotime($inputraw['startdate']));
            $outputrow['yearmonth'] = date('Y-m-01', strtotime($inputraw['startdate']));
            $outputrow['id_in_platform'] = $inputraw['id_in_platform'];
            $outputrow['campananame'] = isset($inputraw['campaign_name']) ? $inputraw['campaign_name'] : '';
            $outputrow['platformid'] = 3;
            $outputrow['video_starts'] = $inputraw['video_content_starts'];
            $outputrow['video_completes'] = $inputraw['video_views_100'];
            $outputrow['video_25'] = $inputraw['video_views_25'];
            $outputrow['video_50'] = $inputraw['video_views_50'];
            $outputrow['video_75'] = $inputraw['video_views_75'];
            $outputrow['reach'] = is_array($inputraw['impressions']) ? $inputraw['impressions'][0] : 0;

            foreach ($inputraw as $clave => $valor) {
                if (is_array($valor) && count($valor)) {
                    $valor = $valor[0];
                }
                if ($valor == null) {
                    continue;
                }
                $clave = strtolower($clave);
                $clavemapeo = isset($mapeometricas[$platformid][$clave]) ? $mapeometricas[$platformid][$clave] : (isset($mapeometricas['default'][$clave]) ? $mapeometricas['default'][$clave] : ['metrics_rest']);
                foreach ($clavemapeo as $claveout) {
                    if (is_numeric($outputrow[$claveout])) {
                        $outputrow[$claveout] = $outputrow[$claveout] + $valor;
                    } elseif (is_string($outputrow[$claveout])) {
                        $outputrow[$claveout] = $valor;
                    } else {
                        $outputrow[$claveout][$clave] = $valor;
                    }
                }

            }
            //$outputrow['currency'] = $outputrow['currency']['currency'];
            $outputrow['currency'] = $outputrow['currency'] ;
            $outputrow['cost'] = round(($outputrow['cost'] / 1000000), 4);
            $outputrow['engagements'] = $outputrow['engagements'] - $outputrow['clicks'];

            break;
    }

    return $outputrow;
}

function helper_metrics_campana_day($platformid, $user_id, $customer_id = 0, $datos, $ad_account_id, $ad_account_id_platform, $metadata = ['currency' => null], $job_id = "")
{
    global $dbconn_stats, $dbconn;

    if (!isset($metadata['currency'])) {
        $metadata['currency'] = null;
    }
    if (!is_array($datos)) {
        return null;
    }

    echo ' >> STATS to insert/update ' . count($datos) . ' elements : ';
    $i = 1;

    foreach ($datos as $inputraw) {

        echo " **";

        $outputrow = ['id_in_platform' => 0, 'campananame' => '', 'platformid' => $platformid, 'date' => '', 'metrics_delivery' => [], 'metrics_costs' => [], 'metrics_engagement' => [], 'metrics_video' => [], 'metrics_conversion' => [], 'metrics_rest' => [], 'cost' => 0, 'impressions' => 0, 'reach' => 0, 'clicks' => 0, 'engagements' => 0, 'video_views' => 0, 'video_starts' => 0, 'currency' => $metadata['currency'], 'conversions' => 0];
        $outputrow = helper_metrics_keytranslator($platformid, 'campana', $inputraw, $outputrow, $metadata);

        //var_dump($outputrow);die();
        //echo "** impressions: " .  $outputrow['impressions'];
        //echo " ** clicks: " .  $outputrow['clicks'] . PHP_EOL;

        if ($outputrow['impressions'] == 0 && $outputrow['video_views'] == 0 && $outputrow['reach'] == 0 && $outputrow['clicks'] == 0 && $outputrow['engagements'] == 0) {
            //continue;
        }

        $sql = "INSERT INTO adsconcierge_stats.platform_campana_day ( user_id, platformid, customer_id, ad_account_id, campanaid,
                                                idenplatform, campananame, adccountid_pl, dia, unico,
                                                currency, metrics_delivery, metrics_costs, metrics_engagement, metrics_video,
                                                metrics_conversion, metrics_rest, cost, impressions, reach,
                                                clicks, engagements, video_views, conversions, plataforma,
                                                campanaroot, yearweek, yearmonth )
                                SELECT
                                    {$user_id},
                                    '{$outputrow['platformid']}',
                                    '{$customer_id}',
                                    '{$ad_account_id}',
                                    cp.id,
                                    '{$outputrow['id_in_platform']}',
                                    cp.name,
                                    '{$ad_account_id_platform}',
                                    '{$outputrow['date']}',
                                    '" . md5($outputrow['platformid'] . $outputrow['id_in_platform'] . $outputrow['date'] . $user_id) . "',
                                    '{$outputrow['currency']}',
                                    '" . json_encode($outputrow['metrics_delivery']) . "',
                                    '" . json_encode($outputrow['metrics_costs']) . "',
                                    '" . json_encode($outputrow['metrics_engagement']) . "',
                                    '" . json_encode($outputrow['metrics_video']) . "',
                                    '" . json_encode($outputrow['metrics_conversion']) . "',
                                    '" . $dbconn->real_escape_string(json_encode($outputrow['metrics_rest'])) . "',
                                    '{$outputrow['cost']}', '{$outputrow['impressions']}',
                                    '{$outputrow['reach']}',
                                    '{$outputrow['clicks']}', '{$outputrow['engagements']}',
                                    '" . intval($outputrow['video_views']) . "',
                                    '{$outputrow['conversions']}',
                                    '{$platformid}',
                                    cp.campana_root,
                                    YEARWEEK('{$outputrow['date']}'),
                                    date_format('{$outputrow['date']}', '%Y-%m')
                                FROM
                                    app_thesoci_9c37.campaigns_platform cp
                                WHERE
                                    cp.id_en_platform = '{$outputrow['id_in_platform']}' AND cp.user_id = {$user_id}
                                ON DUPLICATE KEY UPDATE
                                    cost                = '{$outputrow['cost']}',
                                    impressions         = '{$outputrow['impressions']}' ,
                                    reach               = '{$outputrow['reach']}',
                                    clicks              = '{$outputrow['clicks']}',
                                    engagements         = '{$outputrow['engagements']}',
                                    video_views         = '" . intval($outputrow['video_views']) . "',
                                    video_starts        = '" . intval($outputrow['video_starts']) . "',
                                    video_completes     = '" . intval($outputrow['video_completes']) . "',
                                    video_25            = '" . intval($outputrow['video_25']) . "',
                                    video_50            = '" . intval($outputrow['video_50']) . "',
                                    video_75            = '" . intval($outputrow['video_75']) . "',
                                    conversions         = '{$outputrow['conversions']}',
	                                metrics_delivery    = '" . json_encode($outputrow['metrics_delivery']) . "',
	                                metrics_costs       = '" . json_encode($outputrow['metrics_costs']) . "',
	                                metrics_engagement  = '" . json_encode($outputrow['metrics_engagement']) . "',
	                                metrics_video       = '" . json_encode($outputrow['metrics_video']) . "',
	                                metrics_conversion  = '" . json_encode($outputrow['metrics_conversion']) . "',
                                    metrics_rest        = '" . $dbconn->real_escape_string(json_encode($outputrow['metrics_rest'])) . "'";

        if (!$dbconn_stats->query($sql)) {
            printf("Error: %s\n", $dbconn_stats->error);
            //echo PHP_EOL . $sql;
        }

    }

    //echo PHP_EOL . ' DB CampañaxDia data updated > ';
    //$sql = "UPDATE adsconcierge_stats.background_job SET status = 'finished' WHERE id = {$job_id}";
    //$db->query($sql);

}

?>