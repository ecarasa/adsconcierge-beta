<?php
use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;
use Hborras\TwitterAdsSDK\TwitterAds\Cursor;
Cursor::setDefaultUseImplicitFetch(true);
function get_adsAccounts (  $requestParams, $infoCredentials){

    global $dbconn_stats, $dbconn;
if (VERBOSE) {
    echo 'Search AdsAccounts '. PHP_EOL;
}
    $twitterapi=loginApi($infoCredentials, $infoCredentials['app_id'], $infoCredentials['app_secret']);
    $accounts = $twitterapi->getAccounts();
       $apibasic=loginApiBasic($infoCredentials ) ;
       //print_r(get_class_methods($apibasic ) );
    $arr = array();

    foreach ($accounts as $account) {

        $account->read();   
        $funding = $account->getFundingInstruments();
        $adaccount_name=$account->getName();
        $ad_accountid= $account->getId();
        $paramsActive=[];
   			$arr[]=$ad_accountid;
/** para llamar al api si no tenemos metodo wrappeado
      $activeTweets = $twitterapi->get('accounts/'.$ad_accountid.'/tracking_tags', $paramsActive);
      $activeTweets = $twitterapi->get('accounts/'.$ad_accountid.'/funding_instruments', $paramsActive);
   **/

 
    
    if (VERBOSE) {
        echo  'Id = ' .$ad_accountid . ' | ' . $account->getName() . ', Currency '.$funding->getCollection()[0]->getCurrency() . ' , Status ' . $account->getApprovalStatus() . PHP_EOL;
		}
        $itemData = array(
            "id_en_platform" => $ad_accountid,  "account_id" =>$ad_accountid, "salt" => $account->getSalt(),  "name" => $account->getName(),
            "timezone" => $account->getTimezone(),  "timezone_switch_at" => $account->getTimezoneSwitchAt(),  "created_at" => $account->getCreatedAt(),
            "updated_at" => $account->getUpdatedAt(), "deleted" => $account->getDeleted(), "status" => $account->getApprovalStatus(), "approval_status" => $account->getApprovalStatus(),
            "business_id" => $account->getBusinessId(), "business_name" => $account->getBusinessName(), "industry_type" => $account->getIndustryType(),"category" => $account->getIndustryType(),
            "getPromotableUsers" => $account->getPromotableUsers(), "funding_instrument" => $funding->getCollection(), "currency" => $funding->getCollection()[0]->getCurrency(), 
            "features"=> $account->getFeatures(), "PromotableUsers"=> $account->getPromotableUsers(), "metadata" => null
        );
        
        persistAdAccount($infoCredentials['user_id'], $itemData, $infoCredentials);
      
     
    get_account_properties($itemData, $infoCredentials,$twitterapi ,$apibasic );
      
      
      
    
     

   //   exit;
     
    }

    //return true;
    return $arr;
}

function get_account_properties($adAccountData, $infoCredentials,$twitterapi=FALSE,$apibasic=FALSE){
 if (!isset($adAccountData['account_platform_id'] )) { return false; }
  $adAccountPlatformId= $adAccountData['account_platform_id'];
  
  if (!$twitterapi) {
    $twitterapi=loginApi($infoCredentials, $infoCredentials['app_id'], $infoCredentials['app_secret']);
  }  
  if (!$apibasic) {
      $apibasic=loginApiBasic($infoCredentials ) ;
  } 
  
   try {
        //traemos las paginas, que twitter llama promotable_users
        $promotable_users = $twitterapi->get('accounts/'.$adAccountPlatformId.'/promotable_users', array('with_deleted'=> true));
          foreach($promotable_users->getBody()->data as $item) {
        //    hay que buscar el user porque no devuelven el nombre en el api promotable_users
         $vlookup=$apibasic-> get('users/lookup', array('user_id'=>$item->user_id));
            $itemData['metadata']='';
            $itemData['name']='(NULL)';
             $itemData['id_en_platform']= $item->user_id;
            if (isset($vlookup[0] ) ){
               $itemData['name']='@'.$vlookup[0]->screen_name .' | '. $vlookup[0]->name ;
              unset($vlookup[0]->status );
              $itemData['metadata']= $vlookup;
            }
              persistProperties( $itemData, $infoCredentials ,  $adAccountPlatformId,  'PAGE');
             
          }
         return true; 
      }catch( Exception $e){
        print_r($e);
      }
  return false;
}

/***
todo 
***/

function get_account_pixels($adAccountData, $infoCredentials){
  echo 'no implementado';
  return false;
 
if (VERBOSE) {
    echo 'SEARCH Pixels '. PHP_EOL;
}
    
    foreach ($adPixels as $adPixel) {

        $pixel = $adPixel->getData();
        //var_dump($pixel);
        //die();

        $itemData = array_intersect_key($adPixel->getData(), array_flip($fields));
        //$adPixelData = array_merge($adPixelData, $this->userPlatformData);
        $itemData['ad_account_platformId'] = $pixel['owner_ad_account']['account_id'];
        $itemData['id_en_platform'] = $adPixel->getData()['id'];
        $itemData['id'] = $adPixel->getData()['id'];
        $itemData['name'] = $adPixel->getData()['name'];
      /**  $adPixelData['status'] = 0; //todo: fixed in origin code
        $adPixelData['category'] = '-';
        $adPixelData['token'] = '-';
        $adPixelData['type'] = 'PIXEL';
**/
        // $this->persistPropertiesAccounts->execute($adPixelData);
     //  print_r ( $itemData );
		 
			persistPixel(  $itemData, $infoCredentials,  $pixel['owner_ad_account']['account_id'] );
 
    }
}