<?php
use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;
use Hborras\TwitterAdsSDK\TwitterAds\Campaign\Campaign;
use Hborras\TwitterAdsSDK\TwitterAds\Campaign\FundingInstrument;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\BadRequest;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\Forbidden;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotAuthorized;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotFound;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\RateLimit;
use Hborras\TwitterAdsSDK\TwitterAds\Creative\PromotedTweet;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServerError;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServiceUnavailable;
use Hborras\TwitterAdsSDK\TwitterAdsException;
use Hborras\TwitterAdsSDK\TwitterAds\Cursor;
use Hborras\TwitterAdsSDK\TwitterAds\Enumerations;
use Hborras\TwitterAdsSDK\TwitterAds\Fields\AnalyticsFields;
Cursor::setDefaultUseImplicitFetch(true);
function get_account_campaigns($adAccount_data, $infoCredentials,$twitterapi=FALSE,$apibasic=FALSE){
 
 if (!isset($adAccount_data['account_platform_id'] )) { return false; }
  $adAccountPlatformId= $adAccount_data['account_platform_id'];
  
  if (!$twitterapi) {
    $twitterapi=loginApi($infoCredentials, $infoCredentials['app_id'], $infoCredentials['app_secret']);
  }  
  if (!$apibasic) {
      $apibasic=loginApiBasic($infoCredentials ) ;
  } 
     
 
    try {
if (VERBOSE) {
        echo 'Ad Account = ' . $adAccount_data['account_platform_id'] . PHP_EOL. PHP_EOL;
}
      $account = new Account($adAccount_data['account_platform_id']);


        $account->read();
 // usar total count reduce el numero de request a 200 en 15 minutos TwitterAds\Fields\CampaignFields::WITH_TOTAL_COUNT => true,
        $campaigns = $account->getCampaigns('', [TwitterAds\Fields\CampaignFields::COUNT => 200, TwitterAds\Fields\CampaignFields::WITH_DRAFT=> true, TwitterAds\Fields\CampaignFields::SORT_BY=> 'created_at-desc' ]);
        $campaigns->setUseImplicitFetch(true);

        $campaignsData = [];
        $campaignLineitems=[];
 

        //$cuentaTwBBDD = getAdAccount_Element($data['ad_account_id']);

        foreach ($campaigns as $item) {

        
      //    print_r( $campaignsData);
      if (VERBOSE) {
        echo 'ID Campaign = '. $item->getId() . ' - Nombre = ' .$item->getName() . PHP_EOL;
      }
             $itemData=[];
              $itemData['id']=  $item->getId()  ;
              $itemData['name']=  $item->getName()  ;
              $itemData['currency']=  $item->getCurrency()  ;
              $itemData['status']=  $item->getEntityStatus()  ;
              $itemData['effective_status']=  $item->getEntityStatus()  ;
              $itemData['start_time']=  ($item->getStartTime())?  $item->getStartTime()->getTimestamp():null  ;   
              $itemData['stop_time']=   ($item->getEndTime())?  $item->getEndTime()->getTimestamp():null  ;
             // $itemData['metadata']=  $item  ;
        
           persistCampaign( $itemData, $infoCredentials,  $adAccount_data,   $infoCredentials['platform']);
          
      
        }

    } catch (exception $e) {
        echo "Error  get_account_campaigns api twitter: " . $e->getMessage(). print_r($e->getErrors(), true);
        //die();
    }
}

function campaignplatform_create($app_id, $app_secret,$access_token,$userid, $add_account_id,$rowdata)
{

}

function campaignplatform_delete($fb, $data, $infoCredentials)
{

}

?>