<?php

//test
use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;
use Hborras\TwitterAdsSDK\TwitterAds\LineItem\LineItem;
//use Hborras\TwitterAdsSDK\TwitterAds\Creative\PromotedTweet;
use Hborras\TwitterAdsSDK\TwitterAds\Campaign\FundingInstrument;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\BadRequest;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\Forbidden;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotAuthorized;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotFound;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\RateLimit;
use Hborras\TwitterAdsSDK\TwitterAds\Creative\PromotedTweet;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServerError;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServiceUnavailable;
use Hborras\TwitterAdsSDK\TwitterAdsException;

use Hborras\TwitterAdsSDK\TwitterAds\Enumerations;
use Hborras\TwitterAdsSDK\TwitterAds\Fields\AnalyticsFields;
use Hborras\TwitterAdsSDK\TwitterAds\Fields\LineItemFields;
use Hborras\TwitterAdsSDK\TwitterAds\Analytics;
use Hborras\TwitterAdsSDK\TwitterAds\Analytics\Job;

use Hborras\TwitterAdsSDK\TwitterAds\Cursor;

Cursor::setDefaultUseImplicitFetch(true);

 

function get_account_ads($adAccount_data, $infoCredentials,$twitterapi=FALSE,$apibasic=FALSE){
  //$promotable_users = $twitterapi->get('accounts/'.$adAccountPlatformId.'/promotable_users', array('with_deleted'=> true)); 
  
 if (!isset($adAccount_data['account_platform_id'] )) { return false; }
  $adAccountPlatformId= $adAccount_data['account_platform_id'];
  
  if (!$twitterapi) {
    $twitterapi=loginApi($infoCredentials, $infoCredentials['app_id'], $infoCredentials['app_secret']);
  }  
  if (!$apibasic) {
      $apibasic=loginApiBasic($infoCredentials ) ;
  } 
     
 
    try {
if (VERBOSE) {
        echo 'Ad Account = ' . $adAccount_data['account_platform_id'] . PHP_EOL. PHP_EOL;
}
      $account = new Account($adAccount_data['account_platform_id']);


        $account->read();

 // usar total count reduce el numero de request a 200 en 15 minutos TwitterAds\Fields\CampaignFields::WITH_TOTAL_COUNT => true,TwitterAds\Fields\CampaignFields::WITH_DRAFT=> false,
 //  $items = $twitterapi->get('accounts/'.$adAccountPlatformId.'/promoted_tweets', array('with_total_count'=>true,'count'=>4, 'with_deleted'=> true));
       Cursor::setDefaultUseImplicitFetch(true);
/*
      $promotedTweet = new PromotedTweet();
        print_r(get_class_methods($promotedTweet));
     
      $items= $promotedTweet->loadResource(null, array('with_total_count'=>true,'count'=>4, 'with_deleted'=> true) );
       print_r( $items->getDefaultUseImplicitFetch());
        print_r(get_class_methods($items));
 //  $itemsb= $promotedTweet->fromResponse($items->getBody()->data);

        //$cuentaTwBBDD = getAdAccount_Element($data['ad_account_id']);
   print_r($items->fromResponse($items) );
 **/
      $params=[  TwitterAds\Fields\CampaignFields::COUNT => 4, TwitterAds\Fields\CampaignFields::WITH_DELETED=> true,TwitterAds\Fields\CampaignFields::WITH_DRAFT=> false, TwitterAds\Fields\CampaignFields::SORT_BY=> 'created_at-desc'  ];
         $items = $account->getPromotedTweets('', $params);
      
     //   $items->setUseImplicitFetch(true);
 while(!$items->isExhausted()) {      
  $items->fetchNext();   
 }
      $itemsdata=$items->getCollection();
$promotedTweets=[];
      $itemDatabulk=[];
        foreach ($itemsdata as $item) {
// print_r($item);
  
           //        print_r(get_class_methods($item));
         if (VERBOSE) {   echo 'ID add = '. $item->getId() . ' - Nombre = ' .$item->getTweetId() . PHP_EOL;
                      }
            $itemData=[];
              $itemData['id']=  $item->getId()  ;
                 $itemData['tweetid']=   $item->getTweetId() ;      
              $itemData['campaign_id']= null  ;
              $itemData['preview_shareable_link']= null  ;
              $itemData['adset_id']=   $item->getLineItemId() ;
              $itemData['name']=   $item->getTweetId() ;

 
              $itemData['status']=  $item->getEntityStatus()  ;
              $itemData['effective_status']=  $item->getApprovalStatus()  ;
          
              $itemData['promoted_object']= [ $item->getTweetId() ] ;
       /*        $itemData['start_time']=  ($item->getStartTime())?  $item->getStartTime()->getTimestamp():null  ;   
              $itemData['end_time']=   ($item->getEndTime())?  $item->getEndTime()->getTimestamp():null  ;
              $itemData['lifetime_budget']=   $item->getTotalBudgetAmountLocalMicro()/1000000 ;
              $itemData['puja_valor']=   $item->getBidAmountLocalMicro()/1000000 ;
              $itemData['bid_strategy']=   $item->getBidStrategy() ;
              $itemData['product_type']=   $item->getProductType() ;
              $itemData['daily_budget']=   null  ;
              $itemData['targeting']['publisher_platforms']=   $item->getPlacements() ;*/
            $itemData['metadata']['promoted']=  $item->toArray() ;    
           $promotedTweets[ $item->getTweetId()]=$itemData;
          
        }
      $i=0;
      $totalitems=count($promotedTweets) ;
      $tweets=[];
    //  echo count($promotedTweets).PHP_EOL;
      $tweetParams=['timeline_type'=>'ALL', 'trim_user'=> true, 'tweet_type'=>'PUBLISHED','tweet_ids'=>'' ];
        foreach ( $promotedTweets as $tweet  ){
                  $i++;
                    $tweets[]=$tweet['tweetid'];
              if ($i==200 || $i==$totalitems) {
                $tweetParams['tweet_ids']= implode(',', $tweets);
                
                 $adsTweets = $twitterapi->get('accounts/'.$adAccount_data['account_platform_id'].'/tweets', $tweetParams); 
              //  print_r($adsTweets->getBody()->data[0]);
                foreach ($adsTweets->getBody()->data   as $creative){
                  $url=isset($creative->entities->urls[0] )? $creative->entities->urls[0]->expanded_url  :'';
                  $mediaurl=isset($creative->entities->media[0] )? $creative->entities->media[0]->media_url_https  :'';
                  $mediatype=isset($creative->entities->media[0] )? $creative->entities->media[0]->type  :'';
                  $promotedTweets[$creative->tweet_id]['type_platform']='TWEET';
                  $promotedTweets[$creative->tweet_id]['content']=$creative->full_text;
                  $fulltextlen = mb_strlen($creative->full_text);
              if ($fulltextlen >120){              
                       $corte= mb_stripos( $creative->full_text ,' ' ,100); //hecho porque no siempre tienen mas de 100 char los tweets
                       $corte= ($corte)? $corte: 100;
              } else {
                $corte=$fulltextlen ;
              }
                  $promotedTweets[$creative->tweet_id]['name']=mb_substr($creative->full_text,0, $corte );
                  $promotedTweets[$creative->tweet_id]['banner']=$mediaurl;
                  $promotedTweets[$creative->tweet_id]['media_type']=$mediatype;
                  $promotedTweets[$creative->tweet_id]['url']=$url;
                  $promotedTweets[$creative->tweet_id]['metadata']=['tweet'=> $creative ];

                }
                 $previewTweets = $twitterapi->get('accounts/'.$adAccount_data['account_platform_id'].'/tweet_previews', $tweetParams); 
                foreach ($previewTweets->getBody()->data   as $preview){
                      $promotedTweets[$preview->tweet_id]['preview']=isset($preview->preview )? $preview->preview  :'';
                }
                
                $i=0;
                $tweets=[];

                      }

        }
    
         persistAd( $promotedTweets, $infoCredentials,  $adAccount_data , $infoCredentials['platform'] );     
    
//
      
    } catch (exception $e) {
      /*
        [0] => __construct
    [1] => getErrors
    [2] => __wakeup
    [3] => getMessage
    [4] => getCode
    [5] => getFile
    [6] => getLine
    [7] => getTrace
    [8] => getPrevious
    [9] => getTraceAsString
    [10] => __toString
)
*/
        echo "Error api twitter: " . $e->getMessage();
        echo "Error api twitter: " .$e->getCode();
      if ( method_exists($e, 'getErrors' )) {
        echo "Error api twitter: " .print_r( $e->getErrors());
      } else {
   //     var_dump($e);
        
      }
 
        //die();
    }
}

 

?>