<?php
function twitter_stats_generic($userid, $accountw, $kindentity, $itemsid_en_pl, $granular, $startDate, $endDate, $campaignsData = null)
{

    $dates = dateRanges($startDate, $endDate, $granular);
    //print_r($dates);die;

    $metricas = [];


    foreach ($dates as $date) {

        if (is_string($itemsid_en_pl)) {
            $items = [$itemsid_en_pl];
        } else {
            $items = $itemsid_en_pl;
        }

        try {
            $stats = $accountw->all_stats(
                $items,
                array(
                    AnalyticsFields::METRIC_GROUPS_BILLING,
                    AnalyticsFields::METRIC_GROUPS_VIDEO,
                    AnalyticsFields::METRIC_GROUPS_MEDIA,
                    AnalyticsFields::METRIC_GROUPS_WEB_CONVERSIONS,
                    AnalyticsFields::METRIC_GROUPS_MOBILE_CONVERSION,
                    AnalyticsFields::METRIC_GROUPS_ENGAGEMENT
                ),
                array(
                    AnalyticsFields::ENTITY => $kindentity,
                    AnalyticsFields::START_TIME => $date[0],
                    AnalyticsFields::END_TIME => $date[1],
                    AnalyticsFields::GRANULARITY => Enumerations::GRANULARITY_TOTAL
                )
            );

        } catch (exception $e) {
            echo "Error: " . $e->getMessage();
            continue;
        }

        foreach ($stats as $statsitem) {

            $statsData = cleanConversions($statsitem->id_data[0]->metrics);
            $name = $campaignsData[$statsitem->id]['name'];
            if ($kindentity == AnalyticsFields::PROMOTED_TWEET)
                $name = $campaignsData[$statsitem->id]['ad_name'];

            $metricas[] = array_merge([
                'entity' => $kindentity,
                'startdate' => $date[0]->format('Y-m-d H:i:s'),
                'enddate' => $date[1]->format('Y-m-d H:i:s'),
                'id_in_platform' => $statsitem->id,
                'id' => null,
                strtolower($kindentity) . '_name' => $name,
                'currency' => (isset($campaignsData[$statsitem->id]['currency']) ? $campaignsData[$statsitem->id]['currency'] : null),
            ], (array)$statsData);
        }
    }
    return $metricas;
}

// STATS functions
function stats_campana_xaccount($twiiterApi, $infoCredentials, $data){

    try {

        $cuentaTwBBDD = getAdAccount_Element($data['ad_account_id']);

        echo 'AdAccount Twitter - ' . $data['ad_account_id'] . PHP_EOL;

        $accountw = new Account($cuentaTwBBDD['account_id']);
        $accountw->read();

        $getFundingInstruments = $accountw->getFundingInstruments();

        $campaigns = $accountw->getCampaigns('');

        $campaigns->setUseImplicitFetch(true);

        $campaignsIds = array();
        $campaignsData = array();
        $campStruct = array();

        foreach ($campaigns as $campaign) {

            if ($campaign->effective_status != 'RUNNING') {
                //continue;
            }

            $campaignsIds[] = $campaign->getId();

            $campaignsData[$campaign->getId()] = array(
                'id' => $campaign->getId(),
                'name' => $campaign->getName(),
                'currency' => $campaign->getCurrency(),
                'status' => $campaign->effective_status
            );

            if (count($campaignsIds) == 20) {
                $campStruct[] = array(
                    'campaignsIds' => $campaignsIds,
                    'campaignsData' => $campaignsData
                );
                $campaignsIds = array();
                $campaignsData = array();
            }
        }

        if (count($campaignsIds) > 0) {
            $campStruct[] = array(
                'campaignsIds' => $campaignsIds,
                'campaignsData' => $campaignsData
            );
        }

        $datos = array();

        foreach ($campStruct as $val) {

            $temp = twitter_stats_generic($infoCredentials['user_id'], $accountw, AnalyticsFields::CAMPAIGN, $val['campaignsIds'], 'P1D', $data['start_date'], $data['end_date'], $val['campaignsData']);

            if (count($datos) > 0) {
                $datos = array_merge($datos, $temp);
            } else {
                $datos = $temp;
            }
        }

        helper_metrics_campana_day( $infoCredentials['platform'],
            $infoCredentials['user_id'],
            $infoCredentials['customer_id_default'],
            $datos,
            $cuentaTwBBDD['id'],
            $cuentaTwBBDD['account_id'],
            $infoCredentials );

    } catch (exception $e) {
        echo "Error api twitter: " . $e->getMessage();
        //die();
    }

}

function get_stats_global($parentobject, $infoCredentials, $params, $message = '', $error_message = [], $parentevent = ''){

}

function get_stats_campaigns( $adAccount_param, $data , $infoCredentials){

}

function get_stats_ad($adAccount_param, $data , $infoCredentials ){

}

function get_stats_adSet($adAccount_param, $data , $infoCredentials ){

}
?>