<?php
use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;
use Hborras\TwitterAdsSDK\TwitterAds\LineItem\LineItem;
//use Hborras\TwitterAdsSDK\TwitterAds\Campaign\FundingInstrument;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\BadRequest;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\Forbidden;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotAuthorized;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotFound;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\RateLimit;
use Hborras\TwitterAdsSDK\TwitterAds\Creative\PromotedTweet;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServerError;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServiceUnavailable;
use Hborras\TwitterAdsSDK\TwitterAdsException;
use Hborras\TwitterAdsSDK\TwitterAds\Cursor;
use Hborras\TwitterAdsSDK\TwitterAds\Enumerations;
use Hborras\TwitterAdsSDK\TwitterAds\Fields\AnalyticsFields;
use Hborras\TwitterAdsSDK\TwitterAds\Fields\LineItemFields;
Cursor::setDefaultUseImplicitFetch(true);

function get_account_adsets($adAccount_data, $infoCredentials,$twitterapi=FALSE,$apibasic=FALSE){
  //$promotable_users = $twitterapi->get('accounts/'.$adAccountPlatformId.'/promotable_users', array('with_deleted'=> true)); 
  
 if (!isset($adAccount_data['account_platform_id'] )) { return false; }
  $adAccountPlatformId= $adAccount_data['account_platform_id'];
  
  if (!$twitterapi) {
    $twitterapi=loginApi($infoCredentials, $infoCredentials['app_id'], $infoCredentials['app_secret']);
  }  
  if (!$apibasic) {
      $apibasic=loginApiBasic($infoCredentials ) ;
  } 
     
 
    try {

        echo 'Ad Account = ' . $adAccount_data['account_platform_id'] . PHP_EOL. PHP_EOL;
        $account = new Account($adAccount_data['account_platform_id']);


        $account->read();
 // usar total count reduce el numero de request a 200 en 15 minutos TwitterAds\Fields\CampaignFields::WITH_TOTAL_COUNT => true,TwitterAds\Fields\CampaignFields::WITH_DRAFT=> false,
        $items = $account->getLineItems('', [TwitterAds\Fields\CampaignFields::COUNT => 200,  TwitterAds\Fields\CampaignFields::SORT_BY=> 'created_at-desc' ]);
        $items->setUseImplicitFetch(true);

   
   
        //$cuentaTwBBDD = getAdAccount_Element($data['ad_account_id']);

        foreach ($items as $item) {

               
                  // print_r(get_class_methods($item));
            echo 'ID adset = '. $item->getId() . ' - Nombre = ' .$item->getName() . PHP_EOL;
             $itemData=[];
              $itemData['id']=  $item->getId()  ;
              $itemData['campaign_id']=  $item->getCampaignId()  ;
              $itemData['name']=  $item->getName() ;
              $itemData['currency']=  $item->getCurrency()  ;
              $itemData['status']=  $item->getEntityStatus()  ;
              $itemData['effective_status']=  $item->getEntityStatus()  ;
              $itemData['chargeby']=  $item->getPayBy()  ;
              $itemData['objective']=  $item->getObjective()  ;
              $itemData['optimization_goal']=  $item->getGoal()  ;
              $itemData['targeting']= [ ] ;
              $itemData['promoted_object']= [ ] ;
               $itemData['start_time']=  ($item->getStartTime())?  $item->getStartTime()->getTimestamp():null  ;   
              $itemData['end_time']=   ($item->getEndTime())?  $item->getEndTime()->getTimestamp():null  ;
              $itemData['lifetime_budget']=   $item->getTotalBudgetAmountLocalMicro()/1000000 ;
              $itemData['puja_valor']=   $item->getBidAmountLocalMicro()/1000000 ;
              $itemData['bid_strategy']=   $item->getBidStrategy() ;
              $itemData['product_type']=   $item->getProductType() ;
              $itemData['daily_budget']=   null  ;
              $itemData['targeting']['publisher_platforms']=   $item->getPlacements() ;
            $itemData['metadata']=  $item->toArray() ;
    
            persistAdset( $itemData, $infoCredentials,  $adAccount_data,   $infoCredentials['platform'] );
           
        }

    } catch (exception $e) {
        echo "Error api twitter: " . $e->getMessage();
        //die();
    }
}

 

?>