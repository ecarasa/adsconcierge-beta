<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
//error_reporting(E_ERROR   | E_ALL  );
error_reporting(E_ERROR   | E_PARSE  );

define('PLATFORM_NAME','TWITTER');
define('PLATFORM_ID',3);
define('VERSIONCODIGO','TW_2.1.0'); //max 10 char
define('VERSIONGTASKS','_TW_2_1.0'); //max 10 char
define('VERBOSE',FALSE);
define('FUNCTION_API_NAME','function-twitter-api');
define('CLOUD_COLANAME','twitter-sync');
define('CLOUD_ENABLED',TRUE);


use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;
use Hborras\TwitterAdsSDK\TwitterAds\Campaign\Campaign;
use Hborras\TwitterAdsSDK\TwitterAds\Campaign\FundingInstrument;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\BadRequest;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\Forbidden;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotAuthorized;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotFound;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\RateLimit;
use Hborras\TwitterAdsSDK\TwitterAds\Creative\PromotedTweet;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServerError;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServiceUnavailable;
use Hborras\TwitterAdsSDK\TwitterAdsException;
use Hborras\TwitterAdsSDK\TwitterAds\Cursor;
use Hborras\TwitterAdsSDK\TwitterAds\Enumerations;
use Hborras\TwitterAdsSDK\TwitterAds\Fields\AnalyticsFields;
use Abraham\TwitterOAuth\TwitterOAuth;

use Psr\Http\Message\ServerRequestInterface;
 

include 'vendor/autoload.php';
include 'configs/twitter.php';
include 'configs/general.php';
include 'configs/stats_mapeometrics.php';
include 'common/basics.php';
include 'common/persists.php';
include 'common/persistsStats.php';
include 'common/extLogger.php';
include 'get_properties.php';
include 'campaigns.php';
include 'atomos.php';
include 'ads.php';
include 'get_stats.php';
/*
include 'creativities.php';
**/

include 'helpers/metrics.php';

$DEBUG = false;



function refresh_token($infoCredentials)
{
 
}


//chequear token vive
 
function check_token_isalive($infoCredentials)
{
 
  
}

function loginApi($infoCredentials, $appId, $appSecret)
{
   $user_credentials = json_decode($infoCredentials['retorno']);
   $twitterapi = TwitterAds::init($infoCredentials['app_id'], $infoCredentials['app_secret'], $user_credentials->oauth_token, $user_credentials->oauth_token_secret);
return	$twitterapi;	
}

function loginApiBasic($infoCredentials ) {
	  $user_credentials = json_decode($infoCredentials['retorno']);
	$basicapi = new TwitterOAuth($infoCredentials['app_id'], $infoCredentials['app_secret'], $user_credentials->oauth_token, $user_credentials->oauth_token_secret);
//$content = $basicapi->get("account/verify_credentials");
//	print_r($content );
	return $basicapi;
}
function retrieve_all($data, $infoCredentials){
 
	if (VERBOSE) {
    echo 'Retrieve all entities for ' . $infoCredentials['user_name'] . ' ' . $infoCredentials['user_email'] . PHP_EOL;
	}

	  $accountsArray = get_adsAccounts($data, $infoCredentials);
		$random=rand();
    foreach ($accountsArray as $adAccount){
 
					
	 /*	$adAccountData=	getAdAccount_DataByPlatformId($adAccount, $infoCredentials);
			  get_account_campaigns($adAccountData, $infoCredentials);
			  get_account_adsets($adAccountData, $infoCredentials);
			  get_account_ads($adAccountData, $infoCredentials);
	 */
 
			/***
        // buscamos properties
        $event = array(  'auth_id' => $infoCredentials['id'], 'type' => 'get_properties','action' => 'get_account_properties', 'function' => 'function-facebook-api', 'subject_id' =>  $adAccount['id'], 'callchild'=> [ array('type' => 'get_campaigns')     ] );
				execGoogleTask($event, $data);
        $event = array(  'auth_id' => $infoCredentials['id'], 'type' => 'get_properties','action' => 'get_account_campaigns', 'function' => 'function-tw-api', 'subject_id' =>  $adAccount['id'], 'callchild'=> [ ] );

				execGoogleTask($event, $data);
				**/ 
		 
				//delay de 10 minutos para que de tiempo a traerse las properties, los tiempos son arbitrarios y estimados en cuanto puede llevar ejecutarse la funcion
				//no se hace encadenado por si falla la tarea no entrar en un bucle infinito
	 			//echo 'get campa'.PHP_EOL;
	
        $event = array('delaySeconds'=>false, 'random'=>$random, 'action' => 'get_account_campaigns','entity_platformId' => $adAccount,'ad_account_platformId' => $adAccount, 'type' => 'get_entity_data', 'function' => FUNCTION_API_NAME, 'entity_name'=> 'ad_account'  );
				execGoogleTask($event,$infoCredentials,$data);
				//echo 'get ADSET '.PHP_EOL;
			//delay de 30 minutos para que de tiempo a traerse las campaigns
				$event = array('delaySeconds'=>1800,  'random'=>$random, 'action' => 'get_account_adsets','entity_platformId' => $adAccount, 'ad_account_platformId' => $adAccount,  'type' => 'get_entity_data', 'function' => FUNCTION_API_NAME, 'work_entity_name'=> 'ad_account'  );
        execGoogleTask($event,$infoCredentials,$data);
					// echo 'get ADS '.PHP_EOL;
				//delay de 90 minutos para que de tiempo a traerse las get_account_adsets
				$event = array('delaySeconds'=>5400,  'random'=>$random, 'action' => 'get_account_ads','entity_platformId' => $adAccount, 'ad_account_platformId' => $adAccount,   'type' => 'get_entity_data', 'function' => FUNCTION_API_NAME, 'work_entity_name'=> 'ad_account'  );			
        execGoogleTask($event,$infoCredentials,$data);
 
			//exit;
			/**
				//aqui va una tarea a las 3 horas para ejecutar los update de id's internos  (campana_root, customer_id, campanaplatform_id etc) en las tablas por si no dio tiempo a que exisitieran cuando se importaron
			***/
    }
}

function retrieve_all_adsAccounts_stats($requestParams,$period='last_3d' , $infoCredentials = []){
				$adAccounts= getAllAdsAccounts_by_authPublicId( $infoCredentials);
  			$delay=0;	
			$random=rand();
  			foreach ($adAccounts as $adAccountData ){
				//solo llamamos a stats por ads, y de ahí consolidamos en adsets y campanas, asi ahorramos api calls	
			//'delaySeconds'=> $delay+=30,
				$event = array( 'random'=>$random,  'action' => 'get_account_stats_ads','ad_account_publicId' => $adAccountData['public_id'], 'periodEnum'=> $period , 'entity_publicId' => $adAccountData['public_id'],  'entity_platformId' =>  $adAccountData['account_platform_id'],  'type' => 'get_account_stats', 'function' => FUNCTION_API_NAME, 'work_entity_name'=> 'ad_account'  );								       
				execGoogleTask($event,$infoCredentials,$requestParams);	
	 
				}
        
}

function index(ServerRequestInterface $request){ 
//function index(){
  //datos demo
	//si no hay request se ejecuta desde php cli
 
 if (!isset($request) ) {   
    /*     $dataRequest=[ 'action' => 'retrieve_all', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
            $dataRequest=[ 'action' => 'get_account_adsaccounts', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
      $dataRequest=[ 'action' => 'get_account_properties', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
      $dataRequest=[ 'action' => 'get_account_pixels', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
     $dataRequest=[ 'action' => 'get_account_campaigns', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
     $dataRequest=[ 'action' => 'get_account_adsets', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
      $dataRequest=[ 'action' => 'get_account_ads', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
  
 $dataRequest=[ 'action' => 'get_stats_campaigns', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
 $dataRequest=[ 'action' => 'get_account_stats_adsets', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
 $dataRequest=[ 'action' => 'get_account_stats_ads', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
  */
	/*
	{"auth_id":"61c5fda2-0057-11ec-8d81-ac1f6b17ff4a","auth_publicId":"61c5fda2-0057-11ec-8d81-ac1f6b17ff4a","action":"get_account_adsets",
	"ad_account_publicId":false,"entity_publicId":false,"entity_platformId":"9pk1j","work_entity_name":"ad_account","periodEnum":false,"type":"get_entity_data","callchild":[],"versioncodigo":"TW_2.0.9"}
  
      $dataRequest=[ 'action' => 'get_account_stats_ads', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '18ce55ekt8c'];
  */     $dataRequest=[ 'action' => 'retrieve_all', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a" ];
     // $dataRequest=[ 'action' => 'get_account_adsets', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'periodEnum' => 'last_30d',	 'ad_account_platformId' => '18ce55ekt8c', 'ad_account_publicId' => '0ad09116-147f-11ec-8d81-ac1f6b17ff4a'];
     $dataRequest=[ 'action' => 'get_account_ads', 'auth_id'=> "61c5fda2-0057-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '18ce55ekt8c'];
 }
 
	
    if (isset($request)){
        $body = $request->getBody()->getContents();
        $dataRequest = json_decode($body, true);
        if(!isset($dataRequest['action'])){ echo 'No action defined in request'; die(); }
        if( !isset($dataRequest['auth_id'])  && !isset($dataRequest['auth_publicId']) ){ echo 'No AUTH ID defined in request'; die(); }
    }

    // conformamos el request
    $requestParams = array(
        'action'=> isset($dataRequest['action']) ? $dataRequest['action'] : false,
        'auth_id'=>isset($dataRequest['auth_id']) ? $dataRequest['auth_id'] : ( isset($dataRequest['auth_publicId']) ? $dataRequest['auth_publicId'] : NULL ), // 139 = 2ccd9123-d929-11eb-8d81-ac1f6b17ff4a
        'auth_publicId'=>isset($dataRequest['auth_publicId']) ? $dataRequest['auth_publicId'] : ( isset($dataRequest['auth_id']) ? $dataRequest['auth_id'] : NULL ) , // 139 = 2ccd9123-d929-11eb-8d81-ac1f6b17ff4a					
        'ad_account_platformId' => isset($dataRequest['ad_account_platformId']) ? $dataRequest['ad_account_platformId'] :  ( isset($dataRequest['ad_account_id']) ? $dataRequest['ad_account_id'] : NULL ) ,  // VACIO 
        'ad_account_publicId' => isset($dataRequest['ad_account_publicId']) ? $dataRequest['ad_account_publicId'] : false,  // VACIO
				'work_entity_name' => isset($dataRequest['work_entity_name']) ? $dataRequest['work_entity_name'] : false,  // VACIO			
         'entity_platformId' => isset($dataRequest['entity_platformId']) ? $dataRequest['entity_platformId'] : false,  // VACIO			
         'entity_publicId' => isset($dataRequest['entity_publicId']) ? $dataRequest['entity_publicId'] : false,  // VACIO			
        	'periodEnum'=> isset($dataRequest['periodEnum']) ? $dataRequest['periodEnum'] :  false ,// opcional
			//compatiblidad
				 'platform_object_id' => isset($dataRequest['ad_account_id']) ? $dataRequest['ad_account_id'] : null,  // VACIO
        'start_date'=> isset($dataRequest['start_date']) ? $dataRequest['start_date'] : date('Y-m-d', strtotime(' - 7 day')), // opcional
        'end_date'=> isset($dataRequest['end_date']) ? $dataRequest['end_date'] : date('Y-m-d', strtotime('today')) ,// opcional

        'callchild'=> isset($dataRequest['callchild']) ? $dataRequest['callchild'] : []  // opcional
    );

	  $loggermessage = array( 'level'=> 'info', 'source'=>'index' , 'step' => 'inicio index', 'payload'=>serialize($requestParams) );
		extLogger($loggermessage );
	
	
  if ($requestParams['action']== null){ 
    echo 'NO ACTION SPECIFIED'; 
                                         
  $loggermessage = array( 'level'=> 'info', 'source'=>'index' , 'shortmessage'=>  'NO ACTION SPECIFIED', 'payload'=>serialize($requestParams) );
             extLogger($loggermessage );                   
                                die(); 
  }

 
 
 
					$infoCredentials= 	getUserCredentialsByPublicId($requestParams['auth_publicId'] );
			     $user_id = $infoCredentials['user_id'];
 
 
    
 		
 
			
    // validamos token
    try { 
  // check_token_isalive($infoCredentials);
		 
    }catch (Exception $e){      
       $loggermessage = array( 'level'=> 'Exception', 'source'=>'GeneralException' ,  'shortmessage'=>  'token no validado',  'message'=>    $e->getMessage(),  'user_id' => $infoCredentials['user_id'], 'auth_id' => $infoCredentials['id'], 'payload'=>serialize($requestParams),  'error' => serialize($e));
             extLogger($loggermessage );
          
        echo 'Exeption as ' . $e->getMessage() . PHP_EOL;
        exit;
    }

 
		if (isset($requestParams['ad_account_platformId'])  &&  $requestParams['ad_account_platformId']!=false ) { 
					$adAccountData= 	getAdAccount_DataByPlatformId($requestParams['ad_account_platformId'] );
				}
						if (!isset($adAccountData) &&  $requestParams['ad_account_publicId']   ) {  		echo 'ad_account_publicId'.PHP_EOL;
					$adAccountData= 	getAdAccount_ByPublicID($requestParams['ad_account_publicId'] );
				}

	switch ($requestParams['action']) {

        case "retrieve_all":        
            retrieve_all($requestParams, $infoCredentials);
            
            break;
        case "get_account_adsaccounts":
            get_adsAccounts($requestParams , $infoCredentials);
            break;				
				
        case "get_account_properties":
        case "get_properties":
		//to-do falla en fb
						 
          //  get_account_properties($adAccountData, $infoCredentials);
            break;
				
        case "get_account_pixels":
        case "get_pixels":
				echo 'getpisexl';
				// no funciona todavia
          //  get_account_pixels($requestParams['ad_account_id'], $infoCredentials);
            break;       
				
				
        case "get_account_campaigns":
        case "get_campaigns":
           
            get_account_campaigns($adAccountData, $infoCredentials);
            break;





        case "get_adsets":
        case "get_account_adsets":
            get_account_adsets($adAccountData, $infoCredentials);
            break;

        case "get_account_ads":
							get_account_ads($adAccountData, $infoCredentials);
				break;
				// TODO
        case "get_creativities":
            get_creativities($adAccountData, $infoCredentials);
            break; 

        // STATS
					// TODO
        case "get_stats_campaigns":
        case "get_account_stats_campaigns":
            get_account_stats_campaigns($adAccountData, $requestParams, 'last_30d' , $infoCredentials ); 
            break;    
				
					// TODO
        case "get_stats_adSet":
        case "get_account_stats_adsets":
            get_account_stats_adSets($adAccountData, $requestParams , 'last_7d' , $infoCredentials ); 
            break;  

        case "get_stats_ad":
        case "get_account_stats_ads":
				echo 'get ads stats';
            get_account_stats_ads($adAccountData, $requestParams , 'last_7d' , $infoCredentials );  //to-do el periodo que venga desde el request
            break;  

        case "retrieve_stats_all":
        case "retrieve_all_accounts_stats":
 
          	retrieve_all_adsAccounts_stats($requestParams,'last_3d' , $infoCredentials , null);
 
            break;
        default:
            echo "NO ACTION FOUND";

    }
       
    if ( !empty($requestParams['callchild'] )  ) {
        $event = array(  'auth_id' => $infoCredentials['id'], 'type' => null , 'function' => 'function-facebook-api',  'subject_id' =>  $fila['account_id'] );
      foreach ($requestParams['callchild'] as $item  ) {
        $new_event=array_merge_recursive($event, $item   );
            execGoogleTask($new_event);
      }

      
    }
	return 'fin';
exit;
}
  //index();
?>