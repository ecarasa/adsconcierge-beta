<?php
use FacebookAds\Api;
use FacebookAds\Cursor;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Post;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\User;
use FacebookAds\Http\Exception\ServerException;
Cursor::setDefaultUseImplicitFetch(false);

function get_stats_global($parentobject, $infoCredentials, $params,  $parentevent = ''){
      $fields = ['account_id','impressions','clicks','campaign_id','campaign_name','account_currency','buying_type','date_start','objective','reach','spend','inline_post_engagement','cost_per_action_type','cpc','cpm','website_ctr'];
    switch ($parentevent) {
        case 'get_stats_campaigns':
        case 'get_account_stats_campaigns':
            $fields = ['account_id','impressions','clicks','campaign_id','campaign_name','account_currency','buying_type','date_start','objective','reach','spend','inline_post_engagement','cost_per_action_type','cpc','cpm','website_ctr'];
            break;
        case 'get_stats_ad':
            //$fields = ['ad_id, adset_id, impressions', 'campaign_id', 'account_id', 'account_currency', 'buying_type', 'campaign_name', 'clicks', 'conversions', 'date_start', 'date_stop', 'objective', 'reach', 'spend', 'inline_post_engagement', 'actions', 'wish_bid', 'ad_bid_type', 'social_spend', 'video_thruplay_watched_actions', 'video_play_actions', 'ad_bid_value'];
            $fields[]= 'ad_id';
            $fields[]= 'ad_name';
            $fields[]= 'adset_id';
            $fields[]= 'campaign_id';
            break;
        case 'get_stats_adSet':
            $fields[]= 'adset_name';
            $fields[]= 'adset_id';
        // ['impressions', 'campaign_id', 'account_id', 'account_currency', 'clicks', 'conversions', 'date_start', 'date_stop', 'objective', 'reach', 'spend',  'ad_bid_type', 'video_play_actions', 'ad_bid_value', 'adset_name', 'adset_id'];
            break;
    }

    $api = Api::init($infoCredentials['app_id'], $infoCredentials['app_secret'], $infoCredentials['access_token']);
    try {
       // echo $message;
     
        $datos = ( $parentobject )->getInsights($fields, $params);
        $datos->setDefaultUseImplicitFetch(true);
        $retorno = [];

        foreach ($datos as $citem) {
            $retorno[] = $citem->getData();
        } 
        return $retorno;
    } catch (Exception $e) {
        print_r($e->getMessage());      
      $loggermessage = array( 'level'=> 'Exception', 'category'=>'FbapiError', 'message'=> $e->getMessage()  , 'params'=> json_encode($params),  'infoCredentials' => json_encode(  $infoCredentials ) );
          extLogger($loggermessage );
    }
}
// addaccount/campaign stats
//https://developers.facebook.com/docs/marketing-api/insights/parameters/v11.0
// date_preset enum{today, yesterday, this_month, last_month, this_quarter, maximum, last_3d, last_7d, last_14d, last_28d, last_30d, last_90d, last_week_mon_sun, last_week_sun_sat, last_quarter, last_year, this_week_mon_today, this_week_sun_today, this_year}
function get_account_stats_campaigns( $adAccount, $data ,$period='last_3d', $infoCredentials){    
    $parentobject =  new AdAccount("act_" . $adAccount );
    $AdAccountObj = getAdAccount_DataByPlatformId($adAccount);
    $params = [
        'level' => 'campaign',
        'date_preset' => 'last_3d',
        'time_increment' => '1',
        'breakdowns' => 'publisher_platform,platform_position,device_platform'
    ];
 if (isset($period)  ) { $params['date_preset']= $period;    }
  
    $message = 'Stats campaigns ad_account: '. $data['ad_account_id'] .' from '. $data['start_date'].' to '. $data['end_date'] . PHP_EOL;
    $error_message = ['add_acount' => $adAccount];
    $bulkdata = get_stats_global($parentobject, $infoCredentials, $params,  'get_stats_campaigns');
 
    echo PHP_EOL . PHP_EOL . ' Qty items - FB API'. count($bulkdata) . PHP_EOL;    
   persistStats_platform_Campana_day($infoCredentials, $infoCredentials['platform'], $AdAccountObj, $bulkdata );  
}

function get_account_stats_adSets( $adAccount, $data ,$period='last_3d', $infoCredentials){    
    $parentobject =  new AdAccount("act_" . $adAccount );
    $AdAccountObj = getAdAccount_DataByPlatformId($adAccount);
    $params = [
        'level' => 'adset',
        'date_preset' => 'last_3d',
        'time_increment' => '1',
        'breakdowns' => 'publisher_platform,platform_position,device_platform'
    ];
 if (isset($period)  ) { $params['date_preset']= $period;    }
    $message = 'Stats adset ad_account: '. $data['ad_account_id'] .' from '. $data['start_date'].' to '. $data['end_date'] . PHP_EOL;
    $error_message = ['add_acount' => $adAccount];
    $bulkdata = get_stats_global($parentobject, $infoCredentials, $params,  'get_stats_adSet');
 
    echo PHP_EOL . PHP_EOL . 'get_account_stats_adSet  Qty items - FB API'. count($bulkdata) . PHP_EOL;    
   persistStats_platform_Atomo_day($infoCredentials, $infoCredentials['platform'], $AdAccountObj, $bulkdata );  
}


function get_account_stats_ads( $adAccount, $data ,$period='last_3d', $infoCredentials){    
    $parentobject =  new AdAccount("act_" . $adAccount );
    $AdAccountObj = getAdAccount_DataByPlatformId($adAccount);
    $params = [
        'level' => 'ad',
        'date_preset' => 'last_3d',
        'time_increment' => '1',
        'breakdowns' => 'publisher_platform,platform_position,device_platform'
    ];
  //  'time_range' => array('since' => substr($data['start_date'], 0, 10), 'until' => substr($data['end_date'], 0, 10)),
 if (isset($period)  ) { $params['date_preset']= $period;    }
    $message = 'Stats adset ad_account: '. $data['ad_account_id'] .' from '. $data['start_date'].' to '. $data['end_date'] . PHP_EOL;
    $error_message = ['add_acount' => $adAccount];
    $bulkdata = get_stats_global($parentobject, $infoCredentials, $params,  'get_stats_ad');
 
    echo PHP_EOL . PHP_EOL . 'get_account_stats_ads  Qty items - FB API'. count($bulkdata) . PHP_EOL;    
   persistStats_platform_Ads_day($infoCredentials, $infoCredentials['platform'], $AdAccountObj, $bulkdata );  
}



 
 

?>