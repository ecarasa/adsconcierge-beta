<?php
use FacebookAds\Api;
use FacebookAds\Cursor;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\User;
use FacebookAds\Http\Exception\ServerException;
Cursor::setDefaultUseImplicitFetch(false);


function get_account_campaigns($adAccountData, $infoCredentials){
    // get_campaigns($data['ad_account_id'], $infoCredentials);
    print_r( $adAccountData);

  //  global $data,  $api;
$adAccount = $adAccountData['account_platform_id'];

    echo ' - Search Campaigns -  '. PHP_EOL;

    $items = ( (new AdAccount('act_'.$adAccount) )->getCampaigns(CampaignFields::getInstance()->getvalues(),  array('summary' => false) ));
    $items->setDefaultUseImplicitFetch(true);
    
    $adAccount_data=getAdAccount_DataByPlatformId($adAccount ); 
 
  
    echo ' . AdAccount : ' . $adAccount . ', ';
    echo ' - Campaigns : ' . count($items) . PHP_EOL;

    $arr = array();

    foreach ($items as $citemmm) {

        $citem = $citemmm->getData();
     
  
        echo '      -> Campaign: ' . $citem['name'] . '  act_' . $adAccount . PHP_EOL;
    
      persistCampaign( $citem, $infoCredentials,  $adAccount_data,   $infoCredentials['platform']);

       

    }

    // Get campaigns, después del loop de inserción en tabla de las campañas, lanzas una sola task get adsets de la cuenta, en lugar de champaña
    $event = array( 'auth_id' => $infoCredentials['id'], 'type' => 'get_account_adsets', 'function' => 'function-facebook-api',  'subject_id' => $adAccount);
   // execGoogleTask($event);

    //$event = array( 'auth_id' => $infoCredentials['id'], 'type' => 'get_stats_campaigns', 'function' => 'function-facebook-api',  'subject_id' => $adAccount);
    //execGoogleTask($event);

}


function campaignplatform_create($app_id, $app_secret,$access_token,$userid, $add_account_id,$rowdata)
{
    $api = Api::init($app_id, $app_secret ,$access_token);
    $fields = array();
    $params = array(
        'name' => $userid . '|' . $rowdata['id'] . ':' . $rowdata['name'] . ' ' . date('Y/m/d h:i:s a', time()),
        'objective' => $rowdata['objective'], //ver esto https://developers.facebook.com/docs/reference/ads-api/adcampaign#create
        //enum{APP_INSTALLS, BRAND_AWARENESS, CONVERSIONS, EVENT_RESPONSES, LEAD_GENERATION, LINK_CLICKS, LOCAL_AWARENESS, MESSAGES, OFFER_CLAIMS, PAGE_LIKES, POST_ENGAGEMENT, PRODUCT_CATALOG_SALES, REACH, VIDEO_VIEWS}
        'status' => 'PAUSED',
    );
    $campananueva=(new AdAccount($add_account_id))->createCampaign($fields,  $params)->exportAllData();

    return $campananueva;
}

function campaignplatform_update($fb, $data, $infoCredentials)
{
    echo '** PLATFORM '. $infoCredentials['platform'].PHP_EOL;
    print_r($data);

    switch($infoCredentials['platform']){
        case 'FACEBOOK':
            fb_update_campaign_fields($fb, $infoCredentials, $data['parent_platform_id'], $data['fields']);
            break;
    }
}

function campaignplatform_delete($fb, $data, $infoCredentials)
{
    echo '** PLATFORM '. $infoCredentials['platform'].PHP_EOL;
    print_r($data);

    switch($infoCredentials['platform']){
        case 'FACEBOOK':
            fb_update_campaign_fields($fb, $infoCredentials, $data['parent_platform_id'], $data['fields']);
            break;
    }
}


function fb_update_campaign_fields($fb, $infoCredentials, $campaign_id_en_platform, $fields)
{

    try {
        // endpoint definition
        $endpoint =  "/" . $campaign_id_en_platform;
        echo '** Graph version '. $fb->getDefaultGraphVersion() .PHP_EOL;
        $response = $fb->post($endpoint, $fields, $infoCredentials['access_token'], null, 'v10.0');
        var_dump($response);

    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }

    $graphNode = $response->getGraphNode();
    print_r($graphNode);
    // return $graphNode;
    /* handle the result */
}
?>