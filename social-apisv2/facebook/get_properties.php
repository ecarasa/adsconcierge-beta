<?php
use FacebookAds\Api;
use FacebookAds\Cursor;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Post;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\User;
use FacebookAds\Http\Exception\ServerException;
Cursor::setDefaultUseImplicitFetch(false);

function get_account_properties($account, $infoCredentials){
    //            get_properties($data['ad_account_id'], $infoCredentials);
    global  $dbconn;

    $fields = array('id', 'name', 'category');
    $params = array( 'summary' => true);

    echo 'Search Properties '. PHP_EOL;

    $adAccount = new AdAccount('act_' . $account);

    echo ' Properties from Addcount ' . ' act_' . $account . PHP_EOL;
   try {
    foreach ( $adAccount->getPromotePages($fields,  $params) as $p){

        $page= $p->getData();
        $page_id=$page['id'];
        $pagename=$page['name'];
        $category=$page['category'];
        $token= $infoCredentials['access_token'];

        $status=0;
        $metadata=json_encode( $page, true);
        $stmt2 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` (`user_id`, `platform`, `app_id`,`platform_user_id`,
            `id_en_platform`, `token`,`name`,`status`,`category`, `metadata`, `adaccount_id`, `auth_id`)
                                VALUES (?,?,?,?,?,?,?,?,?,?,?,?)
                                ON DUPLICATE KEY UPDATE    `name`= ?,  `status`= ?,`platform_user_id`=?,  `metadata`= ? ");


        $stmt2->bind_param("ssssssssssssssss",
            ...[$infoCredentials["user_id"], $infoCredentials["platform"], $infoCredentials["app_id"], $infoCredentials['platform_user_id'], $page_id, $token,
                $pagename, $status, $category, $metadata, $account, $infoCredentials["id"],
                $pagename, $status, $infoCredentials['platform_user_id'], $metadata]);
        $stmt2->execute();

        echo '   Property added -> ' . $page['name']. PHP_EOL;

        $query = "SELECT id as pageid, public_id as property_public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE  auth_id = '{$infoCredentials["id"]}' and platform = '{$infoCredentials["platform"]}' and platform_user_id= '{$infoCredentials['platform_user_id']}' and id_en_platform = '{$page_id}'";
        $res_pa = $dbconn->query($query);
        $id_pa = $res_pa->fetch_assoc();

        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` 
                            (`type`,`auth_id`, `ad_account`,  `ad_account_name`,`property_name`, 
                            `property_id`, `user_id`,`platform`,`auth_publicid`,   `user_publicid`,  
                            `ad_account_publicid`,`property_publicid`) 
                            VALUES (
                                'PAGE', 
                                ?,
                                ?,
                                (SELECT name FROM app_thesoci_9c37.ads_accounts where ads_accounts.auth_id = ? and account_id = ?),
                                ?,
                                ?,
                                ?,
                                ?,
                                ?,
                                ?,
                                (SELECT public_id FROM app_thesoci_9c37.ads_accounts where account_id = ? and ads_accounts.auth_id = ?),
                                ?)
                            ON DUPLICATE KEY UPDATE `ad_account_name` = (SELECT name FROM app_thesoci_9c37.ads_accounts where ads_accounts.auth_id = ? and account_id = ?), 
                            `property_name` = ?");

        $adc_publicid= getAdAccount_DataByPlatformId($account);

        echo "SELECT name FROM app_thesoci_9c37.ads_accounts where ads_accounts.auth_id = ".$infoCredentials["id"]." and account_id = ".$account.PHP_EOL;

        $stmt3->bind_param("ssssssssssssssss",
            ...[$infoCredentials["id"],
                $account,
                $infoCredentials["id"],
                $account,
                $pagename,
                $id_pa["pageid"],
                $infoCredentials["user_id"],
                $infoCredentials["platform"],
                $infoCredentials["auths_user_id"],
                getUserPublicId($infoCredentials["user_id"]),
                $account,
                $infoCredentials["id"],
                $id_pa["property_public_id"],
                $account,
                $infoCredentials["id"],
                $pagename]);

        $stmt3->execute();
			 if ($stmt3->error != "") {
        extLogger(array( 'level'=> 'Error', 'category'=>'mysqlError', 'message'=> $stmt3->error  , 'infoCredentials' => json_encode(  $infoCredentials ) ) );
        }
	 

    }
		    } catch (Exception $e) {
      print_r($e->getMessage());
		   extLogger(array( 'level'=> 'Exception', 'category'=>'apiError',  'message'=> $e->getMessage()   , 'infoCredentials' => json_encode(  $infoCredentials ) ) );
    }  
}
 
function get_adsAccounts( $requestParams ,$infoCredentials){

    global $dbconn_stats, $dbconn;
//print_r($infoCredentials);
  //  $fields = array('account_status','name','id','age','amount_spent','attribution_spec','account_id','balance','business_name','business_city','business_country_code','currency','owner','partner','user_tos_accepted','spend_cap','tos_accepted','offsite_pixels_tos_accepted','user_tasks','disable_reason','has_migrated_permissions','is_prepay_account','media_agency','can_create_brand_lift_study','is_direct_deals_enabled','is_in_middle_of_local_entity_migration');
    $fields = array('account_status','name','id','age','amount_spent','attribution_spec','account_id','balance','business_name','business_city','business_country_code','currency', 'partner','user_tos_accepted','spend_cap','tos_accepted','offsite_pixels_tos_accepted', 'user_tasks','disable_reason', 'has_migrated_permissions',  'media_agency','can_create_brand_lift_study','is_direct_deals_enabled' ,'is_in_middle_of_local_entity_migration' );
    $params = array('summary' => true, 'limit'=> 80);

    echo 'Search - Ads Accounts '. PHP_EOL;

    try{
        $adAccounts = (new User($infoCredentials['platform_user_id']))->getAdAccounts($fields, $params);
        $adAccounts->setDefaultUseImplicitFetch(true);

        $arr = array();

        foreach($adAccounts as $adAccount) {

            $accountItem = $adAccount->getData();
		//			print_r($accountItem );
				
            echo 'get_adsAccounts '. $accountItem['account_id'] . ' --- ' . $accountItem['name'] . ',' . PHP_EOL;
           // array_push($arr, array("id"=>$accountItem['account_id'], "currency"=>$accountItem['currency']));
            //array_push($arr, array("id"=>$accountItem['account_id'], "currency"=>$accountItem['currency']));
					$arr[]= $accountItem['account_id'];
            persistAdAccount($infoCredentials['user_id'], $accountItem, $infoCredentials);
 
            //break;

        }

    } catch (Exception $e) {
        echo $e->getMessage().PHP_EOL;
    }

    return $arr;
}

function get_account_pixels($ad_account, $infoCredentials){

    global  $dbconn_stats, $dbconn;

    echo 'SEARCH Pixels '. PHP_EOL;

    $fields = array('id',
        'name',
        'is_unavailable',
        'code',
        'owner_ad_account',
        'account_id',
        'first_party_cookie_status',
        'automatic_matching_fields');

    $adAccount = new AdAccount('act_'.$ad_account);
  //  $adAccountData = $adAccount->getData();
    $adPixels = $adAccount->getAdsPixels($fields,  array('summary' => true, 'limit' => 80));

    foreach ($adPixels as $adPixel) {

        $pixel = $adPixel->getData();
        //var_dump($pixel);
        //die();

        $itemData = array_intersect_key($adPixel->getData(), array_flip($fields));
        //$adPixelData = array_merge($adPixelData, $this->userPlatformData);
        $itemData['ad_account_platformId'] = $pixel['owner_ad_account']['account_id'];
        $itemData['id_en_platform'] = $adPixel->getData()['id'];
        $itemData['id'] = $adPixel->getData()['id'];
        $itemData['name'] = $adPixel->getData()['name'];
      /**  $adPixelData['status'] = 0; //todo: fixed in origin code
        $adPixelData['category'] = '-';
        $adPixelData['token'] = '-';
        $adPixelData['type'] = 'PIXEL';
**/
        // $this->persistPropertiesAccounts->execute($adPixelData);
     //  print_r ( $itemData );
		 
			persistPixel(  $itemData, $infoCredentials,  $pixel['owner_ad_account']['account_id'] );

        // buscamos y persistimos la relacion de las ads con la properties_accounts

    
        // die();
    }
}
