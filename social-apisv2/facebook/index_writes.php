<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'vendor/autoload.php';
include 'configs/general.php';
include 'configs/stats_mapeometrics.php';

include 'basics.php';
include 'atomo.php';
include 'campaigns.php';
include 'creativities.php';

use FacebookAds\Api;
use FacebookAds\Cursor;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Post;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\User;
use FacebookAds\Http\Exception\ServerException;
use Psr\Http\Message\ServerRequestInterface;
Cursor::setDefaultUseImplicitFetch(false);

//function index(ServerRequestInterface $request){
function index()
{

    global $dbconn;
   
    if (isset($request)){
        $body = $request->getBody()->getContents();
        $dataRequest = json_decode($body, true);

        if(!isset($dataRequest['action'])){
            echo 'No action defined in request';
            die();
        }

        if(!isset($dataRequest['auth_id'])){
            echo 'No AUTH ID defined in request';
            die();
        }

    }

    // conformamos el request
    $data = array(
        
        'auth_id'=>isset($dataRequest['auth_id']) ? $dataRequest['auth_id']: '2ccd6be6-d929-11eb-8d81-ac1f6b17ff4a',
        

        /*  // atomo
        'action'=> isset($dataRequest['action']) ? $dataRequest['action']: 'campaign_platform_atomo_update',
        'parent_platform_id' => isset($dataRequest['parent_platform_id']) ? $dataRequest['parent_platform_id'] : '23847762373380502', //23847762373380502
        'fields' => isset($dataRequest['fields']) ? $dataRequest['fields'] : array(  'name' => 'new_adset_demo', 'budget' => '15', 'special_ad_categories' => '[]' ),  
         */
        // creas
        'action'=> isset($dataRequest['action']) ? $dataRequest['action']: 'creatividad_update',
        'parent_platform_id' => isset($dataRequest['parent_platform_id']) ? $dataRequest['parent_platform_id'] : '23847762374010502', //23847762373380502
        'fields' => isset($dataRequest['fields']) ? $dataRequest['fields'] : array(  'name' => 'MEMO OCHOA TEST UPDATE', 'special_ad_categories' => '[]' ),  

    );
    
    $query = "SELECT AUP.id, AUP.auths_user_id, AUP.user_id, AUP.platform, AUP.app_id, AUP.app_secret, AUP.platform_user_id,  AUP.access_token, AUP.refresh_token, U.name, U.email, U.customer_id_default, U.campaign_root_default  FROM auths_user_platform AUP JOIN users U ON (AUP.user_id = U.id)  WHERE AUP.public_id = '" . $data["auth_id"] . "';";
    $result = $dbconn->query($query);
    $infoCredentials = $result->fetch_assoc();

    if (isset($infoCredentials['user_id'])){
        $user_id = $infoCredentials['user_id'];
    }else{
        echo 'No AUTH ID found';
        die();  
    }

    $api = Api::init($infoCredentials['app_id'], $infoCredentials['app_secret'], $infoCredentials['access_token']);

    if ($data['action']== null){
        echo 'NO ACTION SPECIFIED';
        die();
    }

    try {
    
        $fb = new \Facebook\Facebook(
                                        [ 'app_id' => $infoCredentials['app_id'], 
                                        'app_secret' => $infoCredentials['app_secret'], 
                                        'default_graph_version' => 'v10.0'] 
                                    );
            
          try {
              $response = $fb->get('/me', $infoCredentials["access_token"]);
          } catch(\Facebook\Exceptions\FacebookResponseException $e) {
              echo 'Graph returned an error: ' . $e->getMessage() . PHP_EOL;
              echo 'EXPIRADO' . PHP_EOL;
             /*  $query = "UPDATE auths_user_platform set auths_user_platform.activa = 'EXPIRATE' WHERE auths_user_platform.id = '" . $infoCredentials['id'] . "'";
              $dbconn->query($query); */
              echo 'EVENT MAIL NOTIFY USER TOKEN IS EXPIRATED' . PHP_EOL;
              exit;
  
          } catch(\Facebook\Exceptions\FacebookSDKException $e) {
              echo 'Facebook SDK returned an error: ' . $e->getMessage();
              exit;
          }
  
          if (isset($response)) {
              $me = $response->getGraphUser();
              echo 'Logged in as ' . $me->getName() . PHP_EOL . PHP_EOL;
          } 
  
      }catch (Exception $e){
          echo 'Exeption as ' . $e->getMessage() . PHP_EOL;
          exit;
      }



    switch ($data['action']) {

        case "campaign_platform_update":        
            campaignplatform_update($fb, $data, $infoCredentials);
            break;


        case "campaign_platform_atomo_update":
            campaign_platform_atomo_update($fb, $data, $infoCredentials);
            break;


        case "creatividad_update":
            creatividad_update($fb, $data, $infoCredentials);
            break;

            
        default:
            echo "NO ACTION FOUND";

    }

}

/*** InfoCredentials
 * 
    ["id"] => string(3) "110"
    ["auths_user_id"] => string(32) "------"
    ["user_id"] => string(1) "6"
    ["platform"] => string(8) "FACEBOOK"
    ["app_id"] => string(16) "2230398120518804"
    ["app_secret"] => string(32) "----"
    ["platform_user_id"] => string(17) "10215767923927052"
    ["access_token"] => string(188) "EAAfsiQeuZAJQBAA8KBYnjiEenZBycv4vqh9z7KvZBQjdr8ZANLZBGZC9mk9e8dl8ssiNU9c6DdZAnwL4sTUlt7qNF3n5LeUY7YeOnb07zxkT2Hla5yJBswePGCs7ZCgJgOWZCz4fEoLZAhWFHGnlUybwFwWkw3TAf9kNfY1lZB58x3K4fv2Ac51qJhL"
    ["refresh_token"] => NULL
    ["name"] => string(2) "JR"
    ["email"] => string(19) "admin@turecarga.com"
    ["customer_id_default"] =>  string(2) "32"
    ["campaign_root_default"] => string(1) "1"
  *
  
*/

index();

?>