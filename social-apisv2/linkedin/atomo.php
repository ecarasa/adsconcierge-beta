<?php
function get_linkedin_atomos($infoCredentials, $data)
{
    global $dbconn_stats, $dbconn;


    $ch = curl_init();
    //GET https://api.linkedin.com/v2/adCreativesV2?q=search&search.{searchCriteria}.values[0]={searchValue}

    curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/adCreativesV2?q=search');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 0);

    $headers = array();
    $headers[] = 'Authorization: Bearer ' . $infoCredentials['access_token'];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    $result = json_decode($result);
    $data = [];

    //var_dump($result);die();

    foreach ($result->elements as $item) {
        $data[] = (array) $item;
    }
}

function atomo_stats_get_linkedin($infoCredentials, $data)
{
    foreach ($data['parent_platform_ids'] as $parent) {
        $datos = linkedin_stats_atomo_xdias($infoCredentials['app_id'], $infoCredentials['access_token'], $infoCredentials['user_id'], $parent['id'], $args['start_date'], $args['end_date']) ;
        helper_metrics_lineitem_day($infoCredentials['platform'], $infoCredentials['user_id'], $infoCredentials['customer_id'], $parent['id'], $datos, null);
    }
}

?>