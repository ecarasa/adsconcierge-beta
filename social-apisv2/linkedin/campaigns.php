<?php
function campaigns_get_linkedin($infoCredentials, $data)
{
    global $dbconn_stats, $dbconn;

    /*** Conditions
     * Advertising accounts have a hard limit of 5,000 campaigns, regardless of campaign status.
     * Advertising accounts are limited to a maximum of 1,000 concurrent campaigns in ACTIVE status at any given time.
     * A campaign can have a maximum of 15 active creatives and 85 inactive creatives.
     * A campaign can only have creatives matching the ad format selected at time of creation.
     * If a campaign has no ad format set, it will be set by the first creative created under that campaign. Dynamic, carousel, and video ad campaigns must have their format set upon creation.
     * A campaign is considered active until it reaches its end time or gets deleted.
     * Paused campaigns are considered active until their designated end times.
     * **/

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/adCampaignGroupsV2?q=search');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 0);
    $headers = array();
    $headers[] = 'Authorization: Bearer ' . $infoCredentials['access_token'];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    $result = json_decode($result);
    $campanas = [];

    foreach ($result->elements as $item) {
        $campanas['urn:li:sponsoredCampaignGroup:' . $item->id] = (array) $item;
    }

    $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (user_id, platform, app_id, auth_id, name, id_en_platform, budget, 
                                        status, currency, metadata, ad_account, campana_root, customer_id, source)
                                        values (?,?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts 
                                        WHERE ads_accounts.account_id = ? and ads_accounts.user_id = ? ), ?, ?, 'IMPORTED')
                                        on duplicate key update status = ?, metadata = ? ");

    foreach ($campanas as $item) {

        $item = (object) $item;
        $accountbb = str_replace('urn:li:sponsoredAccount:', '', $item->account);
        $accountAd = getAdAccount($accountbb);

        //var_dump($item->account);die();
        echo '    campaign ' . $item->id . ' - '. $item->name . PHP_EOL;

        $stmtcampana->bind_param("issssssssssissss", ...[   $infoCredentials['user_id'],
            $infoCredentials['platform'],
            $infoCredentials['app_id'],
            $infoCredentials['id'],
            $item->name,
            $item->id,
            null,
            unificastatus('campana', $infoCredentials['platform'], $item->status, ['estado' => $item->status, 'otros' => $item->servingStatuses[0], 'record' => $item]),
            $accountAd['currency'],
            json_encode($item),
            $accountbb,
            $infoCredentials['user_id'],
            $infoCredentials["campaign_root_default"],
            $infoCredentials["customer_id_default"],
            unificastatus('campana',
                $infoCredentials['platform'],
                $item->status, ['estado' => $item->status, 'otros' => $item->servingStatuses[0], 'record' => $item]), json_encode($item)]);

        $stmtcampana->execute();

        if ($stmtcampana->error != "") {
            print_r("Error: %s.\n", $stmtcampana->error);
        }
    }
}

//estadisticas
function campaigns_stats_get_linkedin($infoCredentials, $data)
{

    foreach ($data['parent_platform_ids'] as $parent) {

        $datos = linkedin_stats_campana_xdias(
            $infoCredentials['app_id'],
            $infoCredentials['access_token'],
            $infoCredentials['user_id'],
            $parent['id'],
            $data['start_date'],
            $data['end_date']
        );

        helper_metrics_campana_day(
            $infoCredentials['platform'],
            $infoCredentials['user_id'],
            $infoCredentials['customer_id_default'],
            $datos,
            $infoCredentials['id'],
            $parent['id'],
            $infoCredentials,
            null
        );
    }
}

function campaignplatform_create($app_id, $app_secret,$access_token,$userid, $add_account_id,$rowdata)
{

}

function campaignplatform_delete($fb, $data, $infoCredentials)
{

}

?>