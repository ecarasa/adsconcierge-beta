<?php
function get_accounts($infoCredentials){

    global $dbconn_stats, $dbconn;

    // Advertising accounts can have a maximum of 5,000 campaigns and 15,000 creatives.

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/adAccountsV2?q=search'); //&search.status.values[0]=ACTIVE
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 0);

    $headers = array();
    $headers[] = 'Authorization: Bearer ' . $infoCredentials['access_token'];

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    $result = json_decode($result);

    if(!isset($result->elements)){
        echo 'error getadsccount '.  print_r($result, true);
        //logger
        die();
    }


    $properties = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` (`user_id`, `platform`, `adaccount_id`, `app_id`,`platform_user_id`, `id_en_platform`, `token`,`name`,`status`, `category`, `currency`, `metadata`, `auth_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `name`= ?, `adaccount_id`= ? , `status`= ?, `platform_user_id`=?,  `metadata`= ? ");
    $cuentasarray = [];

    foreach ($result->elements as $AdAccountLinkeding){

        echo $AdAccountLinkeding->id . ' .. '.$AdAccountLinkeding->name . PHP_EOL;
        persistAccount($AdAccountLinkeding, $infoCredentials);

        $pagina = linkedin_retrieve_organization(null, $infoCredentials['access_token'], $infoCredentials['user_id'], array('organizationlookup'=> $AdAccountLinkeding->reference  ) );

        $adaccount_name= $AdAccountLinkeding->name;
        $ad_accountid= $AdAccountLinkeding->id;

        $cuentasarray[$AdAccountLinkeding->id] = $AdAccountLinkeding;


        $query = "SELECT id as adacc_id, public_id as adaccount_public_id FROM `app_thesoci_9c37`.`ads_accounts` WHERE auth_id = '{$infoCredentials["id"]}' and platform = '{$infoCredentials["platform"]}' and platform_user_id= '{$infoCredentials['platform_user_id']}' and account_id = '{$ad_accountid}' limit 1    ";
        $res_ac = $dbconn->query($query);
        $id_ac = $res_ac->fetch_assoc();


        $pagename = isset($pagina->localizedName) ? $pagina->localizedName : $AdAccountLinkeding->name;
        $AdAccountLinkeding->pagedata = $pagina;
        $page_id = $AdAccountLinkeding->reference;

        $properties->bind_param("ssssssssssssssssss", ...[
            $infoCredentials['user_id'],
            $infoCredentials['platform'],
            $id_ac["adacc_id"],
            $infoCredentials["app_id"],
            $infoCredentials['platform_user_id'],
            $page_id ,
            '-',
            $pagename,
            $AdAccountLinkeding->status . '>' . $AdAccountLinkeding->servingStatuses[0],
            $AdAccountLinkeding->type,
            $AdAccountLinkeding->currency,
            json_encode($AdAccountLinkeding),
            $infoCredentials['id'],
            $pagename,
            $id_ac["adacc_id"],
            $AdAccountLinkeding->status . '>' . $AdAccountLinkeding->servingStatuses[0],
            $infoCredentials['platform_user_id'],
            json_encode($AdAccountLinkeding)]);
        $properties->execute();

        if ($properties->error != "") {
            print_r("Error: %s.\n", $properties->error);
        }

        // properties_relations
        $query = "SELECT id as pageid, public_id as property_public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE  auth_id = '{$infoCredentials["id"]}' and platform = '{$infoCredentials["platform"]}' and platform_user_id= '{$infoCredentials['platform_user_id']}' and id_en_platform = '{$page_id}'";
        $res_pa = $dbconn->query($query);
        $id_pa = $res_pa->fetch_assoc();

        /*** Inserto registros en tabla de relaciones Properties y Ads Accounts
        $query = "SELECT auths_user_id FROM `app_thesoci_9c37`.`auths_user_platform` WHERE id = " .$auths["id"];
        $res_aup = $dbconn->query($query);
        $id_aup = $res_aup->fetch_assoc();
        $id_aup = isset($id_aup["auths_user_id"]) ? $id_aup["auths_user_id"] : "";
        $query = "SELECT public_id FROM `app_thesoci_9c37`.`ads_accounts` WHERE account_id LIKE '" .$item->id ."'";
        $res_ac = $dbconn->query($query);
        $id_ac = $res_ac->fetch_assoc();
        $id_ac = isset($id_ac["public_id"]) ? $id_ac["public_id"] : "";
        $query = "SELECT public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE id_en_platform LIKE '" .$item->id ."'";
        $res_pa = $dbconn->query($query);
        $id_pa = $res_pa->fetch_assoc();
        $id_pa = isset($id_pa["public_id"]) ? $id_pa["public_id"] : "";
        $query = "SELECT public_id FROM `app_thesoci_9c37`.`users` WHERE id = " .$auths["user_id"];
        $res_u = $dbconn->query($query);
        $id_u = $res_u->fetch_assoc();
        $id_u = isset($id_u["public_id"]) ? $id_u["public_id"] : "";
        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` (`auth_id`, `auth_publicid`, `ad_account`, `ad_account_publicid`, `ad_account_name`, `property_id`, `property_publicid`, `property_name`, `user_id`, `user_publicid`) VALUES (?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");
        $stmt3->bind_param("isssssssisss", $auths["id"], $id_aup, $item->id, $id_ac, $item->name, $item->id, $id_pa, $item->name, $auths["user_id"], $id_u, $item->name, $item->name);
        $stmt3->execute();
         **/

        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` 
                                    (`type`,`auth_id`, `ad_account`,  `ad_account_name`,`property_name`, `property_id`, `user_id`,`platform`,`auth_publicid`,   `user_publicid`,  `ad_account_publicid`,`property_publicid`) 
                                        VALUES ('PAGE', ?,?,?,?,?,?,?,?,?,?,?)
                                            ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");

        $stmt3->bind_param("sssssssssssss",
            $infoCredentials["id"],
            $id_ac["adacc_id"] ,
            $adaccount_name,
            $pagename,
            $id_pa["pageid"],
            $infoCredentials['user_id'],
            $infoCredentials["platform"],
            $infoCredentials["auths_user_id"],
            $infoCredentials["user_public_id"],
            $id_ac["adaccount_public_id"],
            $id_pa["property_public_id"],
            $adaccount_name,
            $pagename);

        $stmt3->execute();
        if ($stmt3->error != "") {
            print_r("Error: %s.\n", $subadsaccount->error);
        }



    }

    return $cuentasarray;

}

function linkedin_retrieve_organization($appid, $access_token, $userid, $rowdata)
{
    // Advertising accounts can have a maximum of 5,000 campaigns and 15,000 creatives.
    $organizationid= str_replace('urn:li:organization:',  '', $rowdata['organizationlookup']);
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/organizations/'.$organizationid); //&search.status.values[0]=ACTIVE
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 0);

    $headers = array();
    $headers[] = 'Authorization: Bearer ' . $access_token;

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    $result = json_decode($result);

    if(!isset($result->vanityName)){
        echo 'error linkedin_retrieve_organization '.  print_r($result, true);
        return false;
    } else {
        return $result ;
    }

}