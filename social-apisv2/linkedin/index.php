<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'vendor/autoload.php';
include 'configs/linkedin.php';
include 'configs/general.php';
include 'configs/stats_mapeometrics.php';

include 'common/basics.php';
include 'common/extLogger.php';
include 'get_stats.php';
include 'get_properties.php';
include 'campaigns.php';
include 'atomo.php';
include 'creativities.php';
include 'helpers/metrics.php';

use Psr\Http\Message\ServerRequestInterface;

/* 
    - LINKEDIN ADS API - 
    POST 
    Request
    [action, auth_id, parent_platform_ids, platform_id, period ,star_date, end_date] 
*/
function retrieve_all($data, $infoCredentials){

    echo 'Retrieve all entities for ' . $infoCredentials['name'] . ' ' . $infoCredentials['email'] . PHP_EOL;

    $accountsArray = get_accounts($data, $infoCredentials);

    foreach ($accountsArray as $adAccount){
        // buscamos properties
        $event = array(  'auth_id' => $infoCredentials['id'], 'type' => 'get_properties', 'function' => 'function-facebook-api', 'subject_id' =>  $adAccount['id'], 'callchild'=> [ array('type' => 'get_campaigns')     ] );
        //var_dump($event);
        execGoogleTask($event, $data);
        //esto se llamada desde properties
        // buscamos campaigns

        $event = array(  'auth_id' => $infoCredentials['id'], 'type' => 'get_campaigns', 'function' => 'function-facebook-api',  'subject_id' =>  $adAccount['id'] );
        //var_dump($event);
        //   execGoogleTask($event);
    }
}

function retrieve_all_ads_accounts_stats($data, $infoCredentials, $user_id, $accountsArray = null){

    if ($accountsArray==null) {
        $accountsArray = get_accounts($data, $infoCredentials);
    }

    get_stats_campaigns($accountsArray, $data, $infoCredentials);
}

function refresh_token($infoCredentials)
{

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://www.linkedin.com/oauth/v2/accessToken');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=refresh_token&refresh_token=" . $infoCredentials['refresh_token'] . "&client_id=" . $infoCredentials['app_id'] . "&client_secret=" . $infoCredentials['app_secret']);

    $result = curl_exec($ch);
    $result = json_decode($result);
    print_r($result);
    die();
    return $result['access_token'];
}

function check_token_isalive()
{

}

// entidades

function index(){
//function index(ServerRequestInterface $request){

    global $dbconn;
    $dataRequest=[];
    $array_methods = [
        'retrieve_all',
        'get_accounts',
        'campaigns_get_linkedin',
        'get_linkedin_atomos',
        'get_linkedin_creas',
        'properties_linkedin',
        'campaigns_stats_get_linkedin',
        'get_stats_campaigns',
        'get_stats_adSet',
        'get_stats_ad',
        'retrieve_stats_all',
    ];

        //   {"auth_id":"fd891ff8-0138-11ec-8d81-ac1f6b17ff4a","action":"get_properties","ad_account_id":"25533038","callchild":[]}
    $dataRequest=[ 'action' => 'retrieve_all', 'auth_id'=> "fd891ff8-0138-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
 //     $dataRequest=[ 'action' => 'get_properties', 'auth_id'=> "fd891ff8-0138-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '4425702020786837',"callchild"=>[] ];
    //  $dataRequest=[ 'action' => 'retrieve_stats_all', 'auth_id'=> "fd891ff8-0138-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '631856017672306',"callchild"=>[] ];
    //  $dataRequest=[ 'action' => 'get_stats_campaigns', 'auth_id'=> "fd891ff8-0138-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '631856017672306',"callchild"=>[] ];
  
  
    if (isset($request)){
        $body = $request->getBody()->getContents();
        $dataRequest = json_decode($body, true);

        if(!isset($dataRequest['action'])){ echo 'No action defined in request'; die(); }
    
        if(!isset($dataRequest['auth_id'])){
            echo 'No AUTH ID defined in request';
            die();
        }

    }

    // conformamos el request

    $data = array(
        'action'=> isset($dataRequest['action']) ? $dataRequest['action']: 'get_linkedin_creas',
        'auth_id'=>isset($dataRequest['auth_id']) ? $dataRequest['auth_id']: '2ccd38ce-d929-11eb-8d81-ac1f6b17ff4a',
        'ad_account_id' => isset($dataRequest['ad_account_id']) ? $dataRequest['ad_account_id'] : '508170419',
        'platform_id'=> isset($dataRequest['platform_id']) ? $dataRequest['platform_id']: null,
        'period'=> isset($dataRequest['period']) ? $dataRequest['period'] : null, //mandatory para stats
        'start_date'=> isset($dataRequest['start_date']) ? $dataRequest['start_date'] : null, //opcional
        'end_date'=> isset($dataRequest['end_date']) ? $dataRequest['end_date'] : null //opcional
    );
    
    // {"action" : "get_accounts",  "auth_id": "2ccd2f2f-d929-11eb-8d81-ac1f6b17ff4a"}

    $query = "SELECT AUP.id, AUP.auths_user_id, AUP.user_id, AUP.platform, AUP.retorno, AUP.app_id, AUP.app_secret, AUP.platform_user_id,  AUP.access_token, AUP.refresh_token, U.name, U.email, U.customer_id_default, U.campaign_root_default  FROM auths_user_platform AUP JOIN users U ON (AUP.user_id = U.id)  WHERE AUP.public_id = '" . $data["auth_id"] . "';";
    $result = $dbconn->query($query);
    $infoCredentials = $result->fetch_assoc();

    //var_dump($infoCredentials);die();

    if (isset($infoCredentials['user_id'])){
        $user_id = $infoCredentials['user_id'];
    }else{
        echo 'No AUTH ID found';
        die();  
    }

    //$infoCredentials['access_token'] = refresh_token($infoCredentials);



    // to do - validate token
    // to do - refresh token 
    // if token largo = caduco
    //$query = "UPDATE auths_user_platform set auths_user_platform.activa = 'EXPIRATE' WHERE auths_user_platform.ID = '" . $infoCredentials['id'] . "'";
    //$dbconn->query($query);
    //redirect(/route/desactivar_token_por_Caducidad)

    if ($data['action']== null){
        echo 'NO ACTION SPECIFIED';
        die();
    }

    switch ($data['action']) {

        case "retrieve_all":        
            retrieve_all($data, $infoCredentials, $user_id);
            break;

                case "retrieve_stats_all":
            
            $query = "SELECT account_id FROM app_thesoci_9c37.ads_accounts where platform='LINKEDIN' AND  auth_id in (select id from auths_user_platform where public_id = '".$data["auth_id"]."');";
            $result = $dbconn->query($query);
            
            while ($fila = $result->fetch_assoc()) {
                $event = array(  'auth_id' => $infoCredentials['id'], 'type' => 'get_stats_campaigns', 'function' => 'function-facebook-api',  'subject_id' =>  $fila['account_id'], 'period'=> '48H', 'nextcalls'=>['get_account_stats_adSet'=>['get_account_stats_ad'] ]);
                execGoogleTask($event);
               /* $event = array(  'auth_id' => $infoCredentials['id'], 'type' => 'get_stats_adSet', 'function' => 'function-facebook-api',  'subject_id' =>  $fila['account_id'] );
                execGoogleTask($event);
                $event = array(  'auth_id' => $infoCredentials['id'], 'type' => 'get_stats_ad', 'function' => 'function-facebook-api',  'subject_id' =>  $fila['account_id'] );
                execGoogleTask($event);
                */
            }
        
        
        case "get_accounts":
            get_accounts($infoCredentials);
            break;


        case "campaigns_get_linkedin":
            campaigns_get_linkedin($infoCredentials, $data);
            break;


        case "get_linkedin_atomos":
            get_linkedin_atomos($infoCredentials, $data);
            break;

        case "get_linkedin_creas":
            get_linkedin_creas($infoCredentials, $data);
            break;


        case "properties_linkedin":
            properties_linkedin($infoCredentials, $data);
            break;


        // STATS
        case "campaigns_stats_get_linkedin":
            campaigns_stats_get_linkedin($infoCredentials, $data);
            break;

        // STATS
        case "get_stats_campaigns":
            get_stats_campaigns($data['ad_account_id'], $data , $infoCredentials );
            break;

        case "get_stats_adSet":
            get_stats_adSet($data['ad_account_id'], $data , $infoCredentials );
            break;

        case "get_stats_ad":
            get_stats_ad($data['ad_account_id'], $data , $infoCredentials );
            break;

        case "retrieve_stats_all":

            $query = "SELECT account_id FROM app_thesoci_9c37.ads_accounts where platform='LINKEDIN' and status='1|0' AND  auth_id in (select id from auths_user_platform where public_id = '".$data["auth_id"]."');";
            $result = $dbconn->query($query);

            while ($fila = $result->fetch_assoc()) {
                $event = array(  'auth_id' => $infoCredentials['id'], 'type' => 'get_stats_campaigns', 'function' => 'function-facebook-api',  'subject_id' =>  $fila['account_id'], 'period'=> '48H', 'nextcalls'=>['get_account_stats_adSet'=>['get_account_stats_ad'] ]);
                execGoogleTask($event);

                /* $event = array(  'auth_id' => $infoCredentials['id'], 'type' => 'get_stats_adSet', 'function' => 'function-facebook-api',  'subject_id' =>  $fila['account_id'] );
                 execGoogleTask($event);
                 $event = array(  'auth_id' => $infoCredentials['id'], 'type' => 'get_stats_ad', 'function' => 'function-facebook-api',  'subject_id' =>  $fila['account_id'] );
                 execGoogleTask($event);
                 */
            }
            break;

        default:
            echo "NO ACTION";

    }

}



/*** 
 * 
 **** INFO Credentials --->

 
*/

index();

?>