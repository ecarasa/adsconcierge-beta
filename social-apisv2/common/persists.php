<?php

include_once 'basics.php';


mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
function persistAccount($user_id, $accountItem, $infoCredentials){
	return persistAdAccount($user_id, $accountItem, $infoCredentials);
}
function persistAdAccount($user_id, $accountItem, $infoCredentials){

    global $dbconn;

    if (isset($accountItem['account_status']) && isset($accountItem['disable_reason'])){
        $status = $accountItem['account_status'] . '|' . $accountItem['disable_reason'];
    }else{
        $status = 'NO_STATUS_RETRIEVE';
    }
		$status= $accountItem['status'];
    $metadata = json_encode($accountItem, true);
//TO-DO
//UNIFICA STATUS
    $stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`ads_accounts` (`version`,`user_id`, `platform`, `app_id`, `account_id`, `name`, `platform_user_id`, `status`, `currency`, `metadata`, `customer_id`, `auth_id`) VALUES
		(?, ?,?,?,?,?,?,?,?,?,?,?) 	ON DUPLICATE KEY UPDATE `name`= ?,  `status`= ?, `platform_user_id`=?,  `metadata`= ? ");
 
	
    $stmt->bind_param("ssssssssssssssss",...[VERSIONCODIGO ,$user_id,
        $infoCredentials["platform"],
        $infoCredentials["app_id"],
        $accountItem['account_id'],
        $accountItem['name'],
        $infoCredentials['platform_user_id'],
        $status,
        $accountItem['currency'],
        $metadata,
        $infoCredentials["customer_id_default"],
        $infoCredentials["id"],
        $accountItem['name'],
        $status,
        $infoCredentials['platform_user_id'],
        $metadata]);
    try {
        $stmt->execute();
        return true;
    } catch (Exception $e) {
        print_r($e);
    }
}
function persistPixel ( $itemData, $infoCredentials , $adaccount_platform_id){
	return persistProperties( $itemData, $infoCredentials,  $adaccount_platform_id,  'PIXEL');
}


function persistProperties( $itemData, $infoCredentials, $adaccount_platform_id=null, $type='PAGE'){
	  global $dbconn;

				$adPixelData['status'] = 0; //todo: fixed in origin code
        $itemData['category'] = isset($itemData['category'])? $itemData['category']  :  '-';
        $adPixelData['token'] = '-';
        $adPixelData['type'] = $type;
   
	
 $stmt2 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` 
                                    ( `version`,`type`, `user_id`, `platform`, `app_id`, `platform_user_id`, 
                                    `id_en_platform`, `token`,`name`,`status`,`category`, `metadata`, 
                                    `adaccount_id`, `auth_id` , `currency` )
                                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
                                    ON DUPLICATE KEY UPDATE  `name` = ?, `status` = ?, `platform_user_id` = ?, `metadata`= ? ");

        $stmt2->bind_param("Sssssssssssssssssss",
            ...[VERSIONCODIGO,
                $type,
                $infoCredentials["user_id"],
                $infoCredentials["platform"],
                $infoCredentials["app_id"],
                $infoCredentials['platform_user_id'],
                $itemData['id_en_platform'],
                $infoCredentials['access_token'],
              	$itemData['name'],
                $adPixelData['status'],
                $itemData['category'],
                json_encode($itemData,true),
               	$adaccount_platform_id,
                $infoCredentials["id"],
                isset($itemData["currency"]) ? $itemData["currency"] : NULL,
                $itemData['name'],
                $adPixelData['status'],
                $infoCredentials['platform_user_id'],
                json_encode($itemData,true) ]);
      		  $stmt2->execute();
	 if ($stmt2->error != "") {
            printf("Error: %s.\n", $stmt2->error);
		 extLogger(array( 'level'=> 'Error', 'category'=>'mysqlError', 'message'=> $stmt2->error  , 'infoCredentials' => json_encode(  $infoCredentials ) ) );
		 
        }
        echo ' Property - '. $type .'- added -> ' . $itemData['name']. PHP_EOL;
 
 
	        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` 
				(`type`,`platform`,`auth_id`, `auth_publicid`,`user_id`,`user_publicid`, `ad_account`, `ad_account_name`,`ad_account_publicid`,`property_name`,`property_id`, `property_publicid`) 
				(SELECT ?,?,?,?, ?, ? ,account_id, name, public_id, ?,?, (select public_id from `app_thesoci_9c37`.`properties_accounts` where id_en_platform=? and auth_id=? limit 1) as property_public_id from `app_thesoci_9c37`.`ads_accounts` where account_id=? and `auth_id`= ? limit 1) 
				ON DUPLICATE KEY UPDATE  `property_name` = ?");

                                    // ON DUPLICATE KEY UPDATE `ad_account_name` = name, `property_name` = ?");

       // $adc_publicid= getAdAccount_PublicID($adAccountData['id']);

        $stmt3->bind_param("sssssssssssss",
            ...[$type, $infoCredentials["platform"],$infoCredentials["id"],$infoCredentials["auth_public_id"], $infoCredentials["user_id"],$infoCredentials["user_public_id"],$itemData['name'], 
								$itemData['id_en_platform'], $itemData['id_en_platform'],$infoCredentials["id"],$adaccount_platform_id,$infoCredentials["id"]	, $itemData['name']
							 ]);
 
 
       $stmt3->execute();
 	 if ($stmt3->error != "") {
           // printf("Error: %s.\n", $stmt3->error);

          extLogger(array( 'level'=> 'Error', 'category'=>'mysqlError','message'=> $stmt3->error  , 'infoCredentials' => json_encode(  $infoCredentials ) ) );
        }
	
}



function persistCampaign( $itemData, $infoCredentials,  $account=[ ] ,$platform=null ){
	  global $dbconn;

				$adPixelData['status'] = 0; //todo: fixed in origin code
        $adPixelData['category'] = '-';
        $adPixelData['token'] = '-';
        $adPixelData['type'] = 'PIXEL';
	
 $stmt2 =  $dbconn->prepare("INSERT INTO campaigns_platform (version,user_id, platform, app_id, auth_id, name, id_en_platform, status, activa, currency, metadata, ad_account, campana_root, customer_id, source, start,end) 
 values (?,?,?,?,?,?,?,?,?,?,?,?, ?,?, 'IMPORTED', ?, ? ) 
 on duplicate key update status = ?, metadata = ?, campana_root = ?, customer_id = ?, activa= ? ");

 $status= unificastatus('campana', $infoCredentials['platform'], $itemData['status'], ['estado' => $itemData['status'],  'otros' => $itemData['effective_status'], 'record' => $itemData]);
 $isactive=unifyActive('campana', $infoCredentials['platform'], $itemData['status'] );
	
	        $stmt2->bind_param("sssssssssssssssssssss", ...[ VERSIONCODIGO,
						$infoCredentials['user_id'],
            $infoCredentials['platform'],
            $infoCredentials['app_id'],
            $infoCredentials['id'],
            $itemData['name'],
            $itemData['id'],
            $status,
						$isactive,
            isset($account['currency']) ? $account['currency'] : 'USD',
            json_encode($itemData),
            $account['id'],
          //  $infoCredentials['user_id'],
            isset($infoCredentials["campaign_root_default"]) ? $infoCredentials["campaign_root_default"] : '0',
            $infoCredentials["customer_id_default"],
						$itemData['start_time'],
						 $itemData['stop_time'],
            $status,
            json_encode($itemData),
            isset($infoCredentials["campaign_root_default"]) ? $infoCredentials["campaign_root_default"] : '0',
            $infoCredentials["customer_id_default"],
						$isactive] );
	
        $stmt2->execute();
	 if ($stmt2->error != "") {
       //     printf("Error: %s.\n", $stmt2->error);
		     $loggermessage = array( 'level'=> 'Error', 'category'=>'mysqlError', 'message'=> $stmt2->error  , 'infoCredentials' => json_encode(  $infoCredentials ) );
          extLogger($loggermessage );
        }
}


function persistAdset( $itemData, $infoCredentials,   $account=[ ],$platform=null){
	  global $dbconn;
	
	$base_itemData=['bid_unit'=>null, 'bid_type'=>null, 'product_type'=>null, 'targeting'=>[ 'publisher_platforms'=>[], 'genders'=>[]  , 'device'=>[]  , 'device_platforms'=>[] , 'geo_locations'=> [] ], 'bid_strategy'=> null , 'chargeby'=> null, 'property_platform_id'=> null ,'currency'=>$account['currency'], 'objective'=>null];
	$property_page_id=0;
$itemData=array_replace_recursive($base_itemData,  $itemData  );
	if (!isset($itemData['targeting_age'])) {		$itemData['targeting_age']=['age_min'=>18,'age_max'=>65];   }
	//if (!isset($itemData['targeting']['genders'])) {		$itemData['targeting']['genders']=[]; }
	switch($platform){
		case 'FACEBOOK':
			if (isset($itemData['targeting']['age_min'] )) {$itemData['targeting_age']['age_min']= $itemData['targeting']['age_min'];  }
			if (isset($itemData['targeting']['age_max'] )) {$itemData['targeting_age']['age_max']= $itemData['targeting']['age_max'];  }
			if (isset($itemData['billing_event']  )) {$itemData['chargeby']  = $itemData['billing_event']; } 
				$property_page_id = isset($itemData['promoted_object']['page_id'] ) ?  $itemData['promoted_object']['page_id'] : null;
			break;
			
			
	}
	
		/*		$adPixelData['status'] = 0; //todo: fixed in origin code
        $adPixelData['category'] = '-';
        $adPixelData['token'] = '-';
        $adPixelData['type'] = 'PIXEL'; */
	
	/*	SET @customer_id :=  ? ;	SET @campana_root_id := ? ; 
	SELECT @customer_id:=`customer_id`, @campana_root_id:=`campana_root`, @campanaplatform_id:=`id`  from campaigns_platform where id_en_platform =? and user_id= ? and platform = ? limit 1;
	*/
	$query = " 	INSERT INTO campaigns_platform_atomo (version,
	user_id,platform, app_id, auth_id, ad_account, 
	id_en_platform,campana_platform_id,adaccount_platform_id,property_platform_id,
	name, status,	budget, budget_total,platform_properties_id,
	metadata,source,
	 gender,geo_include,platform_placements, start, end,
	 optimizacion, age , device, bid_type,chargeby, currency,objetivo,
	 puja_valor, product_type,
	 customer_id, 	
	 campana_root_id, 
	 campanaplatform_id
	 )

	values (?,?,?,?,?,?  ,?,?,?,?,  ?,?,?,?,?,  ?,'IMPORTED', ?,?,?,?,?, ?,?,?,?,?, ?, ?, ?,?,(Select customer_id  from campaigns_platform where id_en_platform =? and user_id= ? and platform = ? limit 1), 
	(Select campana_root  from campaigns_platform where id_en_platform =? and user_id= ? and platform = ? limit 1),  (Select id  from campaigns_platform where id_en_platform =? and user_id= ? and platform = ? limit 1)   ) 
    on duplicate key update  status = ?,  metadata = ?, budget = ?, budget_total= ?, start= ?, end= ? "; //31 ?
	//, 	
	// ,  @customer_id ,@campana_root_id,@campanaplatform_id
	
  $stmt2 = $dbconn->prepare($query);
	 //campana_root_id,customer_id
	
	
	$status=unificastatus('adset', $infoCredentials['platform'], $itemData['status'], ['estado' => $itemData['status'],  'otros' => $itemData['effective_status'], 'record' => $itemData]);
	//print_r($account );

           
    		$stmt2->bind_param("sssssssssssssssssssssssssssssssssssssssssssss", ...[VERSIONCODIGO,		
							 $infoCredentials['user_id'],  $infoCredentials['platform'], $infoCredentials['app_id'],   $infoCredentials['id'], $account['id'], 
							 $itemData['id'],   $itemData['campaign_id'],  $account['account_platform_id'],  $property_page_id,  
						  $itemData['name'], $status,	$itemData['daily_budget'],  $itemData['lifetime_budget'],  json_encode($itemData['promoted_object']), 
json_encode($itemData),
json_encode($itemData['targeting']['genders']),json_encode($itemData['targeting']['geo_locations']),json_encode($itemData['targeting']['publisher_platforms']),$itemData['start_time' ],$itemData['end_time' ] ,
							$itemData['optimization_goal' ] , json_encode($itemData['targeting_age'] ),	json_encode($itemData['targeting']['device']),$itemData['bid_strategy'],$itemData['chargeby'],$itemData['currency'],$itemData['objective'],		
					$itemData['puja_valor'], $itemData['product_type'],
						 $itemData['campaign_id'],	 $infoCredentials['user_id'],  $infoCredentials['platform'],
						 $itemData['campaign_id'],	 $infoCredentials['user_id'],  $infoCredentials['platform'],
						 $itemData['campaign_id'],	 $infoCredentials['user_id'],  $infoCredentials['platform'],
							$status, json_encode($itemData),$itemData['daily_budget'],  $itemData['lifetime_budget'], $itemData['start_time' ],$itemData['end_time' ]  	
                                                            ]);
 
        $stmt2->execute();

	 if ($stmt2->error != "") {
  //          printf("Error: %s.\n", $stmt2->error);

        extLogger(array( 'level'=> 'Error', 'category'=>'mysqlError','message'=> $stmt2->error  , 'infoCredentials' => json_encode(  $infoCredentials ) ) );
        }
	 
}


function persistAd( $itemDatabulk, $infoCredentials, $account=[ ],$platform=null){
	  global $dbconn;
	$query="INSERT INTO creatividades (version, user_id,platform, app_id, auth_id, ad_account, 	id_en_platform,adset_platform_id, campana_platform_id,adaccount_platform_id,property_platform_id,	name,content,url, status,platform_status,	previewurl,	metadata,source, 	
customer_id, 		 campana_root, 	 campanaplatform_id,	 atomo_id  ) 
values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'IMPORTED',
(Select customer_id  from campaigns_platform_atomo where id_en_platform =? and user_id= ? and platform = ? limit 1), 
(Select campana_root_id  from campaigns_platform_atomo where id_en_platform =? and user_id= ? and platform = ? limit 1),  
(Select campanaplatform_id  from campaigns_platform_atomo where id_en_platform =? and user_id= ? and platform = ? limit 1)  ,
(Select id from campaigns_platform_atomo where id_en_platform =? and user_id= ? and platform = ? limit 1) 
) on duplicate key update platform_status=?, metadata=? " ;
 
  $stmt2 = $dbconn->prepare($query);
	
	$base_itemData=[ 'id'=>null, 'name'=>null,'content'=>null, 'description'=>null, 'url'=>null, 'type_platform'=>null,'banner'=>null,'media_type'=>null, 'metadata'=>json_encode([]),'adset_id'=> null, 'preview_shareable_link'=>null ]; 
	foreach($itemDatabulk as $itemData){
	$property_page_id=0;	
$itemData=array_replace_recursive($base_itemData,  $itemData  );
	if (!isset($itemData['targeting_age'])) {		$itemData['targeting_age']=['age_min'=>18,'age_max'=>65];   }
	
	switch($platform){
		case 'FACEBOOK':
				$property_page_id = isset($itemData['promoted_object']['page_id'] ) ?  $itemData['promoted_object']['page_id'] : null;
			break;
			
			
	}
	
 
	$status=unificastatus('ad', $infoCredentials['platform'], $itemData['status'], ['estado' => $itemData['status'],  'otros' => $itemData['effective_status'], 'record' => $itemData]);
//	echo substr_count($query,'?'  ).PHP_EOL;
//	echo strlen("ssssssssssssssssssssssssssss" ).PHP_EOL;
 //$campana_id= isset(  $itemData['campaign_id']  )?   $itemData['campaign_id']: "Select campana_platform_id  from campaigns_platform_atomo where id_en_platform ='{$itemData['adset_id']}' and user_id='{$infoCredentials['user_id']}' and platform ='{$infoCredentials['platform']}' limit 1  ";
//	echo $campana_id;
	
	//$itemData['name']=substr($itemData['name'],0, (                ) )
	
 if ( !isJson(json_encode($itemData)  )) {
	 // to-do logear en log ovh 
//	 error_reporting(E_ALL  );
	// echo 'abc'.PHP_EOL;
	// 	print_r( $itemData );
//	 	print_r(json_encode($itemData,JSON_UNESCAPED_UNICODE));
	// 	print_r(json_encode($itemData,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
	 
	}
	
	$stmt2->bind_param("ssssssssssssssssssssssssssssssss", ...[	VERSIONCODIGO,
		$infoCredentials['user_id'],  $infoCredentials['platform'], $infoCredentials['app_id'],   $infoCredentials['id'], $account['id'],
		 $itemData['id'],  $itemData['adset_id'],   $itemData['campaign_id'],  $account['account_platform_id'],  $property_page_id, 
		 $itemData['name'],  $itemData['content'],  $itemData['url'], $status,	$status,	$itemData['preview_shareable_link'],json_encode($itemData),							
		$itemData['adset_id'],	 $infoCredentials['user_id'],  $infoCredentials['platform'],			
		$itemData['adset_id'],	 $infoCredentials['user_id'],  $infoCredentials['platform'],			
		$itemData['adset_id'],	 $infoCredentials['user_id'],  $infoCredentials['platform'],			
		$itemData['adset_id'],	 $infoCredentials['user_id'],  $infoCredentials['platform'],					
		$status, json_encode($itemData)
	]);
 

        $stmt2->execute();
	 			if ($stmt2->error != "") {
					print_r($stmt2->error );
        	extLogger(array( 'level'=> 'Error', 'category'=>'mysqlError','message'=> $stmt2->error  , 'infoCredentials' => json_encode(  $infoCredentials ) ) );
        }
	}
 
}
