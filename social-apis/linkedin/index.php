<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'vendor/autoload.php';
include 'configs/general.php';
include 'configs/stats_mapeometrics.php';

use Psr\Http\Message\ServerRequestInterface;

/* 
    - LINKEDIN ADS API - 
    POST 
    Request
    [action, auth_id, parent_platform_ids, platform_id, period ,star_date, end_date] 
*/

function getAdAccount_PublicID($accountID){
    global $dbconn;

    $query = "SELECT * FROM app_thesoci_9c37.ads_accounts where account_id = '" . $accountID . "';";
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();

    return $row['public_id'];
}

function getAdAccount($id){
    global $dbconn;

    $query = "SELECT * FROM app_thesoci_9c37.ads_accounts where account_id = '" . $id . "';";
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();

    return $row;
}

function linkedin_retrieve_organization($appid, $access_token, $userid, $rowdata)
{
    // Advertising accounts can have a maximum of 5,000 campaigns and 15,000 creatives.
    $organizationid= str_replace('urn:li:organization:',  '', $rowdata['organizationlookup']);
    $ch = curl_init();
 
    curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/organizations/'.$organizationid); //&search.status.values[0]=ACTIVE
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 0);

    $headers = array();
    $headers[] = 'Authorization: Bearer ' . $access_token;

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    $result = json_decode($result);

    if(!isset($result->vanityName)){
      echo 'error linkedin_retrieve_organization '.  print_r($result, true);
      return false;
    } else {
        return $result ;    
    }
    
}

function persistAccount($accountItem, $infoCredentials){
   
    global $dbconn;

    $stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`ads_accounts` (`user_id`, `platform`, `app_id`, `account_id`, `name`, `platform_user_id`, `status`, `currency`, `metadata`, `customer_id`, `auth_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?) 	ON DUPLICATE KEY UPDATE `name` = ?, `status`= ?, `platform_user_id` = ?, `metadata` = ? ");
        
    $metadataaa = serialize($accountItem);

    $stmt->bind_param("isississsiissis", $infoCredentials["user_id"],  
                                        $infoCredentials["platform"], 
                                        $infoCredentials["app_id"], 
                                        $accountItem->id, 
                                        $accountItem->name,
                                        $infoCredentials['platform_user_id'], 
                                        $accountItem->status, 
                                        $accountItem->currency, 
                                        $metadataaa,
                                        $infoCredentials["customer_id_default"], 
                                        $infoCredentials["id"], 
                                        $accountItem->name, 
                                        $accountItem->status, 
                                        $infoCredentials['platform_user_id'], 
                                        $metadataaa
                                    );

    try {
        
        $stmt->execute();

    } catch (Exception $e) {
        print_r($e);
    }    

}

function refresh_token($infoCredentials)
{

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://www.linkedin.com/oauth/v2/accessToken');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=refresh_token&refresh_token=" . $infoCredentials['refresh_token'] . "&client_id=" . $infoCredentials['app_id'] . "&client_secret=" . $infoCredentials['app_secret']);

    $result = curl_exec($ch);
    $result = json_decode($result);
    print_r($result);
    die();
    return $result['access_token'];
}


// helpers

function linkedin_stats_creative_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate)
{
    return linkedin_stats_account_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, 'CREATIVE', 'DAILY');
}

function linkedin_stats_ad_group_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate)
{
    return linkedin_stats_account_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, 'AD_GROUP', 'DAILY');
}

function linkedin_stats_campana_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate)
{
    $entity_filter = trim("accounts[0]=urn:li:sponsoredAccount:{$rowdata}");
    return linkedin_stats_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, 'CAMPAIGN_GROUP', 'DAILY', $entity_filter);
}

function linkedin_stats_atomo_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate)
{
    $entity_filter = "campaignGroups[0]=urn:li:sponsoredCampaignGroup:{$rowdata['id_en_platform']}";
    return linkedin_stats_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, 'CAMPAIGN', 'DAILY', $entity_filter);
}

function linkedin_stats_ads_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate)
{
    $entity_filter = "campaigns[0]=urn:li:sponsoredCampaign:{$rowdata['id_en_platform']}";
    return linkedin_stats_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, 'CAMPAIGN', 'DAILY', $entity_filter);
}

function linkedin_stats_account_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, $pivot, $timegranularity)
{
    $entity_filter = "accounts[0]=urn:li:sponsoredAccount:{$rowdata['platform_account_id']}";
    return linkedin_stats_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, $pivot, $timegranularity, $entity_filter);
}

function linkedin_stats_xdias($appid, $access_token, $userid, $rowdata, $startdate, $enddate, $pivot, $timegranularity, $entity_filter)
{

    $startdate = strtotime($startdate);
    $enddate = strtotime($enddate);

    $startyear = date('Y', $startdate);
    $startmonth = date('m', $startdate);
    $startday = date('j', $startdate);

    $endyear = date('Y', $enddate);
    $endmonth = date('m', $enddate);
    $endday = date('j', $enddate);

    $url = "https://api.linkedin.com/v2/adAnalyticsV2?q=analytics&pivot={$pivot}&timeGranularity={$timegranularity}&dateRange.start.month={$startmonth}&dateRange.start.day={$startday}&dateRange.start.year={$startyear}&dateRange.end.month={$endmonth}&dateRange.end.day={$endday}&dateRange.end.year={$endyear}&fields=clicks,dateRange,impressions,likes,shares,costInLocalCurrency,pivot,pivotValue,costInUsd,approximateUniqueImpressions,videoStarts,videoCompletions,videoFirstQuartileCompletions,videoMidpointCompletions,videoThirdQuartileCompletions,totalEngagements&" . $entity_filter;
    $headers = array();
    $headers[] = 'Authorization: Bearer ' . $access_token;;
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer {$access_token}"
        ),
    ));

    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);


    if ($err) {
        var_dump($err);
    }

    $response = json_decode($response, true);

    if (isset($response['elements'])) {
        $response = $response['elements'];
    } else {
        print_r($response);
    }
    return $response;

}

function helper_metrics_campana_day($platformid, $user_id, $customer_id = 0, $datos, $ad_account_id, $ad_account_id_platform, $metadata = ['currency' => null], $job_id = "")
{
    global $dbconn_stats, $db;
    if (!isset($metadata['currency'])) {
        $metadata['currency'] = null;
    }
    if (!is_array($datos)) {
        return null;
    }

    echo ' > Updating ' . count($datos) . ' elements : ';
    $i = 1;

    foreach ($datos as $inputraw) {
        echo $i++ . ', ';

        $outputrow = ['id_in_platform' => 0, 'campananame' => '', 'platformid' => $platformid, 'date' => '', 'metrics_delivery' => [], 'metrics_costs' => [], 'metrics_engagement' => [], 'metrics_video' => [], 'metrics_conversion' => [], 'metrics_rest' => [], 'cost' => 0, 'impressions' => 0, 'reach' => 0, 'clicks' => 0, 'engagements' => 0, 'video_views' => 0, 'video_starts' => 0, 'currency' => $metadata['currency'], 'conversions' => 0];

        $outputrow = helper_metrics_keytranslator($platformid, 'campana', $inputraw, $outputrow, $metadata);
        echo "campana_day_************" . PHP_EOL;
        echo $outputrow['impressions'] . PHP_EOL;
        echo "************" . PHP_EOL;
        if ($outputrow['impressions'] == 0 && $outputrow['video_views'] == 0 && $outputrow['reach'] == 0 && $outputrow['clicks'] == 0 && $outputrow['engagements'] == 0) {
            continue;
        }

        $sql = "INSERT INTO adsconcierge_stats.platform_campana_day ( user_id, platformid, customer_id, ad_account_id, campanaid,
                                                idenplatform, campananame, adccountid_pl, dia, unico,
                                                currency, metrics_delivery, metrics_costs, metrics_engagement, metrics_video,
                                                metrics_conversion, metrics_rest, cost, impressions, reach,
                                                clicks, engagements, video_views, conversions, plataforma,
                                                campanaroot, yearweek, yearmonth )
                                SELECT
                                    {$user_id},
                                    '{$outputrow['platformid']}',
                                    '{$customer_id}',
                                    '{$ad_account_id}',
                                    cp.id,
                                    '{$outputrow['id_in_platform']}',
                                    cp.name,
                                    '{$ad_account_id_platform}',
                                    '{$outputrow['date']}',
                                    '" . md5($outputrow['platformid'] . $outputrow['id_in_platform'] . $outputrow['date'] . $user_id) . "',
                                    '{$outputrow['currency']}',
                                    '" . json_encode($outputrow['metrics_delivery']) . "',
                                    '" . json_encode($outputrow['metrics_costs']) . "',
                                    '" . json_encode($outputrow['metrics_engagement']) . "',
                                    '" . json_encode($outputrow['metrics_video']) . "',
                                    '" . json_encode($outputrow['metrics_conversion']) . "',
                                    '" . $db->real_escape_string(json_encode($outputrow['metrics_rest'])) . "',
                                    '{$outputrow['cost']}', '{$outputrow['impressions']}',
                                    '{$outputrow['reach']}',
                                    '{$outputrow['clicks']}', '{$outputrow['engagements']}',
                                    '" . intval($outputrow['video_views']) . "',
                                    '{$outputrow['conversions']}',
                                    '{$platformid}',
                                    cp.campana_root,
                                    YEARWEEK('{$outputrow['date']}'),
                                    date_format('{$outputrow['date']}', '%Y-%m')
                                FROM
                                    app_thesoci_9c37.campaigns_platform cp
                                WHERE
                                    cp.id_en_platform = '{$outputrow['id_in_platform']}' AND cp.user_id = {$user_id}
                                ON DUPLICATE KEY UPDATE
                                    cost                = '{$outputrow['cost']}',
                                    impressions         = '{$outputrow['impressions']}' ,
                                    reach               = '{$outputrow['reach']}',
                                    clicks              = '{$outputrow['clicks']}',
                                    engagements         = '{$outputrow['engagements']}',
                                    video_views         = '" . intval($outputrow['video_views']) . "',
                                    video_starts        = '" . intval($outputrow['video_starts']) . "',
                                    video_completes     = '" . intval($outputrow['video_completes']) . "',
                                    video_25            = '" . intval($outputrow['video_25']) . "',
                                    video_50            = '" . intval($outputrow['video_50']) . "',
                                    video_75            = '" . intval($outputrow['video_75']) . "',
                                    conversions         = '{$outputrow['conversions']}',
	                                metrics_delivery    = '" . json_encode($outputrow['metrics_delivery']) . "',
	                                metrics_costs       = '" . json_encode($outputrow['metrics_costs']) . "',
	                                metrics_engagement  = '" . json_encode($outputrow['metrics_engagement']) . "',
	                                metrics_video       = '" . json_encode($outputrow['metrics_video']) . "',
	                                metrics_conversion  = '" . json_encode($outputrow['metrics_conversion']) . "',
                                    metrics_rest        = '" . $db->real_escape_string(json_encode($outputrow['metrics_rest'])) . "'
                                ";

        if (!$db->query($sql)) {
            printf("Error: %s\n", $db->error);
            echo $sql;
        }

    }

    echo PHP_EOL . ' DB CampañaxDia data updated > ';
    $sql = "UPDATE adsconcierge_stats.background_job SET status = 'finished' WHERE id = {$job_id}";
    //$db->query($sql);

}

function helper_metrics_lineitem_day($platformid, $user_id, $customer_id = 0, $campanaInfo, $datos, $job_id)
{
    global $dbconn_stats, $db;
    if (!isset($metadata['currency'])) {
        $metadata['currency'] = null;
    }

    foreach ($datos as $inputraw) {

        $outputrow = ['id_in_platform' => 0, 'campananame' => '', 'platformid' => $platformid, 'date' => '', 'metrics_delivery' => [], 'metrics_costs' => [], 'metrics_engagement' => [], 'metrics_video' => [], 'metrics_conversion' => [], 'metrics_rest' => [], 'cost' => 0, 'impressions' => 0, 'reach' => 0, 'clicks' => 0, 'engagements' => 0,
            'video_views' => 0, 'video_starts' => 0, 'currency' => $metadata['currency'], 'conversions' => 0];
        $outputrow = helper_metrics_keytranslator($platformid, 'lineitem', $inputraw, $outputrow, $metadata);

        echo "helper_metrics_lineitem_day************" . PHP_EOL;
        echo $outputrow['impressions'] . PHP_EOL;
        echo "************" . PHP_EOL;

        if ($outputrow['impressions'] == 0 && $outputrow['video_views'] == 0 && $outputrow['reach'] == 0 && $outputrow['clicks'] == 0 && $outputrow['engagements'] == 0) {
            continue;
        }
        $sql = "INSERT INTO adsconcierge_stats.platform_atomo_day (
                                                user_id, platformid, customer_id, ad_account_id, campanaid,
                                                atomoid, idenplatform, adccountid_pl, dia, unico,
                                                metrics_delivery, metrics_costs, metrics_engagement, metrics_video,
                                                metrics_conversion, metrics_rest, cost, impressions, reach,
                                                clicks, engagements, video_views, conversions, plataforma,
                                                campanaroot, yearweek, yearmonth
                                                )
                                SELECT
                                        {$user_id}, {$outputrow['platformid']}, cpo.customer_id, {$campanaInfo['ad_account_id']}, cpo.campana_platform_id,
                                        cpo.id, '{$outputrow['id_in_platform']}',  aa.account_id, '{$outputrow['date']}', '" . md5($outputrow['platformid'] . $outputrow['id_in_platform'] . $outputrow['date'] . $user_id) . "',
                                        '" . json_encode($outputrow['metrics_delivery']) . "', '" . json_encode($outputrow['metrics_costs']) . "', '" . json_encode($outputrow['metrics_engagement']) . "', '" . json_encode($outputrow['metrics_video']) . "',
                                        '" . json_encode($outputrow['metrics_conversion']) . "', '" . $db->real_escape_string(json_encode($outputrow['metrics_rest'])) . "', '{$outputrow['cost']}', '{$outputrow['impressions']}', '{$outputrow['reach']}',
                                        '{$outputrow['clicks']}', '{$outputrow['engagements']}', '{$outputrow['video_views']}', '{$outputrow['conversions']}', '{$platformid}',
                                        cp.campana_root,  YEARWEEK('{$outputrow['date']}'),  date_format('{$outputrow['date']}', '%Y-%m')
                                FROM app_thesoci_9c37.campaigns_platform_atomo cpo
                                    JOIN app_thesoci_9c37.campaigns_platform cp ON cp.id = cpo.campana_platform_id
                                    JOIN app_thesoci_9c37.ads_accounts aa ON aa.id = cpo.ad_account
                                    WHERE cpo.id_en_platform = '{$outputrow['id_in_platform']}' and cpo.user_id = {$user_id}
                                ON DUPLICATE KEY UPDATE
                                    cost                = '{$outputrow['cost']}',
                                    impressions         = '{$outputrow['impressions']}' ,
                                    reach               = '{$outputrow['reach']}',
                                    clicks              = '{$outputrow['clicks']}',
                                    engagements         = '{$outputrow['engagements']}',
                                    video_views         = '{$outputrow['video_views']}',
                                    conversions         = '{$outputrow['conversions']}',
	                                metrics_delivery    = '" . json_encode($outputrow['metrics_delivery']) . "',
	                                metrics_costs       = '" . json_encode($outputrow['metrics_costs']) . "',
	                                metrics_engagement  = '" . json_encode($outputrow['metrics_engagement']) . "',
	                                metrics_video       = '" . json_encode($outputrow['metrics_video']) . "',
	                                metrics_conversion  = '" . json_encode($outputrow['metrics_conversion']) . "',
                                    metrics_rest        = '" . $db->real_escape_string(json_encode($outputrow['metrics_rest'])) . "'";

        $db->query($sql);
    }

    echo ' DB AtomoxDia data updated > ';

    $db->query("UPDATE adsconcierge_stats.background_job SET status = 'finished' WHERE id = {$job_id}");
}

function helper_metrics_ads_day($platformid, $user_id, $customer_id = 0, $atomoInfo, $datos, $job_id)
{
    global $dbconn_stats, $db;
    if (!isset($metadata['currency'])) {
        $metadata['currency'] = null;
    }

    foreach ($datos as $inputraw) { //echo "<pre>";print_r($inputraw);
        $outputrow = ['id_in_platform' => 0, 'campananame' => '', 'platformid' => $platformid, 'date' => '', 'metrics_delivery' => [], 'metrics_costs' => [], 'metrics_engagement' => [], 'metrics_video' => [], 'metrics_conversion' => [], 'metrics_rest' => [], 'cost' => 0, 'impressions' => 0, 'reach' => 0, 'clicks' => 0, 'engagements' => 0,
            'video_views' => 0, 'video_starts' => 0, 'currency' => $metadata['currency'], 'conversions' => 0];
        $outputrow = helper_metrics_keytranslator($platformid, 'ad', $inputraw, $outputrow, $metadata);
        if ($outputrow['impressions'] == 0 && $outputrow['video_views'] == 0 && $outputrow['reach'] == 0 && $outputrow['clicks'] == 0 && $outputrow['engagements'] == 0) {
            continue;
        }
        echo "helper_metrics_ads_day************" . PHP_EOL;
        echo $outputrow['impressions'] . PHP_EOL;
        echo "************" . PHP_EOL;
        $sql = "INSERT INTO adsconcierge_stats.platform_ads_day (
                                                user_id, platformid, customer_id, ad_account_id, campanaid,
                                                lineitemid, idenplatform, adccountid_pl, dia, unico,
                                                ad_name, metrics_delivery, metrics_costs, metrics_engagement, metrics_video,
                                                metrics_conversion, metrics_rest, cost, impressions, reach,
                                                clicks, engagements, video_views, conversions, plataforma,
                                                campanaid_enplatform, lineitem_enplatform,
                                                campanaroot, yearweek, yearmonth
                                                )
                                SELECT
                                        {$user_id}, {$outputrow['platformid']}, c.customer_id, {$atomoInfo['ad_account_id']}, cp.id,
                                        cpo.id, '{$outputrow['id_in_platform']}',  aa.account_id, '{$outputrow['date']}', '" . md5($outputrow['platformid'] . $outputrow['id_in_platform'] . $outputrow['date'] . $user_id) . "',
                                        c.title,'" . json_encode($outputrow['metrics_delivery']) . "', '" . json_encode($outputrow['metrics_costs']) . "', '" . json_encode($outputrow['metrics_engagement']) . "', '" . json_encode($outputrow['metrics_video']) . "',
                                        '" . json_encode($outputrow['metrics_conversion']) . "', '" . $db->real_escape_string(json_encode($outputrow['metrics_rest'])) . "', '{$outputrow['cost']}', '{$outputrow['impressions']}', '{$outputrow['reach']}',
                                        '{$outputrow['clicks']}', '{$outputrow['engagements']}', '{$outputrow['video_views']}', '{$outputrow['conversions']}', '{$platformid}',
                                        cp.id_en_platform, cpo.id_en_platform,
                                        cp.campana_root,  YEARWEEK('{$outputrow['date']}'),  date_format('{$outputrow['date']}', '%Y-%m')
                                FROM app_thesoci_9c37.creatividades c
                                    JOIN app_thesoci_9c37.campaigns_platform_atomo cpo on cpo.id = c.atomo_id
                                    JOIN app_thesoci_9c37.campaigns_platform cp ON cp.id = cpo.campana_platform_id
                                    JOIN app_thesoci_9c37.ads_accounts aa ON aa.id = cpo.ad_account
                                WHERE c.id_en_platform = '{$outputrow['id_in_platform']}' and c.user_id = {$user_id}
                                ON DUPLICATE KEY UPDATE
                                    cost                = '{$outputrow['cost']}',
                                    impressions         = '{$outputrow['impressions']}' ,
                                    reach               = '{$outputrow['reach']}',
                                    clicks              = '{$outputrow['clicks']}',
                                    engagements         = '{$outputrow['engagements']}',
                                    video_views         = '{$outputrow['video_views']}',
                                    conversions         = '{$outputrow['conversions']}',
	                                metrics_delivery    = '" . json_encode($outputrow['metrics_delivery']) . "',
	                                metrics_costs       = '" . json_encode($outputrow['metrics_costs']) . "',
	                                metrics_engagement  = '" . json_encode($outputrow['metrics_engagement']) . "',
	                                metrics_video       = '" . json_encode($outputrow['metrics_video']) . "',
	                                metrics_conversion  = '" . json_encode($outputrow['metrics_conversion']) . "',
	                                metrics_rest        = '" . $db->real_escape_string(json_encode($outputrow['metrics_rest'])) . "'

                     "; //echo $sql;die;
        $db->query($sql);
    }
    echo ' DB AdsxDia data updated > ';

    $sql = "UPDATE adsconcierge_stats.background_job SET status = 'finished' WHERE id = {$job_id}";
    //$db->query($sql);
}

function helper_metrics_keytranslator($platformid, $tipoinput, $inputraw, $outputrow, $metadata = null)
{
    global $mapeometricas;

    //var_dump($inputraw);
    //echo PHP_EOL;
    // die();

    switch ($platformid) {
        case 1:
        case "FACEBOOK":
            switch ($tipoinput) {
                case 'campana':
                    $outputrow['id_in_platform'] = $inputraw['campaign_id'];
                    break;
                case 'ad':
                    $outputrow['id_in_platform'] = $inputraw['ad_id'];
                    $outputrow['lineitem_id'] = $inputraw['adset_id'];

                    break;
                case 'lineitem':
                    $outputrow['id_in_platform'] = $inputraw['adset_id'];
                    $outputrow['lineitem_id'] = $inputraw['adset_id'];

                    break;

            }
            $outputrow['date'] = $inputraw['date_start'];
            $outputrow['yearmonth'] = date('Y-m-01', strtotime($inputraw['date_start']));
            $outputrow['platformid'] = 1;
            switch ($inputraw['publisher_platform']) {
                case 'facebook':
                    $outputrow['platformid'] = 1;
                    break;
                case 'instagram':
                    $outputrow['platformid'] = 4;
                    break;
            }
            //https://developers.facebook.com/docs/marketing-api/insights/parameters/v9.0
            $outputrow['video_starts'] = $inputraw['video_play_actions'];
            $outputrow['video_views'] = $inputraw['video_play_actions'];
            $outputrow['video_completes'] = $inputraw['video_p100_watched_actions'];
            $outputrow['video_25'] = $inputraw['video_p25_watched_actions'];
            $outputrow['video_50'] = $inputraw['video_p50_watched_actions'];
            $outputrow['video_75'] = $inputraw['video_p75_watched_actions'];

            foreach ($inputraw as $clave => $valor) {
                if ($valor == null) {
                    continue;
                }
                $clave = strtolower($clave);
                $clavemapeo = isset($mapeometricas[$platformid][$clave]) ? $mapeometricas[$platformid][$clave] : (isset($mapeometricas['default'][$clave]) ? $mapeometricas['default'][$clave] : ['metrics_rest']);

                foreach ($clavemapeo as $claveout) {
                    if (is_numeric($outputrow[$claveout])) {
                        $outputrow[$claveout] = $outputrow[$claveout] + $valor;
                    } elseif (is_string($outputrow[$claveout])) {
                        $outputrow[$claveout] = $valor;
                    } else {
                        $outputrow[$claveout][$clave] = $valor;
                    }
                }

            }
            //die();
            break;
        case 2:
        case "LINKEDIN":

            $outputrow['id_in_platform'] = explode(':', $inputraw['pivotValue']);
            $outputrow['id_in_platform'] = $outputrow['id_in_platform'][3];
            $outputrow['platformid'] = 2;
            $outputrow['date'] = $inputraw['dateRange']['start']['year'] . '-' . $inputraw['dateRange']['start']['month'] . '-' . $inputraw['dateRange']['start']['day'];
            $outputrow['yearmonth'] = $inputraw['dateRange']['start']['year'] . '-' . $inputraw['dateRange']['start']['month'] . '-01';
            $outputrow['campananame'] = (isset($metadata['campaignsData'][$inputraw['pivotValue']]) ? $metadata['campaignsData'][$inputraw['pivotValue']]['name'] : null);
            $outputrow['currency'] = (isset($metadata['campaignsData'][$inputraw['pivotValue']]['totalBudget']) ? $metadata['campaignsData'][$inputraw['pivotValue']]['totalBudget']['currencyCode'] : $metadata['currency']);
            $outputrow['impressions'] = $inputraw['impressions'];
            $outputrow['clicks'] = $inputraw['clicks'];
            $outputrow['cost'] = $inputraw['costInLocalCurrency'];
            $outputrow['reach'] = $inputraw['approximateUniqueImpressions'];
            $outputrow['video_starts'] = $inputraw['videoStarts'];
            $outputrow['video_views'] = $inputraw['videoStarts'];
            $outputrow['video_completes'] = $inputraw['videoCompletions'];
            $outputrow['video_25'] = $inputraw['videoFirstQuartileCompletions'];
            $outputrow['video_50'] = $inputraw['videoMidpointCompletions'];
            $outputrow['video_75'] = $inputraw['videoThirdQuartileCompletions'];
            $outputrow['engagements'] = $inputraw['totalEngagements'] - $inputraw['clicks'];

            foreach ($inputraw as $clave => $valor) {
                $clave = strtolower($clave);
                $clavemapeo = isset($mapeometricas[$platformid][$clave]) ? $mapeometricas[$platformid][$clave] : (isset($mapeometricas['default'][$clave]) ? $mapeometricas['default'][$clave] : ['metrics_rest']);
                foreach ($clavemapeo as $claveout) {
                    if (is_numeric($outputrow[$claveout])) {
                        $outputrow[$claveout] = $outputrow[$claveout] + $valor;
                    } else {
                        $outputrow[$claveout][$clave] = $valor;
                    }
                }
            }

            break;
        case 3:
        case "TWITTER":

            $outputrow['date'] = date('Y-m-d', strtotime($inputraw['startdate']));
            $outputrow['yearmonth'] = date('Y-m-01', strtotime($inputraw['startdate']));
            $outputrow['id_in_platform'] = $inputraw['id_in_platform'];
            $outputrow['campananame'] = isset($inputraw['campaign_name']) ? $inputraw['campaign_name'] : '';
            $outputrow['platformid'] = 3;
            $outputrow['video_starts'] = $inputraw['video_content_starts'];
            $outputrow['video_completes'] = $inputraw['video_views_100'];
            $outputrow['video_25'] = $inputraw['video_views_25'];
            $outputrow['video_50'] = $inputraw['video_views_50'];
            $outputrow['video_75'] = $inputraw['video_views_75'];
            $outputrow['reach'] = is_array($inputraw['impressions']) ? $inputraw['impressions'][0] : 0;

            foreach ($inputraw as $clave => $valor) {
                if (is_array($valor)) {
                    $valor = $valor[0];
                }
                if ($valor == null) {
                    continue;
                }
                $clave = strtolower($clave);
                $clavemapeo = isset($mapeometricas[$platformid][$clave]) ? $mapeometricas[$platformid][$clave] : (isset($mapeometricas['default'][$clave]) ? $mapeometricas['default'][$clave] : ['metrics_rest']);
                foreach ($clavemapeo as $claveout) {
                    if (is_numeric($outputrow[$claveout])) {
                        $outputrow[$claveout] = $outputrow[$claveout] + $valor;
                    } elseif (is_string($outputrow[$claveout])) {
                        $outputrow[$claveout] = $valor;
                    } else {
                        $outputrow[$claveout][$clave] = $valor;
                    }
                }

            }
            $outputrow['currency'] = $outputrow['currency']['currency'];
            $outputrow['cost'] = round(($outputrow['cost'] / 1000000), 4);
            $outputrow['engagements'] = $outputrow['engagements'] - $outputrow['clicks'];

            break;
    }

    return $outputrow;
}

// entidades

function get_accounts($infoCredentials){
    
    global $dbconn_stats, $dbconn;
    
    // Advertising accounts can have a maximum of 5,000 campaigns and 15,000 creatives.

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/adAccountsV2?q=search'); //&search.status.values[0]=ACTIVE
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 0);

    $headers = array();
    $headers[] = 'Authorization: Bearer ' . $infoCredentials['access_token'];

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    $result = json_decode($result);

    if(!isset($result->elements)){
        echo 'error getadsccount '.  print_r($result, true);
        //logger
        die();
    }


    $properties = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` (`user_id`, `platform`, `adaccount_id`, `app_id`,`platform_user_id`, `id_en_platform`, `token`,`name`,`status`, `category`, `currency`, `metadata`, `auth_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `name`= ?, `adaccount_id`= ? , `status`= ?, `platform_user_id`=?,  `metadata`= ? ");
    $cuentasarray = [];

    foreach ($result->elements as $AdAccountLinkeding){
        
        echo $AdAccountLinkeding->id . ' .. '.$AdAccountLinkeding->name . PHP_EOL;
        persistAccount($AdAccountLinkeding, $infoCredentials);

        $pagina = linkedin_retrieve_organization(null, $infoCredentials['access_token'], $infoCredentials['user_id'], array('organizationlookup'=> $AdAccountLinkeding->reference  ) );

        $adaccount_name= $AdAccountLinkeding->name;
        $ad_accountid= $AdAccountLinkeding->id;

        $cuentasarray[$AdAccountLinkeding->id] = $AdAccountLinkeding;


        $query = "SELECT id as adacc_id, public_id as adaccount_public_id FROM `app_thesoci_9c37`.`ads_accounts` WHERE auth_id = '{$infoCredentials["id"]}' and platform = '{$infoCredentials["platform"]}' and platform_user_id= '{$infoCredentials['platform_user_id']}' and account_id = '{$ad_accountid}' limit 1    ";
        $res_ac = $dbconn->query($query);
        $id_ac = $res_ac->fetch_assoc();


        $pagename = isset($pagina->localizedName) ? $pagina->localizedName : $AdAccountLinkeding->name;
        $AdAccountLinkeding->pagedata = $pagina;             
        $page_id = $AdAccountLinkeding->reference;        

        $properties->bind_param("ssssssssssssssssss", ...[
                                        $infoCredentials['user_id'], 
                                        $infoCredentials['platform'],  
                                        $id_ac["adacc_id"], 
                                        $infoCredentials["app_id"], 
                                        $infoCredentials['platform_user_id'],
                                        $page_id , 
                                        '-',
                                        $pagename, 
                                        $AdAccountLinkeding->status . '>' . $AdAccountLinkeding->servingStatuses[0], 
                                        $AdAccountLinkeding->type, 
                                        $AdAccountLinkeding->currency, 
                                        json_encode($AdAccountLinkeding),
                                        $infoCredentials['id'], 
                                        $pagename,  
                                        $id_ac["adacc_id"], 
                                        $AdAccountLinkeding->status . '>' . $AdAccountLinkeding->servingStatuses[0], 
                                        $infoCredentials['platform_user_id'],
                                        json_encode($AdAccountLinkeding)]);
        $properties->execute();

        if ($properties->error != "") {
            print_r("Error: %s.\n", $properties->error);
        }

        // properties_relations
        $query = "SELECT id as pageid, public_id as property_public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE  auth_id = '{$infoCredentials["id"]}' and platform = '{$infoCredentials["platform"]}' and platform_user_id= '{$infoCredentials['platform_user_id']}' and id_en_platform = '{$page_id}'";
        $res_pa = $dbconn->query($query);
        $id_pa = $res_pa->fetch_assoc();

        /*** Inserto registros en tabla de relaciones Properties y Ads Accounts
            $query = "SELECT auths_user_id FROM `app_thesoci_9c37`.`auths_user_platform` WHERE id = " .$auths["id"];
            $res_aup = $dbconn->query($query);
            $id_aup = $res_aup->fetch_assoc();
            $id_aup = isset($id_aup["auths_user_id"]) ? $id_aup["auths_user_id"] : "";
            $query = "SELECT public_id FROM `app_thesoci_9c37`.`ads_accounts` WHERE account_id LIKE '" .$item->id ."'";
            $res_ac = $dbconn->query($query);
            $id_ac = $res_ac->fetch_assoc();
            $id_ac = isset($id_ac["public_id"]) ? $id_ac["public_id"] : "";
            $query = "SELECT public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE id_en_platform LIKE '" .$item->id ."'";
            $res_pa = $dbconn->query($query);
            $id_pa = $res_pa->fetch_assoc();
            $id_pa = isset($id_pa["public_id"]) ? $id_pa["public_id"] : "";
            $query = "SELECT public_id FROM `app_thesoci_9c37`.`users` WHERE id = " .$auths["user_id"];
            $res_u = $dbconn->query($query);
            $id_u = $res_u->fetch_assoc();
            $id_u = isset($id_u["public_id"]) ? $id_u["public_id"] : "";
            $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` (`auth_id`, `auth_publicid`, `ad_account`, `ad_account_publicid`, `ad_account_name`, `property_id`, `property_publicid`, `property_name`, `user_id`, `user_publicid`) VALUES (?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");
            $stmt3->bind_param("isssssssisss", $auths["id"], $id_aup, $item->id, $id_ac, $item->name, $item->id, $id_pa, $item->name, $auths["user_id"], $id_u, $item->name, $item->name);
            $stmt3->execute();
        **/

        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` 
                                    (`type`,`auth_id`, `ad_account`,  `ad_account_name`,`property_name`, `property_id`, `user_id`,`platform`,`auth_publicid`,   `user_publicid`,  `ad_account_publicid`,`property_publicid`) 
                                        VALUES ('PAGE', ?,?,?,?,?,?,?,?,?,?,?)
                                            ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");              
      
        $stmt3->bind_param("sssssssssssss", 
                            $infoCredentials["id"], 
                            $id_ac["adacc_id"] ,
                            $adaccount_name,
                            $pagename, 
                            $id_pa["pageid"],      
                            $infoCredentials['user_id'],
                            $infoCredentials["platform"],                   
                            $infoCredentials["auths_user_id"],
                            $infoCredentials["user_public_id"],
                            $id_ac["adaccount_public_id"], 
                            $id_pa["property_public_id"],  
                            $adaccount_name, 
                            $pagename);
        
                            $stmt3->execute();                  
        if ($stmt3->error != "") {
            print_r("Error: %s.\n", $subadsaccount->error);
        }



    }

    return $cuentasarray;

}

function campaigns_get_linkedin($infoCredentials, $data)
{
    global $dbconn_stats, $dbconn;

	/*** Conditions
	 * Advertising accounts have a hard limit of 5,000 campaigns, regardless of campaign status.
	 * Advertising accounts are limited to a maximum of 1,000 concurrent campaigns in ACTIVE status at any given time.
	 * A campaign can have a maximum of 15 active creatives and 85 inactive creatives.
	 * A campaign can only have creatives matching the ad format selected at time of creation.
	 * If a campaign has no ad format set, it will be set by the first creative created under that campaign. Dynamic, carousel, and video ad campaigns must have their format set upon creation.
	 * A campaign is considered active until it reaches its end time or gets deleted.
	 * Paused campaigns are considered active until their designated end times.
	* **/

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/adCampaignGroupsV2?q=search');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 0);
    $headers = array();
    $headers[] = 'Authorization: Bearer ' . $infoCredentials['access_token'];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    $result = json_decode($result);
    $campanas = [];
    
    foreach ($result->elements as $item) {
        $campanas['urn:li:sponsoredCampaignGroup:' . $item->id] = (array) $item;
    }

    $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (user_id, platform, app_id, auth_id, name, id_en_platform, budget, 
                                        status, currency, metadata, ad_account, campana_root, customer_id, source)
                                        values (?,?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts 
                                        WHERE ads_accounts.account_id = ? and ads_accounts.user_id = ? ), ?, ?, 'IMPORTED')
                                        on duplicate key update status = ?, metadata = ? ");

    foreach ($campanas as $item) {

        $item = (object) $item;
        $accountbb = str_replace('urn:li:sponsoredAccount:', '', $item->account);
        $accountAd = getAdAccount($accountbb);

        //var_dump($item->account);die();
        echo '    campaign ' . $item->id . ' - '. $item->name . PHP_EOL;

        $stmtcampana->bind_param("issssssssssissss", ...[   $infoCredentials['user_id'], 
                                                            $infoCredentials['platform'], 
                                                            $infoCredentials['app_id'], 
                                                            $infoCredentials['id'], 
                                                            $item->name, 
                                                            $item->id, 
                                                            null, 
                                                            unificastatus('campana', $infoCredentials['platform'], $item->status, ['estado' => $item->status, 'otros' => $item->servingStatuses[0], 'record' => $item]), 
                                                            $accountAd['currency'], 
                                                            json_encode($item), 
                                                            $accountbb, 
                                                            $infoCredentials['user_id'], 
                                                            $infoCredentials["campaign_root_default"], 
                                                            $infoCredentials["customer_id_default"], 
                                                            unificastatus('campana', 
                                                            $infoCredentials['platform'], 
                                                            $item->status, ['estado' => $item->status, 'otros' => $item->servingStatuses[0], 'record' => $item]), json_encode($item)]);

        $stmtcampana->execute();

        if ($stmtcampana->error != "") {
            print_r("Error: %s.\n", $stmtcampana->error);
        }

    }


}

function get_linkedin_creas($infoCredentials, $data)
{
    global $dbconn_stats, $dbconn;

	/*** Conditions
	 * Advertising accounts have a hard limit of 5,000 campaigns, regardless of campaign status.
	 * Advertising accounts are limited to a maximum of 1,000 concurrent campaigns in ACTIVE status at any given time.
	 * A campaign can have a maximum of 15 active creatives and 85 inactive creatives.
	 * A campaign can only have creatives matching the ad format selected at time of creation.
	 * If a campaign has no ad format set, it will be set by the first creative created under that campaign. Dynamic, carousel, and video ad campaigns must have their format set upon creation.
	 * A campaign is considered active until it reaches its end time or gets deleted.
	 * Paused campaigns are considered active until their designated end times.
	* **/

    $ch = curl_init();
    //GET https://api.linkedin.com/v2/adCreativesV2?q=search&search.{searchCriteria}.values[0]={searchValue}

    curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/adCreativesV2?q=search');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 0);

    $headers = array();
    $headers[] = 'Authorization: Bearer ' . $infoCredentials['access_token'];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    $result = json_decode($result);
    $data = [];

    //var_dump($result);die();
    
    foreach ($result->elements as $item) {
        $data[] = (array) $item;
    }

    var_dump($data);die();


    /*    $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (user_id, platform, app_id, auth_id, name, id_en_platform, budget, 
                                        status, currency, metadata, ad_account, campana_root, customer_id, source)
                                        values (?,?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts 
                                        WHERE ads_accounts.account_id = ? and ads_accounts.user_id = ? ), ?, ?, 'IMPORTED')
                                        on duplicate key update status = ?, metadata = ? ");

    foreach ($campanas as $item) {

        $item = (object) $item;
        $accountbb = str_replace('urn:li:sponsoredAccount:', '', $item->account);
        $accountAd = getAdAccount($accountbb);

        //var_dump($item->account);die();
        echo '    campaign ' . $item->id . ' - '. $item->name . PHP_EOL;

        $stmtcampana->bind_param("issssssssssissss", ...[   $infoCredentials['user_id'], 
                                                            $infoCredentials['platform'], 
                                                            $infoCredentials['app_id'], 
                                                            $infoCredentials['id'], 
                                                            $item->name, 
                                                            $item->id, 
                                                            null, 
                                                            unificastatus('campana', $infoCredentials['platform'], $item->status, ['estado' => $item->status, 'otros' => $item->servingStatuses[0], 'record' => $item]), 
                                                            $accountAd['currency'], 
                                                            json_encode($item), 
                                                            $accountbb, 
                                                            $infoCredentials['user_id'], 
                                                            $infoCredentials["campaign_root_default"], 
                                                            $infoCredentials["customer_id_default"], 
                                                            unificastatus('campana', 
                                                            $infoCredentials['platform'], 
                                                            $item->status, ['estado' => $item->status, 'otros' => $item->servingStatuses[0], 'record' => $item]), json_encode($item)]);

        $stmtcampana->execute();

        if ($stmtcampana->error != "") {
            print_r("Error: %s.\n", $stmtcampana->error);
        }

    } */


}

function get_linkedin_atomos($infoCredentials, $data)
{
    global $dbconn_stats, $dbconn;


    $ch = curl_init();
    //GET https://api.linkedin.com/v2/adCreativesV2?q=search&search.{searchCriteria}.values[0]={searchValue}

    curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/adCreativesV2?q=search');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 0);

    $headers = array();
    $headers[] = 'Authorization: Bearer ' . $infoCredentials['access_token'];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    $result = json_decode($result);
    $data = [];

    //var_dump($result);die();
    
    foreach ($result->elements as $item) {
        $data[] = (array) $item;
    }



}
//estadisticas
function campaigns_stats_get_linkedin($infoCredentials, $data)
{

    foreach ($data['parent_platform_ids'] as $parent) {
       
        $datos = linkedin_stats_campana_xdias(
            $infoCredentials['app_id'],
            $infoCredentials['access_token'],
            $infoCredentials['user_id'],
            $parent['id'],
            $data['start_date'],
            $data['end_date']
        );

        helper_metrics_campana_day(
            $infoCredentials['platform'],
            $infoCredentials['user_id'],
            $infoCredentials['customer_id_default'],
            $datos,
            $infoCredentials['id'],
            $parent['id'],
            $infoCredentials,
            null
        );

    }
}

function ads_stats_get_linkedin($infoCredentials, $data)
{
    foreach ($data['parent_platform_ids'] as $parent) {
        $datos = linkedin_stats_ads_xdias($infoCredentials['app_id'], $infoCredentials['access_token'], $infoCredentials['user_id'], $args['atomo_info'], $args['start_date'], $args['end_date']) ;
        helper_metrics_ads_day($infoCredentials['platform'], $infoCredentials['user_id'], $infoCredentials['customer_id'], $parent['id'], $datos, null);
    }
}

function atomo_stats_get_linkedin($infoCredentials, $data)
{
    foreach ($data['parent_platform_ids'] as $parent) {
        $datos = linkedin_stats_atomo_xdias($infoCredentials['app_id'], $infoCredentials['access_token'], $infoCredentials['user_id'], $parent['id'], $args['start_date'], $args['end_date']) ;
        helper_metrics_lineitem_day($infoCredentials['platform'], $infoCredentials['user_id'], $infoCredentials['customer_id'], $parent['id'], $datos, null);
    }
}

function creas_stats_get_linkedin($infoCredentials, $data)
{
    foreach ($data['parent_platform_ids'] as $parent) {
        $datos = linkedin_stats_creative_xdias($infoCredentials['app_id'], $infoCredentials['access_token'], $infoCredentials['user_id'], $parent['id'], $args['start_date'], $args['end_date']) ;
        helper_metrics_lineitem_day($infoCredentials['platform'], $infoCredentials['user_id'], $infoCredentials['customer_id'], $parent['id'], $datos, null);
    }
}

function adGroup_stats_get_linkedin($infoCredentials, $data)
{
    foreach ($data['parent_platform_ids'] as $parent) {
        $datos = linkedin_stats_ad_group_xdias($infoCredentials['app_id'], $infoCredentials['access_token'], $infoCredentials['user_id'], $parent['id'], $args['start_date'], $args['end_date']) ;
        helper_metrics_lineitem_day($infoCredentials['platform'], $infoCredentials['user_id'], $infoCredentials['customer_id'], $parent['id'], $datos, null);
    }
}

function index(){
//function index(ServerRequestInterface $request){

    global $dbconn;
    $dataRequest=[];
    
    if (isset($request)){
        $body = $request->getBody()->getContents();
        $dataRequest = json_decode($body, true);

        if(!isset($dataRequest['action'])){ echo 'No action defined in request'; die(); }
    
        if(!isset($dataRequest['auth_id'])){
            echo 'No AUTH ID defined in request';
            die();
        }

    }

    // conformamos el request

    $data = array(
        'action'=> isset($dataRequest['action']) ? $dataRequest['action']: 'get_linkedin_creas',
        'auth_id'=>isset($dataRequest['auth_id']) ? $dataRequest['auth_id']: '2ccd38ce-d929-11eb-8d81-ac1f6b17ff4a',
        'ad_account_id' => isset($dataRequest['ad_account_id']) ? $dataRequest['ad_account_id'] : '508170419',
        'platform_id'=> isset($dataRequest['platform_id']) ? $dataRequest['platform_id']: null,
        'period'=> isset($dataRequest['period']) ? $dataRequest['period'] : null, //mandatory para stats
        'start_date'=> isset($dataRequest['start_date']) ? $dataRequest['start_date'] : null, //opcional
        'end_date'=> isset($dataRequest['end_date']) ? $dataRequest['end_date'] : null //opcional
    );
    
    // {"action" : "get_accounts",  "auth_id": "2ccd2f2f-d929-11eb-8d81-ac1f6b17ff4a"}

    $query = "SELECT AUP.id, AUP.auths_user_id, AUP.user_id, AUP.platform, AUP.retorno, AUP.app_id, AUP.app_secret, AUP.platform_user_id,  AUP.access_token, AUP.refresh_token, U.name, U.email, U.customer_id_default, U.campaign_root_default  FROM auths_user_platform AUP JOIN users U ON (AUP.user_id = U.id)  WHERE AUP.public_id = '" . $data["auth_id"] . "';";
    $result = $dbconn->query($query);
    $infoCredentials = $result->fetch_assoc();

    //var_dump($infoCredentials);die();

    if (isset($infoCredentials['user_id'])){
        $user_id = $infoCredentials['user_id'];
    }else{
        echo 'No AUTH ID found';
        die();  
    }

    //$infoCredentials['access_token'] = refresh_token($infoCredentials);



    // to do - validate token
    // to do - refresh token 
    // if token largo = caduco
    //$query = "UPDATE auths_user_platform set auths_user_platform.activa = 'EXPIRATE' WHERE auths_user_platform.ID = '" . $infoCredentials['id'] . "'";
    //$dbconn->query($query);
    //redirect(/route/desactivar_token_por_Caducidad)

    if ($data['action']== null){
        echo 'NO ACTION SPECIFIED';
        die();
    }

    switch ($data['action']) {

        case "retrieve_all":        
            retrieve_entities_all($data, $infoCredentials, $user_id);
            break;

                case "retrieve_stats_all":
            
            $query = "SELECT account_id FROM app_thesoci_9c37.ads_accounts where platform='LINKEDIN' AND  auth_id in (select id from auths_user_platform where public_id = '".$data["auth_id"]."');";
            $result = $dbconn->query($query);
            
            while ($fila = $result->fetch_assoc()) {
                $event = array(  'subject_id' => $infoCredentials['id'], 'type' => 'get_stats_campaigns', 'function' => 'function-facebook-api',  'ad_account_id' =>  $fila['account_id'], 'period'=> '48H', 'nextcalls'=>['get_account_stats_adSet'=>['get_account_stats_ad'] ]);
                execGoogleTask($event);
               /* $event = array(  'subject_id' => $infoCredentials['id'], 'type' => 'get_stats_adSet', 'function' => 'function-facebook-api',  'ad_account_id' =>  $fila['account_id'] );
                execGoogleTask($event);
                $event = array(  'subject_id' => $infoCredentials['id'], 'type' => 'get_stats_ad', 'function' => 'function-facebook-api',  'ad_account_id' =>  $fila['account_id'] );
                execGoogleTask($event);
                */
            }
        
        
        case "get_accounts":
            get_accounts($infoCredentials);
            break;


        case "campaigns_get_linkedin":
            campaigns_get_linkedin($infoCredentials, $data);
            break;


            case "get_linkedin_atomos":
                get_linkedin_atomos($infoCredentials, $data);
                break;

            case "get_linkedin_creas":
                get_linkedin_creas($infoCredentials, $data);
                break;


        case "properties_linkedin":
            properties_linkedin($infoCredentials, $data);
            break;


        // STATS
        case "campaigns_stats_get_linkedin":
            campaigns_stats_get_linkedin($infoCredentials, $data);
            break;


        default:
            echo "NO ACTION";

    }

}



/*** 
 * 
 **** INFO Credentials --->

 
*/

index();

?>