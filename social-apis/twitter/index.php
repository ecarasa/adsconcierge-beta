<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'vendor/autoload.php';
include 'configs/general.php';
include 'configs/twitter.php';
include 'configs/stats_mapeometrics.php';

use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;
use Hborras\TwitterAdsSDK\TwitterAds\Campaign\Campaign;
use Hborras\TwitterAdsSDK\TwitterAds\Campaign\FundingInstrument;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\BadRequest;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\Forbidden;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotAuthorized;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\NotFound;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\RateLimit;
use Hborras\TwitterAdsSDK\TwitterAds\Creative\PromotedTweet;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServerError;
use Hborras\TwitterAdsSDK\TwitterAds\Errors\ServiceUnavailable;
use Hborras\TwitterAdsSDK\TwitterAdsException;

use Hborras\TwitterAdsSDK\TwitterAds\Enumerations;
use Hborras\TwitterAdsSDK\TwitterAds\Fields\AnalyticsFields;


use Google\Cloud\Scheduler\V1\AppEngineHttpTarget;
use Google\Cloud\Scheduler\V1\CloudSchedulerClient;
use Google\Cloud\Scheduler\V1\Job;
use Google\Cloud\Scheduler\V1\Job\State;

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Tasks\V2\CloudTasksClient;
use Google\Cloud\Tasks\V2\HttpMethod;
use Google\Cloud\Tasks\V2\HttpRequest;
use Google\Cloud\Tasks\V2\Task;

use Psr\Http\Message\ServerRequestInterface;

$DEBUG = false;

/* 
    - TWIITER ADS API - 
    POST 
    Request
    [action, auth_id, parent_platform_ids, platform_id, period ,star_date, end_date] 

    Orden de retrieve_entities_all
        - addaccount
        - campaign
        - adsets . atomos
        - creatividades

*/

function getAuthFrom_authId($id){

    global $dbconn;

    $query = "SELECT * FROM app_thesoci_9c37.auths_user_platform where id = '" . $id . "';";
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();

    return $row;

} 

function getAdAccount_PublicID($accountID){
    global $dbconn;

    $query = "SELECT * FROM app_thesoci_9c37.ads_accounts where account_id = '" . $accountID . "';";
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();

    return $row['public_id'];

}

function getAdAccount_Element($accountID){
    
    global $dbconn;

    $query = "SELECT * FROM app_thesoci_9c37.ads_accounts where account_id = '" . $accountID . "';";
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();

    return $row;

}

function persistAccount($accountItem, $infoCredentials){
   
    global $dbconn;

    $stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`ads_accounts` (`user_id`, `platform`, `app_id`, `account_id`, `name`, `platform_user_id`, `status`, `currency`, `metadata`, `customer_id`, `auth_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?) 	ON DUPLICATE KEY UPDATE `name` = ?, `status`= ?, `platform_user_id` = ?, `metadata` = ? ");
        
    $stmt->bind_param("isississsiissis", $infoCredentials["user_id"],  
                                        $infoCredentials["platform"], 
                                        $infoCredentials["app_id"], 
                                        $accountItem['id'], 
                                        $accountItem['name'],
                                        $infoCredentials['platform_user_id'], 
                                        $accountItem['approval_status'], 
                                        $accountItem['currency'], 
                                        $accountItem['metadata'],
                                        $infoCredentials["customer_id_default"], 
                                        $infoCredentials["id"], 
                                        $accountItem['name'],
                                        $accountItem['approval_status'], 
                                        $infoCredentials['platform_user_id'], 
                                        $accountItem['metadata']);

    try {
        
        $stmt->execute();

    } catch (Exception $e) {
        print_r($e);
    }    

}


// ENTITY

function get_accounts_and_properties($twitterapi, $data, $infoCredentials){

    global $dbconn_stats, $dbconn;
    
    echo 'Search AdsAccounts '. PHP_EOL;
    
    $accounts = $twitterapi->getAccounts();
    $arr = array();

    foreach ($accounts as $account) {

        $account->read();
        $funding = $account->getFundingInstruments();
        $adaccount_name=$account->getName();
        $ad_accountid= $account->getId();
        
        echo  'Id = ' .$account->getId() . ' | ' . $account->getName() . ', Currency '.$funding->getCollection()[0]->getCurrency() . PHP_EOL;
        
        $data = array(
            "id" => $account->getId(), "salt" => $account->getSalt(),  "name" => $account->getName(),
            "timezone" => $account->getTimezone(),  "timezone_switch_at" => $account->getTimezoneSwitchAt(),  "created_at" => $account->getCreatedAt(),
            "updated_at" => $account->getUpdatedAt(), "deleted" => $account->getDeleted(), "approval_status" => $account->getApprovalStatus(),
            "business_id" => $account->getBusinessId(), "business_name" => $account->getBusinessName(), "industry_type" => $account->getIndustryType(),
            "getPromotableUsers" => $account->getPromotableUsers(), "funding_instrument" => $funding->getCollection(), "currency" => $funding->getCollection()[0]->getCurrency(), "metadata" => null
        );
        
        persistAccount($data, $infoCredentials);

        //array_push($arr, array( "id" => $account->getId() ));
        unset($data );
        
        // 1 add acount 1 properti tipo PAGE
        $query = "SELECT id as adacc_id, public_id as adaccount_public_id 
                    FROM `app_thesoci_9c37`.`ads_accounts` 
                    WHERE auth_id = '{$infoCredentials["id"]}' 
                    and platform = '{$infoCredentials["platform"]}' 
                    and platform_user_id= '{$infoCredentials['platform_user_id']}' 
                    and account_id = '{$account->getId()}' limit 1 ;";
    
        $res_ac = $dbconn->query($query);
        $id_ac = $res_ac->fetch_assoc();
        
        $properties = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` (`type`, `user_id`, `platform`, `adaccount_id`, `app_id`,`platform_user_id`, `id_en_platform`, `token`,`name`,`status`, `category`, `currency`, `metadata`, `auth_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `name`= ?, `adaccount_id`= ? , `status`= ?, `platform_user_id`=?,  `metadata`= ? ");

        $pagename= $account->getName();  
        $page_id = $account->getId();

        $properties->bind_param("sssssssssssssssssss", ...[ "PAGE",
                                                            $infoCredentials['user_id'], 
                                                            $infoCredentials['platform'],
                                                            $id_ac["adacc_id"], 
                                                            $infoCredentials["app_id"], 
                                                            $infoCredentials['platform_user_id'], 
                                                            $account->getId(),
                                                            '-',
                                                            $pagename,
                                                            $account->getApprovalStatus(),
                                                            $account->getIndustryType(),
                                                            $funding->current()->getCurrency(),
                                                            json_encode(get_object_vars($account)),
                                                            $infoCredentials['id'],
                                                            $pagename, 
                                                            $id_ac["adacc_id"], 
                                                            $account->getApprovalStatus(), 
                                                            $infoCredentials['platform_user_id'], 
                                                            json_encode(get_object_vars($account))
                                                        ]);

        if(! $properties->execute() ){
            print_r("error insert properties account");
            //die();
        } // insert properties_accounts


        // PROPERTY TIPO FUNDINGINSTRUMENT
        $query = "SELECT id as pageid, public_id as property_public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE  auth_id = '{$infoCredentials["id"]}' and platform = '{$infoCredentials["platform"]}' and platform_user_id= '{$infoCredentials['platform_user_id']}' and id_en_platform = '{$page_id}'";
        $res_pa = $dbconn->query($query);
        $id_pa = $res_pa->fetch_assoc();   
        //  echo 'current '.   print_r( $funding->current() , true);
        
        foreach (  $funding->getCollection() as $item ) {
            
            $itemb = get_object_vars($item);
            
            $properties->bind_param("sssssssssssssssssss", ...[ "FUNDINGINSTRUMENT",
                                                        $infoCredentials['user_id'], 
                                                        $infoCredentials['platform'],
                                                        $id_ac["adacc_id"], 
                                                        $infoCredentials["app_id"], 
                                                        $infoCredentials['platform_user_id'], 
                                                        $item->getId(),
                                                        '-',
                                                        $item->getDescription(),
                                                        $itemb['entity_status'],
                                                        "-",
                                                        $item->getCurrency(),
                                                        json_encode(array($item,$itemb)),
                                                        $infoCredentials['id'],
                                                        $item->getDescription(), 
                                                        $id_ac["adacc_id"], 
                                                        $itemb['entity_status'], 
                                                        $infoCredentials['platform_user_id'], 
                                                        json_encode(array($item,$itemb))
                                                    ] );
            $properties->execute();
            if ( $properties->execute() ){
                echo 'FUNDINGINSTRUMENT ok'.PHP_EOL;
            }else{
                print_r("error insert FUNDINGINSTRUMENT account");
                //die();  
            }
        }


        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` 
                                    (`type`,`auth_id`, `ad_account`,  `ad_account_name`,`property_name`, `property_id`, `user_id`,`platform`,`auth_publicid`,   `user_publicid`,  `ad_account_publicid`,`property_publicid`) 
                                    VALUES ('PAGE', ?,?,?,?,?,?,?,?,?,?,?)
                                    ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");              
        
        $stmt3->bind_param("sssssssssssss", 
                        $infoCredentials["id"], 
                        $id_ac["adacc_id"] ,
                        $adaccount_name,
                        $pagename, 
                        $id_pa["pageid"],      
                        $infoCredentials['user_id'],
                        $infoCredentials["platform"],                   
                        $infoCredentials["auth_public_id"],
                     //   $infoCredentials["auths_user_id"],
                        $infoCredentials["user_public_id"],
                        $id_ac["adaccount_public_id"], 
                        $id_pa["property_public_id"],  
                        $adaccount_name, 
                        $pagename);
        
        if ( $stmt3->execute() ){
            echo 'Properties_adsaccount_relations added '.PHP_EOL;
        }else{
            print_r("error insert properties account");
            //die();  
        }



        $event = array( 'subject_id' => $infoCredentials['id'], 'type' => 'get_campaigns_and_atoms', 'function' => 'function-twitter-api', 'ad_account_id' => $account->getId() );
        //var_dump($event);
  
        execGoogleTask($event);

        //die();

    }

    //return $arr;
}

function get_campaigns_and_atoms($twiiterApi, $infoCredentials, $data){
   
    global $dbconn;

    $app_secret = json_decode($infoCredentials['app_secret']);
    $user_credentials = json_decode($infoCredentials['retorno']);

    try {

        echo 'Ad Account = ' . $data['ad_account_id'] . PHP_EOL. PHP_EOL;
        $account = new Account($data['ad_account_id']);


        $account->read();

      /*   $aaa = $account->getTwitterAds();
        var_dump($aaa);
        die();  */

        
        $campaigns = $account->getCampaigns('', [TwitterAds\Fields\CampaignFields::COUNT => 100]);
        $campaigns->setUseImplicitFetch(false);
        
        $campaignsData = [];
        $campaignLineitems=[];	 
                    
        $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (   user_id, 
                                                                            platform, 
                                                                            app_id, 
                                                                            auth_id, 
                                                                            name, 
                                                                            id_en_platform, 
                                                                            status, 
                                                                            currency, 
                                                                            metadata, 
                                                                            ad_account, 
                                                                            campana_root, 
                                                                            customer_id, 
                                                                            source)
                                        values (?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id = ? and ads_accounts.user_id = ? ), ?,?, 'IMPORTED')
                                        on duplicate key update status = ?, metadata = ?, campana_root = ?, customer_id = ?");

        //$cuentaTwBBDD = getAdAccount_Element($data['ad_account_id']);
        
        foreach ($campaigns as $campaign) {
            
            $campaignsData[] = $campaign;            
            echo 'ID Campaign = '. $campaign->getId() . ' - Nombre = ' .$campaign->getName() . PHP_EOL;
            
            $infoCredentials['platform'] = 3; //for twitter.
            $campaignLineitems[$campaign->getId()] = $campaign->getLineItems([TwitterAds\Fields\LineItemFields::COUNT => 200]);

            $stmtcampana->bind_param("sssssssssssssssss", ...[  $infoCredentials['user_id'],
                                                                $infoCredentials['platform'],
                                                                $infoCredentials['app_id'], 
                                                                $infoCredentials['id'],
                                                                $campaign->getName(), 
                                                                $campaign->getId(),
                                                                unificastatus('campana', $infoCredentials['platform'], $campaign->getEntityStatus(), ['estado'=> $campaign->getEntityStatus(), 'otros'=> $campaign->getReasonsNotServable(), 'record'=>$campaign ] ), 
                                                                $campaign->getCurrency(),
                                                                json_encode($campaign), 
                                                                $data['ad_account_id'], 
                                                                $infoCredentials['user_id'], 
                                                                isset($infoCredentials["campaign_root_default"]) ? $infoCredentials["campaign_root_default"] : '0', 
                                                                $infoCredentials["customer_id_default"], 
                                                                unificastatus('campana', $infoCredentials['platform'], $campaign->getEntityStatus(), ['estado'=> $campaign->getEntityStatus(), 'otros'=> $campaign->getReasonsNotServable(), 'record'=>$campaign ] ), 
                                                                json_encode($campaign), 
                                                                isset($infoCredentials["campaign_root_default"]) ? $infoCredentials["campaign_root_default"] : '0', 
                                                                $infoCredentials["customer_id_default"] ]);
            $stmtcampana->execute();

            $stmt_atomo_tw = $dbconn->prepare("INSERT INTO campaigns_platform_atomo 
                                                (auth_id, campana_root_id, user_id, platform, app_id, id_en_platform, name, objetivo, platform_placements, status, 
                                                chargeby, bid_unit, puja_valor, optimizacion, bid_type,
                                                product_type, budget_total, targeting_include, metadata, campana_platform_id) 
            values (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, (SELECT campaigns_platform.id FROM campaigns_platform WHERE campaigns_platform.id_en_platform=? and   campaigns_platform.user_id=? )) on duplicate key update metadata = ? ;");

            $stmtadset = $dbconn->prepare("INSERT INTO creatividades 
                                ( id_en_platform, 
                                user_id, 
                                customer_id, 
                                name,  
                                platform, 
                                campana_root, 
                                campana_platform_id, 
                                atomo_id, 
                                platform_status, 
                                metadata, 
                                source ) 
                                values ( ?, ?, ?, ?, ?, ?,
                                (SELECT id FROM campaigns_platform WHERE campaigns_platform.id_en_platform = ? and campaigns_platform.user_id = ? LIMIT 1), 
                                (SELECT id FROM campaigns_platform_atomo WHERE id_en_platform = ? and user_id = ? LIMIT 1),
                                ?,?, 'IMPORTED') 
                                on duplicate key update platform_status=?, metadata=?");

            
            foreach ( $campaignLineitems[$campaign->getId()] as $lineItem) {
        
                echo  'Atomo added = '. $lineItem->getId() . ' Name: ' . $lineItem->getName().PHP_EOL;

                try {

                    $stmt_atomo_tw->bind_param("iiiissssssssssssssssss", ...[   
                                                                        $infoCredentials['id'],
                                                                        $infoCredentials['campaign_root_default'],
                                                                        $infoCredentials['user_id'],
                                                                        $infoCredentials['platform'],
                                                                        $infoCredentials['app_id'],
                                                                        $lineItem->getId(),
                                                                        $lineItem->getName(),
                                                                        $lineItem->getObjective(),
                                                                        json_encode($lineItem->getPlacements()),
                                                                        $lineItem->getEntityStatus(),
                                                                        $lineItem->getChargeBy(),
                                                                        $lineItem->getBidUnit(),
                                                                        ($lineItem->getBidAmountLocalMicro() / 1000000),
                                                                        $lineItem->getOptimization(),
                                                                        $lineItem->getBidType(),
                                                                        $lineItem->getProductType(),
                                                                        ($lineItem->getTotalBudgetAmountLocalMicro()/ 1000000),
                                                                        json_encode($lineItem->getTargetingCriteria()),
                                                                        json_encode($lineItem),
                                                                        $campaign->getId(),
                                                                        $infoCredentials['user_id'],
                                                                        json_encode($lineItem)]);
                    $stmt_atomo_tw->execute();
                
                } catch (exception $e) {
                    echo "Error DB: " . $e->getMessage();
                    //die();
                }

                // creatividades 

                $promotedtweets = $lineItem->getPromotedTweets();
               
                foreach ($promotedtweets as $pTweet) {

                    $tweet = $pTweet->read();
                    $dataCrea = $tweet->toParams();
                    
                    $stmtadset->bind_param( "sisssssisissss", ...[ 
                                                                    $dataCrea['tweet_id'],
                                                                    $infoCredentials['user_id'],
                                                                    $infoCredentials["customer_id_default"],
                                                                    '', // twiter va vacio
                                                                    $infoCredentials['platform'],
                                                                    $infoCredentials["campaign_root_default"],

                                                                    $campaign->getId(),  // atomo id
                                                                    $infoCredentials['user_id'],

                                                                    $lineItem->getId(),
                                                                    $infoCredentials['user_id'], 

                                                                    $dataCrea['entity_status'],
                                                                    json_encode($dataCrea),
                                                                    $dataCrea['entity_status'],
                                                                    json_encode($dataCrea)
                                                                ]);
                    
                    $stmtadset->execute();

                }
                            
            }

            if ($stmtcampana->error != "") {
                var_dump($stmtcampana);
            }
        }

    } catch (exception $e) {
        echo "Error api twitter: " . $e->getMessage();
        //die();
    }


}

/* function get_creatives($twiiterApi, $infoCredentials, $data){
   
    global $dbconn;

    $app_secret = json_decode($infoCredentials['app_secret']);
    $user_credentials = json_decode($infoCredentials['retorno']);

    try {

        echo 'Ad Account = ' . $data['ad_account_id'] . PHP_EOL. PHP_EOL;
       
        $account = new Account($data['ad_account_id']);
        $account->read();
        
        $campaigns = $account->getPromotedTweet('', []);
        $campaigns->setUseImplicitFetch(false);

   
        
        $campaignsData = [];
        $campaignLineitems=[];	 
                    
        $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (   user_id, 
                                                                            platform, 
                                                                            app_id, 
                                                                            auth_id, 
                                                                            name, 
                                                                            id_en_platform, 
                                                                            status, 
                                                                            currency, 
                                                                            metadata, 
                                                                            ad_account, 
                                                                            campana_root, 
                                                                            customer_id, 
                                                                            source)
                                        values (?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id = ? and ads_accounts.user_id = ? ), ?,?, 'IMPORTED')
                                        on duplicate key update status = ?, metadata = ?, campana_root = ?, customer_id = ?");

        //$cuentaTwBBDD = getAdAccount_Element($data['ad_account_id']);
        
        foreach ($campaigns as $campaign) {
            
            $campaignsData[] = $campaign;            
            echo 'ID Campaign = '. $campaign->getId() . ' - Nombre = ' .$campaign->getName() . PHP_EOL;
            
            $infoCredentials['platform'] = 3; //for twitter.
            $campaignLineitems[$campaign->getId()] = $campaign->getLineItems([TwitterAds\Fields\LineItemFields::COUNT => 200]);

            $stmtcampana->bind_param("sssssssssssssssss", ...[  $infoCredentials['user_id'],
                                                                $infoCredentials['platform'],
                                                                $infoCredentials['app_id'], 
                                                                $infoCredentials['id'],
                                                                $campaign->getName(), 
                                                                $campaign->getId(),
                                                                unificastatus('campana', $infoCredentials['platform'], $campaign->getEntityStatus(), ['estado'=> $campaign->getEntityStatus(), 'otros'=> $campaign->getReasonsNotServable(), 'record'=>$campaign ] ), 
                                                                $campaign->getCurrency(),
                                                                json_encode($campaign), 
                                                                $data['ad_account_id'], 
                                                                $infoCredentials['user_id'], 
                                                                isset($infoCredentials["campaign_root_default"]) ? $infoCredentials["campaign_root_default"] : '0', 
                                                                $infoCredentials["customer_id_default"], 
                                                                unificastatus('campana', $infoCredentials['platform'], $campaign->getEntityStatus(), ['estado'=> $campaign->getEntityStatus(), 'otros'=> $campaign->getReasonsNotServable(), 'record'=>$campaign ] ), 
                                                                json_encode($campaign), 
                                                                isset($infoCredentials["campaign_root_default"]) ? $infoCredentials["campaign_root_default"] : '0', 
                                                                $infoCredentials["customer_id_default"] ]);
            $stmtcampana->execute();

            $stmt_atomo_tw = $dbconn->prepare("INSERT INTO campaigns_platform_atomo 
                                                (auth_id, campana_root_id, user_id, platform, app_id, id_en_platform, name, objetivo, platform_placements, status, 
                                                chargeby, bid_unit, puja_valor, optimizacion, bid_type,
                                                product_type, budget_total, targeting_include, metadata, campana_platform_id) 
            values (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, (SELECT campaigns_platform.id FROM campaigns_platform WHERE campaigns_platform.id_en_platform=? and   campaigns_platform.user_id=? )) on duplicate key update metadata = ? ;");


            foreach ( $campaignLineitems[$campaign->getId()] as $lineItem) {
        

              

                echo  'Atomo added = '. $lineItem->getId() . ' Name: ' . $lineItem->getName().PHP_EOL;

                try {

                    $stmt_atomo_tw->bind_param("iiiissssssssssssssssss", ...[   
                                                                        $infoCredentials['id'],
                                                                        $infoCredentials['campaign_root_default'],
                                                                        $infoCredentials['user_id'],
                                                                        $infoCredentials['platform'],
                                                                        $infoCredentials['app_id'],
                                                                        $lineItem->getId(),
                                                                        $lineItem->getName(),
                                                                        $lineItem->getObjective(),
                                                                        json_encode($lineItem->getPlacements()),
                                                                        $lineItem->getEntityStatus(),
                                                                        $lineItem->getChargeBy(),
                                                                        $lineItem->getBidUnit(),
                                                                        ($lineItem->getBidAmountLocalMicro() / 1000000),
                                                                        $lineItem->getOptimization(),
                                                                        $lineItem->getBidType(),
                                                                        $lineItem->getProductType(),
                                                                        ($lineItem->getTotalBudgetAmountLocalMicro()/ 1000000),
                                                                        json_encode($lineItem->getTargetingCriteria()),
                                                                        json_encode($lineItem),
                                                                        $campaign->getId(),
                                                                        $infoCredentials['user_id'],
                                                                        json_encode($lineItem)]);
                    $stmt_atomo_tw->execute();

                    // lala
               
                } catch (exception $e) {
                    echo "Error DB: " . $e->getMessage();
                    //die();
                }
                            
                }

            if ($stmtcampana->error != "") {
                var_dump($stmtcampana);
            }
        }

    } catch (exception $e) {
        echo "Error api twitter: " . $e->getMessage();
        //die();
    }

} 

function getAddAccountFromCampaign($campaignId){
  
    global $dbconn;

    $query = "SELECT * FROM app_thesoci_9c37.ads_accounts JOIN app_thesoci_9c37.campaigns_platform on ads_accounts.id = campaigns_platform.ad_account WHERE campaigns_platform.id_en_platform = '" . $campaignId . "';";
    
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();

    return $row;

}
 */

// STATS helpers
function cleanConversions($stat)
{
    foreach ($stat as $key => $value) {
        if (strpos($key, 'conversion') !== false) {
            $checkEmpty = (object)array_filter((array)$stat->{$key});
            if ((array)$checkEmpty) {
                $stat->{$key} = $value;
            } else {
                unset ($stat->{$key});
            }
        }
    }
    return $stat;
}

function twitter_stats_generic($userid, $accountw, $kindentity, $itemsid_en_pl, $granular, $startDate, $endDate, $campaignsData = null)
{

    $dates = dateRanges($startDate, $endDate, $granular);
    //print_r($dates);die;

    $metricas = [];


    foreach ($dates as $date) {

        if (is_string($itemsid_en_pl)) {
            $items = [$itemsid_en_pl];
        } else {
            $items = $itemsid_en_pl;
        }

        try {
            $stats = $accountw->all_stats(
                $items,
                array(
                    AnalyticsFields::METRIC_GROUPS_BILLING,
                    AnalyticsFields::METRIC_GROUPS_VIDEO,
                    AnalyticsFields::METRIC_GROUPS_MEDIA,
                    AnalyticsFields::METRIC_GROUPS_WEB_CONVERSIONS,
                    AnalyticsFields::METRIC_GROUPS_MOBILE_CONVERSION,
                    AnalyticsFields::METRIC_GROUPS_ENGAGEMENT
                ),
                array(
                    AnalyticsFields::ENTITY => $kindentity,
                    AnalyticsFields::START_TIME => $date[0],
                    AnalyticsFields::END_TIME => $date[1],
                    AnalyticsFields::GRANULARITY => Enumerations::GRANULARITY_TOTAL
                )
            );

        } catch (exception $e) {
            echo "Error: " . $e->getMessage();
            continue;
        }

        foreach ($stats as $statsitem) {

            $statsData = cleanConversions($statsitem->id_data[0]->metrics);
            $name = $campaignsData[$statsitem->id]['name'];
            if ($kindentity == AnalyticsFields::PROMOTED_TWEET)
                $name = $campaignsData[$statsitem->id]['ad_name'];

            $metricas[] = array_merge([
                'entity' => $kindentity,
                'startdate' => $date[0]->format('Y-m-d H:i:s'),
                'enddate' => $date[1]->format('Y-m-d H:i:s'),
                'id_in_platform' => $statsitem->id,
                'id' => null,
                strtolower($kindentity) . '_name' => $name,
                'currency' => (isset($campaignsData[$statsitem->id]['currency']) ? $campaignsData[$statsitem->id]['currency'] : null),
            ], (array)$statsData);
        }

    }

    return $metricas;
}

function helper_metrics_keytranslator($platformid, $tipoinput, $inputraw, $outputrow, $metadata = null)
{
    global $mapeometricas;

    //var_dump($inputraw);
    //echo PHP_EOL;
    // die();

    switch ($platformid) {
        case 1:
        case "FACEBOOK":
            switch ($tipoinput) {
                case 'campana':
                    $outputrow['id_in_platform'] = $inputraw['campaign_id'];
                    break;
                case 'ad':
                    $outputrow['id_in_platform'] = $inputraw['ad_id'];
                    $outputrow['lineitem_id'] = $inputraw['adset_id'];

                    break;
                case 'lineitem':
                    $outputrow['id_in_platform'] = $inputraw['adset_id'];
                    $outputrow['lineitem_id'] = $inputraw['adset_id'];

                    break;

            }
            $outputrow['date'] = $inputraw['date_start'];
            $outputrow['yearmonth'] = date('Y-m-01', strtotime($inputraw['date_start']));
            $outputrow['platformid'] = 1;
            switch ($inputraw['publisher_platform']) {
                case 'facebook':
                    $outputrow['platformid'] = 1;
                    break;
                case 'instagram':
                    $outputrow['platformid'] = 4;
                    break;
            }
            //https://developers.facebook.com/docs/marketing-api/insights/parameters/v9.0
            $outputrow['video_starts'] = $inputraw['video_play_actions'];
            $outputrow['video_views'] = $inputraw['video_play_actions'];
            $outputrow['video_completes'] = $inputraw['video_p100_watched_actions'];
            $outputrow['video_25'] = $inputraw['video_p25_watched_actions'];
            $outputrow['video_50'] = $inputraw['video_p50_watched_actions'];
            $outputrow['video_75'] = $inputraw['video_p75_watched_actions'];

            foreach ($inputraw as $clave => $valor) {
                if ($valor == null) {
                    continue;
                }
                $clave = strtolower($clave);
                $clavemapeo = isset($mapeometricas[$platformid][$clave]) ? $mapeometricas[$platformid][$clave] : (isset($mapeometricas['default'][$clave]) ? $mapeometricas['default'][$clave] : ['metrics_rest']);

                foreach ($clavemapeo as $claveout) {
                    if (is_numeric($outputrow[$claveout])) {
                        $outputrow[$claveout] = $outputrow[$claveout] + $valor;
                    } elseif (is_string($outputrow[$claveout])) {
                        $outputrow[$claveout] = $valor;
                    } else {
                        $outputrow[$claveout][$clave] = $valor;
                    }
                }

            }
            //die();
            break;
        case 2:
        case "LINKEDIN":

            $outputrow['id_in_platform'] = explode(':', $inputraw['pivotValue']);
            $outputrow['id_in_platform'] = $outputrow['id_in_platform'][3];
            $outputrow['platformid'] = 2;
            $outputrow['date'] = $inputraw['dateRange']['start']['year'] . '-' . $inputraw['dateRange']['start']['month'] . '-' . $inputraw['dateRange']['start']['day'];
            $outputrow['yearmonth'] = $inputraw['dateRange']['start']['year'] . '-' . $inputraw['dateRange']['start']['month'] . '-01';
            $outputrow['campananame'] = (isset($metadata['campaignsData'][$inputraw['pivotValue']]) ? $metadata['campaignsData'][$inputraw['pivotValue']]['name'] : null);
            $outputrow['currency'] = (isset($metadata['campaignsData'][$inputraw['pivotValue']]['totalBudget']) ? $metadata['campaignsData'][$inputraw['pivotValue']]['totalBudget']['currencyCode'] : $metadata['currency']);
            $outputrow['impressions'] = $inputraw['impressions'];
            $outputrow['clicks'] = $inputraw['clicks'];
            $outputrow['cost'] = $inputraw['costInLocalCurrency'];
            $outputrow['reach'] = $inputraw['approximateUniqueImpressions'];
            $outputrow['video_starts'] = $inputraw['videoStarts'];
            $outputrow['video_views'] = $inputraw['videoStarts'];
            $outputrow['video_completes'] = $inputraw['videoCompletions'];
            $outputrow['video_25'] = $inputraw['videoFirstQuartileCompletions'];
            $outputrow['video_50'] = $inputraw['videoMidpointCompletions'];
            $outputrow['video_75'] = $inputraw['videoThirdQuartileCompletions'];
            $outputrow['engagements'] = $inputraw['totalEngagements'] - $inputraw['clicks'];

            foreach ($inputraw as $clave => $valor) {
                $clave = strtolower($clave);
                $clavemapeo = isset($mapeometricas[$platformid][$clave]) ? $mapeometricas[$platformid][$clave] : (isset($mapeometricas['default'][$clave]) ? $mapeometricas['default'][$clave] : ['metrics_rest']);
                foreach ($clavemapeo as $claveout) {
                    if (is_numeric($outputrow[$claveout])) {
                        $outputrow[$claveout] = $outputrow[$claveout] + $valor;
                    } else {
                        $outputrow[$claveout][$clave] = $valor;
                    }
                }
            }

            break;
        case 3:
        case "TWITTER":

            $outputrow['date'] = date('Y-m-d', strtotime($inputraw['startdate']));
            $outputrow['yearmonth'] = date('Y-m-01', strtotime($inputraw['startdate']));
            $outputrow['id_in_platform'] = $inputraw['id_in_platform'];
            $outputrow['campananame'] = isset($inputraw['campaign_name']) ? $inputraw['campaign_name'] : '';
            $outputrow['platformid'] = 3;
            $outputrow['video_starts'] = $inputraw['video_content_starts'];
            $outputrow['video_completes'] = $inputraw['video_views_100'];
            $outputrow['video_25'] = $inputraw['video_views_25'];
            $outputrow['video_50'] = $inputraw['video_views_50'];
            $outputrow['video_75'] = $inputraw['video_views_75'];
            $outputrow['reach'] = is_array($inputraw['impressions']) ? $inputraw['impressions'][0] : 0;

            foreach ($inputraw as $clave => $valor) {
                if (is_array($valor)) {
                    $valor = $valor[0];
                }
                if ($valor == null) {
                    continue;
                }
                $clave = strtolower($clave);
                $clavemapeo = isset($mapeometricas[$platformid][$clave]) ? $mapeometricas[$platformid][$clave] : (isset($mapeometricas['default'][$clave]) ? $mapeometricas['default'][$clave] : ['metrics_rest']);
                foreach ($clavemapeo as $claveout) {
                    if (is_numeric($outputrow[$claveout])) {
                        $outputrow[$claveout] = $outputrow[$claveout] + $valor;
                    } elseif (is_string($outputrow[$claveout])) {
                        $outputrow[$claveout] = $valor;
                    } else {
                        $outputrow[$claveout][$clave] = $valor;
                    }
                }

            }
            $outputrow['currency'] = $outputrow['currency']['currency'];
            $outputrow['cost'] = round(($outputrow['cost'] / 1000000), 4);
            $outputrow['engagements'] = $outputrow['engagements'] - $outputrow['clicks'];

            break;
    }

    return $outputrow;
}

function helper_metrics_campana_day($platformid, $user_id, $customer_id = 0, $datos, $ad_account_id, $ad_account_id_platform, $metadata = ['currency' => null], $job_id = "")
{
    global $dbconn_stats, $dbconn;

    if (!isset($metadata['currency'])) {
        $metadata['currency'] = null;
    }
    if (!is_array($datos)) {
        return null;
    }

    echo ' >> STATS to insert/update ' . count($datos) . ' elements : ';
    $i = 1;

    foreach ($datos as $inputraw) {

         echo " **";

        $outputrow = ['id_in_platform' => 0, 'campananame' => '', 'platformid' => $platformid, 'date' => '', 'metrics_delivery' => [], 'metrics_costs' => [], 'metrics_engagement' => [], 'metrics_video' => [], 'metrics_conversion' => [], 'metrics_rest' => [], 'cost' => 0, 'impressions' => 0, 'reach' => 0, 'clicks' => 0, 'engagements' => 0, 'video_views' => 0, 'video_starts' => 0, 'currency' => $metadata['currency'], 'conversions' => 0];
        $outputrow = helper_metrics_keytranslator($platformid, 'campana', $inputraw, $outputrow, $metadata);

        //var_dump($outputrow);die();
        //echo "** impressions: " .  $outputrow['impressions'];
        //echo " ** clicks: " .  $outputrow['clicks'] . PHP_EOL;
        
        if ($outputrow['impressions'] == 0 && $outputrow['video_views'] == 0 && $outputrow['reach'] == 0 && $outputrow['clicks'] == 0 && $outputrow['engagements'] == 0) {
            //continue;
        }

        $sql = "INSERT INTO adsconcierge_stats.platform_campana_day ( user_id, platformid, customer_id, ad_account_id, campanaid,
                                                idenplatform, campananame, adccountid_pl, dia, unico,
                                                currency, metrics_delivery, metrics_costs, metrics_engagement, metrics_video,
                                                metrics_conversion, metrics_rest, cost, impressions, reach,
                                                clicks, engagements, video_views, conversions, plataforma,
                                                campanaroot, yearweek, yearmonth )
                                SELECT
                                    {$user_id},
                                    '{$outputrow['platformid']}',
                                    '{$customer_id}',
                                    '{$ad_account_id}',
                                    cp.id,
                                    '{$outputrow['id_in_platform']}',
                                    cp.name,
                                    '{$ad_account_id_platform}',
                                    '{$outputrow['date']}',
                                    '" . md5($outputrow['platformid'] . $outputrow['id_in_platform'] . $outputrow['date'] . $user_id) . "',
                                    '{$outputrow['currency']}',
                                    '" . json_encode($outputrow['metrics_delivery']) . "',
                                    '" . json_encode($outputrow['metrics_costs']) . "',
                                    '" . json_encode($outputrow['metrics_engagement']) . "',
                                    '" . json_encode($outputrow['metrics_video']) . "',
                                    '" . json_encode($outputrow['metrics_conversion']) . "',
                                    '" . $dbconn->real_escape_string(json_encode($outputrow['metrics_rest'])) . "',
                                    '{$outputrow['cost']}', '{$outputrow['impressions']}',
                                    '{$outputrow['reach']}',
                                    '{$outputrow['clicks']}', '{$outputrow['engagements']}',
                                    '" . intval($outputrow['video_views']) . "',
                                    '{$outputrow['conversions']}',
                                    '{$platformid}',
                                    cp.campana_root,
                                    YEARWEEK('{$outputrow['date']}'),
                                    date_format('{$outputrow['date']}', '%Y-%m')
                                FROM
                                    app_thesoci_9c37.campaigns_platform cp
                                WHERE
                                    cp.id_en_platform = '{$outputrow['id_in_platform']}' AND cp.user_id = {$user_id}
                                ON DUPLICATE KEY UPDATE
                                    cost                = '{$outputrow['cost']}',
                                    impressions         = '{$outputrow['impressions']}' ,
                                    reach               = '{$outputrow['reach']}',
                                    clicks              = '{$outputrow['clicks']}',
                                    engagements         = '{$outputrow['engagements']}',
                                    video_views         = '" . intval($outputrow['video_views']) . "',
                                    video_starts        = '" . intval($outputrow['video_starts']) . "',
                                    video_completes     = '" . intval($outputrow['video_completes']) . "',
                                    video_25            = '" . intval($outputrow['video_25']) . "',
                                    video_50            = '" . intval($outputrow['video_50']) . "',
                                    video_75            = '" . intval($outputrow['video_75']) . "',
                                    conversions         = '{$outputrow['conversions']}',
	                                metrics_delivery    = '" . json_encode($outputrow['metrics_delivery']) . "',
	                                metrics_costs       = '" . json_encode($outputrow['metrics_costs']) . "',
	                                metrics_engagement  = '" . json_encode($outputrow['metrics_engagement']) . "',
	                                metrics_video       = '" . json_encode($outputrow['metrics_video']) . "',
	                                metrics_conversion  = '" . json_encode($outputrow['metrics_conversion']) . "',
                                    metrics_rest        = '" . $dbconn->real_escape_string(json_encode($outputrow['metrics_rest'])) . "'";

        if (!$dbconn_stats->query($sql)) {
            printf("Error: %s\n", $dbconn_stats->error);
            //echo PHP_EOL . $sql;
        }

    }

    //echo PHP_EOL . ' DB CampañaxDia data updated > ';
    //$sql = "UPDATE adsconcierge_stats.background_job SET status = 'finished' WHERE id = {$job_id}";
    //$db->query($sql);

}

// STATS functions
function stats_campana_xaccount($twiiterApi, $infoCredentials, $data){

        try {

           $cuentaTwBBDD = getAdAccount_Element($data['ad_account_id']);

            echo 'AdAccount Twitter - ' . $data['ad_account_id'] . PHP_EOL;

            $accountw = new Account($cuentaTwBBDD['account_id']);
            $accountw->read();

            $getFundingInstruments = $accountw->getFundingInstruments();

            $campaigns = $accountw->getCampaigns('');

            $campaigns->setUseImplicitFetch(true);

            $campaignsIds = array();
            $campaignsData = array();
            $campStruct = array();

            foreach ($campaigns as $campaign) {

                if ($campaign->effective_status != 'RUNNING') {
                    //continue;
                }

                $campaignsIds[] = $campaign->getId();
                
                $campaignsData[$campaign->getId()] = array(
                                                            'id' => $campaign->getId(),
                                                            'name' => $campaign->getName(),
                                                            'currency' => $campaign->getCurrency(),
                                                            'status' => $campaign->effective_status
                                                        );

                if (count($campaignsIds) == 20) {
                    $campStruct[] = array(
                        'campaignsIds' => $campaignsIds,
                        'campaignsData' => $campaignsData
                    );
                    $campaignsIds = array();
                    $campaignsData = array();
                }
            }
            
            if (count($campaignsIds) > 0) {
                $campStruct[] = array(
                    'campaignsIds' => $campaignsIds,
                    'campaignsData' => $campaignsData
                );
            }

            $datos = array();

            foreach ($campStruct as $val) {

                $temp = twitter_stats_generic($infoCredentials['user_id'], $accountw, AnalyticsFields::CAMPAIGN, $val['campaignsIds'], 'P1D', $data['start_date'], $data['end_date'], $val['campaignsData']);

                if (count($datos) > 0) {
                    $datos = array_merge($datos, $temp);
                } else {
                    $datos = $temp;
                }
            }

            helper_metrics_campana_day( $infoCredentials['platform'], 
                                        $infoCredentials['user_id'], 
                                        $infoCredentials['customer_id_default'],
                                        $datos,
                                        $cuentaTwBBDD['id'], 
                                        $cuentaTwBBDD['account_id'], 
                                        $infoCredentials );

        } catch (exception $e) {
            echo "Error api twitter: " . $e->getMessage();
            //die();
        }

}
/* 
function stats_atomo_xaccount($twitterapi, $infoCredentials, $data)
{
    
        try {
        
            $datos = array();
            
            //foreach ($lineitemsStruct as $val) {
            foreach ($data['parent_platform_ids'] as $var) {

                $cuentaTwBBDD = getAdAccount_Element($var['id']);
                echo 'AdAccount Twitter - ' . $cuentaTwBBDD['account_id'] . PHP_EOL;

                $accountw = new Account($cuentaTwBBDD['account_id']);

                $temp = twitter_stats_generic($infoCredentials['user_id'], $accountw, AnalyticsFields::LINE_ITEM, $val['id'], 'P1D', $data['start_date'], $data['start_date']);
                
                if (count($datos) > 0) {
                    $datos = array_merge($datos, $temp);
                } else {
                    $datos = $temp;
                }
            }



        } catch (exception $e) {
            echo "Error api twitter: " . $e->getMessage();
            //die();
        }
    
    
    

    return $datos;
}

function stats_ads_xaccount($userid, $accountw, $itemsid_en_pl = null, $startDate, $endDate, $adsStruct)
{

    $datos = array();
    foreach ($adsStruct as $val) {
        $temp = twitter_stats_generic($userid, $accountw, AnalyticsFields::PROMOTED_TWEET, $val, 'P1D', $startDate, $endDate);
        if (count($datos) > 0) {
            $datos = array_merge($datos, $temp);
        } else {
            $datos = $temp;
        }
    }

    return $datos;
}

function stats_ad_xaccount($userid, $accountw, $itemsid_en_pl, $startDate, $endDate)
{
    $getFundingInstruments = $accountw->getFundingInstruments();

    $lineitems = $accountw->getLineItems('');
    $lineitems->setUseImplicitFetch(true);
    foreach ($lineitems as $lineitem) {
        $lineitemid = $lineitem->getId();
        $CampaignId = $lineitem->getCampaignId();

        foreach ($lineitem->getPromotedTweets() as $item) {
            $itemsIds[] = $item->getId();
            $itemsData[$item->getId()] = ['id' => $item->getId(), 'name' => $item->getTweetId(), 'lineitem_id' => $lineitem->getId(), 'campaign_id' => $lineitem->getCampaignId(), 'currency' => $lineitem->getCurrency()];
        }
    }

    $metricas = twitter_stats_generic($userid, $accountw, AnalyticsFields::PROMOTED_TWEET, $itemsIds, 'P1D', $startDate, $endDate, $itemsData);


    return $metricas;
} */

function execGoogleTask($tarea)
{

    Global $DEBUG;
//print('execGoogleTask');
    if ($DEBUG == false) {
        try {
            $projectId = 'adsconcierge';
            $locationId = 'us-central1';
            $colaTrabajo = 'twitter-sync';
                    
            $interval = isset($tarea['Execution_interval']) ? substr($tarea['Execution_interval'], 0, 2) : '12H';
            $queueId = isset($tarea['cola_trabajo']) ? $tarea['cola_trabajo'] : $colaTrabajo;

            //https://us-central1-adsconcierge.cloudfunctions.net/function-facebook-api
            $url = 'https://' . $locationId . '-' . $projectId . '.cloudfunctions.net/' . $tarea['function'];

            $auth = getAuthFrom_authId($tarea['subject_id']);
            $auth_id = $auth['public_id'];
            $user_id = $auth['user_id'];
            
            $payload = array(   "auth_id" => $auth_id,  "action" => $tarea['type'], "ad_account_id" => $tarea['ad_account_id'] );
            echo json_encode($payload).PHP_EOL;
            
            // Instantiate the client and queue name.
            $client = new CloudTasksClient(['credentials' => __DIR__ . '/configGoogleCloud.json', 'projectId' => $projectId ]);
            $queueName = $client->queueName($projectId, $locationId, $queueId);

            // Create an Http Request Object.
            $httpRequest = new HttpRequest();
            $httpRequest->setUrl($url);
            $httpRequest->setHttpMethod(HttpMethod::POST);
                
            if (isset($payload)) {
                $httpRequest->setBody(json_encode($payload));
            }
//print('Task ');     echo json_encode($tarea).PHP_EOL;
            $task = new Task();
            $task->setHttpRequest($httpRequest);

            $response = $client->createTask($queueName, $task);
          print('Task creada');
            echo 'Tarea creada .. '. $response->getName() . PHP_EOL;
            //$this->persistGCloudTask_db($user_id, $auth_id, '', $interval, $payload['action'], json_encode($payload), str_replace("projects/adsconcierge/locations/us-central1/queues/ads-concierge-sync/tasks/", "", $response->getName()));

        } catch (Exception $e) {
            print_r($e);
        }
    }else{
        echo 'Task will be created on DEBUG == false'.PHP_EOL;
        echo json_encode($tarea).PHP_EOL;
    }
  return true;
}


// codigo para probar
//function index(){

//function index(ServerRequestInterface $request){
 
    global $dbconn;
    $dataRequest=[];
 
    if (isset($request)){
        $body = $request->getBody()->getContents();
        $dataRequest = json_decode($body, true);
        if(!isset($dataRequest['action'])){ echo 'No action defined in request'; die(); }
        if(!isset($dataRequest['auth_id'])){ echo 'No AUTH ID defined in request'; die(); }
    }
  //** codigo a descomentar para test por cli
$dataRequest=['action' => 'retrieve_all', 'auth_id'=> '61c5fda2-0057-11ec-8d81-ac1f6b17ff4a'];
$dataRequest=['action' => 'retrieve_stats_all', 'auth_id'=> '61c5fda2-0057-11ec-8d81-ac1f6b17ff4a'];
             
    // conformamos el request
    $data = array(
        'action'=> isset($dataRequest['action']) ? $dataRequest['action'] : null,
        'auth_id'=>isset($dataRequest['auth_id']) ? $dataRequest['auth_id']: null,
        'ad_account_id' => isset($dataRequest['ad_account_id']) ? $dataRequest['ad_account_id'] : null, //ads_accounts
        
        //'platform_id'=> isset($dataRequest['platform_id']) ? $dataRequest['platform_id']: null,
        //'period'=> isset($dataRequest['period']) ? $dataRequest['period'] : null, //mandatory para stats
        'start_date'=> isset($dataRequest['start_date']) ? $dataRequest['start_date'] : date('Y-m-d', strtotime(' - 1 day')), //opcional
        'end_date'=> isset($dataRequest['end_date']) ? $dataRequest['end_date'] : date('Y-m-d', strtotime('today')) //opcional
    );
    
    // {"action" : "retrieve_all",  "auth_id": "2ccd2f2f-d929-11eb-8d81-ac1f6b17ff4a"}

    $query = "SELECT AUP.id, AUP.auths_user_id,   AUP.public_id as auth_public_id, AUP.user_id, AUP.platform, AUP.retorno, AUP.app_id, AUP.app_secret, AUP.platform_user_id,  AUP.access_token, AUP.refresh_token, U.name, U.email, U.customer_id_default, U.campaign_root_default , U.public_id as user_public_id FROM auths_user_platform AUP JOIN users U ON (AUP.user_id = U.id)  WHERE AUP.public_id = '" . $data["auth_id"] . "';";
    $result = $dbconn->query($query);
    $infoCredentials = $result->fetch_assoc();
  print_r($infoCredentials );
    if (isset($infoCredentials['user_id'])){
        $user_id = $infoCredentials['user_id'];
    }else{
        echo 'No AUTH ID found';
        die();  
    }

    $app_secret = json_decode($infoCredentials['app_secret']);
    $user_credentials = json_decode($infoCredentials['retorno']);
$TWITTER_CONSUMER_KEY = 'iy5pXGlxt4ru0GPYE81WXbdtT'; //(API key)
$TWITTER_CONSUMER_SECRET = 'WriLQUz8DnUOJm2oeUnaQzzK2gCdLO1d4oPHXeS413k3aJpxri';
    //app_secret -> {"consumer_key":"iy5pXGlxt4ru0GPYE81WXbdtT","consumer_secret":"WriLQUz8DnUOJm2oeUnaQzzK2gCdLO1d4oPHXeS413k3aJpxri"}
    //retorno -> {"oauth_token":"327004085-gnVzt31NzsA6pMjus8ck1GACZfYbe0IQGv70aCJl","oauth_token_secret":"gHhmKXtosBLPr9kwY3RGcaT32bt4cdVwlHGa6FHNaQAxZ","user_id":"327004085","screen_name":"JrHabana"}
    $twitterapi = TwitterAds::init($TWITTER_CONSUMER_KEY, $TWITTER_CONSUMER_SECRET, $user_credentials->oauth_token, $user_credentials->oauth_token_secret);
    
    // to do - validate token
    // to do - refresh token 
    // if token largo = caduco
    //$query = "UPDATE auths_user_platform set auths_user_platform.activa = 'EXPIRATE' WHERE auths_user_platform.ID = '" . $infoCredentials['id'] . "'";
    //$dbconn->query($query);
    //redirect(/route/desactivar_token_por_Caducidad)

    if ($data['action']== null){
        echo 'NO ACTION SPECIFIED';
        die();
    }

    switch ($data['action']) {

        case "retrieve_all":       
            get_accounts_and_properties($twitterapi, $data, $infoCredentials);
            break;

        case "get_campaigns_and_atoms":
            get_campaigns_and_atoms($twitterapi, $infoCredentials, $data);
            break;


        case "get_creatives":
            get_creatives($twitterapi, $infoCredentials, $data);
            break;
            

        
        // stats
        case "stats_campana_xaccount":
            stats_campana_xaccount($twitterapi, $infoCredentials, $data);
            break;
/* 
        case "stats_atomo_xaccount":
            stats_atomo_xaccount($twitterapi, $infoCredentials, $data);
            break;



        case "stats_campana_xaccount":
            stats_campana_xaccount($twitterapi, $infoCredentials, $data);
            break;
        case "stats_campana_xaccount":
            stats_campana_xaccount($twitterapi, $infoCredentials, $data);
            break; */


        

        default:
            echo "NO ACTION";

    }

}

/*** 
 

*/
/** descomentar para pruebas en local
*/
index();

?>