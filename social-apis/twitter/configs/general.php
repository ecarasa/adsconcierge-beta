<?php

date_default_timezone_set('UTC');

$DB_HOST = '37.187.95.206';
$DB_PORT = 3306;
$DB_DATABASE = "app_thesoci_9c37";
$DB_USERNAME = "appthesoci9c37";
$DB_PASSWORD = "5efc1eed04037ae2";

$DB_HOST_STATS = '37.187.95.206';
$DB_PORT_STATS = 3306;
$DB_DATABASE_STATS = "adsconcierge_stats";
$DB_USERNAME_STATS = "appthesoci9c37";
$DB_PASSWORD_STATS = "5efc1eed04037ae2";


try{
    $dbconn = new mysqli($DB_HOST, $DB_USERNAME, $DB_PASSWORD, $DB_DATABASE);
    $dbconn_stats = new mysqli($DB_HOST_STATS, $DB_USERNAME_STATS, $DB_PASSWORD_STATS, $DB_DATABASE_STATS);
}catch (mysqli_sql_exception $e) {
    echo "Error db: " .  $e->getMessage();
    die();
}

if (!function_exists('unificastatus')){
	function unificastatus($tipoentidad, $plataforma, $campoestado, $record) {
		switch($plataforma) {	
			case 3:
				switch($tipoentidad) {
					case 'campana':
						if ( count($record['otros'])) { return $campoestado.'-'.json_encode($record['otros'] ) ;  }	
					default:
						return $campoestado;
						break;
					}
			}
						return $campoestado;
	}
}

if(!function_exists('get_user_id')){
	function get_user_id(){
		global $dbconn;
		$userid = NULL;
		if ($resultado = $dbconn->query("SELECT id FROM users WHERE hashuser ='".$_COOKIE["id_user"]."'")) {
			$userid = $resultado->fetch_object()->id;
			$resultado->close();
		}
		return $userid;
	}
}

if (!function_exists('dateRanges')){
	function dateRanges($startTime, $endTime,$intervalmodel='P1D')
	{
	 if (is_string($startTime)) { $startTime=   new DateTime( $startTime );  }
	 if (is_string($endTime)) { $endTime=   new DateTime( $endTime );  }
	   // $interval = new DateInterval('P7D');
		$interval = new DateInterval($intervalmodel);
		$dateRange = new DatePeriod($startTime, $interval, $endTime);
		$previous = null;
		$dates = array();
		foreach ($dateRange as $dt) {
			$current = $dt;
			if (!empty($previous)) {
				$show = $current;
				$dates[] = array($previous, $show);
			}
			$previous = $current;
		}
		if (isset($dates[count($dates) - 1])) {
			$dates[] = array($dates[count($dates) - 1][1], $endTime);
		} else {
			$dates[] = array($startTime, $endTime);
		}
		return $dates;
	}
}