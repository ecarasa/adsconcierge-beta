<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
define('PLATFORM_NAME','FACEBOOK');

include 'vendor/autoload.php';
include 'configs/general.php';
include 'helpers/extLogger.php';
include 'helpers/stats_mapeometrics.php';


use FacebookAds\Api;
use FacebookAds\Cursor;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Post;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\User;

use FacebookAds\Http\Exception\ServerException;

use Psr\Http\Message\ServerRequestInterface;

use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdCreativeFields;


use Google\Cloud\Scheduler\V1\AppEngineHttpTarget;
use Google\Cloud\Scheduler\V1\CloudSchedulerClient;
use Google\Cloud\Scheduler\V1\Job;
use Google\Cloud\Scheduler\V1\Job\State;

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Tasks\V2\CloudTasksClient;
use Google\Cloud\Tasks\V2\HttpMethod;
use Google\Cloud\Tasks\V2\HttpRequest;
use Google\Cloud\Tasks\V2\Task;

Cursor::setDefaultUseImplicitFetch(false);

/* 
    POST 
    Request
    [action, auth_id, parent_platform_ids, platform_id, period ,star_date, end_date] 
    
    ej:
    {"action" : "get_accounts",  "auth_id": "2ccd6be6-d929-11eb-8d81-ac1f6b17ff4a"}

    Orden de retrieve_entities_all
        - addaccount
        - campaign
        - adsets . atomos
        - creatividades

*/

$DEBUG = false;

function getAdAccount_PublicID($accountID){
    global $dbconn;

    $query = "SELECT * FROM app_thesoci_9c37.ads_accounts where account_id = '" . $accountID . "';";
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();

    return $row['public_id'];

}

function getAdAccount_Data($accountID){
    global $dbconn;

    $query = "SELECT * FROM app_thesoci_9c37.ads_accounts where account_id = '" . $accountID . "';";
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();

    return $row;

}

function getATOM_Data($atomID){
    global $dbconn;

    $query = "SELECT * FROM app_thesoci_9c37.campaigns_platform_atomo where id_en_platform = '" . $atomID . "';";
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();

    return $row;
}

function persistAccount($user_id, $accountItem, $infoCredentials){
   
    global $dbconn;

    if (isset($accountItem['account_status']) && isset($accountItem['disable_reason'])){
        $status = $accountItem['account_status'] . '|' . $accountItem['disable_reason'];
    }else{
        $status = 'NO_STATUS_RETRIEVE';
    }
    
    $metadata = json_encode($accountItem, true);

    $stmt = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`ads_accounts` (`user_id`, `platform`, `app_id`, `account_id`, `name`, `platform_user_id`, `status`, `currency`, `metadata`, `customer_id`, `auth_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?) 	ON DUPLICATE KEY UPDATE `name`= ?,  `status`= ?, `platform_user_id`=?,  `metadata`= ? ");

    $stmt->bind_param("isississsiissis", $user_id, 
                                        $infoCredentials["platform"], 
                                        $infoCredentials["app_id"], 
                                        $accountItem['account_id'], 
                                        $accountItem['name'],
                                        $infoCredentials['platform_user_id'], 
                                        $status, 
                                        $accountItem['currency'], 
                                        $metadata, 
                                        $infoCredentials["customer_id_default"], 
                                        $infoCredentials["id"], 
                                        $accountItem['name'],
                                        $status, 
                                        $infoCredentials['platform_user_id'], 
                                        $metadata);

    try {
        $stmt->execute();
        return true;
    } catch (Exception $e) {
        print_r($e);
    }    

}

function getUserPublicId($id){

    global $dbconn;
    $query = "SELECT * FROM app_thesoci_9c37.users where id = '" . $id . "';";
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();
    return $row['public_id'];

}

function getAuthFrom_authId($id){

    global $dbconn;

    $query = "SELECT * FROM app_thesoci_9c37.auths_user_platform where id = '" . $id . "';";
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();

    return $row;

} 

// ok
function get_accounts( $data ,$infoCredentials){

    global $dbconn_stats, $dbconn;

    $fields = array('account_status','name','id','age','amount_spent','attribution_spec','account_id','balance','business_name','business_city','business_country_code','currency','owner','partner','user_tos_accepted','spend_cap','tos_accepted','offsite_pixels_tos_accepted','user_tasks','disable_reason','has_migrated_permissions','is_prepay_account','media_agency','can_create_brand_lift_study','is_direct_deals_enabled','is_in_middle_of_local_entity_migration');
    $fields = array('account_status','name','id','age','amount_spent','attribution_spec','account_id','balance','business_name','business_city','business_country_code','currency', 'partner','user_tos_accepted','spend_cap','tos_accepted','offsite_pixels_tos_accepted', 'user_tasks','disable_reason', 'has_migrated_permissions',  'media_agency','can_create_brand_lift_study','is_direct_deals_enabled' ,'is_in_middle_of_local_entity_migration' );
    $params = array('summary' => true, 'limit'=> 80);

    echo 'Search - Ads Accounts '. PHP_EOL;
    
    try{
        $adAccounts = (new User($infoCredentials['platform_user_id']))->getAdAccounts($fields, $params);
        $adAccounts->setDefaultUseImplicitFetch(true);

        $arr = array();

        foreach($adAccounts as $adAccount) {
            
            $accountItem = $adAccount->getData();
            echo  $accountItem['account_id'] . ' --- ' . $accountItem['name'] . ',' . PHP_EOL;
            array_push($arr, array("id"=>$accountItem['account_id'], "currency"=>$accountItem['currency']));
            persistAccount($infoCredentials['user_id'], $accountItem, $infoCredentials);
      
            //break;
        
        }

    } catch (Exception $e) {
        echo $e->getMessage().PHP_EOL;
    }

    return $arr;

}

//ok 
function get_properties($account, $infoCredentials){
   //            get_properties($data['ad_account_id'], $infoCredentials);

    global  $dbconn_stats, $dbconn;

    $fields = array('id', 'name', 'category');
    $params = array( 'summary' => true);

    echo 'Search Properties '. PHP_EOL;

    $adAccount = new AdAccount('act_' . $account);

    echo ' Properties from Addcount ' . ' act_' . $account . PHP_EOL; 
        
    foreach ( $adAccount->getPromotePages($fields,  $params) as $p){

        $page= $p->getData();
        $page_id=$page['id'];
        $pagename=$page['name'];
        $category=$page['category'];
        $token= $infoCredentials['access_token'];

        $status=0;
        $metadata=json_encode( $page, true);
        $stmt2 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` (`user_id`, `platform`, `app_id`,`platform_user_id`,
            `id_en_platform`, `token`,`name`,`status`,`category`, `metadata`, `adaccount_id`, `auth_id`)
                                VALUES (?,?,?,?,?,?,?,?,?,?,?,?)
                                ON DUPLICATE KEY UPDATE    `name`= ?,  `status`= ?,`platform_user_id`=?,  `metadata`= ? ");
        

        $stmt2->bind_param("ssssssssssssssss", 
        ...[$infoCredentials["user_id"], $infoCredentials["platform"], $infoCredentials["app_id"], $infoCredentials['platform_user_id'], $page_id, $token, 
            $pagename, $status, $category, $metadata, $account, $infoCredentials["id"], 
            $pagename, $status, $infoCredentials['platform_user_id'], $metadata]);
            $stmt2->execute();

        echo '   Property added -> ' . $page['name']. PHP_EOL; 

        $query = "SELECT id as pageid, public_id as property_public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE  auth_id = '{$infoCredentials["id"]}' and platform = '{$infoCredentials["platform"]}' and platform_user_id= '{$infoCredentials['platform_user_id']}' and id_en_platform = '{$page_id}'";
        $res_pa = $dbconn->query($query);
        $id_pa = $res_pa->fetch_assoc();

        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` 
                            (`type`,`auth_id`, `ad_account`,  `ad_account_name`,`property_name`, 
                            `property_id`, `user_id`,`platform`,`auth_publicid`,   `user_publicid`,  
                            `ad_account_publicid`,`property_publicid`) 
                            VALUES (
                                'PAGE', 
                                ?,
                                ?,
                                (SELECT name FROM app_thesoci_9c37.ads_accounts where ads_accounts.auth_id = ? and account_id = ?),
                                ?,
                                ?,
                                ?,
                                ?,
                                ?,
                                ?,
                                (SELECT public_id FROM app_thesoci_9c37.ads_accounts where account_id = ? and ads_accounts.auth_id = ?),
                                ?)
                            ON DUPLICATE KEY UPDATE `ad_account_name` = (SELECT name FROM app_thesoci_9c37.ads_accounts where ads_accounts.auth_id = ? and account_id = ?), 
                            `property_name` = ?");
        
        $adc_publicid= getAdAccount_PublicID($account);

        echo "SELECT name FROM app_thesoci_9c37.ads_accounts where ads_accounts.auth_id = ".$infoCredentials["id"]." and account_id = ".$account.PHP_EOL;

        $stmt3->bind_param("ssssssssssssssss",
                            ...[$infoCredentials["id"],
                                $account,
                                $infoCredentials["id"],
                                $account,
                                $pagename,
                                $id_pa["pageid"],
                                $infoCredentials["user_id"],
                                $infoCredentials["platform"],
                                $infoCredentials["auths_user_id"],
                                getUserPublicId($infoCredentials["user_id"]), 
                                $account,
                                $infoCredentials["id"],
                                $id_pa["property_public_id"],
                                $account,
                                $infoCredentials["id"],
                                $pagename]);

        $stmt3->execute();

    }

}

// ok
function get_campaigns($account, $infoCredentials){
    // get_campaigns($data['ad_account_id'], $infoCredentials);

    global $data, $dbconn_stats, $dbconn, $api;

    echo ' - Search Campaigns -  '. PHP_EOL;
        
    $campaigns = ( (new AdAccount('act_'.$account) )->getCampaigns(CampaignFields::getInstance()->getvalues(),  array('summary' => true) ));
    $campaigns->setDefaultUseImplicitFetch(true);
    $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (user_id, platform, app_id, auth_id, name, id_en_platform, status, currency, metadata, ad_account, campana_root, customer_id, source) values (?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id =? and ads_accounts.user_id=?), ?,?, 'IMPORTED') on duplicate key update status = ?, metadata = ?, campana_root = ?, customer_id = ?");

    echo ' . AdAccount : ' . $account . ', ';
    echo ' - Campaigns : ' . count($campaigns) . PHP_EOL;

    $arr = array();

    foreach ($campaigns as $citemmm) {

        $citem = $citemmm->getData();

        echo '      -> Campaign: ' . $citem['name'] . '  act_' . $account . PHP_EOL;
        
        $stmtcampana->bind_param("issssssssssssssss", ...[  $infoCredentials['user_id'], 
                                                            $infoCredentials['platform'],
                                                            $infoCredentials['app_id'], 
                                                            $infoCredentials['id'],
                                                            $citem['name'], 
                                                            $citem['id'], 
                                                            unificastatus('campana', $infoCredentials['platform'], $citem['status'],  ['estado' => $citem['status'], 'otros' => $citem['effective_status'],  'record' => $citem]), 
                                                            isset($account['currency']) ? $account['currency'] : 'EUR',  
                                                            json_encode($citem), 
                                                            $account, 
                                                            $infoCredentials['user_id'], 
                                                            isset($infoCredentials["campaign_root_default"]) ? $infoCredentials["campaign_root_default"] : '0', 
                                                            $infoCredentials["customer_id_default"], 
                                                            unificastatus('campana', $infoCredentials['platform'], $citem['status'], ['estado' => $citem['status'],  'otros' => $citem['effective_status'], 'record' => $citem]), 
                                                            json_encode($citem), 
                                                            isset($infoCredentials["campaign_root_default"]) ? $infoCredentials["campaign_root_default"] : '0', 
                                                            $infoCredentials["customer_id_default"]] );
        $stmtcampana->execute();

        if ($stmtcampana->error != "") {
            printf("Error: %s.\n", $stmtcampana->error);
        }
        
    }

    // Get campaigns, después del loop de inserción en tabla de las campañas, lanzas una sola task get adsets de la cuenta, en lugar de champaña
    $event = array( 'subject_id' => $infoCredentials['id'], 'type' => 'get_adsets', 'function' => 'function-facebook-api',  'ad_account_id' => $account);
    execGoogleTask($event);

    //$event = array( 'subject_id' => $infoCredentials['id'], 'type' => 'get_stats_campaigns', 'function' => 'function-facebook-api',  'ad_account_id' => $account);
    //execGoogleTask($event);

}

function get_adsets($account, $infoCredentials ){
    // get_adsets($data['ad_account_id'], $infoCredentials);

    global $dbconn_stats, $dbconn;
    
    $AdSetFields = AdSetFields::getInstance();
    $fields = $AdSetFields->getvalues();
    
    unset($fields[24]);
    unset($fields[54]);
    unset($fields[57]);
    unset($fields[61]);

    $ad_account = new AdAccount('act_'. $account);
    $adsets = $ad_account->getAdSets($fields);

    $stmtadset = $dbconn->prepare("INSERT INTO campaigns_platform_atomo (budget, budget_total, user_id, id_en_platform, platform, app_id, campana_root_id, campana_platform_id, auth_id, name, status, customer_id, metadata, ad_account, source) values (?,?,?,?,?,?,?, (SELECT id FROM campaigns_platform WHERE campaigns_platform.id_en_platform = ? and campaigns_platform.user_id = ?),?,?,?,?,?, (SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id =? and ads_accounts.user_id=?), 'IMPORTED') 
    on duplicate key update  status = ?,  metadata = ?, budget = ?, budget_total= ? ");

    echo " * Campaign - AdSets (Atom) -> " . count($adsets) .PHP_EOL;

    try {
        foreach ($adsets as $adset) {
            
            $adset = $adset->getData();
            
            echo "  >> ". $adset['name'] . ' - ' .$adset['id'] . PHP_EOL;
            
            $stmtadset->bind_param("ssisssssissssssissss", ...[ $adset['daily_budget'],
                                                                $adset['lifetime_budget'], 
                                                                $infoCredentials['user_id'],
                                                                $adset['id'], 
                                                                $infoCredentials['platform'], 
                                                                $infoCredentials['app_id'], 
                                                                $infoCredentials["campaign_root_default"], 
                                                                $adset['campaign_id'], 
                                                                $infoCredentials['user_id'], 
                                                                $infoCredentials['id'], 
                                                                $adset['name'], 
                                                                $adset['status'],
                                                                $infoCredentials["customer_id_default"], 
                                                                json_encode($adset),
                                                                $adset['account_id'], 
                                                                $infoCredentials['user_id'], 
                                                                $adset['status'],  
                                                                json_encode($adset),
                                                                $adset['daily_budget'], 
                                                                $adset['lifetime_budget']
                                                            ]);

            $stmtadset->execute();
            
            if ($stmtadset->error != "") {
                printf("Error: %s.\n", $stmtadset->error);
            }

        }
    } catch (Exception $e) {
        print_r($e);
    }  
    
    // Lanzamos task get creas x cada campaigns y con todos los parent ( atomos de la campaing)
    // Después del loop de insert de los adsets en tabla, al final (como en campañas)
    $event = array( 'subject_id' => $infoCredentials['id'], 'type' => 'get_creativities', 'function' => 'function-facebook-api', 'ad_account_id' => $account );
    //var_dump($event);
    execGoogleTask($event);

}

// ok
function get_creativities($ad_account, $infoCredentials){
    // get_creativities($data['ad_account_id'], $infoCredentials);

    global  $dbconn_stats, $dbconn;

    $AdFields = AdFields::getInstance();
    $fields = $AdFields->getvalues();
    

    try {

        $account = new AdAccount('act_'. $ad_account);
        $ads = $account->getAds($fields);

        $stmtadset = $dbconn->prepare("INSERT INTO creatividades 
                                                    ( id_en_platform, 
                                                    user_id, 
                                                    customer_id, 
                                                    name,  
                                                    platform, 
                                                    campana_root, 
                                                    campana_platform_id, 
                                                    atomo_id, 
                                                    platform_status, 
                                                    metadata, 
                                                    source ) 
                                                    values (
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                    (SELECT id FROM campaigns_platform WHERE campaigns_platform.id_en_platform = ? and campaigns_platform.user_id = ? LIMIT 1), 
                                                    (SELECT id FROM campaigns_platform_atomo WHERE id_en_platform = ? and user_id = ? LIMIT 1),?,?, 'IMPORTED') 
                                                    on duplicate key update platform_status=?, metadata=?");

        echo "Creatividades -> " . count($ads) . PHP_EOL;
    
        foreach ($ads as $ad) {
            
            $ad = $ad->getData();
            echo ' Creatividades ' . $ad['name'] . ' id: '.$ad['id'] . PHP_EOL;

            $stmtadset->bind_param( "sisssssisissss", ...[ $ad['id'],
                                        $infoCredentials['user_id'],
                                        $infoCredentials["customer_id_default"],
                                        $ad['name'],
                                        $infoCredentials['platform'],
                                        $infoCredentials["campaign_root_default"],
                                        $ad['campaign']['id'],
                                        $infoCredentials['user_id'],
                                        $ad['adset_id'],
                                        $infoCredentials['user_id'], 
                                        $ad['status'],
                                        json_encode($ad),
                                        $ad['status'],
                                        json_encode($ad)]);

            $stmtadset->execute();

            if ($stmtadset->error != "") {
                printf("not inserted - Error: %s.\n", $stmtadset->error);
            }
            echo PHP_EOL;

        }
    
    } catch (Exception $e) {
        print_r($e);
    }    

}

function get_pixels($ad_account, $infoCredentials){

    global  $dbconn_stats, $dbconn;

    echo 'SEARCH Pixels '. PHP_EOL;

    $fields = array('id',
                    'name',
                    'is_unavailable',
                    'code',
                    'owner_ad_account',
                    'first_party_cookie_status',
                    'automatic_matching_fields');

    $adAccount = new AdAccount('act_'.$ad_account);
    $adAccountData = $adAccount->getData();
    $adPixels = $adAccount->getAdsPixels($fields,  array('summary' => true, 'limit' => 80));

    foreach ($adPixels as $adPixel) {
        
        $pixel = $adPixel->getData();
        //var_dump($pixel);
        //die();

        $adPixelData = array_intersect_key($adPixel->getData(), array_flip($fields));
        //$adPixelData = array_merge($adPixelData, $this->userPlatformData);
        $adPixelData['ad_account_id'] = $adAccount->getData()['id'];
        $adPixelData['id_en_platform'] = $adPixel->getData()['id'];
        $adPixelData['status'] = 0; //todo: fixed in origin code
        $adPixelData['category'] = '-';
        $adPixelData['token'] = '-';
        $adPixelData['type'] = 'PIXEL';
        
        // $this->persistPropertiesAccounts->execute($adPixelData);
        $stmt2 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` 
                                    ( `type`, `user_id`, `platform`, `app_id`, `platform_user_id`, 
                                    `id_en_platform`, `token`,`name`,`status`,`category`, `metadata`, 
                                    `adaccount_id`, `auth_id` )
                                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)
                                    ON DUPLICATE KEY UPDATE  `name` = ?, `status` = ?, `platform_user_id` = ?, `metadata`= ? ");

        $stmt2->bind_param("sssssssssssssssss", 
                                        ...[
                                        $adPixelData['type'],
                                        $infoCredentials["user_id"], 
                                        $infoCredentials["platform"], 
                                        $infoCredentials["app_id"], 
                                        $infoCredentials['platform_user_id'], 
                                        $pixel['id'], 
                                        $infoCredentials['access_token'],
                                        $pixel['name'], 
                                        $adPixelData['status'], 
                                        $adPixelData['category'],
                                        json_encode($pixel,true), 
                                        $adAccount->getData()['id'], 
                                        $infoCredentials["id"], 
                                        $pixel['name'], 
                                        $adPixelData['status'], 
                                        $infoCredentials['platform_user_id'], 
                                        json_encode($pixel,true) ]);
        $stmt2->execute();

        echo ' Pixel added -> ' . $pixel['name']. PHP_EOL; 

        // buscamos y persistimos la relacion de las ads con la properties_accounts

        $query = "SELECT id as propertyid, public_id as property_public_id FROM `app_thesoci_9c37`.`properties_accounts` 
                    WHERE type='PIXEL' and auth_id = '{$infoCredentials["id"]}' and platform = '{$infoCredentials["platform"]}' 
                    and platform_user_id= '{$infoCredentials['platform_user_id']}' and id_en_platform = '{$pixel['id']}'";
        
        $res_pa = $dbconn->query($query);
        $id_pa = $res_pa->fetch_assoc();
        
        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` 
                                        (`type`, `auth_id`, `ad_account`, `ad_account_name`, `property_name`, 
                                        `property_id`, `user_id`, `platform`, `auth_publicid`, `user_publicid`, 
                                        `ad_account_publicid`,`property_publicid`) 
                                    VALUES ('PIXEL',?,?,?,?,?,?,?,?,?,?,?)
                                    ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");
        
        $adc_publicid= getAdAccount_PublicID($adAccountData['id']);

        $stmt3->bind_param("sssssssssssss", 
                            ...[$infoCredentials["id"], 
                            $adAccountData["id"], 
                            $adAccountData["name"] ,
                            $pixel['name'],  
                            $id_pa["propertyid"],      
                            $infoCredentials["user_id"], 
                            $infoCredentials["platform"], 
                            $infoCredentials["auths_user_id"], 
                            getUserPublicId($infoCredentials["user_id"]), 
                            $adc_publicid, 
                            $id_pa["property_public_id"], 
                            $adAccountData["name"] ,
                            $pixel['name'], ]);
        
        $stmt3->execute();
        // die();
    }
}

function execGoogleTask($tarea)
{

    Global $DEBUG;

    if ($DEBUG == false) {
        try {
            $projectId = 'adsconcierge';
            $locationId = 'us-central1';
            $colaTrabajo = 'facebook-sync';
                    
            $interval = isset($tarea['Execution_interval']) ? substr($tarea['Execution_interval'], 0, 2) : '12H';
            $queueId = isset($tarea['cola_trabajo']) ? $tarea['cola_trabajo'] : 'facebook-sync';

            //https://us-central1-adsconcierge.cloudfunctions.net/function-facebook-api
            $url = 'https://' . $locationId . '-' . $projectId . '.cloudfunctions.net/' . $tarea['function'];

            $auth = getAuthFrom_authId($tarea['subject_id']);
            $auth_id = $auth['public_id'];
            $user_id = $auth['user_id'];
            
            $payload = array(   "auth_id" => $auth_id,  "action" => $tarea['type'], "ad_account_id" => $tarea['ad_account_id'] );
            echo json_encode($payload).PHP_EOL;
            
            // Instantiate the client and queue name.
            $client = new CloudTasksClient(['credentials' => __DIR__ . '/configGoogleCloud.json', 'projectId' => $projectId ]);
            $queueName = $client->queueName($projectId, $locationId, $queueId);

            // Create an Http Request Object.
            $httpRequest = new HttpRequest();
            $httpRequest->setUrl($url);
            $httpRequest->setHttpMethod(HttpMethod::POST);
                
            if (isset($payload)) {
                $httpRequest->setBody(json_encode($payload));
            }

            $task = new Task();
            $task->setHttpRequest($httpRequest);

            $response = $client->createTask($queueName, $task);
            echo 'Tarea creada .. '. $response->getName() . PHP_EOL;
            //$this->persistGCloudTask_db($user_id, $auth_id, '', $interval, $payload['action'], json_encode($payload), str_replace("projects/adsconcierge/locations/us-central1/queues/ads-concierge-sync/tasks/", "", $response->getName()));
        } catch (Exception $e) {
            print_r($e);
        }
    }else{
        echo 'Task will be created on DEBUG == false'.PHP_EOL;
        echo json_encode($tarea).PHP_EOL;
    }
}

//ok
function retrieve_all($data, $infoCredentials){
    
    echo 'Retrieve all entities for ' . $infoCredentials['name'] . ' ' . $infoCredentials['email'] . PHP_EOL;
    
    $accountsArray = get_accounts($data, $infoCredentials);

    foreach ($accountsArray as $adAccount){
        // buscamos properties
        $event = array(  'subject_id' => $infoCredentials['id'], 'type' => 'get_properties', 'function' => 'function-facebook-api', 'ad_account_id' =>  $adAccount['id'], 'callchild'=> ['get_campaigns' ] );
        //var_dump($event);
        execGoogleTask($event);
      
      // buscamos campaigns
        $event = array(  'subject_id' => $infoCredentials['id'], 'type' => 'get_campaigns', 'function' => 'function-facebook-api',  'ad_account_id' =>  $adAccount['id'] );
        //var_dump($event);
        execGoogleTask($event);



    }

}

// STATS
function helper_metrics_keytranslator($platformid, $tipoinput, $inputraw, $outputrow, $metadata = null)
{
    global $mapeometricas;

    /*     
    var_dump($inputraw);
    //echo PHP_EOL;
     die(); */

    switch ($platformid) {
        case 1:
        case "FACEBOOK":
            switch ($tipoinput) {
                case 'campana':
                    $outputrow['id_in_platform'] = $inputraw['campaign_id'];
                    break;
                case 'ad':
                    $outputrow['id_in_platform'] = $inputraw['ad_id'];
                    $outputrow['lineitem_id'] = $inputraw['adset_id'];

                    break;
                case 'lineitem':
                    $outputrow['id_in_platform'] = $inputraw['adset_id'];
                    $outputrow['lineitem_id'] = $inputraw['adset_id'];

                    break;

            }
            $outputrow['date'] = $inputraw['date_start'];
            $outputrow['yearmonth'] = date('Y-m-01', strtotime($inputraw['date_start']));
            $outputrow['platformid'] = 1;
 $outputrow['plataforma'] = 'FACEBOOK';
            switch ($inputraw['publisher_platform']) {
                case 'facebook':
                    $outputrow['platformid'] = 1;
                    $outputrow['plataforma'] = 'FACEBOOK';
                    break;
                case 'instagram':
                    $outputrow['platformid'] = 4;
                    $outputrow['plataforma'] = 'INSTAGRAM';
                    break;
            }

            //https://developers.facebook.com/docs/marketing-api/insights/parameters/v9.0
            $outputrow['video_starts'] = $inputraw['video_play_actions'];
            //$outputrow['video_views'] = $inputraw['video_play_actions'];
            $outputrow['video_completes'] = $inputraw['video_p100_watched_actions'];
            $outputrow['video_25'] = $inputraw['video_p25_watched_actions'];
            $outputrow['video_50'] = $inputraw['video_p50_watched_actions'];
            $outputrow['video_75'] = $inputraw['video_p75_watched_actions'];


            foreach ($inputraw as $clave => $valor) {
            
                if ($valor == null) {
                    continue;
                }

                $clave = strtolower($clave);
                $clavemapeo = isset($mapeometricas[$platformid][$clave]) ? $mapeometricas[$platformid][$clave] : (isset($mapeometricas['default'][$clave]) ? $mapeometricas['default'][$clave] : ['metrics_rest']);

                foreach ($clavemapeo as $claveout) {
                                    
                    if (is_numeric($outputrow[$claveout])) {
                        $outputrow[$claveout] = $outputrow[$claveout] + $valor;
                    } elseif (is_string($outputrow[$claveout])) {
                        $outputrow[$claveout] = $valor;
                    } else {
                        $outputrow[$claveout][$clave] = $valor;
                    }

                }

            }

            if (isset($inputraw["actions"])) {
                foreach ($inputraw["actions"] as $item) {
                    if ($item["action_type"] == "video_view") {
                        $outputrow['video_views'] = $item["value"];
                    }
                }
            }

            //die();
            break;
      case '4':
      case 'INSTAGRAM':
         $outputrow['plataforma'] = 'INSTAGRAM';
        break;
        
        }

    return $outputrow;

}

// helpers
function helper_metrics_campana_day($platformid, $user_id, $customer_id = 0, $datos, $ad_account_id, $ad_account_id_platform, $metadata = ['currency' => null])
{
    global $dbconn_stats, $dbconn;

    if (!isset($metadata['currency'])) {
        $metadata['currency'] = null;
    }
    if (!is_array($datos)) {
        return null;
    }

    echo ' > Updating ' . count($datos) . ' elements : ';
    $i = 1;

    foreach ($datos as $inputraw) {
//print_r( $inputraw);
   
        $outputrow = ['id_in_platform' => 0, 'campananame' => '', 'platformid' => $platformid, 'date' => '', 'metrics_delivery' => [], 'metrics_costs' => [], 'metrics_engagement' => [], 'metrics_video' => [], 'metrics_conversion' => [], 'metrics_rest' => [], 'cost' => 0, 'impressions' => 0, 'reach' => 0, 'clicks' => 0, 'engagements' => 0, 'video_views' => 0, 'video_starts' => 0, 'currency' => $metadata['currency'], 'conversions' => 0];
        $outputrow = helper_metrics_keytranslator($platformid, 'campana', $inputraw, $outputrow, $metadata);
    //  print_r( $outputrow);
   //     exit;   
    //    echo "Impressions:" . $outputrow['impressions'] . PHP_EOL;
     //   echo "Cost:" . $outputrow['cost'] . PHP_EOL;

        if ($outputrow['impressions'] == 0 && $outputrow['video_views'] == 0 && $outputrow['reach'] == 0 && $outputrow['clicks'] == 0 && $outputrow['engagements'] == 0) {
            //continue;
        }

        $sql = "INSERT INTO adsconcierge_stats.platform_campana_day ( user_id, platformid, customer_id, ad_account_id, campanaid, idenplatform, campananame, adccountid_pl, dia, unico, currency, metrics_delivery, metrics_costs, metrics_engagement, metrics_video, metrics_conversion, metrics_rest, cost, impressions, reach, clicks, engagements, video_views, conversions, plataforma, campanaroot, yearweek, yearmonth )
                                SELECT
                                    {$user_id},
                                    '{$outputrow['platformid']}',
                                    '{$customer_id}',
                                    '{$ad_account_id}',
                                    cp.id,
                                    '{$outputrow['id_in_platform']}',
                                    cp.name,
                                    '{$ad_account_id_platform}',
                                    '{$outputrow['date']}',
                                    '" . md5($outputrow['platformid'] . $outputrow['id_in_platform'] . $outputrow['date'] . $user_id) . "',
                                    '{$outputrow["metrics_rest"]["account_currency"]}',
                                    '" . json_encode($outputrow['metrics_delivery']) . "',
                                    '" . json_encode($outputrow['metrics_costs']) . "',
                                    '" . json_encode($outputrow['metrics_engagement']) . "',
                                    '" . json_encode($outputrow['metrics_video']) . "',
                                    '" . json_encode($outputrow['metrics_conversion']) . "',
                                    '" . $dbconn_stats->real_escape_string(json_encode($outputrow['metrics_rest'])) . "',
                                    '{$outputrow['cost']}', '{$outputrow['impressions']}',
                                    '{$outputrow['reach']}',
                                    '{$outputrow['clicks']}', '{$outputrow['engagements']}',
                                    '" . intval($outputrow['video_views']) . "',
                                    '{$outputrow['conversions']}',
                                    '{$outputrow['plataforma']}',
                                    cp.campana_root,
                                    YEARWEEK('{$outputrow['date']}'),
                                    date_format('{$outputrow['date']}', '%Y-%m')
                                FROM
                                    app_thesoci_9c37.campaigns_platform cp
                                WHERE
                                    cp.id_en_platform = '{$outputrow['id_in_platform']}' AND cp.user_id = {$user_id}
                                ON DUPLICATE KEY UPDATE
                                    cost                = '{$outputrow['cost']}',
                                    impressions         = '{$outputrow['impressions']}' ,
                                    reach               = '{$outputrow['reach']}',
                                    clicks              = '{$outputrow['clicks']}',
                                    engagements         = '{$outputrow['engagements']}',
                                    video_views         = '" . intval($outputrow['video_views']) . "',
                                    video_starts        = '" . intval($outputrow['video_starts']) . "',
                                    video_completes     = '" . intval($outputrow['video_completes']) . "',
                                    video_25            = '" . intval($outputrow['video_25']) . "',
                                    video_50            = '" . intval($outputrow['video_50']) . "',
                                    video_75            = '" . intval($outputrow['video_75']) . "',
                                    conversions         = '{$outputrow['conversions']}',
	                                metrics_delivery    = '" . json_encode($outputrow['metrics_delivery']) . "',
	                                metrics_costs       = '" . json_encode($outputrow['metrics_costs']) . "',
	                                metrics_engagement  = '" . json_encode($outputrow['metrics_engagement']) . "',
	                                metrics_video       = '" . json_encode($outputrow['metrics_video']) . "',
	                                metrics_conversion  = '" . json_encode($outputrow['metrics_conversion']) . "',
                                    metrics_rest        = '" . $dbconn_stats->real_escape_string(json_encode($outputrow['metrics_rest'])) . "'";
echo $sql.PHP_EOL;
      print_R( $dbconn_stats->query($sql) ); 
        if (!$dbconn_stats->query($sql)) {
            
            var_dump( $inputraw);
            echo PHP_EOL; echo PHP_EOL; echo PHP_EOL;            echo PHP_EOL; echo PHP_EOL; echo PHP_EOL;
            echo $dbconn_stats->error;
            echo PHP_EOL; echo PHP_EOL; echo PHP_EOL;            echo PHP_EOL; echo PHP_EOL; echo PHP_EOL;
            var_dump( $outputrow);
            echo PHP_EOL; echo PHP_EOL; echo PHP_EOL;
            echo PHP_EOL . $sql;
            die();
        }
      exit;

    }

}

function helper_metrics_ads_day($platformid, $user_id, $customer_id = 0, $addAcountID, $datos)
{
    global $dbconn_stats, $db;

    if (!isset($metadata['currency'])) {
        $metadata['currency'] = null;
    }

    foreach ($datos as $inputraw) { 
        
     //   print_r($inputraw);
        //die();
        $outputrow = ['id_in_platform' => 0, 'campananame' => '', 'platformid' => $platformid, 'date' => '', 'metrics_delivery' => [], 'metrics_costs' => [], 'metrics_engagement' => [], 'metrics_video' => [], 'metrics_conversion' => [], 'metrics_rest' => [], 'cost' => 0, 'impressions' => 0, 'reach' => 0, 'clicks' => 0, 'engagements' => 0, 'video_views' => 0, 'video_starts' => 0, 'currency' => $metadata['currency'], 'conversions' => 0];

        $outputrow = helper_metrics_keytranslator($platformid, 'ad', $inputraw, $outputrow, $metadata);
        
        if ($outputrow['impressions'] == 0 && $outputrow['video_views'] == 0 && $outputrow['reach'] == 0 && $outputrow['clicks'] == 0 && $outputrow['engagements'] == 0) {
            continue;
        }
        echo "helper_metrics_ads_day *** " . $outputrow['impressions'] . PHP_EOL . PHP_EOL;
        
        $sql = "INSERT INTO adsconcierge_stats.platform_ads_day ( user_id, platformid, customer_id, ad_account_id, campanaid,
                                                lineitemid, idenplatform, adccountid_pl, dia, unico,
                                                ad_name, metrics_delivery, metrics_costs, metrics_engagement, metrics_video,
                                                metrics_conversion, metrics_rest, cost, impressions, reach,
                                                clicks, engagements, video_views, conversions, plataforma,
                                                campanaid_enplatform, lineitem_enplatform,
                                                campanaroot, yearweek, yearmonth )
                                SELECT
                                        {$user_id}, {$outputrow['platformid']}, c.customer_id, aa.id, cp.id,
                                        cpo.id, '{$outputrow['id_in_platform']}', aa.account_id, '{$outputrow['date']}', '" . md5($outputrow['platformid'] . $outputrow['id_in_platform'] . $outputrow['date'] . $user_id) . "',
                                        c.title,'" . json_encode($outputrow['metrics_delivery']) . "', '" . json_encode($outputrow['metrics_costs']) . "', '" . json_encode($outputrow['metrics_engagement']) . "', '" . json_encode($outputrow['metrics_video']) . "',
                                        '" . json_encode($outputrow['metrics_conversion']) . "', '" . $dbconn_stats->real_escape_string(json_encode($outputrow['metrics_rest'])) . "', '{$outputrow['cost']}', '{$outputrow['impressions']}', '{$outputrow['reach']}',
                                        '{$outputrow['clicks']}', '{$outputrow['engagements']}', '{$outputrow['video_views']}', '{$outputrow['conversions']}', '{$platformid}',
                                        cp.id_en_platform, cpo.id_en_platform,
                                        cp.campana_root,  YEARWEEK('{$outputrow['date']}'),  date_format('{$outputrow['date']}', '%Y-%m')
                              
                                FROM app_thesoci_9c37.creatividades c
                                    JOIN app_thesoci_9c37.campaigns_platform_atomo cpo on cpo.id = c.atomo_id
                                    JOIN app_thesoci_9c37.campaigns_platform cp ON cp.id = cpo.campana_platform_id
                                    JOIN app_thesoci_9c37.ads_accounts aa ON aa.id = cpo.ad_account
                                    
                                WHERE c.id_en_platform = '{$outputrow['id_in_platform']}' and c.user_id = {$user_id}
                                ON DUPLICATE KEY UPDATE
                                    cost                = '{$outputrow['cost']}',
                                    impressions         = '{$outputrow['impressions']}' ,
                                    reach               = '{$outputrow['reach']}',
                                    clicks              = '{$outputrow['clicks']}',
                                    engagements         = '{$outputrow['engagements']}',
                                    video_views         = '{$outputrow['video_views']}',
                                    conversions         = '{$outputrow['conversions']}',
	                                metrics_delivery    = '" . json_encode($outputrow['metrics_delivery']) . "',
	                                metrics_costs       = '" . json_encode($outputrow['metrics_costs']) . "',
	                                metrics_engagement  = '" . json_encode($outputrow['metrics_engagement']) . "',
	                                metrics_video       = '" . json_encode($outputrow['metrics_video']) . "',
	                                metrics_conversion  = '" . json_encode($outputrow['metrics_conversion']) . "',
	                                metrics_rest        = '" . $dbconn_stats->real_escape_string(json_encode($outputrow['metrics_rest'])) . "'"; 
        
        echo $sql.PHP_EOL;

        if (!$dbconn_stats->query($sql)) {
            var_dump( $inputraw);
            echo $dbconn_stats->error;
            //var_dump( $outputrow);
            echo PHP_EOL . PHP_EOL . $sql;
            //die();
        }
    }
    echo ' DB AdsxDia data updated > ';
    //$sql = "UPDATE adsconcierge_stats.background_job SET status = 'finished' WHERE id = {$job_id}";
    //$db->query($sql);
}

function helper_metrics_lineitem_day($platformid, $user_id, $customer_id = 0, $ad_account_id, $datos)
{
    global $dbconn_stats, $dbconn;
    if (!isset($metadata['currency'])) {
        $metadata['currency'] = null;
    }

    foreach ($datos as $inputraw) {

        $outputrow = ['id_in_platform' => 0, 'campananame' => '', 'platformid' => $platformid, 'date' => '', 'metrics_delivery' => [], 'metrics_costs' => [], 'metrics_engagement' => [], 'metrics_video' => [], 'metrics_conversion' => [], 'metrics_rest' => [], 'cost' => 0, 'impressions' => 0, 'reach' => 0, 'clicks' => 0, 'engagements' => 0,
            'video_views' => 0, 'video_starts' => 0, 'currency' => $metadata['currency'], 'conversions' => 0];
        
        $outputrow = helper_metrics_keytranslator($platformid, 'lineitem', $inputraw, $outputrow, $metadata);

        echo "helper_metrics_lineitem_day ************" . PHP_EOL;
        
        if ($outputrow['impressions'] == 0 && $outputrow['video_views'] == 0 && $outputrow['reach'] == 0 && $outputrow['clicks'] == 0 && $outputrow['engagements'] == 0) {
            continue;
        }

        $sql = "INSERT INTO adsconcierge_stats.platform_atomo_day (
                                                user_id, platformid, customer_id, ad_account_id, campanaid,
                                                atomoid, idenplatform, adccountid_pl, dia, unico,
                                                metrics_delivery, metrics_costs, metrics_engagement, metrics_video,
                                                metrics_conversion, metrics_rest, cost, impressions, reach,
                                                clicks, engagements, video_views, conversions, plataforma,
                                                campanaroot, yearweek, yearmonth
                                                )
                                SELECT
                                        {$user_id}, {$outputrow['platformid']}, cpo.customer_id, aa.id, cpo.campana_platform_id,
                                        cpo.id, '{$outputrow['id_in_platform']}',  aa.account_id, '{$outputrow['date']}', '" . md5($outputrow['platformid'] . $outputrow['id_in_platform'] . $outputrow['date'] . $user_id) . "',
                                        '" . json_encode($outputrow['metrics_delivery']) . "', '" . json_encode($outputrow['metrics_costs']) . "', '" . json_encode($outputrow['metrics_engagement']) . "', '" . json_encode($outputrow['metrics_video']) . "',
                                        '" . json_encode($outputrow['metrics_conversion']) . "', '" . $dbconn_stats->real_escape_string(json_encode($outputrow['metrics_rest'])) . "', '{$outputrow['cost']}', '{$outputrow['impressions']}', '{$outputrow['reach']}',
                                        '{$outputrow['clicks']}', 
                                        '{$outputrow['engagements']}', 
                                        '{$outputrow['video_views']}',
                                         '{$outputrow['conversions']}', 
                                         '{$platformid}',
                                        cp.campana_root,  YEARWEEK('{$outputrow['date']}'),  date_format('{$outputrow['date']}', '%Y-%m')
                                FROM app_thesoci_9c37.campaigns_platform_atomo cpo
                                    JOIN app_thesoci_9c37.campaigns_platform cp ON cp.id = cpo.campana_platform_id
                                    JOIN app_thesoci_9c37.ads_accounts aa ON aa.id = cpo.ad_account
                                    WHERE cpo.id_en_platform = '{$outputrow['id_in_platform']}' and cpo.user_id = {$user_id}
                                ON DUPLICATE KEY UPDATE
                                    cost                = '{$outputrow['cost']}',
                                    impressions         = '{$outputrow['impressions']}' ,
                                    reach               = '{$outputrow['reach']}',
                                    clicks              = '{$outputrow['clicks']}',
                                    engagements         = '{$outputrow['engagements']}',
                                    video_views         = '{$outputrow['video_views']}',
                                    conversions         = '{$outputrow['conversions']}',
	                                metrics_delivery    = '" . json_encode($outputrow['metrics_delivery']) . "',
	                                metrics_costs       = '" . json_encode($outputrow['metrics_costs']) . "',
	                                metrics_engagement  = '" . json_encode($outputrow['metrics_engagement']) . "',
	                                metrics_video       = '" . json_encode($outputrow['metrics_video']) . "',
	                                metrics_conversion  = '" . json_encode($outputrow['metrics_conversion']) . "',
                                    metrics_rest        = '" . $dbconn_stats->real_escape_string(json_encode($outputrow['metrics_rest'])) . "'";


        //var_dump($outputrow);die();

        if (!$dbconn_stats->query($sql)){
            echo 'ERROR INSERT ' . PHP_EOL;
            print_r();
        }
    }

    echo ' DB AtomoxDia data updated > ';

    //$db->query("UPDATE adsconcierge_stats.background_job SET status = 'finished' WHERE id = {$job_id}");
}

function retrieve_all_ads_accounts_stats($data, $infoCredentials, $user_id, $accountsArray = null){
    
    if ($accountsArray==null) {
        $accountsArray = get_accounts($data, $infoCredentials);
    }

    get_stats_campaigns($accountsArray, $data, $infoCredentials);
    
}

// addaccount/campaign stats
function get_stats_campaigns( $adAccount_param, $data , $infoCredentials){
  
 
          $AdAccountObj = getAdAccount_Data($adAccount_param);
  $parentobject =  new AdAccount("act_" . $adAccount_param );
 
  $data['platform_object_id']= $adAccount_param;
$retorno=  get_stats_global($parentobject  , $data , $infoCredentials ,'campaign', 'get_stats_campaigns' );
  
  helper_metrics_campana_day( $infoCredentials['platform'] , $infoCredentials['user_id'], $infoCredentials['customer_id_default'],  $retorno, $AdAccountObj['id'], $AdAccountObj['account_id'],  $infoCredentials );
            
}
function get_stats_global( $parentobject, $data , $infoCredentials ,$level, $parentevent){
//https://developers.facebook.com/docs/marketing-api/insights/parameters/v11.0
  
    $start_date = isset($data['start_date']) ? $data['start_date'] : date('Y-m-d', strtotime(' - 30 day'));
    $end_date = isset($data['end_date']) ? $data['end_date'] : date('Y-m-d', strtotime('today'));

//act_36894230/insights?fields=account_id,impressions,clicks,campaign_id,campaign_name,account_currency,buying_type,date_start,objective,reach,spend,inline_post_engagement,cost_per_action_type,cpc,cpm,frequency,estimated_ad_recall_rate,estimated_ad_recallers,full_view_impressions,inline_link_clicks,optimization_goal,quality_ranking,website_ctr,qualifying_question_qualify_answer_rate,adset_id,adset_name,action_values&date_preset=yesterday&time_increment=1&level=ad&breakdowns=device_platform,publisher_platform
//filtering=[{field:"ad.impressions",operator:"GREATER_THAN",value:0}]
//  date_preset

  
  
    $fields = ['impressions', 'campaign_id', 'account_id', 'account_currency', 'buying_type', 'campaign_name', 'clicks',  'date_start', 'date_stop', 'objective', 'reach', 'spend',
               'inline_post_engagement', 'wish_bid',  'social_spend', 'video_thruplay_watched_actions', 'video_play_actions'
               , 'ad_bid_value', 'cost_per_action_type', 'cost_per_thruplay','cpc','cpm','frequency','full_view_impressions','inline_link_clicks'
               , 'optimization_goal', 'quality_ranking', 'website_ctr','cost_per_unique_action_type'];
  
  $stringgraphapi ='account_id,impressions,clicks,campaign_id,campaign_name,account_currency,buying_type,date_start,objective,reach,spend,inline_post_engagement,cost_per_action_type,cpc,cpm,website_ctr';
  $fields=explode (',', $stringgraphapi );


    $params = [ 'level' =>$level, 
               // 'time_range' => array('since' => substr($data['start_date'], 0, 10), 'until' => substr($data['end_date'], 0, 10)), 
//  enum{today, yesterday, this_month, last_month, this_quarter, maximum, last_3d, last_7d, last_14d, last_28d, last_30d, last_90d, last_week_mon_sun, last_week_sun_sat, last_quarter, last_year, this_week_mon_today, this_week_sun_today, this_year}                 
                'date_preset' => 'last_month', 
                'time_increment' => '1', 
                'breakdowns' => 'publisher_platform,device_platform'];
//si falla, quitar:     conversions, actions 'ad_bid_type',  'website_purchase_roas', y logear
  
    $api = Api::init($infoCredentials['app_id'], $infoCredentials['app_secret'], $infoCredentials['access_token']);

    
        try {
            
            echo 'Stats campaigns ad_account: '. $data['platform_object_id'] .' from '. $data['start_date'].' to '. $data['end_date'] . PHP_EOL;
    
       
            $datos = ( $parentobject )->getInsights($fields, $params);
            $datos->setDefaultUseImplicitFetch(true);
            $retorno = [];
            
            foreach ($datos as $citem) { $retorno[] = $citem->getData(); }

            
            echo PHP_EOL . PHP_EOL . ' Qty items - FB API'. count($retorno) . PHP_EOL;
          return $retorno;

        } catch (Exception $e) {
            
            print_r($e);
            $data = array( 'user_id' => $infoCredentials['user_id'], 'auth_id' => $infoCredentials['id'], 'add_acount' => $data['platform_object_id'] ,  'error' => serialize($e));
            extLogger($data);
            
        }

    

}

// creas
function get_stats_ad($adAccount_param, $data , $infoCredentials ){

    $fields = ['ad_id, adset_id, impressions', 'campaign_id', 'account_id', 'account_currency', 'buying_type', 'campaign_name', 'clicks', 'conversions', 'date_start', 'date_stop', 'objective', 'reach', 'spend', 'inline_post_engagement', 'actions', 'wish_bid', 'ad_bid_type', 'social_spend', 'video_thruplay_watched_actions', 'video_play_actions', 'ad_bid_value'];
    
    $params = [ 'level' => 'ad', 
                'time_range' => array('since' => substr($data['start_date'], 0, 10), 'until' => substr($data['end_date'], 0, 10)), 
                'time_increment' => '1', 
                'breakdowns' => 'publisher_platform'];
    
    $api = Api::init($infoCredentials['app_id'], $infoCredentials['app_secret'], $infoCredentials['access_token']);
            
        try {
            
            echo 'Stats CREAS | Ad Account: '.$adAccount_param .' from '.$data['start_date'].' to '. $data['end_date'] . PHP_EOL;
    
            $datos = ( new AdAccount("act_" . $adAccount_param ) )->getInsights($fields, $params);
            $datos->setDefaultUseImplicitFetch(true);
            $retorno = [];
            
            foreach ($datos as $citem) { 
                $dataIterator = $citem->getData(); 
                $retorno[] = $dataIterator;
            }

            helper_metrics_ads_day( $infoCredentials['platform'] , $infoCredentials['user_id'], $infoCredentials['customer_id_default'], $adAccount_param, $retorno );
            echo   PHP_EOL .' ------------------------------- ' . PHP_EOL . ' Qty items - FB API'. count($retorno) . PHP_EOL;

        } catch (Exception $e) {
            
            print_r($e);
            $data = array( 'user_id' => $infoCredentials['user_id'], 'auth_id' => $infoCredentials['id'], 'add_acount' => $adAccount_param,  'error' => serialize($e));
            extLogger($data);
            
        }

} 

// atomos
function get_stats_adSet($adAccount_param, $data , $infoCredentials ){

    $fields = ['impressions', 'campaign_id', 'account_id', 'account_currency', 'buying_type', 'campaign_name', 'clicks', 'conversions', 'date_start', 'date_stop', 'objective', 'reach', 'spend', 'inline_post_engagement', 'actions', 'wish_bid', 'ad_bid_type', 'social_spend', 'video_thruplay_watched_actions', 'video_play_actions', 'ad_bid_value'];
    // sacamos campaign_name --> falla
    $fields = ['impressions', 'campaign_id', 'account_id', 'account_currency', 'clicks', 'conversions', 'date_start', 'date_stop', 'objective', 'reach', 'spend',  'ad_bid_type', 'video_play_actions', 'ad_bid_value'];
    $fields = array_merge($fields, ['adset_name', 'adset_id']);

    $params = [ 'level' => 'adset', 
                'time_range' => array('since' => substr($data['start_date'], 0, 10), 'until' => substr($data['end_date'], 0, 10)), 
                'time_increment' => '1', 
                'breakdowns' => 'publisher_platform'];
    
    $api = Api::init($infoCredentials['app_id'], $infoCredentials['app_secret'], $infoCredentials['access_token']);

    try {

        echo 'Stats ATOMS | Ad Account: '.$adAccount_param .' from '.$data['start_date'].' to '. $data['end_date'] . PHP_EOL;

        $datos = ( new AdAccount("act_" . $adAccount_param ) )->getInsights($fields, $params);
        $datos->setDefaultUseImplicitFetch(true);
        $retorno = [];
        
        foreach ($datos as $citem) { 
            $retorno[] = $citem->getData(); 
        }
        
        helper_metrics_lineitem_day( $infoCredentials['platform'] , $infoCredentials['user_id'], $infoCredentials['customer_id_default'], $adAccount_param, $retorno );
        
        echo PHP_EOL  . ' Qty items - FB API'. count($retorno) . PHP_EOL;

    } catch (Exception $e) {
        print_r($e);
        $data = array( 'level'=> 'Exception', 'user_id' => $infoCredentials['user_id'], 'auth_id' => $infoCredentials['id'], 'add_acount' => $adAccount_param,  'error' => serialize($e));
        extLogger($data );

    }

}

 


function index(ServerRequestInterface $request){ 
//     function index(){

    global $dbconn;
   //datos demo
  //     $dataRequest=[ 'action' => 'get_stats_campaigns', 'auth_id'=> "6a9a0fd3-004e-11ec-8d81-ac1f6b17ff4a", 'ad_account_id' => '36894230'];
    if (isset($request)){
        $body = $request->getBody()->getContents();
        $dataRequest = json_decode($body, true);
        if(!isset($dataRequest['action'])){ echo 'No action defined in request'; die(); }
        if(!isset($dataRequest['auth_id'])){ echo 'No AUTH ID defined in request'; die(); }
    }

    // conformamos el request
    $data = array(
        'action'=> isset($dataRequest['action']) ? $dataRequest['action'] : null,
        'auth_id'=>isset($dataRequest['auth_id']) ? $dataRequest['auth_id'] : null, // 139 = 2ccd9123-d929-11eb-8d81-ac1f6b17ff4a
        'ad_account_id' => isset($dataRequest['ad_account_id']) ? $dataRequest['ad_account_id'] : null,  // VACIO
        'start_date'=> isset($dataRequest['start_date']) ? $dataRequest['start_date'] : date('Y-m-d', strtotime(' - 30 day')), // opcional
        'end_date'=> isset($dataRequest['end_date']) ? $dataRequest['end_date'] : date('Y-m-d', strtotime('today')) // opcional
    );

    //{"auth_id":"2ccd78b7-d929-11eb-8d81-ac1f6b17ff4a","action":"retrieve_all"}
    //{"auth_id":"2ccd78b7-d929-11eb-8d81-ac1f6b17ff4a","action":"get_stats_campaigns","ad_account_id":"649463701920592"}
    
 
    $query = "SELECT AUP.id, AUP.auths_user_id, AUP.user_id, AUP.platform, AUP.app_id, AUP.app_secret, AUP.platform_user_id,  AUP.access_token, AUP.refresh_token, U.name, U.email, U.customer_id_default, U.campaign_root_default  FROM auths_user_platform AUP JOIN users U ON (AUP.user_id = U.id)  WHERE AUP.public_id = '" . $data["auth_id"] . "';";
    $result = $dbconn->query($query);
    $infoCredentials = $result->fetch_assoc();
    
   // print_r($infoCredentials);

    if (isset($infoCredentials['user_id'])){
        $user_id = $infoCredentials['user_id'];
    }else{
        echo 'No AUTH ID found';
  $loggermessage = array( 'level'=> 'Error', 'source'=>'index' , 'message'=>  'No AUTH ID found',  'user_id' => $infoCredentials['user_id'], 'auth_id' => $infoCredentials['id'], 'add_acount' => $adAccount_param);
  //           extLogger($loggermessage );
        die();  
    }

    $api = Api::init($infoCredentials['app_id'], $infoCredentials['app_secret'], $infoCredentials['access_token']);
//echo print_r($api, true);

    // validamos token
    try {
    
      $fb = new \Facebook\Facebook([ 'app_id' => $infoCredentials['app_id'], 'app_secret' => $infoCredentials['app_secret'], 'default_graph_version' => 'v2.10',]);
// $loggermessage = array( 'level'=> 'Exception', 'source'=>'FacebookResponseException' , 'message'=> 'EXPIRADO' .  $e->getMessage(),  'user_id' => $infoCredentials['user_id'], 'auth_id' => $infoCredentials['id'], 'add_acount' => $adAccount_param,  'error' => serialize($e));
  //           extLogger($loggermessage );    
   //   echo print_r($fb, true);  
        try {
            // Get the \Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
          //  $response = $fb->get('/me', $infoCredentials["access_token"]);
          //chequear token vive
          //https://developers.facebook.com/docs/facebook-login/access-tokens/session-info-access-token
          
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
        
             $loggermessage = array( 'level'=> 'Exception', 'source'=>'FacebookResponseException' , 'message'=> 'EXPIRADO' .  $e->getMessage(),  'user_id' => $infoCredentials['user_id'], 'auth_id' => $infoCredentials['id'], 'payload'=>serialize($data) ,  'error' => serialize($e));
             extLogger($loggermessage );
          
            echo 'Graph returned an error: ' . $e->getMessage() . PHP_EOL;
            echo 'EXPIRADO' . PHP_EOL;
         $query = "UPDATE auths_user_platform set auths_user_platform.activa = 'EXPIRATE' WHERE auths_user_platform.public_id = '" . $dataRequest['auth_id'] . "'";
            $dbconn->query($query);
            echo 'EVENT MAIL NOTIFY USER TOKEN IS EXPIRATED' . PHP_EOL;
            exit;

        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
           $loggermessage = array( 'level'=> 'Exception', 'source'=>'FacebookResponseException' , 'message'=>   $e->getMessage(),  'user_id' => $infoCredentials['user_id'], 'auth_id' => $infoCredentials['id'], 'payload'=>serialize($data),  'error' => serialize($e));
             extLogger($loggermessage );
          
            exit;
        }

        if (isset($response)) {
         //   $me = $response->getGraphUser();
         //   echo 'Logged in as ' . $me->getName() . PHP_EOL;
            echo 'Logged in as ' . $me->getName() . PHP_EOL;
        } 

    }catch (Exception $e){
      
       $loggermessage = array( 'level'=> 'Exception', 'source'=>'GeneralException' , 'message'=>    $e->getMessage(),  'user_id' => $infoCredentials['user_id'], 'auth_id' => $infoCredentials['id'], 'payload'=>serialize($data),  'error' => serialize($e));
             extLogger($loggermessage );
          
        echo 'Exeption as ' . $e->getMessage() . PHP_EOL;
        exit;
    }

    if ($data['action']== null){ echo 'NO ACTION SPECIFIED'; 
  $loggermessage = array( 'level'=> 'info', 'source'=>'index' , 'message'=>  'NO ACTION SPECIFIED',  'user_id' => $infoCredentials['user_id'], 'auth_id' => $infoCredentials['id']);
             extLogger($loggermessage );                   
                                die(); }
       
  $loggermessage = array( 'level'=> 'info', 'source'=>'index' , 'payload'=>serialize($data) ,  'user_id' => $infoCredentials['user_id'], 'auth_id' => $infoCredentials['id']);
             extLogger($loggermessage );
       
    switch ($data['action']) {

        case "retrieve_all":        
            retrieve_all($data, $infoCredentials);
            break;

        case "get_campaigns":
            
            get_campaigns($data['ad_account_id'], $infoCredentials);
            break;

        case "get_properties":
            get_properties($data['ad_account_id'], $infoCredentials);
            break;

        case "get_pixels":
            get_pixels($data['ad_account_id'], $infoCredentials);
            break;

        case "get_adsets":
            get_adsets($data['ad_account_id'], $infoCredentials);
            break;

        case "get_creativities":
            get_creativities($data['ad_account_id'], $infoCredentials);
            break; 

        // STATS
        case "get_stats_campaigns":
            get_stats_campaigns($data['ad_account_id'], $data , $infoCredentials ); 
            break;    

        case "get_stats_adSet":
            get_stats_adSet($data['ad_account_id'], $data , $infoCredentials ); 
            break;  

        case "get_stats_ad":
            get_stats_ad($data['ad_account_id'], $data , $infoCredentials ); 
            break;  

        case "retrieve_stats_all":
            
            $query = "SELECT account_id FROM app_thesoci_9c37.ads_accounts where platform='FACEBOOK' and status='1|0' AND  auth_id in (select id from auths_user_platform where public_id = '".$data["auth_id"]."');";
            $result = $dbconn->query($query);
            
            while ($fila = $result->fetch_assoc()) {
                $event = array(  'subject_id' => $infoCredentials['id'], 'type' => 'get_stats_campaigns', 'function' => 'function-facebook-api',  'ad_account_id' =>  $fila['account_id'], 'period'=> '48H', 'nextcalls'=>['get_account_stats_adSet'=>['get_account_stats_ad'] ]);
                execGoogleTask($event);

                $event = array(  'subject_id' => $infoCredentials['id'], 'type' => 'get_stats_adSet', 'function' => 'function-facebook-api',  'ad_account_id' =>  $fila['account_id'] );
                execGoogleTask($event);
                $event = array(  'subject_id' => $infoCredentials['id'], 'type' => 'get_stats_ad', 'function' => 'function-facebook-api',  'ad_account_id' =>  $fila['account_id'] );
                execGoogleTask($event);
               
            }
            
            
            break;

        default:
            echo "NO ACTION FOUND";

    }

}

 
//index();

?>