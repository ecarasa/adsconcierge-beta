<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'vendor/autoload.php';
include 'configs/general.php';
include 'configs/stats_mapeometrics.php';

use FacebookAds\Api;
use FacebookAds\Cursor;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Post;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\User;
use FacebookAds\Http\Exception\ServerException;
use Psr\Http\Message\ServerRequestInterface;
Cursor::setDefaultUseImplicitFetch(false);




function get_adaccount_from_campaign_platform($campaign_id_en_platform){
    global $dbconn;

    $query = "SELECT * FROM app_thesoci_9c37.ads_accounts where id in (SELECT ad_account FROM campaigns_platform where id_en_platform = '".$campaign_id_en_platform."');";
    $res = $dbconn->query($query);
    $row = $res->fetch_assoc();

    return $row['account_id'];

}


//en la campaña laliga-engangement verano platform id 23847762372850502 de la adaccount platform id 702658056601156 de facebook
//(no tiene metricas esa campaña, se puede tocar lo que sea de ahi)

//primero haces un update_field, que  solo actualices el name
//luego vas ampliando, la idea era esto:

//function update_fields($auths, $itemid, $valores[campo=>valor]) {  sdsdds }

//function fb_update_name($public_id, $newname) { 
    //$itemid=   select platform_id from campaign_platform where public_id=$public_id and platform='facebook' limit 1;
    //update_fields($authid, $itemid, $valores['name'=>$newname]); 
//}

// a fb_update_name existe en laravel, a modo de apificacion (que puede ser un metodo unico para todas las plataformas y seria asi

// SELECT * FROM ads_accounts where ads_accounts.account_id = 702658056601156; 
// SELECT * FROM campaigns_platform where id_en_platform = 23847762372850502;


function fb_update_campaign_fields($fb, $infoCredentials, $campaign_id_en_platform, $fields) 
{

	try {		
		// endpoint definition
        $endpoint =  "/" . $campaign_id_en_platform;
        echo '** Graph version '. $fb->getDefaultGraphVersion() .PHP_EOL;
        $response = $fb->post($endpoint, $fields, $infoCredentials['access_token'], null, 'v10.0');
        var_dump($response);    

	} catch(Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
	}
	
    $graphNode = $response->getGraphNode();
     print_r($graphNode);
	// return $graphNode;
	/* handle the result */
}

function fb_update_campaign_atomo_fields($fb, $infoCredentials, $campaign_id_en_platform, $fields) 
{

        try {		
            // endpoint definition
            $endpoint =  "/" . $campaign_id_en_platform;
            echo '** Graph version '. $fb->getDefaultGraphVersion() .PHP_EOL;
            $response = $fb->post($endpoint, $fields, $infoCredentials['access_token'], null, 'v10.0');
            var_dump($response);    
    
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
        }
        
        $graphNode = $response->getGraphNode();
         print_r($graphNode);
        // return $graphNode;
        /* handle the result */
}

function fb_update_creatividad_fields($fb, $infoCredentials, $id_en_platform, $fields) 
{
    
    try {		
        // endpoint definition
        $endpoint =  "/" . $id_en_platform;
        echo '** Graph version '. $fb->getDefaultGraphVersion() .PHP_EOL;
        $response = $fb->post($endpoint, $fields, $infoCredentials['access_token'], null, 'v10.0');
        var_dump($response);    

    } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
    }
    
    $graphNode = $response->getGraphNode();
     print_r($graphNode);
    // return $graphNode;
    /* handle the result */
}

//https://developers.facebook.com/docs/marketing-api/reference/ad-account/campaigns/#ejemplo-2
function fb_creacampana($app_id, $app_secret,$access_token,$userid, $add_account_id,$rowdata) 
{

	$api = Api::init($app_id, $app_secret ,$access_token);
	$fields = array();
	$params = array(
	    'name' => $userid . '|' . $rowdata['id'] . ':' . $rowdata['name'] . ' ' . date('Y/m/d h:i:s a', time()),
	    'objective' => $rowdata['objective'], //ver esto https://developers.facebook.com/docs/reference/ads-api/adcampaign#create
	    //enum{APP_INSTALLS, BRAND_AWARENESS, CONVERSIONS, EVENT_RESPONSES, LEAD_GENERATION, LINK_CLICKS, LOCAL_AWARENESS, MESSAGES, OFFER_CLAIMS, PAGE_LIKES, POST_ENGAGEMENT, PRODUCT_CATALOG_SALES, REACH, VIDEO_VIEWS}
	    'status' => 'PAUSED',
	);
	$campananueva=(new AdAccount($add_account_id))->createCampaign($fields,  $params)->exportAllData();

	return $campananueva;
}

function campaign_platform_update($fb, $data, $infoCredentials) 
{ 
    
    echo '** PLATFORM '. $infoCredentials['platform'].PHP_EOL;
    print_r($data);

    switch($infoCredentials['platform']){
        case 'FACEBOOK':
            fb_update_campaign_fields($fb, $infoCredentials, $data['parent_platform_id'], $data['fields']); 
            break;
    }

}

function campaign_platform_atomo_update($fb, $data, $infoCredentials)
{

    echo '** PLATFORM '. $infoCredentials['platform'].PHP_EOL;
    print_r($data);

    switch($infoCredentials['platform']){
        case 'FACEBOOK':
            campaign_platform_atomo_update($fb, $infoCredentials, $data['parent_platform_id'], $data['fields']); 
            break;
    }

}

function creatividad_update($fb, $data, $infoCredentials)
{

    echo '** PLATFORM '. $infoCredentials['platform'].PHP_EOL;
    print_r($data);

    switch($infoCredentials['platform']){
        case 'FACEBOOK':
            fb_update_creatividad_fields($fb, $infoCredentials, $data['parent_platform_id'], $data['fields']); 
            break;
    }

}

function extLogger($data)
{

    $post_data = json_encode($data);
    
    $crl = curl_init('http://localhost:8000/api/external-log');
    
    curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($crl, CURLINFO_HEADER_OUT, true);
    curl_setopt($crl, CURLOPT_POST, true);
    curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);    
    curl_setopt($crl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($post_data)));
        
    $result = curl_exec($crl);
    echo ' >> SERVER LOG SENT!';
    curl_close($crl); 

}


//function index(ServerRequestInterface $request){
function index()
{

    global $dbconn;
   
    if (isset($request)){
        $body = $request->getBody()->getContents();
        $dataRequest = json_decode($body, true);

        if(!isset($dataRequest['action'])){
            echo 'No action defined in request';
            die();
        }

        if(!isset($dataRequest['auth_id'])){
            echo 'No AUTH ID defined in request';
            die();
        }

    }

    // conformamos el request
    $data = array(
        
        'auth_id'=>isset($dataRequest['auth_id']) ? $dataRequest['auth_id']: '2ccd6be6-d929-11eb-8d81-ac1f6b17ff4a',
        

        /*  // atomo
        'action'=> isset($dataRequest['action']) ? $dataRequest['action']: 'campaign_platform_atomo_update',
        'parent_platform_id' => isset($dataRequest['parent_platform_id']) ? $dataRequest['parent_platform_id'] : '23847762373380502', //23847762373380502
        'fields' => isset($dataRequest['fields']) ? $dataRequest['fields'] : array(  'name' => 'new_adset_demo', 'budget' => '15', 'special_ad_categories' => '[]' ),  
         */
        // creas
        'action'=> isset($dataRequest['action']) ? $dataRequest['action']: 'creatividad_update',
        'parent_platform_id' => isset($dataRequest['parent_platform_id']) ? $dataRequest['parent_platform_id'] : '23847762374010502', //23847762373380502
        'fields' => isset($dataRequest['fields']) ? $dataRequest['fields'] : array(  'name' => 'MEMO OCHOA TEST UPDATE', 'special_ad_categories' => '[]' ),  

    );
    
    $query = "SELECT AUP.id, AUP.auths_user_id, AUP.user_id, AUP.platform, AUP.app_id, AUP.app_secret, AUP.platform_user_id,  AUP.access_token, AUP.refresh_token, U.name, U.email, U.customer_id_default, U.campaign_root_default  FROM auths_user_platform AUP JOIN users U ON (AUP.user_id = U.id)  WHERE AUP.public_id = '" . $data["auth_id"] . "';";
    $result = $dbconn->query($query);
    $infoCredentials = $result->fetch_assoc();

    if (isset($infoCredentials['user_id'])){
        $user_id = $infoCredentials['user_id'];
    }else{
        echo 'No AUTH ID found';
        die();  
    }

    $api = Api::init($infoCredentials['app_id'], $infoCredentials['app_secret'], $infoCredentials['access_token']);

    if ($data['action']== null){
        echo 'NO ACTION SPECIFIED';
        die();
    }

    try {
    
        $fb = new \Facebook\Facebook(
                                        [ 'app_id' => $infoCredentials['app_id'], 
                                        'app_secret' => $infoCredentials['app_secret'], 
                                        'default_graph_version' => 'v10.0'] 
                                    );
            
          try {
              $response = $fb->get('/me', $infoCredentials["access_token"]);
          } catch(\Facebook\Exceptions\FacebookResponseException $e) {
              echo 'Graph returned an error: ' . $e->getMessage() . PHP_EOL;
              echo 'EXPIRADO' . PHP_EOL;
             /*  $query = "UPDATE auths_user_platform set auths_user_platform.activa = 'EXPIRATE' WHERE auths_user_platform.id = '" . $infoCredentials['id'] . "'";
              $dbconn->query($query); */
              echo 'EVENT MAIL NOTIFY USER TOKEN IS EXPIRATED' . PHP_EOL;
              exit;
  
          } catch(\Facebook\Exceptions\FacebookSDKException $e) {
              echo 'Facebook SDK returned an error: ' . $e->getMessage();
              exit;
          }
  
          if (isset($response)) {
              $me = $response->getGraphUser();
              echo 'Logged in as ' . $me->getName() . PHP_EOL . PHP_EOL;
          } 
  
      }catch (Exception $e){
          echo 'Exeption as ' . $e->getMessage() . PHP_EOL;
          exit;
      }



    switch ($data['action']) {

        case "campaign_platform_update":        
            campaign_platform_update($fb, $data, $infoCredentials);
            break;


        case "campaign_platform_atomo_update":
            campaign_platform_atomo_update($fb, $data, $infoCredentials);
            break;


        case "creatividad_update":
            creatividad_update($fb, $data, $infoCredentials);
            break;

            
        default:
            echo "NO ACTION FOUND";

    }

}

/*** InfoCredentials
 * 
    ["id"] => string(3) "110"
    ["auths_user_id"] => string(32) "------"
    ["user_id"] => string(1) "6"
    ["platform"] => string(8) "FACEBOOK"
    ["app_id"] => string(16) "2230398120518804"
    ["app_secret"] => string(32) "----"
    ["platform_user_id"] => string(17) "10215767923927052"
    ["access_token"] => string(188) "EAAfsiQeuZAJQBAA8KBYnjiEenZBycv4vqh9z7KvZBQjdr8ZANLZBGZC9mk9e8dl8ssiNU9c6DdZAnwL4sTUlt7qNF3n5LeUY7YeOnb07zxkT2Hla5yJBswePGCs7ZCgJgOWZCz4fEoLZAhWFHGnlUybwFwWkw3TAf9kNfY1lZB58x3K4fv2Ac51qJhL"
    ["refresh_token"] => NULL
    ["name"] => string(2) "JR"
    ["email"] => string(19) "admin@turecarga.com"
    ["customer_id_default"] =>  string(2) "32"
    ["campaign_root_default"] => string(1) "1"
  *
  
*/

index();

?>