<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-develop',
    'version' => 'dev-develop',
    'aliases' => 
    array (
    ),
    'reference' => '04187953c3471c2f071e507aa9375d72a1c43e12',
    'name' => 'laravel/laravel',
  ),
  'versions' => 
  array (
    'facebook/graph-sdk' => 
    array (
      'pretty_version' => '5.7.0',
      'version' => '5.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2d8250638b33d73e7a87add65f47fabf91f8ad9b',
    ),
    'facebook/php-business-sdk' => 
    array (
      'pretty_version' => '10.0.1',
      'version' => '10.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a4b73f96770c28012a84736c75bc87e52d6948b9',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v5.4.0',
      'version' => '5.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2113d9b2e0e349796e72d2a63cf9319100382d2',
    ),
    'google/apiclient' => 
    array (
      'pretty_version' => 'v2.10.1',
      'version' => '2.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '11871e94006ce7a419bb6124d51b6f9ace3f679b',
    ),
    'google/apiclient-services' => 
    array (
      'pretty_version' => 'v0.204.0',
      'version' => '0.204.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c718661b22f9b92e0d89520dbb8f16c62307885a',
    ),
    'google/auth' => 
    array (
      'pretty_version' => 'v1.16.0',
      'version' => '1.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c747738d2dd450f541f09f26510198fbedd1c8a0',
    ),
    'google/cloud-core' => 
    array (
      'pretty_version' => 'v1.42.2',
      'version' => '1.42.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3fff3ca4af92c87eb824e5c98aaf003523204a2',
    ),
    'google/cloud-functions-framework' => 
    array (
      'pretty_version' => 'v0.7.2',
      'version' => '0.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1bf9f428d54d383da8c203f89ad6b186ba5c3d9e',
    ),
    'google/cloud-pubsub' => 
    array (
      'pretty_version' => 'v1.32.0',
      'version' => '1.32.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b3efb7463d47664d68248ff15ccbaa1eb19dc396',
    ),
    'google/cloud-scheduler' => 
    array (
      'pretty_version' => 'v1.5.4',
      'version' => '1.5.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '319a7929fd2960920cecd0f87a0a8358cbba21bd',
    ),
    'google/cloud-storage' => 
    array (
      'pretty_version' => 'v1.24.1',
      'version' => '1.24.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '440e195a11dbb9a6a98818dc78ba09857fbf7ebd',
    ),
    'google/cloud-tasks' => 
    array (
      'pretty_version' => 'v1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7dda80965d679c60b3403f14e8e34c690b8cac4f',
    ),
    'google/common-protos' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c348d1545fbeac7df3c101fdc687aba35f49811f',
    ),
    'google/crc32' => 
    array (
      'pretty_version' => 'v0.1.0',
      'version' => '0.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8525f0dea6fca1893e1bae2f6e804c5f7d007fb',
    ),
    'google/gax' => 
    array (
      'pretty_version' => 'v1.7.1',
      'version' => '1.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '48cd41dbea7b8fece8c41100022786d149de64ca',
    ),
    'google/grpc-gcp' => 
    array (
      'pretty_version' => '0.1.5',
      'version' => '0.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb9bdbf62f6ae4e73d5209d85b1d0a0b9855ff36',
    ),
    'google/protobuf' => 
    array (
      'pretty_version' => 'v3.17.3',
      'version' => '3.17.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae9282cf11dd2933b7e71a611f9590f07d53d3f3',
    ),
    'grpc/grpc' => 
    array (
      'pretty_version' => '1.39.0',
      'version' => '1.39.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '101485614283d1ecb6b2ad1d5b95dc82495931db',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.3.0',
      'version' => '7.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7008573787b430c1c1f650e3722d9bba59967628',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e7d04f1f6450fef59366c399cfad4b9383aa30d',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc960a912984efb74d0a90222870c72c87f10c91',
    ),
    'laravel/laravel' => 
    array (
      'pretty_version' => 'dev-develop',
      'version' => 'dev-develop',
      'aliases' => 
      array (
      ),
      'reference' => '04187953c3471c2f071e507aa9375d72a1c43e12',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.3.2',
      'version' => '2.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '71312564759a7db5b789296369c1a264efc43aad',
    ),
    'paragonie/constant_time_encoding' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f34c2b11eb9d2c9318e13540a1dbc2a3afbd939c',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '3.0.9',
      'version' => '3.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a127a5133804ff2f47ae629dd529b129da616ad7',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'rize/uri-template' => 
    array (
      'pretty_version' => '0.3.3',
      'version' => '0.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e0b97e00e0f36c652dd3c37b194ef07de669b82',
    ),
  ),
);
