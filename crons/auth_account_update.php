<?php
//cron cada hora

require_once __DIR__ . '/../public/apis/configs/general.php';
require __DIR__ . '/../public/apis/apis_curls/LinkedInApi.php';
//require __DIR__ . '/../public/apis/apis_curls/facebookApi.php';
require __DIR__ . '/../social-apis/facebook/entities/facebookApi.php';


use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;

/***
if($argv) {
$argv[1]=platform
$argv[2]=user_id
$argv[3]=app_id
$argv[4]=id
}
"1"    "FACEBOOK"
"2"    "LINKEDIN"
"3"    "TWITTER"
"4"    "INSTAGRAM"
"5"    "SNAPCHAT"
"6"    "GANALYTICS"
"7"    "SUNNATIVO"
"8"    "FB Insights"
"9"    "PINTEREST"
"10""ADWORDS
 **/

// Traer las cuentas, campañas, adgroups y creatividades
// Refrescar tokens de auth2

$id_auths_user_platform = "";

if (isset($argv[1])) {
    $id_auths_user_platform = $argv[1];
}

if ($id_auths_user_platform == "") {
    $sql_query = "SELECT AUP.id, AUP.auths_user_id, AUP.user_id, AUP.platform, AUP.app_id, AUP.app_secret, AUP.platform_user_id, AUP.access_token, AUP.refresh_token, U.name, U.email, U.customer_id_default, U.campaign_root_default, U.public_id as user_public_id
	FROM auths_user_platform AUP JOIN users U ON (AUP.user_id = U.id) where U.id = 6 and platform = 'FACEBOOK' order by AUP.id";
} else {
    $sql_query = "SELECT AUP.id, AUP.auths_user_id, AUP.user_id, AUP.platform, AUP.app_id, AUP.app_secret, AUP.platform_user_id, AUP.access_token, AUP.refresh_token,AUP.expiretime, U.name, U.email, U.customer_id_default, U.campaign_root_default, U.public_id as user_public_id
	FROM auths_user_platform AUP
    JOIN users U ON (AUP.user_id = U.id) WHERE AUP.id = " . $id_auths_user_platform;
}

$stmt = $dbconn->prepare($sql_query);
$stmt->execute();
$authsresults = $stmt->get_result();


/* now you can fetch the results into an array - NICE */

while ($auths = $authsresults->fetch_assoc()) {

    echo "USER ID ". $auths['user_id'] . PHP_EOL; 
    echo "Auth ID ". $auths['id'] . PHP_EOL; 
    echo 'PLATAFORMA:' . $auths['platform'] . "\n\r";

    switch ($auths['platform']) {

        case "FACEBOOK":
            try {
                
                facebook_account_retrieve_all($credentials = ['app_id' => $auths['app_id'], 'app_secret' => $auths['app_secret'], 'access_token' => $auths['access_token'], 'platform_user_id' => $auths['platform_user_id']], $auths['user_id'], $auths);

            } catch (Exception $e) {
                echo 'Excepción capturada: ', $e->getMessage(), "\n";
            }
            break;

        case "FBINSIGHTS":
            try {
                //facebook_account_retrieve_all($credentials = ['app_id' => $auths['app_id'], 'app_secret' => $auths['app_secret'], 'access_token' => $auths['access_token'], 'platform_user_id' => $auths['platform_user_id']], $auths['user_id'], $auths);
            } catch (Exception $e) {
                echo 'Excepción capturada: ', $e->getMessage(), "\n";
            }
            break;

        case "LINKEDIN":

            if ($auths['refresh_token'] != "") {
                $token = refresh_token($auths['app_id'], $auths['app_secret'], $auths['refresh_token']);
                // print_R($auths);
                // print_R($token);
                // update access_token, refresh_token y retorno en db el id del registro
                // $refresh_token = $dbconn->prepare("UPDATE auths_user_platform SET access_token = ?, refresh_token = ?, retorno = ? WHERE platform = 'LINKEDIN' AND user_id = ?"); old-deprecated
                $refresh_token = $dbconn->prepare("UPDATE auths_user_platform SET access_token = ?, refresh_token = ?,  expiretime = ?,  retorno = ? WHERE id = ?");
                $refresh_token->bind_param("ssssi", ...[$token->access_token, $token->refresh_token, $token->expires_in, json_encode($token), $auths['id']]);
                $refresh_token->execute();

                $auths['access_token'] = $token->access_token ;
                $accounts = ads_accounts_get_linkedin(null, $auths['access_token'], $auths['user_id'], $auths);

                // obtenemos entidades y propiedades de la cuenta de linkedin
                $subadsaccount = $dbconn->prepare("INSERT INTO ads_accounts (user_id, platform, app_id, account_id, name, platform_user_id, status, currency, metadata, customer_id, auth_id) values (?,?,?,?,?,?,?,?,?,?,?) on duplicate key update status=?, metadata=? ");
                            
                $properties = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` (`user_id`, `platform`, `adaccount_id`, `app_id`,`platform_user_id`, `id_en_platform`, `token`,`name`,`status`, `category`, `currency`, `metadata`, `auth_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `name`= ?, `adaccount_id`= ? , `status`= ?, `platform_user_id`=?,  `metadata`= ? ");

                $cuentasarray = [];

                foreach ($accounts as $item) {
 
                    $adaccount_name= $item->name;
                    $ad_accountid= $item->id;
                    //print_r([$auths['user_id'], $auths['platform'], $item->id, $auths["app_id"], $auths['platform_user_id'], $item->id, $auths['access_token'], $item->name, $item->status . '>' . $item->servingStatuses[0], $item->type, $item->currency, json_encode($item), $item->name, $item->id, $item->status . '>' . $item->servingStatuses[0], $auths['platform_user_id'], json_encode($item)]);
               
                    $cuentasarray[$item->id] = $item;
                  
                    $pagina=linkedin_retrieve_organization(null, $auths['access_token'], $auths['user_id'], array('organizationlookup'=> $item->reference  ) );
                            
                    $subadsaccount->bind_param("sssssssssssss", ...[$auths['user_id'], $auths['platform'], $auths['app_id'], $item->id, $adaccount_name, $auths['platform_user_id'], $item->status . '>' . $item->servingStatuses[0], $item->currency, json_encode($item), $auths["customer_id_default"], $auths["id"], $item->status . '>' . $item->servingStatuses[0], json_encode($item)]);
                    $subadsaccount->execute();
                    
                    if ($subadsaccount->error != "") {
                        print_r("Error: %s.\n", $subadsaccount->error);
                    }

                    $query = "SELECT id as adacc_id, public_id as adaccount_public_id FROM `app_thesoci_9c37`.`ads_accounts` WHERE auth_id = '{$auths["id"]}' and platform = '{$auths["platform"]}' and platform_user_id= '{$auths['platform_user_id']}' and account_id = '{$ad_accountid}' limit 1    ";
       
                    $res_ac = $dbconn->query($query);
                    $id_ac = $res_ac->fetch_assoc();
 
                    $pagename=isset($pagina->localizedName) ? $pagina->localizedName :  $adaccount_name;                  
                    $item->pagedata=$pagina;             
                    $page_id=$item->reference;                  
                    $properties->bind_param("ssssssssssssssssss", ...[$auths['user_id'], $auths['platform'],  $id_ac["adacc_id"], $auths["app_id"], $auths['platform_user_id'],$page_id , '-', $pagename, $item->status . '>' . $item->servingStatuses[0], $item->type, $item->currency, json_encode($item),$auths['id'], $pagename,  $id_ac["adacc_id"], $item->status . '>' . $item->servingStatuses[0], $auths['platform_user_id'], json_encode($item)]);
                    $properties->execute();
                    
                    if ($properties->error != "") {
                        print_r("Error: %s.\n", $properties->error);
                    }

                    $query = "SELECT id as pageid, public_id as property_public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE  auth_id = '{$auths["id"]}' and platform = '{$auths["platform"]}' and platform_user_id= '{$auths['platform_user_id']}' and id_en_platform = '{$page_id}'";
                    $res_pa = $dbconn->query($query);
                    $id_pa = $res_pa->fetch_assoc();                  
     
                    /*** Inserto registros en tabla de relaciones Properties y Ads Accounts
                        $query = "SELECT auths_user_id FROM `app_thesoci_9c37`.`auths_user_platform` WHERE id = " .$auths["id"];
                        $res_aup = $dbconn->query($query);
                        $id_aup = $res_aup->fetch_assoc();
                        $id_aup = isset($id_aup["auths_user_id"]) ? $id_aup["auths_user_id"] : "";
                        $query = "SELECT public_id FROM `app_thesoci_9c37`.`ads_accounts` WHERE account_id LIKE '" .$item->id ."'";
                        $res_ac = $dbconn->query($query);
                        $id_ac = $res_ac->fetch_assoc();
                        $id_ac = isset($id_ac["public_id"]) ? $id_ac["public_id"] : "";
                        $query = "SELECT public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE id_en_platform LIKE '" .$item->id ."'";
                        $res_pa = $dbconn->query($query);
                        $id_pa = $res_pa->fetch_assoc();
                        $id_pa = isset($id_pa["public_id"]) ? $id_pa["public_id"] : "";
                        $query = "SELECT public_id FROM `app_thesoci_9c37`.`users` WHERE id = " .$auths["user_id"];
                        $res_u = $dbconn->query($query);
                        $id_u = $res_u->fetch_assoc();
                        $id_u = isset($id_u["public_id"]) ? $id_u["public_id"] : "";
                        $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` (`auth_id`, `auth_publicid`, `ad_account`, `ad_account_publicid`, `ad_account_name`, `property_id`, `property_publicid`, `property_name`, `user_id`, `user_publicid`) VALUES (?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");
                        $stmt3->bind_param("isssssssisss", $auths["id"], $id_aup, $item->id, $id_ac, $item->name, $item->id, $id_pa, $item->name, $auths["user_id"], $id_u, $item->name, $item->name);
                        $stmt3->execute();
                    **/

                    $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` 
                    (`type`,`auth_id`, `ad_account`,  `ad_account_name`,`property_name`, `property_id`, `user_id`,`platform`,`auth_publicid`,   `user_publicid`,  `ad_account_publicid`,`property_publicid`) 
                    VALUES ('PAGE', ?,?,?,?,?,?,?,?,?,?,?)
                    ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");              
                  
                    $stmt3->bind_param("sssssssssssss", 
                                        $auths["id"], 
                                        $id_ac["adacc_id"] ,
                                        $adaccount_name,
                                        $pagename, 
                                        $id_pa["pageid"],      
                                        $auths['user_id'],
                                        $auths["platform"],                   
                                        $auths["auths_user_id"],
                                        $auths["user_public_id"],
                                        $id_ac["adaccount_public_id"], 
                                        $id_pa["property_public_id"],  
                                        $adaccount_name, 
                                        $pagename);
                    
                                        $stmt3->execute();                  
                      if ($stmt3->error != "") {
                        print_r("Error: %s.\n", $subadsaccount->error);
                    }
                
                }

                $campaigns = campaigns_get_linkedin(null, $auths['access_token'], $auths['user_id'], null);
                $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (user_id, platform, app_id, auth_id, name, id_en_platform, budget, status, currency, metadata, ad_account, campana_root, customer_id, source)
													values (?,?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id =? and ads_accounts.user_id=?), ?, ?, 'IMPORTED')
														on duplicate key update status = ?, metadata = ?");

                foreach ($campaigns as $item) {

                    $item = (object) $item;
                    $accountbb = str_replace('urn:li:sponsoredAccount:', '', $item->account);
                    $stmtcampana->bind_param("issssssssssissss", ...[$auths['user_id'], $auths['platform'], $auths['app_id'], $auths['id'], $item->name, $item->id, null, unificastatus('campana', $auths['platform'], $item->status, ['estado' => $item->status, 'otros' => $item->servingStatuses[0], 'record' => $item]),
                     $cuentasarray[$accountbb]->currency, json_encode($item), $accountbb, $auths['user_id'], $auths["campaign_root_default"], $auths["customer_id_default"], unificastatus('campana', $auths['platform'], $item->status, ['estado' => $item->status, 'otros' => $item->servingStatuses[0], 'record' => $item]), json_encode($item)]);

                    $stmtcampana->execute();
                    if ($stmtcampana->error != "") {
                        print_r("Error: %s.\n", $stmtcampana->error);
                    }
                }
            }
            break;

        case "TWITTER":
            try {
              
                $auth_settings = json_decode($auths['app_secret']);

                $twitterapi = TwitterAds::init($auth_settings->consumer_key, $auth_settings->consumer_secret, $auths['app_id'], $auths['access_token']);
                $accounts = $twitterapi->getAccounts();

                $subadsaccount = $dbconn->prepare("INSERT INTO ads_accounts (user_id, platform, app_id, account_id, name, platform_user_id, status, currency, metadata, customer_id, auth_id) values (?,?,?,?,?,?,?,?,?,?,?) on duplicate key update status=?, metadata=? ");
                $cuentasarray = [];

               
                foreach ($accounts as $account) {

                    $account->read();
                    $funding = $account->getFundingInstruments();
                    $adaccount_name=$account->getName();
                    $ad_accountid= $account->getId();
                   // print_r($account );
                    $data = array(
                        "id" => $account->getId(),
                        "salt" => $account->getSalt(),
                        "name" => $account->getName(),
                        "timezone" => $account->getTimezone(),
                        "timezone_switch_at" => $account->getTimezoneSwitchAt(),
                        "created_at" => $account->getCreatedAt(),
                        "updated_at" => $account->getUpdatedAt(),
                        "deleted" => $account->getDeleted(),
                        "approval_status" => $account->getApprovalStatus(),
                        "business_id" => $account->getBusinessId(),
                        "business_name" => $account->getBusinessName(),
                        "industry_type" => $account->getIndustryType(),
                        "getPromotableUsers" => $account->getPromotableUsers(),
                        "funding_instrument" => $funding->getCollection(),
                       
                    );
                 
                    $data = (object) $data;
                    // print_r( $data);
                    $cuentasarray[$account->getId()] = $account;

                    $subadsaccount->bind_param("issssssssssss", ...[
                                                                        $auths['user_id'], 
                                                                        $auths['platform'], 
                                                                        $auths['app_id'], 
                                                                        $ad_accountid,
                                                                        $adaccount_name, 
                                                                        $auths['platform_user_id'], 
                                                                        $account->getApprovalStatus(),
                                                                        $funding->getCollection()[0]->getCurrency(),
                                                                        json_encode($data), 
                                                                        $auths["customer_id_default"],
                                                                        $auths["id"], 
                                                                        $account->getApprovalStatus(),
                                                                        json_encode($data)
                                                                                                                    ]);

                    $subadsaccount->execute();
                    $query = "SELECT id as adacc_id, public_id as adaccount_public_id FROM `app_thesoci_9c37`.`ads_accounts` WHERE auth_id = '{$auths["id"]}' and platform = '{$auths["platform"]}' and platform_user_id= '{$auths['platform_user_id']}' and account_id = '{$ad_accountid}' limit 1    ";
       
                    $res_ac = $dbconn->query($query);
                    $id_ac = $res_ac->fetch_assoc();
                  
                    $properties = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_accounts` (`type`, `user_id`, `platform`, `adaccount_id`, 
                    `app_id`,`platform_user_id`, `id_en_platform`, `token`,`name`,`status`, `category`, `currency`, `metadata`, `auth_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `name`= ?, `adaccount_id`= ? , `status`= ?, `platform_user_id`=?,  `metadata`= ? ");
            
                    $pagename= $account->getName();  
                    $page_id = $account->getId();

                    //echo 'properties ' .  print_r($account->getProperties() , true);
                    //      echo 'getCollection ' .  print_r($account->getPromotableUsers()->getCollection()[0] , true);
                    //      echo 'get_class_methods  ' .  print_r(get_class_methods ($account->getPromotableUsers()) , true);
                    //     echo 'getPromotableUsers ' .  print_r(get_object_vars($account->getPromotableUsers()) , true);
                    // echo 'get_class_vars  ' .  print_r(get_class_vars ($account->getPromotableUsers()) , true);
          
                    $properties->bind_param("sssssssssssssssssss", ...[ "PAGE",
                                                                        $auths['user_id'], 
                                                                        $auths['platform'],
                                                                        $id_ac["adacc_id"], 
                                                                        $auths["app_id"], 
                                                                        $auths['platform_user_id'], 
                                                                        $account->getId(),
                                                                        '-',
                                                                        $pagename,
                                                                        $account->getApprovalStatus(),
                                                                        $account->getIndustryType(),
                                                                        $funding->current()->getCurrency(),
                                                                        json_encode(get_object_vars($account)),
                                                                        $auths['id'],
                                                                        $pagename, 
                                                                        $id_ac["adacc_id"], 
                                                                        $account->getApprovalStatus(), 
                                                                        $auths['platform_user_id'], 
                                                                        json_encode(get_object_vars($account))
                                                                    ]);
                    $properties->execute();

                    $query = "SELECT id as pageid, public_id as property_public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE  auth_id = '{$auths["id"]}' and platform = '{$auths["platform"]}' and platform_user_id= '{$auths['platform_user_id']}' and id_en_platform = '{$page_id}'";
                    $res_pa = $dbconn->query($query);
                    $id_pa = $res_pa->fetch_assoc();   
                    //  echo 'current '.   print_r( $funding->current() , true);
                    
                    foreach (  $funding->getCollection() as $item ) {
                        
                        $itemb=get_object_vars($item) ;
                        
                        $properties->bind_param("sssssssssssssssssss", ...[ "FUNDINGINSTRUMENT",
                                                                    $auths['user_id'], 
                                                                    $auths['platform'],
                                                                    $id_ac["adacc_id"], 
                                                                    $auths["app_id"], 
                                                                    $auths['platform_user_id'], 
                                                                    $item->getId(),
                                                                    '-',
                                                                    $item->getDescription(),
                                                                    $itemb['entity_status'],
                                                                    "-",
                                                                    $item->getCurrency(),
                                                                    json_encode(array($item,$itemb)),
                                                                    $auths['id'],
                                                                    $item->getDescription(), 
                                                                    $id_ac["adacc_id"], 
                                                                    $itemb['entity_status'], 
                                                                    $auths['platform_user_id'], 
                                                                    json_encode(array($item,$itemb))
                                                                ] );
                        $properties->execute();
                    }
                   
                    /***
                     Inserto registros en tabla de relaciones Properties y Ads Accounts
                    $query = "SELECT auths_user_id FROM `app_thesoci_9c37`.`auths_user_platform` WHERE id = " .$auths["id"];
                    $res_aup = $dbconn->query($query);
                    $id_aup = $res_aup->fetch_assoc();
                    $id_aup = isset($id_aup["auths_user_id"]) ? $id_aup["auths_user_id"] : "";

                    $query = "SELECT public_id FROM `app_thesoci_9c37`.`ads_accounts` WHERE account_id LIKE '" .$account->getId() ."'";
                    $res_ac = $dbconn->query($query);
                    $id_ac = $res_ac->fetch_assoc();
                    $id_ac = isset($id_ac["public_id"]) ? $id_ac["public_id"] : "";

                    $query = "SELECT public_id FROM `app_thesoci_9c37`.`properties_accounts` WHERE id_en_platform LIKE '" .$account->getId() ."'";
                    $res_pa = $dbconn->query($query);
                    $id_pa = $res_pa->fetch_assoc();
                    $id_pa = isset($id_pa["public_id"]) ? $id_pa["public_id"] : "";

                    $query = "SELECT public_id FROM `app_thesoci_9c37`.`users` WHERE id = " .$auths["user_id"];
                    $res_u = $dbconn->query($query);
                    $id_u = $res_u->fetch_assoc();
                    $id_u = isset($id_u["public_id"]) ? $id_u["public_id"] : "";


                    $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` (`auth_id`, `auth_publicid`, `ad_account`, `ad_account_publicid`, `ad_account_name`, `property_id`, `property_publicid`, `property_name`, `user_id`, `user_publicid`) VALUES (?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");
                    $stmt3->bind_param("isssssssisss", $auths["id"], $id_aup, $account->getId(), $id_ac, $account->getName(), $account->getId(), $id_pa, $account->getName(), $auths["user_id"], $id_u, $account->getName(), $account->getName());
                    ***/
                    //    $stmt3->execute();
                    $stmt3 = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`properties_adsaccount_relations` 
                                                (`type`,`auth_id`, `ad_account`,  `ad_account_name`,`property_name`, `property_id`, `user_id`,`platform`,`auth_publicid`,   `user_publicid`,  `ad_account_publicid`,`property_publicid`) 
                                                VALUES ('PAGE', ?,?,?,?,?,?,?,?,?,?,?)
                                                ON DUPLICATE KEY UPDATE `ad_account_name` = ?, `property_name` = ?");              
                    $stmt3->bind_param("sssssssssssss", 
                                                        $auths["id"], 
                                                        $id_ac["adacc_id"] ,
                                                        $adaccount_name,
                                                        $pagename, 
                                                        $id_pa["pageid"],      
                                                        $auths['user_id'],
                                                        $auths["platform"],                   
                                                        $auths["auths_user_id"],
                                                        $auths["user_public_id"],
                                                        $id_ac["adaccount_public_id"], 
                                                        $id_pa["property_public_id"],  
                                                        $adaccount_name, 
                                                        $pagename);
                    $stmt3->execute();                  
                    
                    if ($stmt3->error != "") {
                        print_r("Error: %s.\n", $subadsaccount->error);
                    }
                  
                    $account->read();
                    $campaigns = $account->getCampaigns();

                    $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (user_id, platform, app_id, auth_id, name, id_en_platform, budget, status, currency, metadata, ad_account, campana_root, customer_id, source)
														values (?,?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id =? and ads_accounts.user_id=?), ?, ?, 'IMPORTED' )
														on duplicate key update status=?, metadata=?");
 
                   foreach ($campaigns as $campaign) {

                        $item = array(
                            "id" => $campaign->getId(),
                            "reasons_not_servable" => $campaign->getReasonsNotServable(),
                            "servable" => $campaign->getServable(),
                            "created_at" => $campaign->getCreatedAt(),
                            "updated_at" => $campaign->getUpdatedAt(),
                            "deleted" => $campaign->getDeleted(),
                            "currency" => $campaign->getCurrency(),
                            "properties" => $campaign->getProperties(),
                            "name" => $campaign->getName(),
                            "funding_instrument_id" => $campaign->getFundingInstrumentId(),
                            "start_time" => $campaign->getStartTime(),
                            "end_time" => $campaign->getEndTime(),
                            "entity_status" => $campaign->getEntityStatus(),
                            "standard_delivery" => $campaign->getStandardDelivery(),
                            "daily_budget_amount_local_micro" => $campaign->getDailyBudgetAmountLocalMicro(),
                            "total_budget_amount_local_micro" => $campaign->getTotalBudgetAmountLocalMicro(),
                            "duration_in_days" => $campaign->getDurationInDays(),
                            "frequency_cap" => $campaign->getFrequencyCap(),
                        );
                        $item = (object) $item;

                        $stmtcampana->bind_param("issssssssssissss", ...[$auths['user_id'], $auths['platform'], $auths['app_id'], $auths['id'],
                            $item->name, $item->id, null, unificastatus('campana', $auths['platform'], $item->entity_status, ['estado' => $item->entity_status, 'otros' => "", 'record' => $item]), $item->currency, json_encode($item), $data->id, $auths['user_id'], $auths["campaign_root_default"], $auths["customer_id_default"], unificastatus('campana', $auths['platform'], $item->entity_status, ['estado' => $item->entity_status, 'otros' => "", 'record' => $item]), json_encode($item)]);

                        $stmtcampana->execute();
                        if ($stmtcampana->error != "") {
                            print_r("Error: %s.\n", $stmtcampana->error);
                        }
                    }

                }


            } catch (Exception $e) {
                echo 'Excepción capturada: ', $e->getMessage(), "\n";
                print_r($auths);
            }
            break;
    }
}

echo "updates generales\n";

$dbconn->query("UPDATE auths_user_platform
					SET count_ads = (
						SELECT COUNT(1)
							FROM ads_accounts
							WHERE auths_user_platform.app_id = ads_accounts.app_id AND
							auths_user_platform.platform = ads_accounts.platform AND
							auths_user_platform.platform_user_id = ads_accounts.platform_user_id)");

$dbconn->query("UPDATE auths_user_platform
						SET count_properties = (
						   	SELECT COUNT(1)
								FROM properties_accounts
								WHERE auths_user_platform.app_id = properties_accounts.app_id AND
								auths_user_platform.platform = properties_accounts.platform AND
								auths_user_platform.platform_user_id = properties_accounts.platform_user_id
					)");

$dbconn->query("UPDATE
						(SELECT campanaid, SUM(cost) AS 'cost', SUM(impressions) as 'impressions', SUM(clicks) as 'clicks', SUM(conversions) as 'conversions' FROM adsconcierge_stats.platform_campana_day
						GROUP BY campanaid) as PCD, app_thesoci_9c37.campaigns_platform CP
						SET
						CP.spent = PCD.cost, CP.impression = PCD.impressions, CP.clicks = PCD.clicks, CP.conversion = PCD.conversions,
						CP.cpc = IF(PCD.clicks > 0, PCD.cost/PCD.clicks, 0) ,
						CP.ctr = IF(PCD.impressions > 0, (PCD.clicks/PCD.impressions)*100, 0),
						CP.cpm = IF(PCD.impressions > 0, PCD.cost/(PCD.impressions/1000), 0),
						CP.cpa = IF(PCD.conversions > 0, PCD.cost/PCD.conversions, 0)
						WHERE PCD.campanaid = CP.id");

$dbconn->query("UPDATE
						(SELECT customer_id, SUM(cost) AS 'cost', SUM(impressions) as 'impressions', SUM(clicks) as 'clicks', SUM(conversions) as 'conversions' FROM adsconcierge_stats.platform_campana_day
						GROUP BY customer_id) as PCD, app_thesoci_9c37.customers C
						SET
						C.spend = PCD.cost, C.impressions = PCD.impressions, C.clicks = PCD.clicks, C.conversions = PCD.conversions,
						C.cpc = IF(PCD.clicks > 0, PCD.cost/PCD.clicks, 0) ,
						C.ctr = IF(PCD.impressions > 0, (PCD.clicks/PCD.impressions)*100, 0),
						C.cpm = IF(PCD.impressions > 0, PCD.cost/(PCD.impressions/1000), 0)
						WHERE PCD.customer_id = C.id");