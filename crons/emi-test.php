<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once __DIR__ . '/../public/apis/configs/general.php';
require_once __DIR__ . '/helper/config.php';
require_once __DIR__ . '/helper/db.php';

$db = new db();

$jobs = getAllJobs();

if ($jobs) {
    foreach ($jobs as $job) {
        switch ($job['type']) {
            case "account":
                handleStatsAccount($job);
                break;
            case "campaign":
                handleStatsCampaign($job);
                break;
            case 'atomo':
                handleStatsAtomo($job);
                break;
            case 'get_atomo':
                handleGetAtomo($job);
                break;
            case 'get_creatividad':
                handleGetCreatividad($job);
                break;
        }

    }
}

//handleUpdateAccount();
//=============================================
//=============================================

function getAllJobs()
{
    global $db;

    $sql = "SELECT 
                *
            FROM
                `adsconcierge_stats`.background_job
            WHERE
            id in (SELECT 
                MAX(id) as id
            FROM
                `adsconcierge_stats`.background_job
            WHERE
                subject_id IN (SELECT 
                        a.id AS account_id
                    FROM
                        app_thesoci_9c37.ads_accounts a
                            LEFT JOIN
                        app_thesoci_9c37.auths_user_platform auth ON a.auth_id = auth.id
                        where a.platform = 'LINKEDIN'
                            )
            GROUP BY subject_id) order by background_job.id";
    
    $jobs = $db->getRecordSet($sql);
    
    echo "GETTING ALL JOBS .. >> " .count($jobs). PHP_EOL;
    echo "-------------------------------------------------------------------------- >> " . PHP_EOL;
    if ($jobs) {
        $ids = array();
        
        foreach ($jobs as $val) {
            $ids[] = $val['id'];
        }
        
        if (count($ids) > 0) {
            $sql = "UPDATE adsconcierge_stats.background_job SET status = 'in_progress' WHERE id IN (" . implode(", ", $ids) . ")";
            //$db->query($sql);
        }
    
        return $jobs;
    
    } else {
    
        return false;
    
    }
}

function handleStatsAccount($job)
{
    global $db, $config;
   
   // $job['subject_id'] = 12891;

    $sql = "SELECT
    a.name,
                a.platform_user_id  as id_en_platform,
                a.id       			as account_id,
                a.customer_id       as customer_id,
                a.account_id		as platform_account_id,
                a.currency          as currency,
                auth.platform       as platform,
                auth.user_id        as user_id,
                auth.app_id         as app_id,
                auth.access_token   as access_token,
                auth.app_secret     as app_secret,
                auth.retorno        as retorno
            FROM app_thesoci_9c37.ads_accounts a
                LEFT JOIN app_thesoci_9c37.auths_user_platform auth ON a.auth_id = auth.id
            WHERE a.id = ". $job['subject_id'] ;


    echo "subject_id (app_thesoci_9c37.ads_accounts) >> " . $job['subject_id'] . PHP_EOL;

    $info = $db->getRow($sql);
    
    //var_dump($info);

    $fechasBuscar = [
        '2021-03-17',      
        '2021-03-18',      
        '2021-03-19',               
        '2021-03-20',               
        '2021-03-21',               
        '2021-03-22',               
        '2021-03-23',               
        '2021-03-24',               
        '2021-03-25',               

        
    ];

    if ($info) {
        //echo "Checking Account ->> " . $args['platform'] . ' user: ' . $info['user_id'] . ' account_id: ' . $info['account_id'] . PHP_EOL;
        echo "                  >> " .  $info['name'] . PHP_EOL;

        foreach ($fechasBuscar as $fBusq){
            $args['job_id'] = $job['id'];
            $args['platform'] = $info['platform'];
            $args['user_id'] = $info['user_id'];
            $args['job_id']         = $job['id'];
            $args['platform']       = $info['platform'];
            $args['user_id']        = $info['user_id'];
            $args['start_date'] = $fBusq . " 00:00:00";
            $args['end_date'] = $fBusq . " 23:59:59";
            $args['ad_account']     = $info['account_id'];
            $args['ad_account_info']  = array(
                'id_en_platform'        => $info['id_en_platform'],
                'ad_account_id'         => $info['account_id'],
                'platform_account_id'   => $info['platform_account_id'],
                'currency'              => $info['currency']
            );
            $args['customer_id']    = $info['customer_id'];
            $args['job']    = $job;
    

            switch ($info['platform']) {
                case 'FACEBOOK':
                case 'LINKEDIN':
                case 'SNAPCHAT':
                    $args['token_info']['access_token'] = $info['access_token'];
                    $args['token_info']['app_secret'] = $info['app_secret']; //arg9 token_info
                    $args['app_id'] = $info['app_id'];
                    break;

                case 'TWITTER': //twitter
                    $lineitems_ids_platform = get_lineitems_ids_platform($job['subject_id']);
                    $row['settings'] = [];
                    $dataArray = json_decode($info['app_secret']);
                    $row['settings']['oauth_access_token'] = $info['app_id'];
                    $row['settings']['oauth_access_token_secret'] = $info['access_token'];
                    $row['settings']['consumer_key'] = $dataArray->consumer_key;
                    $row['settings']['consumer_secret'] = $dataArray->consumer_secret;
                    $args['app_id'] = $dataArray->consumer_key;
                    $args['token_info']['oauth_access_token'] = $info['access_token'];
                    $args['token_info']['app_secret'] = $info['app_secret'];
                    $args['token_info']['user_credentials'] = $info['retorno']; //arg9 token_info
                    $args['auth_settings'] = $row['settings']; //arg9 token_info
                    $args['lineitems_ids_platform'] = $lineitems_ids_platform; //arg10 lineitems_ids_platform
                    break;
            }

        $cmd = $config['php_executable_file'] . " -f " . __DIR__ . "/cron_stats_campanas.php ";
        $arg_json = json_encode($args);
        $cmd .= " \"" . $db->real_escape_string($arg_json) . "\" >> " . "pivot_campana_log_emi.txt";
     

            echo $cmd . "\n\n"; //die;
            exec($cmd);
        
    }
    }
}

function handleStatsCampaign($job)
{
    global $db, $config;

    $sql = "SELECT
                cp.id_en_platform   as id_en_platform,
                cp.ad_account       as account_id,
                cp.customer_id      as customer_id,
                auth.platform       as platform,
                auth.user_id        as user_id,
                auth.app_id         as app_id,
                auth.access_token   as access_token,
                auth.app_secret     as app_secret,
                auth.retorno        as retorno,
                a.account_id		as platform_account_id

            FROM app_thesoci_9c37.campaigns_platform cp
                LEFT JOIN app_thesoci_9c37.auths_user_platform auth ON cp.auth_id = auth.id
                LEFT JOIN app_thesoci_9c37.ads_accounts a ON a.id = cp.ad_account
            WHERE cp.id = {$job['subject_id']}
            ";

    $info = $db->getRow($sql);
    //echo "check" . $sql . PHP_EOL;
    //var_dump($info);

    if ($info) {
        $args['job_id'] = $job['id'];
        $args['platform'] = $info['platform'];
        $args['user_id'] = $info['user_id'];
        $args['campain_id'] = $info['id_en_platform'];
        $args['start_date'] = date("Y-m-d", strtotime($job['next_execution'])) . " 00:00:00";
        $args['end_date'] = date("Y-m-d", strtotime($job['next_execution'])) . " 00:00:00";
        $args['campaign_info'] = array(
            'id_en_platform' => $info['id_en_platform'],
            'campana_id' => $job['subject_id'],
            'ad_account_id' => $info['account_id'],
            'platform_account_id' => $info['platform_account_id'],
        );
        $args['customer_id'] = $info['customer_id'];
        //    $campana['customer_id']=isset($campana['customer_id']) ?  $campana['customer_id']:  $campana['customer_id_default'];
        echo "Checking Campaing ->> " . $args['platform'] . ' user: ' . $info['user_id'] . ' ad_account: ' . $info['account_id'] . PHP_EOL;

        switch ($args['platform']) {
            case 'FACEBOOK':
            case 'LINKEDIN':
            case 'SNAPCHAT':
                $args['token_info']['access_token'] = $info['access_token'];
                $args['token_info']['app_secret'] = $info['app_secret']; //arg9 token_info
                $args['app_id'] = $info['app_id'];
                break;

            case 'TWITTER': //twitter
                $lineitems_ids_platform = get_lineitems_ids_platform($job['subject_id']);

                $row['settings'] = [];
                $dataArray = json_decode($info['app_secret']);
                $row['settings']['oauth_access_token'] = $info['app_id'];
                $row['settings']['oauth_access_token_secret'] = $info['access_token'];
                $row['settings']['consumer_key'] = $dataArray->consumer_key;
                $row['settings']['consumer_secret'] = $dataArray->consumer_secret;

                $args['app_id'] = $dataArray->consumer_key;
                $args['token_info']['oauth_access_token'] = $info['access_token'];
                $args['token_info']['app_secret'] = $info['app_secret'];
                $args['token_info']['user_credentials'] = $info['retorno']; //arg9 token_info
                $args['auth_settings'] = $row['settings']; //arg9 token_info
                $args['lineitems_ids_platform'] = $lineitems_ids_platform; //arg10 lineitems_ids_platform
                break;
        }
        //echo '<pre>';print_r($args);die;
        $cmd = $config['php_executable_file'] . " -f " . __DIR__ . "/cron_stats_atomo.php ";
        $arg_json = json_encode($args);
        $cmd .= " \"" . $db->real_escape_string($arg_json) . "\" >> " . $config['async_php_log_path'] . "pivot_atomo_log.txt &";
        //$cmd .= " \"" . $db->real_escape_string($arg_json) . "\"";
        echo $cmd . "\n\n";

        exec($cmd); //die;
        sleep(1);
    }

}

function handleStatsAtomo($job)
{
    global $config, $db;

    $sql = "SELECT
                cpo.id_en_platform   as id_en_platform,
                cpo.ad_account       as account_id,
                cpo.customer_id      as customer_id,
                auth.platform        as platform,
                auth.user_id         as user_id,
                auth.app_id          as app_id,
                auth.access_token    as access_token,
                auth.app_secret      as app_secret,
                cp.id                as campana_id,
                cpo.id               as atomo_id,
                auth.retorno        as retorno,
                a.account_id		as platform_account_id

            FROM app_thesoci_9c37.campaigns_platform_atomo cpo
                LEFT JOIN app_thesoci_9c37.campaigns_platform cp on cp.id = cpo.campana_platform_id
                LEFT JOIN app_thesoci_9c37.auths_user_platform auth ON cp.auth_id = auth.id
                LEFT JOIN app_thesoci_9c37.ads_accounts a ON a.id = cp.ad_account
            WHERE cpo.id = {$job['subject_id']}
            ";
    $info = $db->getRow($sql); //echo '<pre>';echo $sql;print_r($info);die;

    if ($info) {
        $args['job_id'] = $job['id'];
        $args['platform'] = $info['platform'];
        $args['user_id'] = $info['user_id'];
        $args['campain_id'] = $info['id_en_platform'];
        $args['start_date'] = date("Y-m-d", strtotime($job['next_execution'])) . " 00:00:00";
        $args['end_date'] = date("Y-m-d", strtotime($job['next_execution'])) . " 23:59:59";
        $args['atomo_info'] = array(
            'id_en_platform' => $info['id_en_platform'],
            'atomo_id' => $info['atomo_id'],
            'campana_id' => $info['campana_id'],
            'ad_account_id' => $info['account_id'],
            'platform_account_id' => $info['platform_account_id'],
        );
        $args['customer_id'] = $info['customer_id'];
        //  $campana['customer_id']=isset($campana['customer_id']) ?  $campana['customer_id']:  $campana['customer_id_default'];
        echo "Checking Atomo ->> " . $args['platform'] . ' user: ' . $info['user_id'] . ' ad_account: ' . $info['account_id'] . PHP_EOL;

        switch ($info['platform']) {
            case 'FACEBOOK':
            case 'LINKEDIN':
            case 'SNAPCHAT':
                $args['token_info']['access_token'] = $info['access_token'];
                $args['token_info']['app_secret'] = $info['app_secret']; //arg9 token_info
                $args['app_id'] = $info['app_id'];
                break;

            case 'TWITTER': //twitter
                $ads_ids_platform = get_ads_ids_platform($job['subject_id']);
                $row['settings'] = [];
                $dataArray = json_decode($info['app_secret']);
                $row['settings']['oauth_access_token'] = $info['app_id'];
                $row['settings']['oauth_access_token_secret'] = $info['access_token'];
                $row['settings']['consumer_key'] = $dataArray->consumer_key;
                $row['settings']['consumer_secret'] = $dataArray->consumer_secret;

                $args['app_id'] = $dataArray->consumer_key;
                $args['token_info']['oauth_access_token'] = $info['access_token'];
                $args['token_info']['app_secret'] = $info['app_secret'];
                $args['token_info']['user_credentials'] = $info['retorno']; //arg9 token_info
                $args['auth_settings'] = $row['settings']; //arg9 token_info
                $args['ads_ids_platform'] = $ads_ids_platform;
                break;
        }

        $cmd = $config['php_executable_file'] . " -f " . __DIR__ . "/cron_stats_ads.php ";
        $arg_json = json_encode($args);
        $cmd .= " \"" . $db->real_escape_string($arg_json) . "\" >> " . $config['async_php_log_path'] . "pivot_ads_log.txt &";
        echo $cmd . "\n\n"; //die;
        exec($cmd); //die;
        //    sleep(1);
    }
}

function handleGetAtomo($job)
{
    global $config, $db;

    $sql = "SELECT
                cp.id_en_platform   as id_en_platform,
                cp.platform         as platform,
                cp.auth_id		    as platform_account_id
            FROM app_thesoci_9c37.campaigns_platform cp
            WHERE cp.id = {$job['subject_id']} ";

    $info = $db->getRow($sql); //echo '<pre>';echo $sql;print_r($info);die;
    $info["function"] = "get_atomo";
    $info["job"] = $job;

    if ($info) {
        switch ($info['platform']) {
            case 'FACEBOOK':
                $cmd = $config['php_executable_file'] . " -f " . __DIR__ . "/../public/apis/apis_curls/facebookApi.php ";
                $arg_json = json_encode($info);
                $cmd .= " \"" . $db->real_escape_string($arg_json) . "\" >> " . $config['async_php_log_path'] . "pivot_get_atomo_log.txt &";
                echo $cmd . "\n\n"; //die;
                exec($cmd); //die;
                break;
            case 'LINKEDIN':
            case 'SNAPCHAT':
            case 'TWITTER':
                //TODO
                break;
        }
    }
}

function handleGetCreatividad($job)
{
    global $config, $db;

    $sql = "SELECT
                cpa.id_en_platform   as id_en_platform,
                cpa.platform         as platform,
                cpa.auth_id		    as platform_account_id

            FROM app_thesoci_9c37.campaigns_platform_atomo cpa
            WHERE cpa.id = {$job['subject_id']}
            ";

    $info = $db->getRow($sql); //echo '<pre>';echo $sql;print_r($info);die;
    $info["function"] = "get_creatividad";

    //echo $info['platform'];

    echo "Checking Cratividades ->> " . $info['platform'] . PHP_EOL;

    switch ($info['platform']) {
        case 'FACEBOOK':
            $cmd = $config['php_executable_file'] . " -f " . __DIR__ . "/../public/apis/apis_curls/facebookApi.php ";
            $arg_json = json_encode($info);
            $cmd .= " \"" . $db->real_escape_string($arg_json) . "\" >> " . $config['async_php_log_path'] . "pivot_get_creatividad_log.txt &";
            echo $cmd . "\n\n"; //die;
            exec($cmd); //die;
            break;
        case 'LINKEDIN':
        case 'SNAPCHAT':
        case 'TWITTER':
            echo "holaaaaa";
            //TODO
            break;
    }

}

function handleUpdateAccount()
{
    global $db, $config;

    $sql = "SELECT * FROM app_thesoci_9c37.auths_user_platform where (next_execution < '" . date("Y-m-d H:i:s") . "' || next_execution is null) and activa = 'Y' ORDER BY id";

    $auths = $db->getRecordSet($sql);

    if ($auths) {
        foreach ($auths as $val) {

            $cmd = $config['php_executable_file'] . " -f " . __DIR__ . "/auth_account_update.php " . $val['id'];
            $cmd .= " >> " . $config['async_php_log_path'] . "pivot_get_creatividad_log.txt &";
            echo $cmd . "\n\n"; //die;
            exec($cmd); //die;

            $next_execution = "";
            switch ($val['update_account']) {
                case "15M":
                    $next_execution = date("Y-m-d H:i:s", strtotime('+15 minutes'));
                    break;
                case "12H":
                    $next_execution = date("Y-m-d H:i:s", strtotime('+12 hours'));
                    break;
                default:
                case "24H":
                    $next_execution = date("Y-m-d H:i:s", strtotime('+24 hours'));
                    break;
            }

            $sql = "UPDATE app_thesoci_9c37.auths_user_platform SET laststatus = '" . date("Y-m-d H:i:s") . "', next_execution = '" . $next_execution . "' WHERE id = " . $val['id'];
            echo $sql . PHP_EOL;
            $db->query($sql);
        }
    } else {
        return false;
    }
}

//=============================================
//=============================================

function get_lineitems_ids_platform($campaign_id)
{
    global $db;

    $sql = "SELECT id_en_platform FROM app_thesoci_9c37.campaigns_platform_atomo WHERE campana_platform_id = '{$campaign_id}'";

    $rs = $db->getRecordSet($sql);
    $lineitemsStruct = array();
    $lineitem_ids = array();
    foreach ($rs as $row) {
        $lineitem_ids[] = $row['id_en_platform'];

        if (count($lineitem_ids) == 20) {
            $lineitemsStruct[] = $lineitem_ids;
            $lineitem_ids = array();
        }
    }
    if (count($lineitem_ids) > 0) {
        $lineitemsStruct[] = $lineitem_ids;
    }

    return $lineitemsStruct;
}

function get_ads_ids_platform($atomo_id)
{
    global $db;

    $sql = "SELECT id_en_platform FROM app_thesoci_9c37.creatividades WHERE atomo_id = '{$atomo_id}'";

    $rs = $db->getRecordSet($sql);
    $adsStruct = array();
    $ads_ids = array();

    foreach ($rs as $row) {
        $ads_ids[] = $row['id_en_platform'];

        if (count($ads_ids) == 20) {
            $adsStruct[] = $ads_ids;
            $ads_ids = array();
        }
    }
    if (count($ads_ids) > 0) {
        $adsStruct[] = $ads_ids;
    }

    return $adsStruct;
}