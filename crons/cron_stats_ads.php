<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once __DIR__ . '/../vendor/autoload.php';
require_once  __DIR__ .'/../public/apis/configs/general.php';
require_once  __DIR__ .'/../public/apis/apis_curls/stats_helper.php';
require_once  __DIR__ .'/../public/apis/apis_curls/stats_twitter.php';
require_once  __DIR__ .'/../public/apis/apis_curls/stats_linkedin.php';
require_once  __DIR__ .'/../public/apis/apis_curls/facebookApi.php';
require_once  __DIR__ .'/../public/apis/apis_curls/stats_facebook.php';

require_once  __DIR__ .'/helper/config.php';
require_once  __DIR__ .'/helper/db.php';
$db = new db();

//require_once  __DIR__ .'/../public/apis/configs/linkedin.php'; se llama en stats_linkedin

use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;
/***

//parametro 1= Id plataforma,
//parametro 2= user id
//parametro 3=app_id
//parametro 4=ad_account
//parametro 5= $stardate=new DateTime("today -8 days");
//parametro 6= 	$enddate= new DateTime("today");
//parametro 7=id's campanas por ,
//parametro 8=customer id
//parametro 9=access token
$argv
(
[0] => /home/app.thesocialaudience.com/public_html/www/crons/cron_stats_campanas.php
[1] => 3
[2] => 1
[3] => iy5pXGlxt4ru0GPYE81WXbdtT
[4] => 28
[5] => 2019-11-16 00:00:00
[6] => 2019-11-24 00:00:00
[7] => 67
)
//en realidad esta query seria hija de una primera query que traiga los usuarios activos, inicialice las cuentas (como el metodo de la linea 32  TwitterAds::init ) y llame a los items de un user: campana, line item y creatividad
 **/
/*
$args['platform'] 1
$args['user_id']2
$args['app_id']3
$args['ad_account']4
$args['start_date']5
$args['end_date']6
$args['campaign_info']['id_en_platform']7
$args['campaign_info']['campana_id']
$args['campaign_info']['ad_account_id']
$args['customer_id']8
case 'LINKEDIN':
case 'SNAPCHAT':
    $args['token_info']['access_token']         = $auth['access_token'];
    $args['token_info']['app_secret']           = $auth['app_secret'];      //arg9 token_info

case 'TWITTER': //twitter
    $args['token_info']['oauth_access_token']   = $auth['access_token'];
    $args['token_info']['app_secret']           = $auth['app_secret'];
    $args['token_info']['user_credentials']     = $auth['retorno'];      //arg9 token_info
    $args['auth_settings']                      = $auth['settings'];      //arg9 token_info
 */
$args = json_decode($argv[1], true);
//echo '<pre>';print_r($args);die;

switch( $args['platform']) {
    case 1:
    case 'FACEBOOK':
        $datos = facebook_stats_from_creative_ad($credentials=['app_id'=>$args['app_id'], 'app_secret'=>$args['token_info']['app_secret'], 'access_token'=>$args['token_info']['access_token']], $args['user_id'], $args['atomo_info']['id_en_platform'], $args['start_date'], $args['end_date'], '1','ad');
        helper_metrics_ads_day($args['platform'] ,$args['user_id'],$args['customer_id'],  $args['atomo_info'],$datos, $args['job_id']);
        break;

    case 2:
    case 'LINKEDIN'://DO TUKA
        $datos = linkedin_stats_ads_xdias( $args['app_id'], $args['token_info']['access_token'], $args['user_id'], $args['atomo_info'], $args['start_date'], $args['end_date']) ;
        //	 helper_metrics_campana_day($platformid, $user_id, $customer_id=0, $campanaid=0,$datos,$ad_account_id,$ad_account_id_platform)

        //MAP FIELD AND FILL DB
        helper_metrics_ads_day($args['platform'] ,$args['user_id'],$args['customer_id'],  $args['atomo_info'],$datos, $args['job_id']);

        break;

    case 3:
    case 'TWITTER':
        $user_credentials   = json_decode($args['token_info']['user_credentials'], true);//echo '<pre>'; print_r($user_credentials);die;
        $appdata            = json_decode($args['token_info']['app_secret'], true);
        $auth_settings      = $args['auth_settings'];

        $twitterapi = TwitterAds::init($auth_settings['consumer_key'], $auth_settings['consumer_secret'], $user_credentials['oauth_token'], $user_credentials['oauth_token_secret']);
        $accountw = new Account($args['atomo_info']['platform_account_id'] );
        $accountw->read();
        //echo '<pre>'; print_r($accountw);die;
        $start_date     = str_replace(" ", "T", $args['start_date']) .'Z';
        $end_date       = str_replace(" ", "T", $args['end_date']) .'Z';

        $datos = twitter_stats_ads_xaccount($args['user_id'],  $accountw,null , $start_date , $end_date, $args['ads_ids_platform']);
        //echo '<pre>';print_r($datos);die;
        helper_metrics_ads_day($args['platform'] ,$args['user_id'],$args['customer_id'],  $args['atomo_info'],$datos, $args['job_id']);
        break;


}



