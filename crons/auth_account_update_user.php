<?php
//cron cada hora

require_once __DIR__ . '/../public/apis/configs/general.php';
require __DIR__ . '/../public/apis/apis_curls/LinkedInApi.php';
require __DIR__ . '/../public/apis/apis_curls/facebookApi.php';
use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;
/*

if($argv) {
$argv[1]=platform
$argv[2]=user_id
$argv[3]=app_id
$argv[4]=id
}
"1"    "FACEBOOK"
"2"    "LINKEDIN"
"3"    "TWITTER"
"4"    "INSTAGRAM"
"5"    "SNAPCHAT"
"6"    "GANALYTICS"
"7"    "SUNNATIVO"
"8"    "FB Insights"
"9"    "PINTEREST"
"10"    "ADWORDS
 **/
//traer las cuentas, campañas, adgroups y creatividades
//refrescar tokens de auth2

$stmt = $dbconn->prepare("SELECT * FROM `app_thesoci_9c37`.`users` LIMIT 1000");
$stmt->execute();

$resultado = $stmt->get_result();
while ($user = $resultado->fetch_assoc()) {
    $customer_id = $user['customer_id_default'];
    if ($customer_id == null) {
        $newclient = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`customers` (`name`, `user_id`, `isdefault`) VALUES (?, ?, 'Y')");
        $newclient->bind_param("si", ...['Customer Default ' . $user['email'], $user['id']]);
        $newclient->execute();
        $customer_id = $newclient->insert_id;
        $dbconn->query("UPDATE `app_thesoci_9c37`.`users` SET `customer_id_default`='{$customer_id}' WHERE  `id`={$user['id']};");
    }

    if ($user['campaign_root_default'] == null) {
        $newcampaign = $dbconn->prepare("INSERT INTO `app_thesoci_9c37`.`campaigns` (`name`, `user_id`, `customer_id`) VALUES (?, ?, ?)");
        $newcampaign->bind_param("sis", ...['Campaigns Default ' . $user['email'], $user['id'], $customer_id]);
        $newcampaign->execute();
        $campaign_root = $newcampaign->insert_id;
        $dbconn->query("UPDATE `app_thesoci_9c37`.`users` SET `campaign_root_default`='{$campaign_root}' WHERE  `id`={$user['id']};");
    }
}

$stmt = $dbconn->prepare("SELECT AUP.id, AUP.auths_user_id, AUP.user_id, AUP.platform, AUP.app_id, AUP.app_secret, AUP.platform_user_id, AUP.access_token, AUP.refresh_token, U.name, U.email, U.customer_id_default, U.campaign_root_default FROM auths_user_platform AUP JOIN users U ON (AUP.user_id = U.id) WHERE 1=1");
$stmt->execute();
$authsresults = $stmt->get_result();

/* now you can fetch the results into an array - NICE */
while ($auths = $authsresults->fetch_assoc()) {

    switch ($auths['platform']) {
        case "FACEBOOK":
            try {
                facebook_account_retrieve_all($credentials = ['app_id' => $auths['app_id'], 'app_secret' => $auths['app_secret'], 'access_token' => $auths['access_token'], 'platform_user_id' => $auths['platform_user_id']], $auths['user_id'], $auths);
            } catch (Exception $e) {
                echo 'Excepción capturada: ', $e->getMessage(), "\n";
                print_r($auths);
            }
            break;

        case "LINKEDIN":
            if ($auths['refresh_token'] != "") {
                $token = refresh_token($auths['app_id'], $auths['app_secret'], $auths['refresh_token']);

                $refresh_token = $dbconn->prepare("UPDATE auths_user_platform
													SET access_token = ?, refresh_token = ?, retorno = ?
													WHERE platform = 'LINKEDIN' AND user_id = ?");
                $refresh_token->bind_param("sssi", ...[$token->access_token, $token->refresh_token, json_encode($token), $auths['user_id']]);
                $refresh_token->execute();

                $auths['access_token'] = $token->access_token;
                $accounts = ads_accounts_get_linkedin(null, $auths['access_token'], $auths['user_id'], $auths);

                $subadsaccount = $dbconn->prepare("INSERT INTO ads_accounts (user_id,platform,app_id,account_id,name,platform_user_id,status,currency,metadata, customer_id, auth_id)
														values (?,?,?,?,?,?,?,?,?, ?, ?)
														on duplicate key update status=?, metadata=?  ");
                $cuentasarray = [];
                foreach ($accounts as $item) {
                    $cuentasarray[$item->id] = $item;
                    $subadsaccount->bind_param("issssssssssss", ...[$auths['user_id'], $auths['platform'], $auths['app_id'], $item->id, $item->name, $auths['platform_user_id'],
                        $item->status . '>' . $item->servingStatuses[0], $item->currency, json_encode($item), $auths["customer_id_default"], $auths["id"],
                        $item->status . '>' . $item->servingStatuses[0], json_encode($item)]);
                    $subadsaccount->execute();
                }

                $campaigns = campaigns_get_linkedin(null, $auths['access_token'], $auths['user_id'], null);
                //print_r($campaigns);
                $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (user_id, platform, app_id, auth_id, name, id_en_platform, budget, status, currency, metadata, ad_account, campana_root, customer_id)
														values (?,?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id =? and ads_accounts.user_id=?), ?, ? )
														on duplicate key update status=?, metadata=?");
                //printf("Error: %s.\n", $stmt->error);

                foreach ($campaigns as $item) {

                    $item = (object) $item;
                    $accountbb = str_replace('urn:li:sponsoredAccount:', '', $item->account);

                    $stmtcampana->bind_param("issssssssssissss", ...[$auths['user_id'], $auths['platform'], $auths['app_id'], $auths['id'], $item->name, $item->id, null, unificastatus('campana', $auths['platform'], $item->status, ['estado' => $item->status, 'otros' => $item->servingStatuses[0], 'record' => $item]), $cuentasarray[$accountbb]->currency, json_encode($item), $accountbb, $auths['user_id'], $auths["campaign_root_default"], $auths["customer_id_default"], unificastatus('campana', $auths['platform'], $item->status, ['estado' => $item->status, 'otros' => $item->servingStatuses[0], 'record' => $item]), json_encode($item)]);

                    $stmtcampana->execute();
                    if ($stmtcampana->error != "") {
                        print_r("Error: %s.\n", $stmtcampana->error);
                    }

                }
            }
            break;

        case "TWITTER":
            try {
                $auth_settings = json_decode($auths['app_secret']);

                $twitterapi = TwitterAds::init($auth_settings->consumer_key, $auth_settings->consumer_secret, $auths['app_id'], $auths['access_token']);
                $accounts = $twitterapi->getAccounts();

                $subadsaccount = $dbconn->prepare("INSERT INTO ads_accounts (user_id, platform, app_id, account_id, name, platform_user_id, status, currency, metadata, customer_id, auth_id) values (?,?,?,?,?,?,?,?,?,?,?) on duplicate key update status=?, metadata=? ");
                $cuentasarray = [];

                foreach ($accounts as $account) {

                    $data = array(
                        "id" => $account->getId(),
                        "salt" => $account->getSalt(),
                        "name" => $account->getName(),
                        "timezone" => $account->getTimezone(),
                        "timezone_switch_at" => $account->getTimezoneSwitchAt(),
                        "created_at" => $account->getCreatedAt(),
                        "updated_at" => $account->getUpdatedAt(),
                        "deleted" => $account->getDeleted(),
                        "approval_status" => $account->getApprovalStatus(),
                        "business_id" => $account->getBusinessId(),
                        "business_name" => $account->getBusinessName(),
                        "industry_type" => $account->getIndustryType(),
                        "properties" => $account->getProperties(),
                    );

                    $data = (object) $data;
                    $cuentasarray[$account->getId()] = $account;

                    $subadsaccount->bind_param("issssssssssss", ...[$auths['user_id'], $auths['platform'], $auths['app_id'], $account->getId(),
                        $account->getName(), $auths['platform_user_id'], $account->getApprovalStatus(), '',
                        json_encode($data), $auths["customer_id_default"], $auths["id"], $account->getApprovalStatus(), json_encode($data)]);

                    $subadsaccount->execute();

                    $account->read();
                    $campaigns = $account->getCampaigns();

                    $stmtcampana = $dbconn->prepare("INSERT INTO campaigns_platform (user_id, platform, app_id, auth_id, name, id_en_platform, budget, status, currency, metadata, ad_account, campana_root, customer_id)
														values (?,?,?,?,?,?,?,?,?,?,(SELECT ads_accounts.id FROM ads_accounts WHERE ads_accounts.account_id =? and ads_accounts.user_id=?), ?, ? )
														on duplicate key update status=?, metadata=?");

                    foreach ($campaigns as $campaign) {

                        $item = array(
                            "id" => $campaign->getId(),
                            "reasons_not_servable" => $campaign->getReasonsNotServable(),
                            "servable" => $campaign->getServable(),
                            "created_at" => $campaign->getCreatedAt(),
                            "updated_at" => $campaign->getUpdatedAt(),
                            "deleted" => $campaign->getDeleted(),
                            "currency" => $campaign->getCurrency(),
                            "properties" => $campaign->getProperties(),
                            "name" => $campaign->getName(),
                            "funding_instrument_id" => $campaign->getFundingInstrumentId(),
                            "start_time" => $campaign->getStartTime(),
                            "end_time" => $campaign->getEndTime(),
                            "entity_status" => $campaign->getEntityStatus(),
                            "standard_delivery" => $campaign->getStandardDelivery(),
                            "daily_budget_amount_local_micro" => $campaign->getDailyBudgetAmountLocalMicro(),
                            "total_budget_amount_local_micro" => $campaign->getTotalBudgetAmountLocalMicro(),
                            "duration_in_days" => $campaign->getDurationInDays(),
                            "frequency_cap" => $campaign->getFrequencyCap(),
                        );
                        $item = (object) $item;

                        $stmtcampana->bind_param("issssssssssissss", ...[$auths['user_id'], $auths['platform'], $auths['app_id'], $auths['id'],
                            $item->name, $item->id, null, unificastatus('campana', $auths['platform'], $item->entity_status, ['estado' => $item->entity_status, 'otros' => "", 'record' => $item]), $item->currency, json_encode($item), $data->id, $auths['user_id'], $auths["campaign_root_default"], $auths["customer_id_default"], unificastatus('campana', $auths['platform'], $item->entity_status, ['estado' => $item->entity_status, 'otros' => "", 'record' => $item]), json_encode($item)]);

                        $stmtcampana->execute();
                        if ($stmtcampana->error != "") {
                            print_r("Error: %s.\n", $stmtcampana->error);
                        }

                    }

                }

            } catch (Exception $e) {
                echo 'Excepción capturada: ', $e->getMessage(), "\n";
                print_r($auths);
            }
            break;
    }
}

$dbconn->query("UPDATE auths_user_platform
					SET count_ads = (
						SELECT COUNT(1)
							FROM ads_accounts
							WHERE auths_user_platform.app_id = ads_accounts.app_id AND
							auths_user_platform.platform = ads_accounts.platform AND
							auths_user_platform.platform_user_id = ads_accounts.platform_user_id
					)");

$dbconn->query("UPDATE auths_user_platform
						SET count_properties = (
						   	SELECT COUNT(1)
								FROM properties_accounts
								WHERE auths_user_platform.app_id = properties_accounts.app_id AND
								auths_user_platform.platform = properties_accounts.platform AND
								auths_user_platform.platform_user_id = properties_accounts.platform_user_id
					)");
