<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once __DIR__ . '/../vendor/autoload.php';

require_once  __DIR__ .'/helper/config.php';
require_once  __DIR__ .'/helper/db.php';
$db = new db();


$args = json_decode($argv[1], true);


if(($args['status'] == 'PAUSED') || ($args['status'] == 'STOPED')){

  $sql = "UPDATE app_thesoci_9c37.background_cron SET status = 'finished' WHERE id = ".$args['job']["id"];
  $db->query($sql);

}else{

  $next_execution = "";
  switch($args['job']['Execution_interval']){
      case "15M":
          $next_execution = date("Y-m-d H:i:s", strtotime('+15 minutes'));
      break;
      case "12H":
          $next_execution = date("Y-m-d H:i:s", strtotime('+12 hours'));
      break;
      default:
      case "24H":
          $next_execution = date("Y-m-d H:i:s", strtotime('+24 hours'));
      break;
  }

  $sql = "UPDATE app_thesoci_9c37.background_cron
  SET status = 'pending', next_execution = '".$next_execution."'
  WHERE id = ".$args['job']["id"];
  $db->query($sql);
}

//============================================
//=========THIS IS END OF THE SCRIPT==========
//============================================
exit;
//============================================
