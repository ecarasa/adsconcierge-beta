<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once  __DIR__ .'/../public/apis/configs/general.php';
require_once  __DIR__ .'/helper/config.php';
require_once  __DIR__ .'/helper/db.php';
$db = new db();

$crons = getAllCrons();

if ($crons){
    foreach($crons as $job){
        switch( $job['type']) {
            case "ads_spend_check":
                handle_ads_spend_check($job);
                break;
            case "organic":
                handle_organic_setting($job);
                break;
        }
    }
}

//=============================================
//=============================================
//=============================================
//=============================================
//=============================================

function handle_ads_spend_check($job){
    global $db, $config;

    $sql = "SELECT
                c.id, c.name, c.user_id, c.status
            FROM app_thesoci_9c37.campaigns c
            WHERE c.id = {$job['subject_id']}
            ";
    $args = $db->getRow($sql);

    $args['job']         = $job;

    //echo '<pre>';print_r($args);die;
    $cmd  = $config['php_executable_file'] . " -f ".__DIR__."/cron_ads_spend_check.php ";
    $arg_json = json_encode($args);
    $cmd .= " \"" . $db->real_escape_string($arg_json) . "\" >> " . $config['async_php_log_path']  . "log_ads_spend_check.txt";
    echo $cmd . "\n\n";//die;
    exec($cmd ) ;//die;
}


function handle_organic_setting($job){
    global $db, $config;

    $sql = "SELECT OS.*, PA.platform_user_id as 'accountId', PA.id_en_platform as 'profileId'
              FROM app_thesoci_9c37.organic_settings OS
              LEFT JOIN app_thesoci_9c37.properties_accounts PA on (OS.property = PA.id)
             WHERE OS.id = {$job['subject_id']}
            ";
    $args = $db->getRow($sql);

    $args['job']         = $job;

    $cmd = "";
    if($args['sourcetype'] == 'RSS'){
        $cmd  = $config['php_executable_file'] . " -f ".__DIR__."/cron_organic_rss.php ";
    }else{
        $cmd  = $config['php_executable_file'] . " -f ".__DIR__."/cron_organic_analytic.php ";
    }
    //echo '<pre>';print_r($args);die;

    $arg_json = json_encode($args);
    $cmd .= " \"" . $db->real_escape_string($arg_json) . "\" >> " . $config['async_php_log_path']  . "log_ads_spend_check.txt";
    echo $cmd . "\n\n";//die;
    exec($cmd ) ;//die;
}


function getAllCrons(){
    global $db;
    $sql = "SELECT * FROM app_thesoci_9c37.background_cron
                WHERE status = 'pending'
                AND next_execution < '".date("Y-m-d H:i:s")."'
                OR next_execution IS NULL
                ORDER BY id";

    $jobs = $db->getRecordSet($sql);

    if ($jobs){
        $ids = array();
        foreach($jobs as $val){
            $ids[] = $val['id'];
        }
        if(count($ids) > 0){
            $sql = "UPDATE app_thesoci_9c37.background_cron SET
                    status = 'in_progress'
                WHERE id IN (" . implode(", ", $ids) . ")";
            $db->query($sql);
        }

        return $jobs;
    }
    else {
        return false;
    }
}
