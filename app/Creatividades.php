<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creatividades extends Model
{
    protected $table = 'creatividades';
    protected $fillable = [
        'id',
        'id_en_platform',
        'user_id',
        'customer_id',
        'title',
        'content',
        'banner',
        'macrotag',
        'type',
        'size',
        'impressions',
        'clicks',
        'ctr',
        'costs_per_click',
        'conversion_costs',
        'platform',
        'campana_root',
        'campana_platform_id',
        'atomo_id',
        'description',
        'linkdescription',
        'calltoaction',
        'url',
        'type_platform',
        'metadata',
        'status',
        'platform_status',
        'platform_configuration',
        'public_id',
        'darkpost_id',
        'budget'
    ];
    public $timestamps = false;
}
