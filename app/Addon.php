<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addon extends Model
{
    protected $table = "addons";
    protected $fillable = ['addon_id', 'quantity', 'name', 'subscription_id'];
}
