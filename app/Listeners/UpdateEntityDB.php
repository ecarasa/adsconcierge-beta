<?php

namespace App\Listeners;

use App\Events\UpdateEntity;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

use DB;
use Config;

use Illuminate\Support\Facades\Log;
use Illuminate\Pipeline\Pipeline;

use App\Campaign_platform;
use App\Campaign_platform_atomo;
use App\Creatividades;
use App\Customer;
use App\Campaign;
use App\Creativity;

use App\Events\newCronEvent;
use App\Events\newCronJobEvent;
use App\Events\TaskCreate;

class UpdateEntityDB
{

    public function __construct()
    {
        //
    }


    public function handle($eventDatos)
    {
        
        //dd(Auth::user()->id);
        //dd($eventDatos->eventData);
        //$event = array("entity" => "campaigns", "entity_table" => "campaigns", "entity_id" => $id, "entity_field" => $entityField,"entity_field_value" => $entityFieldValue);

        $event = $eventDatos->eventData;
        $actionJobType = "";


        // ACA DEFINIMOS LA ACCION QUE LE VAMOS A PASAR AL LISTENER DE CREACION DE TASK EN GOOGLE (newCronJobListener)
        // definidas al momento retrieve_all y retrieve_all_stats, esta podria ser update_status etc..

        switch($event['entity_field']){
            case "status":
                $actionJobType = 'update_status';
                break;

            case "name":
                $actionJobType = 'update_field';
                break;

            case "budget":
                $actionJobType = 'update_budget';
                break;
        }

        switch ($event['entity']) {
            
            case "campaigns":
                
                $affected = DB::table($event['entity_table'])->where('public_id', $event['entity_id'])->where('user_id', Auth::user()->id)->update([ $event['entity_field'] => $event['entity_field_value']]);
                $affected_to_cron = DB::table($event['entity_table'])->where('public_id', $event['entity_id'])->where('user_id', Auth::user()->id)->first();
                                                
                if ($actionJobType == 'update_budget') {
                    $data = array( 'campaign_id'=> $affected_to_cron->id, 'budgetdistribution'=> json_decode($affected_to_cron->budgetdistribution) , 'importeNuevo'=> $event['entity_field_value'],  'job'=>$actionJobType );
                    app(Pipeline::class)->send($data)->through([ \App\EventsPipeline\CampaignBudgetUpdate::class, ])->thenReturn($dataResponse);
                }
                
                if ($actionJobType == 'update_status') {
                    $data = array( 'campaign_id'=> $affected_to_cron->id, 'status'=> $event['entity_field_value'], 'job'=>$actionJobType, 'platform'=> null );
                    app(Pipeline::class)->send($data)->through([ \App\EventsPipeline\CampaignStatusUpdate::class, ])->thenReturn($data);
                }

                break;

            case "customer":

                $affected = DB::table($event['entity_table'])->where('public_id', $event['entity_id'])->where('user_id', Auth::user()->id)->update([ $event['entity_field'] => $event['entity_field_value']]);
                $customer = DB::table($event['entity_table'])->where('public_id', $event['entity_id'])->first();

                $campaigs_affected = DB::table('campaigns')->where('customer_id', $customer->id)->where('user_id', Auth::user()->id)->get();

                foreach ($campaigs_affected as $cc) {
                    $data = array( 'campaign_id'=> $cc->id, 'status'=> $entityFieldValue, 'job'=>$actionJobType, 'platform'=> null, 'type' => 'campaign' );
                    app(Pipeline::class)->send($data)->through([ \App\EventsPipeline\CampaignStatusUpdate::class, ])->thenReturn($data);
                    event(new TaskCreate($data));
                }

                break;

            case "campaigns_platform":

                $campaing_root = Campaign_platform::where('public_id', $event['entity_id'])->select('id', 'id_en_platform')->first();

                if ($campaing_root != null) {
                    $affected = DB::table($event['entity_table'])->where('campana_root', $campaing_root['id'])
                                    ->where('user_id', Auth::user()->id)->where('platform', $event['platform'])
                                    ->update([$event['entity_field'] => $event['entity_field_value']]);
                    
                    $affected_to_cron = DB::table($event['entity_table'])->where('campana_root', $campaing_root['id'])
                                            ->where('user_id', Auth::user()->id)->where('platform', $event['platform'])->get();

                    if ($actionJobType == 'update_status') {
                        $data = array( 'campaign_id'=> $campaing_root['id'], 'status'=> $event['entity_field_value'],  'job'=>$actionJobType, 'platform'=> null );
                        app(Pipeline::class)->send($data)->through([ \App\EventsPipeline\CampaignStatusUpdate::class, ])->thenReturn($data);
                        
                        // lanzamos event gcloud --> status API - enum{ACTIVE, PAUSED, DELETED, ARCHIVED}
                        $data = array('subject_id' => $campaing_root['id_en_platform'], 'type' => 'campaign_platform_update',  'function' => 'facebook-write-api', 'cola_trabajo' => 'fb-write',
                        'values' => array('status' => $event['entity_field_value']) );
                        event(new TaskCreate($data));
                    }

                    if ($actionJobType == 'update_budget') {
                        $data = array('campaign_id'=>$campaing_root['id'], 'budgetdistribution'=> json_decode($campaing_root['budgetdistribution']) ,
                                            'importeNuevo'=> $event['entity_field_value'],  'job' => $actionJobType );
                
                        app(Pipeline::class)->send($data)->through([ \App\EventsPipeline\CampaignBudgetUpdate::class, ])->thenReturn($dataResponse);
                    }
                } else {
                    //lo hacemos por customer
                    $cusstomer = Customer::where('public_id', $event['entity_id'])->select('id')->first();
                    $campaing_root = Campaign::where('customer_id', $cusstomer['id'])->select('id')->get()->toArray();

                    $affected = DB::table($event['entity_table'])
                                    ->whereIn('campana_root', $campaing_root)
                                    ->where('user_id', Auth::user()->id)
                                    ->where('platform', $event['platform'])
                                    ->update([$event['entity_field'] => $event['entity_field_value']]);
    
                    $affected_to_cron = DB::table($event['entity_table'])
                                        ->whereIn('campana_root', $campaing_root)
                                        ->where('user_id', Auth::user()->id)
                                        ->where('platform', $event['platform'])
                                        ->get();

                    $bulkData = array();

                    foreach ($affected_to_cron as $rr) {
                        array_push($bulkData, array( 'type' => 'campaigns_platform',  'action_type'=> $actionJobType,  'subject_id'=> $rr->id, 'next_execution'=>date('Y-m-d H:i:s') ));
                    }

                    //event(new newCronJobEvent($bulkData)); TODO

                }

                break;

            case "atom":
                // actualizamos y obtenemos los registros que actualizamos
                $affected = DB::table($event['entity_table'])->where('public_id', $event['entity_id'])->where('user_id', Auth::user()->id )->update([$event['entity_field'] => $event['entity_field_value']]);
                $affected_to_cron = DB::table($event['entity_table'])->where('public_id', $event['entity_id'])->where('user_id', Auth::user()->id )->get();
                
                $bulkData = array();

                foreach ($affected_to_cron as $rr) {
                    array_push($bulkData, array( 'type' => 'campaigns_platform_atomo', 'action_type'=> $actionJobType, 'subject_id'=> $rr->id, 'next_execution'=>date('Y-m-d H:i:s')));
                }

                //event(new newCronJobEvent($bulkData)); TODO

                break;
            

            case "creativity":
                // actualizamos y obtenemos los registros que actualizamos
                $affected = DB::table($event['entity_table'])->where('public_id', $event['entity_id'])->where('user_id', Auth::user()->id )->update([$event['entity_field'] => $event['entity_field_value']]);
                $affected_to_cron = DB::table($event['entity_table'])->where('public_id', $event['entity_id'])->where('user_id', Auth::user()->id )->get();
                
                $bulkData = array();

                foreach($affected_to_cron as $rr){
                    array_push($bulkData, array( 'type' => 'creatividades', 'action_type'=> $actionJobType,  'subject_id'=> $rr->id, 'next_execution'=>date('Y-m-d H:i:s')));
                }

                //event(new newCronJobEvent($bulkData)); TODO

                break;
        
        }
    }
}
