<?php

namespace App\Listeners;

use App\Events\TaskCreate;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Google\Cloud\Scheduler\V1\AppEngineHttpTarget;
use Google\Cloud\Scheduler\V1\CloudSchedulerClient;
use Google\Cloud\Scheduler\V1\Job;
use Google\Cloud\Scheduler\V1\Job\State;

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Tasks\V2\CloudTasksClient;
use Google\Cloud\Tasks\V2\HttpMethod;
use Google\Cloud\Tasks\V2\HttpRequest;
use Google\Cloud\Tasks\V2\Task;

use Illuminate\Support\Facades\DB;
use Auth;



class GCloudTaskCreate_JOB
{

    public function getAuthFrom_CampaignLocalID($campaign_pub_id){
        return DB::select("select * from auths_user_platform where id in ( select auth_id from campaigns_platform where id_en_platform = ? );", [ $campaign_pub_id ]);
    }

    public function getAuthFrom_authId($id){
        return  DB::table('auths_user_platform')->where('id', $id)->get();    
    } 

    public function persistGCloudTask_db($userId, $authId, $googleClient, $interval, $action, $json, $taskId){
 
        $result =  DB::table('gcloud_tasks')->upsert( [ 'user_id' => $userId,
                                                    'auth_id' => $authId,
                                                    'google_client_id' =>'sss'. $googleClient,
                                                    'date' => 'now()',
                                                    'interval' => $interval,
                                                    'action' => $action,
                                                    'json' => $json,
                                                    'task_id' => $taskId ,
                                                     'platform' => (isset($cron['platform']))? $cron['platform'] : '-' ],                                                     
                                                    ['user_id', 'auth_id', 'action'], // si estas columnas ya existen con los datos proporcionados, ejecuta el update
                                                    ['json'] // y si ejecuta update, solo actualiza este campo
                                                );
    
        //dd(DB::getPDO()->lastInsertId());
        return $result;

    }


    public function already_TaskCreated($userId, $authId, $action){

        if ( DB::table('gcloud_tasks')->where('user_id', $userId)->where('auth_id', $authId)->where('action', $action)->count() > 0 ){
            return true;
        }else{
            return false;
        }

    }

    public function handle($event)
    {
        try {
            /**     params  */
            $projectId = 'adsconcierge';
            $locationId = 'us-central1';
            
            // $data = array( 'subject_id' => $auths[0]->id, 'type' => 'retrieve_all', 'function' => 'function-facebook-api' );
            $tarea = $event->task;
            $interval = isset($tarea['Execution_interval']) ? substr($tarea['Execution_interval'], 0, 2) : '12H';
            $queueId = isset($tarea['cola_trabajo']) ? $tarea['cola_trabajo'] : 'default';


            //https://us-central1-adsconcierge.cloudfunctions.net/function-facebook-api
            $url = 'https://' . $locationId . '-' . $projectId . '.cloudfunctions.net/' . $tarea['function'];

            // segun la accion formamos el payload
            switch ($tarea['type']) {
                case "campaign_platform_update":
                    $auth = $this->getAuthFrom_CampaignIDPlatform($tarea['subject_id']);
                    $auth_id = $auth[0]->public_id;

                    $payload = array(   "auth_id" => $auth_id, 
                                        "action"=> "update_fields",
                                        "parent_platform_id" => $tarea['subject_id'],
                                        "fields"=> $tarea['values']
                                    );

                    //print_r(json_encode($payload));

                    break;

                case "auth_callback":
                    $auth = $this->getAuthFrom_authId($tarea['subject_id']);
                    $auth_id = $auth[0]->public_id;
                    $user_id = $auth[0]->user_id;
                    $payload = array("auth_id" => $auth_id, "action"=> "retrieve_all");
                    break;

                case "retrieve_all":
                    
                    $auth = $this->getAuthFrom_authId($tarea['subject_id']);
         
                    $auth_id = $auth[0]->public_id;
                    $user_id = $auth[0]->user_id;
                    
                    /*** logica 3) retrieveall trae adsaccounts 
                            *      Y dentro de retrieveall, lee las ads account y crea estas tasks por cada adsaccount
                            ---dame campaigns(de esa adsaccount)                        ---dame properties (de esa adsaccount) */
                    $payload = array("auth_id" => $auth_id, "action"=> "retrieve_all");
                    //echo json_encode($payload);
                    break;
                
            }

            // validamos que no exista la tarea en la nube para evitar duplicados
            //if ( !$this->already_TaskCreated($user_id, $auth_id, $payload['action']) ) {
                    
            // Instantiate the client and queue name.
            $client = new CloudTasksClient(['credentials' => '../app/Listeners/configGoogleCloud.json', 'projectId' => $projectId ]);
           
            $queueName = $client->queueName($projectId, $locationId, $queueId);
           
           
            // Create an Http Request Object.
            $httpRequest = new HttpRequest();
            $httpRequest->setUrl($url);
            $httpRequest->setHttpMethod(HttpMethod::POST);
                
            if (isset($payload)) {
                $httpRequest->setBody(json_encode($payload));
            }

            $task = new Task();
            $task->setHttpRequest($httpRequest);

            $response = $client->createTask($queueName, $task);
            echo 'Tarea creada .. '. $response->getName().PHP_EOL;
            echo json_encode($payload) . PHP_EOL;
            $this->persistGCloudTask_db($user_id, $auth_id, '', $interval, $payload['action'], json_encode($payload), str_replace("projects/adsconcierge/locations/us-central1/queues/".$queueId, "", $response->getName()));

            /*   }else{
                echo 'task already exists, abort task creation';
            } */

        }catch(Exception $e){
          //  dd($e);
        }
       
    }
}
