<?php

namespace App\Listeners;

use App\Events\CloneEntity;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

use DB;
use Config;

use Illuminate\Support\Facades\Log;
use Illuminate\Pipeline\Pipeline;

use App\Campaign_platform;
use App\Campaign_platform_atomo;
use App\Creatividades;
use App\Customer;
use App\Campaign;
use App\Creativity;

use App\Events\newCronEvent;
use App\Events\newCronJobEvent;
use App\Events\TaskCreate;


class CloneEntityDB
{

    


    public function handle($eventDatos)
    {
        
        /* objeto recibido        

            array:5 [
            "entity" => "campaigns"
            "assignTo" => "06e33187-c2e0-11ea-86d6-de2e501f7018"
            "campaign" => "52369427-c2e0-11ea-86d6-de2e501f7018"
            "item_type" => "campaigns"
            "item_id" => "52369427-c2e0-11ea-86d6-de2e501f7018"
            ] 
        
        */

        $event = $eventDatos->eventData;

        //dd($event);
        //var_dump($event);

        switch ($event['entity']) {
            
            case "campaigns":
                
                $cliente = Customer::where('public_id', $event['assignTo'])->select('id','public_id')->first();
                $id_customer = $cliente["id"];


                $old = Campaign::where('public_id', $event['item_id'])->where('user_id', Auth::user()->id )->first();
                $new = $old->replicate();

                //chequeamos ads_accounts
                //var_dump($old->ads_accounts);
                $adsOldCampaign = $old->ads_accounts ? (array) $old->ads_accounts : array();
                //dd($adsOldCampaign);

                $adsToDuplicate = array();
                
                foreach($adsOldCampaign as $ads){
                    $adsAcc = DB::table('ads_accounts')->where('public_id', $ads)->where('customers','like','%'.$cliente["public_id"].'%')->select('public_id')->get();
                    if (count($adsAcc)>0){
                        array_push($adsToDuplicate, $adsAcc['public_id']);
                        array_push($adsToDuplicate, $adsAcc['public_id']);
                    }
                }

                $propsOldCampaign = $old->properties_accounts ? json_decode($old->properties_accounts) :array();
                $propsToDuplicate = array();
                
                foreach($propsOldCampaign as $props){
                    $propAcc = DB::table('properties_accounts')->where('public_id', $props)->where('customers','like','%'.$cliente["public_id"].'%')->select('public_id')->get();
                    if (count($propAcc)>0){
                        array_push($propsToDuplicate, $propAcc['public_id']);
                    }
                }


                $new->public_id = null;
                $new->status = 'DRAFT';
                
                $new->ads_accounts = json_encode($adsToDuplicate);
                $new->platforms_properties_ids = json_encode($propsToDuplicate);

                $new->customer_id = $id_customer;
                $new->save();
                break;

            case "customer":

                break;

            case "campaigns_platform":

                break;

            case "atomo":

                $results = Campaign::where('public_id', $event['assignTo'])->select('id')->first();

                $id_campaigns = $results["id"];

                $old = Campaign_platform_atomo::where('public_id', $event['item_id'])->where('user_id', Auth::user()->id  )->first();
                $new = $old->replicate();
                $new->campana_root_id = $id_campaigns;
                $new->id_en_platform = time();
                $new->public_id = null;
                $new->status = 'DRAFT';
                $new->save();

                break;
            
            case "creativity":


                $results = Campaign::where('public_id', $event['assignTo'])->select('id')->first();

                $id_campaigns = $results["id"];

                $old = Creativity::where('public_id', $event['item_id'])->where('user_id', Auth::user()->id )->first();
                
                $new = $old->replicate();
                
                $new->campana_root = $id_campaigns;
                $new->public_id = null;
                
                $new->status = 'DRAFT';
        
                $new->save();

                break;
        
        }
    }



}
