<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Auth;
use Google\Cloud\Scheduler\V1\HttpTarget;
use Google\Cloud\Scheduler\V1\CloudSchedulerClient;
use Google\Cloud\Scheduler\V1\Job;
use Google\Cloud\Scheduler\V1\Job\State;

use Google\Cloud\Storage\StorageClient;

class newCronJobListener
{

    public function __construct()
    {

    }


    public function getAuthFrom_authId($id){
        return  DB::table('auths_user_platform')->where('id', $id)->get();    
    } 


    public function already_TaskCreated($userId, $authId, $action){

        if ( DB::table('gcloud_tasks')->where('user_id', $userId)->where('auth_id', $authId)->where('action', $action)->count() > 0 ){
            return true;
        }else{
            return false;
        }

    }
    public function persistGCloudTask_db($userId, $authId, $googleClient, $interval, $action, $json, $taskId, $cron){

        $result =  DB::table('gcloud_tasks')->upsert( [ 'user_id' => $userId,
                                                    'auth_id' => $authId,
                                                    'google_client_id' => $googleClient,                                                   
                                                    'interval' => $interval,
                                                    'action' => $action,
                                                    'json' => $json,
                                                    'task_id' => $taskId ,
                                                    'platform' => (isset($cron['platform']))? $cron['platform'] : '-'
                                                      ],
                                                    ['user_id', 'auth_id', 'action'], // si estas columnas ya existen con los datos proporcionados, ejecuta el update
                                                    ['json'] // y si ejecuta update, solo actualiza este campo
                                                );
    
        //dd(DB::getPDO()->lastInsertId());
 
        return $result;

    }

    public function gCloudCronSchedule($event){

        $projectId = 'adsconcierge';
        $location = 'us-central1';
        
        //dd($event);
        $cron = $event->cronjob;
        //dd($cron);
        if (!isset($cron['Execution_interval'])){
            $cron['Execution_interval'] = '12H';
        }
$minute=random_int(1, 50);
$horarandom=random_int(0,23);
 //to-do aplicar intervalo en funcion del tier del usuario
      
        switch ($cron['Execution_interval']) {
            case "12H":
                $intervalScheduled = $minute." */3 * * *";
               // $intervalScheduled = '0 */12 * * *';
                break;
            case "24H":
                $intervalScheduled = "$minute $horarandom * * *";
                break;
        }
 
     //   echo $intervalScheduled.PHP_EOL;
//exit;
        // segun la accion formamos el payload
        switch ($cron['type']) {
            case "retrieve_stats_all":
                $auth = $this->getAuthFrom_authId($cron['subject_id']);
                $auth_id = $auth[0]->public_id;
                $user_id = $auth[0]->user_id;
                $payload = array("auth_publicId" => $auth_id, "action"=> "retrieve_stats_all");
                //echo json_encode($payload);
                break;

            case "retrieve_all":
                $auth = $this->getAuthFrom_authId($cron['subject_id']);
                $auth_id = $auth[0]->public_id;
                $user_id = $auth[0]->user_id;
                $payload = array("auth_publicId" => $auth_id, "action"=> "retrieve_all");
                //echo json_encode($payload);
                break;

                
        }

$jobname= $cron['prefix'].$cron['type']  ;

        if (!$this->already_TaskCreated(Auth::user()->id, $cron['subject_id'], $cron['type'])) {
            $cloudSchedulerClient = new CloudSchedulerClient(['credentials' => '../app/Listeners/configGoogleCloud.json', 'projectId' => $projectId ]);
            $parent = $cloudSchedulerClient->locationName($projectId, $location);

            try {
 
                $jobname= $cron['prefix'].$cron['type']  ;
 
                $job = new Job([
                    'name' => CloudSchedulerClient::jobName($projectId, $location, $jobname  ),
                    'http_target' => new HttpTarget(
                        [
                            'uri' => 'https://' . $location . '-' . $projectId . '.cloudfunctions.net/' . $cron['function'],
                            'body' => json_encode($payload)
                        ]
                    ),
                    'schedule' => $intervalScheduled ,
                  'time_zone' => 'Europe/Madrid'
                  
            ]);
             

               $response = $cloudSchedulerClient->createJob($parent, $job);
 
            
              //  echo $job->getName();
              //die();
                $this->persistGCloudTask_db(Auth::user()->id, $cron['subject_id'], '', $intervalScheduled, $cron['type'], json_encode($payload),  str_replace('projects/adsconcierge/locations/us-central1/jobs/','', $job->getName()), $cron );

 

                //var_dump($response);

                /*
                 foreach ($cloudSchedulerClient->listJobs($parent) as $job) {
                    printf(
                        'Job: %s : %s' . PHP_EOL,
                        $job->getName(),
                        State::name($job->getState())
                    );
                } */
            } catch (Exception $ex) {
 echo $ex->getMessage();
}
          finally {
                $cloudSchedulerClient->close();
            }
        }else{
            echo 'no scheduler available, already running.';
        }
    }




    public function handle($cronjob)
    {
        
        

        $this->gCloudCronSchedule($cronjob);
        //dd($cronjob);
        /* if ( DB::connection('stats')->table('background_job')->insert($cronjob) ){
            
            // llamamos cloud google dayScheduleSelector
            //$this->gCloudCronSchedule($cronjob);

            return true;
        
        }else{
        
            return false;
        } */
    }


}