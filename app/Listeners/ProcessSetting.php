<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Creatividades;

require_once __DIR__ . '/../public/apis/configs/general.php';


class ProcessSetting
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle($event)
    {
        // procesamos URL de Settings -->> $event->settings->itemschoose
        // para rss procesamos y creamos creas
        // $thumbAttr = $item->children('media', true)->thumbnail->attributes();
                
        if ($event->settings->sourcetype == 'RSS'){
            
            $rss = simplexml_load_file($event->settings->Source_url);            
            $items = $rss->channel->xpath("//item");

            switch ($event->settings->itemschoose) {
                case 'Recent First':
                    usort($items, function ($a, $b) { return strtotime($b->pubDate) > strtotime($a->pubDate); });
                    break;
                case 'Old First':
                    usort($items, function ($a, $b) { return strtotime($b->pubDate) < strtotime($a->pubDate); });
                    break;
                case 'Top to bottom':
                    // nothing to do
                    break;
                case 'Random':
                    // todo random
                    break;
            }

            // retiramos lo que no nos interesa
            $filtered_Data = array_slice($items, 0, $event->settings->itemstoread);
            
            if(!empty($filtered_Data)){
                
                foreach ($filtered_Data->item as $item) {
                    /* $newcampaign = $dbconn->prepare(" INSERT INTO `app_thesoci_9c37`.`creatividades` (`id`, `id_en_platform`, `user_id`, `customer_id`, `name`, `title`, `content`, `banner`, `macrotag`, `type`, `size`, `impressions`, `clicks`, `ctr`, `costs_per_click`, `conversion_costs`, `platform`, `campana_root`, `campana_platform_id`, `atomo_id`, `description`, `linkdescription`, `calltoaction`, `url`, `type_platform`, `metadata`, `status`, `platform_status`, `platform_configuration`, `public_id`, `darkpost_id`, `budget`, `source`) VALUES   ( NULL,  ); ");
                    $newcampaign->bind_param("sis", ...['Campaigns Default ' . $user['email'], $user['id'], $customer_id]);
                    $newcampaign->execute();
                    $campaign_root = $newcampaign->insert_id;   
                    */                  
                }
            }

 

           





        }

    }

}