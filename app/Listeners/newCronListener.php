<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

use Google\Cloud\Tasks\V2\CloudTasksClient;
use Google\Cloud\Tasks\V2\HttpMethod;
use Google\Cloud\Tasks\V2\HttpRequest;
use Google\Cloud\Tasks\V2\Task;

class newCronListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function gCloudTaskCreate($event){

/*         $projectId = 'adsconcierge';
        $location = 'us-central1';
        $function = "function-test-job";
        $topicName = "ads-back-cron-job";

        $interval = substr($event['Execution_interval'],0,2);
        $pubsub = new PubSubClient([ 'projectId' => $projectId, 'keyFilePath' => '/Users/ecarasa/Desktop/adsconcierge-beta/app/Listeners/configGoogleCloud.json' ]);
    

        $client = new CloudTasksClient();
        $queueName = $client->queueName($projectId, $locationId, $queueId);


        $topic = $pubsub->topic($topicName);
        $topic->publish(['data' => json_encode($event)]);

        // 4.creamos coronjob y ejecutamos
        $cloudSchedulerClient = new CloudSchedulerClient(['credentials' => '/Users/ecarasa/Desktop/adsconcierge-beta/app/Listeners/configGoogleCloud.json', 'projectId' => $projectId ]);
        $parent = $cloudSchedulerClient->locationName($projectId, $location);

        try {

            $job = new Job([ 'name' => CloudSchedulerClient::jobName( $projectId, $location, uniqid() ),
                'http_target' => new HttpTarget([ 'uri' => 'https://' . $location . '-' . $projectId . '.cloudfunctions.net/' . $function ]),
                'schedule' => $interval . ' * * * *' ]);

            $response = $cloudSchedulerClient->createJob($parent, $job);
        
        } finally {
            $cloudSchedulerClient->close();
        }   */

    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($cron)
    {

        /* 
            type
            subject_id
            status
            action_type
            Execution_interval
            nex_execution
            last_update
            fieldsaffected 
        */

        if ( DB::table('background_cron')->insert($cron)){

            // llamamos cloud google dayScheduleSelector
            $this->gCloudTaskCreate($cron);

            return true;
        }else{
            return false;
        }

    }
}