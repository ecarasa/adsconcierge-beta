<?php
 
namespace App;
 
//use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;
 
class OvhLogger  extends AbstractProcessingHandler
{

    protected $recordbase;
    protected $headers;
    protected $ovhurl;

    public function __invoke(array $config)
    {
 
        return new Logger('custom');
    }
  
    public function __construct(string $tokenlog, string $auth, string $url )
    {
        $this->ovhurl = $url;
        $this->headers =   array(    "Authorization: Basic ".$auth ,    "Content-Type: application/json" );
        $this->recordbase =  ["version"=>"1.1", "_X-OVH-TOKEN"=>$tokenlog ];
    }
  
        protected function write(array $record): void
    {
     
         
        $recordbase=  $this->recordbase;
        $recordbase['timestamp'] = time();
        $recordbase['host']= "adsconcierge.com";
        $recordbase['short_message'] = $record['message'];
        $recordbase['full_message'] = json_encode($record);
        $recordbase['level'] = $record['level_name'];
        $userid =  isset(auth()->user()->public_id) ?auth()->user()->public_id:null;
        //  $recordbase['_user'] =  $userid;
     
          $curl = curl_init($this->ovhurl);
          curl_setopt($curl, CURLOPT_HTTPHEADER,  $this->headers);
           curl_setopt($curl, CURLOPT_POST, true);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
     //      print_r($record);
       //    print_r($recordbase);
        
      
          curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($recordbase));

          //for debug only!
          curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

         $resp = curl_exec($curl);
          curl_close($curl);
      // var_dump($resp);
  
    }
 
}

 