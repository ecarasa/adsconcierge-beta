<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use App\Listeners\ProcessSetting;
use App\Listeners\newCronJobEvent;
use App\Listeners\newCronEvent;
use App\Listeners\GCloudTaskCreate_JOB;
use App\Listeners\FetchDataReportV3;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \App\Events\SettingsRegistered::class => [
            ProcessSetting::class,
        ],
        \App\Events\newCronJobEvent::class => [
            \App\Listeners\newCronJobListener::class,
        ],
        \App\Events\newCronEvent::class => [
            \App\Listeners\newCronListener::class,
        ],
        \App\Events\TaskCreate::class => [
            \App\Listeners\GCloudTaskCreate_JOB::class,
        ],
        \App\Events\DataReportingV3::class => [
            \App\Listeners\FetchDataReportV3::class,
        ],
        \App\Events\UpdateEntity::class => [
            \App\Listeners\UpdateEntityDB::class,
        ],
        \App\Events\CloneEntity::class => [
            \App\Listeners\CloneEntityDB::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
