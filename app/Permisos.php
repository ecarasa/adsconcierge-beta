<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permisos extends Model
{
    
    protected $table = 'permisos';
    
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'plataformas', 'num_plataformas', 'num_campanas', 'cross_plataformas', 'crear_usuarios', 'compartir_reportes', 'importe_maximo_mes'
    ];
    
    protected $primaryKey = 'user_id';
    
}
