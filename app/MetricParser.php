<?php


namespace App;


use Illuminate\Support\Facades\DB;

class MetricParser
{
    public static function parse(array $metrics): array
    {
        return array_map(function (string $metric) {
            return DB::raw("SUM({$metric}) AS {$metric}");
        } , $metrics);
    }
}
