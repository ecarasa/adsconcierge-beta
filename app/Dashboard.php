<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
protected $table = 'dashboards';
        
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'configuration','id', 'public_id'
    ];


  

}
