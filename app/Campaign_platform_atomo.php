<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;

class Campaign_platform_atomo extends Model
{
    protected $table = 'campaigns_platform_atomo';

    protected $fillable = [
        'id',
        'user_id',
        'id_en_platform',
        'platform',
        'app_id',
        'campana_root_id',
        'campana_platform_id',
        'auth_id',
        'name',
        'created_at',
        'updated_at',
        'start',
        'end',
        'budget',
        'budget_total',
        'ctr',
        'cpc',
        'clicks',
        'impression',
        'spent',
        'conversion',
        'cpm',
        'cpa',
        'customer_id',
        'geo_include',
        'geo_exclude',
        'ad_account',
        'property_id',
        'platform_placements',
        'gender',
        'age',
        'language',
        'platform_propertie_id',
        'targeting_include',
        'metadata',
        'targeting_exclude',
        'device',
        'conectivity',
        'platforma_budget',
        'creativities_ids',
        'status',
        'objetivo',
        'chargeby',
        'bid_unit',
        'bid_type',
        'puja_valor',
        'optimizacion',
        'product_type',
        'public_id',
        'targeting_exclude_full',
        'targeting_include_full',
        'locale'

    ];

}