<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appsettings extends Model
{
     protected $table = 'app_settings';
   protected $primaryKey = 'id';
   protected $keyType = 'string';
  protected $fillable = ['id', 'keyname','value'];
}
