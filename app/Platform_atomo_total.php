<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;

class Platform_atomo_total extends Model
{
    protected $connection = 'stats';
    protected $table = 'platform_atomo_total';

    protected $fillable = [
        'unico',
        'user_id',
        'clicks',
        'impressions',


    ];

}
