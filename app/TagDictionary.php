<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagDictionary extends Model
{
    protected $table = 'organic_tagdictionary';

    public $timestamps = false;

    protected $dates = [
        'creationtime',
        'lasttimeupdate'
    ];

    protected $appends = ['actions'];


    public function getActionsAttribute()
    {
        return '';
    }
}
