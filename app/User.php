<?php

namespace App;

use App\Role;
use App\Permisos;
use App\Campaign;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;

//use TijmenWierenga\LaravelChargebee\Billable;

class User extends Authenticatable
{
    //use SoftDeletes;
    /*use Notifiable;
    use Billable;*/
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'office', 'role', 'consumo', 'plan', 'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function permisos()
    {
        return $this->hasOne(Permisos::class , 'user_id');
    }

    public function checkPlatform($value)
    {


        if ( Session::has('permisos') ){
            $permisos = Session::get('permisos');
            $permisos = json_decode($permisos);

            //print_r($permisos->plataformas);

            if (isset($permisos->plataformas)){
                $plataformas = (array) $permisos->plataformas;
                return in_array($value, $plataformas) ?  true : false;
            }
       
        }
       
        return false;
    }

    public function checkPlatformArr($arr)
    {

        if ( Session::has('permisos') ){
            $permisos = Session::get('permisos');
            $permisos = json_decode($permisos);

            if (isset($permisos->plataformas)){
                $plataformas = (array) $permisos->plataformas;
                //print_r( $plataformas );
                return empty(array_diff($arr, $plataformas));
            }
        }
       
        return false;
    }

    public function getRolePermission($key, $val=null)
    {
        if ( Session::has('permisos') ){
            $permisos = Session::get('permisos');
            $permisos = (array) json_decode($permisos);

            if ($val==null){
                return isset($permisos[$key]) ?  $permisos[$key] : false;
            }else{
                return isset($permisos[$key]) && $permisos[$key] == $val ?  true : false;
            }
        
        }
       
        return false;
    }
    


    
    public function damepermisos($permiso="")
    {
        /*$permisos = $this->hasOne(Permisos::class , 'user_id')->select($permiso. ' as permiso')->get();
        
        if (count($permisos) == 0){
            return false;
        }elseif($permisos[0]->permiso == 1){
            return true;
        }elseif($permisos[0]->permiso > 1){
            return $permisos[0]->permiso;
        }     
        
        return false;
        */
    }
    
    public function campaigns()
    {
        return $this->hasMany(Campaign::class , 'user_id');
    }
    
    public function validarplataforma( )
    {
        
        
        //New       

        if ( Session::has('permisos') ){
            $permisos = Session::get('permisos');
            $permisos = (array) json_decode($permisos);
         
            $findme   = '+';
            $pos = strpos($permisos['num_plataformas'], $findme);

            if ($pos !== false) {
                // tiene el +
                return 99;
            } else {
                return isset($permisos['num_plataformas']) ?  $permisos['num_plataformas'] : false;
            }

            

        }

        return false;




        // OLD
        $num_plataformas = $this->damepermisos("num_plataformas");
        dd( $num_plataformas);
            
        $auths_user_platform = $this->hasMany(auths_user_platform::class , 'user_id')->get();
        
        if($num_plataformas > count($auths_user_platform)){
            return true;
        }
        
        return false;
    }
    
    public function roles()
    {
      return false;
        return $this->belongsToMany(Role::class , 'role_user', 'user_id', 'role_id' )->withTimestamps();
    }

    public function offices()
    {
        return $this->belongsToMany(Office::class , 'office_id' );
    }

    public function authorizeRoles($roles)
    {
      return false;
        if ($this->hasAnyRole($roles)) {
            return true;
        }else
            abort(401, 'Esta acción no está autorizada.');
    }

    public function hasAnyRole($roles)
    {
      return false;
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
      return false;
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }


    public function getLastIdRole()
    {
      return false;
        foreach ($this->roles as $role) {
            return $role->id;
        }
        return false;
    }



}
