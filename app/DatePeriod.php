<?php


namespace App;

class DatePeriod
{
    public const DATE_FORMAT = 'Y-m-d';


    private const ALLOWED_PERIODS = [
        'today' => [
            false, // 00:00
            'now', // 23:59
        ],
        'yesterday' => [
            '-1 days',
            false,
        ],
        'last_2days' => [
            '-2 days',
            '-1 days',
        ],
        'this_week' => [
            'Monday this week',
            false,
        ],
        'last_week' => [
            'Monday last week',
            'Sunday last week',
        ],
        'last_7days' => [
            '-7 days',
            false,
        ],
        'this_month' => [
            'first day of this month',
            false,
        ],
        'last_month' => [
            'first day of last month',
            'last day of last month',
        ],
        'last_30days' => [
            '-30 days',
            false,
        ],
        'all_time' => [
            'first day of January 2021',
            false,
        ]
    ];

    public static function fromString(string $period): array
    {
        if (false === isset(self::ALLOWED_PERIODS[$period])) {
            throw new \Exception('invalid date period');
        }

        list($diffStart, $diffEnd) = self::ALLOWED_PERIODS[$period];

        $startDate = new \DateTimeImmutable('today midnight');
        if (false !== $diffStart) {
            $startDate = $startDate->modify($diffStart);
        }

        $endDate = new \DateTimeImmutable('today midnight');
        if (false !== $diffEnd) {
            $endDate = $endDate->modify($diffEnd);
        }

        return [$startDate, $endDate];
    }

}
