<?php

namespace App\Http\Middleware;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Authenticatable;
use App\User;
use Auth;
use Log;
use Session;
use App\Appsettings;
class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }
    
    protected function authenticate($request, array $guards)
    {
        if (empty($guards)) {
            $guards = [null];
        }

        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }
      
      
         
 
       
      
        if(isset($_COOKIE['id_user']) && Auth::user()){
            //dd($_COOKIE['id_user']);
      $metadata = Appsettings::find( md5( 'plan:'. Auth::user()->plan  ) , [ 'value' ] ) ;   
      if($metadata ) {
       Session::put('permisos', $metadata->value); 
      }
          
            $user = User::where('hashuser', $_COOKIE['id_user'])->first();
            if (isset($user)&&(Auth::loginUsingId($user->id)))
            {
                return route('index');
            }
        }
        
        //Log::channel('ovhlogs')->warning('Login fails failed.'.json_encode( $validator->messages() ),  [ '_context' => 'campaignWizard', '_action' => 'mysql_insert', 'short_message' => json_decode( $validator->messages() ) ] );
        throw new AuthenticationException(
            'Unauthenticated.', $guards, $this->redirectTo($request)
        );
    }
    
}
