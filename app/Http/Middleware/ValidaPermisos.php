<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
  use Illuminate\Support\Facades\Redis;
use App\Appsettings;


class ValidaPermisos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
     /**  print_r( $request->route()->getActionName());
          $metadata = Appsettings::find( md5( '_plan:'. Auth::user()->plan  ) , [ 'value' ] ) ;
      print_r (Auth::user()->plan );
      print_r ( $metadata  );
    
       Session::put('_permisos', $metadata->value);
      //print_r($request->session() );
     exit;
     ***/
      /**
      if (Auth::user()  ) {
        $metadata = Appsettings::find( md5( 'plan:'. Auth::user()->plan  ) , [ 'value' ] ) ;
 
$permisos= json_decode($metadata->value, true);
  
    Redis::hSet('userpermisos:'.Auth::user()->public_id,  'permisos ', json_encode($permisos )   );
      foreach (array_keys($permisos) as $key   ) {
       $itemvalue=is_array($permisos[$key] )? json_encode($permisos[$key] ): $permisos[$key]; 
        Redis::hSet('userpermisos:'.Auth::user()->public_id,  $key, $itemvalue   );
      }
        
         
      }
      ****/
 
     if ( $request->route()->getActionName()!= "App\Http\Controllers\HomeController@subscription" 
         &&  $request->route()->getActionName()!= "App\Http\Controllers\Auth\LoginController@showLoginForm"
         &&  $request->route()->getActionName()!= "App\Http\Controllers\Auth\LoginController@login"
         && Auth::user()->status!='active') {
      
        return redirect()->route('mysubscription');
       
     }  
   
          return $next($request);
      
       
    }
}
