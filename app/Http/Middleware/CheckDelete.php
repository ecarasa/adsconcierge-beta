<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckDelete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (auth()->check() && Auth::user()->deleted_at && now()->greaterThan(Auth::user()->deleted_at)) {
			$delete_days = now()->diffInDays(Auth::user()->deleted_at); 
			echo $message = 'Your account has been suspended for '.$delete_days.' '.str_plural('day', $delete_days).'. Please contact administrator.';
            
            auth()->logout();    
            die();
			return redirect()->route('login')->withMessage($message); 
        }
        return $next($request);
    }
}
