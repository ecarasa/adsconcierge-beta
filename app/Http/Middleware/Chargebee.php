<?php
//esta libreria es para la conexion con chargebee en el login del user
//desactivada el 17-18 agosto 2021
namespace App\Http\Middleware;

use App\Permisos;
use App\Role;
use App\User;
use App\Appsettings;
use Auth;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Session;

class Chargebee
{

    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        $url = $request->path();

        Session::forget('permisos');

        if (!is_null($user) && $url != "subscription" && $url != "delete_perfil") {
            if (!Session::has('permisos')) {      
                //header('x-debug:chrbsubscriptions');
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://adsconcierge.chargebee.com/api/v2/subscriptions/" . md5($user->email));
                curl_setopt($ch, CURLOPT_USERPWD, "live_UhGIHjzvcdOWTwEU28Rt1Mk0khGjVGk40");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $server_output = json_decode(curl_exec($ch));
        
                if ($error_number = curl_errno($ch)) {
                    if (in_array($error_number, array(CURLE_OPERATION_TIMEDOUT, CURLE_OPERATION_TIMEOUTED))) {
                        print_r(curl_errno($ch));
                    }
                }

                curl_close($ch);

                if (isset($server_output->error_code) && $server_output->error_code == 'resource_not_found') {
                    $server_output = $this->create($user);
                }

                $status = ["active", "in_trial", "non_renewing"];

                //si el tio no tiene un plan "active" lo enviamos a mostrar la pagina de planes              
                if (!isset($server_output->subscription->status) ||
                    (!in_array($server_output->subscription->status, $status))) {
                    return redirect()->route('subscription');
                }
            
                //print_r($server_output);
                        
                if (isset($server_output->subscription->plan_id)) {

                    $metadata = Appsettings::find( md5( 'plan:'. $server_output->subscription->plan_id  ) , [ 'value', 'keyname'] );   

                    if ( $metadata == null ){
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, "https://adsconcierge.chargebee.com/api/v2/plans/" .$server_output->subscription->plan_id);
                        curl_setopt($ch, CURLOPT_USERPWD, "live_UhGIHjzvcdOWTwEU28Rt1Mk0khGjVGk40");
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        
                        $plans_output = json_decode(curl_exec($ch));
                        //$permisos= $plans_output->plan->meta_data;
                        $permisos = json_encode($plans_output->plan->meta_data);

                        Appsettings::create([
                            'id' => md5( 'plan:'. $plans_output->plan->id  ),
                            'keyname' => 'plan:'.$plans_output->plan->id,
                            'value' => json_encode($plans_output->plan->meta_data)
                        ]);

                    } else {
                        $permisos = $metadata['value'];
                    }
                }

                Session::put('permisos', $permisos);

            } //fin if no-existen permisos en $session
        }
       
        return $next($request);
    }

    private function create($user)
    {
        $param = array(
            "id" => md5($user->email),
            "plan_id" => $user->plan,
            "customer" => array(
                "id" => md5($user->email),
                "first_name" => $user->name,
                "email" => $user->email,
            ),
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://adsconcierge.chargebee.com/api/v2/subscriptions");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "live_UhGIHjzvcdOWTwEU28Rt1Mk0khGjVGk40");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));

        //var_dump($param);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($error_number = curl_errno($ch)) {
            if (in_array($error_number, array(CURLE_OPERATION_TIMEDOUT, CURLE_OPERATION_TIMEOUTED))) {
                print_r(curl_errno($ch));
            }
        }
        $server_output = curl_exec($ch);
        //dd($server_output);
        curl_close($ch);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://adsconcierge.chargebee.com/api/v2/subscriptions/" . md5($user->email));
        curl_setopt($ch, CURLOPT_USERPWD, "live_UhGIHjzvcdOWTwEU28Rt1Mk0khGjVGk40");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = json_decode(curl_exec($ch));
  
        if ($error_number = curl_errno($ch)) {
            if (in_array($error_number, array(CURLE_OPERATION_TIMEDOUT, CURLE_OPERATION_TIMEOUTED))) {
                print_r(curl_errno($ch));
            }
        }
        curl_close($ch);

        return ($server_output);
    }

    protected function authenticate($request, array $guards)
    {
        if (empty($guards)) {
            $guards = [null];
        }
        dd("2asd");
        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }

        if (isset($_COOKIE['id_user'])) {
            $user = User::where('hashuser', $_COOKIE['id_user'])->first();
            if (Auth::loginUsingId($user->id)) {
                return route('index');
            }
        }

        throw new AuthenticationException('Unauthenticated.', $guards, $this->redirectTo($request));

    }

}
