<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Dashboard;
use App\Platform;
//use TijmenWierenga\LaravelChargebee\Subscriber;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    private $user;
    public $plans = [
        'cbdemo_free', 'cbdemo_grow', 'cbdemo_hustle', 'cbdemo_scale',
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->user = Auth::user();


        if (isset($this->user->hashuser)) {
            setcookie("id_user", $this->user->hashuser, time() + 3600);
        }

       // var_dump(Auth::check());


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function asdindex(Request $request)
    {
        return redirect()->action(
            'SubscriptionController@create'
        );
    }

    public function index(Request $request, string $campaing_array = "")
    {

        $data = array();
        $user = Auth::user();
        $user_id = $user->id;

        if ($campaing_array != "") {

            $ids_campañas = explode(",", $campaing_array);
            $publicsIdsQuoted = "";

            foreach ($ids_campañas as $idPublic) {
                $publicsIdsQuoted .= "'" . $idPublic . "',";
            }

            $publicsIdsQuoted = substr($publicsIdsQuoted, 0, -1);

            /**$campaigns = DB::table('campaigns')->join('customers', 'campaigns.customer_id', '=', 'customers.id')->where('campaigns.user_id', $user_id)->whereIn('customers.public_id', explode(",", $publicsIdsQuoted))->select('campaigns.public_id as id', 'campaigns.name', 'customers.public_id as customer_id', 'campaigns.status', 'campaigns.plataformas as platforms')->get();*/
            $campaigns = DB::select(DB::raw("select `campaigns`.`public_id` as `id`, `campaigns`.`name`, `customers`.`public_id` as `customer_id`, `campaigns`.`status`, `campaigns`.`plataformas` as `platforms` from `campaigns` inner join `customers` on `campaigns`.`customer_id` = `customers`.`id` where `campaigns`.`user_id` = 6 and `customers`.`public_id` in (" . $publicsIdsQuoted . ")"));

            return $campaigns;
        } else {
            $campaigns = DB::table('campaigns')
                        ->where('user_id', $user_id)
                        ->select('public_id as id', 'name', 'customer_id', 'status', 'plataformas as platforms')->get();
        }

        $dashboard = Dashboard::where('user_id', $user_id)->get();
        $customers = Customer::select('id', 'public_id', 'name')->where('user_id', '=', $user_id)->get();
        $channels = Platform::all();


        $auth = DB::table('auths_user_platform')->select('access_token')->where('user_id', $user->id)->where('platform', "FACEBOOK")->where('activa', "Y")->first();
        $data["token_facebook"] = (isset($auth->access_token) && $auth->access_token != "") ? $auth->access_token : "";

        $data["reports"] = ['dashboards' => $dashboard];
        $data["channels"] = ['channels' => $channels];
        $data["campaigns"] = $campaigns;
        $data["title"] = "Accounts";
        $data["customers"] = $customers;
        $data["account"] = [
            'title' => 'Customers',
            //spend        impressions        clicks  ctr cpc   conversions   cr cpa   cpm
            //'columns' => ['id', 'customer', 'spend', 'impressions', 'clicks', 'cpc', 'cpm', 'ctr', 'conversions'],
            'columns' => ['id', 'customer', 'spend', 'impressions', 'clicks', 'ctr', 'cpc', 'conversions', 'cr', 'cpa','cpm'],
            'api' => '/api/accounts',
            'base' => 'ssss',
        ];

        return view('template.dashboard', $data);

    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function campana(Request $request)
    {
        $user = Auth::user();


        $data['devices'] = DB::table('mapeo_x_plataforma')->where('campo', 'devices')->orderBy('id')->get();
        $data['url_placeholder'] = DB::table('app_settings')->where('keyname', 'url_placeholder')->get();
        $data['gender'] = DB::table('mapeo_x_plataforma')->where('campo', 'gender')->orderBy('id')->get();
        $data['conectivity'] = DB::table('mapeo_x_plataforma')->where('campo', 'conectivity')->orderBy('id')->get();
        $data['customers'] = DB::table('customers')->where('user_id', $user->id)->orderBy('name')->get();
        $data['placements'] = DB::table('placements')->join('platforms', 'placements.platform', '=', 'platforms.id')->orderBy('platform')->select('placements.id','placements.public_id', 'placements.name', 'platforms.name as platforms_name')->get();
        
        $data['ads_accounts_facebook'] = DB::table('ads_accounts')->select('public_id', 'name', 'currency', 'account_id')->where('platform', 1)->get();
        $data['properties_accounts_facebook']=array();
        $data['pixels_facebook']=array();
        if ( $data['ads_accounts_facebook']->get(0)  !== null  ){
            $account_id = $data['ads_accounts_facebook']->get(0)->account_id;
            //$data['properties_accounts_facebook'] = DB::table('properties_accounts')->select('public_id', 'name', 'platform')->where('platform', 1)->where('adaccount_id', $account_id)->get();
            //$data['pixels_facebook'] = DB::table('properties_adsaccount_relations')->select('property_publicid as id', 'property_name as name')->where('platform', 1)->where('type', '=', 'PIXEL')->where('user_id', $user->id)->orderBy('name')->get();
            $data['properties_accounts_facebook']=array();
            $data['pixels_facebook']=array();
        }

        $data['ads_accounts_linkedin'] = DB::table('ads_accounts')->select('public_id', 'name', 'currency', 'account_id')->where('platform', 2)->get();
        $data['properties_accounts_linkedin']=array();
        $data['pixels_linkedin']=array();
        if ( $data['ads_accounts_linkedin']->get(0)  !== null  ){
            $account_id = $data['ads_accounts_linkedin']->get(0)->account_id;
            //$data['properties_accounts_linkedin'] = DB::table('properties_accounts')->select('public_id', 'name', 'platform')->where('platform', 2)->where('adaccount_id', $account_id)->get();
            //$data['pixels_linkedin'] = DB::table('properties_adsaccount_relations')->select('property_publicid as id', 'property_name as name')->where('platform', 2)->where('type', '=', 'PIXEL')->where('user_id', $user->id)->orderBy('name')->get();
            $data['properties_accounts_linkedin']=array();
            $data['pixels_linkedin']=array();
        }

        $data['ads_accounts_twitter'] = DB::table('ads_accounts')->select('public_id', 'name', 'currency', 'account_id')->where('platform', 3)->get();
        $data['properties_accounts_twitter']=array();
        $data['pixels_twitter']=array();
        if ( $data['ads_accounts_twitter']->get(0)  !== null  ){
            $account_id = $data['ads_accounts_twitter']->get(0)->account_id;
            //$data['properties_accounts_twitter'] = DB::table('properties_accounts')->select('public_id', 'name', 'platform')->where('platform', 3)->where('adaccount_id', $account_id)->get();
            //$data['pixels_twitter'] = DB::table('properties_adsaccount_relations')->select('property_publicid as id', 'property_name as name')->where('platform', 3)->where('type', '=', 'PIXEL')->where('user_id', $user->id)->orderBy('name')->get();
            $data['properties_accounts_twitter']=array();
            $data['pixels_twitter']=array();
        }

        $data['ads_accounts_instagram'] = DB::table('ads_accounts')->select('public_id', 'name', 'currency', 'account_id')->where('platform', 4)->get();
        $data['properties_accounts_instagram']=array();
        $data['pixels_instagram']=array();
        if ( $data['ads_accounts_linkedin']->get(0)  !== null  ){
            $account_id = is_object($data['ads_accounts_instagram']->get(0)) ? $data['ads_accounts_instagram']->get(0)->account_id : 0;
           /* $data['properties_accounts_instagram'] = DB::table('properties_accounts')->select('public_id', 'name', 'platform')->where('platform', 4)->where('adaccount_id', $account_id)->get();
            
            $data['pixels_instagram'] = DB::table('properties_adsaccount_relations')
            ->select('property_publicid as id', 'property_name as name')
            ->where('platform', 4)
            ->where('type', '=', 'PIXEL')
            ->where('user_id', $user->id)->orderBy('name')
            ->get();*/
            $data['properties_accounts_instagram']=array();
            $data['pixels_instagram']=array();
        }

        $data['ads_accounts_snapchat'] = DB::table('ads_accounts')->select('public_id', 'name', 'currency', 'account_id')->where('platform', 5)->get();
        $data['properties_accounts_snapchat']=array();
        $data['pixels_snapchat']=array();
        if ( $data['ads_accounts_linkedin']->get(0)  !== null  ){
            $account_id = is_object($data['ads_accounts_snapchat']->get(0)) ? $data['ads_accounts_snapchat']->get(0)->account_id : 0;
           /* $data['properties_accounts_snapchat'] = DB::table('properties_accounts')->select('public_id', 'name', 'platform')->where('platform', 5)->where('adaccount_id', $account_id)->get();
            
            $data['pixels_snapchat'] = DB::table('properties_adsaccount_relations')
            ->select('property_publicid as id', 'property_name as name')
            ->where('platform', 5)
            ->where('type', '=', 'PIXEL')
            ->where('user_id', $user->id)->orderBy('name')
            ->get();*/
            $data['properties_accounts_snapchat']=array();
            $data['pixels_snapchat']=array();
        }

        $data['ads_accounts_amazon'] = array();
        $data['properties_accounts_amazon'] = array();
        $data['pixels_amazon'] = array();

        $data['ads_accounts_adwords'] = array();
        $data['properties_accounts_adwords'] = array();
        $data['pixels_adwords'] = array();

        $data['ganalitycs'] = DB::table('properties_adsaccount_relations')
                                        ->select('property_publicid as id', 'property_name as name')
                                        ->where('type', '=', 'GANALYTICS')
                                        ->where('user_id', $user->id)
                                        ->orderBy('name')
                                        ->get();

        $objetivos = DB::table('objetivos_matrix')->select('id', 'name', 'metadata')->get();

        $auth = DB::table('auths_user_platform')->select('access_token')->where('user_id', $user->id)->where('platform', "FACEBOOK")->where('activa', "Y")->first();
        $data["access_token_FACEBOOK"] = (isset($auth->access_token) && $auth->access_token != "") ? $auth->access_token : "";

        $auth = DB::table('auths_user_platform')->select('app_secret', 'access_token', 'app_id')->where('user_id', $user->id)->where('platform', "TWITTER")->where('activa', "Y")->first();
        $consulmer = json_decode($auth->app_secret);
        /* $data["consumer_key_TWITTER"] = $consulmer->consumer_key;
        $data["consumer_secret_TWITTER"] = $consulmer->consumer_secret;
        $data["token_TWITTER"] = $auth->app_id;
        $data["secret_TWITTER"] = $auth->access_token; */
        $data["consumer_key_TWITTER"] = (isset($consulmer->consumer_key) && $consulmer->consumer_key != "") ? $consulmer->consumer_key : "";
        $data["consumer_secret_TWITTER"] = (isset($consulmer->consumer_secret) && $consulmer->consumer_secret != "") ? $consulmer->consumer_secret : "";
        $data["token_TWITTER"] = (isset($auth->app_id) && $auth->app_id != "") ? $auth->app_id : "";
        $data["secret_TWITTER"] = (isset($auth->access_token) && $auth->access_token != "") ? $auth->access_token : "";

        $auth = DB::table('auths_user_platform')->select('access_token')->where('user_id', $user->id)->where('platform', "LINKEDIN")->where('activa', "Y")->first();
        $data["access_token_LINKEDIN"] = (isset($auth->access_token) && $auth->access_token != "") ? $auth->access_token : "";

        return view('template.campana', ['title' => 'Campaña', 'data' => $data, 'objectives' => $objetivos]);
    }


    public function campaigns(Request $request)
    {
        $user = Auth::user();
        
        $auth = DB::table('auths_user_platform')->select('access_token')->where('user_id', $user->id)->where('platform', "FACEBOOK")->where('activa', "Y")->first();
        $data["token_facebook"] = (isset($auth->access_token) && $auth->access_token != "") ? $auth->access_token : "";
        
        $data["title"] = "Campaigns";
        
        $data["account"] = [
            'title' => 'Campaigns',
            'columns' => ['id', 'campaign', 'customer', 'spend', 'impressions', 'clicks', 'ctr', 'cpc', 'conversions', 'cr', 'cpa','cpm'],
            'api' => '/api/campaigns',
            'base' => 'ssss',
        ];

        return view('template.campaigns', $data);

    }


    public function connections(string $text = "", Request $request)
    {
        $id_user = Auth::id();

        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $keyword = '';
        if (!is_null($request->get('search'))) {$keyword = $request->get('search')['value'];}

        $customer = Customer::with('users')
            ->where(function ($query) use ($keyword) {$query->whereRaw("name like  '%{$keyword}%' ");})
            ->where('user_id', $id_user)
            ->paginate($limit, ['*'], 'page', $page);

        $customer->getCollection()->transform(function ($value) {
            $data = [
                "id" => $value->public_id,
                "text" => $value->name,
            ];
            return $data;
        });

        //$request->user()->authorizeRoles(‘admin’);
        //return view(‘some.view’);
        return view('template.connections', [
            'title' => 'Connections',
            'newElement' => 'New Connections',
            //'columns' => ['rrss', 'platform_user_id', 'platform_email', 'ads_accounts', 'properties', 'default_client', 'clients', 'status', 'user'],
            'columns' => ['rrss', 'platform_user_id', 'platform_email', 'ads_accounts', 'properties', 'default_client', 'Actions'],
            'api' => '/api/connections',
            'base' => 'users',
            'entity' => 'Usuario',
            'text_message' => $text,
            'clients' => $customer->items(),
        ]);
    }

    public function ads_accounts(string $id = "")
    {
        $url = "";
        if ($id == "") {
            $url = "/api/ads_accounts";
        } else {
            $url = "/api/ads_accounts/" . $id;
        }


        $id_user = Auth::id();

        $customers = Customer::all();
        //$request->user()->authorizeRoles(‘admin’);
        //return view(‘some.view’);
        return view('template.ads_accounts', [
            'title' => 'Ads Accounts',
            'newElement' => 'New Connections',
            'columns' => ['rrss', 'name', 'platform_user_id', 'user', 'clients'],
            'api' => $url,
            'base' => 'users',
            'entity' => 'Usuario',
            'text_message' => '',
            'clients' => $customers
       ]);
    }

    public function properties(string $id = "")
    {
        $url = "";

        if ($id == "") {
            $url = "/api/properties";
        } else {
            $url = "/api/properties/" . $id;
        }

        //$request->user()->authorizeRoles(‘admin’);
        //return view(‘some.view’);
        return view('template.properties', [
            'title' => 'Properties',
            'newElement' => 'New Connections',
            'columns' => ['rrss', 'name', 'platform_user_id',  'customers'],
            'api' => $url,
            'base' => 'users',
            'entity' => 'Usuario',
        ]);
    }

    public function subscription(Request $request)
    {
        /*$request->user()->authorizeRoles(['account_manager', 'admin_group', 'superadmin']);

        $data = array();*/

        $user = Auth::user();
        $data = array(
            "title" => "Plans",
            "plan" => $user->plan,
        );

        return view('template.plan_chargebee', $data);
    }


    /*
    public function planChosen(Request $request)
    {
    $user = Auth::user();

    $subscrtiption = Subscription::where("user_id", $user->id)
    ->get()->first();
    if(Subscription::where("user_id", $user->id)
    ->count() > 0)
    {
    if ($subscrtiption->ends_at === null) {
    if ($subscrtiption->next_billing_at > Carbon::now()) {

    //If user has sub

    return redirect()
    ->action(
    "HomeController@afterChargebeeCheck"
    );
    } else {

    //If user has no sub or expired one

    $responseChargebeeUrl = (app('App\Http\Controllers\SubscribeController')->createSub($request->plan));
    if ($responseChargebeeUrl !== null && $responseChargebeeUrl !== [] && $responseChargebeeUrl !== "") {
    return redirect($responseChargebeeUrl);
    }
    }

    } else {

    if ($subscrtiption->ends_at > Carbon::now()) {

     * If user has sub valid until it ends

    return redirect()
    ->action(
    "HomeController@afterChargebeeCheck"
    );
    } else {

     * If user has no sub or expired one

    $responseChargebeeUrl = (app('App\Http\Controllers\SubscribeController')->createSub($request->plan));
    if ($responseChargebeeUrl !== null && $responseChargebeeUrl !== [] && $responseChargebeeUrl !== "") {
    return redirect($responseChargebeeUrl);
    }
    }
    }
    }else{
    /**
     * If user does not have active sub yet.

    $responseChargebeeUrl = (app('App\Http\Controllers\SubscribeController')->createSub($request->plan));
    if ($responseChargebeeUrl !== null && $responseChargebeeUrl !== [] && $responseChargebeeUrl !== "") {
    return redirect($responseChargebeeUrl);
    }
    }
    }

    public function afterChargebeeCheck(Request $request)
    {
    $data = array();

    $user = Auth::user();
    $user_id = $user->id;

    $dashboard = Dashboard::where('user_id', $user_id)->get();
    $channels = Platform::all();

    $campaigns = DB::table('campaigns')
    ->where('user_id', $user_id)
    ->select('public_id as id', 'name', 'customer_id', 'status', 'plataformas as platforms')
    ->get();

    $data["reports"] = [
    'dashboards' => $dashboard,
    ];
    $data["channels"] = [
    'channels' => $channels
    ];

    $data["campaigns"] = $campaigns;

    $data["account"] = [
    'title' => 'Accounts',
    'columns' => ['id', 'name', 'spend', 'impressions', 'clicks', 'cpc', 'cpm', 'ctr', 'conversions'],
    'api' => '/api/accounts',
    'base' => 'ssss',
    ];
    $data["title"] = "Accounts";

    //$data["user_plan"] = Subscription::where("user_id", $user->id)->get()->first()->plan_id;
    return view('template.dashboard', $data);

    }

    public function cancelSubscription(Request $request)
    {
    $cancel = app('App\Http\Controllers\SubscribeController')->cancelSubscription();
    if ($cancel) {
    $request->session()->put('cancelMassageSuccess', 'The subscription was successfully canceled. Will end at the next billing date.');
    } else {
    $request->session()->put('cancelMassageFail', 'The subscription cannot be canceled.');
    }
    return redirect()->action(
    "HomeController@afterChargebeeCheck"
    );
    }
    public function changeSubscriptionPlan(Request $request)
    {
    $change = app('App\Http\Controllers\SubscribeController')->changeSubscriptionPlan($request->plan);
    if ($change) {
    $request->session()->put('cancelMassageSuccess', 'The subscription plan has been changed successfully.');
    } else {
    $request->session()->put('cancelMassageFail', 'The subscription plan cannot be changed at the moment.');
    }
    return redirect()->action(
    "HomeController@afterChargebeeCheck"
    );
    }
     */




}

