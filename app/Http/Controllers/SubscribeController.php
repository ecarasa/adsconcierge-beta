<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use TijmenWierenga\LaravelChargebee\Billable;
//use TijmenWierenga\LaravelChargebee\HandlesWebhooks;

class SubscribeController extends Controller
{

    use Billable, HandlesWebhooks;

    public function createSub($plan)
    {
        $user = Auth::user();
        $embed = true;
        $addonId = "cbdemo_conciergesupport";
        //$plan = 'cbdemo_free';
	//dd(env("CHARGEBEE_SITE"));
        $url = $user->subscription($plan)
            ->getCheckoutUrl($embed);

        return $url;
    }

    public function handleCallbackSub($id)
    {
        $user = Auth::user();
        // Attach the subscription to the user from the hosted page identifier.
        $result = $user->subscription()
            ->registerFromHostedPage($id);
        // Return the user to a success page.
        return $result;
    }

    public function changeSubscriptionPlan($plan)
    {
        $user = Auth::user();

        // Get the subscription you want to change plans from
        $subscription = $user->subscriptions->first();

        // Change the current plan
        $result = $subscription->swap($plan);

        return $result;
    }

    public function cancelSubscription()
    {
        $user = Auth::user();
        // Get the subscription you want to change plans from
        $subscription = $user->subscriptions->first();

        return $subscription->cancel();
    }


    public function subscribedChargebee(Request $request)
    {
        $this->handleCallbackSub($request->id);

        return redirect()->action(
            "HomeController@index"
        );
    }
}
