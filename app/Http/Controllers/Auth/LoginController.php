<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Auth;
use Session;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
 use Illuminate\Support\Facades\Redis;
use App\Appsettings;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
  
      protected function authenticated(Request $request, $user)
    {
      $metadata = Appsettings::find( md5( 'plan:'. Auth::user()->plan  ) , [ 'value' ] ) ;   
      if($metadata ) {
                   Session::put('permisos', $metadata->value); 
                   Redis::hSet('userpermisos:'.Auth::user()->public_id,  'permisos ', $metadata->value  );

                  $permisos= json_decode($metadata->value, true);
                    foreach (array_keys($permisos) as $key   ) {
                        $itemvalue=is_array($permisos[$key] )? json_encode($permisos[$key] ): $permisos[$key]; 
                        Redis::hSet('userpermisos:'.Auth::user()->public_id,  $key, $itemvalue   );
                    }
         }
        
    }


    public function getLogout(Request $request){
        
       // session_start();        
    //    session_regenerate_id(true);
        
        Cookie::forget('id_user');
        unset($_COOKIE['id_user']);
     //   $_COOKIE['id_user'] = null;
        //dd("eliminamos cookie");
        Cookie::queue('id_user', 'chupame la polla', 10);
        //unset($_COOKIE['id_user']);

        Auth::logout();
        Auth::guard()->logout();

      //  $request->session()->flush();
      //  Session::flush();

        return Redirect::to(env('URL_LOGOUT', null));
        
    }


    public function __construct()
    {
  
        $this->middleware('guest')->except(['getLogout','logout']);
    }
   
   
}