<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;



use Google\Cloud\Scheduler\V1\HttpTarget;
use Google\Cloud\Scheduler\V1\CloudSchedulerClient;
use Google\Cloud\Scheduler\V1\Job;
use Google\Cloud\PubSub\PubSubClient;

use Google\Cloud\Scheduler\V1\Job\State;
use Google\Cloud\Storage\StorageClient;



class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function demoGCloud()
    {

        $projectId = 'adsconcierge';
        $location = 'us-central1';
        $function = "function-test-job";
        $topicName = "ads-back-cron-job";

        $job =  [ 'type' => 'campaign', 'subject_id' => '9999', 'action_type' => 'ads_spend_check', 'Execution_interval' => '15M', 'next_execution' => now() ];
        $interval = substr($job['Execution_interval'],0,2);

        // creamos un cloud google scheduler
        // 1 crear function gcloud 2. subsiribir tema a function
        // 3.publicamos mensaje de pbu/sub
        // export GOOGLE_APPLICATION_CREDENTIALS="/Users/ecarasa/Desktop/adsconcierge-beta/app/Listeners/configGoogleCloud.json"

        $pubsub = new PubSubClient([ 'projectId' => $projectId, 'keyFilePath' => '/Users/ecarasa/Desktop/adsconcierge-beta/app/Listeners/configGoogleCloud.json' ]);
    
        $topic = $pubsub->topic($topicName);
        $topic->publish(['data' => json_encode($job)]);

        // 4.creamos coronjob y ejecutamos
        $cloudSchedulerClient = new CloudSchedulerClient(['credentials' => '/Users/ecarasa/Desktop/adsconcierge-beta/app/Listeners/configGoogleCloud.json', 'projectId' => $projectId ]);
        $parent = $cloudSchedulerClient->locationName($projectId, $location);

        try {

            $job = new Job([ 'name' => CloudSchedulerClient::jobName( $projectId, $location, uniqid() ),
                'http_target' => new HttpTarget([ 'uri' => 'https://' . $location . '-' . $projectId . '.cloudfunctions.net/' . $function ]),
                'schedule' => $interval . ' * * * *' ]);

            $response = $cloudSchedulerClient->createJob($parent, $job);
        
        } finally {
            $cloudSchedulerClient->close();
        }  

    }





}