<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Events\newCronEvent;
use App\Events\newCronJobEvent;
use App\Events\TaskCreate;
use Log;
use Config; 
use App\auths_user_platform;
use Auth;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Abraham\TwitterOAuth\TwitterOAuth;

class PlatformsCallback extends Controller
{
  
    public function callbackFacebook(Request $request, $app_id, $app_secret){
/**
        $ip = "37.187.95.206";
        $port = "3306";
        $db = "app_thesoci_9c37";
        $user = "appthesoci9c37";
        $pass = "5efc1eed04037ae2";

        $dbConn = new \mysqli($ip, $user, $pass, $db);

        if ($mysqli->connect_errno) {
            echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        }
**/
$platformname='FACEBOOK';
        try{
          $metadata=[];
            $fb = new \Facebook\Facebook(['app_id' => $app_id, 'app_secret' => $app_secret, 'default_graph_version' => 'v4.0']);
            $helper = $fb->getRedirectLoginHelper();
            
            $permissions = ['email', 'leads_retrieval',  'read_insights', 'ads_management', 'instagram_manage_insights'];
            $loginUrl = $helper->getLoginUrl('https://pre.adsconcierge.com/api/callback/facebook', $permissions);
            
            try {
                $accessToken = $helper->getAccessToken();
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            
            if (!isset($accessToken)) {
                if ($helper->getError()) {
                    //   header('HTTP/1.0 401 Unauthorized');
                    echo "Error: " . $helper->getError() . "\n";
                    echo "Error Code: " . $helper->getErrorCode() . "\n";
                    echo "Error Reason: " . $helper->getErrorReason() . "\n";
                    echo "Error Description: " . $helper->getErrorDescription() . "\n";
                  exit;
                } else {
                    //   header('HTTP/1.0 400 Bad Request');
                  //  header('Location: ' . $loginUrl);
                    echo '<a href="' . htmlspecialchars($loginUrl) . '" target="_blank">Log in with Facebook!</a>';
                  exit;
                }
                exit;
            }
            $metadata['accessToken']= $accessToken ;
            $accessToken_value = $accessToken->getValue();
            // The OAuth 2.0 client handler helps us manage access tokens
            $oAuth2Client = $fb->getOAuth2Client();                
            // Get the access token metadata from /debug_token
            $tokenMetadata = $oAuth2Client->debugToken($accessToken);
            $metadata['debugToken']= $tokenMetadata ;
            // Validation (these will throw FacebookSDKException's when they fail)
            $tokenMetadata->validateAppId($app_id); // Replace {app-id} with your app id
            // If you know the user ID this access token belongs to, you can validate it here
            //$tokenMetadata->validateUserId('123');
            $tokenMetadata->validateExpiration();
              
            if (!$accessToken->isLongLived()) {
                // Exchanges a short-lived access token for a long-lived one
                try {
                    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
                    exit;
                }
            }
            $metadata['isLongLived']= $accessToken->isLongLived() ;
            $platform_user_id = $tokenMetadata->getUserId();

            //if ($resultado = $dbconn->query("SELECT id FROM users WHERE hashuser ='".$_COOKIE["id_user"]."'")) {
            //    $resultado->close();
            //}

            $userid = Auth::user()->id;
            $accessToken_value = $accessToken->getValue();
         //   $retorno = print_r($tokenMetadata, true);
            $email = json_decode(file_get_contents("https://graph.facebook.com/v7.0/me?fields=email&access_token=" . $accessToken_value))->email;
            
         /**   
            $stmt = $dbConn->prepare("INSERT INTO `app_thesoci_9c37`.`auths_user_platform` (`user_id`, `platform`, `app_id`, `app_secret`,`platform_user_id`, `platform_email`, `access_token`, `retorno`, `expiretime`) VALUES (?,?,?,?,?,?,?,?,?)  ON DUPLICATE KEY UPDATE activa = 'Y', `access_token`= ?, `platform_user_id`=?, `platform_email`=?, `retorno`= ?, `expiretime`= ? ");                
            $stmt->bind_param("ssssssssssssss", ...[    $userid, 
                                                        $platformid_facebook, 
                                                        $app_id, 
                                                        $app_secret, 
                                                        $platform_user_id, 
                                                        $email, 
                                                        $accessToken_value, 
                                                        $retorno, 
                                                        time() + (3600 * 59), 
                                                        $accessToken_value, 
                                                        $platform_user_id, 
                                                        $email, 
                                                        $retorno, 
                                                        time() + (3600 * 59) ]);
            
            $stmt->execute();
            **/
            $data = DB::table('auths_user_platform')->upsert(
                [ 'user_id' => $userid, 'platform' => 'FACEBOOK', 'app_id' => $app_id,
                'app_secret' => $app_secret, 'platform_user_id' => $platform_user_id, 'platform_email' => $email,
                'access_token' => $accessToken_value, 'retorno' => print_r($metadata, true), 'expiretime' => time() + (3600 * 59) ],
                ['user_id', 'platform', 'app_id', 'platform_user_id'], // key on duplicate ..
                [ 'access_token', 'platform_user_id', 'platform_email', 'retorno', 'expiretime'] // if key, update this fields
            );

            $lastinsertID  = DB::getPDO()->lastInsertId();
        
            // Obtenemos adsaccounts 
            //exec('php /home/app.thesocialaudience.com/public_html/www/crons/auth_account_update.php ' . $stmt->insert_id);
            // aca viene lo nuevo // revisamos en bbdd que no tengamos ninguna task de este tipo para este usuario y la lanzamos en la nube
            $data = array('prefix'=> 'FACEBOOK'.$lastinsertID.'_'. $platform_user_id ,'platform' =>  'FACEBOOK', 'cola_trabajo' => 'facebook-sync' , 'auth_id' =>  $lastinsertID, 'subject_id' =>  $lastinsertID, 'type' => 'auth_callback', 'function' => 'function-facebook-api' );

        //  event(new TaskCreate($data));
                      $data = array(  'prefix'=>$platformname. $lastinsertID.'_'. $platform_user_id , 'subject_id' =>$lastinsertID, 
                'type' => 'retrieve_all', 
                'Execution_interval' => '12H',
                'function' => 'function-'.strtolower($platformname).'-api' 
            );

            event(new newCronJobEvent($data)); 



    
            $data = array('prefix'=> $platformname. $lastinsertID.'_'. $platform_user_id ,
                        'subject_id' => $lastinsertID,
                        'type' => 'retrieve_stats_all',
                        'Execution_interval' => '24H',
                        'function' => 'function-'.strtolower($platformname).'-api'
                    );

            event(new newCronJobEvent($data));
            
        }catch(Exception $e){
            Log::channel('ovhlogs')->critical('AUTH token facebook ERROR - FAILED' . serialize($e),  [ '_context' => 'auth_renew', '_action' => 'auth_renew', 'short_message' => "FB error" ] );
        }


    }
  public function callbackTwitter(Request $request, $app_id, $app_secret){
 
$TWITTER_CONSUMER_KEY = 'iy5pXGlxt4ru0GPYE81WXbdtT'; //(API key)
$TWITTER_CONSUMER_SECRET = 'WriLQUz8DnUOJm2oeUnaQzzK2gCdLO1d4oPHXeS413k3aJpxri'; //(API secret key)
if (empty($app_id )) {$app_id= $TWITTER_CONSUMER_KEY; }
if (empty($app_secret )) {$app_secret= $TWITTER_CONSUMER_SECRET; }
$TWITTER_ACCESS_TOKEN = '327004085-gnVzt31NzsA6pMjus8ck1GACZfYbe0IQGv70aCJl'; // (Access token)
$tWWITER_ACCESS_TOKEN_SECRET = 'gHhmKXtosBLPr9kwY3RGcaT32bt4cdVwlHGa6FHNaQAxZ'; // (Access token secret)

 
$twitter_redirect_uri = 'https://pre.adsconcierge.com/callback/twitter/';
define('OAUTH_CALLBACK', $twitter_redirect_uri);
$twitter_scope = 'write,read'; //(Access level)
$platformid = 3;

$platformname='TWITTER';
    
 
$userid =  Auth::user()->id;
   
if(!isset($_GET['oauth_verifier'])){


$twconnection = new TwitterOAuth($app_id, $app_secret);
$request_token = $twconnection->oauth('oauth/request_token', array('oauth_callback' => $twitter_redirect_uri));
    // $_SESSION['oauth_token']=  $request_token['oauth_token'];
   // $_SESSION['oauth_token_secret']=  $request_token['oauth_token_secret'] ;
 
    Session::put('tw_oauth_token',$request_token['oauth_token']); 
    Session::put('tw_oauth_token_secret',  $request_token['oauth_token_secret']); 
   Session::save();
//    print_r($request_token ); 
 //     print_r($request->session()->all() ); 
 
    $url = $twconnection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
// header('_Location: '.$url);
 return Redirect::away($url);
//return redirect()->away('https://www.google.com');
    // exit;
}
else{
  //print_r($_SESSION);
  $oauth_verifier = $_GET['oauth_verifier'];
$oauth_token = $_GET['oauth_token'];
    $request_token = [];
$request_token['oauth_token'] = session('tw_oauth_token') ;
$request_token['oauth_token_secret'] = session('tw_oauth_token_secret');
 //     print_r($request->session()->all() ); 
 
  
        $twconnection = new TwitterOAuth($TWITTER_CONSUMER_KEY, $TWITTER_CONSUMER_SECRET, $request_token['oauth_token'], $request_token['oauth_token_secret']);
   $response = $twconnection->oauth("oauth/access_token", ["oauth_verifier" => $_REQUEST['oauth_verifier']]);
  
$tokenMetadata= json_encode($response);
$settingstxt= json_encode(array( 'consumer_key' => $app_id,       'consumer_secret' => $app_secret ));
  $platform_user_id = $response['user_id'];
  $email = $response['screen_name'];
  $accessToken_value = $response['oauth_token_secret'];
  //$expiretime= 
  //$userid = Auth::user()->id;
  $data = DB::table('auths_user_platform')->upsert(
                [ 'user_id' => $userid, 'platform' => $platformname, 'app_id' => $app_id,
                'app_secret' => $app_secret, 'platform_user_id' => $platform_user_id, 'platform_email' => $email,
                'access_token' => $accessToken_value, 'retorno' => print_r($tokenMetadata, true), 'expiretime' => time() + (3600 * 59) ],
                ['user_id', 'platform', 'app_id', 'platform_user_id'], // key on duplicate ..
                [ 'access_token', 'platform_user_id', 'platform_email', 'retorno', 'expiretime'] // if key, update this fields
            );

            $lastinsertID  = DB::getPDO()->lastInsertId();

   $data = array('prefix'=> $platformname.$lastinsertID.'_'. $platform_user_id ,'platform' => $platformname  , 'cola_trabajo' => 'twitter-sync' , 'auth_id' =>  $lastinsertID, 'subject_id' =>  $lastinsertID, 'type' => 'auth_callback', 'function' => 'function-twitter-api' );
return redirect('/connections/success');
          event(new TaskCreate($data));
 
                      $data = array(  'prefix'=>$platformname. $lastinsertID.'_'. $platform_user_id , 'subject_id' =>$lastinsertID, 
                'type' => 'retrieve_all', 
                'Execution_interval' => '12H',
                'function' => 'function-'.strtolower($platformname).'-api' 
            );

            event(new newCronJobEvent($data)); 



    
            $data = array('prefix'=> $platformname. $lastinsertID.'_'. $platform_user_id ,
                        'subject_id' => $lastinsertID,
                        'type' => 'retrieve_stats_all',
                        'Execution_interval' => '24H',
                        'function' => 'function-'.strtolower($platformname).'-api'
                    );

            event(new newCronJobEvent($data));
  
return redirect('/connections/success');

}    
    
    
  }
  public function callbackLinkedin(Request $request, $app_id, $app_secret){
 $client_id = '86a00km0hzlzo6';
$client_secret = 'TrkXfPzg1fMx2S86';
$linkedin_redirect_uri = 'https://pre.adsconcierge.com/api/callback/linkedin/';
$scope = 'r_liteprofile r_emailaddress w_member_social rw_ads r_ads_reporting';
$scope = 'r_liteprofile r_emailaddress rw_ads r_ads_reporting r_organization_social w_member_social w_organization_social r_basicprofile rw_organization_admin r_1st_connections_size';
//rw_ads r_ads_reporting r_ads_reporting rw_organization_admin rw_dmp_segments    r_organization_lookup 
$platformname = 'LINKEDIN';
 
$platformid = 4;

 
    



$userid =  Auth::user()->id;
   
 

//$access_token = 'AQU4KJ_5cYwHWCG0VAJvP7xMWpGeFKXnEmZQ8bifIgfGb1XvC86aiyAkkv6NxxS8v5qFUp2521XY0dJHnRhJ7f9_fbdcbE8PcxSzSvfoReo1rUsBt8UJzvGJIJk3xjYoHdx2xuXHk7B8Yah1WG4sokooLYu_MEkGAPlJqLbGvCV013dfWeQTu8CjuLDUy02zxBcjDfm0n97wnqLopOrvKH7ffkuGOb0ap7Q2GtDjU9rnaibPciW0GsLI08btR7ilvJN_IjHiH7dPIpCKUzcNVLD61XDf4QcOSLZd0DiPDADTWfYP-w8XNZnKE-n4Hvb6j4TNlR0upT6Fsw6nU62Rvsky_rCvrw';
//{"access_token":"AQU4KJ_5cYwHWCG0VAJvP7xMWpGeFKXnEmZQ8bifIgfGb1XvC86aiyAkkv6NxxS8v5qFUp2521XY0dJHnRhJ7f9_fbdcbE8PcxSzSvfoReo1rUsBt8UJzvGJIJk3xjYoHdx2xuXHk7B8Yah1WG4sokooLYu_MEkGAPlJqLbGvCV013dfWeQTu8CjuLDUy02zxBcjDfm0n97wnqLopOrvKH7ffkuGOb0ap7Q2GtDjU9rnaibPciW0GsLI08btR7ilvJN_IjHiH7dPIpCKUzcNVLD61XDf4QcOSLZd0DiPDADTWfYP-w8XNZnKE-n4Hvb6j4TNlR0upT6Fsw6nU62Rvsky_rCvrw","expires_in":5183999}
if($request->has('code')){
    //TODO
  
$code =  $request->query('code');
    $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.linkedin.com/oauth/v2/accessToken",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=authorization_code&code=".$code."&redirect_uri=".$linkedin_redirect_uri."&client_id=".$client_id."&client_secret=".$client_secret."&prueba=id",
            CURLOPT_HTTPHEADER => array('Content-Type: application/x-www-form-urlencoded'),
        ));

        $tokenMetadata = json_decode(curl_exec($curl));
 
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "Error:" . $err;
     
        } else {
            if(isset($tokenMetadata->error)){
                //   header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $tokenMetadata->error . "\n";
                echo "Error Description: " . $tokenMetadata->error_description . "\n";
             
            }else{
                $accessToken_value = $tokenMetadata->access_token;
                $refresh_token = $tokenMetadata->refresh_token;
            }
        }



}else if ($request->has('error')){
    //   header('HTTP/1.0 401 Unauthorized');
    echo "Error: " .  $request->query('error') . "\n";
    echo "Error Description: " .  $request->query('error_description') . "\n";
  
}else{
    $url = 'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id='.$client_id.'&redirect_uri='. urlencode( $linkedin_redirect_uri).'&scope='.$scope;
    echo '<a href="'.htmlspecialchars($url).'">login</a>';
    header('Location: '.$url);
}

    
if(isset($accessToken_value)){
    $curl = curl_init();
 
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.linkedin.com/v2/me",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "authorization: Bearer ".$accessToken_value,
            "cache-control: no-cache",
        ),
    ));

    $response = json_decode(curl_exec($curl));
  $platform_user_id=   $response->id;
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL".$err;
    } else {

        $email = json_decode(file_get_contents('https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))&oauth2_access_token='.$accessToken_value), true);

        $email = $email["elements"][0]["handle~"]["emailAddress"];

        //echo $email->elements->elements->handle~->emailAddress;

       $data = DB::table('auths_user_platform')->upsert(
                [ 'user_id' => $userid, 'platform' => $platformname, 'app_id' => $client_id,
                'app_secret' => $client_secret, 'platform_user_id' => $platform_user_id, 'platform_email' => $email,
                'access_token' => $accessToken_value,   'refresh_token' => $refresh_token, 'retorno' => print_r($tokenMetadata, true), 'expiretime' => time() + (3600 * 59) ],
                ['user_id', 'platform', 'app_id', 'platform_user_id'], // key on duplicate ..
                [ 'access_token', 'platform_user_id', 'platform_email', 'retorno','refresh_token', 'expiretime'] // if key, update this fields
            );

            $lastinsertID  = DB::getPDO()->lastInsertId();
    
   $data = array('prefix'=> $platformname.$lastinsertID.'_'. $platform_user_id ,'platform' => $platformname  , 'cola_trabajo' => 'twitter-sync' , 'auth_id' =>  $lastinsertID, 'subject_id' =>  $lastinsertID, 'type' => 'auth_callback', 'function' => 'function-twitter-api' );

          event(new TaskCreate($data));
 
                      $data = array(  'prefix'=>$platformname. $lastinsertID.'_'. $platform_user_id , 'subject_id' =>$lastinsertID, 
                'type' => 'retrieve_all', 
                'Execution_interval' => '12H',
                'function' => 'function-'.strtolower($platformname).'-api' 
            );

            event(new newCronJobEvent($data)); 



    
            $data = array('prefix'=> $platformname. $lastinsertID.'_'. $platform_user_id ,
                        'subject_id' => $lastinsertID,
                        'type' => 'retrieve_stats_all',
                        'Execution_interval' => '24H',
                        'function' => 'function-'.strtolower($platformname).'-api'
                    );

            event(new newCronJobEvent($data));
 

      
        //crear metodo en general que llame traiga todos los items de la cuenta
    }
    
}
    
    
     
    
  
  }
  
  
 public function callbackSocialApi(Request $request, $platform){


        if(!session_id()) {
            session_start();
        }

        $redirect_uri = 'https://app.thesocialaudience.com/apis/jr_auth_fb.php';
        $user = Auth::user();
        $data = $request->all();
        
        // buscamos credenciales app login del user y la platform
        //$auth = DB::table('auths_user_platform')->where('platform', $platform)->where('user_id', $user->id)->first();

        // cuenta de kepesaoeldburgos@hotmail.com
         $auth['app_id'] = '2230398120518804';
        $auth['app_secret'] = '7-fdb89a242acff2678080d01b80bbba5';
        $auth['app_secret'] = '626ea4f683f47ba22a89cc84471a2984';
        
        switch ($platform) {    
            case 'facebook':
                $this->callbackFacebook( $request, $auth['app_id'], $auth['app_secret']);
                break;

            case 'twitter':

            
            return $this->callbackTwitter($request,null,null );
                break;
            
            case 'linkedin':
            
                $this->callbackLinkedin($request,null,null);
                break;            
        }
        
     //   return redirect('/connections/success');

    }



}
