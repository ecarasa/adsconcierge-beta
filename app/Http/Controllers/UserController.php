<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\User;
use App\Role_User;
use App\Role;
use App\Office;
use Auth;

use \Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request){
 
        $validator = Validator::make($request->all(), [
            'name'  => 'required|string|max:255',
            'email' => 'required|email|min:3|max:50|string|unique:users',
            'rol'   => 'required',
    
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $user = User::create([
                                'email'     =>  $request->get('email'),
                                'name'      =>  $request->get('name'),
                                'password'  => bcrypt(  'secret' ),
                            ]);
        if( !is_null( $request->get('rol') ) ){
                $user->roles()->attach( $request->get('rol') );
        }
        $user->hashuser = md5($user->id);
        
        $user->save();
        return redirect()->route('userlist')->with('success','User successfully created');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request){
        

        $validator = Validator::make($request->all(), [
            'name'  => 'required|string|max:255',
            'email' => 'required|email|min:3|max:50|string',
            'email' => Rule::unique('users')->ignore($request->get('id'), "public_id"),  
        ]);
        
        if($validator->fails())
        {	
            return redirect()->back()->withErrors($validator)->withInput();
        }
        //dd( $request->get('id') );die();
        $user = User::where('public_id', $request->get('id') )->first();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        
        if( !is_null( $request->get('rol') ) ){
            $user->roles()->detach();
            $user->roles()->attach( $request->get('rol') );
        }
        if(!is_null($request->get('password'))){
            $user->password = bcrypt($request->get('password'));
        }
        
        $user->save();
        return redirect()->route('userlist')->with('success','User successfully update');
    }     
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function delete(string $id, Request $request){
      
		$user = User::where('public_id', $id )->first();
        $name = $user->name;
		
		$param = array (
            "end_of_term" => "true"
        );  
        $ch = curl_init();
		
        curl_setopt($ch, CURLOPT_URL,"https://adsconcierge.chargebee.com/api/v2/subscriptions/".md5($user->email)."/cancel");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "live_UhGIHjzvcdOWTwEU28Rt1Mk0khGjVGk40");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));
                    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
		$server_output = json_decode($server_output);
		//print_r($server_output->subscription->cancelled_at);
		
        curl_close ($ch);
		
		$user->deleted_at = date("Y-m-d H:i:s", $server_output->subscription->cancelled_at);
		$user->save();
		
		return redirect()->route('userlist')->with('success','User "'.$name.'" delete');
		
    }  

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function new(Request $request)
    { 
        if(Auth::user()->damepermisos('crear_usuarios') != 1){
            return redirect()->route('mysubscription');
        }
        //$request->user()->authorizeRoles(['account_manager', 'admin_group', 'superadmin']);
        $roles = Role::where('name' , 'not like' , 'superadmin')->get();
        $offices = Office::all();
        return view('template.usuarios', [ 'title' => 'New User', 'user' => null, 'roles' => $roles, 'offices' => $offices, 'formaction' => '/users/create' ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(string $id, Request $request)
    { 
        $user = User::where('public_id', $id )->first();
        //$request->user()->authorizeRoles(['account_manager', 'admin_group', 'superadmin']);
        $roles = Role::where('name' , 'not like' , 'superadmin')->get();
        $offices = Office::all();
        $role_user= Role_User::join('users', 'users.id','=','role_user.user_id')->where('users.id','=',auth()->user()->id)->first();
        return view('template.usuarios', [ 'title' => 'Edit user | '.$user->name, 'user' => $user, 'roles' => $roles,'role_user' => $role_user, 'offices' => $offices, 'formaction' => '/users/update' ]);
    }
     
	
	public function perfil(Request $request)
    { 
		$user = Auth::user();
        //$request->user()->authorizeRoles(['account_manager', 'admin_group', 'superadmin']);
        //$roles = Role::where('name' , 'not like' , 'superadmin')->get();
        //$offices = Office::all();
        return view('template.perfil', [ 'title' => 'My perfil', 'user' => $user, 'formaction' => '/update_perfil' ]);
    }
    
    public function update_perfil(Request $request){

        $validator = Validator::make($request->all(), [
            'name'  => 'required|string|max:255', 
        ]);
        
		if(!is_null($request->get('password')) && $request->get('password') != ""){
			if(($request->get('password') != "") || ($request->get('password_confirmation' != ""))){
				$validator = Validator::make($request->all(), [
					'name'  => 'required|string|max:255', 
					'password'  => 'required|string|same:password_confirmation|max:20'
				]);
			}
		}
		
        if($validator->fails())
        {
            return redirect('perfil')->withErrors($validator)->withInput();
        }
		
		
        $user = User::where('public_id', Auth::user()->public_id )->first();
        $user->name = $request->get('name');
        
        if(!is_null($request->get('password')) && $request->get('password') != ""){
			if($request->get('password') ==  $request->get('password_confirmation')){
            	$user->password = bcrypt($request->get('password'));
			}
        }
        
        $user->save();
        return redirect()->route('perfil')->with('success','Your profile has been modified successfully');
    }     
	
	public function delete_perfil(){
		
		$user = Auth::user();
		
		$param = array (
            "end_of_term" => "true"
        );  
        $ch = curl_init();
		
        curl_setopt($ch, CURLOPT_URL,"https://adsconcierge.chargebee.com/api/v2/subscriptions/".md5($user->email)."/cancel");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "live_UhGIHjzvcdOWTwEU28Rt1Mk0khGjVGk40");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));
                    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
		$server_output = json_decode($server_output);
		//print_r($server_output->subscription->cancelled_at);
		
        curl_close ($ch);
		
		$user->deleted_at = date("Y-m-d H:i:s", $server_output->subscription->cancelled_at);
		$user->save();
		
		return redirect()->route('perfil');
	}
}
