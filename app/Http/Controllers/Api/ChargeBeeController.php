<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Appsettings;

class ChargeBeeController extends Controller
{

    public function handleRequest(Request $request)
    {

        try {
            $content = json_encode($request->json()->all());
 
    
            $event_type = $request->json()->get('event_type');
 
            DB::connection('mysql')->table('userevents_ext')->insert(['origen'=>'CHARGEBEE', 'messagereceived'=> $content, 'event_type'=> $event_type]);

            // posibles events a los que debemos actuar desde chargebee_data
            $eventsCatch = [  'subscription_paused', 'subscription_changed', 'subscription_renewed','subscription_reactivated'];
            $planeventsCatch = ['plan_updated', 'plan_created' ];  // 'plan_deleted',
         
            if (in_array($event_type, $planeventsCatch)) {
              $permisos='{}';
               $item = $request->json()->get('content')['plan'] ;
 
              if (isset($item['meta_data'])  ) {
                
                 $permisos = json_encode($item['meta_data']);
              }
                
                Appsettings::updateOrCreate([   'id' => md5( 'plan:' . $item['id']  ), 'keyname' => 'plan:' . $item['id'], 'value' => $permisos ]);
            
              }
                
 
         
          
                if (in_array($event_type, $eventsCatch)) {
              $permisos='{}';
                  $item = $request->json()->get('content')['subscription'] ;
             
                
               // $permisos = json_encode($content->content->subscription->meta_data);
             //   Users::update([   'id' => md5( 'plan:' . $content->content->subscription->plan_id  ), 'keyname' => 'plan:' . $content->content->subscription->plan_id, 'value' => $permisos ]);
            DB::table('users')
            ->where( 'subscriptionid', $item['id']  )
            ->update(['plan' =>  $item['plan_id'], 'status' =>  $item['status']  ]);
             
                
 
            }

          
          

            $eventsCatch_cronjobsPause = [  'subscription_paused', 'subscription_cancelled'];
            
            if (in_array($event_type, $eventsCatch_cronjobsPause)) {
                // pausamos los crons jobs, asociados al usuario
            }



            $eventsCatch_cronjobsActivate = ['subscription_reactivated', 'subscription_renewed'];
            if (in_array($event_type, $eventsCatch_cronjobsPause)) {
                // activamos los cronjobs asociados al usuario
            }


 

        }catch(\Exception $e){
            //Devolviendo este error, chargebee volvera a intentar el llamado al webhook hasta 7 veces
          echo 'Excepción capturada2: ',  $e->getMessage(), "\n";
        
          //  abort(400);
        }

        return response(200);
    }

}
