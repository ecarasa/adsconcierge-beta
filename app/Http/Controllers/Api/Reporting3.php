<?php


namespace App\Http\Controllers\Api;


use App\ChartDataParser;
use App\Dashboard;
use App\DatePeriod;
use App\MetricParser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Symfony\Component\HttpFoundation\JsonResponse;

class Reporting3
{

    public function data(Request $request)
    {
        $filters = $request->only([
            'customers',
            'platforms',
            'campaigns',
            'period',
            'metrics',
            'type',
            'groupBy'
        ]);

        if (false === isset($filters['type'])) {
            throw new \Exception('Type is mandatory');
        }
        $type = $filters['type'];

        $fields = ['dia', 'campananame'];
        if (true === isset($filters['metrics'])) {
            $metrics = $filters['metrics'];
            $fields = array_merge($fields, MetricParser::parse($metrics));
        } else {
            throw new \Exception('At least one metric is mandatory');
        }

        $queryBuilder = DB::connection('stats')
            ->table('platform_campana_day')
            ->select($fields);

        if (isset($filters['customers']) && count($filters['customers']) > 0) {
            $queryBuilder->whereIn('customer_id', $filters['customers']);
        }
        if (isset($filters['platforms']) && count($filters['platforms']) > 0) {
            $queryBuilder->whereIn('platformid', $filters['platforms']);
        }
        if (isset($filters['campaigns']) && count($filters['campaigns']) > 0) {
            $queryBuilder->whereIn('campanaroot', $filters['campaigns']);
        }
        
        $groupBy = isset($filters['groupBy']) ? $filters['groupBy'] : 'dia';
        
        $queryBuilder->groupBy([$filters['groupBy'] ?? 'dia']);

        if (false === isset($filters['period'])) {
            throw new \Exception('Period is mandatory');
        }

        list($startDate, $endDate) = DatePeriod::fromString($filters['period']);

        $queryBuilder->where('dia', '>=', $startDate->format(DatePeriod::DATE_FORMAT));
        $queryBuilder->where('dia', '<=', $endDate->format(DatePeriod::DATE_FORMAT));

        $data = $queryBuilder->get()->all();

        return ChartDataParser::convert($type, $data, $metrics, $startDate, $endDate, $groupBy);
    }

    public function save(Request $request)
    {

        // si viene id, actualizamos
         if (isset($request->id)) {
            
            $reporteExistente = Dashboard::where('public_id', '=', $request->id)->first();
            
            if($reporteExistente){
                $configAnt = json_decode($reporteExistente->configuration);
                $dataConfigNew = array("default"=> (boolean) $configAnt->default, "charts"=>json_decode($request->charts));
                
                if(Dashboard::where('public_id', '=', $request->id)->update(['configuration' => json_encode($dataConfigNew)])){
                    return ['success'=>true, 'msj' => "Report updated successfully" ];
                }else{
                    return ['success'=>false, 'msj' => "Something fail while updating." ];
                }

            }
        } 
        





        // checkName Duplicity
        $reportExists = Dashboard::where('name','=',$request->name)->get();

        if(count($reportExists)>0){
            return ['success'=>false, 'msj' => "Duplicate 'NAME' for new report, set another value and try again." ];
        }

        if ($request->default == true){
            // deberiamos eliminar el anterior default
            $dashboardsList = Dashboard::where('type','reporting3')->get();

            foreach($dashboardsList as $dash){
                $config = json_decode($dash->configuration);
                $config->default = false;
                $dash->configuration = json_encode($config);
                $dash->save();
            }

        }

        $dataConfig = array("default"=> (boolean) $request->default, "charts"=>json_decode($request->charts));

        $item = new Dashboard();
        $item->name = $request->name;
        $item->user_id = Auth::user()->id;
        $item->configuration = json_encode($dataConfig);
        $item->type = 'reporting3';     

        if ($item->save()) {
            return ['success'=>true, 'id' => $item->id, 'name' => $item->name, 'data' => $item->configuration, 'msj' => "Report saved succesfully." ];
        }else{
            return ['success'=>false, 'msj' => "Error while saving report. Try again." ];
        }

    }

    public function config(Request $request)
    {

        $id = $request->get('id');

        $report = DB::table('dashboards')->where(
            'public_id',
            '=',
            $id
        )->first();

        return JsonResponse::create(json_decode($report->configuration, true));
    }
}
