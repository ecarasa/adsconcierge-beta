<?php

namespace App\Http\Controllers\API;

use App\Customer;
use App\Http\Controllers\Controller;
use App\settings_matrix;
use Auth;
use DateTime;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\TargetingFields;
use FacebookAds\Object\Search\TargetingSearchTypes;
use FacebookAds\Object\Targeting;
use FacebookAds\Object\TargetingSearch;
use FacebookAds\Object\User;
use FacebookAds\Object\Values\AdSetBillingEventValues;
use FacebookAds\Object\Values\AdSetOptimizationGoalValues;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Hborras\TwitterAdsSDK\TwitterAds;
use Hborras\TwitterAdsSDK\TwitterAds\Account;
use App\auths_user_platform;
use Illuminate\Support\Facades\Storage;
use Log;
use Config; 


use Illuminate\Support\Facades\Validator;

use App\Events\newCronEvent;
use App\Events\newCronJobEvent;
use App\Events\TaskCreate;

class FunctionsController extends Controller
{
    
    
    public $access_token = 'EAAan9RpA1s8BAAIrp6LcruNNsAymtZA1WAt0zjYoNfUqf56QTY8FXGPxtZA1KRGgqhgrQZCxqhb185FwpFbpVam6py3Bw6dN60US1f4MVOQWWQl4UudtEZC47l4ZBVER4hy0j2NZAB1JUHdRzVrRS30dfvAW3SmiuuxLYPxGOZBRsmlswbVoBl7';
    public $app_secret = '595fe3f68443cc4b991d4daa814161d3';
    public $app_id = '1873521009546959';
    
    // public $id = '2266570523613927';
    //public $id_businesses = '1378199772451011';
    //public $id_campaign = '6124145398262';
    // public $url = 'https://4b6707ea.ngrok.io/api/token/facebook';

    private $customersList = null;



    
    public function log_externo(Request $request){

        $data = $request->all();
        //print_r(json_encode($data));
        Log::channel('ovhlogs')->critical(json_encode($data),  [ '_context' => 'external_log', '_action' => 'get_stats_campagins', 'short_message' => "Log callbacks apis." ] );
        
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $api = Api::init($this->app_id, $this->app_secret, $this->access_token);
        $api->setLogger(new CurlLogger());

        $fields = array(
        );
        $params = array(
        );
        echo "<pre>";
        echo json_encode((new User($this->id))->getBusinesses(
            $fields,
            $params
        )->getResponse()->getContent(), JSON_PRETTY_PRINT);
        echo "</pre>";
    }

    public function post_upload_image(Request $request)
    {

        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'fileUpload' => 'required|mimes:jpeg,jpg,png|max:2048'
        ]);
         
        if ($validator->fails()) {
             abort(400);
        }

        $path = storage_path(env("MEDIAFOLDER", "/uploads/mediafolder"). '/'. $user->public_id.'/img');
        $pathRoot = storage_path();
        //dd($path);

        header('Content-Type: application/json');

        if (!file_exists($path)) {echo 'creamos dir';
            mkdir($path, 0777, true);
        }

        //$arrFilesinputs = $request->input('fileInputTags');
        $file = $request->file('fileUpload') ;
        $imagenesGuardadas = array();
        $dimm = getimagesize($file);
        
        if ( $dimm[0] >= 1200 && $dimm[1] >= 628 ){

            $name = md5( $user->public_id . microtime() . trim($file->getClientOriginalName()) ) .'.'. $file->extension();
            $nameWaterMark = md5( $user->public_id . microtime() . trim($file->getClientOriginalName()) ) .'-marked.'. $file->extension();
            $subido = $file->move($path, $name);

            if (!Auth::user()->getRolePermission('watermark')){
                $this->addImageWatermark( $path . '/' . $name, $pathRoot . '/watermark/ads-watermark-pb.png', $path . '/' . $nameWaterMark, 40 );
            }else{
                //$watermarkPath = null;
            }

            $img = array (  
                            'path' => $path . '/' . $name, 
                            'path_watermarked'=> $nameWaterMark, 
                            'original_name' => $file->getClientOriginalName(),  
                            'status'=> true,  
                            'tags'=> null 
                        );

        }else{
            $img = array ( 'original_name' => $file->getClientOriginalName(), 'status'=> false, 'msj'=> "Image is less than 1200x638px" );
        }
        
        array_push($imagenesGuardadas, $img);
        
        return json_encode($imagenesGuardadas);
    }


    private function  addImageWatermark($SourceFile, $WaterMark, $DestinationFile=NULL, $opacity) {
        $main_img = $SourceFile; 
        $watermark_img = $WaterMark; 
        $padding = 5; 
        $opacity = $opacity;
        
        // creamos watermark
        $watermark = imagecreatefrompng($watermark_img); 
        $image = imagecreatefromjpeg($main_img); 
        
        if(!$image || !$watermark) die("Error: main image or watermark image could not be loaded!");

        $watermark_size = getimagesize($watermark_img);
        
        $watermark_width = $watermark_size[0]; 
        $watermark_height = $watermark_size[1]; 
        
        $image_size = getimagesize($main_img); 
        
        $dest_x = $image_size[0] / 2; 
        $dest_y = $image_size[1] / 2;

        imagecopymerge($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, $opacity);
        
        if ($DestinationFile <> '') {

            imagejpeg($image, $DestinationFile, 100); 
            return $DestinationFile;

        }
        
        imagedestroy($image); 
        imagedestroy($watermark); 
    }




    public function post_upload_video(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'fileUpload' => 'required|mimes:gif,mp4|max:2048'
        ]);
         
        if ($validator->fails()) {
             abort(400);
        }

        $user = Auth::user();

        $path = storage_path(env("MEDIAFOLDER", "tmp/uploads"). '/'. $user->public_id.'/vid');

        header('Content-Type: application/json');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('fileUpload') ;

       // dd($file);

        $videosUploaded = array();
        
        $name = md5( $user->public_id . microtime() . trim($file->getClientOriginalName())).'.'. $file->extension();
        $file->move($path, $name);
        $vid = array ( 'path' => $path.'/'.$name, 'original_name' => $file->getClientOriginalName(),  'status'=> true, 'tags'=> null );
    
        
        array_push($videosUploaded, $vid);
        
        return json_encode($videosUploaded);
    }




     /*   formData.append("howPromoted", $('input[name=rad__promote]:checked').val()); // 1 al 2
        formData.append("typePromoted", $('input[name=aPromoted]:checked').val()); // 1 al 3
        formData.append("postPromoted",*/
    public function getHowPromote($val){
        switch ($val) {
            case 1:
              return "SPECIFIC_POST";
              break;
            case 2:
                return "LATEST_POST";
              break;
          }
    }


    public function getTypeOfPromote($val){
        switch ($val) {
            case 1:
                return "RSS";
                break;
            case 2:
                return "G_ANALYTICS";
                break;
            case 3:
                return "LATEST_POST";
                break;
          }
    }

    public function getPostPromote($val){
        switch ($val) {
            case 1:
                return "ALL";
                break;
            case 2:
                return "LINK";
                break;
            case 3:
                return "STATUS";
                break;
            case 4:
                return "PHOTO";
                break;
            case 5:
                return "VIDEO";
                break;
          }
    }



    public function createCampaign(Request $request)
    {
        /*
            COLUMNAS DB TABLE CAMPAIGNS        
            
            id	int
            name	varchar(100)
            user_id	int
            customer_id	int
            created_at	timestamp
            updated_at	timestamp
            start_date	timestamp
            end_date	timestamp
            budget	decimal(10,2)
            isdefault	char(1)
            budgetdistribution	char(3)
            currency	char(3)
            ctr	float
            status	varchar(50)
            cpc	float
            clicks	int
            impression	int
            spent	float
            conversion	int
            cpm	float
            cpa	float
            geo_include	longtext
            geo_exclude	longtext
            ads_accounts	longtext
            plataformas	longtext
            platforms_facebook	tinyint
            platforms_twitter	tinyint
            platforms_linkedin	tinyint
            platforms_snapchat	tinyint
            platforms_adwords	tinyint
            platforms_amazon	tinyint
            platforms_instagram	tinyint
            platforms_tiktok	tinyint
            platforms_pinterest	tinyint
            platforms_reddit	tinyint
            platform_placements	longtext
            genders	longtext
            ages	longtext
            languages	longtext
            platforms_properties_ids	longtext
            intereses_facebook	longtext
            intereses_twitter	longtext
            intereses_linkedin	longtext
            intereses_snapchat	longtext
            intereses_adwords	longtext
            intereses_amazon	longtext
            intereses_instagram	longtext
            intereses_tiktok	longtext
            intereses_pinterest	longtext
            intereses_reddit	longtext
            devices	longtext
            conectivity	longtext
            budgets_x_plataforma	longtext
            creativity_headlines	longtext
            creativity_img_ids	longtext
            creativity_adtexts	longtext
            creativity_callstoaction	longtext
            creativity_urls	longtext
            creativity_displaylinks	longtext
            creativity_linkdescription	json
            kpis	json
            dynamicpostsconditions	json
            public_id	varchar(256)
            budget_target_diary	int
            budget_target_diary_type	enum('CLICKS','IMPRESSIONS','SPEND','CONVERSIONS')
            budget_target_total	int
            budget_target_total_type	enum('CLICKS','IMPRESSIONS','SPEND','CONVERSIONS')
            run_only_on_schedule	json
            spend_on_each_ad	float
            impressions_per_day	float
            internal_holder	varchar(128)
            brand_name	varchar(128)
            target json
            pixels json
            custom_audiences json

         */

        /*
            -Validaciones
            -armado y validado de datos
            -Guardado en bbdd -> status: readytopublish
        */

        $user = Auth::user();

        $array['response'] = array();
        
        // Validamos plataformas permitidas

        if ( !Auth::user()->checkPlatformArr(json_decode($request->get('rrss_list'))) ){
            Log::channel('ovhlogs')->critical('Subscription Handler. User try to operate with a platform that is not allowed.',  [ '_context' => 'campaignWizard', '_action' => 'subscription_fail_for_campaign', 'short_message' => "Try to operate with a platform that is not allowed." ] );
            return redirect()->route('mysubscription');
        }

        if ((!Auth::user()->getRolePermission('cross_plataformas'))) {
            Log::channel('ovhlogs')->critical('Subscription Handler. User try to cross platforms and it is not allowed',  [ '_context' => 'campaignWizard', '_action' => 'subscription_fail_for_campaign', 'short_message' => "Try to cross platforms and it is not allowed." ] );
            return redirect()->route('mysubscription');
        }


        //validamos cantidad de campañas segun perfil de chargebee
        $campaigns = DB::table('campaigns')->where('user_id', $user->id)->get();
       
        if (count($campaigns) >= Auth::user()->getRolePermission('num_plataformas') ) {
            Log::channel('ovhlogs')->critical('Subscription Handler. User try to add more campaigns that allowes by current plan subscription.',  [ '_context' => 'campaignWizard', '_action' => 'subscription_fail_for_campaign', 'short_message' => "Subscription Handler. User try to add more campaigns that allowes by current plan subscription." ] );
            $array['response']['status'] = false;
            $array['response']['message'] = "You cannot create a new campaign, to continue upgrade plan <a href='/subscription'>here</a>.";
            return response()->json($array);
        }

        //DB::table('campa')->select('ads_accounts.public_id', 'ads_accounts.currency')->whereIn('ads_accounts.public_id', $cuentas)->get();
        //if (Auth::user()->getRolePermission('cross_plataformas') == ) {

       // if ($this->importe_maximo_mes($_POST['start_date'], $_POST['end_date'], $_POST["budget"], $_POST["budgetdistribution"])) {
       //     //return redirect()->route('mysubscription');
       //  }
       

        if (strtoupper($request->get('status')) == strtoupper('readytopublish')){

            $validator = Validator::make($request->all(), [  'campaign_name' => 'required',
                                                        'client' => 'required',
                                                        'location_in' => 'required',
                                                        'language' => 'required',
                                                        'start_date' => 'required',
                                                        'end_date' => 'required',
                                                        'budget_split' => 'required',
                                                        
                                                        'budget' => 'required',

                                                        'gender' => 'required',
                                                        'ages' => 'required',
                                                        'language' =>'required',
                                                        'conectivity' => 'required',
                                                        'devices' => 'required',

                                                        'run_only_on_schedule' => 'required',
                                                        'spend_on_each_ad' => 'required',
                                                        'impressions_per_day' => 'required',

                                                        'ads_accounts' => 'required',
                                                        'ads_accounts_properties' => 'required',
                                                        

                                                        'budget_target_diary' => 'required',
                                                        'budget_target_diary_type' => 'required',
                                                        'budget_target_total' => 'required',
                                                        'budget_target_total_type' => 'required',

                                                        // 'internal_holder' => 'required',
                                                        // 'brand_name' => 'required',
                    ]);
     
            if ($validator->fails()) {
                
                $array['response'] = false;
                $array['response']['message'] = $validator->messages();
                
                Log::channel('ovhlogs')->warning('Validator failed.'.json_encode( $validator->messages() ),  [ '_context' => 'campaignWizard', '_action' => 'mysql_insert', 'short_message' => json_decode( $validator->messages() ) ] );

                return response()->json($array);
            }

        }


        $customer = Customer::where('public_id', $request->get('client'))->first();
       

        //location process
        
        $location_in = array();
        $location_out = array();

        foreach (json_decode($request->get('rrss_list')) as $rrss){
            $location_in[$rrss] = array();
            $location_out[$rrss] = array();
            $custom_audience[$rrss] = array();
        }

        if ($request->has('location_in')){
            foreach($request->get('location_in') as $value) {
                $value = json_decode($value);
                array_push($location_in[$value->platform], $value);
            }
        }

        if ($request->has('location_out')){
            foreach($request->get('location_out') as $value) {
                $value = json_decode($value);
                array_push($location_out[$value->platform], $value);
            }
        }


        $languages = json_decode($request->get('languages'));


        $dt = DateTime::createFromFormat('d/m/Y', $request->get('start_date') != 'false' ? $request->get('end_date') : date('d/m/Y'));
        $start_time = $dt->format("Y-m-d H:i:s");
        $dt = DateTime::createFromFormat('d/m/Y', $request->get('end_date') != 'false' ? $request->get('end_date') : date('d/m/Y'));
        $end_time = $dt->format("Y-m-d H:i:s");
        

        $budget_by_platform = array();
        if ($request->has('budget_split')){
            foreach($request->get('budget_split') as $value) {
                $value = json_decode($value);
                $budget_by_platform[$value->platform]= $value;
            }
        }

        $call_to_actions = array();
        if ($request->has('call_actions')){
            foreach($request->get('call_actions') as $value) {
                $value = json_decode($value);
                array_push($call_to_actions, $value);
            }
        }

        $headline = array();
        if ($request->has('headlines')){
            foreach($request->get('headlines') as $value) {
                $value = json_decode($value);
                array_push($headline, $value);
            }
        }

        $urls = array();
        if ($request->has('urls')){
            foreach($request->get('urls') as $value) {
                $value = json_decode($value);
                array_push($urls, $value);
            }
        }

        $text_links = array();
        if ($request->has('text_links')){
            foreach($request->get('text_links') as $value) {
                $value = json_decode($value);
                array_push($text_links, $value);
            }
        }

        $ad_texts = array();
        if ($request->has('ad_texts')){
            foreach($request->get('ad_texts') as $value) {
                $value = json_decode($value);
                array_push($ad_texts, $value);
            }
        }
        
        /*     
        Autopost form json
        */
        
        $latestPost = false;

        if ($request->get('action_creas') == 2){

            $latestPost = true;
            $jsonLatestpost=[];
            
            $jsonLatestpost['howPromoted'] = $this->getHowPromote($request->get('howPromoted'));
            $jsonLatestpost['typePromoted'] = $this->getTypeOfPromote($request->get('howPromoted'));
            $jsonLatestpost['postPromoted'] = $this->getPostPromote($request->get('howPromoted'));

            $jsonLatestpost['promoteForValue'] = $request->get('promoteForValue');
            $jsonLatestpost['promoteForType'] = $request->get('promoteForType');

            $jsonLatestpost['t_postLink'] = $request->get('t_postLink');
            $jsonLatestpost['t_postText'] = $request->get('t_postText');
            
            if ($request->get('latestpost_filter_post') != null ){
                $jsonLatestpost['latestpost_filter_post'] =  $request->get('latestpost_filter_post');
            }

            if ($request->get('ga_filter_post') != null){
                $jsonLatestpost['ga_filter_post'] =  $request->get('ga_filter_post');
            }

        } 

        // custom audience formatting
        foreach (json_decode($request->get('rrss_list')) as $rrss){
            if ($request->has('audiencia_list_'.$rrss)){            
                foreach($request->get('audiencia_list_'.$rrss) as $value) {
                    $value = json_decode($value);
                    array_push($custom_audience[$rrss], $value);

                }
            }
        }

        // checkcurrency values from ads_account
        $cuentas = array();
        foreach (json_decode($request->get('ads_accounts')) as $publicid) {
            array_push($cuentas, $publicid);
        }


        $accounts = DB::table('ads_accounts')->select('ads_accounts.public_id', 'ads_accounts.currency')->whereIn('ads_accounts.public_id', $cuentas)->get();
        $currency = array();
        foreach ($accounts as $ad) { array_push($currency,$ad->currency); }
        $currency = array_unique($currency);
        if (count($currency)==1){
            $currency = $currency[0];
        }else{
            $currency = 'MIX';
        }


        $rowdata = [
            'name' => $request->get('campaign_name') ? $request->get('campaign_name') : null,
            'user_id' => $user->id,
            'customer_id' => $customer->id,
            
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),

            'start_date' => $start_time,
            'end_date' => $end_time,
            
            'status' => $request->get('status') ? strtoupper($request->get('status')) : 'READYTOPUBLISH',
            
            'budget' => $request->get('budget'),
            'budgetdistribution' => json_encode($budget_by_platform),
            
            'currency' =>  $currency,
            
            'geo_include' => json_encode($location_in),
            'geo_exclude' => json_encode($location_out),
            'plataformas' => $request->get('rrss_list'),

            'platforms_facebook' => null,
            'platforms_twitter' => null,
            'platforms_linkedin' => null,
            'platforms_snapchat' => null,
            'platforms_adwords' => null,
            'platforms_amazon' => null,
            'platforms_instagram' => null,
            'platforms_tiktok' => null,
            'platforms_pinterest' => null,
            'platforms_reddit' => null,
            
            'platform_placements' => $request->get('placement'),

            'cpc' => null, // costo per click
            'clicks' => null,
            'impression' => null,
            'spent' => null, 
            'conversion' => null, // conversion
            'cpm' => null, // costo x mil impresiones
            'cpa' => null, // costo por adquisision

            'genders' => $request->get('gender'),
            'ages' => $request->get('ages'),
            'languages' => $request->get('language'),
            'conectivity' => $request->get('conectivity'),
            'devices' => $request->get('devices'),


            'run_only_on_schedule' => $request->get('run_only_on_schedule'),
            'spend_on_each_ad' => $request->get('spend_on_each_ad'),
            'impressions_per_day' => $request->get('impressions_per_day'),

            'ads_accounts' => $request->get('ads_accounts'),
            'platforms_properties_ids' => $request->get('ads_accounts_properties'),
            
            //intereses
            'intereses_facebook' => $request->get('interest_list_facebook') ? json_encode($request->get('interest_list_facebook')) : null,
            'intereses_twitter' => $request->get('interest_list_twitter') ? json_encode($request->get('interest_list_twitter')) : null,
            'intereses_linkedin' => $request->get('interest_list_linkedin') ? json_encode($request->get('interest_list_linkedin')) : null,
            'intereses_snapchat' => $request->get('interest_list_snapchat') ? json_encode($request->get('interest_list_snapchat')) : null,
            'intereses_adwords' => $request->get('interest_list_adwords') ? json_encode($request->get('interest_list_adwords')) : null,
            'intereses_amazon' => $request->get('interest_list_amazon') ? json_encode($request->get('interest_list_amazon')) : null,
            'intereses_instagram' => $request->get('interest_list_instagram') ? json_encode($request->get('interest_list_instagram')) : null,
            'intereses_tiktok' => $request->get('interest_list_tiktok') ? json_encode($request->get('interest_list_tiktok')) : null,
            'intereses_pinterest' => $request->get('interest_list_pinterest') ? json_encode($request->get('interest_list_pinterest')) : null,
            'intereses_reddit' => $request->get('interest_list_reddit') ? json_encode($request->get('interest_list_reddit')) : null,

            'creativity_headlines' =>json_encode(  $headline),
            'creativity_img_ids' => $request->get('images_uploaded') ? $request->get('images_uploaded') : null,
            'creativity_adtexts' => json_encode( $ad_texts),
            'creativity_callstoaction' => json_encode( $call_to_actions),
            'creativity_urls' => json_encode( $urls),
            'creativity_displaylinks' => $request->get('link_display') ? $request->get('link_display') : null,
            'creativity_linkdescription' => json_encode( $text_links ),

            'autopost' => json_encode( $jsonLatestpost ),


            'budget_target_diary' => $request->get("budget_target_diary"),
            'budget_target_diary_type' => $request->get("budget_target_diary_type"),
            'budget_target_total' => $request->get("budget_target_total"),
            'budget_target_total_type' => $request->get("budget_target_total_type"),

            'internal_holder' => $request->get('internal_holder'),
            'brand_name' => $request->get('brand_name'),

            'target' => $request->get('target') ,
            'custom_audiences' => isset($custom_audience) ? json_encode($custom_audience) : null,
            'pixels' =>  $request->get('pixeles') ,

            'googleanalytics_track' => json_encode($request->get('itm_googleAnalytics')),
            'impression_tag' => $request->get('pixelview') == '' ? $request->get('pixelview') : null,
            'url_extra_option' => $request->get('url_extra_option') == '' ? $request->get('url_extra_option') : null,

        ];
        

        try { 
           
            $newcampaing = DB::table('campaigns')->insert($rowdata);

        } catch(\Illuminate\Database\QueryException $ex){ 
            
            Log::channel('ovhlogs')->error('Mysql insert failed -->> '.$ex->getMessage(),  [ '_context' => 'campaignWizard', '_action' => 'mysql_insert_for_campaign', 'short_message' =>  $ex->getMessage() ]);
                   
            $array['response'] = array();
            $array['response']['status'] = false;
            $array['response']['message'] = "Error, please try again. We already notify the team to fix up this issue!";
            return response()->json($array);

          }

    

        if ((isset($_POST["spend_on_each_ad"]) && ($_POST["spend_on_each_ad"] != "")) ||
            (isset($_POST["impressions_per_day"]) && ($_POST["impressions_per_day"] != ""))) {
           
                    $job =  [
                        'type' => 'campaign',
                        'subject_id' => $last_id = DB::getPDO()->lastInsertId(),
                        'action_type' => 'ads_spend_check',
                        'Execution_interval' => '15M',
                        'next_execution' => now()
                    ];
        
                    //DB::connection('stats')->table('background_job')->insert($job);
                    event(new newCronJobEvent($job));

                    
                    //Log::channel('ovhlogs')->info('New Campaign cronjob created.',  [ '_context' => 'campaignWizard', '_action' => 'create_job_for_campaign', 'short_message' => "Background CronJob created OK."]);

        }
        
        $array['response'] = array();
        $array['response']['status'] = true;

        if ($request->get('status') == 'readytupublish'){
            $array['response']['message'] = "Campaign created sucessfully!";
        }else{
            $array['response']['message'] = "Campaign draft saved sucessfully!";
        }


        Log::channel('ovhlogs')->info('Campaign created ok',  [ '_context' => 'campaignWizard', '_action' => 'create_campaign', 'short_message' => "Campagin created succesfully."]);


        return response()->json($array);
    
    }

        //  $this->insert_creatividades($_POST);
        //$this->insert_tablecreartividades($_POST);

        //print_r($_POST);
        //die();
        /*try{

        $api = Api::init($this->app_id, $this->app_secret, $this->access_token);
        $api->setLogger(new CurlLogger());

        //Create Campaign
        $fields = array(
        );
        $params = array(
        'name' => $_POST["nombre_campaña"],
        'objective' => $_POST["facebok_objective"],
        'status' => 'PAUSED',
        );

        $campaign = (new AdAccount($_POST["ad_account"]))->createCampaign(
        $fields,
        $params
        )->exportAllData();

        //Create adset
        /*$campaign = (object)array('id' => '6128042159862' );*/
        //print_r($campaign['id']);

        /*$array = array();
        if(count($_POST["age"])){$array["age"] = $_POST["age"];}
        if(count($location_in)){$array["location_in"] = $location_in;}
        if(count($location_out)){$array["location_out"] = $location_out;}
        if(count($languages_in)){$array["languages_in"] = $languages_in;}
        if(count($interest_in)){$array["interest_in"] = $interest_in;}
        if(count($interest_out)){$array["interest_out"] = $interest_out;}

        $geneder = $_POST["radio7"];

        foreach ($_POST["age"] as $value) {
        $targeting = new Targeting();
        $targeting->{TargetingFields::GENDERS} = array($_POST["radio7"]);
        $targeting->{TargetingFields::AGE_MIN} = $value['start'];
        $targeting->{TargetingFields::AGE_MAX} = $value['end'];
        foreach ($location_in as $value1) {
        $targeting->{TargetingFields::GEO_LOCATIONS} =
        array(
        'cities' => array(array('key' => $value1->id))
        );
        foreach ($interest_in as $value2) {
        $targeting->{TargetingFields::INTERESTS} =
        array(
        array( 'id' => $value2->id),
        );
        }
        }

        $fields = array(
        );
        $params = array(
        'name' => 'Installs Ad Set',
        'lifetime_budget' => '2000',
        'bid_amount' => '500',
        'billing_event' => 'IMPRESSIONS',
        'campaign_id' => $campaign['id'],
        'targeting' => $targeting,
        'status' => 'PAUSED',
        'start_time' => $start_time,
        'end_time' => $end_time,
        );

        (new AdAccount($_POST["ad_account"]))->createAdSet(
        $fields,
        $params
        );

        }
        }catch(Exception $e){
        echo "entre";
        }
        $array['response']= true;
        echo json_encode($array);
        /*
        $targeting = new Targeting();
        $targeting->{TargetingFields::GENDERS} = array($_POST["radio7"]);
        $targeting->{TargetingFields::AGE_MIN} = $_POST["age"][0]['start'];
        $targeting->{TargetingFields::AGE_MAX} = $_POST["age"][0]['end'];
        $targeting->{TargetingFields::GEO_LOCATIONS} =
        array(
        'cities' => array(array('key' => $location_in[0]->id))
        );
        $targeting->{TargetingFields::INTERESTS} =
        array(
        array( 'id' => $interest_in[0]->id),
        );
         */

        /*  $fb = new \Facebook\Facebook([
        'app_id' => $this->app_id,
        'app_secret' => $this->app_secret,
        'default_graph_version' => 'v4.0',
        ]);

        try {
        // Returns a `Facebook\FacebookResponse` object
        /*$response = $fb->post(
        '/'.$_POST["ad_account"].'/adsets',
        array (
        'name' => 'My First AdSet',
        'lifetime_budget' => '20000',
        'start_time' => '2019-10-20T16:27:15-0700',
        'end_time' => '2019-10-27T16:27:15-0700',
        'campaign_id' => $campaign['id'],
        'bid_amount' => '500',
        'billing_event' => 'IMPRESSIONS',
        'targeting' => '{"age_min":20,"age_max":24,"behaviors":[{"id":6002714895372,"name":"All+travelers"}],"genders":[1],"geo_locations":{"countries":["US"],"regions":[{"key":"4081"}],"cities":[{"key":"777934","radius":10,"distance_unit":"mile"}]}}',
        'status' => 'PAUSED',
        ),
        $this->access_token
        );
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
        }
        $graphNode = $response->getGraphNode();*/
        /*$targeting = new Targeting();
        $targeting->{TargetingFields::GENDERS} = array($_POST["radio7"]);
        $targeting->{TargetingFields::AGE_MIN} = $_POST["age"][0]['start'];
        $targeting->{TargetingFields::AGE_MAX} = $_POST["age"][0]['end'];*/

        /*$targeting->{TargetingFields::GEO_LOCATIONS} =
        array(
        'regions' => array(array('key' => $location_in[0]->id)),1
        );
        $targeting->{TargetingFields::INTERESTS} =
        array(
        array( 'id' => $interest_in[0]->id),
        );*/

        /*    $adset = new AdSet(null, $_POST["ad_account"]);
        $adset->setData(array(
        AdSetFields::NAME => 'My Adset',
        AdSetFields::BILLING_EVENT => AdSetBillingEventValues::LINK_CLICKS,
        AdSetFields::BID_AMOUNT => 2,
        AdSetFields::DAILY_BUDGET => 500,
        AdSetFields::CAMPAIGN_ID => $campaign['id'],
        AdSetFields::TARGETING => $targeting,
        AdSetFields::START_TIME => $start_time,
        AdSetFields::END_TIME => $end_time,
        ));
        $adset->create(array(
        AdSet::STATUS_PARAM_NAME => AdSet::STATUS_PAUSED,
        ));

        /*

        $targeting = new Targeting();
        $targeting->{TargetingFields::GENDERS} = array($_POST["radio7"]);
        $targeting->{TargetingFields::AGE_MIN} = $_POST["age"][0]['start'];
        $targeting->{TargetingFields::AGE_MAX} = $_POST["age"][0]['end'];
        $targeting->{TargetingFields::GEO_LOCATIONS} =
        array(
        'regions' => array(array('key' => $location_in[0]->id)),1
        );
        $targeting->{TargetingFields::INTERESTS} =
        array(
        array( 'id' => $interest_in[0]->id),
        );

        $adset = new AdSet(null, $_POST["ad_account"]);
        $adset->setData(array(
        AdSetFields::NAME => 'AdSet '.$_POST["nombre_campaña"],
        /*AdSetFields::OPTIMIZATION_GOAL => AdSetOptimizationGoalValues::REACH,
        AdSetFields::BILLING_EVENT => AdSetBillingEventValues::IMPRESSIONS,
        AdSetFields::BID_AMOUNT => 2,
        AdSetFields::DAILY_BUDGET => 10,
        AdSetFields::CAMPAIGN_ID => $campaign['id'],
        AdSetFields::TARGETING => $targeting,*/
        /*AdSetFields::START_TIME => $start_time,
        AdSetFields::END_TIME => $end_time,
        ));
        $adset->create(array(
        AdSet::STATUS_PARAM_NAME => AdSet::STATUS_PAUSED,
        ));*/

        //'targeting' => array('excluded_geo_locations' => array('regions' => array(array('key' => '3847'))),'geo_locations' => array('countries' => array('US'))),


    public function importe_maximo_mes($fechaInicio, $fechaFin, $budget, $budgetdistribution)
    {

        if ((date('Y-m-d', strtotime($fechaInicio)) != $fechaInicio) or
            (date('Y-m-d', strtotime($fechaFin)) != $fechaFin) or
            ((float) $budget == 0) or ((int) $budgetdistribution == 0)) {
            return "false";
        }

        $importe_temporal = array();
        //Calculamos los importes mensuales de la BBDD a partir del mes en curso
        $user = Auth::user();
        $campaigns = DB::table('campaigns')
            ->select('start_date', 'end_date', 'budget', 'budgetdistribution')
            ->where('user_id', $user->id)
            ->where('start_date', '>', date("Y-m") . "-01")
            ->get();
        foreach ($campaigns as $camp) {
            $Inicio = strtotime($camp->start_date);
            $Fin = strtotime($camp->end_date);

            if ($camp->budgetdistribution == 1) {
                //Por Día
                $importe = $camp->budget;
            } else {
                //En total, divido entre los días y lo calculo por día
                $fecha1 = new DateTime($camp->start_date);
                $fecha2 = new DateTime($camp->end_date);
                $diff = $fecha1->diff($fecha2);
                $dias = $diff->days;
                $importe = $camp->budget / $dias;
            }
            for ($i = $Inicio; $i <= $Fin; $i += 86400) {
                $mes = date("n", $i);
                $importe_temporal[$mes] = (isset($importe_temporal[$mes])) ? $importe_temporal[$mes] + $importe : $importe;
            }
        }

        if ($budgetdistribution == 1) {
            //Por Día
            $importe = $budget;
        } else {
            //En total, divido entre los días y lo calculo por día
            $fecha1 = new DateTime($fechaInicio);
            $fecha2 = new DateTime($fechaFin);
            $diff = $fecha1->diff($fecha2);
            $dias = $diff->days;
            $importe = $budget / $dias;
        }

        $Inicio = strtotime($fechaInicio);
        $Fin = strtotime($fechaFin);

        for ($i = $Inicio; $i <= $Fin; $i += 86400) {
            $mes = date("n", $i);
            $importe_temporal[$mes] = (isset($importe_temporal[$mes])) ? $importe_temporal[$mes] + $budget : $budget;
        }

        $importe_maximo_mes = Auth::user()->damepermisos('importe_maximo_mes');
        foreach ($importe_temporal as $importe) {
            if ($importe_maximo_mes < $importe) {
                return "true";
            }
        }

        return "false";
    }

    public function insert_creatividades($datos)
    {
        $id_en_platform = 1;
        $user = Auth::user();
        $link_display = (isset($datos['link_display'])) ? $datos['link_display'] : "";

        if (!isset($datos['rrss']) || count($datos['rrss']) == 0) {$datos['rrss'] = array('empty');}
        if (!isset($datos['titulo']) || count($datos['titulo']) == 0) {$datos['titulo'] = array('empty');}
        if (!isset($datos['descripcion']) || count($datos['descripcion']) == 0) {$datos['descripcion'] = array('empty');}
        if (!isset($datos['banner']) || count($datos['banner']) == 0) {$datos['banner'] = array('empty');}
        if (!isset($datos['url']) || count($datos['url']) == 0) {$datos['url'] = array('empty');}
        if (!isset($datos['text_link']) || count($datos['text_link']) == 0) {$datos['text_link'] = array('empty');}
        if (!isset($datos['call_to_action']) || count($datos['call_to_action']) == 0) {$datos['call_to_action'] = array('empty');}

        foreach ($datos['rrss'] as $platform) {
            $platform = substr($platform, 0, 3);

            DB::table('creativities')->insert([
                'id_en_platform' => $id_en_platform,
                'user_id' => $user->id,
                'title' => json_encode($datos['titulo']),
                'content' => json_encode($datos['descripcion']),
                'banner' => json_encode($datos['banner']),
                'macrotag' => 0,
                //'type' => 0,
                'size' => 0,
                'platform' => $platform,
                'campana_root' => 0,
                'campana_platform_id' => 0,
                'atomo_id' => 0,
                'description' => $link_display,
                'linkdescription' => json_encode($datos['text_link']),
                'calltoaction' => json_encode($datos['call_to_action']),
                'url' => json_encode($datos['url']),
            ]);

        }
    }

    public function insert_creatividades_permutadas($datos)
    {
        $id_en_platform = 1;
        $user = Auth::user();
        $link_display = (isset($datos['link_display'])) ? $datos['link_display'] : "";

        if (!isset($datos['rrss']) || count($datos['rrss']) == 0) {$datos['rrss'] = array('empty');}
        if (!isset($datos['titulo']) || count($datos['titulo']) == 0) {$datos['titulo'] = array('empty');}
        if (!isset($datos['descripcion']) || count($datos['descripcion']) == 0) {$datos['descripcion'] = array('empty');}
        if (!isset($datos['banner']) || count($datos['banner']) == 0) {$datos['banner'] = array('empty');}
        if (!isset($datos['url']) || count($datos['url']) == 0) {$datos['url'] = array('empty');}
        if (!isset($datos['text_link']) || count($datos['text_link']) == 0) {$datos['text_link'] = array('empty');}
        if (!isset($datos['call_to_action']) || count($datos['call_to_action']) == 0) {$datos['call_to_action'] = array('empty');}

        foreach ($datos['rrss'] as $platform) {
            $platform = substr($platform, 0, 3);
            foreach ($datos['titulo'] as $titulo) {
                $title = (isset($titulo->titulo)) ? $titulo->titulo : "";
                foreach ($datos['descripcion'] as $descripcion) {
                    $content = (isset($descripcion->descripcion)) ? $descripcion->descripcion : "";
                    foreach ($datos['banner'] as $banner) {
                        $banner_txt = (isset($banner->banner)) ? $banner->banner : "";
                        foreach ($datos['url'] as $url) {
                            $urls = (isset($url->url)) ? $url->url : "";
                            foreach ($datos['text_link'] as $text_link) {
                                $linkdescription = (isset($text_link->text_link)) ? $text_link->text_link : "";
                                foreach ($datos['call_to_action'] as $call_to_action) {
                                    $calltoaction = (isset($call_to_action->call_to_action)) ? $call_to_action->call_to_action : "";

                                    DB::table('creativities')->insert([
                                        'id_en_platform' => $id_en_platform,
                                        'user_id' => $user->id,
                                        'title' => $title,
                                        'content' => $content,
                                        'banner' => $banner_txt,
                                        'macrotag' => 0,
                                        //'type' => 0,
                                        'size' => 0,
                                        'platform' => 0,
                                        'campana_root' => 0,
                                        'campana_platform_id' => 0,
                                        'atomo_id' => 0,
                                        'description' => 0,
                                        'linkdescription' => $linkdescription,
                                        'calltoaction' => $calltoaction,
                                        'url' => $urls,
                                    ]);

                                }}}}}}}
    }

    public function createTargeting()
    {

    }

    public function tokenCallback($var = 0)
    {
        session_start();
        if ($var == "facebook") {

            $fb = new \Facebook\Facebook([
                'app_id' => $this->app_id,
                'app_secret' => $this->app_secret,
                'default_graph_version' => 'v3.2',
            ]);

            $helper = $fb->getRedirectLoginHelper();

            try {
                $accessToken = $helper->getAccessToken();
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            if (!isset($accessToken)) {
                if ($helper->getError()) {
                    header('HTTP/1.0 401 Unauthorized');
                    echo "Error: " . $helper->getError() . "\n";
                    echo "Error Code: " . $helper->getErrorCode() . "\n";
                    echo "Error Reason: " . $helper->getErrorReason() . "\n";
                    echo "Error Description: " . $helper->getErrorDescription() . "\n";
                } else {
                    header('HTTP/1.0 400 Bad Request');
                    echo 'Bad request';
                }
                exit;
            }

            // Logged in
            echo '<h3>Access Token</h3>';
            var_dump($accessToken->getValue());

            // The OAuth 2.0 client handler helps us manage access tokens
            $oAuth2Client = $fb->getOAuth2Client();

            // Get the access token metadata from /debug_token
            $tokenMetadata = $oAuth2Client->debugToken($accessToken);
            echo '<h3>Metadata</h3>';
            var_dump($tokenMetadata);

            // Validation (these will throw FacebookSDKException's when they fail)
            $tokenMetadata->validateAppId($this->app_id); // Replace {app-id} with your app id
            // If you know the user ID this access token belongs to, you can validate it here
            //$tokenMetadata->validateUserId('123');
            $tokenMetadata->validateExpiration();

            if (!$accessToken->isLongLived()) {
                // Exchanges a short-lived access token for a long-lived one
                try {
                    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
                    exit;
                }

                echo '<h3>Long-lived</h3>';
                var_dump($accessToken->getValue());
            }
        }
    }

    public function createAdset()
    {

        $adset = new AdSet(null, 'act_' . $this->id_businesses);
        $adset->setData(array(
            AdSetFields::NAME => 'My AdSet',
            AdSetFields::OPTIMIZATION_GOAL => AdSetOptimizationGoalValues::REACH,
            AdSetFields::BILLING_EVENT => AdSetBillingEventValues::IMPRESSIONS,
            AdSetFields::BID_AMOUNT => 2,
            AdSetFields::DAILY_BUDGET => 1000,
            AdSetFields::CAMPAIGN_ID => $this->id_campaign,
            AdSetFields::TARGETING => (new Targeting())->setData(array(
                TargetingFields::GENDERS => array(1),
                TargetingFields::AGE_MIN => 18,
                TargetingFields::AGE_MAX => 43,
            )),
        ));

        $adset->create(array(
            AdSet::STATUS_PARAM_NAME => AdSet::STATUS_ACTIVE,
        ));

    }

    public function insertdata($data)
    {
        $type = "LOCATION";
        if ($data["type"] == "country" || $data["type"] == "region" || $data["type"] == "zip" || $data["type"] == "city" || $data["type"] == "country_group" || $data["type"] == "geo_market" || $data["type"] == "subcity") {
            $type = "GEO";
        }

        $row = settings_matrix::where(['name' => $data['name'],
            'nombre' => $data['name'],
            'type' => $type])->first();
        if ($row == null) {
            $row = settings_matrix::create([
                'uuid' => md5($type),
                'name' => $data['name'],
                'nombre' => $data['name'],
                'type' => $type,
                'valor_fb' => $data['name'],
                'metadata' => json_encode($data),
            ]);
            $row->uuid = md5($row->id . $type);
            $row->save();
        }
    }



    public function getLocation ( Request $request ){
        
        /** TYPE  * GEO, LOCATION, INTEREST, BEHAVIOR, DEMOGRAPHICS, LANGUAGE */
        $location = $request->get('query');
        $locations = DB::table('settings_matrix')->select( 'settings_matrix.uuid','settings_matrix.type', 'settings_matrix.name','settings_matrix.nombre')->where('type', 'LOCATION')->where('name', 'like', '%' . $location . '%')->get();
        return $locations;

    }

    public function getLanguage ( Request $request ){

        /** TYPE  * GEO, LOCATION, INTEREST, BEHAVIOR, DEMOGRAPHICS, LANGUAGE */
        $language = $request->get('query');

        $languages = DB::table('settings_matrix')
                                ->select( 'settings_matrix.uuid','settings_matrix.type', 'settings_matrix.name','settings_matrix.nombre')
                                ->where('type', 'LANGUAGE')
                                ->where('name', 'like', '%' . $language . '%')
                                ->get();
        return $languages;

    }


    public function getCustomAudiences($id_AdAccount = "", $var = "")
    {
        header('Access-Control-Allow-Origin: *');
        if ($var == "") {
            return array();
        }
        $ads_accounts = DB::table('ads_accounts')
            ->select('app_id', 'account_id')
            ->where('id', $id_AdAccount)
            ->first();

        if ($ads_accounts == null) {
            return array();
        }
        $App = DB::table('auths_user_platform')
            ->select('app_id', 'app_secret', 'access_token')
            ->where('app_id', $ads_accounts->app_id)
            ->first();

        $api = Api::init($App->app_id, $App->app_secret, $App->access_token);

        $api->setLogger(new CurlLogger());
        $resp = [];

        $fields = array(
            'id',
            'name',
            'approximate_count',
            'description',
            'data_source',
            'subtype',
        );
        $params = array(
        );

        $result = (new AdAccount($ads_accounts->account_id))->getCustomAudiences(
            $fields,
            $params
        )->getResponse()->getContent();

        foreach ($result['data'] as $value) {
            $pos = strpos($value['name'], $var);
            if (!($pos === false)) {
                $resp[] = $value;
            }
        }

        usort($resp, function ($a, $b) {
            return $a['name'] <=> $b['name'];
        });

        return $resp;
    }

    public function getCustomAudiencesV2 ( Request $request )
    {
       $adAccount = $request->get('adAccount');
       $customAudiences  = $request->get('customAudiences');

        if ($customAudiences == "") { return array(); }

        $data = DB::table('ads_accounts')
        ->join('properties_accounts', 'ads_accounts.id', '=', 'properties_accounts.adaccount_id')
        ->select('properties_accounts.public_id', 'properties_accounts.name','properties_accounts.category', 'properties_accounts.type')
        ->where('ads_accounts.account_id', $adAccount)
        ->where('properties_accounts.name', 'like',  '%' . $customAudiences. '%')
        ->where('properties_accounts.type', 'AUDIENCE')
        ->orderBy('properties_accounts.name')
        ->get();

        if ($data == null) {
            return array();
        }
        return json_encode(array("data"=>$data));

    }

    public function getInterest_V2( Request $request)
    {
        $user = Auth::user();

        $account = auths_user_platform::where('user_id', $user->id)->where('platform', $request->get('platform'))->firstOrFail();
        $query = $request->get('query');
        $interests = [];

        switch ($account->platform) {
            case "TWITTER":

                $auth_settings = json_decode($account->app_secret);
                $twitterapi = TwitterAds::init($auth_settings->consumer_key, $auth_settings->consumer_secret, $account->app_id, $account->access_token);
                $resource = 'targeting_criteria/interests'; 
                $params = ['q'=> $query ];
                $response = $twitterapi->get($resource, $params);

                foreach ($response->getBody()->data as $element){
                    $interests[] = $element;
                }
                break;

            case "LINKEDIN":
                $headers = array();
                $headers[] = 'Authorization: Bearer ' . $account->access_token;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/adTargetingEntities?q=TYPEAHEAD&facet=urn:li:adTargetingFacet:interests&query='.$query);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $result = curl_exec($ch);
                $result = json_decode($result);

                if(!empty($result)){
                    foreach ($result->elements as $element){
                        $interests[] =$element;
                    }
                }                
            break;
        }
        return json_encode(array( "data"=>$interests));

        // return json_encode(array( "data"=>$interests, "response"=>  $result , "acces-token"=>'Authorization: Bearer ' . $account->access_token, "endpoint" => 'https://api.linkedin.com/v2/adTargetingEntities?q=TYPEAHEAD&facet=urn:li:adTargetingFacet:interests&query='.$query));
    }

    // deprecated
    public function getInterest($var = "")
    {
        header('Access-Control-Allow-Origin: *');
        if ($var == "") {
            return array();
        }
        $api = Api::init($this->app_id, $this->app_secret, $this->access_token);
        $params = array('limit' => '100');
        $resp = [];

        $result = TargetingSearch::search(
            TargetingSearchTypes::INTEREST,
            null,
            $var,
            $params
        );

        foreach ($result->getObjects() as $value) {
            $data = $value->getData();
            $row = settings_matrix::where(['name' => $data['name'],
                'nombre' => $data['name'],
                'type' => 'INTEREST'])->first();
            if ($row == null) {
                $row = settings_matrix::create([
                    'uuid' => md5('INTEREST'),
                    'name' => $data['name'],
                    'nombre' => $data['name'],
                    'type' => 'INTEREST',
                    'valor_fb' => $data['name'],
                    'metadata' => json_encode($data),
                ]);
                $row->uuid = md5($row->id . 'INTEREST');
                $row->save();
            }
            $data['type'] = 'interests';
            $resp[] = $data;
        }

        /*  , behaviors, demographics, life_events, industries, income, family_statuses, user_device, user_os*/

        $result = TargetingSearch::search(
            TargetingSearchTypes::TARGETING_CATEGORY,
            'behaviors'
        );

        foreach ($result->getObjects() as $value) {
            $pos = strpos($value->name, $var);
            if (!($pos === false)) {

                $data = $value->getData();
                $row = settings_matrix::where(['name' => $data['name'],
                    'nombre' => $data['name'],
                    'type' => 'BEHAVIOR'])->first();
                if ($row == null) {
                    $row = settings_matrix::create([
                        'uuid' => md5('BEHAVIOR'),
                        'name' => $data['name'],
                        'nombre' => $data['name'],
                        'type' => 'BEHAVIOR',
                        'valor_fb' => $data['name'],
                        'metadata' => json_encode($data),
                    ]);
                    $row->uuid = md5($row->id . 'BEHAVIOR');
                    $row->save();
                }

                $resp[] = $data;
            }
        }

        $result = TargetingSearch::search(
            TargetingSearchTypes::TARGETING_CATEGORY,
            'demographics'
        );

        foreach ($result->getObjects() as $value) {
            $pos = strpos($value->name, $var);
            if (!($pos === false)) {
                $data = $value->getData();
                $row = settings_matrix::where(['name' => $data['name'],
                    'nombre' => $data['name'],
                    'type' => 'DEMOGRAPHICS'])->first();
                if ($row == null) {
                    $row = settings_matrix::create([
                        'uuid' => md5('DEMOGRAPHICS'),
                        'name' => $data['name'],
                        'nombre' => $data['name'],
                        'type' => 'DEMOGRAPHICS',
                        'valor_fb' => $data['name'],
                        'metadata' => json_encode($data),
                    ]);
                    $row->uuid = md5($row->id . 'DEMOGRAPHICS');
                    $row->save();
                }
                $data['type'] = 'interests';
                $resp[] = $data;
            }
        }

        $result = TargetingSearch::search(
            TargetingSearchTypes::TARGETING_CATEGORY,
            'life_events'
        );

        foreach ($result->getObjects() as $value) {
            $pos = strpos($value->name, $var);
            if (!($pos === false)) {
                $data = $value->getData();
                $row = settings_matrix::where(['name' => $data['name'],
                    'nombre' => $data['name'],
                    'type' => 'BEHAVIOR'])->first();
                if ($row == null) {
                    $row = settings_matrix::create([
                        'uuid' => md5('BEHAVIOR'),
                        'name' => $data['name'],
                        'nombre' => $data['name'],
                        'type' => 'BEHAVIOR',
                        'valor_fb' => $data['name'],
                        'metadata' => json_encode($data),
                    ]);
                    $row->uuid = md5($row->id . 'BEHAVIOR');
                    $row->save();
                }
                $data['type'] = 'interests';
                $resp[] = $data;
            }
        }

        usort($resp, function ($a, $b) {
            return $a['name'] <=> $b['name'];
        });

        return $resp;
    }

    public function getAdAccount($var = "")
    {
        header('Access-Control-Allow-Origin: *');
        //$api = Api::init($this->app_id, $this->app_secret, $this->access_token);
        //$api->setLogger(new CurlLogger());
        $fb = new \Facebook\Facebook([
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_graph_version' => 'v4.0',
        ]);
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get(
                '/' . $this->id . '/adaccounts?fields=id,name',
                $this->access_token
            );
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        $response = json_decode($response->getBody());

        /*echo '<pre>';
        print_r($response);
        echo '</pre>';
        foreach ($response->data as $data) {
        echo $data->id."<br>";

        }*/

        return $response->data;
    }

    public function getPromotePages($AdAccount, $var = "")
    {
        header('Access-Control-Allow-Origin: *');

        //TODO Api calls

        //switch case PLATFORM
        // get auth credentials
        // get api
        // platofrm id, value json response
        
        $return = "";
        
        $fb = new \Facebook\Facebook([ 'app_id' => $this->app_id, 'app_secret' => $this->app_secret, 'default_graph_version' => 'v10.0' ]);
        
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get(
                '/' . $var . '/promote_pages?fields=id,name,picture',
                $this->access_token
            );
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        $response = json_decode($response->getBody());
        $return = $response->data;
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get(
                '/' . $this->id . '/accounts?fields=id,name,picture',
                $this->access_token
            );
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $response = json_decode($response->getBody());

        $return = array_merge($return, $response->data);

        return $return;
    }

    /*
    public function renoveAccessToken (){
        file_get_contents('https://graph.facebook.com/oauth/access_token?client_id='.$this->app_id.'&client_secret='.$this->app_secret.'&grant_type=fb_exchange_token&fb_exchange_token='.$this->access_token);
    }*/
    public function call()
    {
        session_start();

        $fb = new \Facebook\Facebook([
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_graph_version' => 'v3.2',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl($this->url, $permissions);

        echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
    }

    public function createClient()
    {
        $id_user = Auth::id();
        echo DB::table('customers')->insertGetId(['name' => $_POST['nameClient'], 'user_id' => $id_user]);
    }

    public function updateClient(Request $request)
    {
        echo DB::table('customers')->where('id', $_POST['id'])->update($request->all());
    }

    public function get_connections(Request $request)
    {

        $id_user = Auth::id();

        //$column = array('rrss', 'platform_user_id', 'platform_email', 'ads_accounts', 'properties', 'default_client', 'clients', 'status', 'user');
        $column = array('rrss', 'platform_user_id', 'platform_email', 'ads_accounts', 'properties', 'default_client', 'status');

        $order = is_null($request->get('order')) ? array('column' => 0, 'dir' => 'asc') : $request->get('order')[0];
        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $keyword = '';

        if (!is_null($request->get('search'))) {$keyword = $request->get('search')['value'];}


        $connections = DB::table('auths_user_platform')
                            ->join('users', 'users.id', '=', 'auths_user_platform.user_id')
                            ->leftJoin(
                                DB::raw("(select auth_id, COALESCE(count(*), 0) as 'count_ads' from ads_accounts group by auth_id) as tmp_table_ads"), 
                                'tmp_table_ads.auth_id', 
                                '=', 
                                'auths_user_platform.id')

                            ->leftJoin(
                                DB::raw("(select auth_id, COALESCE(count(*), 0) as 'count_properties' from properties_adsaccount_relations group by auth_id) as tmp_table_prop"), 
                                'tmp_table_prop.auth_id', 
                                '=', 
                                'auths_user_platform.id')

                            ->select(
                                'auths_user_platform.id', 
                                'auths_user_platform.user_id',
                                'auths_user_platform.auths_user_id',
                                'auths_user_platform.platform',
                                'auths_user_platform.app_id', 
                                'auths_user_platform.platform_user_id',
                                'auths_user_platform.platform_email', 
                                'auths_user_platform.activa',
                                'auths_user_platform.clients', 
                                'auths_user_platform.default_client', 
                                'users.name',
                                'tmp_table_ads.count_ads',
                                'tmp_table_prop.count_properties',
                                
                            )
                            ->where('users.id', $id_user)
                            ->get();


        $connections->transform(function ($value) {

            $default_client = DB::table('customers')->select('public_id', 'name')->where('id', $value->default_client)->first();
            $clients = array("public_id" => "", "name" => "");
           
            if ($value->clients != "") {
                $client = json_decode($value->clients);
                $clients = DB::table('customers')->select('public_id', 'name')->whereIn('id', $client)->get();

                $str_id = "";
                $str_clients = "";
                foreach ($clients as $client) {
                    $str_clients .= $client->name . ", ";
                    $str_id .= $client->public_id . ",";
                }

                $str_clients = trim($str_clients, ', ');
                $str_id = trim($str_id, ',');

                $clients = array("public_id" => $str_id, "name" => $str_clients);
            }

            $data = [
                'id' => $value->id,
                'public_id' => $value->auths_user_id,
                'rrss' => $value->platform,
                'platform_user_id' => $value->platform_user_id,
                'platform_email' => $value->platform_email,
                'ads_accounts' => $value->count_ads,
                'properties' => $value->count_properties,
                'status' => $value->activa,
                //   'clients' => $clients,
                'default_client' => (isset($default_client)) ? $default_client : array("public_id" => "", "name" => ""),
                //  'user' => $value->user_id . " - " . $value->name,
            ];
            return $data;
        });

        $total = count($connections);
        if (!is_null($request->get('columns'))) {
            foreach ($request->get('columns') as $req_column) {
                if ($req_column['search']['value'] != "") {
                    $connections = $connections->filter(function ($value, $key) use ($req_column) {

                        if (strpos(strtoupper($value[$req_column['data']]), strtoupper($req_column['search']['value'])) !== false) {
                            return $value;
                        }
                    });
                }
            }
        }

        $recordsFiltered = count($connections);

        //$page
        $chunks = $connections->chunk($limit);

        $data = "";
        if (count($chunks) > 0) {
            $data = $chunks[$page - 1]->values();
        } else {
            $data = [];
        }

        if ($order['dir'] == 'asc') {
            $connections = $connections->sortBy($column[$order['column']]);
        } else {
            $connections = $connections->sortByDesc($column[$order['column']]);
        }
        $result = [
            "draw" => $request->input('draw') ?: 1,
            "recordsTotal" => $total,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data,
            "length" => $limit,
        ];

        return $result;
    }

    public function get_ads_accounts(Request $request)
    {
        $id_user = Auth::id();
        $platform_user_id = is_null($request->route('id')) ? 0 : $request->route('id');

        $column = array('rrss', 'ADS_name', 'platform_user_id', 'user');

        $order = is_null($request->get('order')) ? array('column' => 0, 'dir' => 'asc') : $request->get('order')[0];
        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $keyword = '';

        if (!is_null($request->get('search'))) {$keyword = $request->get('search')['value'];}

        /** 
         * TODO.query que agrupa y trae todo desde DB, quiza para el futuro optimizacion...
        */

        if ($platform_user_id == 0) {
            $connections = DB::table('ads_accounts')
                ->join('users', 'users.id', '=', 'ads_accounts.user_id')
                ->select('ads_accounts.customers', 'ads_accounts.id', 'ads_accounts.public_id', 'ads_accounts.name AS ADS_name', 'ads_accounts.user_id', 'ads_accounts.platform', 'ads_accounts.app_id', 'ads_accounts.platform_user_id', 'users.name')
                ->where('users.id', $id_user)
                ->get();
        } else {
            $connections = DB::table('ads_accounts')
                ->join('users', 'users.id', '=', 'ads_accounts.user_id')
                ->join('auths_user_platform', function ($join) {
                    $join->on('ads_accounts.platform_user_id', '=', 'auths_user_platform.platform_user_id')
                        ->on('ads_accounts.app_id', '=', 'auths_user_platform.app_id')
                        ->on('ads_accounts.platform', '=', 'auths_user_platform.platform');
                })
                ->select('ads_accounts.customers', 'ads_accounts.id', 'ads_accounts.public_id', 'ads_accounts.name AS ADS_name', 'ads_accounts.user_id', 'ads_accounts.platform', 'ads_accounts.app_id', 'ads_accounts.platform_user_id', 'users.name')
                ->where('auths_user_platform.id', $platform_user_id)
                ->where('users.id', $id_user)
                ->get();
        }


        $this->customersList = DB::table('customers')->select('public_id', 'name', 'ads_accounts')->where('user_id', $id_user)->get();

        $connections->transform(function ($value) {
          
            $clients = array();

            if ($value->customers != null){
                
                $customers_data = explode(",", str_replace( array("[", "]",'"', " "), '', $value->customers) );
                $default_client = DB::table('customers')->select('public_id', 'name')->whereIn('public_id',  $customers_data )->get();
          
                if ( count($default_client) > 0 ) {

                    foreach ($default_client as $client) {
                        array_push($clients, array("public_id" => $client->public_id, "name" =>  $client->name));
                    }

                }

            }

            $data = [
                'id' => $value->public_id,
                'rrss' => is_null($value->platform) ? "OTHER" : $value->platform,
                'name' => $value->ADS_name,
                'platform_user_id' => $value->platform_user_id,
                'user' => $value->user_id . " - " . $value->name,
                'clients' => $clients,
            ];

            return $data;
        });

        $total = count($connections);
        if (!is_null($request->get('columns'))) {
            foreach ($request->get('columns') as $req_column) {
                if ($req_column['search']['value'] != "") {
                    $connections = $connections->filter(function ($value, $key) use ($req_column) {
                        if (strpos(strtoupper($value[$req_column['data']]), strtoupper($req_column['search']['value'])) !== false) {
                            return $value;
                        }
                    });
                }
            }
        }

        $recordsFiltered = count($connections);

        //$page
        $chunks = $connections->chunk($limit);

        $data = "";
        if (count($chunks) > 0) {
            $data = $chunks[$page - 1]->values();
        } else {
            $data = [];
        }

        if ($order['dir'] == 'asc') {
            $connections = $connections->sortBy($column[$order['column']]);
        } else {
            $connections = $connections->sortByDesc($column[$order['column']]);
        }

        $result = [
            "draw" => $request->input('draw') ?: 1,
            "recordsTotal" => $total,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data,
            "length" => $limit,
        ];

        return $result;
    }


    public function connection_updater(Request $request){
        
        $public = $request->get('ad_account');
        $user_id = Auth::user()->id;
        $auths = DB::table('auths_user_platform')->select('id','platform')->where('auths_user_id', $public)->where('user_id', $user_id)->get();

        dd($auths);

        if ($auths!=null){

            // refresh token


            //con el token refrescado y lanzamos una retrieve_all
            $data = array( 'subject_id' => $auths[0]->id, 'type' => 'retrieve_all', 'function' => 'function-'.strtolower($auths[0]->platform).'-api' );
            event(new TaskCreate($data));
            return true;

        }

        return false;

    }





    public function connection_sync(Request $request){
        
        $auth_pid = $request->get('auth_public_id');
        $user_id = Auth::user()->id;
        $auths = DB::table('auths_user_platform')->select('id','platform','platform_user_id')->where('auths_user_id', $auth_pid)->where('user_id', $user_id)->get();
        
        if ($auths!=null){

            $data = array( 
                            'subject_id' => $auths[0]->id, 
                            'type' => 'retrieve_all', 
                            'function' => 'function-'.strtolower($auths[0]->platform).'-api' ,
                            'cola_trabajo' => strtolower($auths[0]->platform) . '-sync'
                        );

            //event(new TaskCreate($data)); 
            
            //sync stats CRONJOB
            //cuando se añade un token o refresca desde el boton refrescar de connections, 
            //se crean las tasks de sync y se crean/update los 2 crones por token: get entitites cada 12 horas, y getstats 24 horas

            $data = array(  'prefix'=> $auths[0]->platform. $auths[0]->id.'_'. $auths[0]->platform_user_id , 'subject_id' => $auths[0]->id, 
                'type' => 'retrieve_all', 
                'Execution_interval' => '12H',
                'function' => 'function-'.strtolower($auths[0]->platform).'-api' 
            );

            event(new newCronJobEvent($data)); 



    
            $data = array('prefix'=> $auths[0]->platform. $auths[0]->id.'_'. $auths[0]->platform_user_id ,
                        'subject_id' => $auths[0]->id,
                        'type' => 'retrieve_stats_all',
                        'Execution_interval' => '24H',
                        'function' => 'function-'.strtolower($auths[0]->platform).'-api'
                    );

            event(new newCronJobEvent($data));


            return true;
        }

        return false;

    }

    public function get_properties(Request $request)
    {
        $id_user = Auth::id();

        $platform_user_id = is_null($request->route('id')) ? 0 : $request->route('id');

        $column = array('rrss', 'properties_name', 'platform_user_id', 'client');

        $order = is_null($request->get('order')) ? array('column' => 0, 'dir' => 'asc') : $request->get('order')[0];
        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $keyword = '';

        if (!is_null($request->get('search'))) {$keyword = $request->get('search')['value'];}


		//$properties_accounts = DB::table('properties_adsaccount_relations')->select('property_publicid as public_id', 'property_name as name', 'platform')->where('user_id', $id_user)->orderBy('platform')->get();
     
        /*   
        select 
            properties_adsaccount_relations.property_publicid,
            properties_adsaccount_relations.property_name,
            properties_adsaccount_relations.user_id,
            auths_user_platform.platform_user_id,
            auths_user_platform.app_id
        from properties_adsaccount_relations join auths_user_platform on properties_adsaccount_relations.auth_id = auths_user_platform.id
        */

        if ($platform_user_id == 0) {
           
            $connections = DB::table('properties_accounts')->join('users', 'users.id', '=', 'properties_accounts.user_id')
                                                ->select(
                                                    'properties_accounts.public_id',
                                                    'properties_accounts.name as prop_name', 
                                                    'properties_accounts.user_id', 
                                                    'properties_accounts.platform', 
                                                    'properties_accounts.app_id',
                                                    'properties_accounts.customers',
                                                    'properties_accounts.platform_user_id',
                                                    'users.name' )
                                                ->where('user_id', $id_user)
                                                ->whereIn('properties_accounts.type', array('PROFILE', 'PAGE'))
                                                ->orderBy('platform')
                                                ->get();


        } else {
           /* $connections = DB::table('properties_accounts')
                ->join('users', 'users.id', '=', 'properties_accounts.user_id')
                ->join('auths_user_platform', function ($join) {
                    $join->on('ads_accounts.platform_user_id', '=', 'auths_user_platform.platform_user_id')
                        ->on('ads_accounts.app_id', '=', 'auths_user_platform.app_id')
                        ->on('ads_accounts.platform', '=', 'auths_user_platform.platform');
                })
                ->select('properties_accounts.id', 'properties_accounts.name AS properties_name', 'properties_accounts.user_id', 'properties_accounts.platform', 'properties_accounts.app_id', 'properties_accounts.platform_user_id', 'users.name')
                ->where('auths_user_platform.id', $platform_user_id)
                ->where('users.id', $id_user)
                ->get();*/
        }

        $connections->transform(function ($value) {

            $customersArray = json_decode($value->customers);

            if (is_array($customersArray)){
                $customers_associated = Customer::whereIn('public_id', $customersArray )->select('public_id', 'name')->get()->toArray();
            }else{
                $customers_associated = null;
            }


            $data = [
                'id' => $value->public_id,
                'rrss' => is_null($value->platform) ? "OTHER" : $value->platform,
                'name' => $value->prop_name,
                'platform_user_id' => $value->platform_user_id,
                'user' => $value->user_id . " - " . $value->name,
                'customers' => $customers_associated,

            ];
            return $data;
        });

        $total = count($connections);

        if (!is_null($request->get('columns'))) {
            foreach ($request->get('columns') as $req_column) {
                if ($req_column['search']['value'] != "") {
                    $connections = $connections->filter(function ($value, $key) use ($req_column) {
                        if (strpos(strtoupper($value[$req_column['data']]), strtoupper($req_column['search']['value'])) !== false) {
                            return $value;
                        }
                    });
                }
            }
        }

        $recordsFiltered = count($connections);

        //$page
        $chunks = $connections->chunk($limit);

        $data = "";
        if (count($chunks) > 0) {
            $data = $chunks[$page - 1]->values();
        } else {
            $data = [];
        }

        if ($order['dir'] == 'asc') {
            $connections = $connections->sortBy($column[$order['column']]);
        } else {
            $connections = $connections->sortByDesc($column[$order['column']]);
        }

        $result = [
            "draw" => $request->input('draw') ?: 1,
            "recordsTotal" => $total,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data,
            "length" => $limit,
        ];

        return $result;
    }

    public function getCustomer(string $id = null)
    {
        return Customer::find($id);
    }

    public function getCustomerDataTable(Request $request)
    {

        $id_user = Auth::id();

        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $keyword = '';
        if (!is_null($request->get('search'))) {$keyword = $request->get('search')['value'];}

        $customer = DB::table('customers')
                        ->join('users', 'customers.user_id', '=', 'users.id')
                        ->leftJoin(
                            DB::raw("(select customer_id, COALESCE(SUM(spent),0) as 'total_spend', SUM(if(campaigns.status = 'ACTIVE', 1, 0)) AS 'active_campaigns', count(*) as 'total_campaigns' from campaigns group by customer_id ) as tmp"), 'tmp.customer_id', 
                                     '=', 
                                     'customers.id')
        ->select(
            "customers.public_id as id",
           // DB::raw("concat(users.id, ' - ',users.name) as user_name"),
            "customers.name",
            //"customers.spend",
            "customers.isdefault",
            "customers.status",
            //"customers.impressions",
            //"customers.conversions",
            "customers.cpc",
            //"customers.cpm",
            //"customers.ctr",
            "tmp.total_spend",
            "tmp.active_campaigns",
            "tmp.total_campaigns",
        )
        ->where('customers.user_id', $id_user)
        ->get();


        $result = [
            "draw" => $request->input('draw') ?: 1,
            "recordsTotal" => count( $customer),
            "recordsFiltered" => count( $customer),
            "data" => $customer->toArray(),
            "length" => 10,
           
        ];

        return $result;
    }

    public function getClients_select(Request $request)
    {

        $id_user = Auth::id();

        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $keyword = '';
        if (!is_null($request->get('search'))) {$keyword = $request->get('search')['value'];}

        $customer = Customer::with('users')
            ->where(function ($query) use ($keyword) {$query->whereRaw("name like  '%{$keyword}%' ");})
            ->where('user_id', $id_user)
            ->paginate($limit, ['*'], 'page', $page);

        $customer->getCollection()->transform(function ($value) {
            $user = $value->users->first();
            $data = [
                $value->public_id => $value->name,
            ];
            return $data;
        });

        return $customer->items();
    }

    public function getClients_select2(Request $request)
    {

        $id_user = Auth::id();

        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $keyword = '';
        if (!is_null($request->get('search'))) {$keyword = $request->get('search')['value'];}

        $customer = Customer::with('users')
            ->where(function ($query) use ($keyword) {$query->whereRaw("name like  '%{$keyword}%' ");})
            ->where('user_id', $id_user)
            ->paginate($limit, ['*'], 'page', $page);

        $customer->getCollection()->transform(function ($value) {
            $data = [
                "id" => $value->public_id,
                "text" => $value->name,
            ];
            return $data;
        });

        header('Content-Type: application/json');
        return $customer->items();
    }

    public function set_default_client(Request $request)
    {

        $client = DB::table('customers')->where('public_id', $_POST['value'])->first();
        DB::table('auths_user_platform')->where('id', $_POST['pk'])->update(['default_client' => $client->id]);
    }



    public function usetClientConnection(Request $req){

        DB::table('auths_user_platform')->where('id', $_POST['idPkAu'])->update(['default_client' => null]);

    }

    public function addClientToProperty(Request $request){
        
        //DB::enableQueryLog();

        // POST request parameters
        // value -> idCliente
        // pk -> id properti

        $client = Customer::where ('public_id', $request->get('value'))->firstOrFail();
        
        $properties_accounts = DB::table('properties_accounts')->where('public_id', $request->get('pk'))->get();

        $cusAssociated = json_decode($properties_accounts[0]->customers);
     
        if(!is_array($cusAssociated)){

            $prop_to_add = array($request->get('value'));
            DB::table('properties_accounts')->where('public_id', $_POST['pk'])->update(['customers' => json_encode($prop_to_add)]);

        }else{

            if ( in_array($request->get('value'), $cusAssociated) === false){
              
                array_push($cusAssociated , $request->get('value')); 
                DB::table('properties_accounts')->where('public_id', $_POST['pk'])->update(['customers' => json_encode($cusAssociated)]);

            }else{              
                return response('Duplicate Entry', 200)->header('Content-Type', 'text/plain');           
             }
        }
        

        // actualizamos json de la tabla customers para que la edicion los tenga bien identificados por platform ...
        
      /*  $client_publicid = $request->get('value');
        $client_properties = json_decode($client->properties);
        dd($client_properties);
        $properti_rrss = $properties_accounts[0]->platform;


        if ($client_properties==null){
            // el cliente no tiene nada asociado 

        }else{

        }
    */


    }



    public function addClientToAdsAccount(Request $request){
        
        $client = Customer::where ('public_id', $request->get('value'))->firstOrFail();
        $ad_account = DB::table('ads_accounts')->where('public_id', $request->get('pk'))->get();

        $cusAssociated = json_decode($ad_account[0]->customers);
       // dd($ad_account[0]->customers);
        if(!is_array($cusAssociated)){

            $ads_accounts = array($request->get('value'));
            DB::table('ads_accounts')->where('public_id', $_POST['pk'])->update(['customers' => json_encode($ads_accounts)]);

        }else{

            if ( in_array($request->get('value'), $cusAssociated) === false){
                array_push($cusAssociated , $request->get('value'));
                DB::table('ads_accounts')->where('public_id', $_POST['pk'])->update(['customers' => json_encode($cusAssociated)]);
            }else{
              
                return response('Duplicate Entry', 200)->header('Content-Type', 'text/plain');            }
        }

    }


    public function clients()
    {
        $values = explode(',', $_POST['value']);
        $client_id = array();
        $return = array();
        $str_id = "";
        $str_text = "";
        foreach ($values as $value) {
            if ($value != "") {
                $client = DB::table('customers')->select('id', 'public_id', 'name')->where('public_id', $value)->first();
                if (isset($client->id)) {
                    $client_id[] = $client->id;
                    $str_id .= $client->public_id . ", ";
                    $str_text .= $client->name . ", ";
                    //$return[] = array("id" => $client->public_id, "text" => $client->name);
                }
            }
        }

        $str_text = trim($str_text, ', ');
        $str_id = trim($str_id, ', ');

        DB::table('auths_user_platform')->where('id', $_POST['pk'])->update(['clients' => json_encode($client_id)]);

        return array("id" => $str_id, "text" => $str_text, "pk" => $_POST['pk']);
    }

    

    public function adAccountProperty(Request $request)
    {
        $idCustomer = $request->input('id');
        $account_id = $request->input('account_id');
        $platform = $request->input('account_type');

        $properties = DB::table('ads_accounts')
            ->join('properties_accounts', 'ads_accounts.account_id', '=', 'properties_accounts.adaccount_id')
            ->select('properties_accounts.id', 'properties_accounts.name')
            ->where('ads_accounts.platform', $platform)
            ->where('ads_accounts.public_id', $account_id)
            ->where('properties_accounts.type', 'PAGE')
            ->get();
        if (count($properties) == 0) {
            $user = Auth::user();

            $properties = DB::table('properties_adsaccount_relations')
                        ->join('properties_accounts', 'properties_adsaccount_relations.property_publicid', '=', 'properties_accounts.public_id')
                        ->select('properties_adsaccount_relations.property_publicid', 'properties_adsaccount_relations.property_name', 'properties_adsaccount_relations.platform', 'properties_accounts.customers')
                        ->distinct()
                        ->where('properties_adsaccount_relations.user_id', $user->id)
                        ->where('properties_adsaccount_relations.platform', $platform)
                        ->where('customers','like', '%' .$idCustomer .'%')
                        ->orderBy('properties_adsaccount_relations.property_name')
                        ->get();
                
            if (count($properties) == 0) {
                $properties = DB::table('properties_adsaccount_relations')
                    ->join('properties_accounts', 'properties_adsaccount_relations.property_publicid', '=', 'properties_accounts.public_id')
                    ->select('properties_adsaccount_relations.property_publicid', 'properties_adsaccount_relations.property_name', 'properties_adsaccount_relations.platform', 'properties_accounts.customers')
                    ->distinct()
                    ->where('properties_adsaccount_relations.user_id', $user->id)
                    ->where('properties_adsaccount_relations.platform', $platform)
                    ->where('customers', null)
                    ->orderBy('properties_adsaccount_relations.property_name')
                    ->get();
            }

            $list = array();

            foreach ($properties as $value) {
                $list[] = array("id" => $value->property_publicid, "name" => $value->property_name);
            }

            $properties = $list;
        }

        return $properties; 
    }

    public function insertAutocomplete($name, $type, $metadata, $rrss)
    {
        DB::table('autocomplete_matrix')->updateOrInsert(
            ['name' => $name,
                'rrss' => $rrss],
            ['name' => $name,
                'type' => $type,
                'type' => $type,
                'metadata' => json_encode($metadata),
                'rrss' => $rrss,
                'count' => DB::raw('count + 1'),
            ]
        );
    }

    public function save_Autocomplete()
    {
        return true;
        $type = "";
        if (isset($_POST["data"])){
            foreach ($_POST["data"] as $data) {
                $data = (object) $data;
                // dd($data);
                if (isset($data->type) && ($data->type != "")) {
                    $type = $data->type;
                } else if (isset($_POST["type"]) && ($_POST["type"] != "")) {
                    $type = $_POST["type"];
                } else if ( isset($data->path[0])  ){
                    $type = $data->path[0];
                }
                $this->insertAutocomplete($data->name, $type, $data, $_POST["rrss"]);
            }
        }
    }

    public function getLocationAutocomplete(Request $request)
    {
        $metadata = DB::table('autocomplete_matrix')
            ->where('name', $request->input('name'))
            ->where('type', $request->input('type'))
            ->value('metadata');
        return json_decode($metadata);
    }

    public function log_chargebee(Request $request)
    {
     //   file_put_contents('/home/app.thesocialaudience.com/logs/chargebee.txt', print_r($request->all(), true));

      /*  $data = $request->all();

        if ($data->event_type == 'subscription_renewed') {
            $user = DB::table('users')->select('id')->where('email', $data->content->customer->email)->get();
            if (count($user) == 1) {

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://adsconcierge.chargebee.com/api/v2/subscriptions/" . md5($user->email));
                curl_setopt($ch, CURLOPT_USERPWD, "live_UhGIHjzvcdOWTwEU28Rt1Mk0khGjVGk40");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $server_output = json_decode(curl_exec($ch));
                
                if (isset($server_output->subscription->meta_data)) {
                    $metadata = $server_output->subscription->meta_data;
                    $permisos = Permisos::where('user_id', $user->id)->first();

                    if ($permisos && $permisos->count()) {
                        $permisos->plataformas = ((isset($metadata->plataformas)) ? json_encode($metadata->plataformas) : "[]");
                        $permisos->num_plataformas = ((isset($metadata->num_plataformas)) ? $metadata->num_plataformas : 0);
                        $permisos->num_campanas = ((isset($metadata->num_campanas)) ? $metadata->num_campanas : 0);
                        $permisos->cross_plataformas = ((isset($metadata->cross_plataformas)) ? (($metadata->cross_plataformas) ? 1 : 0) : 0);
                        $permisos->crear_usuarios = ((isset($metadata->crear_usuarios)) ? (($metadata->crear_usuarios) ? 1 : 0) : 0);
                        $permisos->compartir_reportes = ((isset($metadata->compartir_reportes)) ? (($metadata->compartir_reportes) ? 1 : 0) : 0);
                        $permisos->importe_maximo_mes = ((isset($metadata->importe_maximo_mes)) ? $metadata->importe_maximo_mes : 0);
                        $permisos->save();
                    } else {
                        $permisos = Permisos::create([
                            'user_id' => $user->id,
                            'plataformas' => ((isset($metadata->plataformas)) ? json_encode($metadata->plataformas) : "[]"),
                            'num_plataformas' => ((isset($metadata->num_plataformas)) ? $metadata->num_plataformas : 0),
                            'num_campanas' => ((isset($metadata->num_campanas)) ? $metadata->num_campanas : 0),
                            'cross_plataformas' => ((isset($metadata->cross_plataformas)) ? (($metadata->cross_plataformas) ? 1 : 0) : 0),
                            'crear_usuarios' => ((isset($metadata->crear_usuarios)) ? (($metadata->crear_usuarios) ? 1 : 0) : 0),
                            'compartir_reportes' => ((isset($metadata->compartir_reportes)) ? (($metadata->compartir_reportes) ? 1 : 0) : 0),
                            'importe_maximo_mes' => ((isset($metadata->importe_maximo_mes)) ? $metadata->importe_maximo_mes : 0),
                        ]);
                    }

                }
            }
        }
        */
        $response = array("status" => true, "message" => "Save");

        return $response;
    }

    public function auth_by_client(Request $request, string $id = null)
    {
        $user = Auth::user();
        
        // validamos que exista el customer
        $customer = Customer::where('public_id', $id)->firstOrFail();

        $auths = DB::table('ads_accounts')->select('platform')->distinct()
                        ->where('user_id', $user->id)
                        ->where('customers', 'like', '%' . $id . '%')
                        ->get()
                        ->toArray();

        if (count($auths) == 0) {
            $auths = DB::table('ads_accounts')->select('platform')
                                                ->distinct()->where('user_id', $user->id)
                                                ->get()->toArray();
        }

        // si tenemos asociadas ads_accounts para FB y no para IG, igualmente agregamos IG al response para poder seleccionarla desde el front.                        
        if (  array_search('FACEBOOK', array_column($auths, 'platform')) >= 0 &&  array_search('INSTAGRAM', array_column($auths, 'platform')) === false ){
            array_push($auths, array("platform"=> "INSTAGRAM"));
        }

        return $auths;
    }


    public function account_by_client(Request $request, string $id = null, string $platform = null)
    {
        $user = Auth::user();

        // validamos que exista el customer
        $customer = Customer::where('public_id', $id)->firstOrFail();
        
        // devolvemos las ads_accounts asociadas al cliente seleccionado 
        $accounts = DB::table('ads_accounts')
                                ->select('ads_accounts.public_id', 'ads_accounts.currency', 'ads_accounts.name', 'ads_accounts.account_id','ads_accounts.customers')
                                ->where('ads_accounts.platform', '=', $platform)
                                ->where('ads_accounts.user_id', $user->id)
                                ->whereJsonContains('customers', array($id)) 
                                ->orderBy('name')
                                ->get();

        if (count($accounts) == 0) {
            
            // si el cliente no tiene ads_accounts
            // devolvemos las ads_accounts asociadas al usuario
    
            $accounts = DB::table('ads_accounts')
                                ->select('ads_accounts.public_id', 'ads_accounts.name',  'ads_accounts.account_id')
                                ->where('ads_accounts.platform', '=', $platform)
                                ->where('ads_accounts.user_id', $user->id)
                                ->orderBy('name')
                                ->get();
        }
    
        if (count($accounts) > 0) {
            
            //$accountsList = $accounts->pluck('account_id')->toArray();
            $accountsPubIDList = $accounts->pluck('public_id')->toArray();
            //ad_account_public_id 
            $properties = DB::table('properties_adsaccount_relations')
                                ->select('property_publicid', 'property_name as name', 'ad_account_publicid as ad_account_public_id')
                                ->where('properties_adsaccount_relations.type', 'PAGE')
                                ->whereIn('ad_account_publicid', $accountsPubIDList)
                                ->orderBy('property_name')
                                ->get();
        
            // cargamos todos los pixels para el step4
            $pixels = DB::table('properties_adsaccount_relations')
                            ->select('property_publicid as id', 'property_name as name', 'ad_account_publicid as ad_account_public_id')
                            ->where('properties_adsaccount_relations.type', 'PIXEL')
                            ->whereIn('ad_account_publicid', $accountsPubIDList)
                            ->orderBy('property_name')
                            ->get();
            
            if (count($properties) == 0) {
                $properties = DB::table('properties_adsaccount_relations')
                        ->join('properties_accounts', 'properties_adsaccount_relations.property_publicid', '=', 'properties_accounts.public_id')
                        ->select('properties_adsaccount_relations.ad_account_publicid','properties_adsaccount_relations.property_publicid', 'properties_adsaccount_relations.property_name', 'properties_adsaccount_relations.platform', 'properties_accounts.customers')
                        ->distinct()
                        ->where('properties_adsaccount_relations.user_id', $user->id)
                        ->where('properties_adsaccount_relations.type', 'PAGE')
                        ->where('properties_adsaccount_relations.platform', $platform)
                        ->whereJsonContains('customers', array($id)) 
                        ->orderBy('properties_adsaccount_relations.property_name')
                        ->get();
                
                if (count($properties) == 0) {
                    $properties = DB::table('properties_adsaccount_relations')
                        ->join('properties_accounts', 'properties_adsaccount_relations.property_publicid', '=', 'properties_accounts.public_id')
                        ->select('properties_adsaccount_relations.ad_account_publicid','properties_adsaccount_relations.property_publicid', 'properties_adsaccount_relations.property_name', 'properties_adsaccount_relations.platform', 'properties_accounts.customers')
                        ->distinct()
                        ->where('properties_adsaccount_relations.user_id', $user->id)
                        ->where('properties_adsaccount_relations.platform', $platform)
                        ->where('properties_adsaccount_relations.type', 'PAGE')
                        ->where('customers', null)
                        ->orderBy('properties_adsaccount_relations.property_name')
                        ->get();
                }

                $list = array();

                foreach ($properties as $value) {
                    $list[] = array("id" => $value->property_publicid, "name" => $value->property_name, "ad_account_public_id"=> $value->ad_account_publicid);
                }

                $properties = $list;
            }
            
            return array('platform' => $platform, 'accounts' => $accounts, 'properties' => $properties, 'pixels'=> $pixels);
        }
        
        return array('platform' => $platform, 'accounts' => [], 'properties' => []);
    }

}