<?php

namespace App\Http\Controllers\Api;

use App\Campaign;
use App\Customer;
use App\Http\Controllers\Controller;
use App\Platform;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('auth');
    }

    protected $db2 = "stats";

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $result = [
            "datos" => [
                "periodo" => "2",
                "frecuencia" => "2",
                "opcionGrafica" => "2",
                "busquedaSitio" => ["1", "2"],
                "busquedaFormato" => ["1", "2"],
                "busquedaCampana" => ["2", "3"],
            ],
        ];

        return $result;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        $datos = json_decode($request->post('datos'));

        $result = ['datos' => $datos];

        return $result;
    }


    public function data_v2(Request $request)
    {

        $user = Auth::user();
        $msj="";

        $types_fields = array(
            "impresions" => "SUM(impressions) as y",
            "spend" => "ROUND(SUM(cost), 2) as y",
            "clicks" => "SUM(clicks) as y",
            "ctr" => "ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS y",
            "ecpm" => "ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS y",
        );

        $filter_date = (isset($request->datos['periodo'])) ? $request->datos['periodo'] : [];
        $typeChart = (isset($request->datos['typeChart'])) ? $request->datos['typeChart'] : [];
        $fields = (isset($request->caoptions)) ? $request->caoptions : "";
        $customers = (isset($request->datos['clients'])) ? $request->datos['clients'] : [];
        $platforms = (isset($request->datos['platforms'])) ? $request->datos['platforms'] : [];
        $campaigns = (isset($request->datos['campaigns'])) ? $request->datos['campaigns'] : [];
        $customers_top = (isset($request->datos['customers_top'])) ? $request->datos['customers_top'] : [];
        $platforms_top = (isset($request->datos['platforms_top'])) ? $request->datos['platforms_top'] : [];
        $campaigns_top = (isset($request->datos['campaigns_top'])) ? $request->datos['campaigns_top'] : [];
        //$periodo_top = (isset($request->datos['periodo_top'])) ? $request->datos['periodo_top'] : [];
        $frecuencia = (isset($request->datos['frecuencia'])) ? $request->datos['frecuencia'] : null;
        $date_picker = (isset($request->datos['datefilter'])) ? $request->datos['datefilter'] : "";
        $channels = (isset($request->datos['channels'])) ? $request->datos['channels'] : [];

        $channels = array(1,2,3,4);

        $label_name = [];

        $label_name = Platform::whereIn("id", $channels)->get();

        // if period is date picker
        $date_picker = array_map('trim', explode('-', $date_picker));

        if ($fields == "") {
            $fields = array("impresions");
        } else {
            $fields = array($fields);
        }

        $date_begin = "";
        $date_end = "";
        $today = date("Y-m-d");
        $date = strtotime($today);
        /**
         * 0 = today,
         * 2 = last 3 days,
         * 7 = last 7 days,
         * null = all,
         * 30 = last 30 days,
         * 1 = yesterday,
         * 29 = this month
         * 31 = last month
         * 364 = this year
         * 365 = last year
         **/
        switch ($filter_date) {
            case "periodo":
                $date_begin = date_format(date_create($date_picker[0]), 'Y-m-d');
                $date_end = date_format(date_create($date_picker[1]), 'Y-m-d');
                break;
            case "week":
                $date = strtotime("monday this week");
                $date_begin = date('Y-m-d', $date);
                break;
            case "lweek":
                $date = strtotime("monday last week");
                $date_begin = date('Y-m-d', $date);
                $date = strtotime("sunday last week");
                $date_end = date('Y-m-d', $date);
                break;
            case "29":
                $date = strtotime("first day of this month");
                $date_begin = date('Y-m-d', $date);
                break;
            case "31":
                $date = strtotime("first day of last month");
                $date_begin = date('Y-m-d', $date);
                $date = strtotime("last day of last month");
                $date_end = date('Y-m-d', $date);
                break;
            case "364":
                $date = strtotime("Jan 1");
                $date_begin = date('Y-m-d', $date);
                break;
            case "365":
                $date = strtotime('-1 year Jan 1');
                $date_begin = date('Y-m-d', $date);
                $date = strtotime('-1 year Dec 31');
                $date_end = date('Y-m-d', $date);
                break;
            case "0":
                $date_begin = date('Y-m-d', $date);
                break;
            case "1":
                $date = strtotime("-1 day", $date);
                $date_begin = date('Y-m-d', $date);
                break;
            case "last2":
                $date = strtotime("-2 day", $date);
                $date_begin = date('Y-m-d', $date);
                break;
            case "2":
                $date = strtotime("-3 day", $date);
                $date_begin = date('Y-m-d', $date);
                break;
            case "7":
                $date = strtotime("-7 day", $date);
                $date_begin = date('Y-m-d', $date);
                $date_end = $today;
                break;
            case "30":
                $date = strtotime("-30 day", $date);
                $date_begin = date('Y-m-d', $date);
                $date_end = $today;
                $msj =  $date_begin . ' a ' . $date_end;
                break;
            default:
                $date = strtotime("-30 day", $date);
                $date_begin = date('Y-m-d', $date);
                break;
        }

        // start.validamos que los datos existan en la DB
        if (isset($customers) && count($customers) > 0) {
            $datos_temporal = Customer::whereIn("public_id", $customers)->where("user_id", $user->id)->get();
            $customers_ids = array();
            foreach ($datos_temporal as $value) {
                $customers_ids[] = $value->id;
            }
        }
        
        if (isset($campaigns) && count($campaigns) > 0) {
            $datos_temporal = Campaign::whereIn("public_id", $campaigns)->where("user_id", $user->id)->get();
            $campaigns_ids = array();
            foreach ($datos_temporal as $value) {
                $campaigns_ids[] = $value->id;
            }
        }

        if (isset($customers_top) && count($customers_top) > 0) {
            $datos_temporal = Customer::whereIn("public_id", $customers_top)->where("user_id", $user->id)->get();
            $customers_ids_top = array();
            foreach ($datos_temporal as $value) {
                $customers_ids_top[] = $value->id;
            }
        }

        if (isset($campaigns_top) && count($campaigns_top) > 0) {
            $datos_temporal = Campaign::whereIn("public_id", $campaigns_top)->where("user_id", $user->id)->get();
            $campaigns_ids_top = array();
            foreach ($datos_temporal as $value) {
                $campaigns_ids_top[] = $value->id;
            }
        }
        // end.validamos que los datos existan en la DB

        $fields_query = "SUM(impressions) as impresions, 
                            ROUND(SUM(cost), 2) as spend,
                            SUM(clicks) as clicks, 
                            ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS ctr,
                            ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS ecpm";

        $results = array();
        
        
        
        $fields_data = DB::connection($this->db2)->table('platform_campana_day');

        // DB::connection($this->db2)->enableQueryLog();
        // building query and getting data for fields ("impressions", "spend", "crt", "ecpm", "clicks")

        if (($date_end != "") && ($date_begin != "")) {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
            $fields_data = $fields_data->where('dia', '<=', $date_end);
        } else if ($date_begin != "") {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
        }

        if (isset($customers_ids) && count($customers_ids) > 0) {
            $fields_data = $fields_data->whereIn('customer_id', $customers_ids);
        }

        if (isset($platforms) && count($platforms) > 0) {
            $fields_data = $fields_data->whereIn('platformid', $platforms);
        }

        if (isset($campaigns_ids) && count($campaigns_ids) > 0) {
            $fields_data = $fields_data->whereIn('campanaroot', $campaigns_ids);
        }

        $fields_data = $fields_data->selectRaw($fields_query)->where("user_id", $user->id)->get();
        // end getting header stats


        // building query and getting data for chart
        $querySTR = "SUM(impressions) as y_impresions, ROUND(SUM(cost), 2) as y_spend, SUM(clicks) as y_clicks, ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS y_ctr, ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS y_ecpm";
        $datos = DB::connection($this->db2);
        $datos = $datos->table('platform_campana_day');

        $datos = $datos->selectRaw(' dia as x,  CONCAT(SUBSTRING(yearweek,5,2),"-",SUBSTRING(yearweek,1,4)) as yearweek,  yearmonth, ' . $querySTR);
        $datos = $datos->groupBy('dia');
            
        if (($date_end != "") && ($date_begin != "")) {
            $datos = $datos->where('dia', '>=', $date_begin);
            $datos = $datos->where('dia', '<=', $date_end);
        } else if ($date_begin != "") {
            $datos = $datos->where('dia', '>=', $date_begin);
        }

        if (isset($customers_ids) && count($customers_ids) > 0) {
            $datos = $datos->whereIn('customer_id', $customers_ids);
        }

        if (isset($platforms) && count($platforms) > 0) {
            $datos = $datos->whereIn('platformid', $platforms);
        }

        if (isset($campaigns_ids) && count($campaigns_ids) > 0) {
            $datos = $datos->whereIn('campanaroot', $campaigns_ids);
        }

    
        $results[] = $datos->where("user_id", $user->id)->orderBy("x")->get();

   
        $result = array(
            "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
        );
               
        $msj .= "count channetls ". count($channels) . ' // ';

        foreach ($channels as $channel) {

                $datos = DB::connection($this->db2)->table('platform_campana_day');
                $datos = $datos->selectRaw(' dia as x,  CONCAT(SUBSTRING(yearweek,5,2),"-",SUBSTRING(yearweek,1,4)) as yearweek,  yearmonth, ' . $querySTR);
                $datos = $datos->groupBy('dia');
                        
                if (($date_end != "") && ($date_begin != "")) {
                    $datos = $datos->where('dia', '>=', $date_begin);
                    $datos = $datos->where('dia', '<=', $date_end);
                } else if ($date_begin != "") {
                    $datos = $datos->where('dia', '>=', $date_begin);
                }

                if (isset($customers_ids) && count($customers_ids) > 0) {
                    $datos = $datos->whereIn('customer_id', $customers_ids);
                }

                $datos = $datos->where('platformid', $channel);
            
                if (isset($campaigns_ids) && count($campaigns_ids) > 0) {
                    $datos = $datos->whereIn('campanaroot', $campaigns_ids);
                }

                $result['data_channels'][$channel]['data']  = $datos->where("user_id", $user->id)->orderBy("x")->get();
                $result['data_channels'][$channel]['platform'] = Platform::find($channel); 

        }

        $array = [
            "result" => $result,
            "fields_data" => $fields_data,
            //"queris" => DB::connection($this->db2)->getQueryLog(),
            "debug"=>$msj,
        ];

        return json_encode($array);
    }

    public function data(Request $request)
    {

        $user = Auth::user();

        $types_fields = array(
            "impresions" => "SUM(impressions) as y",
            "spend" => "ROUND(SUM(cost), 2) as y",
            "clicks" => "SUM(clicks) as y",
            "ctr" => "ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS y",
            "ecpm" => "ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS y",
        );

        $filter_date = (isset($request->datos['periodo'])) ? $request->datos['periodo'] : [];
        $typeChart = (isset($request->datos['typeChart'])) ? $request->datos['typeChart'] : [];
        $fields = (isset($request->caoptions)) ? $request->caoptions : "";
        $customers = (isset($request->datos['customers'])) ? $request->datos['customers'] : [];
        $platforms = (isset($request->datos['platforms'])) ? $request->datos['platforms'] : [];
        $campaigns = (isset($request->datos['campaigns'])) ? $request->datos['campaigns'] : [];

        $customers_top = (isset($request->datos['customers_top'])) ? $request->datos['customers_top'] : [];
        $platforms_top = (isset($request->datos['platforms_top'])) ? $request->datos['platforms_top'] : [];
        $campaigns_top = (isset($request->datos['campaigns_top'])) ? $request->datos['campaigns_top'] : [];
        $periodo_top = (isset($request->datos['periodo_top'])) ? $request->datos['periodo_top'] : [];
        $frecuencia = (isset($request->datos['frecuencia'])) ? $request->datos['frecuencia'] : null;

        $date_picker = (isset($request->datos['datefilter'])) ? $request->datos['datefilter'] : "";
        $channels = (isset($request->datos['channels'])) ? $request->datos['channels'] : [];

        $label_name = [];

        $label_name = Platform::whereIn("id", $channels)
            ->get()
            ->pluck("name");

        // if period is date picker
        $date_picker = array_map('trim', explode('-', $date_picker));

        if ($fields == "") {
            $fields = array("impresions");
        } else {
            $fields = array($fields);
        }

        $date_begin = "";
        $date_end = "";
        $today = date("Y-m-d");
        $date = strtotime($today);
        /**
         * 0 = today,
         * 2 = last 3 days,
         * 7 = last 7 days,
         * null = all,
         * 30 = last 30 days,
         * 1 = yesterday,
         * 29 = this month
         * 31 = last month
         * 364 = this year
         * 365 = last year
         **/
        switch ($filter_date) {
            case "periodo":
                $date_begin = date_format(date_create($date_picker[0]), 'Y-m-d');
                $date_end = date_format(date_create($date_picker[1]), 'Y-m-d');
                break;
            case "week":
                $date = strtotime("monday this week");
                $date_begin = date('Y-m-d', $date);
                break;
            case "lweek":
                $date = strtotime("monday last week");
                $date_begin = date('Y-m-d', $date);
                $date = strtotime("sunday last week");
                $date_end = date('Y-m-d', $date);
                break;
            case "29":
                $date = strtotime("first day of this month");
                $date_begin = date('Y-m-d', $date);
                break;
            case "31":
                $date = strtotime("first day of last month");
                $date_begin = date('Y-m-d', $date);
                $date = strtotime("last day of last month");
                $date_end = date('Y-m-d', $date);
                break;
            case "364":
                $date = strtotime("Jan 1");
                $date_begin = date('Y-m-d', $date);
                break;
            case "365":
                $date = strtotime('-1 year Jan 1');
                $date_begin = date('Y-m-d', $date);
                $date = strtotime('-1 year Dec 31');
                $date_end = date('Y-m-d', $date);
                break;
            case "0":
                $date_begin = date('Y-m-d', $date);
                break;
            case "1":
                $date = strtotime("-1 day", $date);
                $date_begin = date('Y-m-d', $date);
                break;
            case "last2":
                $date = strtotime("-2 day", $date);
                $date_begin = date('Y-m-d', $date);
                break;
            case "2":
                $date = strtotime("-3 day", $date);
                $date_begin = date('Y-m-d', $date);
                break;
            case "7":
                $date = strtotime("-7 day", $date);
                $date_begin = date('Y-m-d', $date);
                break;
            case "30":
            default:
                $date = strtotime("-30 day", $date);
                $date_begin = date('Y-m-d', $date);
                break;
        }

        $date_begin_top = "";
        $date_end_top = "";
        $today = date("Y-m-d");
        $date = strtotime($today);

        switch ($periodo_top) {
            case "all":
                $date_begin = "";
                break;
            case "week":
                $date = strtotime("monday this week");
                $date_begin_top = date('Y-m-d', $date);
                break;
            case "lweek":
                $date = strtotime("monday last week");
                $date_begin_top = date('Y-m-d', $date);
                $date = strtotime("sunday last week");
                $date_end_top = date('Y-m-d', $date);
                break;
            case "month":
                $date = strtotime("first day of this month");
                $date_begin_top = date('Y-m-d', $date);
                break;
            case "lmonth":
                $date = strtotime("first day of last month");
                $date_begin_top = date('Y-m-d', $date);
                $date = strtotime("last day of last month");
                $date_end_top = date('Y-m-d', $date);
                break;
            case "today":
                $date_begin = date('Y-m-d', $date);
                break;
            case "yesterday":
                $date = strtotime("-1 day", $date);
                $date_begin_top = date('Y-m-d', $date);
                break;
            case "last2":
                $date = strtotime("-2 day", $date);
                $date_begin_top = date('Y-m-d', $date);
                break;
            case "last7":
                $date = strtotime("-7 day", $date);
                $date_begin_top = date('Y-m-d', $date);
                break;
            case "last30":
                $date = strtotime("-30 day", $date);
                $date_begin_top = date('Y-m-d', $date);
                break;
            default:
                break;
        }

        if (isset($customers) && count($customers) > 0) {

            $datos_temporal = Customer::whereIn("id", $customers)
                ->where("user_id", $user->id)
                ->get();

            $customers_ids = array();
            foreach ($datos_temporal as $value) {
                $customers_ids[] = $value->id;
            }

        }

        if (isset($campaigns) && count($campaigns) > 0) {

            $datos_temporal = Campaign::whereIn("id", $campaigns)
                ->where("user_id", $user->id)
                ->get();

            $campaigns_ids = array();
            foreach ($datos_temporal as $value) {
                $campaigns_ids[] = $value->id;
            }
        }

        if (isset($customers_top) && count($customers_top) > 0) {

            $datos_temporal = Customer::whereIn("id", $customers_top)
                ->where("user_id", $user->id)
                ->get();
            $customers_ids_top = array();
            foreach ($datos_temporal as $value) {
                $customers_ids_top[] = $value->id;
            }
        }

        if (isset($campaigns_top) && count($campaigns_top) > 0) {
            $datos_temporal = Campaign::whereIn("id", $campaigns_top)
                ->where("user_id", $user->id)
                ->get();

            $campaigns_ids_top = array();
            foreach ($datos_temporal as $value) {
                $campaigns_ids_top[] = $value->id;
            }
        }

        $fields_query = "   SUM(impressions) as impresions,
                            ROUND(SUM(cost), 2) as spend,
                            SUM(clicks) as clicks,
                            ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS ctr,
                            ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS ecpm";

        $results = array();

        $fields_data = DB::connection($this->db2)->table('platform_campana_day');


        // building query and getting data for fields ("impressions", "spend", "crt", "ecpm", "clicks")

        if (($date_end != "") && ($date_begin != "")) {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
            $fields_data = $fields_data->where('dia', '<=', $date_end);
        } else if ($date_begin != "") {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
        }

        if (isset($customers_ids) && count($customers_ids) > 0) {
            $fields_data = $fields_data->whereIn('customer_id', $customers_ids);
        }

        if (isset($platforms) && count($platforms) > 0) {
            $fields_data = $fields_data->whereIn('platformid', $platforms);
        }

        if (isset($campaigns_ids) && count($campaigns_ids) > 0) {
            $fields_data = $fields_data->whereIn('campanaroot', $campaigns_ids);
        }

        //Filtros TOP

        if (($date_end_top != "") && ($date_begin_top != "")) {
            $fields_data = $fields_data->where('dia', '>=', $date_begin_top);
            $fields_data = $fields_data->where('dia', '<=', $date_end_top);
        } else if ($date_begin_top != "") {
            $fields_data = $fields_data->where('dia', '>=', $date_begin_top);
        }

        /*if(isset($customers_ids_top) && count($customers_ids_top) > 0){
        $datos = $datos->whereIn('customer_id', $customers_ids_top);
        }

        if(isset($platforms_top) && count($platforms_top) > 0){
        $datos = $datos->whereIn('plataforma', $platforms_top);
        }

        if(isset($campaigns_ids_top) && count($campaigns_ids_top) > 0){
        $datos = $datos->whereIn('campanaid', $campaigns_ids_top);
        }*/

        $fields_data = $fields_data
            ->selectRaw($fields_query)
            ->where("user_id", $user->id)
            ->get();

        // building query and getting data for chart
        foreach ($fields as $value) {

            $datos = DB::connection($this->db2)
                ->table('platform_campana_day');

            if ($typeChart == 'radialBar') {
                $datos = $datos->selectRaw($types_fields[$value]);
            } else {

                if (intval($frecuencia) === 1) {
                    $datos = $datos->selectRaw('dia as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('dia');
                } else if (intval($frecuencia) === 7) {
                    $datos = $datos->selectRaw(' CONCAT(SUBSTRING(yearweek,5,2),"\'",SUBSTRING(yearweek,1,4)) as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearweek');
                } else if (intval($frecuencia) === 30) {
                    $datos = $datos->selectRaw('yearmonth as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearmonth');
                    if ($filter_date == "30") {
                        //$date = strtotime("Jan 1");
                        $date = strtotime("- 3 month");
                        $date_begin = date('Y-m-d', $date);
                    }
                }

            }

            if (($date_end != "") && ($date_begin != "")) {
                $datos = $datos->where('dia', '>=', $date_begin);
                $datos = $datos->where('dia', '<=', $date_end);
            } else if ($date_begin != "") {
                $datos = $datos->where('dia', '>=', $date_begin);
            }

            if (isset($customers_ids) && count($customers_ids) > 0) {
                $datos = $datos->whereIn('customer_id', $customers_ids);
            }

            if (isset($platforms) && count($platforms) > 0) {
                $datos = $datos->whereIn('platformid', $platforms);
            }

            if (isset($campaigns_ids) && count($campaigns_ids) > 0) {
                $datos = $datos->whereIn('campanaroot', $campaigns_ids);
            }

            //Filtros TOP

            if (($date_end_top != "") && ($date_begin_top != "")) {
                $datos = $datos->where('dia', '>=', $date_begin_top);
                $datos = $datos->where('dia', '<=', $date_end_top);
            } else if ($date_begin_top != "") {
                $datos = $datos->where('dia', '>=', $date_begin_top);
            }

            /*if(isset($customers_ids_top) && count($customers_ids_top) > 0){
            $datos = $datos->whereIn('customer_id', $customers_ids_top);
            }

            if(isset($platforms_top) && count($platforms_top) > 0){
            $datos = $datos->whereIn('plataforma', $platforms_top);
            }

            if(isset($campaigns_ids_top) && count($campaigns_ids_top) > 0){
            $datos = $datos->whereIn('campanaid', $campaigns_ids_top);
            }*/

            //$datos->dd();

            $results[] = $datos
                ->where("user_id", $user->id)
                ->orderBy("x")
                ->get();
        }

        switch (count($results)) {
            case 1;

                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0),
                            ),
                            "labels" => array($fields[0]),
                        ),
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0],
                            ),
                        ),
                    );
                }
                break;
            case 2:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0),
                                ((isset($results[1][0]->y)) ? $results[1][0]->y : 0),
                            ),
                            "labels" => array($fields[0], $fields[1]),
                        ),
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0],
                            ),
                            array(
                                "data" => ((count($results[1]) > 0) ? $results[1] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[1],
                            ),
                        ),
                    );
                }
                break;
            default:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(0),
                            "labels" => array("impressions"),
                        ),
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            "data" => json_decode('[{"x":" ","y":"0"}]'),
                            "name" => "impressions",
                        ),
                    );
                }
                break;
        }

        if (count($channels) === 1) {
            foreach ($channels as $channel) {
                $platforms = [];
                $platforms[] = $channel;

                // building query and getting data for chart
                foreach ($fields as $value) {

                    $datos = DB::connection($this->db2)
                        ->table('platform_campana_day');

                    if ($typeChart == 'radialBar') {
                        $datos = $datos->selectRaw($types_fields[$value]);
                    } else {

                        if (intval($frecuencia) === 1) {
                            $datos = $datos->selectRaw('dia as x , ' . $types_fields[$value]);
                            $datos = $datos->groupBy('dia');
                        } else if (intval($frecuencia) === 7) {
                            $datos = $datos->selectRaw('yearweek as x , ' . $types_fields[$value]);
                            $datos = $datos->groupBy('yearweek');
                        } else if (intval($frecuencia) === 30) {
                            $datos = $datos->selectRaw('yearmonth as x , ' . $types_fields[$value]);
                            $datos = $datos->groupBy('yearmonth');
                        }

                    }

                    if (($date_end != "") && ($date_begin != "")) {
                        $datos = $datos->where('dia', '>=', $date_begin);
                        $datos = $datos->where('dia', '<=', $date_end);
                    } else if ($date_begin != "") {
                        $datos = $datos->where('dia', '>=', $date_begin);
                    }

                    if (isset($customers_ids) && count($customers_ids) > 0) {
                        $datos = $datos->whereIn('customer_id', $customers_ids);
                    }

                    if (isset($platforms) && count($platforms) > 0) {
                        $datos = $datos->whereIn('platformid', $platforms);
                    }

                    if (isset($campaigns_ids) && count($campaigns_ids) > 0) {
                        $datos = $datos->whereIn('campanaroot', $campaigns_ids);
                    }

                    //Filtros TOP

                    if (($date_end_top != "") && ($date_begin_top != "")) {
                        $datos = $datos->where('dia', '>=', $date_begin_top);
                        $datos = $datos->where('dia', '<=', $date_end_top);
                    } else if ($date_begin_top != "") {
                        $datos = $datos->where('dia', '>=', $date_begin_top);
                    }

                    $results_channels[] = $datos
                        ->where("user_id", $user->id)
                        ->orderBy("x")
                        ->pluck("y");
                }

            }

        } else if (count($channels) > 1) {

            foreach ($channels as $channel) {
                $platforms = [];
                $platforms[] = $channel;

                // building query and getting data for chart
                foreach ($fields as $value) {

                    $datos = DB::connection($this->db2)
                        ->table('platform_campana_day');

                    if ($typeChart == 'radialBar') {
                        $datos = $datos->selectRaw($types_fields[$value]);
                    } else {

                        if (intval($frecuencia) === 1) {
                            $datos = $datos->selectRaw('dia as x , ' . $types_fields[$value]);
                            $datos = $datos->groupBy('dia');
                        } else if (intval($frecuencia) === 7) {
                            $datos = $datos->selectRaw('yearweek as x , ' . $types_fields[$value]);
                            $datos = $datos->groupBy('yearweek');
                        } else if (intval($frecuencia) === 30) {
                            $datos = $datos->selectRaw('yearmonth as x , ' . $types_fields[$value]);
                            $datos = $datos->groupBy('yearmonth');
                        }

                    }

                    if (($date_end != "") && ($date_begin != "")) {
                        $datos = $datos->where('dia', '>=', $date_begin);
                        $datos = $datos->where('dia', '<=', $date_end);
                    } else if ($date_begin != "") {
                        $datos = $datos->where('dia', '>=', $date_begin);
                    }

                    if (isset($customers_ids) && count($customers_ids) > 0) {
                        $datos = $datos->whereIn('customer_id', $customers_ids);
                    }

                    if (isset($platforms) && count($platforms) > 0) {
                        $datos = $datos->whereIn('platformid', $platforms);
                    }
                    /**
                     * TODO:Nenad: "camapanaroot" to be checked, old one was "campanaid"
                     **/
                    if (isset($campaigns_ids) && count($campaigns_ids) > 0) {
                        $datos = $datos->whereIn('campanaroot', $campaigns_ids);
                    }

                    //Filtros TOP

                    if (($date_end_top != "") && ($date_begin_top != "")) {
                        $datos = $datos->where('dia', '>=', $date_begin_top);
                        $datos = $datos->where('dia', '<=', $date_end_top);
                    } else if ($date_begin_top != "") {
                        $datos = $datos->where('dia', '>=', $date_begin_top);
                    }

                    $results_channels[] = $datos
                        ->where("user_id", $user->id)
                        ->orderBy("x")
                        ->pluck("y");
                }

            }
        }

        $result_channels = [];
        if (isset($results_channels) && $results_channels !== []) {
            foreach ($results_channels as $index => $results_channel) {
                if ($typeChart == 'radialBar') {
                    $result_channels[] = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results_channel[0]->y)) ? $results_channel[0]->y : 0),
                            ),
                            "labels" => array($fields[0]),
                        ),
                        "channelName" => $label_name[$index],
                    );
                } else {
                    $result_channels[] = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results_channel) > 0) ? $results_channel : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0],
                            ),
                        ),
                        "channelName" => $label_name[$index],
                    );
                }
            }
        }


        $array = [
            "result" => $result,
            "fields_data" => $fields_data,
            "result_channels" => $result_channels,
        ];

        return $array;
    }

    /**
     * Show campaings by account.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    //public function sitios( Request $request )
    public function entidades(string $entidad = 'global', Request $request)
    {
        $retorno = [];

        switch ($entidad) {
            case 'customers':
                $retorno['customers'] = \DB::table('customers')->where([['user_id', $request->user()->id]])->select('public_id as id', 'name as nameValue')->get();
                break;
            case 'campaigns':
                $retorno['campaigns'] = \DB::table('campaigns')->where([['user_id', $request->user()->id]])->select('public_id as id', 'name as nameValue', 'customer_id as customer_id', 'status as status', 'plataformas as platforms')->get();
                break;
            case 'platforms':
                $retorno['platforms'] = \DB::table('platforms')->select('id as id', 'name as nameValue')->get();
                break;
            /*** the old thing is this: $retorno['campaigns'] = \DB::table('campaigns')->where([['user_id', $request->user()->id],])->select('id as id', 'name as nameValue', 'customer_id as customer_id', 'status as status', 'plataformas as platforms')->get(); **/
            case 'global':
                $retorno['platforms'] = \DB::table('platforms')->select('id as id', 'name as nameValue')->get();
                //$retorno['campaigns'] = null;
                //$retorno['customers'] = \DB::table('customers')->where([['user_id', $request->user()->id]])->select('public_id as id', 'name as nameValue')->get();
        }

        return $retorno;
    }

    /**
     * Show campaings by account.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function formatos(Request $request)
    {
        $result = ['datos' => [["id" => "1", "nameValue" => "Formato1"], ["id" => "2", "nameValue" => "Formato2"], ["id" => "3", "nameValue" => "Formato3"]]];

        return $result;
    }

    /**
     * Show campaings by account.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function campanas(Request $request)
    {
        $result = ['datos' => [["id" => "1", "nameValue" => "Campana1"], ["id" => "2", "nameValue" => "Campana2"], ["id" => "3", "nameValue" => "Campana3"]]];

        return $result;
    }

    /**
     * Show campaings by account.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function impresiones(int $id, Request $request)
    {
        $result = ['datos' => [32, 44, 31, 41, 22, 36, 88, 27, 15]];

        return $result;
    }

    /**
     * Show campaings by account.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ingresos(int $id, Request $request)
    {
        $result = ['datos' => [2, 4, 31, 41, 60, 88, 90, 115, 80]];

        return $result;
    }

    /**
     * Show campaings by account.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ecpm(int $id, Request $request)
    {
        $result = ['datos' => [32, 20, 14, 31, 21, 40, 55, 73, 90]];

        return $result;
    }

    /**
     * Show campaings by account.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function clics(int $id, Request $request)
    {
        $result = ['datos' => [32, 10, 4, 31, 41, 40, 45, 50, 60]];

        return $result;
    }

    /**
     * Show campaings by account.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ctr(int $id, Request $request)
    {
        $result = ['datos' => [2, 4, 15, 12, 25, 20, 40, 50, 65]];

        return $result;
    }

}