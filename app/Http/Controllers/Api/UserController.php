<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Permisos;
use App\Role;
use App\User;
use App\Appsettings;
//use App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use \Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $keyword = '';
        if (!is_null($request->get('search'))) {$keyword = $request->get('search')['value'];}

        $users = User::with('roles')
            ->where(function ($query) use ($keyword) {$query->whereRaw("name like  '%{$keyword}%' or email like '%{$keyword}%'");})
            ->paginate($limit, ['*'], 'page', $page);

        $users->getCollection()->transform(function ($value) {
            $data = [
                'id' => $value->public_id,
                'name' => $value->name,
                'email' => $value->email,
                'rol' => implode(', ', $value->roles->pluck('description')->toArray()),
            ];
            return $data;
        });
        $result = [
            "draw" => $request->input('draw') ?: 1,
            "recordsTotal" => $users->total(),
            "recordsFiltered" => $users->total(),
            "data" => $users->items(),
            "length" => 10,
        ];
        return $result;
    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->json()->all(), [
            'name' => 'string|max:255',
            'email' => 'required|email|min:3|max:255|string|unique:users',
            'password' => 'required',
            'consumo' => 'integer',
            'rol' => 'exists:roles,name',
            'plan' => 'string|max:255|in:cbdemo_free,cbdemo_grow',
            'permisos.plataformas' => 'array',
            'permisos.plataformas.*' => 'exists:platforms,name',
            'permisos.num_plataformas' => 'integer',
            'permisos.num_campanas' => 'integer',
            'permisos.cross_plataformas' => 'boolean',
            'permisos.crear_usuarios' => 'boolean',
            'permisos.compartir_reportes' => 'boolean',
            'permisos.importe_maximo_mes' => 'integer',
        ]);

        if ($validator->fails()) {
            $response = array(
                "status" => false,
                "message" => $validator->messages(),
            );
            return $response;
        }

        $json = $request->json()->all();

        $user = User::create([
            'name' => ((isset($json['name'])) ? $json['name'] : ""),
            'email' => $json['email'],
            'consumo' => ((isset($json['consumo'])) ? $json['consumo'] : 0),
            'plan' => ((isset($json['plan'])) ? $json['plan'] : "cbdemo_free"),
            'password' => ((isset($json['password'])) ?  bcrypt($json['password']) :bcrypt(time().rand ) ),
        ]);

        // $user->hashuser = $user->public_id;
        $user->save();

        $rol = Role::where('name', 'Account Manager')->first();
      
           
        //  $metadata = Appsettings::find( md5( 'plan:'.  $user->plan  ) );
        /**  $metadata = Appsettings::select('value', 'keyname')
                           ->where('id', md5( 'plan:'.  $user->plan  ) )->first()
                           ->get();**/
          $metadata = Appsettings::find(md5( 'plan:'.  $user->plan  ) , [ 'value', 'keyname'] );
      
      //  $user->roles()->attach($rol->id);
        $user_id = $user->id;
          /**      
        if (isset($json['permisos']) || true) {
            $permisos = Permisos::create([
                'user_id' => $user_id,
                'plataformas' => ((isset($json['permisos']['plataformas'])) ? json_encode($json['permisos']['plataformas']) : "[]"),
                'num_plataformas' => ((isset($json['permisos']['num_plataformas'])) ? $json['permisos']['num_plataformas'] : 0),
                'num_campanas' => ((isset($json['permisos']['num_campanas'])) ? $json['permisos']['num_campanas'] : 0),
                'cross_plataformas' => ((isset($json['permisos']['cross_plataformas'])) ? $json['permisos']['cross_plataformas'] : 0),
                'crear_usuarios' => ((isset($json['permisos']['crear_usuarios'])) ? $json['permisos']['crear_usuarios'] : 0),
                'compartir_reportes' => ((isset($json['permisos']['compartir_reportes'])) ? $json['permisos']['compartir_reportes'] : 0),
                'importe_maximo_mes' => ((isset($json['permisos']['importe_maximo_mes'])) ? $json['permisos']['importe_maximo_mes'] : 0),
            ]);
        }
            **/        
      
        $user = User::find($user->id);
        //setcookie("id_user", $user->hashuser, time() + 3600);
        setcookie("id_user", $user->public_id, time() + 3600);

        $user = User::find($user->id);

        $param = array(
            "id" => $user->subscriptionid,
            "plan_id" => $user->plan,
            "meta_data" => $metadata->value ,            
            "customer" => array(
                "id" => $user->public_id,
                "first_name" => $user->name,
                "email" => $user->email,
         //       "meta_data" => json_encode( ["quiensoy_usecustomerr"=> "metadata customer"] ),
            ),    
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://adsconcierge.chargebee.com/api/v2/subscriptions");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "live_UhGIHjzvcdOWTwEU28Rt1Mk0khGjVGk40");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //$server_output = curl_exec($ch);
        $server_output = json_decode(curl_exec($ch));

        //dd($server_output);
        curl_close($ch);

        $response = array(
            "status" => true,
            "message" => "User successfully created",
            "url_redirect" => route('connections') 
       //     , "chargebee_data" => $server_output
         //  , "metadata" => $metadata
        );

        return $response;
    }

}
