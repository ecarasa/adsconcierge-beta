<?php

namespace App\Http\Controllers\Api;

use App\Campaign_platform;
use App\Campaign_platform_atomo;
use App\Creatividades;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;
use App\Campaign;
use App\Creativity;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Log;

use App\Events\newCronEvent;
use App\Events\newCronJobEvent;


class AccountController extends Controller
{
    
    public function __construct(){
       $this->middleware('auth');
       DB::enableQueryLog();
    }

    protected $db2 = "mysql2";

    protected $rulesCustomerUpdate = [
        "name_input" => "required|string",
        "spend_input" => "required|numeric",
        "impressions_input" => "required|numeric",
        "clicks_input" => "required|numeric",
        "cpc_input" => "required|numeric",
        "cpm_input" => "required|numeric",
        "ctr_input" => "required|numeric",
        "conversions_input" => "required|numeric"
    ];

    protected $rulesCampaignUpdate = [
        "name_input" => "required|string",
        "budget_input" => "required|numeric",
        "spend_input" => "required|numeric",
        "impressions_input" => "required|numeric",
        "clicks_input" => "required|numeric",
        "cpc_input" => "required|numeric",
        "cpm_input" => "required|numeric",
        "ctr_input" => "required|numeric",
        "conversions_input" => "required|numeric"
        ];

    protected $rulesCampaignPlatformUpdate = [
        "name_input" => "required|string",
        "budget_input" => "required|numeric",
        "spend_input" => "required|numeric",
        "impressions_input" => "required|numeric",
        "clicks_input" => "required|numeric",
        "cpc_input" => "required|numeric",
        "cpm_input" => "required|numeric",
        "ctr_input" => "required|numeric",
        "conversions_input" => "required|numeric"
    ];

    protected $rulesCampaignPlatformAtomoUpdate = [
        "name_input" => "required|string",
        "budget_input" => "required|numeric",
        "spend_input" => "required|numeric",
        "impressions_input" => "required|numeric",
        "clicks_input" => "required|numeric",
        "cpc_input" => "required|numeric",
        "cpm_input" => "required|numeric",
        "ctr_input" => "required|numeric",
        "conversions_input" => "required|numeric"
    ];

    protected $rulesCreatividadesUpdate = [
        "name_input" => "required|string",
        "budget_input" => "required|numeric",
        "impressions_input" => "required|numeric",
        "clicks_input" => "required|numeric",
        "ctr_input" => "required|numeric",
    ];

    protected $ads_Calculations = [
        "ctr" => "COALESCE(ROUND( sum(stats.clicks) / sum(stats.impressions) , 2),0) as ctr",
        "cpc" => "COALESCE(ROUND( sum(stats.cost) / sum(stats.impressions) , 2),0) as cpc",
        "cpm" => "COALESCE(ROUND( ( sum(stats.cost) / sum(stats.impressions)) * 1000  , 2) ,0) as cpm",
        "cpa" => "COALESCE(ROUND(  sum(stats.cost) / sum(stats.conversions)  , 2) ,0) as cpa",
        "cr" =>  "COALESCE(ROUND(  sum(stats.conversions) / sum(stats.clicks)  , 2) ,0) as cr",
    ];


    private function getDateFromRequestPeriod( $requestPeriod, $fr){

        $date_begin = "";
        $date_end = "";
        $today = date("Y-m-d");
        $date = strtotime($today);

        switch ( $requestPeriod ) {
            case "0": // today
                $date_begin = date('Y-m-d', $date);
                $date_end = date('Y-m-d', $date);
                break;
            case "2": // last 3 days
                $date = strtotime("-3 day", $date);
                $date_begin = date('Y-m-d', $date);
                $date_end = date('Y-m-d', $date);
                break;
            case "7": //last 7 days
                $date = strtotime("-7 day", $date);
                $date_begin = date('Y-m-d', $date);
                $date_end = date('Y-m-d', $date);
                break;

            case "30": // last 30 days
                $date = strtotime("-30 day", $date);
                $date_begin = date('Y-m-d', $date);
                $date_end = $today;
                break;

            case "1": // yesterday
                $date = strtotime("-1 day", $date);
                $date_begin = date('Y-m-d', $date);
                $date_end = date('Y-m-d', $date);
                break;

            case "29": // this month
                $date = strtotime("first day of this month");
                $date_begin = date('Y-m-d', $date);
                $date_end = date('Y-m-d', $date);
                break;

            case "31": //  last month
                $date = strtotime("first day of last month");
                $date_begin = date('Y-m-d', $date);
                $date = strtotime("last day of last month");
                $date_end = date('Y-m-d', $date);
                break;

            case "364"://this year
                $date = strtotime("Jan 1");
                $date_begin = date('Y-m-d', $date);
                $date = strtotime("last day of this year");
                $date_end = date('Y-m-d', $date);
                break;

            case "365"://last year
                $date = strtotime('-1 year Jan 1');
                $date_begin = date('Y-m-d', $date);
                $date = strtotime('-1 year Dec 31');
                $date_end = date('Y-m-d', $date);
                break;

            default:
                $date = strtotime("-30 day", $date);
                $date_begin = date('Y-m-d', $date);
                break;
        }


        if ($fr == 'start'){
            return $date_begin;
        }else if($fr == 'end'){
            return $date_end;
        }else{
            return false;
        }

    }

    //LIST CAMPAIGNS WITH CUSTOMERS
    // CHECK
    public function campaignsList(Request $request){


        $user = Auth::user();

        $limit = is_null($request->get('length') ) ? 100 :$request->get('length');
        $offset = $request->input('start') ? : 1;
        $page = intval( $offset / $limit) + 1;
        $keyword = '';

    
        $accounts = DB::table('campaigns')                               
                        ->join('customers','campaigns.customer_id', 'customers.id')
                        ->leftjoin('adsconcierge_stats.platform_campana_day as stats', 'campaigns.id', '=', 'stats.campanaroot')
                        ->where('campaigns.user_id', $user->id)
                        //->where('campaigns.public_id', 'IS NOT ', null)
                        ->select( 
                            'campaigns.public_id as id',
                            'campaigns.name as campaign',
                            'customers.name as customer', 
                            'campaigns.status',    
                            DB::raw('ROUND(sum(stats.cost),2) as spend'),
                            DB::raw('sum(stats.impressions) as impressions'),
                            DB::raw('sum(stats.reach) as reach'),
                            DB::raw('sum(stats.clicks) as clicks'),
                            DB::raw('sum(stats.video_views) as video_views'),
                            DB::raw('sum(stats.engagements) as engagements'),
                            DB::raw('sum(stats.conversions) as conversions'),
                            DB::raw( $this->ads_Calculations['ctr'] ),
                            DB::raw( $this->ads_Calculations['cpc'] ),
                            DB::raw( $this->ads_Calculations['cpm'] ),
                            DB::raw( $this->ads_Calculations['cr']  ),
                            DB::raw( $this->ads_Calculations['cpa'] )
                        )
                        ->groupBy('campaigns.id')
                        ->get();

        if( !is_null( $request->get('search') )  ){
            $keyword = $request->get('search')['value'];
        }

        $result = [
            "draw"              => $request->input('draw') ? : 1,
            "recordsTotal"      => count($accounts),
            "recordsFiltered"   => count($accounts),
            // "queries"=> DB::getQueryLog(),
            "data"              => $accounts->toArray()
        ];

        return $result;


    }

    // LIST CUSTOMERS
    // OK
    public function customersList( Request $request )
    {

        DB::enableQueryLog();

        $user = Auth::user();

        $limit = is_null($request->get('length') ) ? 100 :$request->get('length');
        $offset = $request->input('start') ? : 1;
        $page = intval( $offset / $limit) + 1;
        $keyword = '';
        
        $accounts = DB::select("SELECT 
                                    customers.public_id AS id,
                                    customers.name AS customer,
                                    customers.status AS status,
                                    customers.isdefault AS isdefault,
                                    stats_campana.*
                                FROM
                                    app_thesoci_9c37.customers
                                        LEFT JOIN
                                    (SELECT 
                                        `stats`.`customer_id`,
                                        COALESCE(ROUND(SUM(stats.cost), 2), 0) AS spend,
                                        COALESCE(SUM(stats.impressions), 0) AS impressions,
                                        COALESCE(SUM(stats.reach), 0) AS reach,
                                        COALESCE(SUM(stats.clicks), 0) AS clicks,
                                        COALESCE(SUM(stats.video_views), 0) AS video_views,
                                        COALESCE(SUM(stats.engagements), 0) AS engagements,
                                        COALESCE(SUM(stats.conversions), 0) AS conversions,
                                        COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                        COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                        COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                        COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr,
                                        COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa
                                    FROM
                                        `adsconcierge_stats`.`platform_campana_day` AS `stats`
                                    WHERE
                                        stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                        AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                    GROUP BY `customer_id`) AS stats_campana ON customers.id = stats_campana.customer_id
                                WHERE customers.user_id = ?", [$user->id]);

        if( !is_null( $request->get('search') )  ){
            $keyword = $request->get('search')['value'];
        }

        $result = [
            "draw"              => $request->input('draw') ? : 1,
            "recordsTotal"      => count($accounts),
            "recordsFiltered"   => count($accounts),
            "queries"=> DB::getQueryLog(),
            "data"              => $accounts
        ];

        return $result;
    }
    
    // LIST campaigns by customers
    //.OOK
    public function customerCampaignsList( $customer_public_id, Request $request )
    {
        
        DB::enableQueryLog();
        $limit = is_null($request->get('length') ) ? 10 :$request->get('length');
        $offset = $request->input('start') ? : 1;
        $page = intval( $offset / $limit) + 1;
        
        $customerSelected = Customer::where('public_id', $customer_public_id)->get();

        /*      $results = DB::table('campaigns')
                    ->leftjoin('adsconcierge_stats.platform_campana_day as stats', 'campaigns.customer_id', '=', 'stats.customer_id')
                    ->where('campaigns.user_id', $request->user()->id )
                    ->where('campaigns.customer_id', $customerSelected[0]->id)
                    ->select(
                        'campaigns.public_id as public_id',
                        'campaigns.name as name',
                        'campaigns.budget as budget',
                        'campaigns.status as status',
                        DB::raw('ROUND(sum(stats.cost),2) as spent'),
                        DB::raw('sum(stats.impressions) as impressions'),
                        DB::raw('sum(stats.reach) as reach'),
                        DB::raw('sum(stats.clicks) as clicks'),
                        DB::raw('sum(stats.video_views) as video_views'),
                        DB::raw('sum(stats.engagements) as engagements'),
                        DB::raw('sum(stats.conversions) as conversions'),
                        DB::raw( $this->ads_Calculations['ctr'] ),
                        DB::raw( $this->ads_Calculations['cpc'] ),
                        DB::raw( $this->ads_Calculations['cpm'] ),
                        DB::raw( $this->ads_Calculations['cr']  ),
                        DB::raw( $this->ads_Calculations['cpa'] )
                    )
                    ->where('stats.dia','>=', $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') )
                    ->where('stats.dia','<=', $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') )
                    ->groupBy('campaigns.id')
                    ->get(); */


        $res = DB::select("SELECT 
                                campaigns.id,
                                campaigns.name,
                                campaigns.public_id,
                                campaigns.budget,
                                campaigns.status,
                                campana_stats.conversions,
                                campana_stats.spent,
                                campana_stats.impressions,
                                campana_stats.reach,
                                campana_stats.clicks,
                                campana_stats.video_views,
                                campana_stats.engagements,
                                campana_stats.conversions,
                                campana_stats.ctr,
                                campana_stats.cpc,
                                campana_stats.cpm,
                                campana_stats.cpa,
                                campana_stats.cr
                            FROM
                                app_thesoci_9c37.campaigns
                                    LEFT JOIN
                                (SELECT 
                                        stats.campanaroot,
                                        ROUND(SUM(stats.cost), 2) AS spent,
                                        SUM(stats.impressions) AS impressions,
                                        SUM(stats.reach) AS reach,
                                        SUM(stats.clicks) AS clicks,
                                        SUM(stats.video_views) AS video_views,
                                        SUM(stats.engagements) AS engagements,
                                        SUM(stats.conversions) AS conversions,
                                        COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                        COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                        COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                        COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                        COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                FROM
                                    adsconcierge_stats.platform_campana_day AS stats
                                WHERE
                                    stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                        AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                GROUP BY stats.campanaroot) AS campana_stats ON campaigns.id = campana_stats.campanaroot
                            WHERE
                                campaigns.user_id = ?
                                    AND campaigns.customer_id = ? ", 
                            [
                                $request->user()->id, 
                                $customerSelected[0]->id
                            ]
                            );

    
        $result = [
            "draw"    => $request->input('draw') ? : 1,
            "queries" => DB::getQueryLog(),
            "data"    => $res
        ];

        return $result;
    }

    // LIST campaigns by customers
    // OK
    public function customerCampaignsListByPlatform( $customer_public_id, Request $request ){

        $user = Auth::user();

        $limit = is_null($request->get('length') ) ? 100 :$request->get('length');
        $offset = $request->input('start') ? : 1;
        $page = intval( $offset / $limit) + 1;
        $keyword = '';

        $accounts = DB::select("SELECT 
                                    cp.platform as plataforma,
                                    c.public_id as id,
                                    c.isdefault as isdefault,
                                    max(c.budget) as budget,
                                    SUM(campana_stats.spent) AS spend,
                                    SUM(campana_stats.impressions) AS impressions,
                                    SUM(campana_stats.reach) AS reach,
                                    SUM(campana_stats.clicks) AS clicks,
                                    SUM(campana_stats.video_views) AS video_views,
                                    SUM(campana_stats.engagements) AS engagements,
                                    SUM(campana_stats.conversions) AS conversions,
                                    SUM(campana_stats.ctr) AS ctr,
                                    SUM(campana_stats.cpc) AS cpc,
                                    SUM(campana_stats.cpm) AS cpm,
                                    SUM(campana_stats.cpa) AS cpa,
                                    SUM(campana_stats.cr) AS cr
                                    FROM
                                    app_thesoci_9c37.campaigns AS c
                                        LEFT JOIN
                                    app_thesoci_9c37.campaigns_platform AS cp ON c.id = cp.campana_root
                                        LEFT JOIN
                                    app_thesoci_9c37.customers AS cus ON cus.id = c.customer_id
                                        LEFT JOIN
                                    (SELECT 
                                        stats.campanaroot,
                                            stats.plataforma,
                                            ROUND(SUM(stats.cost), 2) AS spent,
                                            SUM(stats.impressions) AS impressions,
                                            SUM(stats.reach) AS reach,
                                            SUM(stats.clicks) AS clicks,
                                            SUM(stats.video_views) AS video_views,
                                            SUM(stats.engagements) AS engagements,
                                            SUM(stats.conversions) AS conversions,
                                            COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                            COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                            COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                            COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                            COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                    FROM
                                        adsconcierge_stats.platform_campana_day AS stats
                                    WHERE
                                        stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                            AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                    GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON c.id = campana_stats.campanaroot
                                    WHERE
                                        c.user_id = ?
                                        AND cus.public_id = ?
                                    GROUP BY cp.platform, c.public_id, c.isdefault", 
                                        [
                                            $request->user()->id, 
                                            $customer_public_id
                                        ]);


        if( !is_null( $request->get('search') )  ){
            $keyword = $request->get('search')['value'];
        }




        // TASK
        // HomeTable: revisar que el switch "activo" se muestra automaticamente como activo, si hay algun item "hijo" activo 
        // (activo=movimiento en el último dia, esto requiere de que se haga en el proceso back de lectura de stats y que setee 
        // como "activo" el item en su tabla.campaigns atomo creatividad, etc"
        //
        // jose, para esto, en la query inclui lo siugiente > SUM( IF(DATE(platform_campana_day.dia) = CURDATE(), 1, 0) ) as status
        // si tenemos alguna estadistica que corresponda al dia actual ( active > 0), mostramos la campaign_platform como activa.

        // que pasa si un usuario "desactiva" desde CUSTOMERS>>list by platform y desactiva FB por ejemplo. Y refresca la pagina.
        // campaign_platform, va a seguir teniendo un registro ese dia, y lo volveremos a mostrar como activa la campaign_platform ??  
        // No se si me explico bien





        $result = [
            "draw"              => $request->input('draw') ? : 1,
            "recordsTotal"      => count($accounts),
            "recordsFiltered"   => count($accounts),
            "queries"=>             DB::getQueryLog(),
            "data"              => $accounts
        ];

        return $result;

    }


      // LIST campaigns by customers
    // OK
    public function customerCampaignsListByPlatformList( $campana_root_pub_id, Request $request ){

        $user = Auth::user();

        $limit = is_null($request->get('length') ) ? 100 :$request->get('length');
        $offset = $request->input('start') ? : 1;
        $page = intval( $offset / $limit) + 1;
        $keyword = '';

        $accounts = DB::select("SELECT 
                                    cp.name,
                                    cp.platform as plataforma,
                                    cp.public_id as id,
                                    c.isdefault as isdefault,
                                    max(c.budget) as budget,
                                    SUM(campana_stats.spent) AS spend,
                                    SUM(campana_stats.impressions) AS impressions,
                                    SUM(campana_stats.reach) AS reach,
                                    SUM(campana_stats.clicks) AS clicks,
                                    SUM(campana_stats.video_views) AS video_views,
                                    SUM(campana_stats.engagements) AS engagements,
                                    SUM(campana_stats.conversions) AS conversions,
                                    SUM(campana_stats.ctr) AS ctr,
                                    SUM(campana_stats.cpc) AS cpc,
                                    SUM(campana_stats.cpm) AS cpm,
                                    SUM(campana_stats.cpa) AS cpa,
                                    SUM(campana_stats.cr) AS cr
                                    FROM
                                    app_thesoci_9c37.campaigns AS c
                                        LEFT JOIN
                                    app_thesoci_9c37.campaigns_platform AS cp ON c.id = cp.campana_root
                                        LEFT JOIN
                                    app_thesoci_9c37.customers AS cus ON cus.id = c.customer_id
                                        LEFT JOIN
                                    (SELECT 
                                        stats.campanaroot,
                                            stats.plataforma,
                                            ROUND(SUM(stats.cost), 2) AS spent,
                                            SUM(stats.impressions) AS impressions,
                                            SUM(stats.reach) AS reach,
                                            SUM(stats.clicks) AS clicks,
                                            SUM(stats.video_views) AS video_views,
                                            SUM(stats.engagements) AS engagements,
                                            SUM(stats.conversions) AS conversions,
                                            COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                            COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                            COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                            COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                            COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                    FROM
                                        adsconcierge_stats.platform_campana_day AS stats
                                    WHERE
                                        stats.dia >= '2022-01-01'
                                            AND stats.dia <= '2029-01-01'
                                    GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON c.id = campana_stats.campanaroot
                                    WHERE
                                        c.user_id = ?
                                        AND c.public_id = ?
                                    GROUP BY cp.name, cp.platform, cp.public_id, c.isdefault", 
                                        [
                                            $request->user()->id, 
                                            $campana_root_pub_id
                                        ]);


        if( !is_null( $request->get('search') )  ){
            $keyword = $request->get('search')['value'];
        }




        // TASK
        // HomeTable: revisar que el switch "activo" se muestra automaticamente como activo, si hay algun item "hijo" activo 
        // (activo=movimiento en el último dia, esto requiere de que se haga en el proceso back de lectura de stats y que setee 
        // como "activo" el item en su tabla.campaigns atomo creatividad, etc"
        //
        // jose, para esto, en la query inclui lo siugiente > SUM( IF(DATE(platform_campana_day.dia) = CURDATE(), 1, 0) ) as status
        // si tenemos alguna estadistica que corresponda al dia actual ( active > 0), mostramos la campaign_platform como activa.

        // que pasa si un usuario "desactiva" desde CUSTOMERS>>list by platform y desactiva FB por ejemplo. Y refresca la pagina.
        // campaign_platform, va a seguir teniendo un registro ese dia, y lo volveremos a mostrar como activa la campaign_platform ??  
        // No se si me explico bien





        $result = [
            "draw"              => $request->input('draw') ? : 1,
            "recordsTotal"      => count($accounts),
            "recordsFiltered"   => count($accounts),
            "queries"=>             DB::getQueryLog(),
            "data"              => $accounts
        ];

        return $result;

    }
    // LIST atoms by customers
    // OK
    public function customerAtoms(string $customer_public_id, Request $request )
    {
        DB::enableQueryLog();

        $customerSelected = Customer::where('public_id', $customer_public_id)->get();
        $customer_id = $customerSelected[0]->id;

        $results_atomos_by_customer = DB::select("SELECT 
                                            cpa.public_id,
                                            cpa.name,
                                            cpa.budget,
                                            cpa.status,
                                            cpa.platform,
                                            max(campana_stats.status) as status,
                                            SUM(campana_stats.spent) AS spent,
                                            SUM(campana_stats.impressions) AS impressions,
                                            SUM(campana_stats.reach) AS reach,
                                            SUM(campana_stats.clicks) AS clicks,
                                            SUM(campana_stats.video_views) AS video_views,
                                            SUM(campana_stats.engagements) AS engagements,
                                            SUM(campana_stats.conversions) AS conversions,
                                            SUM(campana_stats.ctr) AS ctr,
                                            SUM(campana_stats.cpc) AS cpc,
                                            SUM(campana_stats.cpm) AS cpm,
                                            SUM(campana_stats.cpa) AS cpa,
                                            SUM(campana_stats.cr) AS cr
                                            FROM
                                                app_thesoci_9c37.campaigns_platform_atomo as cpa
                                                    LEFT JOIN
                                                app_thesoci_9c37.customers AS cus ON cus.id = cpa.customer_id
                                                    LEFT JOIN
                                                (SELECT 
                                                stats.campanaroot,
                                                    stats.plataforma,
                                                    SUM( IF(DATE(stats.dia) = CURDATE(), 1, 0) ) as status,
                                                    ROUND(SUM(stats.cost), 2) AS spent,
                                                    SUM(stats.impressions) AS impressions,
                                                    SUM(stats.reach) AS reach,
                                                    SUM(stats.clicks) AS clicks,
                                                    SUM(stats.video_views) AS video_views,
                                                    SUM(stats.engagements) AS engagements,
                                                    SUM(stats.conversions) AS conversions,
                                                    COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                                    COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                                    COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                                    COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                                    COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                            FROM
                                                adsconcierge_stats.platform_atomo_day AS stats
                                            WHERE
                                                stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                                AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                                GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON cpa.campana_root_id = campana_stats.campanaroot
                                            WHERE
                                                cpa.user_id = 6
                                                    AND cus.public_id = '06e32590-c2e0-11ea-86d6-de2e501f7018'
                                            GROUP BY cpa.public_id, cpa.name,
                                                cpa.budget,
                                                cpa.status,
                                                cpa.platform", 
                                        [ $request->user()->id,  $customer_public_id ]);

        
        
                                        $result = [
            "draw"              => $request->input('draw') ? : 1,
            //   "recordsTotal"      => $results->total(),
            //   "recordsFiltered"   => $results->total(),
            //"queries"=> DB::getQueryLog(),
            "data"              => $results_atomos_by_customer
        ];

        return $result;
    }
    
    // LIST creas by customers
    // OK
    public function customerCreativities(string $customer_public_id, Request $request )
    {
       //$results = Campaign::where('public_id', $id)->select('id', 'customer_id')->first();
        DB::enableQueryLog();
        $customerSelected = Customer::where('public_id', $customer_public_id)->get();
        $customer_id = $customerSelected[0]->id;
        
        $results = DB::select("SELECT 
                                    creas.public_id,
                                    creas.name,
                                    creas.budget,
                                    creas.status,
                                    max(campana_stats.status) as status,
                                    SUM(campana_stats.spent) AS spent,
                                    SUM(campana_stats.impressions) AS impressions,
                                    SUM(campana_stats.reach) AS reach,
                                    SUM(campana_stats.clicks) AS clicks,
                                    SUM(campana_stats.video_views) AS video_views,
                                    SUM(campana_stats.engagements) AS engagements,
                                    SUM(campana_stats.conversions) AS conversions,
                                    SUM(campana_stats.ctr) AS ctr,
                                SUM(campana_stats.cpc) AS cpc,
                                SUM(campana_stats.cpm) AS cpm,
                                SUM(campana_stats.cpa) AS cpa,
                                SUM(campana_stats.cr) AS cr
                            FROM
                                app_thesoci_9c37.creatividades as creas
                                    LEFT JOIN
                                app_thesoci_9c37.customers AS cus ON cus.id = creas.customer_id
                                    LEFT JOIN
                                (SELECT 
                                    stats.campanaroot,
                                        stats.plataforma,
                                        SUM( IF(DATE(stats.dia) = CURDATE(), 1, 0) ) as status,
                                        ROUND(SUM(stats.cost), 2) AS spent,
                                        SUM(stats.impressions) AS impressions,
                                        SUM(stats.reach) AS reach,
                                        SUM(stats.clicks) AS clicks,
                                        SUM(stats.video_views) AS video_views,
                                        SUM(stats.engagements) AS engagements,
                                        SUM(stats.conversions) AS conversions,
                                        COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                        COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                        COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                        COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                        COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                FROM
                                    adsconcierge_stats.platform_ads_day AS stats
                                WHERE
                                stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                                                        AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON creas.campana_root = campana_stats.campanaroot
                            WHERE
                                creas.user_id = ?
                                    AND cus.public_id = ?
                            GROUP BY creas.public_id, creas.name,
                                creas.budget,
                                creas.status",[$request->user()->id,  $customer_public_id ]);

       $result = [
            "draw"              => $request->input('draw') ? : 1,
            //"queries"=> DB::getQueryLog(),
            "data"              => $results
        ];

        return $result;
    }

    // LIST creas by atomo
    // CHECK
    public function atomCreativities(string $id, Request $request )
    {
        //$results = Campaign::where('public_id', $id)->select('id', 'customer_id')->first();
        DB::enableQueryLog();
        $atomo = DB::table('campaigns_platform_atomo')->where('public_id', $id)->get();
        $results= null;
        
        if ($atomo){
            
            $results = DB::select("SELECT 
                                        creas.public_id,
                                        creas.name,
                                        creas.budget,
                                        creas.status,
                                        MAX(campana_stats.status) AS status,
                                        SUM(campana_stats.spent) AS spent,
                                        SUM(campana_stats.impressions) AS impressions,
                                        SUM(campana_stats.reach) AS reach,
                                        SUM(campana_stats.clicks) AS clicks,
                                        SUM(campana_stats.video_views) AS video_views,
                                        SUM(campana_stats.engagements) AS engagements,
                                        SUM(campana_stats.conversions) AS conversions,
                                        SUM(campana_stats.ctr) AS ctr,
                                        SUM(campana_stats.cpc) AS cpc,
                                        SUM(campana_stats.cpm) AS cpm,
                                        SUM(campana_stats.cpa) AS cpa,
                                        SUM(campana_stats.cr) AS cr
                                    FROM
                                        app_thesoci_9c37.creatividades AS creas
                                            LEFT JOIN
                                        app_thesoci_9c37.customers AS cus ON cus.id = creas.customer_id
                                            LEFT JOIN
                                        (SELECT 
                                            stats.campanaroot,
                                                stats.plataforma,
                                                SUM(IF(DATE(stats.dia) = CURDATE(), 1, 0)) AS status,
                                                ROUND(SUM(stats.cost), 2) AS spent,
                                                SUM(stats.impressions) AS impressions,
                                                SUM(stats.reach) AS reach,
                                                SUM(stats.clicks) AS clicks,
                                                SUM(stats.video_views) AS video_views,
                                                SUM(stats.engagements) AS engagements,
                                                SUM(stats.conversions) AS conversions,
                                                COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                                COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                                COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                                COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                                COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                        FROM
                                            adsconcierge_stats.platform_ads_day AS stats
                                        WHERE
                                            stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                            AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                        GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON creas.campana_root = campana_stats.campanaroot
                                    WHERE
                                        creas.user_id = ?
                                            AND creas.atomo_id = ?
                                    GROUP BY creas.public_id , creas.name , creas.budget , creas.status",
                                     [
                                        $request->user()->id, 
                                        $atomo[0]->id
                                    ]
                                );
        
        
        
        }
        
        $result = [
            "draw"              => $request->input('draw') ? : 1,
            //"queries"=> DB::getQueryLog(),
            "data"              => $results
        ];

        return $result;
    }

    // LIST creas by customers
    // OK
    public function data_creativity_by_campaign( Request $request, string $idPost, $platformGet = "undefined" )
    {
        //$results = Campaign::where('public_id', $id)->select('id', 'customer_id')->first();
        DB::enableQueryLog();
        
        
        $campaign = Campaign::where('public_id', $idPost)->select('id','customer_id')->first();
        
        
        if ($campaign == null) {
            
            $filter = ' and creas.campana_platform_id = ? ';
            $campaign = Campaign_platform::where('public_id', $idPost)->select('id','customer_id')->first();
            //dd($results);
            $campaignID = $campaign["id"];
            $customer_id = $campaign["customer_id"];

        }else{
            $filter = ' and creas.campana_root = ? ';
            $campaignID = $campaign["id"];
            $customer_id = $campaign["customer_id"];
            // el id que viene es de campaña
        } 



        if ($campaign != null ) {
            
          /*   $campaignID = $campaign->id;
            $customer_id = $campaign->customer_id; */

            if ($platformGet != "undefined"){
                $creasPlatformFilter = " and creas.platform =  '" . $platformGet . "'";
            }else{
                $creasPlatformFilter = "";
            }
            
                $resultsQuery = "SELECT 
                                            creas.public_id,
                                            creas.name,
                                            creas.budget,
                                            creas.status,
                                            max(campana_stats.status) as status,
                                            SUM(campana_stats.spent) AS spent,
                                            SUM(campana_stats.impressions) AS impressions,
                                            SUM(campana_stats.reach) AS reach,
                                            SUM(campana_stats.clicks) AS clicks,
                                            SUM(campana_stats.video_views) AS video_views,
                                            SUM(campana_stats.engagements) AS engagements,
                                            SUM(campana_stats.conversions) AS conversions,
                                            SUM(campana_stats.ctr) AS ctr,
                                        SUM(campana_stats.cpc) AS cpc,
                                        SUM(campana_stats.cpm) AS cpm,
                                        SUM(campana_stats.cpa) AS cpa,
                                        SUM(campana_stats.cr) AS cr
                                    FROM
                                        app_thesoci_9c37.creatividades as creas
                                            LEFT JOIN
                                        app_thesoci_9c37.customers AS cus ON cus.id = creas.customer_id
                                            LEFT JOIN
                                        (SELECT 
                                            stats.campanaroot,
                                                stats.plataforma,
                                                SUM( IF(DATE(stats.dia) = CURDATE(), 1, 0) ) as status,
                                                ROUND(SUM(stats.cost), 2) AS spent,
                                                SUM(stats.impressions) AS impressions,
                                                SUM(stats.reach) AS reach,
                                                SUM(stats.clicks) AS clicks,
                                                SUM(stats.video_views) AS video_views,
                                                SUM(stats.engagements) AS engagements,
                                                SUM(stats.conversions) AS conversions,
                                                COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                                COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                                COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                                COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                                COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                        FROM
                                            adsconcierge_stats.platform_ads_day AS stats
                                        WHERE
                                            stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                            AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                        GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON creas.campana_root = campana_stats.campanaroot
                                    WHERE
                                        creas.user_id = ? ". $filter . " AND cus.id = ?
                                    GROUP BY creas.public_id, creas.name,
                                        creas.budget,
                                        creas.status";

                $results =  DB::select( $resultsQuery, [ $request->user()->id,  $campaignID,  $customer_id ] );



                /*  
                $results = DB::table('creatividades')
                        ->leftjoin('adsconcierge_stats.platform_ads_day as stats', 'creatividades.id_en_platform', '=', 'stats.idenplatform')
                        ->where('creatividades.customer_id', $customer_id )
                        ->where('creatividades.campana_root', $campaignID );

                        if ($platformGet != "undefined"){
                            $results =  $results->where('creatividades.platform', $platformGet );
                        }

                        $results =   $results->select( 
                            'creatividades.public_id as public_id',
                            'creatividades.name as name',
                            'creatividades.budget as budget', 
                            'creatividades.status as status', 
                            DB::raw('ROUND(sum(stats.cost),2) as spent'),
                            DB::raw('sum(stats.impressions) as impressions'),
                            DB::raw('sum(stats.clicks) as clicks'),
                            DB::raw('sum(stats.conversions) as conversions'),
                            DB::raw( $this->ads_Calculations['ctr'] ),
                            DB::raw( $this->ads_Calculations['cpc'] ),
                            DB::raw( $this->ads_Calculations['cpm'] ),
                            DB::raw( $this->ads_Calculations['cr']  ),
                            DB::raw( $this->ads_Calculations['cpa'] )
                        )
                        ->where('stats.dia','>=', $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') )
                        ->where('stats.dia','<=', $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') )
                        ->groupBy('creatividades.public_id')
                        ->groupBy('creatividades.name')
                        ->groupBy('creatividades.budget')
                        ->groupBy('creatividades.status')
                        ->get()->toArray(); */

        
            }else{
                
                $customerSelected = Customer::where('public_id', $idPost)->get();
                $customer_id = $customerSelected[0]->id;


                if ($platformGet != "undefined"){
                    $creasPlatformFilter = " and creas.platform =  '" . $platformGet . "'";
                }else{
                    $creasPlatformFilter = "";
                }


                $resultsQuery = "SELECT 
                                            creas.public_id,
                                            creas.name,
                                            creas.budget,
                                            creas.status,
                                            max(campana_stats.status) as status,
                                            SUM(campana_stats.spent) AS spent,
                                            SUM(campana_stats.impressions) AS impressions,
                                            SUM(campana_stats.reach) AS reach,
                                            SUM(campana_stats.clicks) AS clicks,
                                            SUM(campana_stats.video_views) AS video_views,
                                            SUM(campana_stats.engagements) AS engagements,
                                            SUM(campana_stats.conversions) AS conversions,
                                            SUM(campana_stats.ctr) AS ctr,
                                        SUM(campana_stats.cpc) AS cpc,
                                        SUM(campana_stats.cpm) AS cpm,
                                        SUM(campana_stats.cpa) AS cpa,
                                        SUM(campana_stats.cr) AS cr
                                    FROM
                                        app_thesoci_9c37.creatividades as creas
                                            LEFT JOIN
                                        app_thesoci_9c37.customers AS cus ON cus.id = creas.customer_id
                                            LEFT JOIN
                                        (SELECT 
                                            stats.campanaroot,
                                                stats.plataforma,
                                                SUM( IF(DATE(stats.dia) = CURDATE(), 1, 0) ) as status,
                                                ROUND(SUM(stats.cost), 2) AS spent,
                                                SUM(stats.impressions) AS impressions,
                                                SUM(stats.reach) AS reach,
                                                SUM(stats.clicks) AS clicks,
                                                SUM(stats.video_views) AS video_views,
                                                SUM(stats.engagements) AS engagements,
                                                SUM(stats.conversions) AS conversions,
                                                COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                                COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                                COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                                COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                                COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                        FROM
                                            adsconcierge_stats.platform_ads_day AS stats
                                        WHERE
                                        stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                                                                AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                        GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON creas.campana_root = campana_stats.campanaroot
                                    WHERE
                                        creas.user_id = ?
                                     ". $creasPlatformFilter. "
                                            AND cus.id = ?
                                    GROUP BY creas.public_id, creas.name,
                                        creas.budget,
                                        creas.status";
                                        
                                        
                                        
                                        
                                        $results =  DB::select(    $resultsQuery,        [$request->user()->id,  $customer_id]);

                /* 

                //dd($customer_id);
                $results = DB::table('creatividades')
                ->leftjoin('adsconcierge_stats.platform_ads_day as stats', 'creatividades.id_en_platform', '=', 'stats.idenplatform')
                ->where('creatividades.customer_id', $customer_id );
            
                if ($platformGet != "undefined"){
                    $results =  $results->where('creatividades.platform', $platformGet );
                }

                $results =   $results->select( 
                    'creatividades.public_id as public_id',
                    'creatividades.name as name',
                    'creatividades.budget as budget', 
                    'creatividades.status as status', 
                    DB::raw('ROUND(sum(stats.cost),2) as spent'),
                    DB::raw('sum(stats.impressions) as impressions'),
                    DB::raw('sum(stats.clicks) as clicks'),
                    DB::raw('sum(stats.conversions) as conversions'),
                    DB::raw( $this->ads_Calculations['ctr'] ),
                    DB::raw( $this->ads_Calculations['cpc'] ),
                    DB::raw( $this->ads_Calculations['cpm'] ),
                    DB::raw( $this->ads_Calculations['cr']  ),
                    DB::raw( $this->ads_Calculations['cpa'] )
                )
                ->where('stats.dia','>=', $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') )
                ->where('stats.dia','<=', $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') )
                ->groupBy('creatividades.public_id')
                ->groupBy('creatividades.name')
                ->groupBy('creatividades.budget')
                ->groupBy('creatividades.status')
                ->get()->toArray();
                */

            }
        
    
        
            $result = [
                "draw"      => $request->input('draw') ? : 1,
                "queries"   => DB::getQueryLog(),
                "data"      => $results
            ];

        return $result;
    }
    
    // OK
    public function listCampaigns( $id, Request $request )
    {
        $limit = is_null($request->get('length') ) ? 10 :$request->get('length');
        $offset = $request->input('start') ? : 1;
        $page = intval( $offset / $limit) + 1;
        $keyword = '';

        $param = [['id', '<>', '', 'and']];
        if( !is_null( $request->get('search') )  &&  $request->get('search')['value'] != '' ){
            $keyword = $request->get('search')['value'];
            $param[] = [ 'name' , "like", '%'.$keyword.'%' ];
        }

        $campains = Campaign::with(['customer' => function ($query) use ($id){
            $query->where('id', '=', $id );
        }])->where($param)->paginate( $limit , ['*'] , 'page', $page  );

        $campains->getCollection();
        $result = [
            "draw"              => $request->input('draw') ? : 1,
            "recordsTotal"      => $campains->total(),
            "recordsFiltered"   => $campains->total(),
            "data"              => $campains->items()
        ];

        return $result;
    }

    // OK
    public function campaigns( string $id, Request $request )
    {
        $accounts = Customer::find($id);

        return view( 'template.api.campanas', [
            'title'     => 'Campaigns - Account '.$accounts->name,
            'api'       => '/api/dashboard/campaigns/list/'.$id,
            'base'      =>	'users',
            'columns'   => [  'id', 'name', 'spent', 'impression', 'clicks', 'cpc', 'cpm', 'ctr', 'conversions' ]
        ]);
    }


    //campaña, vista por plataforma
    //ok
    public function campaignListByPlatForm(string $id, Request $request )
    {
        DB::enableQueryLog();
        
        /*       $results = Campaign::where('public_id', $id)->select('id')->first();

        $id = $results["id"]; */

        $results = DB::select("SELECT 
                                cp.platform as plataforma,
                                c.public_id as id,
                                c.isdefault as isdefault,
                                max(c.budget) as total_budget,
                                SUM(campana_stats.spent) AS total_spent,
                                SUM(campana_stats.impressions) AS total_impression,
                                SUM(campana_stats.reach) AS reach,
                                SUM(campana_stats.clicks) AS total_clicks,
                                SUM(campana_stats.video_views) AS video_views,
                                SUM(campana_stats.engagements) AS engagements,
                                SUM(campana_stats.conversions) AS total_conversion,
                                SUM(campana_stats.ctr) AS ctr,
                                SUM(campana_stats.cpc) AS cpc,
                                SUM(campana_stats.cpm) AS cpm,
                                SUM(campana_stats.cpa) AS cpa,
                                SUM(campana_stats.cr) AS cr
                                FROM
                                app_thesoci_9c37.campaigns AS c
                                    LEFT JOIN
                                app_thesoci_9c37.campaigns_platform AS cp ON c.id = cp.campana_root
                                    LEFT JOIN
                                app_thesoci_9c37.customers AS cus ON cus.id = c.customer_id
                                    LEFT JOIN
                                (SELECT 
                                    stats.campanaroot,
                                        stats.plataforma,
                                        ROUND(SUM(stats.cost), 2) AS spent,
                                        SUM(stats.impressions) AS impressions,
                                        SUM(stats.reach) AS reach,
                                        SUM(stats.clicks) AS clicks,
                                        SUM(stats.video_views) AS video_views,
                                        SUM(stats.engagements) AS engagements,
                                        SUM(stats.conversions) AS conversions,
                                        COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                        COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                        COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                        COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                        COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                FROM
                                    adsconcierge_stats.platform_campana_day AS stats
                                WHERE
                                    stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                        AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON c.id = campana_stats.campanaroot
                                WHERE c.user_id = ? and c.public_id = ?
                                GROUP BY cp.platform, c.public_id, c.isdefault", 
                                    [ $request->user()->id,  $id ]);

            $result = [
                "draw"    => $request->input('draw') ? : 1,
                "queries" => DB::getQueryLog(),
                "data"    => $results
            ];

            return $result;
    }

    // OK
    public function data_campaign_by_platform(string $id, string $platform, Request $request )
    {
        
        DB::enableQueryLog();

        $results = Campaign::where('public_id', $id)->select('id')->first();
        $id = $results["id"];

        $results = DB::select("SELECT 
                                cp.name,
                                cp.public_id,
                                cp.status,
                                MAX(c.budget) AS budget,
                                SUM(campana_stats.spent) AS spent,
                                SUM(campana_stats.impressions) AS impression,
                                SUM(campana_stats.reach) AS reach,
                                SUM(campana_stats.clicks) AS clicks,
                                SUM(campana_stats.video_views) AS video_views,
                                SUM(campana_stats.engagements) AS engagements,
                                SUM(campana_stats.conversions) AS conversion,
                                SUM(campana_stats.ctr) AS ctr,
                                SUM(campana_stats.cpc) AS cpc,
                                SUM(campana_stats.cpm) AS cpm,
                                SUM(campana_stats.cpa) AS cpa,
                                SUM(campana_stats.cr) AS cr
                            FROM
                                app_thesoci_9c37.campaigns AS c
                                    LEFT JOIN
                                app_thesoci_9c37.campaigns_platform AS cp ON c.id = cp.campana_root
                                    LEFT JOIN
                                app_thesoci_9c37.customers AS cus ON cus.id = c.customer_id
                                    LEFT JOIN
                                (SELECT 
                                    stats.campanaroot,
                                        stats.plataforma,
                                        ROUND(SUM(stats.cost), 2) AS spent,
                                        SUM(stats.impressions) AS impressions,
                                        SUM(stats.reach) AS reach,
                                        SUM(stats.clicks) AS clicks,
                                        SUM(stats.video_views) AS video_views,
                                        SUM(stats.engagements) AS engagements,
                                        SUM(stats.conversions) AS conversions,
                                        COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                        COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                        COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                        COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                        COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                FROM
                                    adsconcierge_stats.platform_campana_day AS stats
                                WHERE
                                    stats.dia >= '2022-01-01'
                                        AND stats.dia <= '2024-01-01'
                                GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON c.id = campana_stats.campanaroot
                            WHERE
                                c.user_id = ? AND c.id = ?
                                    AND cp.platform = 'FACEBOOK'
                            GROUP BY cp.name , cp.public_id , cp.status",
                        
                        [ $request->user()->id,  $id ]);


       $result = [
            "draw"      =>  $request->input('draw') ? : 1,
            //"queries"   =>  DB::getQueryLog(),
            "data"      =>  $results
        ];

        return $result;
    }

    // OK
    public function data_atomo_by_platform(string $id, string $platform, Request $request )
    {
        DB::connection()->enableQueryLog();
        $results = Campaign::where('public_id', $id)->select('id')->first();

        $id = $results["id"];

        $resultsdata_atomo_by_platform = DB::select("SELECT 
                                    cp.name, cp.public_id, cp.status,
                                    max(c.budget) as budget,
                                    SUM(campana_stats.spent) AS spent,
                                    SUM(campana_stats.impressions) AS impression,
                                    SUM(campana_stats.reach) AS reach,
                                    SUM(campana_stats.clicks) AS clicks,
                                    SUM(campana_stats.video_views) AS video_views,
                                    SUM(campana_stats.engagements) AS engagements,
                                    SUM(campana_stats.conversions) AS conversion,
                                    SUM(campana_stats.ctr) AS ctr,
                                    SUM(campana_stats.cpc) AS cpc,
                                    SUM(campana_stats.cpm) AS cpm,
                                    SUM(campana_stats.cpa) AS cpa,
                                    SUM(campana_stats.cr) AS cr
                                    FROM
                                    app_thesoci_9c37.campaigns AS c
                                        LEFT JOIN
                                    app_thesoci_9c37.campaigns_platform AS cp ON c.id = cp.campana_root
                                        LEFT JOIN
                                    app_thesoci_9c37.customers AS cus ON cus.id = c.customer_id
                                        LEFT JOIN
                                    (SELECT 
                                        stats.campanaroot,
                                            stats.plataforma,
                                            ROUND(SUM(stats.cost), 2) AS spent,
                                            SUM(stats.impressions) AS impressions,
                                            SUM(stats.reach) AS reach,
                                            SUM(stats.clicks) AS clicks,
                                            SUM(stats.video_views) AS video_views,
                                            SUM(stats.engagements) AS engagements,
                                            SUM(stats.conversions) AS conversions,
                                            COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                            COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                            COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                            COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                            COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                    FROM
                                        adsconcierge_stats.platform_campana_day AS stats
                                    WHERE
                                        stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                            AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                    GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON c.id = campana_stats.campanaroot
                                    WHERE
                                        c.user_id = ?
                                        and c.id = ?
                                        and cp.platform = ?
                                    GROUP BY cp.name, cp.public_id, cp.status", 
                                    [
                                        $request->user()->id, 
                                        $id ,
                                        $platform
                                        ]);


                            /* $resultsdata_atomo_by_platform = DB::table('campaigns_platform_atomo')
                            ->leftjoin('adsconcierge_stats.platform_atomo_day as stats', 'campaigns_platform_atomo.id', '=', 'stats.atomoid')

                            ->where('stats.user_id',  $request->user()->id )
                            ->where('stats.campanaroot',   $id)
                            ->where('stats.plataforma',   $platform)
                            ->select( 
                            'campaigns_platform_atomo.public_id as public_id',
                            'campaigns_platform_atomo.name as name',
                            //'campaigns_platform_atomo.status as status',
                            'stats.atomoid',
                            //'campaigns_platform_atomo.name as name', 
                            'campaigns_platform_atomo.budget as budget', 
                            DB::raw('ROUND(sum(stats.cost),2) as spent'),
                            DB::raw('sum(stats.impressions) as impression'),
                            DB::raw('sum(stats.clicks) as clicks'),
                            DB::raw('sum(stats.conversions) as conversion'),
                            DB::raw( $this->ads_Calculations['ctr'] ),
                            DB::raw( $this->ads_Calculations['cpc'] ),
                            DB::raw( $this->ads_Calculations['cpm'] ),
                            DB::raw( $this->ads_Calculations['cr']  ),
                            DB::raw( $this->ads_Calculations['cpa'] ),
                            )
                            ->where('stats.dia','>=', $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') )
                            ->where('stats.dia','<=', $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') )
                            //->groupBy('campaigns_platform_atomo.name')
                            ->groupBy('stats.atomoid')
                            ->get(); */

       $result = [
            "draw"              => $request->input('draw') ? : 1,
            // "recordsTotal"      => $results->total(),
            // "queries"=> DB::getQueryLog(),
            "data"              => $resultsdata_atomo_by_platform
        ];

        return $result;
    }

    // CHECK
    public function data_atomo_by_campaign_platform(string $id, Request $request )
    {

        $results = DB::table('campaigns_platform')->where('public_id', $id)->pluck('id')->first();
        $id = $results;

        $results = DB::table('campaigns_platform_atomo')->where( [['user_id',  $request->user()->id], ['campana_platform_id', $id] ])
                    ->select(
                        DB::raw('public_id, name, budget, spent, conversion,status,impression, clicks, ctr, cpc, cpm')
                        )
                    ->get();

        $result = [
            "draw"              => $request->input('draw') ? : 1,
         //   "recordsTotal"      => $results->total(),
         //   "recordsFiltered"   => $results->total(),
            "data"              => $results
        ];

        return $result;
    }

    // TODO, proximo sprint
    public function data_campaigns_platform(string $id, Request $request )
    {
        $limit = is_null($request->get('length') ) ? 10 :$request->get('length');
        $offset = $request->input('start') ? : 1;
        $page = intval( $offset / $limit) + 1;

        DB::connection()->enableQueryLog();

        // $id -> campaign id
        $campaign = Campaign::where('public_id', $id)->select('id')->firstorFail();
        $campaignid = $campaign["id"];
                              
        $results = DB::table('campaigns_platform_atomo_segmentation')
                            ->leftjoin('adsconcierge_stats.platform_atomo_day as stats', 'campaigns_platform_atomo_segmentation.campana_root_id', '=', 'stats.campanaroot')
                            ->where('campaigns_platform_atomo_segmentation.user_id', $request->user()->id )
                            ->where('campaigns_platform_atomo_segmentation.campana_root_id', $campaignid )
                            ->select(
                                    'campaigns_platform_atomo_segmentation.public_id as public_id', 
                                    'campaigns_platform_atomo_segmentation.name as name', 
                                    'campaigns_platform_atomo_segmentation.status as status', 
                                    DB::raw(
                                        'ROUND(SUM(campaigns_platform_atomo_segmentation.budget),2) as budget, 
                                        ROUND(SUM(stats.cost),2) as spent, 
                                        SUM(stats.conversions) as conversion, 
                                        SUM(stats.impressions) as impression,
                                        SUM(stats.clicks) as clicks,'),
                                    
                                        DB::raw( $this->ads_Calculations['ctr'] ),
                                        DB::raw( $this->ads_Calculations['cpc'] ),
                                        DB::raw( $this->ads_Calculations['cpm'] ),
                                        DB::raw( $this->ads_Calculations['cr']  ),
                                        DB::raw( $this->ads_Calculations['cpa'] ),
                                        )
                                        ->where('stats.dia','>=', $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') )
                        ->where('stats.dia','<=', $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') )
                            ->groupBy('campaigns_platform_atomo_segmentation.public_id' )
                            ->groupBy('campaigns_platform_atomo_segmentation.name' )
                            ->groupBy('campaigns_platform_atomo_segmentation.status' )
                            ->get();

        $result = [
            "draw"               => $request->input('draw') ? : 1,
            //"recordsTotal"     => $results->total(),
            //"recordsFiltered"  => $results->total(),
            //"queries"            => DB::getQueryLog(),
            "data"               => $results,
            "campaign_public_id" => $id,
            //"count"              => count($results)
        ];

        return $result;
    }


    // OK
    public function data_atomo(string $id, Request $request )
    {

        $results = Campaign::where('public_id', $id) ->select('id','customer_id')->first();
        $filter = '';


        if ($results == null) {
            
            $filter = ' cpa.campana_platform_id = ? ';
            $results = Campaign_platform::where('public_id', $id) ->select('id','customer_id')->first();
            //dd($results);
            $idCampaign = $results["id"];
            $idCustomer = $results["customer_id"];

        }else{
            $filter = ' cpa.campana_root_id = ? ';
            $idCampaign = $results["id"];
            $idCustomer = $results["customer_id"];
            // el id que viene es de campaña
        } 


        if ($results != null){
            
            $idCampaign = $results["id"];
            $idCustomer = $results["customer_id"];


            $results = DB::select("SELECT 
                                             cpa.public_id,
                                            cpa.name,
                                            cpa.budget,
                                            cpa.status,
                                            cpa.platform,
                                            max(campana_stats.status) as status,
                                            SUM(campana_stats.spent) AS spent,
                                            SUM(campana_stats.impressions) AS impression,
                                            SUM(campana_stats.reach) AS reach,
                                            SUM(campana_stats.clicks) AS clicks,
                                            SUM(campana_stats.video_views) AS video_views,
                                            SUM(campana_stats.engagements) AS engagements,
                                            SUM(campana_stats.conversions) AS conversions,
                                            SUM(campana_stats.ctr) AS ctr,
                                            SUM(campana_stats.cpc) AS cpc,
                                            SUM(campana_stats.cpm) AS cpm,
                                            SUM(campana_stats.cpa) AS cpa,
                                            SUM(campana_stats.cr) AS cr
                                            FROM
                                                app_thesoci_9c37.campaigns_platform_atomo as cpa
                                                    LEFT JOIN
                                                app_thesoci_9c37.customers AS cus ON cus.id = cpa.customer_id
                                                    LEFT JOIN
                                                (SELECT 
                                                stats.campanaroot,
                                                    stats.plataforma,
                                                    SUM( IF(DATE(stats.dia) = CURDATE(), 1, 0) ) as status,
                                                    ROUND(SUM(stats.cost), 2) AS spent,
                                                    SUM(stats.impressions) AS impressions,
                                                    SUM(stats.reach) AS reach,
                                                    SUM(stats.clicks) AS clicks,
                                                    SUM(stats.video_views) AS video_views,
                                                    SUM(stats.engagements) AS engagements,
                                                    SUM(stats.conversions) AS conversions,
                                                    COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                                    COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                                    COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                                    COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                                    COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                            FROM
                                                adsconcierge_stats.platform_atomo_day AS stats
                                            WHERE
                                                
                                                stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                                AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                                GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON cpa.campana_root_id = campana_stats.campanaroot

                                            WHERE " . $filter . " AND cus.public_id = ? GROUP BY cpa.public_id, cpa.name, cpa.budget, cpa.status, cpa.platform", 

                                        [ $idCampaign,  $idCustomer ]);

        
        }else{
                
                $customerSelected = Customer::where('public_id', $id)->get();
                $customer_id = $customerSelected[0]->id;
                //dd($customerSelected);

                $results = DB::select("SELECT 
                                            cpa.public_id,
                                            cpa.name,
                                            cpa.budget,
                                            cpa.status,
                                            cpa.platform,
                                            max(campana_stats.status) as status,
                                            SUM(campana_stats.spent) AS spent,
                                            SUM(campana_stats.impressions) AS impression,
                                            SUM(campana_stats.reach) AS reach,
                                            SUM(campana_stats.clicks) AS clicks,
                                            SUM(campana_stats.video_views) AS video_views,
                                            SUM(campana_stats.engagements) AS engagements,
                                            SUM(campana_stats.conversions) AS conversions,
                                            SUM(campana_stats.ctr) AS ctr,
                                            SUM(campana_stats.cpc) AS cpc,
                                            SUM(campana_stats.cpm) AS cpm,
                                            SUM(campana_stats.cpa) AS cpa,
                                            SUM(campana_stats.cr) AS cr
                                            FROM
                                                app_thesoci_9c37.campaigns_platform_atomo as cpa
                                                    LEFT JOIN
                                                app_thesoci_9c37.customers AS cus ON cus.id = cpa.customer_id
                                                    LEFT JOIN
                                                (SELECT 
                                                stats.campanaroot,
                                                    stats.plataforma,
                                                    SUM( IF(DATE(stats.dia) = CURDATE(), 1, 0) ) as status,
                                                    ROUND(SUM(stats.cost), 2) AS spent,
                                                    SUM(stats.impressions) AS impressions,
                                                    SUM(stats.reach) AS reach,
                                                    SUM(stats.clicks) AS clicks,
                                                    SUM(stats.video_views) AS video_views,
                                                    SUM(stats.engagements) AS engagements,
                                                    SUM(stats.conversions) AS conversions,
                                                    COALESCE(ROUND(SUM(stats.clicks) / SUM(stats.impressions), 2), 0) AS ctr,
                                                    COALESCE(ROUND(SUM(stats.cost) / SUM(stats.impressions), 2), 0) AS cpc,
                                                    COALESCE(ROUND((SUM(stats.cost) / SUM(stats.impressions)) * 1000, 2), 0) AS cpm,
                                                    COALESCE(ROUND(SUM(stats.cost) / SUM(stats.conversions), 2), 0) AS cpa,
                                                    COALESCE(ROUND(SUM(stats.conversions) / SUM(stats.clicks), 2), 0) AS cr
                                            FROM
                                                adsconcierge_stats.platform_atomo_day AS stats
                                            WHERE
                                                stats.dia >= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'start') . "'
                                                AND stats.dia <= '" . $this->getDateFromRequestPeriod($request->get('period_filter'), 'end') . "'
                                                GROUP BY stats.campanaroot , stats.plataforma) AS campana_stats ON cpa.campana_root_id = campana_stats.campanaroot
                                            WHERE
                                                cpa.campana_root_id = ?
                                                    AND cus.public_id = ?
                                            GROUP BY cpa.public_id, cpa.name,
                                                cpa.budget,
                                                cpa.status,
                                                cpa.platform", 
                                        [ $idCampaign,  $idCustomer ]);



        
        }
 
        $result = [
            "draw"  => $request->input('draw') ? : 1,
            "data"  => $results
        ];

        return $result;
    }



    /**
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function switch_type(Request $request ){
        //deprecated
        die();
        $id =  $request->get('id');
        $type = $request->get('type');
        $affected = 0;

        if($request->get('check') == "true"){
            $check = "ACTIVE";
        }else{
            $check = "PAUSED";
        }

        switch($type){
            case "campaigns":
                $affected = DB::table('campaigns')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['status' => $check]);
                break;
            case "creativity":
                $affected = DB::table('creatividades')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['status' => $check]);

                break;
            case "atom":
                $affected = DB::table('campaigns_platform_atomo')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['status' => $check]);
                break;
        }

        if($affected == 1){
            return array("success" => true, 'affected' => $affected);
        }

        return array("success" => false, 'affected' => $affected);
    }

    //Cambio por Miguel Guevara
    private function update_background_job($type, $action_type, $subject_id, $next_execution) {
        DB::connection('stats')->table('adsconcierge_stats.background_job')->insert([
                        'type'=> $type, 'action_type' => $action_type,
                        'subject_id' => $subject_id,'next_execution' => $next_execution]);
    }


    //Actualiza el estado de una Campania, Campania platform, creatividades, atomo
    //Cambio por Miguel Guevara

    public function change_status(Request $request){

        $id =  $request->get('id');
        $type = $request->get('type');
        $response = false;

        if ($request->get('check') == "true") {
            $check = "ACTIVE";
                $status_previous = "PAUSED";
        } else {
            $check = "PAUSED";
                $status_previous = "ACTIVE";
        }

        switch($type){
            case "campaigns":
                $affected = DB::table('campaigns')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['status' => $check]);

                break;
            case "platform":

                $results = Campaign::where('public_id', $id)->select('id')->first();
                $id_campaigns = $results["id"];

                $to_affected = DB::table('campaigns_platform')
                                        ->where('campana_root', $id_campaigns)
                                        ->where('user_id', $request->user()->id )
                                        ->where('platform', $request->get('platform'))
                                        ->where('status', $status_previous)
                                        ->select('id')->get();

                $affected = DB::table('campaigns_platform')
                                        ->where('campana_root', $id_campaigns)
                                        ->where('user_id', $request->user()->id )
                                        ->where('platform', $request->get('platform'))
                                        ->where('status', $status_previous)
                                        ->update(['status' => $check]);


		        foreach($to_affected as $cell){
                    $tmp = $cell->id;
                   
                        $job =  [
                            'type' => 'campaigns_platform',
                            'subject_id' => $tmp,
                            'action_type' => 'update_status',
                            'Execution_interval' => '15M',
                            'next_execution' => date('Y-m-d H:i:s')
                        ];
            
                        //DB::connection('stats')->table('background_job')->insert($job);
                        event(new newCronJobEvent($job));


                }

                break;

	        case "campaigns_platform":
                $affected = DB::table('campaigns_platform')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['status' => $check]);

                $id_chng = DB::table('campaigns_platform')->where('public_id', $id)->pluck('id')->first();

                $this->update_background_job('campaigns_platform', 'update_status', $id_chng, date('Y-m-d H:i:s'));

                break;
            case "creativity":
                $affected = DB::table('creatividades')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['status' => $check]);

		        $id_chng = DB::table('creatividades')->where('public_id', $id)->pluck('id')->first();

                $this->update_background_job('creatividades', 'update_status', $id_chng, date('Y-m-d H:i:s'));
                break;
            case "atom":
                $affected = DB::table('campaigns_platform_atomo')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['status' => $check]);

		        $id_chng = DB::table('campaigns_platform_atomo')->where('public_id', $id)->pluck('id')->first();

                $this->update_background_job('campaigns_platform_atomo', 'update_status', $id_chng, date('Y-m-d H:i:s'));
                break;
        }

        if($affected >= 1){
            $response = true;
        }
        return response()->json($response);
    }

    public function change_budget(Request $request ){
        //deprecated
        die();
        $id =  $request->get('pk');
        $type = $request->get('name');
        $value = $request->get('value');
        $response = false;

        switch($type){
            case "campaigns":
                $affected = DB::table('campaigns')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['budget' => $value]);

                 $results = Campaign::where('public_id', $id)
                    ->select('id')->first();

                $id_campaigns = $results["id"];

                $numero_atomo = DB::table('campaigns_platform_atomo')
                    ->where('campana_root_id', $id_campaigns)
                    ->where('user_id', $request->user()->id )
                    ->count();

                //Si el atomo es distinto de 0, asi evita div/0
                //Cambio por Miguel Guevara
		if ($numero_atomo != 0) {

                    $budget_atomo = $value/$numero_atomo;

                    DB::table('campaigns_platform_atomo')
                        ->where('campana_root_id', $id_campaigns)
                        ->where('user_id', $request->user()->id )
                        ->update(['budget' => $budget_atomo]);
                }
                break;
	    case "campaign_platform":

                $affected = DB::table('campaigns_platform')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['budget' => $value]);

                $id_chng = DB::table('campaigns_platform')->where('public_id', $id)->pluck('id')->first();

                $this->update_background_job('campaigns_platform', 'update_budget', $id_chng, date('Y-m-d H:i:s'));

		break;
            case "creativity":
                $affected = DB::table('creatividades')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['budget' => $value]);

		$id_chng = DB::table('creatividades')->where('public_id', $id)->pluck('id')->first();

                $this->update_background_job('creatividades', 'update_budget', $id_chng, date('Y-m-d H:i:s'));
                break;

            case "atom":
                $affected = DB::table('campaigns_platform_atomo')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['budget' => $value]);

		$id_chng = DB::table('campaigns_platform_atomo')->where('public_id', $id)->pluck('id')->first();

                $this->update_background_job('campaigns_platform_atomo', 'update_budget', $id_chng, date('Y-m-d H:i:s'));
                break;
        }

        if($affected == 1){
            $response = true;
        }

        return response()->json($response);
    }

    //Actualiza el name de una Campania, Campania platform, creatividades, atomo
    //Cambio por Miguel Guevara

    public function change_name(Request $request ){
        //deprecated
        die();
        $id =  $request->get('pk');
        $type = $request->get('name');
        $value = $request->get('value');
        $response = false;

        switch($type){
            case "campaigns":
                $affected = DB::table('campaigns')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['name' => $value]);

                break;
	    case "campaign_platform":

                $affected = DB::table('campaigns_platform')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['name' => $value]);

                $id_chng = DB::table('campaigns_platform')->where('public_id', $id)->pluck('id')->first();

                $this->update_background_job('campaigns_platform', 'update_field', $id_chng, date('Y-m-d H:i:s'));

		break;
            case "creativity":
                $affected = DB::table('creatividades')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['title' => $value]);

		$id_chng = DB::table('creatividades')->where('public_id', $id)->pluck('id')->first();

                $this->update_background_job('creatividades', 'update_field', $id_chng, date('Y-m-d H:i:s'));
                break;

            case "atom":
                $affected = DB::table('campaigns_platform_atomo')
                    ->where('public_id', $id)
                    ->where('user_id', $request->user()->id )
                    ->update(['name' => $value]);

		$id_chng = DB::table('campaigns_platform_atomo')->where('public_id', $id)->pluck('id')->first();

                $this->update_background_job('campaigns_platform_atomo', 'update_field', $id_chng, date('Y-m-d H:i:s'));
                break;
        }

        if($affected == 1){
            $response = true;
        }

        return response()->json($response);
    }

    public function duplicate(Request $request){
        $response["success"] = true;
        
        $new_campaign =  $request->get('new_campaign');
        $items =  $request->get('items');
        $affected = 0;

        foreach($items as $value){
            $type = $value['type'];
            $itemid = $value['itemid'];
            $campaign = $value['campaign'];

            switch($type){
                case "campaigns":

                    $old = Campaign::where('public_id', $itemid)
                    ->where('user_id', $request->user()->id )->first();
                    $new = $old->replicate();
                    $new->public_id = null;
                    $new->save();

                    break;
                case "creativity":

                    if(isset($new_campaign) and $new_campaign != ""){
                        $campaign = $new_campaign;
                    }

                    $results = Campaign::where('public_id', $campaign)
                    ->select('id')->first();

                    $id_campaigns = $results["id"];

                    $old = Creativity::where('public_id', $itemid)
                    ->where('user_id', $request->user()->id )->first();
                    $new = $old->replicate();
                    $new->campana_root = $id_campaigns;
                    //$new->id_en_platform = time();
                    $new->public_id = null;
                    $new->save();

                    break;
                case "atomo":

                    if(isset($new_campaign) and $new_campaign != ""){
                        $campaign = $new_campaign;
                    }

                    $results = Campaign::where('public_id', $campaign)
                    ->select('id')->first();


                    $id_campaigns = $results["id"];

                    $old = Campaign_platform_atomo::where('public_id', $itemid)
                    ->where('user_id', $request->user()->id )->first();
                    $new = $old->replicate();
                    $new->campana_root_id = $id_campaigns;
                    $new->id_en_platform = time();
                    $new->public_id = null;
                    $new->save();

                    break;
            }

            $response["type"][] = $type;

            if($affected == 1){
                $response["success"] = true;

            }

        }
        return response()->json($response);
    }

    /**
     * Show Datos de prueba a.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    /**
     * First table for MORE
     **/
    public function datos_a(int $id, Request $request)
    {
        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $results = Campaign::where('user_id', $request->user()->id)
            ->where('customer_id', $id)
            ->orWhere('user_id', 0)
            ->where('customer_id', $id)
            ->paginate($limit, ['*'], 'page', $page);

        $results->getCollection();

        $result = [
            "draw" => $request->input('draw') ?: 1,
            "recordsTotal" => $results->total(),
            "recordsFiltered" => $results->total(),
            "data" => $results->items()
        ];

        return $result;
    }
    /**
     * Second table for MORE
     **/
    public function datos_aa(int $id, Request $request)
    {
        $campanas = [$id, 0];
        $user_ids = [$request->user()->id, 0];
        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $results = Campaign_platform::whereIn('user_id', $user_ids)
            ->whereIn('campana_root', $campanas)
            ->paginate($limit, ['*'], 'page', $page);

        $results->getCollection();

        $result = [
            "draw" => $request->input('draw') ?: 1,
            "recordsTotal" => $results->total(),
            "recordsFiltered" => $results->total(),
            "data" => $results->items()
        ];
        return $result;
    }
    /**
     * Third table for MORE
     **/
    public function datos_aaa(int $id, Request $request)
    {
        $campana_platform = [$id, 0];
        $user_ids = [$request->user()->id, 0];
        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $results = Campaign_platform_atomo::whereIn('user_id', $user_ids)
            ->whereIn('campana_platform_id', $campana_platform)
            ->paginate($limit, ['*'], 'page', $page);

        $results->getCollection();

        $result = [
            "draw" => $request->input('draw') ?: 1,
            "recordsTotal" => $results->total(),
            "recordsFiltered" => $results->total(),
            "data" => $results->items()
        ];
        return $result;
    }
    /**
     * Fourth table for MORE
     **/
    public function datos_aaaa(int $id, Request $request)
    {
        $campana_platform_atomo = [$id, 0];
        $user_ids = [$request->user()->id, 0];
        $limit = is_null($request->get('length')) ? 10 : $request->get('length');
        $offset = $request->input('start') ?: 1;
        $page = intval($offset / $limit) + 1;
        $results = Creatividades::whereIn('user_id', $user_ids)
            ->whereIn('atomo_id', $campana_platform_atomo)
            ->paginate($limit, ['*'], 'page', $page);

        $results->getCollection();

        $result = [
            "draw" => $request->input('draw') ?: 1,
            "recordsTotal" => $results->total(),
            "recordsFiltered" => $results->total(),
            "data" => $results->items()
        ];
        return $result;
    }
    /**
     * Show Datos de prueba b.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function datos_b(int $id, Request $request)
    {
        $results = \DB::table('campaigns_platform')
            ->where(
                [
                    ['user_id', $request->user()->id],
                    ['customer_id', $id],
                ])
            ->select('platform as id', DB::raw('SUM(budget) as total_budget,SUM(spent) as total_spent,SUM(conversion) as total_conversion'))
            ->groupBy('platform')
            //  ->havingRaw('SUM(price) > ?', [2500])
            ->get();


        $result = [
            "draw" => $request->input('draw') ?: 1,
            //   "recordsTotal"      => $results->total(),
            //   "recordsFiltered"   => $results->total(),
            "data" => $results
        ];

        return $result;
    }

    /**
     * Show Datos de prueba c.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function datos_c(int $id, Request $request)
    {
        $results = \DB::table('creatividades')
            ->where(
                [
                    ['user_id', $request->user()->id],
                    ['customer_id', $id],
                ])
            ->select('id', 'title', 'impressions', 'clicks')
            ->get();


        $result = [
            "draw" => $request->input('draw') ?: 1,
            //   "recordsTotal"      => $results->total(),
            //   "recordsFiltered"   => $results->total(),
            "data" => $results
        ];

        return $result;
    }


    public function chartData(Request $request, $entity, $id, $platform=null){

        $user = Auth::user();
        //$customers = [$id];

        $today = date("Y-m-d");
        $date = strtotime($today);

        $date_begin = strtotime("-100 day", $date);
        $date_begin = date('Y-m-d', $date_begin);
        
        DB::connection('stats')->enableQueryLog();

        switch ($entity) {
            case 'campaign':
                
                $campaign = Campaign::where('public_id',$id)->select('id')->get();

                $stats = DB::connection('stats')->table('adsconcierge_stats.platform_campana_day')
                                                ->where('dia', '>=', $date_begin)
                                                ->where('dia', '<=', $today)
                                                ->selectRaw('dia as x, SUM(impressions) as impression, ROUND(SUM(cost), 2) as spend, SUM(conversions) as conversions')
                                                ->where("user_id", $user->id)
                                                ->where("campanaroot", $campaign[0]->id)
                                                ->groupBy('dia')
                                                ->orderBy("x")
                                                ->get();

                break;
            case 'customer':

                $customer = Customer::where('public_id',$id)->select('id')->get();
                
                $stats = DB::connection('stats')->table('adsconcierge_stats.platform_campana_day')
                                                ->where('dia', '>=', $date_begin)
                                                ->where('dia', '<=', $today)
                                                ->selectRaw('dia as x, SUM(impressions) as impression, ROUND(SUM(cost), 2) as spend, SUM(conversions) as conversions')
                                                ->where("user_id", $user->id)
                                                ->where("customer_id", $customer[0]->id);

                if (($platform != null && $platform != 'undefined')){
                    $stats =    $stats->where("plataforma", $platform);
                }

                $stats =    $stats->groupBy('dia')->orderBy("x")->get();
                
                
                break;
            case 'atom':
                
                $stats = DB::connection('stats')->table('adsconcierge_stats.platform_atomo_day')
                                                ->where('dia', '>=', $date_begin)
                                                ->where('dia', '<=', $today)
                                                ->selectRaw('dia as x, SUM(impressions) as impression, ROUND(SUM(cost), 2) as spend, SUM(conversions) as conversions')
                                                ->where("user_id", $user->id);

                if (($platform != null && $platform != 'undefined')){
                    $stats =    $stats->where("plataforma", $platform);
                }

                $stats =    $stats->groupBy('dia')->orderBy("x")->get();


                break;
            case 'creativity':
                
                $stats = DB::connection('stats')->table('adsconcierge_stats.platform_ads_day')
                ->where('dia', '>=', $date_begin)
                ->where('dia', '<=', $today)
                ->selectRaw('dia as x, SUM(impressions) as impression, ROUND(SUM(cost), 2) as spend, SUM(conversions) as conversions')
                ->where("user_id", $user->id);

                if (($platform != null && $platform != 'undefined')){
                $stats =    $stats->where("plataforma", $platform);
                }

                $stats =    $stats->groupBy('dia')->orderBy("x")->get();

                break;


                case 'campaign_platform':
                
                    $results = Campaign_platform::where('public_id', $id)->where('user_id', $user->id)->select('id','customer_id')->first();
                    $idCampaign = $results["id"];

                    $stats = DB::connection('stats')->table('adsconcierge_stats.platform_campana_day')
                    ->where('dia', '>=', $date_begin)
                    ->where('dia', '<=', $today)
                    ->selectRaw('dia as x, SUM(impressions) as impression, ROUND(SUM(cost), 2) as spend, SUM(conversions) as conversions')
                    ->where("user_id", $user->id)
                    ->where("campanaid", $idCampaign);
    
                    if (($platform != null && $platform != 'undefined')){
                    $stats =    $stats->where("plataforma", $platform);
                    }
    
                    $stats =    $stats->groupBy('dia')->orderBy("x")->get();
    
                    break;





                
        }




        $array = [
            "result" => $stats,
        ];

        return $array;






    }






    public function datos_d(Request $request, $id)
    {

die();




        $typeChart = (isset($request->datos['typeChart'])) ? $request->datos['typeChart'] : [];
        $fields = $request->get("field_name");
        $frecuencia = 1;


        if ($fields == "") {
            $fields = array("impresions");
        } else {
            $fields = array($fields);
        }

        $date_begin = "";
        $date_end = "";
        $today = date("Y-m-d");
        $date = strtotime($today);

        $date = strtotime("-30 day", $date);
        $date_begin = date('Y-m-d', $date);


        if (isset($customers) && count($customers) > 0) {

            $datos_temporal = Customer::whereIn("id", $customers)
                ->where("user_id", $user->id)
                ->get();

            $customers_ids = array();
            foreach ($datos_temporal as $value) {
                $customers_ids[] = $value->id;
            }

        }

        $fields_query = "   SUM(impressions) as impresions,
                            ROUND(SUM(cost), 2) as spend,
                            SUM(clicks) as clicks,
                            ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS ctr,
                            ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS ecpm";


        $results = array();

        $fields_data = DB::connection('stats')->table('platform_campana_day');

        // building query and getting data for fields ("impressions", "spend", "crt", "ecpm", "clicks")

        if (($date_end != "") && ($date_begin != "")) {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
            $fields_data = $fields_data->where('dia', '<=', $date_end);
        } else if ($date_begin != "") {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
        }

        if (isset($customers_ids) && count($customers_ids) > 0) {
            $fields_data = $fields_data->whereIn('customer_id', $customers_ids);
        }

        //Filtros TOP

        $fields_data = $fields_data
            ->selectRaw($fields_query)
            ->where("user_id", $user->id)
            ->get();


        // building query and getting data for chart
        foreach ($fields as $value) {

            $datos = DB::connection('stats')
                ->table('platform_campana_day');

            if ($typeChart == 'radialBar') {
                $datos = $datos->selectRaw($types_fields[$value]);
            } else {

                if (intval($frecuencia) === 1) {
                    $datos = $datos->selectRaw('dia as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('dia');
                } else if (intval($frecuencia) === 7) {
                    $datos = $datos->selectRaw('yearweek as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearweek');
                } else if (intval($frecuencia) === 30) {
                    $datos = $datos->selectRaw('yearmonth as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearmonth');
                }

            }

            if (($date_end != "") && ($date_begin != "")) {
                $datos = $datos->where('dia', '>=', $date_begin);
                $datos = $datos->where('dia', '<=', $date_end);
            } else if ($date_begin != "") {
                $datos = $datos->where('dia', '>=', $date_begin);
            }

            if (isset($customers_ids) && count($customers_ids) > 0) {
                $datos = $datos->whereIn('customer_id', $customers_ids);
            }


            $results[] = $datos
                ->where("user_id", $user->id)
                ->orderBy("x")
                ->get();
        }


        switch (count($results)) {
            case 1;

                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0)
                            ),
                            "labels" => array($fields[0])
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0]
                            )
                        )
                    );
                }
                break;
            case 2:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0),
                                ((isset($results[1][0]->y)) ? $results[1][0]->y : 0)
                            ),
                            "labels" => array($fields[0], $fields[1])
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0]
                            ),
                            array(
                                "data" => ((count($results[1]) > 0) ? $results[1] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[1]
                            )
                        )
                    );
                }
                break;
            default:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(0),
                            "labels" => array("impressions")
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            "data" => json_decode('[{"x":" ","y":"0"}]'),
                            "name" => "impressions"
                        )
                    );
                }
                break;
        }

        $array = [
            "result" => $result,
            "fields_data" => $fields_data,
        ];



        return $array;

    }
    /**
     * Second table for CHARTS
     **/
    public function datos_dd(int $id, Request $request)
    {

        $user = Auth::user();
        $campaigns = [$id];
        $types_fields = array(
            "impresions" => "SUM(impressions) as y",
            "spend" => "ROUND(SUM(cost), 2) as y",
            "clicks" => "SUM(clicks) as y",
            "ctr" => "ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS y",
            "ecpm" => "ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS y",
        );

        $caoptions = 'caoptions_actions_'.$id;
        $typeChart = (isset($request->datos['typeChart'])) ? $request->datos['typeChart'] : [];
        $fields = $request->get("field_name");
        $frecuencia = 1;


        if ($fields == "" || $fields == null) {
            $fields = array("impresions");
        } else {
            $fields = array($fields);
        }

        $date_begin = "";
        $date_end = "";
        $today = date("Y-m-d");
        $date = strtotime($today);

        $date = strtotime("-30 day", $date);
        $date_begin = date('Y-m-d', $date);


        if (isset($campaigns) && count($campaigns) > 0) {

            $datos_temporal = Campaign::whereIn("id", $campaigns)
                ->where("user_id", $user->id)
                ->get();

            $campaigns_ids = array();
            foreach ($datos_temporal as $value) {
                $campaigns_ids[] = $value->id;
            }
        }

        $fields_query = "   SUM(impressions) as impresions,
                            ROUND(SUM(cost), 2) as spend,
                            SUM(clicks) as clicks,
                            ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS ctr,
                            ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS ecpm";


        $results = array();

        $fields_data = DB::connection($this->db2)
            ->table('platform_campana_day');

        // building query and getting data for fields ("impressions", "spend", "crt", "ecpm", "clicks")

        if (($date_end != "") && ($date_begin != "")) {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
            $fields_data = $fields_data->where('dia', '<=', $date_end);
        } else if ($date_begin != "") {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
        }

        if (isset($campaigns_ids) && count($campaigns_ids) > 0) {
            $fields_data = $fields_data->whereIn('campanaroot', $campaigns_ids);
        }

        //Filtros TOP

        $fields_data = $fields_data
            ->selectRaw($fields_query)
            ->where("user_id", $user->id)
            ->get();

        // building query and getting data for chart
        foreach ($fields as $value) {

            $datos = DB::connection($this->db2)
                ->table('platform_campana_day');

            if ($typeChart == 'radialBar') {
                $datos = $datos->selectRaw($types_fields[$value]);
            } else {

                if (intval($frecuencia) === 1) {
                    $datos = $datos->selectRaw('dia as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('dia');
                } else if (intval($frecuencia) === 7) {
                    $datos = $datos->selectRaw('yearweek as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearweek');
                } else if (intval($frecuencia) === 30) {
                    $datos = $datos->selectRaw('yearmonth as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearmonth');
                }

            }

            if (($date_end != "") && ($date_begin != "")) {
                $datos = $datos->where('dia', '>=', $date_begin);
                $datos = $datos->where('dia', '<=', $date_end);
            } else if ($date_begin != "") {
                $datos = $datos->where('dia', '>=', $date_begin);
            }

            if (isset($campaigns_ids) && count($campaigns_ids) > 0) {
                $datos = $datos->whereIn('campanaroot', $campaigns_ids);
            }

            $results[] = $datos
                ->where("user_id", $user->id)
                ->orderBy("x")
                ->get();
        }


        switch (count($results)) {
            case 1;

                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0)
                            ),
                            "labels" => array($fields[0])
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0]
                            )
                        )
                    );
                }
                break;
            case 2:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0),
                                ((isset($results[1][0]->y)) ? $results[1][0]->y : 0)
                            ),
                            "labels" => array($fields[0], $fields[1])
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0]
                            ),
                            array(
                                "data" => ((count($results[1]) > 0) ? $results[1] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[1]
                            )
                        )
                    );
                }
                break;
            default:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(0),
                            "labels" => array("impressions")
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            "data" => json_decode('[{"x":" ","y":"0"}]'),
                            "name" => "impressions"
                        )
                    );
                }
                break;
        }

        $array = [
            "result" => $result,
            "fields_data" => $fields_data,
        ];



        return $array;

    }
    /**
     * Third table for CHARTS
     **/
    public function datos_ddd(int $id, Request $request)
    {

        $user = Auth::user();
        $campaigns_platform = [$id];
        $types_fields = array(
            "impresions" => "SUM(impressions) as y",
            "spend" => "ROUND(SUM(cost), 2) as y",
            "clicks" => "SUM(clicks) as y",
            "ctr" => "ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS y",
            "ecpm" => "ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS y",
        );


        $typeChart = (isset($request->datos['typeChart'])) ? $request->datos['typeChart'] : [];
        $fields = $request->get("field_name");
        $frecuencia = 1;

        //dd($campaigns_platform);
        if ($fields == "" || $fields == null) {
            $fields = array("impresions");
        } else {
            $fields = array($fields);
        }

        $date_begin = "";
        $date_end = "";
        $today = date("Y-m-d");
        $date = strtotime($today);

        $date = strtotime("-30 day", $date);
        $date_begin = date('Y-m-d', $date);


        if (isset($campaigns_platform) && count($campaigns_platform) > 0) {

            $datos_temporal = Campaign_platform::whereIn("id", $campaigns_platform)
                ->where("user_id", $user->id)
                ->get();

            $campaigns_platform_ids = array();
            foreach ($datos_temporal as $value) {
                $campaigns_platform_ids[] = $value->id;
            }
        }

        $fields_query = "   SUM(impressions) as impresions,
                            ROUND(SUM(cost), 2) as spend,
                            SUM(clicks) as clicks,
                            ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS ctr,
                            ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS ecpm";


        $results = array();

        $fields_data = DB::connection($this->db2)
            ->table('platform_campana_day');

        // building query and getting data for fields ("impressions", "spend", "crt", "ecpm", "clicks")

        if (($date_end != "") && ($date_begin != "")) {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
            $fields_data = $fields_data->where('dia', '<=', $date_end);
        } else if ($date_begin != "") {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
        }

        if (isset($campaigns_platform_ids) && count($campaigns_platform_ids) > 0) {
            $fields_data = $fields_data->whereIn('campanaid', $campaigns_platform_ids);
        }

        //Filtros TOP

        $fields_data = $fields_data
            ->selectRaw($fields_query)
            ->where("user_id", $user->id)
            ->get();

        // building query and getting data for chart
        foreach ($fields as $value) {

            $datos = DB::connection($this->db2)
                ->table('platform_campana_day');

            if ($typeChart == 'radialBar') {
                $datos = $datos->selectRaw($types_fields[$value]);
            } else {

                if (intval($frecuencia) === 1) {
                    $datos = $datos->selectRaw('dia as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('dia');
                } else if (intval($frecuencia) === 7) {
                    $datos = $datos->selectRaw('yearweek as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearweek');
                } else if (intval($frecuencia) === 30) {
                    $datos = $datos->selectRaw('yearmonth as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearmonth');
                }

            }

            if (($date_end != "") && ($date_begin != "")) {
                $datos = $datos->where('dia', '>=', $date_begin);
                $datos = $datos->where('dia', '<=', $date_end);
            } else if ($date_begin != "") {
                $datos = $datos->where('dia', '>=', $date_begin);
            }

            if (isset($campaigns_platform_ids) && count($campaigns_platform_ids) > 0) {
                $datos = $datos->whereIn('campanaid', $campaigns_platform_ids);
            }

            $results[] = $datos
                ->where("user_id", $user->id)
                ->orderBy("x")
                ->get();
        }


        switch (count($results)) {
            case 1;

                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0)
                            ),
                            "labels" => array($fields[0])
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0]
                            )
                        )
                    );
                }
                break;
            case 2:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0),
                                ((isset($results[1][0]->y)) ? $results[1][0]->y : 0)
                            ),
                            "labels" => array($fields[0], $fields[1])
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0]
                            ),
                            array(
                                "data" => ((count($results[1]) > 0) ? $results[1] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[1]
                            )
                        )
                    );
                }
                break;
            default:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(0),
                            "labels" => array("impressions")
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            "data" => json_decode('[{"x":" ","y":"0"}]'),
                            "name" => "impressions"
                        )
                    );
                }
                break;
        }

        $array = [
            "result" => $result,
            "fields_data" => $fields_data,
        ];

        return $array;
    }
    /**
     * Fourth table for CHARTS
     **/
    public function datos_dddd(int $id, Request $request)
    {

        $user = Auth::user();
        $campaigns_platform_atomo = [$id];
        $types_fields = array(
            "impresions" => "SUM(impressions) as y",
            "spend" => "ROUND(SUM(cost), 2) as y",
            "clicks" => "SUM(clicks) as y",
            "ctr" => "ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS y",
            "ecpm" => "ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS y",
        );


        $typeChart = (isset($request->datos['typeChart'])) ? $request->datos['typeChart'] : [];
        $fields = $request->get("field_name");
        $frecuencia = 1;

        //dd($campaigns_platform);
        if ($fields == "" || $fields == null) {
            $fields = array("impresions");
        } else {
            $fields = array($fields);
        }

        $date_begin = "";
        $date_end = "";
        $today = date("Y-m-d");
        $date = strtotime($today);

        $date = strtotime("-30 day", $date);
        $date_begin = date('Y-m-d', $date);


        if (isset($campaigns_platform_atomo) && count($campaigns_platform_atomo) > 0) {

            $datos_temporal = Campaign_platform_atomo::whereIn("id", $campaigns_platform_atomo)
                ->where("user_id", $user->id)
                ->get();

            $campaigns_platform_atomo_ids = array();
            foreach ($datos_temporal as $value) {
                $campaigns_platform_atomo_ids[] = $value->id;
            }
        }

        $fields_query = "   SUM(impressions) as impresions,
                            ROUND(SUM(cost), 2) as spend,
                            SUM(clicks) as clicks,
                            ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS ctr,
                            ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS ecpm";


        $results = array();

        $fields_data = DB::connection($this->db2)
            ->table('platform_campana_day');

        // building query and getting data for fields ("impressions", "spend", "crt", "ecpm", "clicks")

        if (($date_end != "") && ($date_begin != "")) {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
            $fields_data = $fields_data->where('dia', '<=', $date_end);
        } else if ($date_begin != "") {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
        }

        if (isset($campaigns_platform_atomo_ids) && count($campaigns_platform_atomo_ids) > 0) {
            $fields_data = $fields_data->whereIn('campanaid', $campaigns_platform_atomo_ids);
        }

        //Filtros TOP

        $fields_data = $fields_data
            ->selectRaw($fields_query)
            ->where("user_id", $user->id)
            ->get();

        // building query and getting data for chart
        foreach ($fields as $value) {

            $datos = DB::connection($this->db2)
                ->table('platform_campana_day');

            if ($typeChart == 'radialBar') {
                $datos = $datos->selectRaw($types_fields[$value]);
            } else {

                if (intval($frecuencia) === 1) {
                    $datos = $datos->selectRaw('dia as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('dia');
                } else if (intval($frecuencia) === 7) {
                    $datos = $datos->selectRaw('yearweek as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearweek');
                } else if (intval($frecuencia) === 30) {
                    $datos = $datos->selectRaw('yearmonth as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearmonth');
                }

            }

            if (($date_end != "") && ($date_begin != "")) {
                $datos = $datos->where('dia', '>=', $date_begin);
                $datos = $datos->where('dia', '<=', $date_end);
            } else if ($date_begin != "") {
                $datos = $datos->where('dia', '>=', $date_begin);
            }

            if (isset($campaigns_platform_atomo_ids) && count($campaigns_platform_atomo_ids) > 0) {
                $datos = $datos->whereIn('campanaid', $campaigns_platform_atomo_ids);
            }

            $results[] = $datos
                ->where("user_id", $user->id)
                ->orderBy("x")
                ->get();
        }


        switch (count($results)) {
            case 1;

                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0)
                            ),
                            "labels" => array($fields[0])
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0]
                            )
                        )
                    );
                }
                break;
            case 2:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0),
                                ((isset($results[1][0]->y)) ? $results[1][0]->y : 0)
                            ),
                            "labels" => array($fields[0], $fields[1])
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0]
                            ),
                            array(
                                "data" => ((count($results[1]) > 0) ? $results[1] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[1]
                            )
                        )
                    );
                }
                break;
            default:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(0),
                            "labels" => array("impressions")
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            "data" => json_decode('[{"x":" ","y":"0"}]'),
                            "name" => "impressions"
                        )
                    );
                }
                break;
        }
        $array = [
            "result" => $result,
            "fields_data" => $fields_data,
        ];
        return $array;
    }
    /**
     * Fifth table for CHARTS
     **/
    public function datos_ddddd(int $id, Request $request)
    {

        $user = Auth::user();
        $creatividades = [$id];
        $types_fields = array(
            "impresions" => "SUM(impressions) as y",
            "spend" => "ROUND(SUM(cost), 2) as y",
            "clicks" => "SUM(clicks) as y",
            "ctr" => "ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS y",
            "ecpm" => "ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS y",
        );


        $typeChart = (isset($request->datos['typeChart'])) ? $request->datos['typeChart'] : [];
        $fields = $request->get("field_name");
        $frecuencia = 1;

        //dd($campaigns_platform);
        if ($fields == "" || $fields == null) {
            $fields = array("impresions");
        } else {
            $fields = array($fields);
        }

        $date_begin = "";
        $date_end = "";
        $today = date("Y-m-d");
        $date = strtotime($today);

        $date = strtotime("-30 day", $date);
        $date_begin = date('Y-m-d', $date);


        if (isset($creatividades) && count($creatividades) > 0) {

            $datos_temporal = Creatividades::whereIn("id", $creatividades)
                ->where("user_id", $user->id)
                ->get();

            $creatividades_ids = array();
            foreach ($datos_temporal as $value) {
                $creatividades_ids[] = $value->id;
            }
        }

        $fields_query = "   SUM(impressions) as impresions,
                            ROUND(SUM(cost), 2) as spend,
                            SUM(clicks) as clicks,
                            ROUND(((SUM(clicks)/SUM(impressions))*100), 2) AS ctr,
                            ROUND((((SUM(clicks)/SUM(impressions))*100) * (SUM(cost)/SUM(clicks)) * 10), 2) AS ecpm";


        $results = array();

        $fields_data = DB::connection($this->db2)
            ->table('platform_campana_day');

        // building query and getting data for fields ("impressions", "spend", "crt", "ecpm", "clicks")

        if (($date_end != "") && ($date_begin != "")) {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
            $fields_data = $fields_data->where('dia', '<=', $date_end);
        } else if ($date_begin != "") {
            $fields_data = $fields_data->where('dia', '>=', $date_begin);
        }

        if (isset($creatividades_ids) && count($creatividades_ids) > 0) {
            $fields_data = $fields_data->whereIn('ad_account_id', $creatividades_ids);
        }

        //Filtros TOP

        $fields_data = $fields_data
            ->selectRaw($fields_query)
            ->where("user_id", $user->id)
            ->get();

        // building query and getting data for chart
        foreach ($fields as $value) {

            $datos = DB::connection($this->db2)
                ->table('platform_campana_day');

            if ($typeChart == 'radialBar') {
                $datos = $datos->selectRaw($types_fields[$value]);
            } else {

                if (intval($frecuencia) === 1) {
                    $datos = $datos->selectRaw('dia as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('dia');
                } else if (intval($frecuencia) === 7) {
                    $datos = $datos->selectRaw('yearweek as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearweek');
                } else if (intval($frecuencia) === 30) {
                    $datos = $datos->selectRaw('yearmonth as x , ' . $types_fields[$value]);
                    $datos = $datos->groupBy('yearmonth');
                }

            }

            if (($date_end != "") && ($date_begin != "")) {
                $datos = $datos->where('dia', '>=', $date_begin);
                $datos = $datos->where('dia', '<=', $date_end);
            } else if ($date_begin != "") {
                $datos = $datos->where('dia', '>=', $date_begin);
            }

            if (isset($creatividades_ids) && count($creatividades_ids) > 0) {
                $datos = $datos->whereIn('ad_account_id', $creatividades_ids);
            }

            $results[] = $datos
                ->where("user_id", $user->id)
                ->orderBy("x")
                ->get();
        }


        switch (count($results)) {
            case 1;

                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0)
                            ),
                            "labels" => array($fields[0])
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0]
                            )
                        )
                    );
                }
                break;
            case 2:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(
                                ((isset($results[0][0]->y)) ? $results[0][0]->y : 0),
                                ((isset($results[1][0]->y)) ? $results[1][0]->y : 0)
                            ),
                            "labels" => array($fields[0], $fields[1])
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            array(
                                "data" => ((count($results[0]) > 0) ? $results[0] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[0]
                            ),
                            array(
                                "data" => ((count($results[1]) > 0) ? $results[1] : json_decode('[{"x":" ","y":"0"}]')),
                                "name" => $fields[1]
                            )
                        )
                    );
                }
                break;
            default:
                if ($typeChart == 'radialBar') {
                    $result = array(
                        "typeChart" => $typeChart,
                        "options" => array(
                            "series" => array(0),
                            "labels" => array("impressions")
                        )
                    );
                } else {
                    $result = array(
                        "typeChart" => $typeChart,
                        "data" => array(
                            "data" => json_decode('[{"x":" ","y":"0"}]'),
                            "name" => "impressions"
                        )
                    );
                }
                break;
        }
        $array = [
            "result" => $result,
            "fields_data" => $fields_data,
        ];
        return $array;
    }

    public function table_first_edit($id,Request $request){

        Validator::make($request->all(), $this->rulesCustomerUpdate, $this->getValidationErrors())
            ->validate();

        $customer = Customer::find($id);

        if($this->customerUpdate($customer, $request->all())){
            return ["alert"=>"success", "msg"=>"Customer updated successfully."];
        }else{
            return ["alert"=>"danger", "msg"=>"Could not update the customer."];
        }

    }
    public function table_second_edit($id,Request $request){

        Validator::make($request->all(), $this->rulesCampaignUpdate, $this->getValidationErrors())
            ->validate();

        $campaign = Campaign::find($id);

        if($this->campaignUpdate($campaign, $request->all())){
            return ["alert"=>"success", "msg"=>"Campaign updated successfully."];
        }else{
            return ["alert"=>"danger", "msg"=>"Could not update the campaign."];
        }

    }
    public function table_third_edit($id,Request $request){

        Validator::make($request->all(), $this->rulesCampaignPlatformUpdate, $this->getValidationErrors())
            ->validate();

        $campaignPlatform = Campaign_platform::find($id);

        if($this->campaignPlatformUpdate($campaignPlatform, $request->all())){
            return ["alert"=>"success", "msg"=>"Update successfully."];
        }else{
            return ["alert"=>"danger", "msg"=>"Could not update."];
        }

    }
    public function table_fourth_edit($id,Request $request){

        Validator::make($request->all(), $this->rulesCampaignPlatformAtomoUpdate, $this->getValidationErrors())
            ->validate();

        $campaignPlatformAtomo = Campaign_platform_atomo::find($id);

        if($this->campaignPlatformAtomoUpdate($campaignPlatformAtomo, $request->all())){
            return ["alert"=>"success", "msg"=>"Update successfully."];
        }else{
            return ["alert"=>"danger", "msg"=>"Could not update."];
        }

    }
    public function table_fifth_edit($id,Request $request){

        Validator::make($request->all(), $this->rulesCreatividadesUpdate, $this->getValidationErrors())
            ->validate();

        $creatividades = Creatividades::find($id);

        if($this->creatividadesUpdate($creatividades, $request->all())){
            return ["alert"=>"success", "msg"=>"Update successfully."];
        }else{
            return ["alert"=>"danger", "msg"=>"Could not update."];
        }

    }
    public function customerUpdate($customer, array $data){

        $customer->name = $data['name_input'];
        $customer->spend = $data['spend_input'];
        $customer->impressions = $data['impressions_input'];
        $customer->clicks = $data['clicks_input'];
        $customer->cpc = $data['cpc_input'];
        $customer->cpm = $data['cpm_input'];
        $customer->ctr = $data['ctr_input'];
        $customer->conversions = $data['conversions_input'];

        return $customer->save();
    }
    public function campaignUpdate($campaign, array $data){

        $campaign->name = $data['name_input'];
        $campaign->budget = $data['budget_input'];
        $campaign->spent = $data['spend_input'];
        $campaign->impression = $data['impressions_input'];
        $campaign->clicks = $data['clicks_input'];
        $campaign->cpc = $data['cpc_input'];
        $campaign->cpm = $data['cpm_input'];
        $campaign->ctr = $data['ctr_input'];
        $campaign->conversion = $data['conversions_input'];

        return $campaign->save();
    }
    public function campaignPlatformUpdate($campaignPlatform, array $data){

        $campaignPlatform->name = $data['name_input'];
        $campaignPlatform->budget = $data['budget_input'];
        $campaignPlatform->spent = $data['spend_input'];
        $campaignPlatform->impression = $data['impressions_input'];
        $campaignPlatform->clicks = $data['clicks_input'];
        $campaignPlatform->cpc = $data['cpc_input'];
        $campaignPlatform->cpm = $data['cpm_input'];
        $campaignPlatform->ctr = $data['ctr_input'];
        $campaignPlatform->conversion = $data['conversions_input'];

        return $campaignPlatform->save();
    }
    public function campaignPlatformAtomoUpdate($campaignPlatformAtomo, array $data){

        $campaignPlatformAtomo->name = $data['name_input'];
        $campaignPlatformAtomo->budget = $data['budget_input'];
        $campaignPlatformAtomo->spent = $data['spend_input'];
        $campaignPlatformAtomo->impression = $data['impressions_input'];
        $campaignPlatformAtomo->clicks = $data['clicks_input'];
        $campaignPlatformAtomo->cpc = $data['cpc_input'];
        $campaignPlatformAtomo->cpm = $data['cpm_input'];
        $campaignPlatformAtomo->ctr = $data['ctr_input'];
        $campaignPlatformAtomo->conversion = $data['conversions_input'];

        return $campaignPlatformAtomo->save();
    }
    public function creatividadesUpdate($creatividades, array $data){

        $creatividades->title = $data['name_input'];
        $creatividades->budget = $data['budget_input'];
        $creatividades->impressions = $data['impressions_input'];
        $creatividades->clicks = $data['clicks_input'];
        $creatividades->ctr = $data['ctr_input'];

        return $creatividades->save();
    }

    public function getValidationErrors()
    {
        return [
            'clicks_input.integer' => 'The clicks input must be numeric value.',
            'clicks_input.required' => 'The clicks input is required.',

            'spend_input.integer' => 'The spend input must be numeric value.',
            'spend_input.required' => 'The spend input is required.',

            'impressions _input.integer' => 'The impressions input must be numeric value.',
            'impressions _input.required' => 'The impressions input is required.',

            'cpc _input.integer' => 'The cpc input must be numeric value.',
            'cpc _input.required' => 'The cpc input is required.',

            'cpm _input.integer' => 'The cpm input must be numeric value.',
            'cpm _input.required' => 'The cpm input is required.',

            'ctr_input.integer' => 'The ctr input must be numeric value.',
            'ctr_input.required' => 'The ctr input is required.',

            'conversions_input.integer' => 'The conversions input must be numeric value.',
            'conversions_input.required' => 'The conversions input is required.',

            'name_input.integer' => 'The name input must be an string.',
            'name_input.required' => 'The name input is required.',

            'budget_input.integer' => 'The budget input must be numeric value.',
            'budget_input.required' => 'The budget input is required.',
        ];
    }
     /**
     * Función que elimina duplicados y une las dos búsquedas en un único array ordenado.
     * @param $array Es el array que queremos quitar los duplicados y ordenar.
     * @param $key Clave del array que guardaremos para revisar los duplicados. ej: 'id'
     * @param $order Clave del array que queramos ordenar. ej: 'name'
     * @return array Array final sin duplicados.
     * @author Vladimir
     */

    public function unique_multidim_array($array, $key,$order) {
        $temp_array = array();
        $i = -1;
        $key_array = array();
        $temp_array2 =array();
        $temp_array3 =array();

        foreach($array as $val) {
            $i++;
            if(array_key_exists('atomoid', $val) and !in_array($val[$key], $key_array) ){
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }else if (!array_key_exists('atomoid', $val) and !in_array($val[$key], $key_array)){
                $temp_array[$i] = $val;
            }
        }
        $temp_array2 = array_values(array_unique($temp_array2,SORT_REGULAR));
        $x = 0;
        foreach($temp_array2 AS $val){
            if(!in_array($val[$key], $key_array)){
                $temp_array3[$x] = $val;
                $x++;
            }
        }
        $temp_array3 = array_values(array_merge((array)$temp_array3, (array)$temp_array));
        foreach ($temp_array3 as $name => $row) {
            $nombre[$name] = $row[$order]; //Desde aqui controlamos como queremos ordenar el array final
        }
        array_multisort($nombre, 'SORT_ASC', $temp_array3);
        return $temp_array3;
    }

 /**
     * Función que une las dos búsquedas en un único array con duplicados!!.
     * @param $object1 Primer objeto.
     * @param $object2 Segundo objeto
     * @param $keysstats los elementos que no existen al juntar los dos objetos que quieres rellenar
     * @return array Array final con los dos objetos juntos pero con duplicados.
     * @author Vladimir
     */

    public function merge_object($object1, $object2,$keysstats) {
        $atomos_salida=[];
        $array = [];
        $key = 0;
        foreach ($object1 as $item1){

            foreach ($object2 as $item2){
                if($item2->id == $item1->atomoid){
                    $atomos_salida[$key] = array_merge((array)$item1, (array)$item2);
                    $key ++;
                    continue;
                }else if($item2->id != $item1->atomoid){
                    $atomos_salida[$key] = array_merge((array)$keysstats, (array)$item2);
                    $key ++;
                    continue;
                }
            }

        }
      return $atomos_salida;
    }
}