<?php

namespace App\Http\Controllers\Api;

use App\Dashboard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null, Request $request)
    {

        $dashboards = Dashboard::where('user_id', $request->user()->id);
        if ($id != null) {
            $dashboards->where('id', $id);
        }
        return $dashboards->get();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $msj="";

        if (isset($request->datos['default']) && $request->datos['default'] == true ) {
            $dashs = Dashboard::where('configuration', 'LIKE', '%' . '"default":"true"' . '%')->where("user_id", $request->user()->id)->get();
            foreach ($dashs as $dash) {
                $dashTemp = json_decode($dash->configuration);
                $dashTemp->default = "false";
                $msj .= ' dash -> '.$dash->configuration . '////';
                $dash->save();
            }
        }

        $item = new Dashboard();
        $item->name = $request->nombre;
        $item->user_id = $request->user()->id;
        $item->configuration = json_encode($request->datos);
        $item->type = $request->type;     
        $item->save();

        $selected = ""; 
        if ($request->datos['default'] == "true"){
            $selected = "** "; 
        }

        return ['id' => $item->id, 'name' => $selected.$item->name, 'data' => $item->configuration, 'msj' => $request->datos['default']  ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function show(Dashboard $dashboard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function edit(Dashboard $dashboard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dashboard $dashboard)
    {
    //
        $item = Dashboard::where('user_id',  $request->user()->id)
      ->where("public_id", $request->id)->firstOrFail();
  
        $item->configuration = json_encode($request->datos);
        $item->save();
        return ['id' => $item->public_id, 'name' => $item->name, 'data' => $item->configuration];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dashboard $dashboard)
    {
        //
    }
}
