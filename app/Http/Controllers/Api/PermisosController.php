<?php
// controller creado para ver los registros, no se usa, anular en algun momento
namespace App\Http\Controllers\Api;

use App\Permisos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermisosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = is_null($request->get('length') ) ? 10 :$request->get('length');
        $offset = $request->input('start') ? : 1;
        $page = intval( $offset / $limit) + 1;
        $keyword = '';
        if( !is_null( $request->get('search') )  ){ $keyword = $request->get('search')['value']; }
        
        
        
        $permisos = Permisos::where( function ($query) use ($keyword){ $query->whereRaw("user_id like  '%{$keyword}%'"); })
                    ->paginate( $limit , ['*'] , 'page', $page  );
               
        
        $permisos->getCollection()->transform(function ($value) {
            $data = [
                        'user_id'             => $value->user_id,
                        'plataformas'         => json_decode($value->plataformas),
                        'num_plataformas'     => $value->num_plataformas,
                        'cross_plataformas'   => $value->cross_plataformas,
                        'crear_usuarios'      => $value->crear_usuarios,
                        'compartir_reportes'  => $value->compartir_reportes,
                        'importe_maximo_mes'  => $value->importe_maximo_mes
                    ];
            return $data;
        });
        
        $result = [
            "draw"              => $request->input('draw') ? : 1,
            "recordsTotal"      => $permisos->total(),
            "recordsFiltered"   => $permisos->total(),
            "data"              => $permisos->items(),
            "length"            => 10
        ];
        return $result;
    }
    
}
