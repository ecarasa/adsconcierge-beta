<?php

namespace App\Http\Controllers\Api;

use App\Campaign_platform;
use App\Campaign_platform_atomo;
use App\Creatividades;
use App\Customer;
use App\Campaign;
use App\Creativity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Illuminate\Support\Facades\Auth;

use DB;
use Config;

use Illuminate\Support\Facades\Log;
use Illuminate\Pipeline\Pipeline;

use App\Events\newCronEvent;
use App\Events\newCronJobEvent;
use App\Events\TaskCreate;

use App\Events\UpdateEntity;
use App\Events\CloneEntity;


class EntityController extends Controller{

    public function getCustomer(Request $request, $id){
        
        $customer = Customer::where('public_id', $id)->firstOrFail();
		$ads_accounts = DB::table('ads_accounts')->select('public_id', 'name', 'platform')->where('user_id', Auth::user()->id)->orderBy('platform')->get();
        $properties_accounts = DB::table('properties_accounts')->select('public_id', 'name', 'platform')->where('user_id', Auth::user()->id)->orderBy('platform')->get();
		$platforms = Config::get('app.CONST_PLATFORMS');


		$list = array();
		foreach($ads_accounts as $value){
			$list[$value->platform][] = array( "public_id" => $value->public_id, "name" => $value->name );	
		}
		$ads_accounts = $list;
		
		$list = array();
		foreach($properties_accounts as $value){
			$list[$value->platform][] = array( "public_id" => $value->public_id, "name" => $value->name );	
		}
		$properties_accounts = $list;


        $data = array(
            'customer' => $customer,
            'ads_accounts' => $ads_accounts,
            'properties_accounts' => $properties_accounts,
            'platforms' => $platforms
        );

        return $data;

    }

    public function getCampaign(Request $request, $id){
                
        $data = array();
        $dataACC = array();

        $campana = Campaign::where('public_id', $id)->firstOrFail();
        $customer = Customer::where('id', $campana->customer_id)->select('public_id','name')->firstOrFail();

        $data['campaign'] = $campana;   
        $data['plataformas'] = json_decode($campana->plataformas);    
        $data['customer'] = $customer;
        

     /*    $data['devices'] = DB::table('mapeo_x_plataforma')->where('campo', 'devices')->orderBy('id')->get();
        $data['url_placeholder'] = DB::table('app_settings')->where('keyname', 'url_placeholder')->get();
        $data['gender'] = DB::table('mapeo_x_plataforma')->where('campo', 'gender')->orderBy('id')->get();
        $data['conectivity'] = DB::table('mapeo_x_plataforma')->where('campo', 'conectivity')->orderBy('id')->get();
        $data['customers'] = DB::table('customers')->where('user_id', Auth::user()->id)->orderBy('name')->get();
        $data['placements'] = DB::table('placements')->join('platforms', 'placements.platform', '=', 'platforms.id')->orderBy('platform')->select('placements.id','placements.public_id', 'placements.name', 'platforms.name as platforms_name')->get();
        */
        return $data;
        
    }

    public function getCampaignPlatform(Request $request, $id){
                
        $data = array();
        $dataACC = array();

        $campana = Campaign_platform::where('public_id', $id)->firstOrFail();
        
        $data['campaign'] = $campana;   
        
        
        return $data;
        
    }

    public function getAtom(Request $request, $id){
        
        $data = array();

        $atom = DB::table('campaigns_platform_atomo')->where('public_id', $id)->first();        
        $languages = DB::table('settings_matrix')->whereIn('uuid', json_decode($atom->language))->select('uuid','nombre')->get();

        $data['atom'] =  $atom;
        $data['atom_language'] =  $languages;
        $data['devices'] = DB::table('mapeo_x_plataforma')->where('campo', 'devices')->orderBy('id')->get();        
        $data['gender'] = DB::table('mapeo_x_plataforma')->where('campo', 'gender')->orderBy('id')->get();
        $data['conectivity'] = DB::table('mapeo_x_plataforma')->where('campo', 'conectivity')->orderBy('id')->get();
    
        return $data;
        
    }
    
    public function getCreativity(Request $request, $id) {

        $data = array();

        $creativity = DB::table('creatividades')->where('public_id', $id)->first();        
        //$languages = DB::table('settings_matrix')->whereIn('uuid', json_decode($atom->language))->select('uuid','nombre')->get();

        $data['creativity'] =  $creativity;
        
    
        return $data;

    }

    public function updateCampaignPlatform(Request $request){

        $campaign =  Campaign_platform::where('public_id', $request->get('campaign_platform_id'))->firstOrFail();  // validamos que exista la campaña

        //dd($campaign);
    
        $rowdata = [
        'name' => $request->get('campaign_platform_name'),
        'budget' => $request->get('campaign_platform_budget'),
        'budget_total' => $request->get('campaign_platform_budget_total')
        ];

        //dd($rowdata);
        DB::table('campaign_platform')->where('public_id', $request->get('campaign_platform_id'))->update($rowdata);
        //$this->createCronJob('campaign', 'update_field',  $campaign->id);

        return true;
        
    }

    public function updateCampaign(Request $request){

        $campaign =  Campaign::where('public_id', $id)->firstOrFail();  // validamos que exista la campaña

        $budget_by_platform = array();
        if ($request->has('budget_split_all')){
           foreach($request->get('budget_split_all') as $value) {
               $value = json_decode($value);
               $budget_by_platform[$value->platform]= $value;
           }
        }

        $rowdata = [
        'name' => $request->input('name'),
        'run_only_on_schedule' =>   $request->get('run_only_on_schedule_serialized'),
        'spend_on_each_ad' => $request->get('spend_on_each_ad'),
        'impressions_per_day' =>$request->get('impressions_per_day'),
        'budget_target_diary' => $request->get("budget_target_diary"),
        'budget_target_diary_type' => $request->get("budget_target_diary_type"),
        'budget_target_total' =>$request->get("budget_target_total"),
        'budget_target_total_type' => $request->get("budget_target_total_type"),
        'budget' => $request->get('budget'),
        'budgetdistribution' => json_encode($budget_by_platform),
        'plataformas' => $request->get("rrss")
        ];

        //dd($rowdata);
        DB::table('campaign')->where('id', $campaign->id)->update($rowdata);
        $this->createCronJob('campaign', 'update_field',  $campaign->id);

        return true;
        
    }

    public function updateCreativity(Request $request){
        
        $creas = DB::table('creatividades')->where('public_id', $request->get('atom_id'))->first();        

        if ($creas){
        

            $call_to_actions = array();
            if ($request->has('call_actions')){
                foreach($request->get('call_actions') as $value) {
                    $value = json_decode($value);
                    array_push($call_to_actions, $value);
                }
            }
    
            $headline = array();
            if ($request->has('headlines')){
                foreach($request->get('headlines') as $value) {
                    $value = json_decode($value);
                    array_push($headline, $value);
                }
            }
    
            $urls = array();
            if ($request->has('urls')){
                foreach($request->get('urls') as $value) {
                    $value = json_decode($value);
                    array_push($urls, $value);
                }
            }
    
            $text_links = array();
            if ($request->has('text_links')){
                foreach($request->get('text_links') as $value) {
                    $value = json_decode($value);
                    array_push($text_links, $value);
                }
            }
    
            $ad_texts = array();
            if ($request->has('ad_texts')){
                foreach($request->get('ad_texts') as $value) {
                    $value = json_decode($value);
                    array_push($ad_texts, $value);
                }
            }



            $rowdata = [
                'name' => $request->input('creas_name'),
                'budget' => $request->get('creas_budget'),

                'banner' =>json_encode(  $headline),
                //'creativity_img_ids' => $request->get('images_uploaded') ? $request->get('images_uploaded') : null,
                'content' => json_encode( $ad_texts),
                'calltoaction' => json_encode( $call_to_actions),
                'url' => json_encode( $urls),
                'link_display' => $request->get('link_display') ? $request->get('link_display') : null,
                'linkdescription' => json_encode( $text_links),
    
    
                ];


            if ( DB::table('creatividades')->where('public_id', $request->get('creas_id'))->update($rowdata) ){

                $this->createCronJob('creatividades', 'update_field',  $creas->id);

                $array['status'] = true;
                $array['message'] = "Creativity edited succesfully.";
                return response()->json($array);
            }else{
                $array['status'] = false;
                $array['message'] = "Error while updating";
                return response()->json($array);

            }

        }

        return false;
        
    }

    public function updateAtom(Request $request){
        
        $atom = DB::table('campaigns_platform_atomo')->where('public_id', $request->get('atom_id'))->first();        

        if ($atom){
        
            $rowdata = [
                'name' => $request->input('atom_name'),
                'gender' => json_encode($request->get('gender')),
                'age' => $request->get('ages'),
                'language' => $request->get('language'),
                'conectivity' => $request->get('conectivity') ,
                'device' => $request->get('devices') ,
                'budget' => $request->get('atom_budget'),
                'targeting_include' => $request->get('interest_list_in'),
                'targeting_exclude' => $request->get('interest_list_ex'), ];


            if ( DB::table('campaigns_platform_atomo')->where('public_id', $request->get('atom_id'))->update($rowdata) ){
                
                $this->createCronJob('atom', 'update_field',  $atom->id);

                $array['status'] = true;
                $array['message'] = "Atom edited succesfully.";
                return response()->json($array);
            }else{
                $array['status'] = false;
                $array['message'] = "Error while updating";
                return response()->json($array);

            }

        }

        return false;
        
    }

    public function duplicateEntity(Request $request){

        DB::connection()->enableQueryLog();
 
             
        $assignTo = $request->get('new_campaign');
        $items =  $request->get('items');
        $affected = 0;

     
        foreach($items as $value){
            
            $type = $value['type'];
            $itemid = $value['itemid'];
            $campaign = $value['campaign'];

            switch($type){

                case "campaigns":

                    if(isset($assignTo) and $assignTo != ""){
                        $customer = $assignTo;
                    }

                    $eventData = array( "entity" => "campaigns", 
                                        "assignTo" => $assignTo, 
                                        "campaign" => $campaign,
                                        
                                        "item_type" => $value['type'],
                                        "item_id" => $value['itemid'],
                                       // "item_type" => $id,   
                                    );

                    event(new CloneEntity($eventData));


                    $response["success"] = true;
                    return response()->json($response);

                    break;

                case "creativity":

                    if(isset($assignTo) and $assignTo != ""){
                        $campaign = $assignTo;
                    }

                    $eventData = array( "entity" => "creativity", 
                                        "assignTo" => $assignTo, 
                                        "campaign" => $campaign,
                                        "item_type" => $id, 
                                        "item_id" => $id, 
                                        "item_type" => $id,   
                                    );

                    event(new CloneEntity($eventData));

                    $response["success"] = true;
                    return response()->json($response);
                    break;

                case "atomo":

                    if(isset($assignTo) and $assignTo != ""){
                        $campaign = $assignTo;
                    }

                    $response["success"] = true;
                    $response["data"] = $new;
                    $response["data_old"] = $old;

                    return response()->json($response);
                    break;
            }


        }
 
            
 
     }

   
    public function update(Request $request){

        /** campaña,  campañan_platform,  atomo, creatividad, platform, etc) */

        DB::enableQueryLog();
        
        $affected = 0;
        $fieldsUpdatables = array('name', 'budget', 'status');

        // from request
        $id =  $request->get('id');
        $entity = $request->get('entity');
        $entityField = $request->get('field');
        
        // comprobamos si podemos o no editar un campo
        if (!in_array($entityField, $fieldsUpdatables)) {
            $result = [ "status" => false, "msj" => 'Invalid request.' ];
            return $result;
        }

        // filter by FIELD to update
        switch($entityField){
            
            case "status":
                $actionJobType = 'update_status';
                if($request->get('check') == "true"){ $entityFieldValue = "ACTIVE"; }else{$entityFieldValue = "PAUSED"; }
                break;

            case "name":
                $actionJobType = 'update_field';
                $entityFieldValue = $request->get('value');
                break;

            case "budget":
                $actionJobType = 'update_budget';
                $entityFieldValue = $request->get('value');

                break;
            
            default:
                echo 'Error';
                die();
                break;

        }


        switch($entity){

            case "customer":

                if ( $actionJobType == 'update_status' ){
                    
                    if ($entityFieldValue == 'PAUSED'){ $entityFieldValue = 'INACTIVE'; }else{ $entityFieldValue = 'ACTIVE'; }

                    $eventData = array("entity" => "customer", "entity_table" => "customers", "entity_id" => $id, "entity_field" => $entityField,"entity_field_value" => $entityFieldValue);
                    event(new UpdateEntity($eventData));

                }

                break;

            case "campaigns": // OK Event

                $eventData = array("entity" => "campaigns", "entity_table" => "campaigns", "entity_id" => $id, "entity_field" => $entityField,"entity_field_value" => $entityFieldValue);
                event(new UpdateEntity($eventData));
                //die();

                break;

            case "campaigns_platform":

                $eventData = array("entity" => "campaigns_platform", "platform" => $request->get('platform') ,"entity_table" => "campaigns_platform", "entity_id" => $id, "entity_field" => $entityField,"entity_field_value" => $entityFieldValue);
                event(new UpdateEntity($eventData));

                break;
            case "platform":
                // nothing yet


                break;
            case "atom":

                $eventData = array("entity" => "atom", "platform" => $request->get('platform') ,"entity_table" => "campaigns_platform_atomo", "entity_id" => $id, "entity_field" => $entityField,"entity_field_value" => $entityFieldValue);
                event(new UpdateEntity($eventData));

                break;
            case "creativity":

                $eventData = array("entity" => "creativity", "platform" => $request->get('platform') ,"entity_table" => "creatividades", "entity_id" => $id, "entity_field" => $entityField,"entity_field_value" => $entityFieldValue);
                event(new UpdateEntity($eventData));

                break;
        
        }

        //if ($affected > 0){
            $result = [ 
                "status" => true, 
                "msj" => 'Updated',
                //"rows" => $affected,
                //"queries"=> DB::getQueryLog()
            ];
        /* }else{
            $result = [ 
                "status" => false, 
                "rows" => $affected,
                "msj" => 'No rows affected.',
                //"queries"=> DB::getQueryLog()
            ];
        } */

        return $result;

    }

    public function createCronJob($entity, $actionType, $idExecute){

        /***** actual operationes in cronjob table *
         * 
         * daily_stats - update_status - update_budget - update_field
         * 
         * 
         * 
         * */
            
        return DB::connection('stats')
                ->table('adsconcierge_stats.background_job')
                ->insert([
                    'type' => $entity, 
                    'action_type'=> $actionType, 
                    'subject_id'=> $idExecute,
                    'next_execution'=>date('Y-m-d H:i:s')
                ]);

    }




}