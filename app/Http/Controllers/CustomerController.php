<?php

namespace App\Http\Controllers;

use App\Customer;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Config;
use \Validator;

class CustomerController extends Controller
{




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$id_user = Auth::id();
		
		$validator = Validator::make($request->all(), [
            'name'  => 'required|string|max:100'
        ]);

        if($validator->fails()){
			$array['status'] = false;
			$array['message']  = $validator->messages();;
			return response()->json($array);
        }
		
		$rrss = $request->get('rrss');
		
		$ads = array();
		$properties = array();
		$fee = array();
		
		if(!is_null($rrss) && count($rrss)>0){
			foreach($rrss as $key => $value){	
				if(isset($value['ads'])){
					$ads[$key] = $value['ads'];

					// guardamos en la relacion de ads_accounts
					$ads_customers = DB::table('ads_accounts')->whereIn('public_id', $value['ads'] )->get()->toArray();
						
					foreach ($ads_customers as $addc){
						$customersArray = json_decode($addc->customers);

						if (is_array($customersArray)){
							if (!in_array($request->get('client_id'), $customersArray) ){
								array_push($customersArray,$request->get('client_id'));
							}
						}else{
							$customersArray = array();
							array_push($customersArray,$request->get('client_id'));
						}

						$rowsaffectedddd = DB::table('ads_accounts')->where('id', $addc->id)->update( ['customers' => json_encode($customersArray)] );
						//$queries = DB::getQueryLog();
					}

				}
				if(isset($value['properties'])){
					$properties[$key] = $value['properties'];

					// guardamos en la relacion de ads_accounts
					$tmp = DB::table('properties_accounts')->whereIn('public_id', $value['properties'] )->get()->toArray();
												
					//dd($tmp);

					foreach ($tmp as $addc){
						$customersArray = json_decode($addc->customers);

						if (is_array($customersArray)){
							if (!in_array($request->get('client_id'), $customersArray) ){
								array_push($customersArray,$request->get('client_id'));
							}
						}else{
							$customersArray = array();
							array_push($customersArray,$request->get('client_id'));
						}

						$rowsaffectedddd = DB::table('properties_accounts')->where('id', $addc->id)->update( ['customers' => json_encode($customersArray)] );
						//$queries = DB::getQueryLog();
					//	dd($rowsaffectedddd);
					}




				}
				if(isset($value['fee'])){
					$fee[$key] = $value['fee'];
				}
			}		
		}

		if ($request->get('default_customer') == 'on' ) {
			$isDefault = 'Y';
			$defaultText= ' (as default)';
			Customer::where('user_id', $id_user)->update( ['isdefault' => 'N'] );
		}else{

			$isDefault = 'N';
			$defaultText= '';

		}

		$customer = Customer::create([  'name'     		=>  $request->get('name'),
										'isdefault'     =>  $isDefault,
										'user_id' 		=> 	$id_user,
										'ads_accounts'	=>  json_encode($ads),
										'properties'    =>  json_encode($properties),
										'fees'      	=>  json_encode($fee), ]);

        if ($customer->save()){
			$array['status'] = true;
			$array['message'] = "Customer " .$customer->name . $defaultText ." created succesfully.";
			return response()->json($array);
		}else{
			$array['status'] = false;
			$array['message'] = "Error while creating customer, try again.";
			return response()->json($array);
		}
        
		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {

		// ocultamos
		//user_name, conversiones, ctr, cpm, clicks, impressions  y spend que lo tomamos de la tabla campaings
		// creamos
		//Total Spend: (es sum de la columan cost o spend de tabla campaigns)
		//Active Campaigns: es count(1) where status=active de la tabla campaigns
		//Total Campaigns:  es count(1) de la tabla campaigns


		$id_user = Auth::id();
		
		$ads_accounts = DB::table('ads_accounts')->select('public_id', 'name', 'platform')->where('user_id', $id_user)->orderBy('platform')->get();
		$properties_accounts = DB::table('properties_accounts')->select('public_id', 'name', 'platform')->where('user_id', $id_user)->orderBy('platform')->get();
		//$platforms = DB::table('platforms')->whereIn('id', [1,2,3,4,5,10,12])->get();
		$platforms = (object) Config::get('app.CONST_PLATFORMS');


		$list = array();
		foreach($ads_accounts as $value){
			$list[$value->platform][] = array( "public_id" => $value->public_id, "name" => $value->name );	
		}
		$ads_accounts = $list;
		
		$list = array();
		foreach($properties_accounts as $value){
			$list[$value->platform][] = array( "public_id" => $value->public_id, "name" => $value->name );	
		}
		$properties_accounts = $list;
		

        return view('template.customers',
		[
	    	'title' 		=>	'Customers',
	    	'newElement'	=>	'New customer',
			'columns' 		=>	[ 'Actions', 'id', 'name', 'isdefault','status', 'cpc', "total_spend", "active_campaigns", "total_campaigns" ],
	    	//'columns' 		=>	[ 'Actions', 'id', 'name', 'spend', 'isdefault', 'user_name', 'impressions', 'clicks', 'cpc', 'cpm', 'ctr', 'conversions' ],
	    	'api'			=>	'/api/getClients',
	    	'base' 			=>	'customers',
	    	'entity'		=>	'Customers',

			'ads_accounts' => $ads_accounts,
			'properties_accounts' => $properties_accounts,
			'form_action' => route('createclient'),
			'platforms' => $platforms,

	    ]);

    }
	
	 /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function new(Request $request)
    { 

		// OLD 
		
		$id_user = Auth::id();
		
		$ads_accounts = DB::table('ads_accounts')
				->select('public_id', 'name', 'platform')
				->where('user_id', $id_user)
				->orderBy('platform')
				->get();
		
		$properties_accounts = DB::table('properties_accounts')
				->select('public_id', 'name', 'platform')
				->where('user_id', $id_user)
				->orderBy('platform')
				->get();
		

		$list = array();
		foreach($ads_accounts as $value){
			$list[$value->platform][] = array( "public_id" => $value->public_id, "name" => $value->name );	
		}
		$ads_accounts = $list;
		
		$list = array();
		foreach($properties_accounts as $value){
			$list[$value->platform][] = array( "public_id" => $value->public_id, "name" => $value->name );	
		}
		$properties_accounts = $list;
		

		$data = [ 
				'title' => 'New Client', 
				'client' => '',
				'form_action' => route('createclient'),
				'ads_accounts' => $ads_accounts,
				'properties_accounts' => $properties_accounts,
			];


        return view('template.form_customers', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(string $id, Customer $customer)
    {
		$id_user = Auth::id();
		
		$client = Customer::where('public_id', '=', $id)
			->where('user_id', '=', $id_user)
			->first();
		
		if(is_null($client)){
			return redirect()->route('clientslist');
		}
				
		$ads_accounts = DB::table('ads_accounts')->select('public_id', 'name', 'platform')->where('user_id', $id_user)->orderBy('platform')->get();
		
		$properties_accounts = DB::table('properties_accounts')->select('public_id', 'name', 'platform')->where('user_id', $id_user)->orderBy('platform')->get();
		
		$platforms = DB::table('platforms')->whereIn('id', [1,2,3,4,5,10,12])->get();

		$list = array();
		foreach($ads_accounts as $value){
			$list[$value->platform][] = array( "public_id" => $value->public_id, "name" => $value->name );	
		}		
		$ads_accounts = $list;
		
		$list = array();
		foreach($properties_accounts as $value){
			$list[$value->platform][] = array( "public_id" => $value->public_id, "name" => $value->name );	
		}
		$properties_accounts = $list;
		
		
		$data = [ 
				'title' => 'Edit Customer | '.$client->name, 
				'client' => $client, 
				'form_action' => route('updateclient'),
				'ads_accounts' => $ads_accounts,
				'properties_accounts' => $properties_accounts,
				'platforms' => $platforms,

			];

        return view('template.form_customers', $data);
		

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
    
		$id_user = Auth::id();
		
		$validator = Validator::make($request->all(), [
            'client_id'  => 'required'
        ]);



        if($validator->fails()){
			$array['status'] = false;
			$array['message']  = $validator->messages();;
			return response()->json($array);
        }

		$customer = Customer::where('public_id', $request->get('client_id'))->firstOrFail();

		if ($customer != null){
	
			$rrss = $request->get('rrss');
		
			$ads = array();
			$properties = array();
			$fee = array();
			
			if(!is_null($rrss) && count($rrss)>0){
				foreach($rrss as $key => $value){	
					if(isset($value['ads'])){
						$ads[$key] = $value['ads'];
						
						$ads_customers = DB::table('ads_accounts')->whereIn('public_id', $value['ads'] )->get()->toArray();
						
						foreach ($ads_customers as $addc){

							$customersArray = json_decode($addc->customers);

							if (is_array($customersArray)){
								if (!in_array($request->get('client_id'), $customersArray) ){
									array_push($customersArray,$request->get('client_id'));
								}
							}else{
								$customersArray = array();
								array_push($customersArray,$request->get('client_id'));
							}

							$rowsaffectedddd = DB::table('ads_accounts')->where('id', $addc->id)->update( ['customers' => json_encode($customersArray)] );
							//$rowsaffectedddd = DB::table('customers')->where('public_id', $request->get('client_id'))->update( ['customers' => json_encode($customersArray)] );

							//$queries = DB::getQueryLog();

						}

					}
					if(isset($value['properties'])){
						
						$properties[$key] = $value['properties'];
						// AL IGUAL QUE EN ADS_ACCOUNTS, DEBERIAMOS GUARDAR LA ASOCIACION EN AMBAS TABLAS.
						// pero LA TABLA PROPERTIES O PROPERTIES RELATION NO TIENE CAMPO PARA SALVAR ESTO.

							// guardamos en la relacion de ads_accounts
							$tmp = DB::table('properties_accounts')->whereIn('public_id', $value['properties'] )->get()->toArray();
							
							//dd($tmp);
							
							foreach ($tmp as $addc){
								$customersArray = json_decode($addc->customers);

								if (is_array($customersArray)){
									if (!in_array($request->get('client_id'), $customersArray) ){
										array_push($customersArray,$request->get('client_id'));
									}
								}else{
									$customersArray = array();
									array_push($customersArray,$request->get('client_id'));
								}

								$rowsaffectedddd = DB::table('properties_accounts')->where('id', $addc->id)->update( ['customers' => json_encode($customersArray)] );
								//$queries = DB::getQueryLog();
							//	dd($rowsaffectedddd);
							}





					}
					if(isset($value['fee'])){
						$fee[$key] = $value['fee'];
					}
				}		
			}
	
			if ($request->get('default_customer') == 'on' ) {
				$isDefault = 'Y';
				$defaultText= ' (as default)';
				Customer::where('user_id', $id_user)->update( ['isdefault' => 'N'] );
			}else{
				$isDefault = 'N';
				$defaultText= '';
			}
	
			/*$customer = Customer::create([  'name'     		=>  $request->get('name'),
											'isdefault'     =>  $isDefault,
											'user_id' 		=> 	$id_user,
											'ads_accounts'	=>  json_encode($ads),
											'properties'    =>  json_encode($properties),
											'fees'      	=>  json_encode($fee), ]);*/

			$customerAffectedRows = Customer::where('public_id', $request->get('client_id'))
																->update( 
																	[  
																		'name'     		=>  $request->get('name'),
																		'isdefault'     =>  $isDefault,
																		'user_id' 		=> 	$id_user,
																		'ads_accounts'	=>  json_encode($ads),
																		'properties'    =>  json_encode($properties),
																		'fees'      	=>  json_encode($fee), 
																	]
																);
			
			//dd($customer);
			
			if ($customerAffectedRows==1){
				$array['status'] = true;
				$array['message'] = "Customer " .$customer->name . $defaultText ." updated succesfully.";
				return response()->json($array);
			}else{
				$array['status'] = false;
				$array['message'] = "Error while updating customer, try again.";
				return response()->json($array);
			}

		}else{

			$array['status'] = false;
			$array['message'] = "Error.";
			return response()->json($array);

		}

    }

    public function editName(Request $request)
    {
        $id_user = Auth::id();
		$customerUpdate = Customer::where('public_id', $request->get('pk'))->update( ['name' => $request->get('value')] );
		
		if ($customerUpdate==1){
			//$array['message'] = "Customer " .$customer->name . $defaultText ." created succesfully.";
			return 'Succesfully.';
		}else{
			return 'Error.';
		}

    }

	public function defaultCustomer(Request $request)
    {
        
		$id_user = Auth::id();

		// marcamos a todos como N default de		
		Customer::where('user_id', $id_user)->update( ['isdefault' => 'N'] );

		$customerUpdate = Customer::where('public_id', $request->get('pk'))->update( ['isdefault' => 'Y' ] );
		
		if ($customerUpdate==1){
			//$array['message'] = "Customer " .$customer->name . $defaultText ." created succesfully.";
			return 'Succesfully set as default.';
		}else{
			return 'Error.';
		}

    }

	
}