<?php

namespace App\Http\Controllers;

use Auth;
use App\Dashboard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function reporting3(Request $request)
    {
        $public_id = $request->get('id');

        $id_user = Auth::id();
        $platforms = DB::table('platforms')
            ->selectRaw('UPPER(name) as id, name')
            ->get();

        $campaigns = DB::table('campaigns')
            ->where('user_id', $id_user)
            ->select('public_id as id', 'name', 'customer_id', 'status', 'plataformas as platforms')
            ->get();

        $customers = DB::table('customers')
            ->where('user_id', $id_user)
            ->select('public_id as id', 'name')
            ->get();

        $fields = (object)[
            (object)[
                "id"      => "impressions",
                "name"    => "impressions",
                "default" => true
            ],
            (object)[
                "id"   => "cost",
                "name" => "cost"
            ],
            (object)[
                "id"   => "cpm",
                "name" => "cpm"
            ],
            (object)[
                "id"   => "clicks",
                "name" => "clicks"
            ],
            (object)[
                "id"   => "ctr",
                "name" => "ctr"
            ]
        ];


        $reports = Dashboard::select('public_id', 'name','configuration')
            ->where('user_id', $id_user)
            ->where('type', 'reporting3')
            ->get();

        $actual_report = DB::table('dashboards')->where('public_id','=',$public_id)->first();

        // si no tenemos seleccionado un report, cargamos el default
        if ($actual_report == null){
            $actual_report = DB::table('dashboards')->where('type','=','reporting3')->where('configuration','like', '%"default":true%')->first();
        }
        
        $data = [
            'title'         => 'reporting 3',
            'fields'        => $fields,
            'platforms'     => $platforms,
            'campaigns'     => $campaigns,
            'customers'     => $customers,
            'reports'       => $reports,
            'edit'          => true,
            'actual_report' => $actual_report,
            'actual_id'     => ((isset($actual_report->public_id)) ? $actual_report->public_id : ""),
            'configuration' => isset($actual_report->configuration) ? json_decode($actual_report->configuration, true) : "",
        ];

        return view('template.reporting_3', $data);
    }


    public function show($public_id = "")
    {
        $id_user = Auth::id();
        $platforms = DB::table('platforms')
            ->selectRaw('UPPER(name) as id, name')
            ->get();

        $campaigns = DB::table('campaigns')
            ->where('user_id', $id_user)
            ->select('public_id as id', 'name', 'customer_id', 'status', 'plataformas as platforms')
            ->get();

        $customers = DB::table('customers')
            ->where('user_id', $id_user)
            ->select('public_id as id', 'name')
            ->get();

        $fields = (object)[
            (object)[
                "id"      => "impressions",
                "name"    => "impressions",
                "default" => true
            ],
            (object)[
                "id"   => "cost",
                "name" => "cost"
            ],
            (object)[
                "id"   => "cpm",
                "name" => "cpm"
            ],
            (object)[
                "id"   => "clicks",
                "name" => "clicks"
            ],
            (object)[
                "id"   => "ctr",
                "name" => "ctr"
            ]
        ];


        $reports = Dashboard::select('public_id', 'name')
            ->where('type', '=', 'reporting2')
            ->get();


        $currentReport = DB::table('dashboards')->where('public_id', '=', $public_id)->first();

        $data = [
            'title'         => 'reporting 3',
            'fields'        => $fields,
            'platforms'     => $platforms,
            'campaigns'     => $campaigns,
            'customers'     => $customers,
            'reports'       => $reports,
            'edit'          => false,
            'actual_report' => $currentReport,
            'actual_id'     => ((isset($currentReport->public_id)) ? $currentReport->public_id : ""),
            'configuration' => ((isset($currentReport->configuration)) ? json_decode($currentReport->configuration) : "")
        ];

        return view('template.reporting_3', $data);
    }
}
