<?php

namespace App\Http\Controllers;

use App\Post;
use App\PostSetting;
use App\PostSource;
use App\Tag;
use App\TagDictionary;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Events\SettingsRegistered;
use App\Events\newCronEvent;
use App\Events\newCronJobEvent;

class OrganicController extends Controller
{
    private $user;

    public $platforms = [
        'FACEBOOK' => 'Facebook',
        'LINKEDIN' => 'Linkdedin',
        'TWITTER' => 'Twitter',
        'INSTAGRAM' => 'Instagram',
        'SNAPCHAT' => 'Snapchat',
        'GANALYTICS' => 'Google Analytics',
       /* 'SUNNATIVO' => 'Sunnativo',
        'FBINSIGHTS' => 'Facebook Insights',
        'PINTEREST' => 'Pinterest',
        'ADWORDS' => 'AdWords'*/
    ];

    public $sourceTypes = [
        'RSS' => 'RSS',
        'Analytics' => 'Analytics'
    ];

    public $itemChooseWays = [
        'Random' => 'Random',
        'Top to bottom' => 'Top to bottom',
        'Recent First' => 'Recent First',
        'Old First' => 'Old First'
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->user = Auth::user();

        if(isset($this->user->hashuser))
        {
            setcookie("id_user", $this->user->hashuser, time()+3600);
        }
        else
        {
            // TODO: handle not auth users.
        }
    }

    public function index(Request $request)
    {
        // object for period and days of the week
        $weekData = $this->getWeekData();

        $posts = $this->getPostsByPeriod($weekData['start_date'], $weekData['end_date']);

        $campaignsWithAtoms = DB::table('campaigns_platform_atomo')
                            ->join('campaigns', 'campaigns.id', 'campaigns_platform_atomo.campana_root_id')
                            ->select(
                                'campaigns.public_id as campaign_id',
                                'campaigns.name',
                                'campaigns_platform_atomo.name as atom_name',
                                'campaigns_platform_atomo.platform',
                                'campaigns_platform_atomo.public_id as atom_id' 
                                )
                            ->where('campaigns.user_id', Auth::user()->id)->get();

        $campaigns = DB::table('campaigns')->select('public_id', 'name')->where('user_id', Auth::user()->id)->get();

        $properties_accounts = DB::table('properties_adsaccount_relations')
        ->select('property_publicid as public_id', 'property_name as name', 'platform')
        ->whereIn('platform', ['FACEBOOK', 'TWITTER', 'LINKEDIN', 'SNAPCHAT', 'INSTAGRAM'])
        ->where('user_id', $this->user->id)
        ->whereIn('type', ['PAGE', 'PROFILE'])->get();


        $columnsTitle = [
            'ID',
            'Name',
        ];

        $columnsNames = [
            'public_id',
            'post_name',
        ];

        $data = [
            'title' => 'Organic Home',
            'isOrganic' => true,
            'weekData' => $weekData,
            'posts' => $posts,
            'campaignsWithAtoms' => $campaignsWithAtoms,
            'properties_accounts' => $properties_accounts,
            'campaigns' => $campaigns,
            'columnsTitles' => $columnsTitle,
            'columnsNames' => $columnsNames,
            'platforms' => $this->platforms,
            'tableDataUrl' => '/organic/getPosts'
        ];

        return view('template.organic.home', $data);
    }

    public function postsByPeriod(Request $request)
    {
        $postData = $request->all();

        // object for period and days of the week
        $weekData = $this->getWeekData($postData);

        $posts = $this->getPostsByPeriod($weekData['start_date'], $weekData['end_date']);

        $data = [
            'weekData' => $weekData,
            'posts' => $posts
        ];

        return json_encode($data);
    }

    public function getPosts(Request $request)
    {
        $limit = is_null($request->get('length') ) ? 100 :$request->get('length');
        $offset = $request->input('start') ? : 1;
        $page = intval( $offset / $limit) + 1;
        $posts = Post::where('user_id',  $this->user->id)->paginate( $limit , ['*'] , 'page', $page );

        $keyword = '';

        if(!is_null($request->get('search')))
        {
            $keyword = $request->get('search')['value'];

            $posts->where("post_name" , "like", '%' . $keyword . '%');
        }

        $posts->getCollection();

        $result = [
            "draw"              => $request->input('draw') ? : 1,
            "recordsTotal"      => $posts->total(),
            "recordsFiltered"   => $posts->total(),
            "data"              => $posts->items()
        ];

        return json_encode($result);
    }

    public function savePost(Request $request, $id = '')
    {
        $url = '/organic/post';

        $data = $request->all();

        $rules = [
            "pdetails_post_title" => "required",
            "pdetails_post_image" => "required|image",
            "pdetails_post_content" => "required",
            "pdetails_post_promoted" => "required",
            "pdetails_post_date" => "required",
            "pdetails_post_platforms" => "required|array|min:1"
        ];

        if($data["pdetails_post_date"] == "SCHEDULED"){
            $rules["pdetails_post_schedule"] = "required|date|after_or_equal:2020-10-04 15:20";
        }

        $messages = [
            "pdetails_post_title.required" => "Post title field is required.",
            "pdetails_post_image.required" => "Post image field is required.",
            "pdetails_post_content.required" => "Post content field is required.",
            "pdetails_post_promoted.required" => "Post promoted field is required.",
            "pdetails_post_date.required" => "Post publish time field is required.",
            "pdetails_post_platforms.required" => "At least one post platform is required.",
            "pdetails_post_platforms.min" => "At least one post platform is required.",
            "pdetails_post_schedule.required" => "Post schedule time field is required.",
        ];

        if(isset($data['pdetails_post_image_uploaded']))
        {
            unset($rules['pdetails_post_image']);
            unset($messages['pdetails_post_image']);
        }

        $validator = Validator::make(
            $data,
            $rules,
            $messages
        );

        if ($validator->fails())
        {
            return json_encode([
                'errors' => $validator->getMessageBag()
            ]);
        }

        if(!empty($id))
        {
            $post = Post::where('public_id', $id)->first();
            $post = $this->savePostModels($post, $data, $request);
        }
        else
        {
            $post = new Post();
            $post = $this->savePostModels($post, $data, $request);
        }

        $url .= '/' . $post->id;

        return json_encode([
            'url' => $url
        ]);
    }

    public function postForm(Request $request, $id = '')
    {
        $post = $this->getPostElement($id);

        $data = [
            'title' => 'Organic Create Post',
            'isOrganic' => true,
            'platforms' => $this->platforms,
            'post' => $post,
            'id' => $id
        ];

        return view('template.organic.create_post_form', $data);
    }

    public function tags(Request $request)
    {
        $columnsTitle = [
            'Name',
            'Profile Source',
            'Tags Number',
            'Last Time Update',
            'Actions',
        ];

        $columnsNames = [
            'name',
            'profilesource',
            'numberofitems',
            'lasttimeupdated',
            'actions'
        ];

        $data = [
            'title' => 'Organic Tags',
            'isOrganic' => true,
            'columnsTitles' => $columnsTitle,
            'columnsNames' => $columnsNames,
            'tableDataUrl' => '/organic/getTags',
            'platforms' => $this->platforms
        ];

        return view('template.organic.tags', $data);
    }

    public function getTags(Request $request)
    {
        $limit = is_null($request->get('length') ) ? 100 :$request->get('length');
        $offset = $request->input('start') ? : 1;
        $page = intval( $offset / $limit) + 1;
        $tagDics= TagDictionary::where('user_id',  $this->user->id)
                    ->select("public_id", 'name', "numberofitems", "platform", "profilesource", "creationtime", "lasttimeupdated", "user_id")
                    ->paginate( $limit , ['*'] , 'page', $page );

        $keyword = '';

        if(!is_null($request->get('search')))
        {
            $keyword = $request->get('search')['value'];

            $tagDics->where("name" , "like", '%' . $keyword . '%');
        }

        $tagDics->getCollection();

        $result = [
            "draw"              => $request->input('draw') ? : 1,
            "recordsTotal"      => $tagDics->total(),
            "recordsFiltered"   => $tagDics->total(),
            "data"              => $tagDics->items()
        ];

        return json_encode($result);
    }

    public function tagForm(Request $request, $id = '')
    {
        $tag = $this->getTagElement($id);


        $data = [
            'title' => 'Organic Create Tag',
            'isOrganic' => true,
            'platforms' => $this->platforms,
            'tag' => $tag,
            'id' => $id
        ];

        return view('template.organic.create_tag_form', $data);
    }

    public function saveTag(Request $request, $id = '')
    {

        $url = '/organic/tag';

        $data = $request->all();

        $rules = [
            "tdetails_tag_name" => "required",
            "tdetails_tag_platform" => "required",
            "tdetails_tag_url" => "required|url",
            //"tdetails_tag_auto_update" => "required",
        ];

        $messages = [
            "tdetails_tag_name.required" => "Tag name field is required.",
            "tdetails_tag_platform.required" => "Tag platform field is required.",
            "tdetails_tag_url.required" => "Tag url field is required.",
            "tdetails_tag_url.url" => "Tag url field must be a full url (http:// or https://)",
            //"tdetails_tag_auto_update.required" => "Tag auto field is required.",
        ];

        $validator = Validator::make(
            $data,
            $rules,
            $messages
        );

        if ($validator->fails()){
            return json_encode([ 'errors' => $validator->getMessageBag()]);
        }

        if(!empty($id)) {
            $tagDic = TagDictionary::where('public_id', $id)->first();
            $tagDic = $this->saveTagModels($tagDic, $data);
        } else{
            $tagDic = new TagDictionary();
            $tagDic = $this->saveTagModels($tagDic, $data);
        }

        $url .= '/' . $tagDic->public_id;

        return json_encode([
            'url' => $url
        ]);
    }

    public function settings(Request $request)
    {
        $columnsTitle = [
            'Name',
            'Source Type',
        ];

        $columnsNames = [
            'name',
            'sourcetype',
        ];

        $tags = TagDictionary::where('user_id',  $this->user->id)->get();

        $ads_accounts = DB::table('ads_accounts')->select('name', 'platform', 'public_id')->where('platform', 'GANALYTICS')->where('user_id', Auth::user()->id)->get();
        //$app_id = is_object($ads_accounts->get(0)) ? $ads_accounts->get(0)->app_id : 0;
        $listAds=[];
        
        foreach ($ads_accounts as $item) {
            array_push($listAds , $item['public_id']);
        }
        
        $properties_accounts_ganalytics = DB::table('properties_adsaccount_relations')
                                                ->select('property_publicid as public_id', 'property_name as name', 'platform')
                                                ->where('platform', 'GANALYTICS')
                                                ->whereIn('ad_account_publicid', $listAds)
                                                ->get();

        $properties_accounts = DB::table('properties_adsaccount_relations')
                                    ->select('property_publicid as public_id', 'property_name as name', 'platform')
                                    ->whereIn('platform', ['FACEBOOK', 'TWITTER', 'LINKEDIN', 'SNAPCHAT', 'INSTAGRAM'])
                                    ->where('user_id', $this->user->id)
                                    ->whereIn('type', ['PAGE', 'PROFILE'])->get();

        $data = [
            'title' => 'Organic Settings',
            'isOrganic' => true,
            'columnsTitles' => $columnsTitle,
            'columnsNames' => $columnsNames,
            'tableDataUrl' => '/organic/getSettings',
            'sourceTypes' => $this->sourceTypes,
            'itemChooseWays' => $this->itemChooseWays,
            'ads_accounts' => $ads_accounts,
            'properties_accounts_ganalytics' => $properties_accounts_ganalytics,
            'properties_accounts' => $properties_accounts,
            'tags' => $tags
        ];

        return view('template.organic.settings', $data);
    }

    public function getSettings(Request $request)
    {
        $limit = is_null($request->get('length') ) ? 100 :$request->get('length');
        $offset = $request->input('start') ? : 1;
        $page = intval( $offset / $limit) + 1;
        $postSettings = PostSetting::where('user_id',  $this->user->id)
                        ->select("public_id", 'name', "sourcetype", "Source_url", "readtypetime", "readintervalminutes", "itemstoread", "itemschoose", "postingtypetime", "postingintervalminutes", "tagsdictionary", "user_id", "filter")
                        ->paginate( $limit , ['*'] , 'page', $page );
        $keyword = '';




        if(!is_null($request->get('search')))
        {
            $keyword = $request->get('search')['value'];

            $postSettings->where("name" , "like", '%' . $keyword . '%');
        }

        $postSettings->getCollection();

        $result = [
            "draw"              => $request->input('draw') ? : 1,
            "recordsTotal"      => $postSettings->total(),
            "recordsFiltered"   => $postSettings->total(),
            "data"              => $postSettings->items()
        ];

        return json_encode($result);
    }

    public function settingForm(Request $request, $id = '')
    {
        $setting = $this->getSettingElement($id);

        $tags = Tag::all();

        $data = [
            'title' => 'Organic Create Setting',
            'isOrganic' => true,
            'sourceTypes' => $this->sourceTypes,
            'itemChooseWays' => $this->itemChooseWays,
            'tags' => $tags,
            'setting' => $setting,
            'id' => $id
        ];

        return view('template.organic.create_setting_form', $data);
    }

    public function saveSetting(Request $request, $id = '')
    {

        $url = '/organic/settings';

        $data = $request->all();

        $rules = [
            "sdetails_setting_name" => "required",
            "sdetails_setting_source_type" => "required",
            //"sdetails_setting_url" => "required|url",
            "sdetails_setting_read_interval" => "required",
            "sdetails_setting_items_to_read" => "required",
            "sdetails_setting_item_choose_way" => "required",
            "sdetails_setting_post_interval" => "required",
            "sdetails_setting_destination_profiles" => "required",
            //"sdetails_setting_tags" => "required|array|min:1"
        ];

        $messages = [
            "sdetails_setting_name.required" => "Setting name field is required.",
            "sdetails_setting_source_type.required" => "Setting source type field is required.",
            //"sdetails_setting_url.required" => "Setting url field is required.",
            "sdetails_setting_read_interval.required" => "Setting read interval minutes field is required.",
            "sdetails_setting_items_to_read.required" => "Setting items to read field is required.",
            "sdetails_setting_item_choose_way.required" => "Setting item choose way field is required.",
            "sdetails_setting_post_interval.required" => "Setting post interval field is required.",
            "sdetails_setting_destination_profiles.required" => "Setting destination profiles field is required.",
            //"sdetails_setting_tags.required" => "At least one setting tag is required.",
            //"sdetails_setting_tags.min" => "At least one setting tag is required.",
        ];

        if($request->input('sdetails_setting_source_type') == 'RSS'){
          $rules['sdetails_setting_url'] = "required|url";
        }else{
          $rules['sdetails_setting_property'] = "required";
        }

        $validator = Validator::make(
            $data,
            $rules,
            $messages
        );

        if ($validator->fails())
        {
            return json_encode([
                'errors' => $validator->getMessageBag()
            ]);
        }

        if(!empty($id))
        {
            $postSetting = PostSetting::where('public_id', $id)->first();

            $postSetting = $this->saveSettingModels($postSetting, $data);


            $job =  [
                'action_type' => 'settings_update',
                'Execution_interval' => $data['sdetails_setting_read_interval'].'M',
                'next_execution' => now()
            ];
            
            DB::connection('stats')->table('background_job')
              ->where('type', '=', 'organic')
              ->where('subject_id', '=', $postSetting->id)
              ->update( $job );

        }
        else
        {
            $postSetting = new PostSetting();

            $postSetting = $this->saveSettingModels($postSetting, $data);

            $job =  [
                'type' => 'organic',
                'subject_id' => $postSetting->id,
                'action_type' => 'settings_update',
                'Execution_interval' => $data['sdetails_setting_read_interval'].'M',
                'next_execution' => now()
            ];

            //DB::connection('stats')->table('background_job')->insert($job);
            event(new newCronJobEvent($job));

        }




        
        //event(new SettingsRegistered($postSetting));
        //$url .= '/' . $postSetting->id;

        return json_encode([
            'url' => $url
        ]);
    }

    // HELPERS

    public function getPostElement($id)
    {
        $post = [
            "pdetails_post_id" => "",
            "pdetails_post_title" => "",
            "pdetails_post_image" => "",
            "pdetails_post_content" => "",
            "pdetails_post_promoted" => "",
            "pdetails_post_date" => "",
            "pdetails_post_platforms" => [],
            "pdetails_post_disabled" => false
        ];

        if(!empty($id))
        {
            $post = Post::where('public_id', $id)->first();
            $postSource = PostSource::where('id', $post->source_content_id)->first();
            $platforms = [];

            /*foreach ( $postSources as $key => $item)
            {
                $platforms[$item->platform] = $item->platform;
            }*/

            $post = [
                "pdetails_post_id" => $id,
                "pdetails_post_title" => $post->post_name,
                "pdetails_post_image" => url("/storage/posts-images/".$post->destination_url),
                "pdetails_post_content" => $post->post_configuration,
                "pdetails_post_promoted" => $post->promoted,
                "pdetails_post_date" => $post->posted_time,
                "pdetails_post_platforms" => $platforms,
                "pdetails_post_disabled" => ((strtotime($post->posted_time) > time())? false : true)
            ];
        }

        return $post;
    }

    public function getTagElement($id)
    {
        $tag = [
            "tdetails_tag_public_id" => "",
            "tdetails_tag_name" => "",
            "tdetails_tag_platform" => "",
            "tdetails_tag_url" => "",
            "tdetails_tag_auto_update" => ""
        ];

        if(!empty($id))
        {
            $tagDic = TagDictionary::where('public_id', $id)->first();

            $tag['tdetails_tag_public_id'] = $tagDic->public_id;
            $tag['tdetails_tag_name'] = $tagDic->name;
            $tag['tdetails_tag_platform'] = $tagDic->platform;
            $tag['tdetails_tag_url'] = $tagDic->profilesource;
            $tag['tdetails_tag_auto_update'] = $tagDic->auto_update;
        }

        return $tag;
    }

    private function saveTagModels($tagDic, $data)
    {
        $tagDic->name = $data['tdetails_tag_name'];
        $tagDic->platform = $data['tdetails_tag_platform'];
        $tagDic->profilesource = $data['tdetails_tag_url'];
        $tagDic->auto_update = $data['tdetails_tag_auto_update'];
        $tagDic->user_id = $this->user->id;

        $tagDic->save();

        return $tagDic;
    }

    public function getSettingElement($id)
    {
        $setting = [
            "sdetails_post_id" => "",
            "sdetails_setting_name" => "",
            "sdetails_setting_source_type" => "",
            "sdetails_setting_url" => "",
            "sdetails_setting_user_from" => "",
            "sdetails_setting_read_interval" => "",
            "sdetails_setting_items_to_read" => "",
            "sdetails_setting_item_choose_way" => "",
            "sdetails_setting_post_interval" => "",
            "sdetails_setting_tags" => [],
            "sdetails_setting_destination_profiles" => []
        ];

        if(!empty($id))
        {
            $postSetting = PostSetting::where('public_id', $id)->first();

            $setting['sdetails_post_id'] = $postSetting->public_id;
            $setting['sdetails_setting_name'] = $postSetting->name;
            $setting['sdetails_setting_source_type'] = $postSetting->sourcetype;
            $setting['sdetails_setting_url'] = $postSetting->Source_url;
            $setting['sdetails_setting_user_from'] = $postSetting->user_from;
            $setting['sdetails_setting_read_interval'] = $postSetting->readintervalminutes;
            $setting['sdetails_setting_items_to_read'] = $postSetting->itemstoread;
            $setting['sdetails_setting_item_choose_way'] = $postSetting->itemschoose;
            $setting['sdetails_setting_post_interval'] = $postSetting->postingintervalminutes;
            $setting['sdetails_setting_tags'] = ($postSetting->tagsdictionary == null)? []: $postSetting->tagsdictionary;
            $setting['sdetails_setting_destination_profiles'] = ($postSetting->destination_profiles == null)? []: json_decode($postSetting->destination_profiles);

        }

        return $setting;
    }

    private function saveSettingModels($postSetting, $data)
    {
        $user = Auth::user();

        $postSetting->name = $data['sdetails_setting_name'];
        $postSetting->sourcetype = $data['sdetails_setting_source_type'];
        $postSetting->Source_url = $data['sdetails_setting_url'];
        $postSetting->readintervalminutes = $data['sdetails_setting_read_interval'];
        $postSetting->itemstoread = $data['sdetails_setting_items_to_read'];
        $postSetting->itemschoose = $data['sdetails_setting_item_choose_way'];
        $postSetting->postingintervalminutes = $data['sdetails_setting_post_interval'];
        $postSetting->user_id = $user->id;

        if(isset($data['sdetails_setting_property']) && ($data['sdetails_setting_source_type'] == "Analytics")){
          $properties_accounts_ganalytics = DB::table('properties_accounts')
          ->select('id')
          ->where('platform', 'GANALYTICS')
          ->where('public_id', $data['sdetails_setting_property'])
          ->first();

          if($properties_accounts_ganalytics != null){
            $postSetting->property = $properties_accounts_ganalytics->id;
          }

          $postSetting->user_from = $data['sdetails_setting_user_from'];
        }

        if(isset($data['sdetails_setting_tags'])){
            $postSetting->tagsdictionary = json_encode($data['sdetails_setting_tags']);
        }else{
            $postSetting->tagsdictionary = json_encode([]);
        }
        if(isset($data['sdetails_setting_destination_profiles'])){
            $postSetting->destination_profiles = json_encode(explode(',', $data['sdetails_setting_destination_profiles']));
        }else{
            $postSetting->destination_profiles = json_encode([]);
        }

        $postSetting->save();


        return $postSetting;
    }

    private function savePostModels($post, $data, $request)
    {
        $isNew = false;
        $path = '';
        $user = Auth::user();

        if(!isset($post->id))
        {
            $isNew = true;
        }

        if(!isset($data['pdetails_post_image_uploaded']))
        {        
            $path = Storage::putFile(env("MEDIAFOLDER", "/mediafolder"). '/'. $user->public_id.'/organic', $request->file('pdetails_post_image'), 'public');
            $post->destination_url = $path;
        }
        else
        {
            if($request->file('pdetails_post_image'))
            {
                $path = Storage::putFile(env("MEDIAFOLDER", "/mediafolder"). '/'. $user->public_id.'/organic', $request->file('pdetails_post_image'), 'public');
                $post->destination_url = $path;
            }
        }

        $post->post_name = $data['pdetails_post_title'];
        $post->post_description = $data['pdetails_post_content'];
        $post->user_id = $user->id;
        $post->organic_souce_post_md5 = md5($post->destination_url);

        if ($data['pdetails_post_promoted']=='Y'){
            $post->campana_root = $data['sdetails_setting_destination_campaign'];
            $post->atom = $data['sdetails_setting_destination_campaign_atom'];
        }else{
            $post->campana_root = '';
            $post->atom = '';
        }

        $listProps = [];
        foreach ($data['sdetails_setting_destination_property'] as $props){
            
            array_push($listProps,$props);
        }

        $post->properties = json_encode($listProps);


        $post->save();

        $date = Carbon::now();

        if($data["pdetails_post_date"] == "SCHEDULED"){
            $date = Carbon::parse($data['pdetails_post_schedule']);
        }

        if($isNew)
        {
            foreach ($data['pdetails_post_platforms'] as $key => $platform)
            {
                $postSource = new PostSource();
                $this->savePostSourceModel($postSource, $data, $date, $post, $platform);
            }
        }
        else
        {
            PostSource::where('content', $post->id)->delete();
            foreach ($data['pdetails_post_platforms'] as $key => $platform)
            {
                $postSource = new PostSource();
                $this->savePostSourceModel($postSource, $data, $date, $post, $platform);
            }
        }

        return $post;
    }

    private function savePostSourceModel($postSource, $data, $date, $post, $platform = '')
    {
        $postSource->posted_time = $date;
        $postSource->status = $data['pdetails_post_date'];
        $postSource->promoted = $data['pdetails_post_promoted'];
        
        if(!empty($platform))
        {
            $postSource->platform = $platform;
        }

        // Relations Field
        $postSource->user_id = $this->user->id;
        $postSource->content = $post->id;


        $postSource->save();

        return $postSource;
    }

    private function getWeekData($postData = [])
    {
        $now = '';

        if(isset($postData['previous_end_day']))
        {
            $now = Carbon::parse($postData['previous_end_day'])->addDay();
        }

        if(isset($postData['next_start_day']))
        {
            $now = Carbon::parse($postData['next_start_day'])->subDay();
        }

        if(empty($now))
        {
            $now = Carbon::now();
        }

        $today = Carbon::now()->format('Y-m-d');
        $period = [];
        $year = $now->year;
        $month = $now->shortEnglishMonth;
        $startOfWeek = $now->startOfWeek(Carbon::MONDAY)->format('Y-m-d');
        $endOfWeek = $now->endOfWeek(Carbon::SUNDAY)->format('Y-m-d');
        $startDayOfWeek = Carbon::parse($startOfWeek)->day;
        $endDayOfWeek = Carbon::parse($endOfWeek)->day;
        $periodObjects = CarbonPeriod::create($startOfWeek, $endOfWeek);

        foreach ($periodObjects as $key => $date)
        {
            $class = 'day-of-week';

            if ($key === array_key_first($periodObjects->toArray()))
            {
                $class = 'first-day-of-week';
            }

            if ($key === array_key_last($periodObjects->toArray()))
            {
                $class = 'last-day-of-week';
            }

            if($date->toDateString() == $today)
            {
                $class .= ' fc-today';
            }
            else
            {
                if($date->lt($today))
                {
                    $class .= ' fc-past';
                }

                if($date->gt($today))
                {
                    $class .= ' fc-future';
                }
            }


            $period[] = [
                'show' => $date->shortEnglishDayOfWeek . ' ' . $date->format('m/d'),
                'raw' =>  $date->format('Y-m-d'),
                'class' => $class
            ];
        }

        return [
            'today' => $today,
            'year' => $year,
            'month' => $month,
            'start_day' => $startDayOfWeek,
            'end_day' => $endDayOfWeek,
            'start_date' => $startOfWeek,
            'end_date' => $endOfWeek,
            'period' => $period
        ];
    }

    protected function getTimePosition($date)
    {
        for($i=0; $i<24; $i++)
        {
            $frame = Carbon::create($date->year, $date->month, $date->day, $i, 0, 0);

            /*if($frame->diffInHours($date) < 1 && $frame->diffInHours($date) >= 0)
            {
                return $i;
            }*/
            return ($frame->diffInMinutes($date) / 60);
        }
    }

    protected function getDatePosition($date, $startDate, $endDate)
    {
        $startOfWeek = Carbon::parse($startDate);
        $endOfWeek = Carbon::parse($endDate);
        $periodObjects = CarbonPeriod::create($startOfWeek, $endOfWeek);
        $i = 0;

        foreach ($periodObjects as $key => $loopDate)
        {
            if($loopDate->format('Y-m-d') == $date->format('Y-m-d'))
            {
                return $i;
            }

            $i++;
        }

        return -1;
    }

    protected function getPostsByPeriod($startDate, $endDate)
    {
        // get posts with related post source by time period

        $posts = Post::where('user_id', $this->user->id)->get();

        // prepare data response of posts

        $items = [];
        /*echo $this->user->id."<br>";
        echo count($posts);
        echo "<br>";
        echo $startDate." - ".$endDate;
        echo "<br>";*/
        foreach ($posts as $post)
        {
            $platforms = PostSource::where('source_content_id', $post->id)
                                ->whereBetween('posted_time', [$startDate." 00:00:00", $endDate." 23:59:59"])
                                ->where('user_id', $this->user->id)
                                ->orderBy('posted_time')
                                ->get();
            /*print_r($post);
            echo count($platforms);
            echo "<br>"."<br>";*/



            foreach ($platforms as $platform)
            {
                if(!isset($items[$post->id]['public_id']))
                {
                    $items[$post->id]['public_id'] = 0;
                }

                if(!isset($items[$post->id]['reach']))
                {
                    $items[$post->id]['reach'] = 0;
                }

                if(!isset($items[$post->id]['impressions']))
                {
                    $items[$post->id]['impressions'] = 0;
                }

                if(!isset($items[$post->id]['clicks']))
                {
                    $items[$post->id]['clicks'] = 0;
                }

                if(!isset($items[$post->id]['reactions']))
                {
                    $items[$post->id]['reactions'] = 0;
                }

                if(!isset($items[$post->id]['engagements']))
                {
                    $items[$post->id]['engagements'] = 0;
                }

                if(!isset($items[$post->id]['platforms']))
                {
                    $items[$post->id]['platforms'] = [];
                }

                if(!isset($items[$post->id]['promoted']))
                {
                    $items[$post->id]['promoted'] = [];
                }

                if(!isset($items[$post->id]['posted_time']))
                {
                    $items[$post->id]['posted_time'] = '';
                }

                if(!isset($items[$post->id]['time_position']))
                {
                    $items[$post->id]['time_position'] = '';
                }

                if(!isset($items[$post->id]['date_position']))
                {
                    $items[$post->id]['date_position'] = '';
                }

                $items[$post->id]['public_id'] = $post->public_id;
                $items[$post->id]['name'] = $post->post_name;

                array_push($items[$post->id]['platforms'], $platform->platform);

                $items[$post->id]['posted_time'] = $platform->posted_time;
                $items[$post->id]['time_position'] = $this->getTimePosition($platform->posted_time);
                $items[$post->id]['date_position'] = $this->getDatePosition($platform->posted_time, $startDate, $endDate);
                $items[$post->id]['reach'] += $platform->reach;
                $items[$post->id]['impressions'] += $platform->impresions;
                $items[$post->id]['clicks'] += $platform->clicks;
                $items[$post->id]['reactions'] += $platform->reactions;
                $items[$post->id]['engagements'] += $platform->engagements;
                $items[$post->id]['promoted'] = $platform->promoted;
            }
        }
        return $items;
    }
}