<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class settings_matrix extends Model
{

    protected $table = 'settings_matrix';
    
    protected $primaryKey = 'id';
    public $incrementing = true;
    
    protected $fillable = ['uuid','type','name','nombre','valor_fb','valor_ig','valor_tw','valor_lkd','valor_adw','valor_amz','metadata'];
    
    public $timestamps = false;
}
