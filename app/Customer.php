<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Campaign;

class Customer extends Model
{
    protected $table = 'customers';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'spend', 'impressions', 'office', 'clicks', 'spend', 'impressions', 'cpm', 'ctr', 'conversions', 'ads_accounts', 'properties', 'fees', 'user_id'
    ];

    public function users()
    {
        return $this->hasMany('App\User', 'id', 'user_id');
    }

    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }
}
