<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, $id)
 */
class PostSource extends Model
{
    protected $table = 'organic_post';

    public $timestamps = false;

    protected $dates = [
        'created_time',
        'posted_time'
    ];

    /**
     * Get the record associated to posts
     */
    public function post()
    {
        return $this->belongsTo('App\Post', 'id', 'content');
    }
}
