<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostSetting extends Model
{
    protected $table = 'organic_settings';

    public $timestamps = false;

    /**
     * Get the record associated to posts
     */
    public function posts()
    {
        return $this->hasMany('App\Post', 'setting_id', 'id');
    }
}
