<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;

class Campaign_platform extends Model
{
    protected $table = 'campaigns_platform';

    protected $fillable = [
        'id',
        'user_id',
        'platform',
        'id_en_platform',
        'app_id',
        'ad_account',
        'auth_id',
        'activa',
        'property_id',
        'campana_root',
        'name',
        'stats_ultimoupdate',
        'created_at',
        'updated_at',
        'start',
        'end',
        'budget',
        'budget_total',
        'ctr',
        'cpc',
        'clicks',
        'impression',
        'spent',
        'conversion',
        'cpm',
        'cpa',
        'customer_id',
        'geo_include',
        'geo_exclude',
        'platform_placements',
        'genders',
        'ages',
        'languages',
        'platform_properties_ids',
        'devices',
        'conectivity',
        'metadata',
        'targeting_include',
        'targeting_exclude',
        'platforma_budget',
        'creativities_ids',
        'objective',
        'bid_strategy',
        'currency',
        'status',
        'process',
        'public_id'
    ];
    public $timestamps = false;

}