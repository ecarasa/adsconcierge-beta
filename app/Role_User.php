<?php

namespace App;

use App\User;
use App\Role;
use Illuminate\Database\Eloquent\Model;

class Role_User extends Model
{
    protected $table = 'role_user';
    protected $fillable = [
        'id', 'role_id', 'user_id', 'created_at', 'updated_at'
    ];


    public function users()
    {
        return $this->hasOne('App/User','user_id','id');
    }
      public function roles()
    {
        return $this->hasOne('App/Role','role_id','id');
    }
}
