<?php 


namespace App\EventsPipeline;

use App\Customer;
use Closure;
use CronJobber;
use Auth;
use DB;

class CampaignBudgetUpdate {

    public function handle($dataEvent, Closure $next){
        
        //$cron = new \App\EventsPipeline\CronJobber();
        $dataEvent['split'] = array();
        foreach ($dataEvent['budgetdistribution'] as $budgetPlat){

            if ($budgetPlat->budgetTypeStr == 'by_percentage'){
                $split = (float) $dataEvent['importeNuevo'] * ( (float)  str_replace('%', '', $budgetPlat->percentaje) / 100);
                //echo " -- SPLIT BUDGET ".$budgetPlat->percentaje. ' para : '. $budgetPlat->platform.' // $'. $split;
                $dataEvent['split'][$budgetPlat->platform] = $split;
            }

            $campaignsByPlatformActives = DB::table('campaigns_platform')
                                            ->where('campana_root', $dataEvent['campaign_id'])
                                            ->where('user_id', Auth::user()->id )
                                            ->where('status', 'ACTIVE')
                                            ->where('platform', $budgetPlat->platform)
                                            ->update(['budget' => $split]);

            $campaignsByPlatformActives = DB::table('campaigns_platform')
                                            ->where('campana_root', $dataEvent['campaign_id'])
                                            ->where('user_id', Auth::user()->id )
                                            ->where('status', 'ACTIVE')
                                            ->where('platform', $budgetPlat->platform)
                                            ->get();

            $bulkData = array();

            foreach($campaignsByPlatformActives as $rr){
                
                array_push($bulkData, array(
                    'type' => 'campaigns_platform', 
                    'action_type'=> $dataEvent['job'], 
                    'subject_id'=> $rr->id,
                    'next_execution'=>date('Y-m-d H:i:s')
                ));

            }
            
            //echo ' campaig_affected for campaign_platform ... '. count($bulkData); 
            DB::connection('stats')->table('adsconcierge_stats.background_job')->insert($bulkData);

        }

        return $next($dataEvent);
    }



}