<?php 


namespace App\EventsPipeline;

use DB;
use Closure;


class CronJobber  {



    public function create($entity, $actionType, $idExecute){

        /***** actual operationes in cronjob table *
         * 
         * daily_stats - update_status - update_budget - update_field
         * 
         * 
         * 
         * */




        return DB::connection('stats')
                ->table('adsconcierge_stats.background_job')
                ->insert([
                    'type' => $entity, 
                    'action_type'=> $actionType, 
                    'subject_id'=> $idExecute,
                    'next_execution'=>date('Y-m-d H:i:s')
                ]);

    }

}