<?php 


namespace App\EventsPipeline;

use App\Customer;
use Closure;
use CronJobber;
use Auth;

use DB;

class CampaignStatusUpdate {

    public function handle($dataEvent, Closure $next){
        
        //$cron = new CronJobber();
        //$data = array('campaign_id'=> $affected_to_cron->id, 'status'=> $entityFieldValue, 'job'=>$actionJobType);

        $campanas_afectadas = DB::table('campaigns_platform')
                                        ->where('campana_root', $dataEvent['campaign_id'])
                                        ->where('user_id',  Auth::user()->id )
                                        ->update(['status' => $dataEvent['status'] ]);

        $campanas_afectadas = DB::table('campaigns_platform')
                                        ->where('campana_root', $dataEvent['campaign_id'])
                                        ->where('user_id',  Auth::user()->id )
                                        ->get();
    
        $bulkData = array();

        foreach($campanas_afectadas as $rr){
            array_push($bulkData, array(
                'type' => 'campaigns_platform', 
                'action_type'=> $dataEvent['job'], 
                'subject_id'=> $rr->id,
                'next_execution'=>date('Y-m-d H:i:s')
            ));        
        }

        DB::connection('stats')->table('adsconcierge_stats.background_job')->insert($bulkData);
        $dataEvent['bulk_cron'] = count($bulkData);
        return $next($bulkData);
    }



}