<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;

class Campaign extends Model
{
    protected $table = 'campaigns';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'spend', 'impressions', 'office', 'clicks', 'spend', 'impressions', 'cpm', 'ctr', 'conversions'
    ];


    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

}
