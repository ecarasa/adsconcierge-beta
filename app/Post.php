<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'organic_sources_posts';

    public $timestamps = false;

    protected $dates = [
        'created_time'
    ];

    /**
     * Get the record associated to posts source
     */
    public function platforms()
    {
        return $this->hasMany('App\PostSource', 'content', 'id');
    }

    /**
     * Get the record associated to setting
     */
    public function setting()
    {
        return $this->belongsTo('App\PostSetting', 'id', 'setting_id');
    }
}
