<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $table = 'offices';

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}