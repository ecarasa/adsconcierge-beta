<?php


namespace App;


class ChartDataParser
{
    public static function convert(
        string $type,
        array $data,
        array $metrics,
        \DateTimeImmutable $startDate,
        \DateTimeImmutable $endDate,
        string $groupBy
    ): array
    {

        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($startDate, $interval ,$endDate);

        //dd($groupBy);

        $xAxis = [];
        $yAxis = [];

        foreach ($period as $day) {
            $xAxis[] = $day->format(DatePeriod::DATE_FORMAT);
        }

        $arrangedData = [];
        foreach ($data as $datum) {
            $arrangedData[$datum->dia] = $datum;
        }

        foreach ($metrics as $metric) {
            $series['name'] = $metric;
            $series['data'] = [];
            foreach ($period as $day) {
                $stringDay = $day->format(DatePeriod::DATE_FORMAT);
                if (true === isset($arrangedData[$stringDay])) {
                    $series['data'][] = $arrangedData[$stringDay]->$metric;
                } else {
                    $series['data'][] = 0;
                }

            }
            $yAxis[] = $series;
        }

        return [
            'series' => $yAxis,
            'labels' => $xAxis
        ];
    }
}
