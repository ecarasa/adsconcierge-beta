<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'organic_tags';

    public $timestamps = false;

    protected $appends = ['actions'];

    public function getActionsAttribute()
    {
        return '';
    }
}
