<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
     @include('layouts.admin.includes.meta')
 
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
      
        @include('layouts.admin.includes.header')
         
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        @include('layouts.admin.includes.content')
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        @include('layouts.admin.includes.footer')
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="../tf/assets/global/plugins/respond.min.js"></script>
<script src="../tf/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        



        <!--begin:: Global Mandatory Vendors -->
        <script src="../assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>

        <!--end:: Global Mandatory Vendors -->

        <!--begin:: Global Optional Vendors -->
        <script src="../assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/custom/components/vendors/bootstrap-datepicker/init.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/custom/components/vendors/bootstrap-timepicker/init.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
        <script src="../assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
        <script src="../assets/vendors/custom/components/vendors/bootstrap-switch/init.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/nouislider/distribute/nouislider.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/autosize/dist/autosize.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/summernote/dist/summernote.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/markdown/lib/markdown.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
        <script src="../assets/vendors/custom/components/vendors/bootstrap-markdown/init.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/custom/components/vendors/bootstrap-notify/init.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
        <script src="../assets/vendors/custom/components/vendors/jquery-validation/init.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/toastr/build/toastr.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/raphael/raphael.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/morris.js/morris.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
        <script src="../assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/counterup/jquery.counterup.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
        <script src="../assets/vendors/custom/components/vendors/sweetalert2/init.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>
        <script src="../assets/vendors/general/dompurify/dist/purify.js" type="text/javascript"></script>

        <!--end:: Global Optional Vendors -->

        <!--begin::Global Theme Bundle(used by all pages) -->
        <script src="../assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

        <!--end::Global Theme Bundle -->

        <!--begin::Page Vendors(used by this page) 
       <script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
        <script src="../assets/vendors/custom/gmaps/gmaps.js" type="text/javascript"></script>
-->
        <script src="../assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
 

        <!--end::Page Vendors -->

        <!--begin::Page Scripts(used by this page) -->
        <script src="../assets/app/custom/general/dashboard.js" type="text/javascript"></script>

        <!--end::Page Scripts -->

        <!--begin::Global App Bundle(used by all pages) -->
        <script src="../assets/app/bundle/app.bundle.js" type="text/javascript"></script>

        <!--end::Global App Bundle -->

        
    </body>
 
</html>