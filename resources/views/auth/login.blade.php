<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>Advertising Manager</title>

    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;700&family=Source+Serif+Pro:wght@600&display=swap" rel="stylesheet">
    <link href="https://cdn.iconmonstr.com/1.3.0/css/iconmonstr-iconic-font.min.css">
    <link href="{{ asset('/assets/app/custom/login/login-v4.default.css') }}" rel="stylesheet" type="text/css" />
    @include('template.metas')
    <link href="{{ asset('/css/reporting.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" type="text/css" />

</head>
<!-- end::Head -->
<!-- begin::Body -->
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                style="background-image: flex:auto;">
                <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__logo">
                            
                                <img src="assets/media/logos/ads-logo.png" style="width: 75%;"> 
                            
                        </div>
                        <div class="kt-login__signin">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">Sign In To Admin</h3>
                            </div>
                            <form class="kt-form" action="{{ route('login') }}" method="POST">
                                @csrf
                                <div class="input-group">
                                    <input id="email" placeholder="{{ __('E-Mail Address') }}" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="email" autofocus>
                                </div>
                                <div class="input-group">
                                    <input class="form-control" type="password" placeholder="{{ __('Password') }}"
                                        name="password">
                                </div>

                                <div class="input-group error"
                                    style="padding: 20px; color: red; text-shadow: 1px 1px 16px white;">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->all() as $error)
                                            {{ $error }}
                                        @endforeach
                                    @endif
                                </div>
                                <div class="input-group align-items-center">
                                    <label class="checkbox">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <span></span>
                                        {{ __('Remember Me') }}
                                    </label>
                                    <div class="col kt-align-right">
                                        {{-- <a href="{{ route('password.request') }}" id="" class="kt-login__link">{{ __('Forgot Your Password?') }}</a> --}}
                                    </div>
                                </div>
                                <div class="kt-login__actions">
                                    <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>

    </script>

    <!-- end::Global Config -->

    <!--begin:: Global Mandatory Vendors -->
    <script src="../assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
   {{--  <script src="../assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>

    <!--end:: Global Mandatory Vendors -->

    <!--begin:: Global Optional Vendors -->
    <script src="../assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
        type="text/javascript"></script>
    <script src="../assets/vendors/custom/components/vendors/bootstrap-datepicker/init.js" type="text/javascript">
    </script>
    <script src="../assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js"
        type="text/javascript"></script>
    <script src="../assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript">
    </script>
    <script src="../assets/vendors/custom/components/vendors/bootstrap-timepicker/init.js" type="text/javascript">
    </script>
    <script src="../assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript">
    </script>
    <script src="../assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js"
        type="text/javascript"></script>
    <script src="../assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript">
    </script>
    <script src="../assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js"
        type="text/javascript"></script>
    <script src="../assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript">
    </script>
    <script src="../assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript">
    </script>
    <script src="../assets/vendors/custom/components/vendors/bootstrap-switch/init.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js"
        type="text/javascript"></script>
    <script src="../assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js"
        type="text/javascript"></script>
    <script src="../assets/vendors/general/nouislider/distribute/nouislider.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/autosize/dist/autosize.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/summernote/dist/summernote.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/custom/components/vendors/bootstrap-notify/init.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript">
    </script>
    <script src="../assets/vendors/custom/components/vendors/jquery-validation/init.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/toastr/build/toastr.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/raphael/raphael.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/morris.js/morris.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
    <script src="../assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"
        type="text/javascript"></script>
    <script src="../assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/counterup/jquery.counterup.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/custom/components/vendors/sweetalert2/init.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/dompurify/dist/purify.js" type="text/javascript"></script>
 --}}
    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="../assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
    <script src="../assets/app/custom/login/login-general.js" type="text/javascript"></script>

</body>
</html>
