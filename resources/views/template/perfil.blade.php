@extends('template.general')


@section('content')	
<style type="text/css">

	.kt-form.nw_usr{
		width: 50%;
		margin-left: auto;
		margin-right: auto;
	}

	/* Boton crear usuario*/

	button.btn-success.creat{
		background-color: #46B346 !important;
		border-color: #46B346 !important;
	}
	.ocultar {
    display: none;
}
 
.mostrar {
    display: block;
}

</style>
<!--Validacion-->


<!--

<div id="msg"></div> 
<div id="error" class="alert alert-danger ocultar" role="alert">
    The passwords is wrong, try again !
</div>
<div id="ok" class="alert alert-success ocultar" role="alert">
    The password is correct ! 
</div>
-->

<!--begin::Form-->
<form id="perfil_form" class="kt-form nw_usr" method="POST" action="{{ $formaction }}"  >
	{{ csrf_field() }}
	
	<input type="hidden" value="{{ isset($user->public_id) ? $user->public_id : '' }}" name="id" />
	
	<div class="kt-portlet__body">
		<div class="form-group form-group-last">
			<div class="alert alert-secondary" role="alert">
				<div class="alert-icon"><i class="flaticon-user kt-font-brand"></i></div>
				<div class="alert-text">
					Edit my perfil <br>
				</div>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-warning alert-elevate" role="alert">
				<div class="alert-icon">
					<i class="flaticon-warning kt-font-brand"></i>
				</div>
				<div class="alert-text">											
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			</div>
		@endif
		
		
		@if ($user->deleted_at != "")
		<div class="alert alert-warning alert-elevate" role="alert">
			<div class="alert-icon">
				<i class="flaticon-warning kt-font-brand"></i>
			</div>
			<div class="alert-text">											
				<ul>
					<li>On {{ $user->deleted_at }} the account will be deleted</li>
				</ul>
			</div>
		</div>
		@endif
		
		<!-- {% dd(get_defined_vars()) %} -->
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label" >Name</label>
			<div class="col-10">
				<input class="form-control @error('name') is-invalid @enderror" name="name" type="text" value="{{  old('name') ?  old('name') : ( $user ? $user->name : '' ) }}" id="example-url-input" placeholder="Insert Name" required>
			</div>
		</div>
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label" >Password</label>
			<div class="col-10">
				<input class="form-control @error('password') is-invalid @enderror" name="password" type="password" value="" id="passwd" placeholder="Insert Pass">
			</div>
		</div>
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label" >Repeat Password</label>
			<div class="col-10">
				<input class="form-control @error('password') is-invalid @enderror" name="password_confirmation" type="password" value="" id="passwd2" placeholder="Insert Pass">
				
			</div>
		</div>
		
	<div class="registrationFormAlert" style="color:green;" id="CheckPasswordMatch">
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-2">
				</div>
				<div class="col-10">					
					<button type="submit" id="login" class="btn btn-success">Submit</button>					
					<button type="buttom" class="btn btn-secondary">Cancel</button>
					
		                        
	                        
				</div>
			</div>
		</div>
	</div>
</form>

<a href="#" class="btn  btn-danger px-4 py-3" data-toggle="modal" data-target="#delete_modal" style="bottom: 10px; left: 10px; position: absolute;">Delete my perfil</a>
	
		<!-- Modal-->
	<div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
		<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Delete Account</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					Are you sure you want to delete your profile?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back</button>
					<button type="button" class="btn btn-danger font-weight-bold btn-delete-profile">Confirm</button>
				</div>
			</div>
		</div>
	</div>



@endsection

@section('customscript')
	<script src="/assets/app/custom/general/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script>
		$(document).ready(function(){
			$("#perfil_form").validate({
				rules: {
					name:"required",
					password_confirmation: {
						equalTo: "#passwd"
					}
				},
				messages: {
					name:"This field is required",
					password_confirmation: "Enter Confirm Password Same as Password"
				}
			 });
			
			$(".btn-delete-profile").click(function() {
			  window.location.href = " {{ route('delete_perfil') }}"
			});
		});
		
		
   
  
	</script>
@endsection
