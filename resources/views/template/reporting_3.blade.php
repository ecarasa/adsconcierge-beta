@extends('template.general')
<!--begin::Page Vendors Styles(used by this page) -->
<link rel="stylesheet" href="https://rawgit.com/dbrekalo/attire/master/dist/css/build.min.css">
<!--link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"-->
<script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"-->

<link href="/css/reporting.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .dataSource {
        cursor: pointer;
        z-index: 1000;
    }

    .card {
        min-height: 400px;
    }

    .chartContainer2 {
        border: none;
    }

    .pull-right {
        left: auto !important;
    }

    .calItem {
        padding-left: 6px;
    }

    .calItem.active {
        font-weight: 600;
        color: #5d78ff !important;
    }

    .dropdown-menu {
        min-width: 8rem !important;
    }

    .ui-resizable-handle {
        position: absolute;
        font-size: 0.1px;
        display: block;
        touch-action: none;
        width: 30px;
        right: -15px;
    }

    .ui-resizable-handle:after {
        content: "";
        display: block;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        margin-top: -10px;
        width: 4px;
        height: 4px;
        border-radius: 50%;
        background: #ddd;
        box-shadow: 0 10px 0 #ddd, 0 20px 0 #ddd;
    }

    .modal {
        background: #80808078;
        position: absolute !important;
        border-radius: 0.25rem;
    }

    .modal-backdrop.fade.show {
        display: none;
    }

    p.editable {
        font-size: 1.25rem;
        font-weight: bold;
        border-bottom: 2px dashed;
    }

    .filters>div {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -ms-flex-line-pack: flex-first;
        align-content: flex-first;
        margin-bottom: 46px;
        margin-top: 23px;
    }

</style>
@section('content')
    <style type="text/css">
        @media (min-width: 576px) {

            .col-sm-3,
            .col-md-3 {
                width: 25%;
            }

            .col-sm-4,
            .col-md-4 {
                width: 33.33333%;
            }

            .col-sm-6,
            .col-md-6 {
                width: 50%;
            }

            .col-sm-12,
            .col-md-12 {
                width: 100%;
            }
        }

    </style>


    <div class="kt-portlet kt-portlet--mobile">

        @if ($edit)
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Make your reports here v3
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">

                            <div class="btn btn-md btn-tall kt-font-bold kt-font-transform-u dataSource ico_lineChart"
                                data-chart="line" style="float:left"><i
                                    class="kt-font-brand flaticon2-line-chart dataSource ico_lineChart"
                                    data-chart="line"></i></div>
                            <div class="btn btn-md btn-tall kt-font-bold kt-font-transform-u dataSource ico_radialBarChart"
                                data-chart="radialBar" style="float:left"><i
                                    class="kt-font-brand flaticon2-pie-chart-2 ico_radialBarChart"
                                    data-chart="radialBar"></i></div>
                            <div class="btn btn-md btn-tall kt-font-bold kt-font-transform-u dataSource ico_barChart"
                                data-chart="bar" style="float:left"><i
                                    class="kt-font-brand flaticon2-bar-chart ico_barChart" data-chart="bar"></i></div>

                            <div style="max-width:220px; min-width:150px; float:left">
                                <select class="form-control" name="reports" id="reports" style="max-width: 200px;">
                                    <option value="nuevo">nuevo</option>
                                    @foreach ($reports as $report)

                                        <?php
                                        $config = json_decode($report->configuration);
                                        //var_dump( $config);
                                        if (isset($config->default)) {
                                        $showDefault = (bool) $config->default;
                                        } else {
                                        $showDefault = false;
                                        }
                                        ?>
                                        <option value="{{ $report->public_id }}" @if ($report->public_id == $actual_id) selected @endif @if ($showDefault === true) selected
                                    @endif
                                    >{{ $report->name }}</option>
        @endforeach
        </select>
    </div>
    <button type="button" id="guardar" class="btn btn-brand btn-md btn-tall kt-font-bold kt-font-transform-u"
        style="float: left; margin-bottom: 0px; margin-left: 5px; margin-right: 5px;height: 38px;width: 48px;">
        <i class="la la-save"></i>
    </button>
    <button type="button" id="share" class="btn btn-brand btn-md btn-tall kt-font-bold kt-font-transform-u"
        style="float: left; margin-bottom: 0px; margin-left: 5px; margin-right: 5px;height: 38px;width: 48px;">
        <i class="la la-share"></i>
    </button>
    </div>
    </div>
    </div>
    </div>

    <div class="kt-portlet__head kt-portlet__head--lg">
        <div id="chart-filters" class="kt-portlet__head-label" style="width: 100%;">
            <div class="col-md-3">
                <select class="selectpicker select_top" id="select_clients" name="clients" data-entidad="clients" multiple
                    show-tick title="Clients" data-live-search="true" data-width="100%">
                    @foreach ($customers as $client)
                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                <select class="selectpicker select_top" id="select_campaigns" name="campaigns" data-entidad="campaigns"
                    multiple show-tick title="Campaigns" data-live-search="true" data-width="100%">
                    @foreach ($campaigns as $campaign)
                        <option value="{{ $campaign->id }}">{{ $campaign->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <select class="selectpicker select_top" id="select_platforms" name="platforms" data-entidad="platforms"
                    multiple show-tick title="Platforms" data-live-search="true" data-width="100%">
                    @foreach ($platforms as $platform)
                        <option value="{{ $platform->id }}">{{ $platform->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <select class="selectpicker select_top" id="select_periodo" name="periodo" data-entidad="periodo" show-tick
                    title="Periodo" data-live-search="true" data-width="100%">
                    <option value="today">Today</option>
                    <option value="yesterday">Yesterday</option>
                    <option value="last_2days">Last 2 days</option>
                    <option value="this_week">This week</option>
                    <option value="last_week">Last week</option>
                    <option value="last_7days">Last 7 days</option>
                    <option value="this_month">This month</option>
                    <option value="last_month">Last month</option>
                    <option value="last_30days">Last 30 days</option>
                    <option value="all_time">All time</option>
                </select>
            </div>
            <div class="col-md-2">
                <select class="selectpicker select_top" id="select_groupby" name="groupby" data-entidad="groupby" show-tick
                    title="Group by" data-live-search="true" data-width="100%" required>
                    <option value="dia">Days</option>
                    <option value="yearmonth">Months</option>
                    <option value="yearweek">Years</option>
                </select>
            </div>
        </div>
    </div>

    @endif

    <div class="kt-portlet__body">
        <!--begin: Dashboard -->
        <div class="container-fluid">
            <div class="row fila" id="contentCard">
            </div>
        </div>
        <div class="modal" id="kt_modal_nombre" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Report</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Name:</label>
                                <input type="text" class="form-control" id="dashboard-name">
                            </div>
                            <div class="form-group" style="padding: 17px;">
                                <input class="form-check-input" type="checkbox" name="setreport_default"
                                    id="setreport_default"> Set as default
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="guardaNameDashboard">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end: Dashboard -->
    </div>
    </div>


@endsection

@section('customscript')

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"-->
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="/js/reporting.js" type="text/javascript"></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js" type="text/javascript"></script>

    <!--script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
        rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js">
    </script>


    <script>
        const ENDPOINTS = {
            REQUEST_REPORT: '/api/reporting/config',
            REQUEST_DATA: '/api/reporting/data',
            SAVE_DATA: '/api/reporting/save',
        };

        const CHART_TYPES = {
            RADIAL_BAR: 'radialBar',
            LINE: 'line',
            BAR: 'bar'
        };
        const CHART_EVENTS = ['impressions', 'cost', 'clicks', 'conversions'];
        const CHARTS_APEX = [];

        let CURRENT_REPORT = @json($configuration);
        console.log("default confi", CURRENT_REPORT)

        let reportId = '{{ $actual_id }}';
        reportId = reportId !== '' ? reportId : null;
        console.log("default reportId", reportId)

        const droppableChartOption = {

            drop: async function(event) {
                //loading.show();

                /* console.log('event.originalEvent.toElement', event.originalEvent.toElement)
                 console.log('event.originalEvent.toElement', event.originalEvent)
                 console.log('event.originalEvent.toElement', $(event.originalEvent.toElement).data("chart"))
                 console.log('event.originalEvent.toElement', $(event.originalEvent.toElement).data("data-chart"))
                 console.log('event.originalEvent.toElement', $(event.originalEvent.toElement).attr("data-chart"))*/


                let chartType = $(event.originalEvent.toElement).data("chart");
                let $contenedor = $(this);
                let isNewChart = $contenedor.children().length === 0;
                let chartIndex = +$contenedor.data('id') - 1;

                let {
                    series,
                    labels
                } = await getReportingData(chartType, chartIndex, isNewChart);

                let options = await getChartOptions(chartType, series, labels);

                await createChart($(this), chartType, chartIndex, isNewChart, options);
                //loading.hide();
            }


        };

        const savedCharts = [];


        $(document).ajaxStart(function() {
            loading.show();
            console.log('start')
        });

        $(document).ajaxComplete(function() {
            loading.hide();
            $('.kt-dialog--loader').hide();

            console.log('end complete')
        });

        $(document).ready(function() {
            loadSavedReport();
            reportingSetup();
        });

        function createChart($contenedor, chartType, chartIndex, isNewChart, options, title = null) {
            let contenedorId = $contenedor.attr('id');
             console.log('contenedorId', contenedorId  );
            $contenedor.data('chart', chartType);
            $contenedor.empty();

            let $header = $(`#header-${contenedorId}`);
            let headerTitle = title ? title : `New ${chartType} chart`;
            $header
                .html(headerTitle)
                .addClass('editable')
                .attr('contenteditable', 'true');

            let chartFilters = `
                                                                                <div class="kt-portlet__head-label" style="width: 100%;">
                                                                                    <div class="col-md-3">
                                                                                        <select class="selectpicker" id="select_customers-${contenedorId}" data-chart="${chartIndex}" name="clients" data-entidad="clients"
                                                                                                multiple show-tick title="Clients" data-live-search="true" data-width="100%">
                                                                                            @foreach ($customers as $client)
                                                                                                <option value="{{ $client->id }}">{{ $client->name }}</option>
                                                                                            @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <select class="selectpicker" id="select_campaigns-${contenedorId}" data-chart="${chartIndex}" name="campaigns"
                                                                                            data-entidad="campaigns" multiple show-tick title="Campaigns" data-live-search="true"
                                                                                            data-width="100%">
                                                                    @foreach ($campaigns as $campaign)
                                                                        <option value="{{ $campaign->id }}">{{ $campaign->name }}</option>
                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <select class="selectpicker" id="select_platforms-${contenedorId}" data-chart="${chartIndex}" name="platforms"
                                                                                            data-entidad="platforms" multiple show-tick title="Platforms" data-live-search="true"
                                                                                            data-width="100%">
                                                                    @foreach ($platforms as $platform)
                                                                        <option value="{{ $platform->id }}">{{ $platform->name }}</option>
                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <select class="selectpicker" id="select_period-${contenedorId}" data-chart="${chartIndex}" name="periodo" data-entidad="periodo"
                                                                                            show-tick title="Periodo" data-live-search="true" data-width="100%">
                                                                                        <option value="today">Today</option>
                                                                                        <option value="yesterday">Yesterday</option>
                                                                                        <option value="last_2days">Last 2 days</option>
                                                                                        <option value="this_week">This week</option>
                                                                                        <option value="last_week">Last week</option>
                                                                                        <option value="last_7days">Last 7 days</option>
                                                                                        <option value="this_month">This month</option>
                                                                                        <option value="last_month">Last month</option>
                                                                                        <option value="last_30days">Last 30 days</option>
                                                                                        <option value="all_time">All time</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <select class="selectpicker" id="select_groupby-${contenedorId}" data-chart="${chartIndex}" name="groupby" data-entidad="groupby"
                                                                                            show-tick title="Group by" data-live-search="true" data-width="100%" required>
                                                                                        <option value="dia">Days</option>
                                                                                        <option value="yearmonth">Months</option>
                                                                                        <option value="yearweek">Years</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>`;
            let $filters = $contenedor.parent().find('.filters');
            $filters.html(chartFilters);

            let appliedFilters = getGlobalFilters();
            updateFilters(appliedFilters, contenedorId);
console.log('contenedorId options', contenedorId , options );
            CHARTS_APEX[chartIndex] = renderChart(contenedorId, options);

            if (isNewChart) {
                createChartPlaceholder();
            }
        }

        function updateFilters(appliedFilters, contenedorId) {
            Object.keys(appliedFilters).forEach(key => {
                let value = appliedFilters[key];
                if (value === undefined) return;
                let values = typeof value === 'string' ? [value] : [...value];
                values.forEach(value => $(`#select_${key}-${contenedorId} option[value="${value}"]`).attr(
                    'selected', 'selected'));
            });

            $(".selectpicker").off("change", onFilterChange)
                .change(onFilterChange)
                .selectpicker('refresh');
        }

        function renderChart(contenedorId, options) {
            // TODO: show Loading state
            let chart = new ApexCharts(document.querySelector(`#${contenedorId}`), options);
            chart.render();
            // TODO: hide specific series
            // chart.hideSeries('impressions');
            return chart;
        }

        function getChartOptions(chartType, series, labels) {
          console.log('getChartOptions'  ,chartType, CHART_TYPES, series  , labels);
          console.log('getChartOptions'  ,chartType );
            let options = {};

            switch (chartType) {
                case CHART_TYPES.LINE:
                    options = {
                        chart: {
                            type: 'line',
                            height: 350,
                            toolbar: {
                                show: true,
                                offsetX: 0,
                                offsetY: -30,
                                tools: {
                                    download: true,
                                    selection: true,
                                    zoom: false,
                                    zoomin: false,
                                    zoomout: false,
                                    pan: false,
                                    reset: false | '<img src="/static/icons/reset.png" width="20">'
                                },
                                autoSelected: 'zoom'
                            },    
                        },
                         series,
                        //series: [],
                        xaxis: {
                          categories: labels,
                          convertedCatToNumeric: false,
                            type: "category", //datetime or numeric
                           // esto es un int, no un string tickAmount: "dataPoints" , //"dataPoints",
                            labels: {
                                hideOverlappingLabels: true,
                                trim: false,
                              //  show: true,
                             //   minHeight: 100,
                             //   maxHeight: 200,
                            }
                        },
                        /* axisTicks: {
                            show: true,
                            borderType: 'solid',
                            color:  '#78909C',
                            height: 6,
                            offsetX: 0,
                            offsetY: 0
                        }, */
                        redrawOnParentResize: true,
                        labels: ['impresions'],
                        lshow: false
                    };
                    /**
                    options = {
                        ...options,
                        series,
                        xaxis['categories']: labels,
                            
                        }                       
                    }; */

                    break;
                case CHART_TYPES.RADIAL_BAR:
             var newseries=[]; 
             var newlabels=[]; 
                series.forEach(function(element){
                  newlabels.push( element.name );
                  newseries.push(
                   // {name: element.name, 
                    // data: element.data.reduce(function(a,b) { return a + b; } ,0 ), 
                   element.data.reduce(function(a,b) { return parseInt(a) + parseInt(b); } ,0 ), 
                   // }  
                  ); 
                
                });  
                  console.log('oppss', newseries,newlabels, options);
                    options = {
                        chart: {
                            height: 350,
                            type: "radialBar",
                            toolbar: {
                                show: true,
                                offsetX: 35,
                                offsetY: 0,
                                tools: {
                                    download: true,
                                    selection: true,
                                    zoom: false,
                                    zoomin: false,
                                    zoomout: false,
                                    pan: false,
                                    reset: false | '<img src="/static/icons/reset.png" width="20">'
                                },
                                autoSelected: 'zoom'
                            },
                        },
                       series: newseries,
                    //  newseries, 
                        plotOptions: {
                            radialBar: {
                                dataLabels: {
                                    total: {
                                        show: true,
                                        label: 'TOTAL'
                                    }
                                }
                            }
                        },
                        //labels: [],
                       labels : newlabels, // ['impressions', 'clicks', 'ecpm'],
                        legend: {
                            show: true,
                            floating: true,
                            fontSize: '16px',
                            position: 'left',
                            offsetX: -30,
                            offsetY: 0,
                            labels: {
                                useSeriesColors: true,
                            },
                            markers: {
                                size: 0
                            },
                            formatter: function(seriesName, opts) {
                                return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex]
                            },
                            itemMargin: {
                                vertical: 3
                            }
                        },
                    };
                console.log('oppss', newseries, options);
               /*     series = [60, 84, 90];
                 labels = ['impressions', 'clicks', 'ecpm'];
                    options = {
                        ...options,
                        series,
                        labels
                    }; */
                    break;
                case CHART_TYPES.BAR:
                    options = {
                        chart: {
                            type: 'bar',
                            height: 350,
                            toolbar: {
                                show: true,
                                offsetX: 0,
                                offsetY: -30,
                                tools: {
                                    download: true,
                                    selection: true,
                                    zoom: false,
                                    zoomin: false,
                                    zoomout: false,
                                    pan: false,
                                    reset: false | '<img src="/static/icons/reset.png" width="20">'
                                },
                                autoSelected: 'zoom'
                            },
                        },
                        series: [],
                        xaxis: {
                            type: "datetime"
                        },
                        redrawOnParentResize: true,
                        labels: [],
                        lshow: false
                    };

                    options = {
                        ...options,
                        series,
                        xaxis: {
                            categories: labels
                        }
                    };
                    break;
                default:
                    throw new Error(`Chart type not implemented: [${chartType}]`);
            }
            console.log('options return', options);
            return options;
        }

        function setDefaultFilters() {
            $('#select_periodo').val('last_7days');
            $('#select_groupby').val('dia');
            $('.selectpicker').selectpicker('refresh');
        }

        function loadSavedReport() {

            console.log(' loadSavedReport start', CURRENT_REPORT);

            if (null === CURRENT_REPORT || !CURRENT_REPORT.charts || 0 === CURRENT_REPORT.charts.length) {
                setDefaultFilters();
                return false;
            }
            console.log(' loadSavedReport pasamos');

            if (typeof CURRENT_REPORT.charts === 'string') CURRENT_REPORT.charts = JSON.parse(CURRENT_REPORT.charts);

            $('#contentCard').empty();
            createChartPlaceholder();

            console.log(' loadSavedReport dale dale');

            CURRENT_REPORT.charts.forEach(async (report, index) => {

                let {
                    series,
                    labels
                } = await getChartData(report);

                saveChartInfo(0, true, report);
                let options = await getChartOptions(report.type, series, labels);
                await createChart($(`#chart${index + 1}`), report.type, index, true, options, report.title);
                updateFilters(report, `chart${index + 1}`);

            });

            //loading.hide();



        }

        function reportingSetup() {
            @if ($edit)
                createChartPlaceholder();
            @endif
            handlers();
        }

        function handlers() {

            $('.ico_lineChart').draggable({
                helper: function(event) {
                    return $('<i class="kt-font-brand flaticon2-line-chart dataSource" data-chart="line"></i>');
                },
                //helper: 'clone',
                scroll: true,
                scrollSensitivity: 100,
                cursor: 'move'
            });

            $('.ico_radialBarChart').draggable({

                helper: function(event) {
                    return $(
                        '<i class="kt-font-brand flaticon2-pie-chart-2 dataSource" data-chart="radialBar"></i>'
                    );
                },
                //helper: 'clone',
                scroll: true,
                scrollSensitivity: 100,
                cursor: 'move'
            });

            $('.ico_barChart').draggable({

                helper: function(event) {
                    return $('<i class="kt-font-brand flaticon2-bar-chart dataSource" data-chart="bar"></i>');
                },
                //helper: 'clone',
                scroll: true,
                scrollSensitivity: 100,
                cursor: 'move'
            });

            $("#reports").change(onReportChange);

            $("#guardar").click(saveReport);

            $("#share").click(() => {
                let reportsValue = $('#reports').val();
                let url = `{{ URL::to('/') }}/reporting3/public/${reportsValue}`;
                window.open(url, '_blank');
            });

            $("#guardaNameDashboard").click(saveNewReport);

            $(".selectpicker").change(onFilterChange);

        }

        async function onFilterChange(event) {

            let isGlobalFilter = $(event.target).hasClass('select_top');
            let chartIndex = $(event.target).data('chart');

            for (const chart of savedCharts) {
                const index = savedCharts.indexOf(chart);

                if (isGlobalFilter || index === chartIndex) {
                    let {
                        series,
                        labels
                    } = await getReportingData(chart.type, index, false);
                    let options = await getChartOptions(chart.type, series, labels);
                    CHARTS_APEX[index].updateOptions(options);
                }
            }

        }

        async function onReportChange() {
            const reportsValue = $('#reports').val();
            const isNewReport = reportsValue === "nuevo";
            let nextUrl = `{{ URL::to('/') }}/reporting3`;

            if (isNewReport) {
                window.history.pushState('', '', nextUrl);
                location.reload();
            } else {
                nextUrl = `{{ URL::to('/') }}/reporting3?id=${reportsValue}`;
                window.history.pushState('', '', nextUrl);

                CURRENT_REPORT = await getSavedReport(reportsValue);
                loadSavedReport();
            }
        }

        async function getSavedReport(id) {
            let data = [];
            try {
                //loading.show();
                await $.getJSON(ENDPOINTS.REQUEST_REPORT, {
                    id
                }, response => {
                    data = response;
                }).fail(error => console.log('ERROR', error));
                loading.hide();
            } catch (e) {
                // TODO: Lanzar toast o algo similar!!
                //loading.hide();
                console.log('e', e);
            }

            return data;
        }

        function saveReport() {
            const isNewReport = $("#reports option:selected").text() === "nuevo";

            if (isNewReport) {
                $('#kt_modal_nombre').modal('show');
            } else {
                saveCreatedReport();
            }

        }

        function saveCreatedReport() {
            const reportId = $('#reports').val();
            saveNewReport(reportId);
        }

        function saveNewReport(reportId = null) {

            $('#kt_modal_nombre').modal('hide');

            savedCharts.forEach((chart, index) => {
                chart.title = $(`#header-chart${index + 1}`).html();

                let hiddenSeries = $(`#chart${index + 1}`).find(
                    '.apexcharts-legend-series.apexcharts-inactive-legend').map((index, legend) => $(legend)
                    .attr('seriesname'));
                chart.hiddenSeries = [...hiddenSeries];
            });

            var formData = new FormData();
            formData.append('name', $('#dashboard-name').val())
            formData.append('id', reportId)
            formData.append('default', $('#setreport_default').is(':checked'))
            formData.append('charts', JSON.stringify(savedCharts))

            loading.show();

            $.ajax({
                url: ENDPOINTS.SAVE_DATA,
                type: "POST",

                contentType: false,
                dataType: "json",
                processData: false,

                method: 'POST',
                data: formData,
            }).done(response => {


                if (response.success) {
                    toastr.success(response.msj, "Reporting v3");
                } else {
                    toastr.error(response.msj, "Reporting v3");
                }
            });
            loading.hide();

        }

        function getGlobalFilters() {
            return {
                customers: $("#select_clients").val() || '',
                platforms: $("#select_platforms").val() || '',
                campaigns: $("#select_campaigns").val() || '',
                period: $("#select_periodo").val() || 'last_7days',
                groupBy: $("#select_groupby").val() || 'dia',
            };
        }

        function getChartFilters(chartIndex) {
            let $customers = $(`#select_customers-chart${chartIndex + 1}`);
            let $platforms = $(`#select_platforms-chart${chartIndex + 1}`);
            let $campaigns = $(`#select_campaigns-chart${chartIndex + 1}`);
            let $period = $(`#select_period-chart${chartIndex + 1}`);
            let $groupBy = $(`#select_groupby-chart${chartIndex + 1}`);

            let customers = $customers.val().length ? $customers.val() : $("#select_clients").val();
            let platforms = $platforms.val().length ? $platforms.val() : $("#select_platforms").val();
            let campaigns = $campaigns.val().length ? $campaigns.val() : $("#select_campaigns").val();
            let period = $period.val().length ? $period.val() : $("#select_periodo").val();
            let groupBy = $groupBy.val().length ? $groupBy.val() : $("#select_groupby").val();

            return {
                customers: customers,
                platforms: platforms,
                campaigns: campaigns,
                period: period,
                groupBy: groupBy,
            };
        }

        async function getReportingData(chartType = CHART_TYPES.LINE, chartIndex = 0, isNewChart = true) {
            const filters = isNewChart ? getGlobalFilters() : getChartFilters(chartIndex);

            const chart = {
                type: chartType,
                metrics: CHART_EVENTS,
                title: $(`#header-chart${chartIndex + 1}`).html()
            };

            let payload = {
                ...filters,
                ...chart
            };

            let data = await getChartData(payload);
            saveChartInfo(chartIndex, isNewChart, payload);

            return data;
        }

        /*  async function getChartData(payload) {
              let data = {
                  series: [],
                  labels: []
              };
              try {
                  await $.postJSON(ENDPOINTS.REQUEST_DATA, payload, response => {
                      data = response;
                  }).fail(error => console.log('ERROR', error));
              } catch (e) {
                  // TODO: Lanzar toast o algo similar!!
                  console.log('e', e);
              }

              return data;
          } */

        async function getChartData(payload) {

            var data = {
                series: [],
                labels: []
            };

            try {

                await $.post(ENDPOINTS.REQUEST_DATA, payload)
                    .done(function(response) {
                        data = response;
                    });

            } catch (e) {
                toastr.error("Error getting data report.", "Reporting v3");
                console.log('e', e);
            }

            return data;
        }

        function saveChartInfo(chartIndex, isNewChart, payload) {
            if (isNewChart) {
                savedCharts.push(payload);
            } else {
                savedCharts[chartIndex] = payload;
            }
        }

        function createChartPlaceholder() {
            const chartCount = $('.chart.droppable-area').length + 1;
            const chartId = `chart${chartCount}`;
            const chartHeaderId = `header-chart${chartCount}`;
            const container = ` <div class="col-md-6 card resizable">
                                    <div class="card-body">
                                        <div class="col-md-12 container">
                                            <div class="row">
                                                <p id="${chartHeaderId}">Arrastre hasta aquí su gráfica</p>
                                            </div>
                                            <div class="filters"></div>
                                            <div id="${chartId}" data-id="${chartCount}" class="chart droppable-area" style="height: 400px; width: 100%;"></div>
                                        </div>
                                    </div>
                                </div>`;
            $("#contentCard").append(container);
            $(".droppable-area").droppable(droppableChartOption);
            cardResizeHandler();
        }

        function cardResizeHandler() {
            let resizableEl = $('.resizable').not(':last-child'),
                columns = 12,
                fullWidth = resizableEl.parent().width(),
                columnWidth = fullWidth / columns,
                totalCol, // this is filled by start event handler
                updateClass = function(el, col) {
                    if (col <= 3) {
                        col = 3;
                    } else if (col > 3 && col < 5) {
                        col = 4;
                    } else if (col >= 5 && col < 7) {
                        col = 6;
                    } else if (col >= 7) {
                        col = 12
                    }

                    el.data('col', col);
                    el.css('width', ''); // remove width, our class already has it
                    el.removeClass(function(index, className) {
                        return (className.match(/(^|\s)col-\S+/g) || []).join(' ');
                    }).addClass('col-md-' + col); //ancho de los cuadros
                };
            resizableEl.resizable({
                handles: 'e',
                start: function(event, ui) {
                    let target = ui.element;
                    let totalCol = 12;

                    target.resizable('option', 'minWidth', columnWidth);
                    target.resizable('option', 'maxWidth', ((totalCol - 1) * columnWidth));
                },
                resize: function(event, ui) {
                    let target = ui.element,
                        targetColumnCount = Math.round(ui.size.width / columnWidth);

                    updateClass(target, targetColumnCount);
                },
                stop: function(event, ui) {
                    let col = $(ui.element).data('col');
                    let index = +$(ui.element).find('.chart').data('id') - 1;
                    savedCharts[index].col = col;
                }
            });
        }

    </script>
@endsection
