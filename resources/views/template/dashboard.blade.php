@extends('template.general')
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

<link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;700&family=Source+Serif+Pro:wght@600&display=swap" rel="stylesheet">
<link href="https://cdn.iconmonstr.com/1.3.0/css/iconmonstr-iconic-font.min.css">
<link href="../assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/css/build.css">
<script src="/js/build.min.js"></script>
<link href="/css/reporting.css" rel="stylesheet" type="text/css" />
<link href="/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<style>
    .filterDateHometable{
        margin-left: 18px;
    }
    </style>
@section('content')

    @if (session()->has('cancelMassageSuccess'))
        <div class="alert alert-success">
            <div class="col-12">{{ session()->get('cancelMassageSuccess') }}</div>
            <a href="#" class="close float-right" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
        @php
            session()->forget('cancelMassageSuccess');
        @endphp
    @endif

    @if (Session::has('cancelMassageFail'))
        <div class="col-12">{{ Session::get('cancelMassageFail') }}</div>
        <a href="#" class="close float-right" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
        @php
            session()->forget('cancelMassageFail');
        @endphp
    @endif

    @if (Session::has('changePlanSuccess'))
        <div class="alert alert-success">
            <div class="col-12">{{ Session::get('changePlanSuccess') }}</div>
            <a href="#" class="close float-right" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
        @php
            session()->forget('changePlanSuccess');
        @endphp
    @endif
    @if (Session::has('changePlanFail'))
        <div class="col-12">{{ Session::get('changePlanFail') }}</div>
        <a href="#" class="close float-right" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
        @php
            session()->forget('changePlanFail');
        @endphp
    @endif

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17">
                        <path  d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"/>
                    </svg>
                </span>
                <h3 class="kt-portlet__head-title">
                    Cuál es la tendencia de tus KPIs más importantes
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                        </div>
                        <button type="button" class="btn btn-info btn-icon-left"><svg id="_27_Icon_download" data-name="27) Icon/download" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                            <defs>
                              <clipPath id="clip-path">
                                <path d="M15,17H1a1,1,0,0,1-1-1V14a1,1,0,1,1,2,0v1H14V14a1,1,0,1,1,2,0v2a1,1,0,0,1-1,1ZM7.425,11.818,3.425,9A1,1,0,0,1,4.575,7.368L7,9.076Q7,9.038,7,9V1A1,1,0,1,1,9,1V9H9l2.4-1.8a1,1,0,0,1,1.2,1.6l-4,3a1,1,0,0,1-1.175.018Z" transform="translate(4 3)" fill="#FFFFFF"/>
                              </clipPath>
                            </defs>
                            <g id="Group_164" data-name="Group 164">
                              <path d="M15,17H1a1,1,0,0,1-1-1V14a1,1,0,1,1,2,0v1H14V14a1,1,0,1,1,2,0v2a1,1,0,0,1-1,1ZM7.425,11.818,3.425,9A1,1,0,0,1,4.575,7.368L7,9.076Q7,9.038,7,9V1A1,1,0,1,1,9,1V9H9l2.4-1.8a1,1,0,0,1,1.2,1.6l-4,3a1,1,0,0,1-1.175.018Z" transform="translate(4 3)" fill="#FFFFFF"/>
                            </g>
                          </svg>
                           Descargar PDF</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Dashboard -->
            <div class="container-fluid">

                <!--begin: cabecera filtros -->
                <div class="row fila">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form-inline d-flex" role="form" style="margin-bottom: 0px;">
                                    <div class="row">
                                        <div class="col-md-4 kt-form__control">
                                            <select name="sitio" class="selectpicker" id="select_clients"
                                                data-entidad="clients" multiple show-tick title="Clients"
                                                data-live-search="true">
                                                @foreach ($customers as $customer)
                                                    <option value="{{ $customer->public_id }}">{{ $customer->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="formato" class="selectpicker" id="select_platforms"
                                                data-entidad="platforms" multiple show-tick title="Platforms"
                                                data-live-search="true">
                                                <option value="Formato1">Formato1</option>
                                                <option value="Formato2">Formato2</option>
                                                <option value="Formato3">Formato3</option>
                                                <option value="Formato4">Formato4</option>
                                                <option value="Formato5">Formato5</option>
                                                <option value="Formato6">Formato6</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="campana" class="selectpicker" id="select_campaigns"
                                                data-entidad="campaigns" multiple show-tick title="Campaigns"
                                                data-live-search="true">
                                                @foreach ($campaigns as $campaign)
                                                    <option value="{{ $campaign->id }}">{{ $campaign->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <fieldset class="gender alineacion2" style="margin-right:10px;">
                                        <ul style="margin-bottom: 0px;">
                                            <li class="radpe_btn_gen" style="height:40px !important;">
                                                <input type="radio" name="frecuencia" id="frecuencia1" value="1"
                                                    class="tod r_frecuencia" checked="checked">
                                                <label for="frecuencia1" style="margin:0px;">
                                                    Daily
                                                    <!--<i class="flaticon2-checkmark"></i>-->
                                                </label>
                                                <input type="radio" name="frecuencia" id="frecuencia7" value="7"
                                                    class="hom r_frecuencia">
                                                <label for="frecuencia7" style="margin:0px;">
                                                    Weeks
                                                    <!--<i class="flaticon2-checkmark"></i>-->
                                                </label>
                                                <input type="radio" name="frecuencia" id="frecuencia3" value="30"
                                                    class="muj r_frecuencia">
                                                <label for="frecuencia3" style="margin:0px;">
                                                    Monthly
                                                    <!--<i class="flaticon2-checkmark"></i>-->
                                                </label>
                                            </li>
                                        </ul>
                                    </fieldset>
                                    <div class="btn-group alineacion2" data-toggle="buttons">
                                        <div class="kt-form__control">
                                            <select class="selectpicker" id="periodo" aria-invalid="false"
                                                Placeholder="Periodo" name="periodo" id="periodo">
                                                <option value="null" id="periodo_">Period</option>
                                                <option value="30" selected>Last 30 days</option>
                                                <option value="7">Last 7 days</option>
                                                <option value="2">Last 3 days</option>
                                                <option value="0">Today</option>
                                                <option value="1">Yesterday</option>
                                                <option value="29">This Month</option>
                                                <option value="31">Last Month</option>
                                                <option value="364">This Year</option>
                                                <option value="365">Last Year</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row d-none" id="date_picker">
                                        <div class="kt-form__control px-3">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="date" class="form-control date-picker" id="date_picker_1"
                                                    name="date_picker_1" placeholder="mm/dd/yyyy">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-form__control">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="date" class="form-control date-picker" id="date_picker_2"
                                                    name="date_picker_2" placeholder="mm/dd/yyyy">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-group alineacion2 align-items-center" data-toggle="buttons">
                                        <button type="button" id="d_cargar_db" class="btn btn-primary btn-icon" style="float: right; margin-bottom: 0px; margin-left: 15px; margin-right: 5px;">
                                            <svg id="_27_Icon_Sync" data-name="27) Icon/Sync" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                                <defs>
                                                  <clipPath id="clip-path">
                                                    <path d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"/>
                                                  </clipPath>
                                                </defs>
                                                <g id="Group_258" data-name="Group 258">
                                                  <path id="_Icon_Сolor-2" data-name="🎨 Icon Сolor" d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"/>
                                                </g>
                                              </svg>
                                              
                                        </button>
                                        <button type="button" id="d_guardar" class="btn btn-primary btn-icon-left"  style="float: right; margin-bottom: 0px; margin-left: 5px; margin-right: 5px;"> 
                                            <svg id="_27_Icon_save" data-name="27) Icon/save" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                                <defs>
                                                  <clipPath id="clip-path">
                                                    <path d="M15,18H3a3,3,0,0,1-3-3V3A3,3,0,0,1,3,0h7.172a2.978,2.978,0,0,1,2.121.879l4.828,4.828A2.982,2.982,0,0,1,18,7.829V15A3,3,0,0,1,15,18ZM3,2A1,1,0,0,0,2,3V15a1,1,0,0,0,1,1H5V13a1,1,0,0,1,1-1h6a1,1,0,0,1,1,1v3h2a1,1,0,0,0,1-1V7.829a1,1,0,0,0-.293-.708L10.879,2.293A1.011,1.011,0,0,0,10.172,2H7V6h3a1,1,0,0,1,0,2H6A1,1,0,0,1,5,7V2ZM7,14v2h4V14Z" transform="translate(3 3)" fill="#FFFFFF"/>
                                                  </clipPath>
                                                </defs>
                                                <g id="Group_153" data-name="Group 153">
                                                  <path id="_Icon_Сolor-2" data-name="🎨 Icon Сolor" d="M15,18H3a3,3,0,0,1-3-3V3A3,3,0,0,1,3,0h7.172a2.978,2.978,0,0,1,2.121.879l4.828,4.828A2.982,2.982,0,0,1,18,7.829V15A3,3,0,0,1,15,18ZM3,2A1,1,0,0,0,2,3V15a1,1,0,0,0,1,1H5V13a1,1,0,0,1,1-1h6a1,1,0,0,1,1,1v3h2a1,1,0,0,0,1-1V7.829a1,1,0,0,0-.293-.708L10.879,2.293A1.011,1.011,0,0,0,10.172,2H7V6h3a1,1,0,0,1,0,2H6A1,1,0,0,1,5,7V2ZM7,14v2h4V14Z" transform="translate(3 3)" fill="#FFFFFF"/>
                                                </g>
                                              </svg>
                                              
                                             Guardar</button>
                                        <div class="kt-form__control">
                                            <select class="selectpicker" aria-invalid="false" name="s_dashboards"
                                                id="s_dashboards">
                                                <option value="-1">New Report</option>
                                                @foreach ($reports['dashboards'] as $dashboard)
                                                    <?php
                                                    $config = json_decode($dashboard->configuration);

                                                    if (isset($config->default)) {
                                                    $showDefault = $config->default;
                                                    } else {
                                                    $showDefault = false;
                                                    }
                                                    ?>
                                                    <option value="{{ $dashboard->public_id }}" <?php if ($showDefault=='true' ) { echo 'selected' ; }?>>
                                                        <?php  
                                                        if ($showDefault=='true' ) { echo '** ' ; }   ?> {{ $dashboard->name }}   </option>

                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
                <!--end: cabecera filtros -->


                <!--begin: chart-->
                <div class="row fila">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!--div class="btn-group btn-group-toggle alineacion" data-toggle="buttons"-->
                                        <fieldset class="" style="margin-right:10px;">
                                            <ul style="margin-bottom: 0px; padding-inline-start:0px;">
                                                <li class="radpe_btn_gen">
                                                    <input type="radio" name="caoptions" id="impresions" value="impresions"
                                                        class="tod r_grafica" checked="checked">
                                                    <label for="impresions" class="text-left"
                                                        style="height: 100px; line-height:30px;">
                                                        <span class="parrafo1">Impresions</span><br>
                                                        <span class="parrafo2" id="p_impresions"></span><br>
                                                    </label>
                                                    <input type="radio" name="caoptions" id="spend" value="spend"
                                                        class="hom r_grafica">
                                                    <label for="spend" class="text-left"
                                                        style="height: 100px; line-height:30px;">
                                                        <span class="parrafo1">Spend</span><br>
                                                        <span class="parrafo2" id="p_spend"></span> EUR<br>
                                                    </label>
                                                    <input type="radio" name="caoptions" id="ecpm" value="ecpm"
                                                        class="hom r_grafica">
                                                    <label for="ecpm" class="text-left"
                                                        style="height: 100px; line-height:30px;">
                                                        <span class="parrafo1">eCPM</span><br>
                                                        <span class="parrafo2" id="p_ecpm"> </span> EUR<br>
                                                    </label>
                                                    <input type="radio" name="caoptions" id="clicks" value="clicks"
                                                        class="hom r_grafica">
                                                    <label for="clicks" class="text-left"
                                                        style="height: 100px; line-height:30px;">
                                                        <span class="parrafo1">Clicks</span><br>
                                                        <span class="parrafo2" id="p_clicks"></span><br>
                                                    </label>
                                                    <input type="radio" name="caoptions" id="ctr" value="ctr"
                                                        class="muj r_grafica">
                                                    <label for="ctr" class="text-left"
                                                        style="height: 100px; line-height:30px;">
                                                        <span class="parrafo1">Ctr</span><br>
                                                        <span class="parrafo2" id="p_ctr"></span><span>%</span><br>
                                                    </label>
                                                </li>
                                            </ul>
                                        </fieldset>
                                        <!--/div-->
                                    </div>
                                </div>
                                <div class="row fila">
                                    <div class="col-md-12" id="chart">
                                        <!--img src="img/grafica.png" style="width: 100%;"-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end: chart-->

                <!--begin: channels buttons-->
                <div class="row fila">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="parrafo3 pl-0 mb-2">Channels</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="align-items-center col-6 d-flex">
                                        <!-- 
                                                                                                1	Facebook
                                                                                                2	Linkedin
                                                                                                3	Twitter
                                                                                                4	Instagram
                                                                                                5	Snapchat
                                                                                                6	GAnalytics
                                                                                                7	SunNativo
                                                                                                8	FB Insights
                                                                                                9	Pinterest
                                                                                                10	Adwords
                                                                                                11	RSS
                                                                                                -->
                                        <button type="button" id="channel_all" onclick="javascript:allChannels()"
                                            class="btn btn-primary btn-icon channel-buton-all mr-2">All</button>
                                        <button type="button" id="channel_1"
                                            onclick="javascript:toggleChannel('channels', 1);"
                                            class="btn btn-primary btn-icon channel-buton mr-2">
                                            <svg id="_27_Icon_facebook-fill" data-name="27) Icon/facebook-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                                <defs>
                                                  <clipPath id="clip-path">
                                                    <path d="M11,.5a.5.5,0,0,0-.5-.5H8A4.77,4.77,0,0,0,3,4.5V7.2H.5a.5.5,0,0,0-.5.5v2.6a.5.5,0,0,0,.5.5H3v6.7a.5.5,0,0,0,.5.5h3a.5.5,0,0,0,.5-.5V10.8H9.619a.5.5,0,0,0,.483-.366l.721-2.6a.5.5,0,0,0-.481-.634H7V4.5a.954.954,0,0,1,1-.9h2.5a.5.5,0,0,0,.5-.5Z" transform="translate(6 3)" fill="#FFFFFF"/>
                                                  </clipPath>
                                                </defs>
                                                <g id="Group_359" data-name="Group 359">
                                                  <path id="_Icon_Сolor-2" data-name="🎨 Icon Сolor" d="M11,.5a.5.5,0,0,0-.5-.5H8A4.77,4.77,0,0,0,3,4.5V7.2H.5a.5.5,0,0,0-.5.5v2.6a.5.5,0,0,0,.5.5H3v6.7a.5.5,0,0,0,.5.5h3a.5.5,0,0,0,.5-.5V10.8H9.619a.5.5,0,0,0,.483-.366l.721-2.6a.5.5,0,0,0-.481-.634H7V4.5a.954.954,0,0,1,1-.9h2.5a.5.5,0,0,0,.5-.5Z" transform="translate(6 3)" fill="#FFFFFF"/>
                                                </g>
                                              </svg>
                                              
                                        </button>
                                        <button type="button" id="channel_4"
                                            onclick="javascript:toggleChannel('channels', 4);"
                                            class="btn btn-primary btn-icon channel-buton mr-2">
                                            <i class="fab fa-instagram" style="font-size: 1.6rem; padding-right: 0 !important;"></i>
                                        </button>
                                        <button type="button" id="channel_3"
                                            onclick="javascript:toggleChannel('channels', 3);"
                                            class="btn btn-primary btn-icon channel-buton mr-2">
                                            <svg id="_27_Icon_twitter-fill" data-name="27) Icon/twitter-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                                <defs>
                                                  <clipPath id="clip-path">
                                                    <path d="M5.077,16A11.064,11.064,0,0,0,16.515,5.035a7.794,7.794,0,0,0,1.471-2.874.441.441,0,0,0-.617-.512,1.871,1.871,0,0,1-2.161-.376A3.874,3.874,0,0,0,9.634,1.1a4.112,4.112,0,0,0-1.14,3.86C5.143,5.2,2.842,3.609.95,1.432a.429.429,0,0,0-.748.24A9.693,9.693,0,0,0,4.85,11.723,6.7,6.7,0,0,1,.376,14.031a.45.45,0,0,0-.136.841A10.923,10.923,0,0,0,5.077,16" transform="translate(3 4)" fill="#FFFFFF"/>
                                                  </clipPath>
                                                </defs>
                                                <g id="Group_365" data-name="Group 365">
                                                  <path id="_Icon_Сolor-2" data-name="🎨 Icon Сolor" d="M5.077,16A11.064,11.064,0,0,0,16.515,5.035a7.794,7.794,0,0,0,1.471-2.874.441.441,0,0,0-.617-.512,1.871,1.871,0,0,1-2.161-.376A3.874,3.874,0,0,0,9.634,1.1a4.112,4.112,0,0,0-1.14,3.86C5.143,5.2,2.842,3.609.95,1.432a.429.429,0,0,0-.748.24A9.693,9.693,0,0,0,4.85,11.723,6.7,6.7,0,0,1,.376,14.031a.45.45,0,0,0-.136.841A10.923,10.923,0,0,0,5.077,16" transform="translate(3 4)" fill="#FFFFFF"/>
                                                </g>
                                              </svg>
                                              
                                        </button>
                                        <button type="button" id="channel_2"
                                            onclick="javascript:toggleChannel('channels', 2);"
                                            class="btn btn-primary btn-icon channel-buton">
                                            <svg id="_27_Icon_linkedin-fill" data-name="27) Icon/linkedin-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                                <defs>
                                                  <clipPath id="clip-path">
                                                    <path d="M15,18a.9.9,0,0,1-.9-.9V11.335a2.023,2.023,0,0,0-1.747-2.048A1.945,1.945,0,0,0,10.2,11.214V17.1a.9.9,0,0,1-.9.9H7.2a.9.9,0,0,1-.9-.9V11.214a5.851,5.851,0,0,1,11.7,0V17.1a.9.9,0,0,1-.9.9ZM.9,18a.9.9,0,0,1-.9-.9V7.2a.9.9,0,0,1,.9-.9H3.6a.9.9,0,0,1,.9.9v9.9a.9.9,0,0,1-.9.9ZM0,2.25A2.25,2.25,0,1,1,2.25,4.5,2.251,2.251,0,0,1,0,2.25Z" transform="translate(3 3)" fill="#FFFFFF"/>
                                                  </clipPath>
                                                </defs>
                                                <g id="Group_360" data-name="Group 360">
                                                  <path id="_Icon_Сolor-2" data-name="🎨 Icon Сolor" d="M15,18a.9.9,0,0,1-.9-.9V11.335a2.023,2.023,0,0,0-1.747-2.048A1.945,1.945,0,0,0,10.2,11.214V17.1a.9.9,0,0,1-.9.9H7.2a.9.9,0,0,1-.9-.9V11.214a5.851,5.851,0,0,1,11.7,0V17.1a.9.9,0,0,1-.9.9ZM.9,18a.9.9,0,0,1-.9-.9V7.2a.9.9,0,0,1,.9-.9H3.6a.9.9,0,0,1,.9.9v9.9a.9.9,0,0,1-.9.9ZM0,2.25A2.25,2.25,0,1,1,2.25,4.5,2.251,2.251,0,0,1,0,2.25Z" transform="translate(3 3)" fill="#FFFFFF"/>
                                                </g>
                                              </svg>
                                              
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--begin: hannels buttons-->

                <div class="modal" id="kt_modal_nombre" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">New Report</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">Name:</label>
                                        <input type="text" class="form-control" id="dashboaard-name">
                                    </div>
                                    <div class="form-group" style="margin-left: 17px;">
                                        <input class="form-check-input" type="checkbox" name="dashboaard_setreport_default"
                                            id="dashboaard_setreport_default"
                                            onclick="javascript:updateobjetoglobal('default', $('#dashboaard_setreport_default').is(':checked'));">
                                        Set as default
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-basic" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="guardaNombreDashboaard">Save</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal" id="kt_modal_duplicate" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                Duplicate in: <br>
                                <center><a id="select_duplicate_same_campaigns">The same campaign </a></center>
                                <br>or<br>
                                <center>
                                    <select class="selectpicker" id="select_duplicate_campaigns"
                                        data-entidad="duplicate_campaigns" how-tick title="Other campaigns"
                                        data-live-search="true">
                                        @foreach ($campaigns as $campaign)
                                            <option value="{{ $campaign->id }}">{{ $campaign->name }}</option>
                                        @endforeach
                                    </select>
                                </center>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-basic" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal" id="kt_modal_duplicate_customer_campaign" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                Duplicate in: <br>
                                <center><a id="select_duplicate_same_customer">The same campaign </a></center>
                                <br>or<br>
                                <center>
                                    <select class="selectpicker" id="select_duplicate_customer"
                                        data-entidad="duplicate_customer" how-tick title="Other customer"
                                        data-live-search="true">
                                        @foreach ($customers as $customer)
                                            <option value="{{ $customer->public_id }}">{{ $customer->name }}</option>
                                        @endforeach
                                    </select>
                                </center>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-basic" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end: Dashdoarb -->
            </div>
        </div>
    </div>


    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17">
                        <path  d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"/>
                    </svg>
                </span>
                <h3 class="kt-portlet__head-title">
                    {{ $account['title'] }}
                </h3>

                <div class="btn-group alineacion2 filterDateHometable" data-toggle="buttons">
                    <div class="kt-form__control">
                        <select id="periodo_hometable" aria-invalid="false" placeholder="Periodo"
                            name="periodo_hometable">
                            <option value="30">Last 30 days</option>
                            <option value="7">Last 7 days</option>
                            <option value="2">Last 3 days</option>
                            <option value="0">Today</option>
                            <option value="1">Yesterday</option>
                            <option value="29">This Month</option>
                            <option value="31">Last Month</option>
                            <option value="364" selected>This Year</option>
                            <option value="365">Last Year</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <button id="duplicate_button" type="button"
                            class="btn btn-info"
                            style="float: right; display:none;"> Duplicate
                        </button>
                        <button id="view_last_button" type="button"
                            class="btn btn-info"
                            style="float: right; display:none;"> View last duplicate
                        </button>
                        <div class="dropdown dropdown-inline"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-checkable withcampaigns"
                id="dashboard_table" data-page-length='10'>
                <thead>
                    <tr>
                        <th>Actions</th>
                        @foreach ($account['columns'] as $column)
                            <th>{{ $column }}</th>
                        @endforeach
                    </tr>
                </thead>
            </table>
            <!--end: Datatable -->
        </div>
    </div>

@endsection

@include('template.dashmodalcustomer')

@include('template.dashModalCampaign')

@include('template.dashModalCampaignPlatform')

@include('template.dashModalAtom')

@include('template.dashModalCreativity')


@section('customscript')
    <!--begin::Page Vendors(used by this page) -->
    <script src="../assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
    <!-- Con este script es que se hace la carga de la data-->
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
        rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js">
    </script>
    <script src="../assets/app/custom/general/crud/datatables/extensions/scroller.js" type="text/javascript"></script>
    <script src="/js/apexcharts.js" type="text/javascript"></script>
    <script src="/js/reporting.js" type="text/javascript"></script>
    <script src="/assets/app/custom/general/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <link href="../assets/app/custom/wizard/wizard-v1.default.css" rel="stylesheet" type="text/css" />


    <script src="/js/app_constans.js" type="text/javascript"></script>

    <style>
        .fab {
            font-size: 16px;
        }

        .fabBig {
            font-size: 32px;
        }

        thead {
            text-transform: capitalize;
        }

        .chosen-container {
            width: 100% !important;
        }


        .SubTableDuplicated{
            background-color: #00000021;
            padding: 25px;
        }

    </style>

    <script src="/js/customCampaign.js" type="text/javascript"></script>
    <script src="/js/chozen.jquery.js" type="text/javascript"></script>
    <script src="/js/dayScheduler.js" type="text/javascript"></script>
    <script src="/js/modalsDashboard.js" type="text/javascript"></script>
    <script src="/js/bootstrap-tagsinput.min.js" type="text/javascript"></script>
    <script src="../assets/app/custom/general/crud/forms/widgets/form-repeater.js" type="text/javascript"></script>
    <script src="../assets/app/custom/general/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="../assets/vendors/custom/jstree/jstree.bundle.js" type="text/javascript"></script>
    <script src="../assets/app/custom/general/components/extended/treeview.js" type="text/javascript"></script>
    <script src="../assets/app/custom/general/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/hmac-sha1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/hmac-sha256.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/enc-base64.min.js"></script>
    <script src="../js/oauth-1.0a.js"></script>

    <script type="text/javascript">
        var stepEditCampaign = 0;

        function toggleStep(id) {

            $('#step_1').hide();
            $('#step_2').hide();
            $('#step_3').hide();
            $('#step_4').hide();

            $('#step_' + id).show();


        }


        function resizeInput() {

            var valueLength = $(this).prop('value').length;

            // Para que no arroje error si el input se vacía
            if (valueLength > 0) {

                $(this).prop('size', valueLength);
            }
        }

        $('input[type="text"]').on('keyup', resizeInput).each(resizeInput);

        var elemento = "";
        var table1 = "";
        var table2 = "";
        var table3 = "";
        var table4 = "";
        var table5 = "";

        let acces_token = "{{ $token_facebook }}";


        // Load tabla customers
        // OK
        var table1 = $('#dashboard_table').DataTable({
            //dom: 'Bfrtip',
            //select: false,
            processing: true,
            serverSide: true,
            autoWidth: false,
            aLengthMenu: [
                [5, 10, 25, 50],
                [5, 10, 25, 50]
            ],
            pageLength: 25,
            rowId: 'id',
            ajax: {
                url: "{{ $account['api'] }}",
                data: function(d) {
                    d.period_filter = $('#periodo_hometable').val();
                }
            },
            oLanguage: {
                //sProcessing: "<div id='dashboard_table_processing' class='dataTables_processing'>Processing...</div>"
            },
            createdRow: function(row, data, dataIndex) {
                $(row).addClass('account');
            },
            columns: [{
                    "className": 'details-control',
                    "width": "250px",
                    "orderable": false,
                    "data": "id",
                    "scrollX": true,
                    "render": function(data, type, row, meta) {

                        var display_status = '';

                        if (row.isdefault == 'Y') {
                            display_status = ' checked';
                        }

                        var botonera =
                            '<span class="switch switch-sm"><label><input class="switch_checkbox_customers" data-type="customer" data-pk="' +
                            data + '" type="checkbox" ' + display_status +
                            ' name="select"><span></span></label></span>&ensp;';
                        botonera = botonera +
                                '<svg class="editCustomer" onclick="javascript:editModalCustomer(\'' + data + '\');"  data-itemid="' + data + '" title="Edit Account" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"/> </svg>&ensp;';

                        botonera = botonera +
							'<svg class="list-campaigns" data-itemid="' + data + '" title="List campaigns" id="_27_Icon_volume-up" data-name="27) Icon/volume-up" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path d="M13,18a.989.989,0,0,1-.564-.175l-6.413-4.4H1a1,1,0,0,1-1-1V5.572a1,1,0,0,1,1-1H6.023l6.413-4.4A.988.988,0,0,1,13,0a1,1,0,0,1,1,1V17a1,1,0,0,1-1,1ZM2,6.572v4.857H6.333A1.013,1.013,0,0,1,6.9,11.6L12,15.1V2.9L6.9,6.4a1.013,1.013,0,0,1-.566.175ZM17.5,16a1,1,0,0,1-.637-1.771A6.789,6.789,0,0,0,19.5,9a6.793,6.793,0,0,0-2.638-5.23A1,1,0,0,1,17.5,2a1,1,0,0,1,.637.23A8.754,8.754,0,0,1,21.5,9a8.753,8.753,0,0,1-3.362,6.77A1,1,0,0,1,17.5,16ZM16,13a1,1,0,0,1-.779-1.627,4.014,4.014,0,0,0,0-4.746A1,1,0,0,1,16,5a1,1,0,0,1,.779.373,5.972,5.972,0,0,1,0,7.254A1,1,0,0,1,16,13Z" transform="translate(1 3)" fill="#222b45"/> </svg>&ensp;';
                            // '<svg class="list-campaigns" data-itemid="' + data + '" title="List campaigns" xmlns="http://www.w3.org/2000/svg" width="14" height="18.001" viewBox="0 0 14 18.001"> <path id="Path" d="M14,1V17a1,1,0,0,1-1.565.825l-6.412-4.4H1a1,1,0,0,1-1-1V5.572a1,1,0,0,1,1-1H6.023l6.412-4.4A1,1,0,0,1,14,1Z" fill="#222b45"/> </svg>&ensp;';
                            
                        botonera = botonera +
							'<svg class="list-customer-by-platform" data-itemid="' + data + '" id="_27_Icon_layers" data-name="27) Icon/layers" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M9,18a1.009,1.009,0,0,1-.5-.132l-8-4.594a1,1,0,0,1-.5-.926.992.992,0,0,1,.606-.86l2.152-.916L.5,9.274A1,1,0,0,1,.608,7.487l2.152-.916L.5,5.274A1,1,0,0,1,.608,3.488l8-3.407a.994.994,0,0,1,.783,0l8,3.407A1,1,0,0,1,17.5,5.274l-2.259,1.3,2.152.916A1,1,0,0,1,17.5,9.274l-2.258,1.3,2.152.917a1,1,0,0,1,.106,1.786l-8,4.594A1.007,1.007,0,0,1,9,18ZM4.934,11.819l-1.694.722L9,15.847l5.76-3.307-1.694-.723L9.5,13.867a1,1,0,0,1-1,0L4.934,11.819Zm0-4L3.24,8.54,9,11.848,14.76,8.54l-1.694-.722L9.5,9.868a1,1,0,0,1-1,0L4.934,7.818ZM9,2.087,3.24,4.541,9,7.847l5.76-3.307L9,2.087Z" transform="translate(3 3)" fill="#222b45"/> </svg>&ensp;';
                            // '<svg  title="List by platform" xmlns="http://www.w3.org/2000/svg" width="19" height="18" viewBox="0 0 19 18"> <path id="Path" d="M19,8.348a1.006,1.006,0,0,0-.641-.861l-2.272-.916,2.385-1.3A.987.987,0,0,0,19,4.348a1.007,1.007,0,0,0-.641-.861L9.913.08a1.1,1.1,0,0,0-.827,0L.642,3.487A1,1,0,0,0,0,4.348a.985.985,0,0,0,.528.926l2.383,1.3L.642,7.487A1,1,0,0,0,0,8.348a.985.985,0,0,0,.528.926l2.383,1.3-2.272.916a1,1,0,0,0-.64.861.985.985,0,0,0,.528.926l8.444,4.593a1.105,1.105,0,0,0,1.051,0l8.444-4.593A.987.987,0,0,0,19,12.348a1.008,1.008,0,0,0-.64-.861l-2.272-.917,2.383-1.3A.987.987,0,0,0,19,8.348Z" fill="#222b45"/> </svg>&ensp;';
                            
                        botonera = botonera +
                            '<svg class="flaticon2-line-chart"  data-itementity="customer" data-itemid="' + data + '" title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"/> </svg>&ensp;';
                        
                        botonera = botonera +
                            '<svg class="list-accounts-atom" data-itemid="' + data + '"  title="Show Atomo" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"> <g id="atom_icon" transform="translate(0)"> <path id="Shape" d="M0,3A3,3,0,1,1,3,6,3,3,0,0,1,0,3Z" transform="translate(7 7)"/> <path id="Shape-2" data-name="Shape" d="M10,18.057A11.814,11.814,0,0,1,4.211,20a4.218,4.218,0,0,1-3.069-1.141c-1.32-1.319-1.5-3.512-.513-6.172A16.214,16.214,0,0,1,1.944,10,16.241,16.241,0,0,1,.629,7.314c-.989-2.661-.807-4.853.513-6.173A4.222,4.222,0,0,1,4.211,0,11.814,11.814,0,0,1,10,1.943,11.814,11.814,0,0,1,15.789,0a4.224,4.224,0,0,1,3.07,1.141c1.319,1.32,1.5,3.512.512,6.173A16.241,16.241,0,0,1,18.056,10a16.214,16.214,0,0,1,1.315,2.687c.989,2.66.807,4.852-.512,6.172A4.22,4.22,0,0,1,15.789,20,11.814,11.814,0,0,1,10,18.057Z"/> </g> </svg>&ensp;';
                            
                        botonera = botonera +
                            '<svg class="list-creativity" data-itemid="' +
                            data + '"  title="Show Creativities" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"> <path d="M15,18H3a3,3,0,0,1-3-3V3A3,3,0,0,1,3,0H15a3,3,0,0,1,3,3V15A3,3,0,0,1,15,18Zm-3.966-8a.73.73,0,0,0-.468.156L3.561,16H15a1,1,0,0,0,1-1V13.994l-4.5-3.84A.72.72,0,0,0,11.034,10ZM3,2A1,1,0,0,0,2,3V14.7L9.277,8.626a2.778,2.778,0,0,1,3.52.006L16,11.364V3a1,1,0,0,0-1-1ZM5,7A1.5,1.5,0,1,1,6.5,5.5,1.5,1.5,0,0,1,5,7Z" fill="#222b45"/> </svg>';

                        return botonera;
                    },
                    "defaultContent": ''
                },

                @foreach ($account['columns'] as $column) {
                    data: '{{ $column }}',
                    orderable: false,
                    @if ($column == 'id')
                        visible: false,
                    @else
                        visible: true,
                    @endif
                    @if ($column != 'Actions')
                        responsivePriority: -1,
                    @endif
                    },
                @endforeach
            ]
        });

        // load all campaigns by customer
        $('#dashboard_table tbody').on('click', '.list-campaigns', function() {

            console.log("LIST CAMPAIGNS 9990", $(this));

            var tr = $(this).closest('tr');




            var row = table1.row(tr);
            var id_tabla = row.data().id;
            var visible = false;

            if ($("#subtabla0_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
            }

            row.child('<table id="subtabla0_' + id_tabla +
                '" class="display" style="width:100%; background-color: rgb(0 0 0 / 0%);">' +
                '<thead>' + '<tr>' +
                '<th>Actions</th>' +
                '<th>Campaign Name</th>' +
                '<th>Budget</th>' +
                '<th>Spend</th>' +
                '<th>Impressions</th>' +
                '<th>Click</th>' +
                '<th>Ctr</th>' +
                '<th>Cpc</th>' +
                '<th>Conversion</th>' +
                '<th>Cr</th>' +
                '<th>Cpa</th>' +
                '<th>Cpm</th>' +
                '</tr>' +
                '</thead>' +
                '</table>');


            row.child.show();

            tr.addClass('show');

            $('td[colspan="11"]').addClass('SubTableDuplicated');

            var ruta = '/api/dashboard/customer/campaigns/' + id_tabla;

            table2 = $('#subtabla0_' + id_tabla).DataTable({
                autoWidth: false,
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",
                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val()
                    }
                },
                "columns": [{
                        "className": 'details-control',
                        "width": "250px",
                        "orderable": false,
                        "data": "public_id",
                        "render": function(data, type, row, meta) {

                            //console.log('load all campaigns by customer ',data)
                            console.log('load all campaigns by customer ', row)

                            var check = '';
                            if (row.status == 'ACTIVE') {
                                check = 'checked="checked"';
                            }
                            var display_status = '';
                            if (row.isdefault == 'Y') {
                                display_status = 'disabled';
                            }

                            var botonera =
                                '<label class="checkbox" style="float:left; margin: 3px 0;">';

                            botonera = botonera +
                                '<input class="duplicate" data-type="campaigns" data-campaign="' +
                                data + '" type="checkbox" data-itemid="' + data +
                                '" name="duplicate" /><span></span></label>&ensp;';
                            botonera = botonera +
                                '<span class="switch switch-sm"><label><input class="switch_checkbox_campaign" data-type="campaigns" type="checkbox" ' +
                                check + ' data-itemid="' + data + '" name="select" data-pk="' +
                                data + '" ' + display_status + ' ><span></span></label></span>&ensp;';


                            botonera = botonera +
								'<svg class="editCampaign" onClick="javascript:editCampaign(\'' + data + '\');" data-itemid="' + data + '" title="Edit Campaign" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"></path> </svg>&ensp;';	

                            botonera = botonera + 
							'<svg class="list-cus-campaigns-plat" data-itemid="' + data + '" title="List campaigns" class="list-campaigns" data-itemid="06e32590-c2e0-11ea-86d6-de2e501f7018" title="List campaigns"  id="_27_Icon_volume-up" data-name="27) Icon/volume-up" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path d="M13,18a.989.989,0,0,1-.564-.175l-6.413-4.4H1a1,1,0,0,1-1-1V5.572a1,1,0,0,1,1-1H6.023l6.413-4.4A.988.988,0,0,1,13,0a1,1,0,0,1,1,1V17a1,1,0,0,1-1,1ZM2,6.572v4.857H6.333A1.013,1.013,0,0,1,6.9,11.6L12,15.1V2.9L6.9,6.4a1.013,1.013,0,0,1-.566.175ZM17.5,16a1,1,0,0,1-.637-1.771A6.789,6.789,0,0,0,19.5,9a6.793,6.793,0,0,0-2.638-5.23A1,1,0,0,1,17.5,2a1,1,0,0,1,.637.23A8.754,8.754,0,0,1,21.5,9a8.753,8.753,0,0,1-3.362,6.77A1,1,0,0,1,17.5,16ZM16,13a1,1,0,0,1-.779-1.627,4.014,4.014,0,0,0,0-4.746A1,1,0,0,1,16,5a1,1,0,0,1,.779.373,5.972,5.972,0,0,1,0,7.254A1,1,0,0,1,16,13Z" transform="translate(1 3)" fill="#222b45"/> </svg>&ensp;';
							// '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="18.001" viewBox="0 0 14 18.001"> <path id="Path" d="M14,1V17a1,1,0,0,1-1.565.825l-6.412-4.4H1a1,1,0,0,1-1-1V5.572a1,1,0,0,1,1-1H6.023l6.412-4.4A1,1,0,0,1,14,1Z" fill="#222b45"></path> </svg>&ensp;';
							
                            botonera = botonera +
								'<svg class="list-platform" data-itemid="' + data + '" title="Show by Platform" id="_27_Icon_layers" data-name="27) Icon/layers" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M9,18a1.009,1.009,0,0,1-.5-.132l-8-4.594a1,1,0,0,1-.5-.926.992.992,0,0,1,.606-.86l2.152-.916L.5,9.274A1,1,0,0,1,.608,7.487l2.152-.916L.5,5.274A1,1,0,0,1,.608,3.488l8-3.407a.994.994,0,0,1,.783,0l8,3.407A1,1,0,0,1,17.5,5.274l-2.259,1.3,2.152.916A1,1,0,0,1,17.5,9.274l-2.258,1.3,2.152.917a1,1,0,0,1,.106,1.786l-8,4.594A1.007,1.007,0,0,1,9,18ZM4.934,11.819l-1.694.722L9,15.847l5.76-3.307-1.694-.723L9.5,13.867a1,1,0,0,1-1,0L4.934,11.819Zm0-4L3.24,8.54,9,11.848,14.76,8.54l-1.694-.722L9.5,9.868a1,1,0,0,1-1,0L4.934,7.818ZM9,2.087,3.24,4.541,9,7.847l5.76-3.307L9,2.087Z" transform="translate(3 3)" fill="#222b45"/> </svg>&ensp;';
                                // '<svg class="list-platform" data-itemid="' + data + '" title="Show by Platform" xmlns="http://www.w3.org/2000/svg" width="19" height="18" viewBox="0 0 19 18"> <path id="Path" d="M19,8.348a1.006,1.006,0,0,0-.641-.861l-2.272-.916,2.385-1.3A.987.987,0,0,0,19,4.348a1.007,1.007,0,0,0-.641-.861L9.913.08a1.1,1.1,0,0,0-.827,0L.642,3.487A1,1,0,0,0,0,4.348a.985.985,0,0,0,.528.926l2.383,1.3L.642,7.487A1,1,0,0,0,0,8.348a.985.985,0,0,0,.528.926l2.383,1.3-2.272.916a1,1,0,0,0-.64.861.985.985,0,0,0,.528.926l8.444,4.593a1.105,1.105,0,0,0,1.051,0l8.444-4.593A.987.987,0,0,0,19,12.348a1.008,1.008,0,0,0-.64-.861l-2.272-.917,2.383-1.3A.987.987,0,0,0,19,8.348Z" fill="#222b45"/> </svg>&ensp;';
                                
                            @feature('hometable-segmentation')
                            botonera = botonera +
                                '<i class="list-segmentations" data-itemid="' +
                                data + '" title="Show segmentations"></i>';
                            @endfeature
                            botonera = botonera +
                                '<svg class="list-atom" data-itemid="' + data + '"  title="Show Atomo" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"> <path id="Shape" d="M10,18.057A11.814,11.814,0,0,1,4.211,20a4.218,4.218,0,0,1-3.069-1.141c-1.32-1.319-1.5-3.512-.513-6.172A16.214,16.214,0,0,1,1.944,10,16.241,16.241,0,0,1,.629,7.314c-.989-2.661-.807-4.853.513-6.173A4.222,4.222,0,0,1,4.211,0,11.814,11.814,0,0,1,10,1.943,11.814,11.814,0,0,1,15.789,0a4.224,4.224,0,0,1,3.07,1.141c1.319,1.32,1.5,3.512.512,6.173A16.241,16.241,0,0,1,18.056,10a16.214,16.214,0,0,1,1.315,2.687c.989,2.66.807,4.852-.512,6.172A4.22,4.22,0,0,1,15.789,20,11.814,11.814,0,0,1,10,18.057Z" transform="translate(0)"/> </svg>&ensp;';
                                
                            botonera = botonera +
								'<svg class="list-creativity-by-campaign" data-itemid="' + data + '" title="Show Creativities" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"> <path d="M15,18H3a3,3,0,0,1-3-3V3A3,3,0,0,1,3,0H15a3,3,0,0,1,3,3V15A3,3,0,0,1,15,18Zm-3.966-8a.73.73,0,0,0-.468.156L3.561,16H15a1,1,0,0,0,1-1V13.994l-4.5-3.84A.72.72,0,0,0,11.034,10ZM3,2A1,1,0,0,0,2,3V14.7L9.277,8.626a2.778,2.778,0,0,1,3.52.006L16,11.364V3a1,1,0,0,0-1-1ZM5,7A1.5,1.5,0,1,1,6.5,5.5,1.5,1.5,0,0,1,5,7Z" fill="#222b45"></path> </svg>&ensp;';
                                
                            botonera = botonera +
                                '<svg class="flaticon2-line-chart" data-itemid="' + data + '" data-itementity="campaign" title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"/> </svg>';


                            return botonera;

                        },
                        "defaultContent": ''
                    },
                    {
                        "data": "name",
                        "render": function(data, type, row, meta) {
                            return ' <a href="#" class="name editable editable-click" data-type="text" data-pk="' +
                                row.public_id + '" data-id="' + row.public_id +
                                '" data-title="Enter name">' + data + '</a>';
                        }
                    },
                    {
                        "data": "budget",
                        "render": function(data, type, row, meta) {
                            return '<a href="#" class="budget editable editable-click" data-type="text" data-pk="' +
                                row.public_id + '" data-id="' + row.public_id +
                                '" data-title="Enter budget">' + data + '</a>';
                        }
                    },
                    {
                        "data": "spent"
                    },
                    {
                        "data": "impressions"
                    },
                    {
                        "data": "clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "conversions"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }

                ]
            });

            table2.on('draw.dt', function() {

                var type = $(this).attr('data-type');
                var id = $(this).attr('data-pk');
                var field = $(this).attr('data-name');
                var value = $(this).val();

                $('.budget').editable({
                    type: 'text',
                    url: '/api/dashboard/entity/update',

                    params: function(params) {
                        params.field = 'budget';
                        params.entity = 'campaigns';
                        params.id = params.pk;
                        console.log(params);
                        return params;
                    },

                    validate: function(value) {

                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                        if ($.isNumeric(value) == '') {
                            return 'This Number is required';
                        }
                    },
                    success: function(response) {

                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.name').editable({
                    type: 'text',
                    url: '/api/dashboard/entity/update',
                    params: function(params) {
                        params.field = 'name';
                        params.entity = 'campaigns';
                        params.id = params.pk;
                        console.log(params);
                        return params;
                    },
                    validate: function(value) {
                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                    },
                    success: function(response) {
                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.duplicate').change(function() {
                    var flag = true;
                    $(".duplicate").each(function(index) {
                        if ($(this).is(":checked")) {
                            $("#duplicate_button").show();
                            flag = false;
                        }
                    });

                    if (flag) {
                        $("#duplicate_button").hide();
                    }
                });

            });


        });

        // load campaigns_platforms by campaigns
        $('#dashboard_table tbody').on('click', '.list-cus-campaigns-plat', function() {
           
            //console.log("load campaigns_platforms by campaigns 888909", $(this)); //data-itemid
            //console.log("load campaigns_platforms by campaigns 888909", $(this).data-itemid); //
            //console.log("load campaigns_platforms by campaigns 888909", $(this).attr('data-itemid')); //
            
            var tr = $(this).closest('tr');
            
            var row = table2.row(tr);
            
            //console.log(row)
            
            var id_tabla = $(this).attr('data-itemid');
            var visible = false;

            if ($("#subtabla0_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
            }

            row.child('<table id="subtabla0_' + id_tabla +
                '" class="display" style="width:100%; background-color: rgb(0 0 0 / 0%);">' + '<thead>' + '<tr>' +
                '<th>Actions</th>' + '<th>Campaign</th>'+ '<th>Platform</th>' + '<th>Budget</th>' + '<th>Spend</th>' +
                '<th>Impressions</th>' + '<th>Click</th>' + '<th>Ctr</th>' + '<th>Cpc</th>' +
                '<th>Conversion</th>' + '<th>Cr</th>' + '<th>Cpa</th>' + '<th>Cpm</th>' + '</tr>' + '</thead>' +
                '</table>');

            row.child.show();

            tr.addClass('show');
            $('td[colspan="11"]').addClass('SubTableDuplicated');

            var ruta = '/api/dashboard/customer/campaigns/byPlatformList/' + id_tabla;

            table2 = $('#subtabla0_' + id_tabla).DataTable({
                autoWidth: false,
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",

                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val();
                    }
                },

                "columns": [{
                        "className": 'details-control',
                        "width": "250px",
                        "orderable": false,
                        "data": "public_id",
                        "render": function(data, type, row, meta) {

                            console.log(data)
                            console.log(row)

                            var check = '';
                            if (row.status > 0) {
                                check = 'checked="checked"';
                            }


                            var display_status = '{{asset("assets/images/placeholder.png")}}';
                            if (row.isdefault == 'Y') {
                                display_status = 'disabled';
                            }

                            var botonera =
                                '<label class="checkbox" style="float:left; margin: 3px 0;">';
                            botonera = botonera +
                                '<input class="duplicate" data-type="campaigns" data-customer="' +
                                row.id + '" type="checkbox" data-itemid="' + row.id +
                                '" name="duplicate" /><span></span></label>&ensp;';
                            botonera = botonera +
                                '<span class="switch switch-sm"><label><input class="switch_checkbox_campaign" data-type="campaigns_platform" type="checkbox" ' +
                                check + ' data-platform="' + row.plataforma + '" data-itemid="' +
                                row.id + '" data-pk="' + row.id +
                                '" name="select"  ><span></span></label></span>&ensp;';
                            //botonera = botonera + '<svg class="kt-font-brand" onclick="javascript:editCampaignPlatform(\'' + row.id + '\');" data-itemid="' + row.id + '" title="Edit Campaign" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"/> </svg>&ensp;';
                            //botonera = botonera + '<i class="kt-font-brand flaticon-tabs  list-platform" data-itemid="' + row.id + '" title="Show by Platform"></i>';

                            @feature('hometable-segmentation')
                            botonera = botonera +
                                // '<i class="kt-font-brand flaticon-customer list-segmentations" data-itemid="' +
                                // row.id + '" title="Show segmentations"></i>&ensp;';
                                '<svg class="kt-font-brand list-segmentations" data-itemid="' + row.id + '" title="Show segmentations" xmlns="http://www.w3.org/2000/svg" width="20" height="18.001" viewBox="0 0 20 18.001"><path id="Exclusion_2" data-name="Exclusion 2" d="M3899-5206a1,1,0,0,1-1-1,5.006,5.006,0,0,0-5-5,5.006,5.006,0,0,0-5,5,1,1,0,0,1-1,1,1,1,0,0,1-1-1,7.007,7.007,0,0,1,7-7,6.955,6.955,0,0,1,4.94,2.045A5.036,5.036,0,0,1,3901-5213a5.006,5.006,0,0,1,5,5,1,1,0,0,1-1,1,1,1,0,0,1-1-1,3,3,0,0,0-3-3,3,3,0,0,0-1.856.649A7.017,7.017,0,0,1,3900-5207,1,1,0,0,1,3899-5206Zm2-8a3,3,0,0,1-3-3,3,3,0,0,1,3-3,3,3,0,0,1,3,3A3,3,0,0,1,3901-5214Zm0-4a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1A1,1,0,0,0,3901-5218Zm-8,2a4,4,0,0,1-4-4,4,4,0,0,1,4-4,4,4,0,0,1,4,4A4,4,0,0,1,3893-5216Zm0-6a2,2,0,0,0-2,2,2,2,0,0,0,2,2,2,2,0,0,0,2-2A2,2,0,0,0,3893-5222Z" transform="translate(-3886 5224)" fill="#222b45"/></svg>&ensp;';
                            @endfeature

                            botonera = botonera +
								'<svg class="list-atom" data-itemid="' + row.id + '" data-platform="' + row.plataforma + '"  title="Show Atomo"  class="list-accounts-atom" data-itemid="06e32590-c2e0-11ea-86d6-de2e501f7018" title="Show Atomo" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"> <g id="atom_icon" transform="translate(0)"> <path id="Shape" d="M0,3A3,3,0,1,1,3,6,3,3,0,0,1,0,3Z" transform="translate(7 7)"></path> <path id="Shape-2" data-name="Shape" d="M10,18.057A11.814,11.814,0,0,1,4.211,20a4.218,4.218,0,0,1-3.069-1.141c-1.32-1.319-1.5-3.512-.513-6.172A16.214,16.214,0,0,1,1.944,10,16.241,16.241,0,0,1,.629,7.314c-.989-2.661-.807-4.853.513-6.173A4.222,4.222,0,0,1,4.211,0,11.814,11.814,0,0,1,10,1.943,11.814,11.814,0,0,1,15.789,0a4.224,4.224,0,0,1,3.07,1.141c1.319,1.32,1.5,3.512.512,6.173A16.241,16.241,0,0,1,18.056,10a16.214,16.214,0,0,1,1.315,2.687c.989,2.66.807,4.852-.512,6.172A4.22,4.22,0,0,1,15.789,20,11.814,11.814,0,0,1,10,18.057Z"></path> </g> </svg>&ensp;';
                                
                            botonera = botonera +
                                '<svg class="list-creativity-by-campaign" data-itemid="' + row.id + '" data-platform="' + row.plataforma + '"  title="Show Creativities" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"> <path d="M15,18H3a3,3,0,0,1-3-3V3A3,3,0,0,1,3,0H15a3,3,0,0,1,3,3V15A3,3,0,0,1,15,18Zm-3.966-8a.73.73,0,0,0-.468.156L3.561,16H15a1,1,0,0,0,1-1V13.994l-4.5-3.84A.72.72,0,0,0,11.034,10ZM3,2A1,1,0,0,0,2,3V14.7L9.277,8.626a2.778,2.778,0,0,1,3.52.006L16,11.364V3a1,1,0,0,0-1-1ZM5,7A1.5,1.5,0,1,1,6.5,5.5,1.5,1.5,0,0,1,5,7Z" fill="#222b45"></path> </svg>&ensp;';
								
                            botonera = botonera +
							    '<svg  class="flaticon2-line-chart" data-itementity="campaign_platform" data-itemid="' + row.id + '" data-platform="' + row.plataforma + '"  title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path  d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"/> </svg>&ensp;';

                            return botonera;

                        },
                        "defaultContent": ''
                    },
                    {
                        "data": "name"
                    },
                     {
                        "data": "plataforma"
                    },
                    {
                        "data": "budget",
                        "render": function(data, type, row, meta) {
                            return data;
                            // draws editable budget
                            //return '<a href="#" class="budget editable editable-click" data-type="text" data-pk="' + row.public_id + '" data-id="' + row.public_id +'" data-title="Enter budget">' + data + '</a>';
                        }
                    },
                    {
                        "data": "spend"
                    },
                    {
                        "data": "impressions"
                    },
                    {
                        "data": "clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "conversions"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }

                ]
            });

            // entities update implemented for campaigns

            table2.on('draw.dt', function() {

                var type = $(this).attr('data-type');
                var id = $(this).attr('data-pk');
                var field = $(this).attr('data-name');
                var value = $(this).val();

                $('.budget').editable({
                    type: 'text',
                    url: '/api/dashboard/entity/update',

                    params: function(params) {
                        params.field = 'budget';
                        params.entity = 'campaigns_platform';
                        params.id = params.pk;
                        //console.log(params);
                        return params;
                    },

                    validate: function(value) {

                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                        if ($.isNumeric(value) == '') {
                            return 'This Number is required';
                        }
                    },
                    success: function(response) {

                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.name').editable({
                    type: 'text',
                    url: '/api/dashboard/entity/update',
                    params: function(params) {
                        params.field = 'name';
                        params.entity = 'campaigns_platform';
                        params.id = params.pk;
                        console.log(params);
                        return params;
                    },
                    validate: function(value) {
                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                    },
                    success: function(response) {
                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.duplicate').change(function() {
                    var flag = true;
                    $(".duplicate").each(function(index) {
                        if ($(this).is(":checked")) {
                            $("#duplicate_button").show();
                            flag = false;
                        }
                    });

                    if (flag) {
                        $("#duplicate_button").hide();
                    }
                });

            });
        });

        // load platformS by customer
        $('#dashboard_table tbody').on('click', '.list-customer-by-platform', function() {
            console.log("LIST customer 9991", $(this));
            var tr = $(this).closest('tr');
            var row = table1.row(tr);
            var id_tabla = row.data().id;
            var visible = false;

            if ($("#subtabla0_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
            }

            row.child('<table id="subtabla0_' + id_tabla +
                '" class="display" style="width:100%; background-color: rgb(0 0 0 / 0%);">' + '<thead>' + '<tr>' +
                '<th>Actions</th>' + '<th>Platform</th>' + '<th>Budget</th>' + '<th>Spend</th>' +
                '<th>Impressions</th>' + '<th>Click</th>' + '<th>Ctr</th>' + '<th>Cpc</th>' +
                '<th>Conversion</th>' + '<th>Cr</th>' + '<th>Cpa</th>' + '<th>Cpm</th>' + '</tr>' + '</thead>' +
                '</table>');
            row.child.show();

            tr.addClass('show');
            $('td[colspan="11"]').addClass('SubTableDuplicated');

            var ruta = '/api/dashboard/customer/campaigns/byPlatform/' + id_tabla;

            table2 = $('#subtabla0_' + id_tabla).DataTable({
                autoWidth: false,
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",

                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val();
                    }
                },

                "columns": [{
                        "className": 'details-control',
                        "width": "250px",
                        "orderable": false,
                        "data": "public_id",
                        "render": function(data, type, row, meta) {

                            console.log(data)
                            console.log(row)

                            var check = '';
                            if (row.status > 0) {
                                check = 'checked="checked"';
                            }


                            var display_status = '';
                            if (row.isdefault == 'Y') {
                                display_status = 'disabled';
                            }

                            var botonera =
                                '<label class="checkbox" style="float:left; margin: 3px 0;">';
                            botonera = botonera +
                                '<input class="duplicate" data-type="campaigns" data-customer="' +
                                row.id + '" type="checkbox" data-itemid="' + row.id +
                                '" name="duplicate" /><span></span></label>&ensp;';
                            botonera = botonera +
                                '<span class="switch switch-sm"><label><input class="switch_checkbox_campaign" data-type="campaigns_platform" type="checkbox" ' +
                                check + ' data-platform="' + row.plataforma + '" data-itemid="' +
                                row.id + '" data-pk="' + row.id +
                                '" name="select"  ><span></span></label></span>&ensp;';
                            // botonera = botonera + '<svg class="kt-font-brand" data-itemid="' + row.id + '" title="Edit Campaign" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"/> </svg>&ensp;';
                            // botonera = botonera + '<i class="kt-font-brand flaticon-tabs  list-platform" data-itemid="' + row.id + '" title="Show by Platform"></i>';

                            @feature('hometable-segmentation')
                            botonera = botonera +
                                // '<i class="kt-font-brand flaticon-customer list-segmentations" data-itemid="' +
                                // row.id + '" title="Show segmentations"></i>';
                                '<svg class="kt-font-brand list-segmentations" data-itemid="' + row.id + '" title="Show segmentations" xmlns="http://www.w3.org/2000/svg" width="20" height="18.001" viewBox="0 0 20 18.001"><path id="Exclusion_2" data-name="Exclusion 2" d="M3899-5206a1,1,0,0,1-1-1,5.006,5.006,0,0,0-5-5,5.006,5.006,0,0,0-5,5,1,1,0,0,1-1,1,1,1,0,0,1-1-1,7.007,7.007,0,0,1,7-7,6.955,6.955,0,0,1,4.94,2.045A5.036,5.036,0,0,1,3901-5213a5.006,5.006,0,0,1,5,5,1,1,0,0,1-1,1,1,1,0,0,1-1-1,3,3,0,0,0-3-3,3,3,0,0,0-1.856.649A7.017,7.017,0,0,1,3900-5207,1,1,0,0,1,3899-5206Zm2-8a3,3,0,0,1-3-3,3,3,0,0,1,3-3,3,3,0,0,1,3,3A3,3,0,0,1,3901-5214Zm0-4a1,1,0,0,0-1,1,1,1,0,0,0,1,1,1,1,0,0,0,1-1A1,1,0,0,0,3901-5218Zm-8,2a4,4,0,0,1-4-4,4,4,0,0,1,4-4,4,4,0,0,1,4,4A4,4,0,0,1,3893-5216Zm0-6a2,2,0,0,0-2,2,2,2,0,0,0,2,2,2,2,0,0,0,2-2A2,2,0,0,0,3893-5222Z" transform="translate(-3886 5224)" fill="#222b45"/></svg>&ensp;';
                            @endfeature

                            botonera = botonera +
								'<svg class="list-atom" data-itemid="' + row.id + '" data-platform="' + row.plataforma + '"  title="Show Atomo" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"> <g id="atom_icon" transform="translate(0)"> <path id="Shape" d="M0,3A3,3,0,1,1,3,6,3,3,0,0,1,0,3Z" transform="translate(7 7)"></path> <path id="Shape-2" data-name="Shape" d="M10,18.057A11.814,11.814,0,0,1,4.211,20a4.218,4.218,0,0,1-3.069-1.141c-1.32-1.319-1.5-3.512-.513-6.172A16.214,16.214,0,0,1,1.944,10,16.241,16.241,0,0,1,.629,7.314c-.989-2.661-.807-4.853.513-6.173A4.222,4.222,0,0,1,4.211,0,11.814,11.814,0,0,1,10,1.943,11.814,11.814,0,0,1,15.789,0a4.224,4.224,0,0,1,3.07,1.141c1.319,1.32,1.5,3.512.512,6.173A16.241,16.241,0,0,1,18.056,10a16.214,16.214,0,0,1,1.315,2.687c.989,2.66.807,4.852-.512,6.172A4.22,4.22,0,0,1,15.789,20,11.814,11.814,0,0,1,10,18.057Z"></path> </g> </svg>&ensp;';
                                
                            botonera = botonera +
								'<svg class="list-creativity-by-campaign" data-itemid="' + row.id + '" data-platform="' + row.plataforma + '"  title="Show Creativities" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"> <path d="M15,18H3a3,3,0,0,1-3-3V3A3,3,0,0,1,3,0H15a3,3,0,0,1,3,3V15A3,3,0,0,1,15,18Zm-3.966-8a.73.73,0,0,0-.468.156L3.561,16H15a1,1,0,0,0,1-1V13.994l-4.5-3.84A.72.72,0,0,0,11.034,10ZM3,2A1,1,0,0,0,2,3V14.7L9.277,8.626a2.778,2.778,0,0,1,3.52.006L16,11.364V3a1,1,0,0,0-1-1ZM5,7A1.5,1.5,0,1,1,6.5,5.5,1.5,1.5,0,0,1,5,7Z" fill="#222b45"></path> </svg>&ensp;';
                                
                            botonera = botonera +
                                '<svg  class="flaticon2-line-chart" data-itementity="customer" data-itemid="' + row.id + '" data-platform="' + row.plataforma + '"  title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path  d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"/> </svg>';
                            
                            return botonera;

                        },
                        "defaultContent": ''
                    },
                    {
                        "data": "plataforma"
                    },
                    {
                        "data": "budget",
                        "render": function(data, type, row, meta) {
                            return data;
                            // draws editable budget
                            //return '<a href="#" class="budget editable editable-click" data-type="text" data-pk="' + row.public_id + '" data-id="' + row.public_id +'" data-title="Enter budget">' + data + '</a>';
                        }
                    },
                    {
                        "data": "spend"
                    },
                    {
                        "data": "impressions"
                    },
                    {
                        "data": "clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "conversions"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }

                ]
            });

            // entities update implemented for campaigns

            table2.on('draw.dt', function() {

                var type = $(this).attr('data-type');
                var id = $(this).attr('data-pk');
                var field = $(this).attr('data-name');
                var value = $(this).val();

                $('.budget').editable({
                    type: 'text',
                    url: '/api/dashboard/entity/update',

                    params: function(params) {
                        params.field = 'budget';
                        params.entity = 'campaigns_platform';
                        params.id = params.pk;
                        console.log(params);
                        return params;
                    },

                    validate: function(value) {

                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                        if ($.isNumeric(value) == '') {
                            return 'This Number is required';
                        }
                    },
                    success: function(response) {

                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.name').editable({
                    type: 'text',
                    url: '/api/dashboard/entity/update',
                    params: function(params) {
                        params.field = 'name';
                        params.entity = 'campaigns_platform';
                        params.id = params.pk;
                        console.log(params);
                        return params;
                    },
                    validate: function(value) {
                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                    },
                    success: function(response) {
                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.duplicate').change(function() {
                    var flag = true;
                    $(".duplicate").each(function(index) {
                        if ($(this).is(":checked")) {
                            $("#duplicate_button").show();
                            flag = false;
                        }
                    });

                    if (flag) {
                        $("#duplicate_button").hide();
                    }
                });

            });
        });

        $('#dashboard_table tbody').on('change', '.switch_checkbox_campaign', function() {

            console.log("switch_checkbox_campaign 9992", $(this));
            console.log("this", $(this));
            console.log("this", $(this));

            var type = $(this).attr('data-type');
            var id = $(this).attr('data-pk');
            var platform = $(this).attr('data-platform');
            var check = false;
            var field = 'status';

            if ($(this).is(":checked")) {
                check = true;
            }

            $.ajax({
                url: "/api/dashboard/entity/update",
                type: "post",
                dataType: "json",
                data: {
                    'id': id,
                    'entity': type,
                    'check': check,
                    'field': field,
                    'platform': platform
                }
            }).done(function(res) {
                if (res)
                    toastr.success("Successful Changes ");
                else
                    toastr.error("Something Went Wrong ");
            });
        });

        $('#dashboard_table tbody').on('change', '.switch_checkbox_customers', function() {

            var type = $(this).attr('data-type');
            var id = $(this).attr('data-pk');
            //var platform = $(this).attr('data-platform');
            var check = false;
            var field = 'status';

            if ($(this).is(":checked")) {
                check = true;
            }

            $.ajax({
                url: "/api/dashboard/entity/update",
                type: "post",
                dataType: "json",
                data: {
                    'id': id,
                    'entity': type,
                    'check': check,
                    'field': field,
                    // 'platform':platform
                }
            }).done(function(res) {
                if (res)
                    toastr.success("Successful Changes ");
                else
                    toastr.error("Something Went Wrong ");
            });

        });

        //List accounts atom OK
        $('#dashboard_table tbody').on('click', '.list-accounts-atom', function() {
            console.log("accounts atoms 9993", $(this));
            var tr = $(this).closest('tr');
            var row = table1.row(tr);
            var id_tabla = $(this).attr('data-itemid');

            var visible = false;
            if ($("#subtabla0_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
                //console.log('Atomo: no visible');
            }
            row.child('<table id="subtabla0_' + id_tabla +
                '" class="display" style="width:100%;">' +
                '<thead>' +
                '<tr>' +
                '<th>Actions</th>' +
                '<th>Accounts Atom Name</th>' +
                '<th>Budget</th>' +
                '<th>Spend</th>' +
                // '<th>Goals</th>' +
                '<th>Impressions</th>' +
                '<th>Click</th>' +
                '<th>Ctr</th>' +
                '<th>Cpc</th>' +
                '<th>Conversion</th>' +
                '<th>Cr</th>' +
                '<th>Cpa</th>' +
                '<th>Cpm</th>' +
                '</tr>' +
                '</thead>' +
                '</table>');


            row.child.show();
            tr.addClass('show');
            $('td[colspan="11"]').addClass('SubTableDuplicated');

            var ruta = '/api/dashboard/customer/atoms/' + id_tabla;
            //console.log(ruta);
            table2 = $('#subtabla0_' + id_tabla).DataTable({
                // ajax: ruta,
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",
                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val();
                    }
                },
                autoWidth: false,
                "columns": [{
                        "className": 'details-control',
                        "width": "250px",
                        "orderable": false,
                        "data": 'public_id',
                        "defaultContent": '',
                        "render": function(data, type, row, meta) {

                            console.log('accont atom name', row);
                            //console.log('accont atom name', RRSS_ICONS);

                            var check = '';
                            if (row.status == 'ACTIVE') {
                                check = 'checked="checked"';
                            }

                            var botonera =
                                '<label class="checkbox" style="float:left; margin: 3px 0;">';
                            botonera = botonera +
                                '<input class="duplicate" data-type="atomo" data-campaign="' +
                                id_tabla + '" type="checkbox" data-itemid="' + data +
                                '" name="duplicate" /><span></span></label>&ensp;';
                            botonera = botonera +
                                '<span class="switch switch-sm"><label><input class="switch_checkbox_atom" data-type="atom" type="checkbox" ' +
                                check + ' data-itemid="' + data + '" data-table="' + id_tabla +
                                '" name="select"><span></span></label></span>&ensp;';
                            botonera = botonera +
								'<svg class="flaticon-edit" onclick="javascript:editModalAtom(\'' + data + '\');" data-itemid="' + data + '" title="Edit Atom" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"></path> </svg>&ensp;';
                                
                            botonera = botonera +
								'<svg class="list-creativity-from-atom-father" data-itemid="' + data + '"  title="Show Creativities" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"> <path d="M15,18H3a3,3,0,0,1-3-3V3A3,3,0,0,1,3,0H15a3,3,0,0,1,3,3V15A3,3,0,0,1,15,18Zm-3.966-8a.73.73,0,0,0-.468.156L3.561,16H15a1,1,0,0,0,1-1V13.994l-4.5-3.84A.72.72,0,0,0,11.034,10ZM3,2A1,1,0,0,0,2,3V14.7L9.277,8.626a2.778,2.778,0,0,1,3.52.006L16,11.364V3a1,1,0,0,0-1-1ZM5,7A1.5,1.5,0,1,1,6.5,5.5,1.5,1.5,0,0,1,5,7Z" fill="#222b45"></path> </svg>&ensp;';
                                
                            botonera = botonera +
                                '<svg class="flaticon2-line-chart" data-itementity="atom" data-itemid="' +data + '"  title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17">    <path  d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"/> </svg>';
                                



                            return botonera;
                        }
                    },
                    {
                        "data": "name",
                        "render": function(data, type, row, meta) {
                            return RRSS_ICONS[row.platform] +
                                ' <a href="#" class="name editable editable-click" data-type="text" data-pk="' +
                                row.public_id + '" data-name="atom" data-title="Enter name">' +
                                data + '</a>';
                        }

                    },
                    {
                        "data": "budget",
                        "render": function(data, type, row, meta) {
                            if (data == null) {
                                data = 0;
                            } else {
                                data = parseFloat(data).toFixed(2);
                            }
                            return ' <a href="#" class="budget editable editable-click" data-type="text" data-pk="' +
                                row.public_id + '" data-name="atom" data-title="Enter budget">' +
                                data + '</a>';
                        }
                    },

                    {
                        "data": "spent"
                    },
                    {
                        "data": "impressions"
                    },
                    {
                        "data": "clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "conversions"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }
                ]
            });

            // actions atom editable entity 
            table2.on('draw.dt', function() {

                $('.budget').editable({
                    type: 'text',
                    //url: '/api/dashboard/change_budget',
                    url: '/api/dashboard/entity/update',
                    params: function(params) {
                        params.field = 'budget';
                        params.entity = 'atom';
                        params.id = params.pk;
                        console.log(params);
                        return params;
                    },
                    validate: function(value) {

                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                        if ($.isNumeric(value) == '') {
                            return 'This Number is required';
                        }
                    },
                    success: function(response) {

                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.name').editable({
                    type: 'text',
                    url: '/api/dashboard/entity/update',
                    params: function(params) {
                        params.field = 'name';
                        params.entity = 'atom';
                        params.id = params.pk;
                        console.log(params);
                        return params;
                    },
                    validate: function(value) {

                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                    },
                    success: function(response) {

                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.duplicate').change(function() {
                    var flag = true;
                    $(".duplicate").each(function(index) {
                        if ($(this).is(":checked")) {
                            $("#duplicate_button").show();
                            flag = false;
                        }
                    });

                    if (flag) {
                        $("#duplicate_button").hide();
                    }
                });

                $('.switch_checkbox_atom').change(function() {
                    var type = $(this).attr('data-type');
                    var id = $(this).attr('data-itemid');
                    var check = false;
                    var field = 'status';


                    if ($(this).is(":checked")) {
                        check = true;

                    }

                    $.ajax({
                        url: "/api/dashboard/entity/update",
                        type: "post",
                        dataType: "json",
                        data: {
                            'id': id,
                            'entity': type,
                            'check': check,
                            'field': field
                        }
                    }).done(function(res) {
                        if (res)
                            toastr.success("Successful Changes ");
                        else
                            toastr.error("Something Went Wrong ");
                    });
                });

            });

        });

        //List platform OK
        $('#dashboard_table tbody').on('click', '.list-platform', function() {

            var tr = $(this).closest('tr');
            var row = table2.row(tr);
            //var id_tabla = row.data().id;
            var visible = false;
            var id_tabla = $(this).attr('data-itemid');
            
            if ($("#subtabla1_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
                //console.log('Platform: no visible');
            }
            row.child('<table id="subtabla1_' + id_tabla +
                '" class="display" style="width:100%;  ">' +
                '<thead>' +
                '<tr>' +
                '<th>Actions</th>' +
                '<th>Platform Name</th>' +
                '<th>Total Budget</th>' +
                '<th>Spend</th>' +
                // '<th>Goals</th>' +
                '<th>Impressions</th>' +
                '<th>Click</th>' +
                '<th>Ctr</th>' +
                '<th>Cpc</th>' +
                '<th>Conversion</th>' +
                '<th>Cr</th>' +
                '<th>Cpa</th>' +
                '<th>Cpm</th>' +
                '</tr>' +
                '</thead>' +
                '</table>');


            /*  if (visible) {
                  // hacer algo aquí si el elemento existe
                  row.child.hide();
                  tr.removeClass('show');
              } else { **/
            row.child.show();
            tr.addClass('show');
            $('td[colspan="11"]').addClass('SubTableDuplicated');

            var ruta = '/api/dashboard/data-platform/' + id_tabla;
            //console.log(ruta);
            table3 = $('#subtabla1_' + id_tabla).DataTable({
                autoWidth: false,
                //ajax: ruta,
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",


                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val();
                    }
                },

                "columns": [{
                        "className": 'details-control',
                        "width": "250px",
                        "orderable": false,
                        "data": "public_id",
                        "defaultContent": '',
                        "render": function(data, type, row, meta) {

                            var check = '';
                            if (row.total_status > 0) {
                                check = 'checked="checked"';
                            }

                            console.log("data atom  fa-atom list-platform-atom", row)
                            console.log("data atom  fa-atom list-platform-atom", data)

                            var botonera =
                                '<label class="checkbox" style="float:left; margin: 3px 0;">';
                            botonera = botonera +
                                '<input class="duplicate-campaign-platform" data-type="campaigns" data-campaign="' +
                                data + '" type="checkbox" data-itemid="' + data +
                                '" name="duplicate" /><span></span></label>&ensp;';
                            botonera = botonera +
                                '<span class="switch switch-sm"><label><input class="switch_checkbox_platform" data-type="campaigns_platform" type="checkbox" ' +
                                check + ' data-platform="' + row.plataforma + '" data-itemid="' + data +
                                '" name="select"  ><span></span></label></span>&ensp;';
                            botonera = botonera +
                                '<svg class="kt-font-brand flaticon-tabs  list-platform-campaign" data-itemid="' + id_tabla + '" data-platform="' + row.plataforma + '" title="Show by Campaign Platform" id="_27_Icon_layers" data-name="27) Icon/layers" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M9,18a1.009,1.009,0,0,1-.5-.132l-8-4.594a1,1,0,0,1-.5-.926.992.992,0,0,1,.606-.86l2.152-.916L.5,9.274A1,1,0,0,1,.608,7.487l2.152-.916L.5,5.274A1,1,0,0,1,.608,3.488l8-3.407a.994.994,0,0,1,.783,0l8,3.407A1,1,0,0,1,17.5,5.274l-2.259,1.3,2.152.916A1,1,0,0,1,17.5,9.274l-2.258,1.3,2.152.917a1,1,0,0,1,.106,1.786l-8,4.594A1.007,1.007,0,0,1,9,18ZM4.934,11.819l-1.694.722L9,15.847l5.76-3.307-1.694-.723L9.5,13.867a1,1,0,0,1-1,0L4.934,11.819Zm0-4L3.24,8.54,9,11.848,14.76,8.54l-1.694-.722L9.5,9.868a1,1,0,0,1-1,0L4.934,7.818ZM9,2.087,3.24,4.541,9,7.847l5.76-3.307L9,2.087Z" transform="translate(3 3)" fill="#222b45"></path> </svg>&ensp;';
                            botonera = botonera +
								'<svg class="list-platform-atom" ' + 'data-platform="' + row.plataforma + '" data-itemid="' + id_tabla + '"  title="Show Atomo" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"> <path id="Shape" d="M10,18.057A11.814,11.814,0,0,1,4.211,20a4.218,4.218,0,0,1-3.069-1.141c-1.32-1.319-1.5-3.512-.513-6.172A16.214,16.214,0,0,1,1.944,10,16.241,16.241,0,0,1,.629,7.314c-.989-2.661-.807-4.853.513-6.173A4.222,4.222,0,0,1,4.211,0,11.814,11.814,0,0,1,10,1.943,11.814,11.814,0,0,1,15.789,0a4.224,4.224,0,0,1,3.07,1.141c1.319,1.32,1.5,3.512.512,6.173A16.241,16.241,0,0,1,18.056,10a16.214,16.214,0,0,1,1.315,2.687c.989,2.66.807,4.852-.512,6.172A4.22,4.22,0,0,1,15.789,20,11.814,11.814,0,0,1,10,18.057Z" transform="translate(0)"/> </svg>&ensp;';
                            botonera = botonera +
                                '<svg class="flaticon2-line-chart" data-itementity="campaign" data-platform="' + row.id + '" data-itemid="' + data + '"  title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path  d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"/> </svg>';
                                

                            return botonera;

                        }

                    },



                    {
                        "data": "plataforma"
                    },
                    {
                        "data": "total_budget"
                    },
                    {
                        "data": "total_spent"
                    },
                    {
                        "data": "total_impression"
                    },
                    {
                        "data": "total_clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "total_conversion"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }

                ]
            });

            table3.on('draw.dt', function() {

                $('.switch_checkbox_platform').change(function() {
                    var type = $(this).attr('data-type');
                    var id = row.data().public_id;
                    var platform = $(this).attr('data-platform');
                    var check = false;
                    var field = 'status';

                    if ($(this).is(":checked")) {
                        check = true;

                    }

                    $.ajax({
                        url: "/api/dashboard/entity/update",
                        type: "post",
                        dataType: "json",
                        data: {
                            'id': id,
                            'platform': platform,
                            'entity': type,
                            'check': check,
                            'field': field
                        }
                    }).done(function(res) {
                        if (res)
                            toastr.success("Successful Changes ");
                        else
                            toastr.error("Something Went Wrong ");
                    });
                });

            });


            //  }
        });

        //Platform - Show List Campaigns by Platform
        $('#dashboard_table tbody').on('click', '.list-creativity-by-campaign', function() {

            var tr = $(this).closest('tr');
            var row = table2.row(tr);
            //var id_tabla = row.data().id;
            var visible = false;
            var id_tabla = $(this).attr('data-itemid');
            var platform = $(this).attr('data-platform');

            if ($("#subtabla1_1_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
                //console.log('Platform: no visible');
            }

            row.child('<table id="subtabla1_1_' + id_tabla +
                '" class="display" style="width:100%;">' +
                '<thead>' +
                '<tr>' +
                '<th>Actions</th>' +
                '<th>Creativity Name</th>' +
                '<th>Budget</th>' +
                '<th>Spend</th>' +
                '<th>Impressions</th>' +
                '<th>Click</th>' +
                '<th>Ctr</th>' +
                '<th>Cpc</th>' +
                '<th>Conversion</th>' +
                '<th>Cr</th>' +
                '<th>Cpa</th>' +
                '<th>Cpm</th>' +
                '</tr>' +
                '</thead>' +
                '</table>');


            row.child.show();
            tr.addClass('show');
            $('td[colspan="11"]').addClass('SubTableDuplicated');

            var ruta = '/api/dashboard/data-creativity-by-campaign/' + id_tabla + '/' + platform;

            table4 = $('#subtabla1_1_' + id_tabla).DataTable({
                autoWidth: false,
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",

                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val();
                    }
                },
                "columns": [{
                        "className": 'details-control',
                        "width": "250px",
                        "orderable": false,
                        "data": 'public_id',
                        "defaultContent": '',
                        "render": function(data, type, row, meta) {

                            var check = '';
                            if (row.status == 'ACTIVE') {
                                check = 'checked="checked"';
                            }


                            return '<label class="checkbox" style="float:left; margin: 3px 0;"><input class="duplicate" data-type="creativity" data-campaign="' +
                                id_tabla + '" type="checkbox" data-itemid="' + data +
                                '" name="duplicate" /> <span></span></label>&ensp;<span class="switch switch-sm"><label><input class="switch_checkbox_creativity" data-type="creativity" type="checkbox" ' +
                                check + ' data-itemid="' + data + '" data-table="' + id_tabla +
                                '" name="select"><span></span></label></span>&ensp;'+
								'<svg class="flaticon-edit" data-itemid="' + data + '" onclick="javascript:editModalCreas(\'' + data + '\');"  title="Edit" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"></path> </svg>&ensp;'+
								'<svg class="flaticon2-line-chart"  data-itementity="creativity" data-itemid="' + data + '"  title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"></path> </svg>&ensp;';

                            return '<label class="checkbox" style="float:left; margin: 3px 0;"><input class="duplicate" data-type="creativity" data-campaign="' +
                                id_tabla + '" type="checkbox" data-itemid="' + data + '" name="duplicate" /> <span></span></label>&ensp;' +
                                '<span class="switch switch-sm"><label><input class="switch_checkbox_creativity" data-type="creativity" type="checkbox" ' + check + ' data-itemid="' + data + '" data-table="' + id_tabla + '" name="select"><span></span></label></span>&ensp;'+
								'<svg class="flaticon-edit" data-itemid="' + data + '" title="Edit Campaign" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"></path> </svg>&ensp;'+
								'<svg class=" list-platform" data-itemid="' + data + '" title="Show by Platform" id="_27_Icon_layers" data-name="27) Icon/layers" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M9,18a1.009,1.009,0,0,1-.5-.132l-8-4.594a1,1,0,0,1-.5-.926.992.992,0,0,1,.606-.86l2.152-.916L.5,9.274A1,1,0,0,1,.608,7.487l2.152-.916L.5,5.274A1,1,0,0,1,.608,3.488l8-3.407a.994.994,0,0,1,.783,0l8,3.407A1,1,0,0,1,17.5,5.274l-2.259,1.3,2.152.916A1,1,0,0,1,17.5,9.274l-2.258,1.3,2.152.917a1,1,0,0,1,.106,1.786l-8,4.594A1.007,1.007,0,0,1,9,18ZM4.934,11.819l-1.694.722L9,15.847l5.76-3.307-1.694-.723L9.5,13.867a1,1,0,0,1-1,0L4.934,11.819Zm0-4L3.24,8.54,9,11.848,14.76,8.54l-1.694-.722L9.5,9.868a1,1,0,0,1-1,0L4.934,7.818ZM9,2.087,3.24,4.541,9,7.847l5.76-3.307L9,2.087Z" transform="translate(3 3)" fill="#222b45"/> </svg>&ensp;'+
								'<svg class="flaticon2-line-chart" data-itemid="' + data + '"  title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"></path></svg>&ensp;';
                        }
                    },
                    {
                        "data": "name",
                        "render": function(data, type, row, meta) {

                            return ' <a href="#" class="name editable editable-click" data-type="text" data-pk="' +
                                row.public_id +
                                '" data-name="creativity" data-title="Enter title">' + data +
                                '</a>';
                        }

                    },
                    {
                        "data": "budget",
                        "render": function(data, type, row, meta) {
                            if (data == null) {
                                data = 0;
                            } else {
                                data = parseFloat(data).toFixed(2);
                            }
                            return ' <a href="#" class="budget editable editable-click" data-type="text" data-pk="' +
                                row.public_id +
                                '" data-name="creativity" data-title="Enter budget">' + data +
                                '</a>';
                        }
                    },
                    {
                        "data": "spent"
                    },
                    {
                        "data": "impressions"
                    },
                    {
                        "data": "clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "conversions"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }
                ]
            });


            table4.on('draw.dt', function() {

                $('.budget').editable({
                    type: 'text',
                    //url: '/api/dashboard/change_budget',
                    url: '/api/dashboard/entity/update',
                    params: function(params) {
                        params.field = 'budget';
                        params.entity = 'creativity';
                        params.id = params.pk;
                        console.log(params);
                        return params;
                    },
                    validate: function(value) {

                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                        if ($.isNumeric(value) == '') {
                            return 'This Number is required';
                        }
                    },
                    success: function(response) {

                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.name').editable({
                    type: 'text',
                    url: '/api/dashboard/entity/update',
                    params: function(params) {
                        params.field = 'name';
                        params.entity = 'creativity';
                        params.id = params.pk;
                        console.log(params);
                        return params;
                    },
                    validate: function(value) {

                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                    },
                    success: function(response) {

                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.switch_checkbox_creativity').change(function() {
                    var type = $(this).attr('data-type');
                    var id = $(this).attr('data-itemid');
                    var check = false;
                    var field = 'status';

                    if ($(this).is(":checked")) {
                        check = true;

                    }

                    $.ajax({
                        url: "/api/dashboard/entity/update",
                        type: "post",
                        dataType: "json",
                        data: {
                            'id': id,
                            'entity': type,
                            'check': check,
                            'field': field,

                        }
                    }).done(function(res) {
                        if (res)
                            toastr.success("Successful Changes ");
                        else
                            toastr.error("Something Went Wrong ");
                    });
                });

            });

        });

        //Platform - Show List Campaigns by Platform
        $('#dashboard_table tbody').on('click', '.list-platform-campaign', function() {

            var tr = $(this).closest('tr');
            var row = table3.row(tr);
            //var id_tabla = row.data().id;
            var visible = false;
            var id_tabla = $(this).attr('data-itemid');
            var platform = $(this).attr('data-platform');


            if ($("#subtabla1_1_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
                //console.log('Platform: no visible');
            }
            row.child('<table id="subtabla1_1_' + id_tabla +
                '" class="display" style="width:100%;">' +
                '<thead>' +
                '<tr>' +
                //'<th>Actions</th>' +
                '<th>Campaigns by Platform Name</th>' +
                '<th>Budget</th>' +
                '<th>Spend</th>' +
                '<th>Impressions</th>' +
                '<th>Click</th>' +
                '<th>Ctr</th>' +
                '<th>Cpc</th>' +
                '<th>Conversion</th>' +
                '<th>Cr</th>' +
                '<th>Cpa</th>' +
                '<th>Cpm</th>' +
                '</tr>' +
                '</thead>' +
                '</table>');


            /* if (visible) {
                 // hacer algo aquí si el elemento existe
                 row.child.hide();
                 tr.removeClass('show');
             } else {*/
            row.child.show();
            $('td[colspan="11"]').addClass('SubTableDuplicated');

            tr.addClass('show');
            var ruta = '/api/dashboard/data-platform-campaign-list/' + id_tabla + '/' + platform;


            table4 = $('#subtabla1_1_' + id_tabla).DataTable({
                autoWidth: false,
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",
                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val();
                    }
                },
                "columns": [{
                        "data": "name"
                    },
                    {
                        "data": "budget"
                    },
                    {
                        "data": "spent"
                    },
                    {
                        "data": "impression"
                    },
                    {
                        "data": "clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "conversion"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }
                ]
            });

        });

        //Platform - Show List Atoms by Platform
        $('#dashboard_table tbody').on('click', '.list-platform-atom', function() {

            var tr = $(this).closest('tr');
            var row = table3.row(tr);
            //var id_tabla = row.data().id;
            var visible = false;
            var id_tabla = $(this).attr('data-itemid');
            var platform = $(this).attr('data-platform');


            if ($("#subtabla1_2_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
                //console.log('Platform: no visible');
            }
            row.child('<table id="subtabla1_2_' + id_tabla +
                '" class="display" style="width:100%;">' +
                '<thead>' +
                '<tr>' +
                // '<th>Actions</th>' +
                '<th>Atoms by Platform Name</th>' +
                '<th>Budget</th>' +
                '<th>Spend</th>' +
                '<th>Impressions</th>' +
                '<th>Click</th>' +
                '<th>Ctr</th>' +
                '<th>Cpc</th>' +
                '<th>Conversion</th>' +
                '<th>Cr</th>' +
                '<th>Cpa</th>' +
                '<th>Cpm</th>' +
                '</tr>' +
                '</thead>' +
                '</table>');


            /* if (visible) {
                 // hacer algo aquí si el elemento existe
                 row.child.hide();
                 tr.removeClass('show');
             } else {*/
            row.child.show();
            tr.addClass('show');
            var ruta = '/api/dashboard/data-platform-atom-list/' + id_tabla + '/' + platform;
            //console.log(ruta);
            $('td[colspan="11"]').addClass('SubTableDuplicated');

            table4 = $('#subtabla1_2_' + id_tabla).DataTable({
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",
                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val();
                    }
                },
                "columns": [{
                        "data": "name"
                    },
                    {
                        "data": "budget"
                    },

                    {
                        "data": "spent"
                    },
                    {
                        "data": "impression"
                    },
                    {
                        "data": "clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "conversion"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }
                ]
            });

            //   }
        });

        //Campaign Platform - Show List Atoms by Campaign Platform
        $('#dashboard_table tbody').on('click', '.list-campaign-platform-atom', function() {

            var tr = $(this).closest('tr');
            var row = table3.row(tr);
            //var id_tabla = row.data().id;
            var visible = false;
            var id_tabla = $(this).attr('data-itemid');


            if ($("#subtabla2_1_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
                //console.log('Platform: no visible');
            }
            row.child('<table id="subtabla2_1_' + id_tabla +
                '" class="display" style="width:100%; ">' +
                '<thead>' +
                '<tr>' +
                // '<th>Actions</th>' +
                '<th>Atoms by Campaign Platform Name</th>' +
                '<th>Budget</th>' +
                '<th>Spend</th>' +
                '<th>Impressions</th>' +
                '<th>Click</th>' +
                '<th>Ctr</th>' +
                '<th>Cpc</th>' +
                '<th>Conversion</th>' +
                '<th>Cr</th>' +
                '<th>Cpa</th>' +
                '<th>Cpm</th>' +
                '</tr>' +
                '</thead>' +
                '</table>');


            /*  if (visible) {
                  // hacer algo aquí si el elemento existe
                  row.child.hide();
                  tr.removeClass('show');
              } else {*/
            row.child.show();
            tr.addClass('show');
            $('td[colspan="11"]').addClass('SubTableDuplicated');

            var ruta = '/api/dashboard/data-atom-campaign-list/' + id_tabla;
            //console.log(ruta);
            table4 = $('#subtabla2_1_' + id_tabla).DataTable({
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",

                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val();
                    }
                },
                autoWidth: false,
                "columns": [{
                        "data": "name"
                    },
                    {
                        "data": "budget"
                    },
                    {
                        "data": "spent"
                    },
                    {
                        "data": "impression"
                    },
                    {
                        "data": "clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "conversion"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }
                ]
            });

            // }
        });

        //List atom OK
        $('#dashboard_table tbody').on('click', '.list-atom', function() {
            // console.log('atom');
            var tr = $(this).closest('tr');
            var row = table2.row(tr);
            var id_tabla = $(this).attr('data-itemid');

            var visible = false;
            if ($("#subtabla3_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
                //console.log('Atomo: no visible');
            }
            row.child('<table id="subtabla3_' + id_tabla +
                '" class="display" style="width:100%; ">' +
                '<thead>' +
                '<tr>' +
                '<th>Actions</th>' +
                '<th>Atom Name</th>' +
                '<th>Budget</th>' +
                '<th>Spend</th>' +
                '<th>Impressions</th>' +
                '<th>Click</th>' +
                '<th>Ctr</th>' +
                '<th>Cpc</th>' +
                '<th>Conversion</th>' +
                '<th>Cr</th>' +
                '<th>Cpa</th>' +
                '<th>Cpm</th>' +
                '</tr>' +
                '</thead>' +
                '</table>');

            /*  if (visible) {
                  // hacer algo aquí si el elemento existe
                  row.child.hide();
                  tr.removeClass('show');
              } else {*/
            row.child.show();
            tr.addClass('show');
            var ruta = '/api/dashboard/data-atom/' + id_tabla;
            //console.log(ruta);
            table3 = $('#subtabla3_' + id_tabla).DataTable({
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",

                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val();
                    }
                },
                autoWidth: false,
                "columns": [{
                        "className": 'details-control',
                        "width": "250px",
                        "orderable": false,
                        "data": 'public_id',
                        "defaultContent": '',
                        "render": function(data, type, row, meta) {
                            console.log('campagins>>atoms', row)
                            var check = '';
                            if (row.status == 'ACTIVE') {
                                check = 'checked="checked"';
                            }

                            var botonera =
                                '<label class="checkbox" style="float:left; margin: 3px 0;"><input class="duplicate" data-type="atomo" data-campaign="' +
                                id_tabla + '" type="checkbox" data-itemid="' + data +
                                '" name="duplicate" /><span></span></label>&ensp;';
                            botonera = botonera +
                                '<span class="switch switch-sm"><label><input class="switch_checkbox_atom" data-type="atom" type="checkbox" ' +
                                check + ' data-itemid="' + data + '" data-table="' + id_tabla +
                                '" name="select"><span></span></label></span>&ensp;';
                            botonera = botonera +
								'<svg class="flaticon-edit" onclick="javascript:editModalAtom(\'' + data + '\');" data-itemid="' + data + '" title="Edit Atomo" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"></path> </svg>&ensp;';
                                
                            //botonera = botonera + '<i class="kt-font-brand flaticon2-image-file list-creativity" data-itemid="' + data + '"  title="Show Creativities"></i>';
                            botonera = botonera +
							'<svg class="flaticon2-line-chart"  data-itemid="' + data + '"  data-itementity="atom"  title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"></path> </svg>&ensp;';

                            return botonera;
                        }
                    },
                    {
                        "data": "name",
                        "render": function(data, type, row, meta) {

                            return RRSS_ICONS[row.platform] +
                                ' <a href="#" class="name editable editable-click" data-type="text" data-pk="' +
                                row.public_id + '" data-name="atom" data-title="Enter name">' +
                                data + '</a>';
                        }

                    },
                    {
                        "data": "budget",
                        "render": function(data, type, row, meta) {
                            if (data == null) {
                                data = 0;
                            } else {
                                data = parseFloat(data).toFixed(2);
                            }
                            return ' <a href="#" class="budget editable editable-click" data-type="text" data-pk="' +
                                row.public_id + '" data-name="atom" data-title="Enter budget">' +
                                data + '</a>';
                        }
                    },
                    {
                        "data": "spent"
                    },
                    {
                        "data": "impression"
                    },
                    {
                        "data": "clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "conversion"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }
                ]
            });

            table3.on('draw.dt', function() {

                $('.budget').editable({
                    type: 'text',
                    //url: '/api/dashboard/change_budget',
                    url: '/api/dashboard/entity/update',
                    params: function(params) {
                        params.field = 'budget';
                        params.entity = 'atom';
                        params.id = params.pk;
                        console.log(params);
                        return params;
                    },
                    validate: function(value) {

                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                        if ($.isNumeric(value) == '') {
                            return 'This Number is required';
                        }
                    },
                    success: function(response) {

                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.name').editable({
                    type: 'text',
                    url: '/api/dashboard/entity/update',
                    params: function(params) {
                        params.field = 'name';
                        params.entity = 'atom';
                        params.id = params.pk;
                        console.log(params);
                        return params;
                    },
                    validate: function(value) {

                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                    },
                    success: function(response) {

                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.switch_checkbox_atom').change(function() {
                    var type = $(this).attr('data-type');
                    var id = $(this).attr('data-itemid');
                    var check = false;
                    var field = 'status';

                    if ($(this).is(":checked")) {
                        check = true;

                    }

                    $.ajax({
                        url: "/api/dashboard/entity/update",
                        type: "post",
                        dataType: "json",
                        data: {
                            'id': id,
                            'entity': type,
                            'check': check,
                            'field': field,

                        }
                    }).done(function(res) {
                        if (res)
                            toastr.success("Successful Changes ");
                        else
                            toastr.error("Something Went Wrong ");
                    });
                });

            });
            //}
        });

        //List creativity OK
        $('#dashboard_table tbody').on('click', '.list-creativity-from-atom-father', function() {
            console.log('creativity');
            var tr = $(this).closest('tr');
            console.log(tr);
            var row = table2.row(tr);
            var id_tabla = $(this).attr('data-itemid');
            var visible = false;
            if ($("#subtabla4_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
                //console.log('Creativity: no visible');
            }
            row.child('<table id="subtabla4_' + id_tabla +
                '" class="display" style="width:100%; ">' +
                '<thead>' +
                '<tr>' +
                '<th>Actions</th>' +
                '<th>Creativity Name</th>' +
                '<th>Budget</th>' +
                '<th>Spend</th>' +
                '<th>Impressions</th>' +
                '<th>Click</th>' +
                '<th>Ctr</th>' +
                '<th>Cpc</th>' +
                '<th>Conversion</th>' +
                '<th>Cr</th>' +
                '<th>Cpa</th>' +
                '<th>Cpm</th>' +
                '</tr>' +
                '</thead>' +
                '</table>');


            row.child.show();
            tr.addClass('show');
            var ruta = '/api/dashboard/atom/creativity/' + id_tabla;
            table2 = $('#subtabla4_' + id_tabla).DataTable({
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",

                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val();
                    }
                },
                autoWidth: false,


                "columns": [{
                        "className": 'details-control',
                        "width": "250px",
                        "orderable": false,
                        "data": 'public_id',
                        "defaultContent": '',
                        "render": function(data, type, row, meta) {

                            var check = '';
                            if (row.status == 'ACTIVE') {
                                check = 'checked="checked"';
                            }


                            return '<label class="checkbox" style="float:left; margin: 3px 0;"><input class="duplicate" data-type="creativity" data-campaign="' +
                                id_tabla + '" type="checkbox" data-itemid="' + data +
                                '" name="duplicate" /> <span></span></label>&ensp;<span class="switch switch-sm"><label><input class="switch_checkbox_creativity" data-type="creativity" type="checkbox" ' +
                                check + ' data-itemid="' + data + '" data-table="' + id_tabla +
                                '" name="select"><span></span></label></span>&ensp; <svg class="flaticon-edit"  onclick="javascript:editModalCreas(\'' + data + '\');"  data-itemid="' + data + '" title="Edit" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"></path> </svg>&ensp;<svg class="flaticon2-line-chart"  data-itementity="creativity" data-itemid="' + data + '"  title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"></path> </svg>&ensp;';

                            return '<label class="checkbox" style="float:left; margin: 3px 0;"><input class="duplicate" data-type="creativity" data-campaign="' +
                                id_tabla + '" type="checkbox" data-itemid="' + data +
                                '" name="duplicate" /> <span></span></label>&ensp;<span class="switch switch-sm"><label><input class="switch_checkbox_creativity" data-type="creativity" type="checkbox" ' +
                                check + ' data-itemid="' + data + '" data-table="' + id_tabla +
                                '" name="select"><span></span></label></span>&ensp;'+
								'<svg class="flaticon-edit" data-itemid="' + data + '" title="Edit Campaign" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"></path> </svg>&ensp;'+
								'<svg class="list-platform" data-itemid="' + data + '" title="Show by Platform" id="_27_Icon_layers" data-name="27) Icon/layers" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M9,18a1.009,1.009,0,0,1-.5-.132l-8-4.594a1,1,0,0,1-.5-.926.992.992,0,0,1,.606-.86l2.152-.916L.5,9.274A1,1,0,0,1,.608,7.487l2.152-.916L.5,5.274A1,1,0,0,1,.608,3.488l8-3.407a.994.994,0,0,1,.783,0l8,3.407A1,1,0,0,1,17.5,5.274l-2.259,1.3,2.152.916A1,1,0,0,1,17.5,9.274l-2.258,1.3,2.152.917a1,1,0,0,1,.106,1.786l-8,4.594A1.007,1.007,0,0,1,9,18ZM4.934,11.819l-1.694.722L9,15.847l5.76-3.307-1.694-.723L9.5,13.867a1,1,0,0,1-1,0L4.934,11.819Zm0-4L3.24,8.54,9,11.848,14.76,8.54l-1.694-.722L9.5,9.868a1,1,0,0,1-1,0L4.934,7.818ZM9,2.087,3.24,4.541,9,7.847l5.76-3.307L9,2.087Z" transform="translate(3 3)" fill="#222b45"/> </svg>&ensp;'+
								'<svg class="flaticon2-line-chart"  data-itemid="' + data + '"  title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"></path> </svg>&ensp;';

                        }
                    },
                    {
                        "data": "title",
                        "render": function(data, type, row, meta) {

                            return ' <a href="#" class="name editable editable-click" data-type="text" data-pk="' +
                                row.public_id +
                                '" data-name="creativity" data-title="Enter name">' + row.name +
                                '</a>';
                        }

                    },
                    {
                        "data": "budget",
                        "render": function(data, type, row, meta) {
                            if (row.budget == null) {
                                row.budget = 0;
                            } else {
                                row.budget = parseFloat(row.budget).toFixed(2);
                            }
                            return ' <a href="#" class="budget editable editable-click" data-type="text" data-pk="' +
                                row.public_id +
                                '" data-name="creativity" data-title="Enter budget">' + row.budget +
                                '</a>';
                        }
                    },
                    {
                        "data": "spent"
                    },
                    {
                        "data": "impressions"
                    },
                    {
                        "data": "clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "conversions"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }
                ]

            });

            table2.on('draw.dt', function() {

                $('.switch_checkbox_creativity').change(function() {
                    var type = $(this).attr('data-type');
                    var id = $(this).attr('data-itemid');
                    var check = false;
                    var field = 'status';

                    if ($(this).is(":checked")) {
                        check = true;

                    }

                    $.ajax({
                        url: "/api/dashboard/entity/update",
                        type: "post",
                        dataType: "json",
                        data: {
                            'id': id,
                            'entity': type,
                            'check': check,
                            'field': field,

                        }
                    }).done(function(res) {
                        if (res)
                            toastr.success("Successful Changes ");
                        else
                            toastr.error("Something Went Wrong ");
                    });
                });

            });


        });

        //List creativity OK
        $('#dashboard_table tbody').on('click', '.list-creativity', function() {
            console.log('creativity');
            var tr = $(this).closest('tr');
            var row = table1.row(tr);
            var id_tabla = $(this).attr('data-itemid');
            var visible = false;
            if ($("#subtabla4_" + id_tabla + "_wrapper").length > 0) {
                visible = true;
                //console.log('Creativity: no visible');
            }
            row.child('<table id="subtabla4_' + id_tabla +
                '" class="display" style="width:100%; ">' +
                '<thead>' +
                '<tr>' +
                '<th>Actions</th>' +
                '<th>Creativity Name</th>' +
                '<th>Budget</th>' +
                '<th>Spend</th>' +
                '<th>Impressions</th>' +
                '<th>Click</th>' +
                '<th>Ctr</th>' +
                '<th>Cpc</th>' +
                '<th>Conversion</th>' +
                '<th>Cr</th>' +
                '<th>Cpa</th>' +
                '<th>Cpm</th>' +
                '</tr>' +
                '</thead>' +
                '</table>');


            row.child.show();
            tr.addClass('show');

            var ruta = '/api/dashboard/customer/creativity/' + id_tabla;

            table3 = $('#subtabla4_' + id_tabla).DataTable({
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-3'p><'col-sm-3'i>>",
                ajax: {
                    url: ruta,
                    data: function(d) {
                        d.period_filter = $('#periodo_hometable').val();
                    }
                },
                autoWidth: false,
                "columns": [{
                        "className": 'details-control',
                        "width": "250px",
                        "orderable": false,
                        "data": 'public_id',
                        "defaultContent": '',
                        "render": function(data, type, row, meta) {

                            var check = '';
                            if (row.status == 'ACTIVE') {
                                check = 'checked="checked"';
                            }


                            return '<label class="checkbox" style="float:left; margin: 3px 0;"><input class="duplicate" data-type="creativity" data-campaign="' +
                                id_tabla + '" type="checkbox" data-itemid="' + data +
                                '" name="duplicate" /> <span></span></label>&ensp;<span class="switch switch-sm"><label><input class="switch_checkbox_creativity" data-type="creativity" type="checkbox" ' +
                                check + ' data-itemid="' + data + '" data-table="' + id_tabla +
                                '" name="select"><span></span></label></span>&ensp;'+
								'<svg class="flaticon-edit"  onclick="javascript:editModalCreas(\'' + data + '\');"  data-itemid="' + data + '" title="Edit Campaign" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"></path> </svg>&ensp;'+
								'<svg class="flaticon2-line-chart" data-itementity="creativity" data-itemid="' + data + '"  title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"/> </svg>&ensp;';

                            return '<label class="checkbox" style="float:left; margin: 3px 0;"><input class="duplicate" data-type="creativity" data-campaign="' +
                                id_tabla + '" type="checkbox" data-itemid="' + data +
                                '" name="duplicate" /> <span></span></label>&ensp;<span class="switch switch-sm"><label><input class="switch_checkbox_creativity" data-type="creativity" type="checkbox" ' +
                                check + ' data-itemid="' + data + '" data-table="' + id_tabla +
                                '" name="select"><span></span></label></span>&ensp;'+
								'<svg class="flaticon-edit" data-itemid="' + data + '" title="Edit Campaign" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"></path> </svg>&ensp;'+
								'<svg class="list-platform" data-itemid="' + data + '" title="Show by Platform" id="_27_Icon_layers" data-name="27) Icon/layers" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M9,18a1.009,1.009,0,0,1-.5-.132l-8-4.594a1,1,0,0,1-.5-.926.992.992,0,0,1,.606-.86l2.152-.916L.5,9.274A1,1,0,0,1,.608,7.487l2.152-.916L.5,5.274A1,1,0,0,1,.608,3.488l8-3.407a.994.994,0,0,1,.783,0l8,3.407A1,1,0,0,1,17.5,5.274l-2.259,1.3,2.152.916A1,1,0,0,1,17.5,9.274l-2.258,1.3,2.152.917a1,1,0,0,1,.106,1.786l-8,4.594A1.007,1.007,0,0,1,9,18ZM4.934,11.819l-1.694.722L9,15.847l5.76-3.307-1.694-.723L9.5,13.867a1,1,0,0,1-1,0L4.934,11.819Zm0-4L3.24,8.54,9,11.848,14.76,8.54l-1.694-.722L9.5,9.868a1,1,0,0,1-1,0L4.934,7.818ZM9,2.087,3.24,4.541,9,7.847l5.76-3.307L9,2.087Z" transform="translate(3 3)" fill="#222b45"/> </svg>&ensp;'+
								'<svg class="flaticon2-line-chart"  data-itemid="' + data + '"  title="Show last 30days Chart" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"> <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"/> </svg>&ensp;';
								
                        }
                    },
                    {
                        "data": "title",
                        "render": function(data, type, row, meta) {

                            return ' <a href="#" class="name editable editable-click" data-type="text" data-pk="' +
                                row.public_id +
                                '" data-name="creativity" data-title="Enter name">' + row.name +
                                '</a>';
                        }

                    },
                    {
                        "data": "budget",
                        "render": function(data, type, row, meta) {
                            if (row.budget == null) {
                                row.budget = 0;
                            } else {
                                row.budget = parseFloat(row.budget).toFixed(2);
                            }
                            return ' <a href="#" class="budget editable editable-click" data-type="text" data-pk="' +
                                row.public_id +
                                '" data-name="creativity" data-title="Enter budget">' + row.budget +
                                '</a>';
                        }
                    },
                    {
                        "data": "spent"
                    },
                    {
                        "data": "impressions"
                    },
                    {
                        "data": "clicks"
                    },
                    {
                        "data": "ctr"
                    },
                    {
                        "data": "cpc"
                    },
                    {
                        "data": "conversions"
                    },
                    {
                        "data": "cr"
                    },
                    {
                        "data": "cpa"
                    },
                    {
                        "data": "cpm"
                    }
                ]

            });

            table3.on('draw.dt', function() {

                $('.switch_checkbox_creativity').change(function() {
                    var type = $(this).attr('data-type');
                    var id = $(this).attr('data-itemid');
                    var check = false;
                    var field = 'status';

                    if ($(this).is(":checked")) {
                        check = true;

                    }

                    $.ajax({
                        url: "/api/dashboard/entity/update",
                        type: "post",
                        dataType: "json",
                        data: {
                            'id': id,
                            'entity': type,
                            'check': check,
                            'field': field,

                        }
                    }).done(function(res) {
                        if (res)
                            toastr.success("Successful Changes ");
                        else
                            toastr.error("Something Went Wrong ");
                    });
                });

            });


        });

        $('#dashboard_table tbody').on('click', '.flaticon2-line-chart', function() {
            console.log('flaticon2-line-chart');

            var tr = $(this).closest('tr');

            console.log($(this))
            console.log($(this).attr('data-itemid'))

           // var row = table2.row(tr);
            var tenemosTabla = false;
            var subTabla = '';

            console.log('tabla4',table4)
            console.log('tabla3',table3)
            console.log('tabla2',table2)
            console.log('tabla1',table1)
            
            if (table4 != "") {
                tenemosTabla = true;
                subTabla='subtabla3_';
                var row = table4.row(tr);
                //console.log('subTabla', subTabla)
               // console.log('tenemosTabla', tenemosTabla)
            }

            if (table3 != "" && !tenemosTabla) {
                tenemosTabla = true;
                subTabla='subtabla2_';
                var row = table3.row(tr);
              //  console.log('subTabla', subTabla)
              //  console.log('tenemosTabla', tenemosTabla)
            }

            if (table2 != "" && !tenemosTabla) {
                tenemosTabla = true;
                subTabla='subtabla1_';
                var row = table2.row(tr);
             //   console.log('subTabla', subTabla)
             //   console.log('tenemosTabla', tenemosTabla)
            }

            if (table1 != "" && !tenemosTabla) {
                tenemosTabla = true;
                subTabla='subtabla0_';
                var row = table1.row(tr);
              //  console.log('subTabla', subTabla)
              //  console.log('tenemosTabla', tenemosTabla)
            }

            var id_tabla = $(this).attr('data-itemid');
            var entity = $(this).attr('data-itementity');
            var platform = $(this).attr('data-platform');
            var visible = false;

            if ($("#"+subTabla + id_tabla + "_wrapper").length > 0) {
                visible = true;
                console.log('visible');
            }
            /*             row.child('<table id="subtabla4_'+id_tabla+'" class="display" style="width:100%">'+
                            '<thead>'+
                                '<tr>'+
                                    '<th></th>'+
                                    '<th>Name3</th>'+
                                    '<th>Clicks3</th>'+
                                    '<th>Cpm3</th>'+
                                    '<th>Ctr3</th>'+
                                '</tr>'+
                            '</thead>'+
                        '</table>'); */


            row.child('<div id="chart-container-30days-' + id_tabla + platform + '"></div>');



            $.ajax({
                    url: '/api/dashboard/chart-data/' + entity + '/' + id_tabla + '/' + platform,
                    data: null,
                    type: 'GET',
                    contentType: false,
                    dataType: "json",
                    processData: false,

                    beforeSend: function() {
                        $('#saveBtnCustomer').html(
                            '<img src="img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">'
                        );
                    },
                })
                .done(function(response) {

                    console.log(response.result);


                    var dataImpressions = [];
                    var dataSpend = [];
                    var dataConversions = [];
                    var dataDates = [];

                    $.each(response.result, function(p, item) {
                        console.log(item);
                        dataDates.push(item.x);
                        dataImpressions.push(item.impression);
                        dataConversions.push(item.conversions);
                        dataSpend.push(item.spend);
                    });
                    console.log(dataImpressions);
                    console.log(dataConversions);
                    console.log(dataSpend);

                    var options = {
                        series: [{
                                name: 'Conversions',
                                type: 'column',
                                data: dataConversions
                            },
                            {
                                name: 'Impressions',
                                type: 'line',
                                data: dataImpressions
                            },
                            {
                                name: 'Spend',
                                type: 'line',
                                data: dataSpend
                            },
                        ],
                        chart: {
                            height: 350,
                            type: 'line',
                        },
                        stroke: {
                            width: [0, 4]
                        },
                        title: {
                            text: 'Chart 30 days'
                        },
                        dataLabels: {
                            enabled: true,
                            enabledOnSeries: [1]
                        },
                        labels: dataDates,
                        xaxis: {
                            type: 'datetime'
                        },
                        yaxis: [{
                            title: {
                                text: 'Column Conversions Blog',
                            },
                        }, {
                            opposite: true,
                            title: {
                                text: 'Lines Impressions & Spend'
                            }
                        }]
                    };

                    row.child.show();
                    tr.addClass('shown');

                    var chart = new ApexCharts(document.querySelector("#chart-container-30days-" + id_tabla +
                        platform), options);
                    chart.render();

                });

        });

        //switchChange
        //$('#dashboard_table tbody').on('change', '.switch_checkbox', function () {

        $('#dashboard_table tbody').on('change', '.switch_checkbox', function() {

            var type = $(this).attr('data-type');
            var id = $(this).attr('data-itemid');
            var check = false;
            if ($(this).is(":checked")) {
                check = true;
            }

            $.ajax({
                    url: "/api/dashboard/switchsssss",
                    type: "post",
                    dataType: "json",
                    data: {
                        'id': id,
                        'entity': type,
                        'check': check
                    }
                })
                .done(function(res) {
                    if (res.success) {
                        toastr.success("Success Change");
                    }
                });
        });

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        var objetoDashboard = {};
        var type_grafica = {};
        var totales = {};
        var filas = {};
        var grender = 0;
        var responseDataAPi = null;
        let caoptions = 'impresions';
        var newReportAdded = false;
        var channelsSelected = [];

        var options = {
            chart: {
                type: 'line',
                height: 350,
            },
            series: [{
                name: "Value", //sales
                data: [30, 40, 35, 50, 49, 60, 70, 91, 136] //o aqui la data de consulta ajax
            }],
            xaxis: {
                categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999]
            },
            colors: ['#FB9A99', '#FF7F00', '#FFC626']
        }

        var chart = new ApexCharts(document.querySelector("#chart"), options);

        $('input[name="caoptions"]').click(function() {
            caoptions = $(this).val();
            //mostrarDatos(responseDataAPi['result']['data']);
            //mostrarDatosChannel(responseDataAPi['result_channels']);
            mostrarDatos(responseDataAPi);

        });

        // Click del boton UPDATE / REFRESH
        $('#d_cargar_db').click(function() {
            loading.show();
            $('.kt-dialog--loader').show();

            $.ajax({
                    url: "/api/reporting/data/v2",
                    type: "post",
                    dataType: "json",
                    data: {
                        'datos': objetoDashboard
                    }
                })
                .done(function(res) {
                    loading.hide();
                    responseDataAPi = res;
                    fillFields(res['fields_data']);
                    mostrarDatos(res);
                    $('.kt-dialog--loader').hide();
                });



        });

        //jose: no tiene sentido renderizar el chart sin haber cargado datos
        //chart.render();
        ////jose: tampoco es buena practica activar un tab desde .click de js , hay que hacerlo con un class "active"
        //$("#impresiones").click();
        //console.log('hola');

        $.ajax({
                url: "/api/reporting/entidades",
                type: "get",
                dataType: "json"
            })
            .done(function(res) {
                Object.keys(res).forEach(function(va) {
                    cargarSelect(va, res[va]);
                });
            });

        /*});*/

        function cargarSelect(entidad, datos) {
            console.log(entidad);
            $('#select_' + entidad).empty();
            if (datos != null) {
                for (i = 0; i < datos.length; i++) {
                    $('#select_' + entidad).append("<option value='" + datos[i]['id'] + "' >" + datos[i]['nameValue'] +
                        "</option>");
                }
            }
            $('#select_' + entidad).selectpicker('refresh');
        };

        function updateobjetoglobal(entidad, dato) {
            objetoDashboard[entidad] = dato;
            return true;
        }

        function allChannels() {
            channelsSelected.splice(0, channelsSelected.length)
            toggleChannel('channels', 1)
            toggleChannel('channels', 2)
            toggleChannel('channels', 3)
            toggleChannel('channels', 4)

            if ($("#channel_1").hasClass("active") &&
                $("#channel_2").hasClass("active") &&
                $("#channel_3").hasClass("active") &&
                $("#channel_4").hasClass("active")) {
                $(".channel-buton").removeClass("active")
            } else {
                $(".channel-buton").addClass("active")
                channelsSelected.push([1, 2, 3, 4]);
                mostrarDatos(responseDataAPi);
            }

        }

        function toggleChannel(entidad, id_platform) {
            // UI
            if ($("#channel_" + id_platform).hasClass("active")) {
                let index = channelsSelected.indexOf(id_platform);
                if (index > -1) {
                    channelsSelected.splice(index, 1);
                }
                $("#channel_" + id_platform).removeClass("active")
            } else {
                channelsSelected.push(id_platform);
                $("#channel_" + id_platform).addClass("active")
            }

            //console.log(channelsSelected);
            mostrarDatos(responseDataAPi);
        }

        function fillFields(data) {
            $("#p_impresions").html(data[0]['impresions']);
            $("#p_spend").html(data[0]['spend']);
            $("#p_clicks").html(data[0]['clicks']);
            $("#p_ctr").html(data[0]['ctr']);
            $("#p_ecpm").html(data[0]['ecpm']);
        }

        function parseFloat2Decimals(value) {

            let decimals = 2;

            if (caoptions == 'impresions') {
                decimals = 0;
            }

            if (caoptions == 'clicks') {
                decimals = 0;
            }

            return parseFloat(parseFloat(value).toFixed(decimals));
        }


        function groupDataBy(datos, group) {

            let copyData = JSON.parse(JSON.stringify(datos));
            let datosSemanales = {};
            let dayDate = null;
            let weekOfYear = null;
            let KeyValue = null;

            $.each(copyData, function(key, value) {

                switch (group) {
                    case 'weekly': // weekly
                        keyValue = value.yearweek;
                        break;
                    case 'monthly': //monthly
                        keyValue = value.yearmonth;
                        break;
                }

                if (keyValue in datosSemanales) {
                    datosSemanales[keyValue] = {
                        x: keyValue,
                        y_clicks: parseFloat2Decimals(datosSemanales[keyValue]['y_clicks']) +
                            parseFloat2Decimals(value.y_clicks),
                        y_ctr: parseFloat2Decimals(datosSemanales[keyValue]['y_ctr']) + parseFloat2Decimals(
                            value.y_ctr),
                        y_ecpm: parseFloat2Decimals(datosSemanales[keyValue]['y_ecpm']) + parseFloat2Decimals(
                            value.y_ecpm),
                        y_impresions: parseFloat2Decimals(datosSemanales[keyValue]['y_impresions']) +
                            parseFloat2Decimals(value.y_impresions),
                        y_spend: parseFloat2Decimals(datosSemanales[keyValue]['y_spend']) + parseFloat2Decimals(
                            value.y_spend)
                    };
                } else {
                    datosSemanales[keyValue] = [];
                    datosSemanales[keyValue] = {
                        x: keyValue,
                        y_clicks: parseFloat2Decimals(value.y_clicks),
                        y_ctr: parseFloat2Decimals(value.y_ctr),
                        y_ecpm: parseFloat2Decimals(value.y_ecpm),
                        y_impresions: parseFloat2Decimals(value.y_impresions),
                        y_spend: parseFloat2Decimals(value.y_spend)
                    };
                }
            });

            return datosSemanales;
        }

        function groupChannelDataBy(datos, group) {
            //let copyData = Object.assign({}, datos);
            let copyData = JSON.parse(JSON.stringify(datos));

            let datosSemanalesAgrup = {};
            let dayDate = null;
            let weekOfYear = null;
            let KeyValue = null;
            let pos = 0;

            $.each(copyData, function(key, channn) {

                if (channelsSelected.indexOf(channn.platform.id) >= 0) {

                    $.each(channn.data, function(key, value) {

                        switch (group) {
                            case 'weekly': // weekly
                                keyValue = value.yearweek;
                                break;
                            case 'monthly': //monthly
                                keyValue = value.yearmonth;
                                break;
                        }

                        if (keyValue in datosSemanalesAgrup) {
                            datosSemanalesAgrup[keyValue] = {
                                x: keyValue,
                                y_clicks: parseFloat2Decimals(datosSemanalesAgrup[keyValue][
                                    'y_clicks'
                                ]) + parseFloat2Decimals(value.y_clicks),
                                y_ctr: parseFloat2Decimals(datosSemanalesAgrup[keyValue]['y_ctr']) +
                                    parseFloat2Decimals(value.y_ctr),
                                y_ecpm: parseFloat2Decimals(datosSemanalesAgrup[keyValue]['y_ecpm']) +
                                    parseFloat2Decimals(value.y_ecpm),
                                y_impresions: parseFloat2Decimals(datosSemanalesAgrup[keyValue][
                                    'y_impresions'
                                ]) + parseFloat2Decimals(value.y_impresions),
                                y_spend: parseFloat2Decimals(datosSemanalesAgrup[keyValue]['y_spend']) +
                                    parseFloat2Decimals(value.y_spend)
                            };
                        } else {
                            datosSemanalesAgrup[keyValue] = [];
                            datosSemanalesAgrup[keyValue] = {
                                x: keyValue,
                                y_clicks: parseFloat2Decimals(value.y_clicks),
                                y_ctr: parseFloat2Decimals(value.y_ctr),
                                y_ecpm: parseFloat2Decimals(value.y_ecpm),
                                y_impresions: parseFloat2Decimals(value.y_impresions),
                                y_spend: parseFloat2Decimals(value.y_spend)
                            };
                        }

                    });

                    channn.data = datosSemanalesAgrup;
                    datosSemanalesAgrup = {};
                    pos++;
                }
            });

            return copyData;
        }

        function mostrarDatos(datos) {

            loading.show();
            $('.kt-dialog--loader').show();

            //let datosCopii = Object.assign({}, datos);
            let datosCopii = JSON.parse(JSON.stringify(datos));

            $.each(totales, function(key, value) {
                $('#p_' + key).text(value);
            });

            let filas = null;
            let filasChannels = null;

            switch (objetoDashboard.frecuencia) {
                case '1': // daily
                    console.log("daily");
                    filas = datos['result']['data'];
                    filasChannels = datos['result']['data_channels'];
                    break;
                case '7': //weekly
                    filas = groupDataBy(datos['result']['data'], 'weekly');
                    filasChannels = groupChannelDataBy(datos['result']['data_channels'], 'weekly');
                    break;
                case '30': //monthly
                    filas = groupDataBy(datos['result']['data'], 'monthly');
                    filasChannels = groupChannelDataBy(datos['result']['data_channels'], 'monthly');
                    break;
            }

            cargarGrafica(caoptions, filas, filasChannels); //ojo aqui van las preferencias
            loading.hide();
            $('.kt-dialog--loader').hide();
        }

        function capitalizeWord(name) {

            return name.charAt(0).toUpperCase() + name.slice(1)

        }

        //se cargar los datos con el nombre de la caja como parámetro
        function cargarGrafica(parametro, filas2, filasChannels2) {


            var arrSeries = [];
            var arrYaxis = [];

            var datosChart = [];
            var categoria = [];

            $.each(filas2, function(key, value) {
                datosChart.push(parseFloat2Decimals(value['y_' + parametro]));
                categoria.push(value['x']);
            });

            if (grender == 0) {
                chart.render();
                grender = 1;
            }

            // sumamos al array para el armado
            arrSeries.push({
                data: datosChart,
                name: 'Total:'
            });
            arrYaxis.push({
                title: {
                    text: capitalizeWord(caoptions)
                }
            });

            // tenemos chart de todas las plataforms sumadas
            // vamos a procesar el chart por channel
            var y_valores = [];

            $.each(filasChannels2, function(key, value2) {
                if (channelsSelected.indexOf(value2.platform.id) >= 0) {
                    y_valores = [];
                    $.each(value2.data, function(key, valueData2) {
                        y_valores.push(parseFloat2Decimals(valueData2['y_' + caoptions]));
                    });
                    arrSeries.push({
                        data: y_valores,
                        name: capitalizeWord(caoptions) + ' ' + value2.platform.name
                    });
                }
            });

            arrYaxis.push({
                title: {
                    text: caoptions + " by platform"
                },
                opposite: true
            });

            chart.updateSeries(arrSeries);

            chart.updateOptions({
                xaxis: {
                    categories: categoria
                },
                yaxis: arrYaxis
            });

            loading.hide();

        }

        function mostrarDatosChannel(datos) {

            console.log("nothing done");


        }

        function duplicate(typeDuplicating) {

            var data = {};
            data.items = [];

            $(".duplicate:checked").each(function(index) {
                var temp = {};

                temp.type = $(this).attr('data-type');
                temp.itemid = $(this).attr('data-itemid');
                temp.campaign = $(this).attr('data-campaign');

                data.items.push(temp);
            });

            console.log(typeDuplicating);

            if (typeDuplicating == 'campaign') {
                data.new_campaign = $('#select_duplicate_customer').val();
            } else {
                data.new_campaign = $('#select_duplicate_campaigns').val();
            }


            console.log(data);

            var url = 'api/entity/duplicate';

            $.getJSON(url, data, function(response) {
                //console.log(response);
                if (response.success) {
                    table2.ajax.reload();

                    $('#duplicate_button').hide();
                    $('#view_last_button').show();
                    $('.duplicate').prop('checked', false);

                    toastr.success("Success Change");
                }
            });

        }

        $(document).ready(function() {

            updateobjetoglobal('periodo', jQuery('#periodo').val());
            updateobjetoglobal('date_picker_1', jQuery('#date_picker_1').val());
            updateobjetoglobal('date_picker_2', jQuery('#date_picker_2').val());
            updateobjetoglobal('frecuencia', jQuery('.r_frecuencia').val());
            updateobjetoglobal('grafica', jQuery('.r_grafica').val());
            updateobjetoglobal('clients', jQuery('#select_clients').val());
            updateobjetoglobal('platforms', jQuery('#select_platforms').val());
            updateobjetoglobal('campaigns', jQuery('#select_campaigns').val());


            $('#d_cargar_db').click();

            /* Boton guardar (editar opciones) dashboard (configuracion) si es new Report abre un modal para solicitar el nombre */
            $('#d_guardar').click(function() {
                console.log(objetoDashboard)
                updateobjetoglobal('periodo', jQuery('#periodo').val());
                updateobjetoglobal('date_picker_1', jQuery('#date_picker_1').val());
                updateobjetoglobal('date_picker_2', jQuery('#date_picker_2').val());
                updateobjetoglobal('frecuencia', jQuery('.r_frecuencia').val());
                updateobjetoglobal('grafica', jQuery('.r_grafica').val());
                updateobjetoglobal('clients', jQuery('#select_clients').val());
                updateobjetoglobal('platforms', jQuery('#select_platforms').val());
                updateobjetoglobal('campaigns', jQuery('#select_campaigns').val());
                updateobjetoglobal('default', $('#dashboaard_setreport_default').is(":checked"));


                console.log(objetoDashboard)

                var db_id = ($('#s_dashboards').val());

                if (db_id != -1) {
                    loading.show();
                    var ruta = "/api/dashboard/save/" + db_id;
                    $.ajax({
                            url: ruta,
                            type: "post",
                            dataType: "json",
                            data: {
                                'datos': objetoDashboard,
                            }
                        })
                        .done(function(res) {
                            //$('#d_cargar_db').click();
                            loading.hide();
                        });
                    loading.hide();
                } else {
                    $('#kt_modal_nombre').modal('show');
                }
            });

            /* Rutina para guardar un dashboard nuevos */
            $('#guardaNombreDashboaard').click(function() {
                loading.show();

                var nombre = $('#dashboaard-name').val();
                updateobjetoglobal('periodo', jQuery('#periodo').val());
                updateobjetoglobal('date_picker_1', jQuery('#date_picker_1').val());
                updateobjetoglobal('date_picker_2', jQuery('#date_picker_2').val());
                updateobjetoglobal('frecuencia', jQuery('.r_frecuencia').val());
                updateobjetoglobal('grafica', jQuery('.r_grafica').val());
                updateobjetoglobal('clients', jQuery('#select_clients').val());
                updateobjetoglobal('platforms', jQuery('#select_platforms').val());
                updateobjetoglobal('campaigns', jQuery('#select_campaigns').val());

                if (nombre != '') {
                    $.ajax({
                            url: "/api/dashboard/save",
                            type: "post",
                            dataType: "json",
                            data: {
                                'datos': objetoDashboard,
                                'nombre': nombre,
                                'type': 'home'
                            }
                        })
                        .done(function(res) {
                            $("#s_dashboards").append(new Option(res.name, res.id));
                            $('#s_dashboards option[value="' + res.id + '"]').prop('selected', true);
                            $('#d_cargar_db').click();
                            loading.hide();
                            //mostrar mensaje de datos guardados
                        });
                    $('#kt_modal_nombre').modal('hide');
                } else {
                    alert("Must fill name to save report.");
                }
            });

            $('#periodo').change(function() {
                updateobjetoglobal('periodo', jQuery(this).val());
            });

            /* Seleccion de Reporte , evento onChange */
            $('#s_dashboards').on('change', function() {

                var db_id = parseInt($(this).val());

                if (db_id == "-1") {
                    $('#periodo').show();
                    $('#d_guardar').show();
                } else {
                    $('#d_guardar').hide();
                    loading.show();
                    $.ajax({
                            url: "/api/dashboard/" + db_id,
                            dataType: "json"
                        })
                        .done(function(res) {
                            let data = jQuery.parseJSON(res[0].configuration);
                            $('#periodo').hide();
                            //$('#periodo').selectpicker('val', data.periodo);
                            $('#date_picker_1').selectpicker('val', data.date_picker_1);
                            $('#date_picker_2').selectpicker('val', data.date_picker_2);
                            $('.r_frecuencia').selectpicker('val', data.frecuencia);
                            $('.r_grafica').selectpicker('val', data.grafica);
                            $('#select_clients').selectpicker('val', data.clients);
                            $('#select_platforms').selectpicker('val', data.platforms);
                            $('#select_campaigns').selectpicker('val', data.campaigns);

                            $('#d_cargar_db').click();
                            loading.hide();
                        });


                }
            })

            $(".date-picker").on("change", function() {
                updateobjetoglobal('periodo', jQuery('#periodo').val());
                updateobjetoglobal('date_picker_1', jQuery('#date_picker_1').val());
                updateobjetoglobal('date_picker_2', jQuery('#date_picker_2').val());
                updateobjetoglobal('frecuencia', jQuery('.r_frecuencia').val());
                updateobjetoglobal('grafica', jQuery('.r_grafica').val());
                updateobjetoglobal('clients', jQuery('#select_clients').val());
                updateobjetoglobal('platforms', jQuery('#select_platforms').val());
                updateobjetoglobal('campaigns', jQuery('#select_campaigns').val());

            });

            $('.r_frecuencia').on('click', function() {
                updateobjetoglobal('frecuencia', jQuery(this).val());
                console.log(responseDataAPi);
                mostrarDatos(responseDataAPi);
            });

            /*$('.r_grafica').on('click', function () {
                //updateobjetoglobal('grafica', jQuery(this).val());
                //cargarGrafica(jQuery(this).val());
            });*/

            $('.selectpicker').on('changed.bs.select', function(e, clickedIndex, isSelected, previousValue) {
                updateobjetoglobal(jQuery(this).data('entidad'), jQuery(this).val());
            });

            $("#periodo").on("change", function() {
                if ($(this).find(":selected").attr("id") === "periodo_") {
                    $("#date_picker").removeClass("d-none");
                } else {
                    $("#date_picker").addClass("d-none");
                }
                $('#d_cargar_db').click();
            });


            $.fn.editable.defaults.mode = 'inline';
            $.fn.editableform.buttons =
                '<button type="submit" class="btn btn-primary btn-icon editable-submit"><i class="fas fa-check"></i></button><button type="button" class="btn btn-basic btn-icon editable-cancel"><i class="fas fa-times"></i></button>';

            $('#duplicate_button').click(function() {

                var flag = false;
                $(".duplicate:checked").each(function(index) {
                    if ($(this).attr('data-type') != 'campaigns') {
                        flag = true;
                    }
                });

                if (flag) {
                    $("#select_duplicate_campaigns").val('default');
                    $("#select_duplicate_campaigns").selectpicker("refresh");
                    $('#kt_modal_duplicate').modal('show');
                } else {
                    //duplicate();
                    $("#select_duplicate_customer").val('default');
                    $("#select_duplicate_customer").selectpicker("refresh");
                    $('#kt_modal_duplicate_customer_campaign').modal('show');
                }
            });

            $('#select_duplicate_campaigns').change(function() {
                $('#kt_modal_duplicate').modal('hide');
                duplicate('other');
            });

            $('#select_duplicate_same_campaigns').click(function() {
                $('#kt_modal_duplicate').modal('hide');
                duplicate('other');
            });



            $('#select_duplicate_customer').change(function() {
                $('#kt_modal_duplicate_customer_campaign').modal('hide');
                duplicate('campaign');
            });

            $('#select_duplicate_same_customer').click(function() {
                $('#kt_modal_duplicate_customer_campaign').modal('hide');
                duplicate('campaign');
            });



            $('#select_clients').change(function() {
                change_client($(this).val());
            });

            function change_client(client) {
                $.ajax({
                    type: "GET",
                    url: "/home/" + client,
                    dataType: "json",
                    success: function(data) {
                        $("#select_campaigns").empty();
                        $.each(data, function(key, registro) {
                            $("#select_campaigns").append('<option value=' + registro.id + '>' +
                                registro.name + '</option>');
                            //$(".selectcamp").prop('disabled', false);
                            $("button[data-id='select_campaigns']").removeClass("disabled");
                        });
                        $("#select_campaigns").selectpicker('refresh');
                    },
                    error: function(data) {
                        $("#select_campaigns").empty();
                        //$(".selectcamp").prop('disabled', true);
                        $("button[data-id='select_campaigns']").addClass("disabled");
                    }
                })
            }

            $(document).ajaxStart(function() {
                loading.show();
            });

            $(document).ajaxStop(function() {
                loading.hide();
            });

        });

    </script>
@endsection
