@extends('template.general')

<link href="../assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link href="/css/custom.css" rel="stylesheet" type="text/css" />
<link href="/css/reporting.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .chosen-container {
        width: 100% !important;
    }

</style>
@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17">
                        <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"></path>
                    </svg>
                </span>
                <h3 class="kt-portlet__head-title">
                    {{ $title }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <?php
                            /*
                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> Export
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">Choose an option</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">Print</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-copy"></i>
                                            <span class="kt-nav__link-text">Copy</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">Excel</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-text-o"></i>
                                            <span class="kt-nav__link-text">CSV</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                            <span class="kt-nav__link-text">PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            */
                            ?>
                        </div>
                        @if (Auth::user()->getRolePermission('crear_usuarios'))
                            <button type="button" class="btn btn-info btn-icon-left" data-toggle="modal"
                                data-target="#exampleModalCenter">
                                <svg id="_27_Icon_plus" data-name="27) Icon/plus" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                    <defs>
                                      <clipPath id="clip-path">
                                        <path id="_Icon_Сolor" data-name="🎨 Icon Сolor" d="M15,7H9V1A1,1,0,0,0,7,1V7H1A1,1,0,0,0,1,9H7v6a1,1,0,0,0,2,0V9h6a1,1,0,0,0,0-2" transform="translate(4 4)" fill="#FFFFFF"/>
                                      </clipPath>
                                    </defs>
                                    <g id="Group_313" data-name="Group 313">
                                      <path id="_Icon_Сolor-2" data-name="🎨 Icon Сolor" d="M15,7H9V1A1,1,0,0,0,7,1V7H1A1,1,0,0,0,1,9H7v6a1,1,0,0,0,2,0V9h6a1,1,0,0,0,0-2" transform="translate(4 4)" fill="#FFFFFF"/>
                                    </g>
                                  </svg>
                                  
                                {{ $newElement }}
                            </button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-checkable" id="kt_table_1"
                data-page-length='10'>
                <thead style="text-transform: capitalize;">
                    <tr>
                        @foreach ($columns as $column)
                            <th>{{ $column }}</th>
                        @endforeach
                    </tr>
                </thead>
            </table>
            <!--end: Datatable -->
        </div>

        <!-- Modal-->
        <div class="modal fade" id="exampleModalCenter" data-backdrop="static" tabindex="-1" role="dialog"
            aria-labelledby="staticBackdrop" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Customer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">

                        @include('template.customersmodal')

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-basic font-weight-bold"
                            data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary font-weight-bold" id="save_new_customer">Save</button>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection

@section('customscript')
    <!--begin::Page Vendors(used by this page) -->
    <script src="../assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <!-- Con este script es que se hace la carga de la data-->
    <script src="../assets/app/custom/general/crud/datatables/extensions/scroller.js" type="text/javascript"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script type="text/javascript">

        var tableCustomers = $('#kt_table_1').DataTable({
            //dom: 'Bfrtip',
            //select: false,
            processing: true,
            oLanguage: {
                //sProcessing: "<div id='dashboard_table_processing' class='dataTables_processing'><img src='https://app.thesocialaudience.com/img/loaderIcon.gif' style='height: 22px; margin-right: 10px;''></div><div> Loading ...</div>"
            },
            serverSide: true,
            aLengthMenu: [
                [5, 10, 25, 50],
                [5, 10, 25, 50]
            ],
            ajax: "{{ $api }}",
            columns: [
                @foreach ($columns as $column)
                    {
                    data: '{{ $column }}',
                    orderable: false,
                    @if ($column == 'id')
                        visible : false,
                    @else
                        visible : true,
                    @endif
                    @if ($column != 'Actions')
                        responsivePriority: -1,
                    @endif
                    },
                @endforeach
            ],
            columnDefs: [{
                    targets: 2,
                    title: 'Name',
                    render: function(data, type, full, meta) {
                        //console.log(full);
                        return '<a href="#" data-type="text" data-pk="' + full.id +
                            '" data-value="' + full.name +
                            '" class="editable editable-click customer_name" data-original-title="" title="">' +
                            data + '</a>&ensp;'+
                            '<svg class="editselect" data-pk="' + full.id + '" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"></path> </svg>';
                            
                    }
                },
                {
                    targets: 3,
                    title: 'Isdefault',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        if (data == 'N') {
                            return '<span class="switch switch-sm"><label class="switch"><input type="checkbox" name="check_default_customer[]" id="check_' + full.id + '" onclick="javascript:changeDefault(\'' + full.id.trim() + '\');"><span></span></label></span>';
                        } else {
                            return '<span class="switch switch-sm"><label class="switch"><input type="checkbox" name="check_default_customer[]" id="check_' + full.id + '" onclick="javascript:changeDefault(\'' + full.id + '\');" checked><span></span></label></span>';
                        }
                    }
                },
                {
                    targets: 4,
                    title: 'status',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        switch (data) {
                            case 'active':
                                return '<span class="switch switch-sm"><label class="switch"><input type="checkbox" name="Status" checked alt="Active"><span></span></label>';
                                break;
                            case 'inactive':
                                return '<span class="switch switch-sm"><label class="switch"><input type="checkbox" name="Status" checked  alt="Inactive"><span></span></label>';
                                break;
                            case 'stopped':
                                return '<span class="switch switch-sm"><label class="switch"><input type="checkbox" name="Status" checked  alt="Stopped"><span></span></label>';
                                break;
                        }
                    }
                },
                {
                    targets: 0,
                    title: 'Actions',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '<a href="/{{ $base }}/' + full.id + '/edit" class="btn btn-icon" title="Editar {{ $base }}" ><svg id="_27_Icon_edit-2" data-name="27) Icon/edit-2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,19.977H1a1,1,0,0,1,0-2H15a1,1,0,0,1,0,2Zm-14-4a1,1,0,0,1-1-1.09l.38-4.17A1.972,1.972,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.926,1.926,0,0,1,.069,2.715l-9,9a1.977,1.977,0,0,1-1.213.57l-4.171.379ZM7.984,5.3h0L2.367,10.918,2.1,13.873,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(4 2)" fill="#222b45"/> </svg></a>';
                    }
                }
            ]
        });


        $.fn.editable.defaults.mode = 'inline'; 
        $.fn.editableform.buttons = '<button type="submit" class="btn btn-primary btn-icon editable-submit"><i class="fas fa-check"></i></button><button type="button" class="btn btn-basic btn-icon editable-cancel"><i class="fas fa-ban"></i></button>';

        tableCustomers.on('draw.dt', function() {

            $('.customer_name').editable({
                type: 'text',
                url: '/customers/edit/name', 
            });

        });

    </script>

    <script src="/assets/app/custom/general/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="/js/chozen.jquery.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {

            $('.chosen-select').chosen();

            $('.check').click(function() {
                console.log($(this).attr('rrss'));
                var rrss = $(this).attr('rrss');

                if ($('#rrss_' + rrss).is(':checked')) {
                    $('#ads_' + rrss).prop('disabled', true).trigger("chosen:updated");
                    $('#properties_' + rrss).prop('disabled', true).trigger("chosen:updated");
                    $('#fee_' + rrss).prop('disabled', true);
                    $('#settings_' + rrss).hide();
                } else {
                    $('#ads_' + rrss).prop('disabled', false).trigger("chosen:updated");
                    $('#properties_' + rrss).prop('disabled', false).trigger("chosen:updated");
                    $('#fee_' + rrss).prop('disabled', false);
                    $('#settings_' + rrss).show();
                }

            });


            $('#ads_facebook').prop('disabled', true).trigger("chosen:updated");
            $('#properties_facebook').prop('disabled', true).trigger("chosen:updated");
            $('#ads_twitter').prop('disabled', true).trigger("chosen:updated");
            $('#properties_twitter').prop('disabled', true).trigger("chosen:updated");
            $('#ads_linkedin').prop('disabled', true).trigger("chosen:updated");
            $('#properties_linkedin').prop('disabled', true).trigger("chosen:updated");
            $('#ads_instagram').prop('disabled', true).trigger("chosen:updated");
            $('#properties_instagram').prop('disabled', true).trigger("chosen:updated");
            $('#ads_snapchat').prop('disabled', true).trigger("chosen:updated");
            $('#properties_snapchat').prop('disabled', true).trigger("chosen:updated");
            $('#ads_amazon').prop('disabled', true).trigger("chosen:updated");
            $('#properties_amazon').prop('disabled', true).trigger("chosen:updated");
            $('#ads_adwords').prop('disabled', true).trigger("chosen:updated");
            $('#properties_adwords').prop('disabled', true).trigger("chosen:updated");

            $('#fee_facebook').prop('disabled', true);
            $('#fee_twitter').prop('disabled', true);
            $('#fee_linkedin').prop('disabled', true);
            $('#fee_instagram').prop('disabled', true);
            $('#fee_snapchat').prop('disabled', true);
            $('#fee_amazon').prop('disabled', true);
            $('#fee_adwords').prop('disabled', true);


            $('#save_new_customer').click(function() {

                $.ajax({
                        url: '/customers/create',
                        data: new FormData(document.getElementById('new_user_form')),
                        contentType: false,
                        dataType: "json",
                        processData: false,
                        beforeSend: function() {
                            $('#save_new_customer').html(
                                '<img src="https://app.thesocialaudience.com/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">'
                            );
                        },
                    })
                    .done(function(response) {

                        $('#save_new_customer').html('Save')

                        if (response.status) {
                            $('#exampleModalCenter').modal('hide')
                            toastr.success(response.message, "Customers");
                            document.getElementById("new_user_form").reset();
                            tableCustomers.ajax.reload();
                        } else {
                            toastr.error(response.message, "Customers");
                        }

                    });


            }); // end editcustomer on click


        });

        function changeDefault(publicid) {

            //$("input[name='check_default_customer[]']").each(function (index, obj) {
            //    $('#check_' + obj.id).prop('checked', false);
           // }); 

            $('#check_' + publicid).prop('checked', true);


            dataForm = new FormData();
            dataForm.append('pk', publicid )

            $.ajax({
                        url: '/customers/edit/defaultCustomer',
                        data: dataForm,
                        contentType: false,
                        type: "POST",
                        dataType: "text",
                        processData: false,
                        beforeSend: function() {
                            
                        },
                    })
                    .done(function(response) {
                            toastr.success(response, "Customers");
                            tableCustomers.ajax.reload();

                    });


        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>

@endsection
