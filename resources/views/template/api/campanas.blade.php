<div class="row">
    <div class="col-sm-12" style="barckgroung:white;box-shadow:0px 0px 5px lightgray">
        <h4>{{ $title }}</h4>
        <table class="table table-striped- table-checkable dataTable no-footer withcampaings" id="withcampaings" data-page-length="5" role="grid" aria-describedby="kt_table_1_info" style="width: 1393px;">
            <thead>
                <tr>												
                    @foreach ($columns as $column)
                        <th>{{ $column }}</th>
                    @endforeach
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    $("#withcampaings").DataTable({
        processing: true,
        serverSide: true,
        aLengthMenu: [[5, 10], [5, 10]],
        ajax: "{{ $api }}",
        columns: [
                    @foreach ($columns as $column)
                        {
                            data: '{{ $column }}',
                            orderable: false,
                            @if ( $column == 'id' )
                                visible : false,
                            @else
                                visible : true,
                            @endif 
                            @if ( $column != 'Actions' )
                                responsivePriority: -1,
                            @endif 
                        },
                    @endforeach
                ],
                columnDefs: [
                    {
                        targets: -1,
                        title: 'Social Network',
                        orderable: false,
                        render: function(data, type, full, meta){
                        return `
                            <ul class="list-group list-group-horizontal list-inline" >
                                <li class="list-inline-item" >
                                    <a class="dropdown-item" href="" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Editar {{ $base }}" ><i class="fab fa-facebook-square"></i></a>
                                </li>
                                <li class="list-inline-item" >
                                    <a class="dropdown-item" href="" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Eliminar {{ $base }}" ><i class="fab fa-twitter"></i></a>
                                </li>	
                                <li class="list-inline-item" >
                                    <a class="dropdown-item" href="" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Eliminar {{ $base }}" ><i class="fab fa-linkedin"></i></a>
                                </li>	
                            </ul>
                        `;
                    }
                }
            ]
        }
    );
</script>