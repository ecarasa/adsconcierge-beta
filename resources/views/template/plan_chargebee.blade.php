@extends('template.general')

		<style>
			.pricing-table {
				text-align: center;
				background-color: #fcfcfc;
				font-family: 'Montserrat', sans-serif;
				text-align: center;
			}


.pricing-item {
  border-radius: 3px;
  display: inline-block;
  width: 260px;
  height: auto;
  background: #fff;
  margin: 20px;
  vertical-align: top;
  position: relative;
  overflow: hidden;
  box-shadow: 0 1.5px 4px rgba(0, 0, 0, 0.24), 0 1.5px 6px rgba(0, 0, 0, 0.12);
  -webkit-transition: all .2s cubic-bezier(.3, .6, .2, 1.8);
  transition: all .2s cubic-bezier(.3, .6, .2, 1.8);
			}
  .pricing-item:hover {
      -webkit-transform: scale(1.04);
      -ms-transform: scale(1.04);
      transform: scale(1.04);
      box-shadow: 0 3px 12px rgba(0, 0, 0, 0.23), 0 3px 12px rgba(0, 0, 0, 0.16);
  }
  .pricing-title{
    width: 100%;
    color: white;
    display: block;
    position: relative;
    background: #0074D9;
    padding: 7px;
    font-weight: bold;
    font-size: 20px;
    //background: #FF4136;
    //background: #2ECC40;
  }
  .pricing-item.pricing-featured .pricing-title{
    background: #FF4136;
  }



/* pricing-value */
.pricing-value {
  width: 180px;
  height: 180px;
  padding-top: 46px;
  border-radius: 50%;
  color: #fff;
  font-size: 46px;
  font-weight: 300;
  margin: 10px auto;
}
.pricing-value .smallText {
  font-size: 14px;
}
.pricing-value .undertext {
  display: block;
  font-size: 16px;
}
.pricing-item .pricing-value {
  background: #0074D9;
  border: 2px solid #0074D9;
}
.pricing-item.pricing-featured .pricing-value{
  background: #FF4136;
  border: 2px solid #FF4136;
}

/* List */
.pricing-item .pricing-features {
  margin: 10px 0;
  padding: 0;
  list-style: none;
			}
  .pricing-item .pricing-features li {
    display: block;
    width: 90%;
    height: 40px;
    line-height: 40px;
    font-size: 15px;
    font-weight: 400;
    border-bottom: 1px solid rgba( 0, 0, 0, 0.2);
    margin: 0 auto;
    
  }
	
	.pricing-item .pricing-features li .keywords {
      font-weight: bold;
    }

.button {
  width: 140px;
  height: 38px;
  font-weight: 300;
  font-size: 16px;
  line-height: 50px;
  margin: 0 auto;
  background: #fff;
  color: #0074D9;
  border: 2px solid #0074D9;
  cursor: pointer;
  padding: 6px;
	margin-bottom: 10px;
  vertical-align: middle;
  -webkit-transition: .2s ease-out;
  -moz-transition: .2s ease-out;
  -o-transition: .2s ease-out;
  -ms-transition: .2s ease-out;
  transition: .2s ease-out;
  /*-webkit-box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2), 0 2px 3px rgba(0, 0, 0, 0.05);
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2), 0 2px 3px rgba(0, 0, 0, 0.05);*/
}
			
.button:hover{
    background: #0074D9;
    color: #fff;
    border: none;
    -webkit-box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15);
    box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15);
  }
.pricing-item.pricing-featured .button{
  color: #FF4136;
  border: 2px solid #FF4136;
}
.pricing-item.pricing-featured .button:hover{
    background: #FF4136;
    color: #fff;
  }
.selected {
  z-index: 10;
  width: 180px;
  height: 32px;
  padding: 0 20px;
  font-size: 12px;
  line-height: 25px;
  text-align: center;
  color: #fff;
  font-weight: bold;
  box-shadow: 0px 2px 5px #888888;
  background: gold;
  border-top: 5px solid gold;
  border-bottom: 5px solid gold;
  //background: #palegoldenrod;
  transform: rotate(35deg);
  position: absolute;
  right: -47px;
  top: 17px;
}
			
.current_plan {
  width: 140px;
  height: 38px;
  font-weight: 600;
  font-size: 16px;
  margin: 0 auto;
  background: #fff;
  color: #0074D9;
  padding: 6px;
  margin-bottom: 10px;
  vertical-align: middle;
  -webkit-transition: .2s ease-out;
  -moz-transition: .2s ease-out;
  -o-transition: .2s ease-out;
  -ms-transition: .2s ease-out;
  transition: .2s ease-out;
  /*-webkit-box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2), 0 2px 3px rgba(0, 0, 0, 0.05);
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2), 0 2px 3px rgba(0, 0, 0, 0.05);*/
}			
		</style>
@section('content')						
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17">
            <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"></path>
          </svg>
				</span>
				<h3 class="kt-portlet__head-title">
				{{ $title }}
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
				<div class="pricing-table">

				  <div class="pricing-item">
					<div class="pricing-title">
					  Free Plan
					</div>
					<div class="pricing-value">Free</span>
					</div>
					<ul class="pricing-features">
					  <li><span class="keywords">1GB</span> Armazenamento</li>
					  <li>Banda <span class="keywords">ilimitada</span></li>
					  <li><span class="keywords">10 Contas</span> de email</li>
					  <li><span class="keywords">50gb</span> Transferência</li>
					</ul>
					@if($plan != "cbdemo_free")
					  	<a class="button" href="javascript:void(0)" data-cb-type="checkout" data-cb-plan-id="cbdemo_free" >subscribe free plan </a>
					@else
						<div class="current_plan">Current plan </div>
					@endif
				  </div>


				  <div class="pricing-item pricing-featured">
					<div class='selected'>Grow plan</div>
					<div class="pricing-title">
					  PRO
					</div>
					<div class="pricing-value">R$39.<span class="smallText">90</span>
					  <span class="undertext">/Mês</span>
					</div>
					<ul class="pricing-features">
					  <li><span class="keywords">5GB</span> Armazenamento</li>
					  <li>Banda <span class="keywords">ilimitada</span></li>
					  <li><span class="keywords">100 Contas</span> de email</li>
					  <li><span class="keywords">100gb</span> Transferência</li>
					</ul>
					  @if($plan != "cbdemo_grow")
					 	<a class="btn btn-danger mb-3" href="javascript:void(0)" data-cb-type="checkout" data-cb-plan-id="cbdemo_grow" >subscribe grow plan</a> {{-- button --}}
					  @else
						<div class="current_plan">Current plan </div>
					  @endif
				  </div>

				</div>
			
		</div>
	</div>						
@endsection

@section('customscript')
<!--begin::Page Vendors(used by this page) -->
		<script src="../assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		<!-- Con este script es que se hace la carga de la data-->
		<script src="../assets/app/custom/general/crud/datatables/extensions/scroller.js" type="text/javascript"></script>
		<script src="https://js.chargebee.com/v2/chargebee.js" data-cb-site="adsconcierge" ></script>

		<script type="text/javascript">
			var cbInstance = Chargebee.init({
				site: "adsconcierge",
				publishableKey: "live_UhGIHjzvcdOWTwEU28Rt1Mk0khGjVGk40"
			});
			
		</script>

@endsection