@extends('template.general')


@section('title', 'Client')
<link href="/css/clients.css" rel="stylesheet" type="text/css" />
<link href="/css/chozen.css" rel="stylesheet" type="text/css" />
<link href="/css/custom.css" rel="stylesheet" type="text/css" />
<link href="/css/reporting.css" rel="stylesheet" type="text/css" />

@section('content')	
<style type="text/css">

	.kt-form.nw_usr{
		width: 50%;
		margin-left: auto;
		margin-right: auto;
	}

	/* Boton crear usuario*/

	button.btn-success.creat{
		background-color: #46B346 !important;
		border-color: #46B346 !important;
	}

</style>
<!--begin::Form-->
<form class="kt-form nw_usr" id="edit_form_customer">	
	{{ csrf_field() }}
	<input type="hidden" value="{{ isset($client->public_id) ? $client->public_id : '' }}" name="client_id" id="client_id" />
	
	<div class="kt-portlet__body">
		<div class="form-group form-group-last">
			<div class="alert alert-secondary" role="alert">
				<div class="alert-icon">
					<svg id="_27_Icon_person" data-name="27) Icon/person" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
						<path d="M13,18a1,1,0,0,1-1-1A5,5,0,1,0,2,17a1,1,0,1,1-2,0,7,7,0,0,1,14,0A1,1,0,0,1,13,18ZM7,8a4,4,0,1,1,4-4A4.005,4.005,0,0,1,7,8ZM7,2A2,2,0,1,0,9,4,2,2,0,0,0,7,2Z" transform="translate(5 3)" fill="#222b45"/>
					</svg>
				</div>
				<div class="alert-text">
				@if ( is_null( $client ) || $client == "")
					Create new Client
				@else
					Edit <b>{{ $client->name }}</b>
				@endif
				</div>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-warning alert-elevate" role="alert">
				<div class="alert-icon">
					<i class="flaticon-warning kt-font-brand"></i>
				</div>
				<div class="alert-text">											
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			</div>
		@endif
		<!-- {% dd(get_defined_vars()) %} -->
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label" >Name</label>
			<div class="col-10">
				<input class="form-control @error('name') is-invalid @enderror" name="name" type="text" value="{{  old('name') ?  old('name') : ( $client ? $client->name : '' ) }}" id="example-url-input" placeholder="Insert Name" required>
			</div>
		</div>
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label" >Default</label>
			<div class="col-10">
				<!-- <input type="radio" id="default_yes" name="default" value="Y" {{ (isset($client->isdefault) && ($client->isdefault == 'Y'))? 'checked="checked"' : '' }}>
				<label for="default_yes">Yes</label><br>
				<input type="radio" id="default_no" name="default" value="N" {{ (isset($client->isdefault) && ($client->isdefault == 'Y'))? '' : 'checked="checked"' }} >
				<label for="default_no">No</label>-->
				<span class="switch switch-sm">
					<label class="switch">
						<input type="checkbox" name="default_customer" id="default_customer" {{ (isset($client->isdefault) && ($client->isdefault == 'Y'))?  'checked="checked"' : ''}} >
						<span></span>
					</label>
				</span>
				<?php /*<input class="form-control @error('password') is-invalid @enderror" name="password" type="text" value="" id="example-url-input" placeholder="Insert Pass">*/?>
			</div>
		</div>		
		
		@foreach ($platforms as $platform)
		<?php 
							
		$ads_accounts_associated = json_decode($client->ads_accounts);
		$properties_associated = json_decode($client->properties);
		$fee_associated = json_decode($client->fees);

		//var_dump($fee_associated);

		






			
		?>
		<div class="form-group row rad_btn">
			<div class="col-3">
				<span class="switch switch-sm">
					<label class="switch">
						<input type="checkbox" rrss="{{strtolower($platform->name)}}" name="rrss[{{strtolower($platform->name)}}][rrss]" id="rrss_{{strtolower($platform->name)}}" value="{{strtolower($platform->name)}}" tab="1" class="redSocial" <?php if (isset($ads_accounts_associated->{strtoupper($platform->name)})){ echo ' checked '; } ?>>
						<span rrss="{{strtolower($platform->name)}}"></span>
					</label>
				</span>
			</div>
			<div class="col-9">
				<i class="fab fa-{{strtolower($platform->name)}}"></i> {{strtoupper($platform->name)}}
			</div>
			<div class="col-12" style="margin-top: 15px;" id="settings_{{strtoupper($platform->name)}}">
				<div class="row mb-4">
					<label for="ads_accounts_facebook" class="col-3 col-form-label" >Ads Account</label>
					<div class="col-9">
						@if (isset($ads_accounts[strtoupper($platform->name)])  && count($ads_accounts[strtoupper($platform->name)]) > 1)
						<select 
							id="ads_{{strtolower($platform->name)}}" 
							name="rrss[{{strtoupper($platform->name)}}][ads][]" 
							class="selectpicker w-100 @error('name') is-invalid @enderror" 
							name="ads_accounts_{{strtolower($platform->name)}}" 
							data-placeholder="Choose a ads accounts..."  
							multiple="true">
							<option value=""></option>
								@foreach ($ads_accounts[strtoupper($platform->name)] as $item)
									<option value="{{$item['public_id']}}"
										<?php 
											if (isset($ads_accounts_associated->{strtoupper($platform->name)})){
												if ( in_array( $item['public_id'],   $ads_accounts_associated->{strtoupper($platform->name)}) ){
													echo ' selected ';
												}
											}
										?>>{{$item['name']}}</option>>
								@endforeach							
						</select>
						@else
							No ads accounts		
						@endif
					</div>
				</div>
				<div class="row mb-4">
					<label for="properties_{{strtolower($platform->name)}}" class="col-3 col-form-label" >Properties</label>
					<div class="col-9">	
						@if (isset($properties_accounts[strtoupper($platform->name)])  && count($properties_accounts[strtoupper($platform->name)]) > 1)
						<select id="properties_{{strtolower($platform->name)}}" name="rrss[{{strtoupper($platform->name)}}][properties][]" class="selectpicker w-100 @error('name') is-invalid @enderror" name="properties_facebook" data-placeholder="Choose a properties..."  multiple="">
							<option value=""></option>
							@foreach ($properties_accounts[strtoupper($platform->name)] as $item)
								<option value="{{$item['public_id']}}"
								<?php 
											if (isset($properties_associated->{strtoupper($platform->name)})){
												if ( in_array( $item['public_id'], $properties_associated->{strtoupper($platform->name)}) ){
													echo ' selected ';
												}
											}
										?>>{{$item['name']}}</option>>
							@endforeach	
						</select>
						@else
						<div style="padding: 0.65rem 1rem;">No properties</div>
						@endif
					</div>
				</div>
				<div class="row">
					<label for="fee" class="col-3 col-form-label">Fee</label>
					<div class="col-9">				
						<input class="form-control @error('name') is-invalid @enderror" name="rrss[{{strtoupper($platform->name)}}][fee]"  type="number" step="0.01" value="{{  ( isset($fee_associated->{strtoupper($platform->name)}) ? $fee_associated->{strtoupper($platform->name)} : '' ) }}" id="fee_{{strtolower($platform->name)}}" placeholder="Insert fee">
					</div>
				</div>
			</div>
		</div>
		@endforeach

	</div>

	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-12">
					<a href="javascript:editClient();" class="btn btn-success">Save</a>&ensp;
					<a href="javascript:window.back();" class="btn btn-outline-basic">Cancel</a>
				</div>
			</div>
		</div>
	</div>
</form>



@endsection

@section('customscript')
	<script src="/assets/app/custom/general/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="/js/chozen.jquery.js" type="text/javascript"></script>
	<script>
		$(document).ready(function(){
			//$('#rol').select2({ placeholder : 'Select a role' });
			
			$('.chosen-select').chosen();
			
			$('.check').click(function(){

				var rrss = $(this).attr('rrss');
				
				if($('#rrss_'+rrss).is(':checked')){
					$('#ads_'+rrss).prop('disabled', true).trigger("chosen:updated");
					$('#properties_'+rrss).prop('disabled', true).trigger("chosen:updated");
					$('#fee_'+rrss).prop('disabled', true);
				}else{					
					$('#ads_'+rrss).prop('disabled', false).trigger("chosen:updated");
					$('#properties_'+rrss).prop('disabled', false).trigger("chosen:updated");
					$('#fee_'+rrss).prop('disabled', false);
				}

			});
			

			$('#ads_facebook').prop('disabled', true).trigger("chosen:updated");
			$('#properties_facebook').prop('disabled', true).trigger("chosen:updated");
			$('#ads_twitter').prop('disabled', true).trigger("chosen:updated");
			$('#properties_twitter').prop('disabled', true).trigger("chosen:updated");
			$('#ads_linkedin').prop('disabled', true).trigger("chosen:updated");
			$('#properties_linkedin').prop('disabled', true).trigger("chosen:updated");
			$('#ads_instagram').prop('disabled', true).trigger("chosen:updated");
			$('#properties_instagram').prop('disabled', true).trigger("chosen:updated");
			$('#ads_snapchat').prop('disabled', true).trigger("chosen:updated");
			$('#properties_snapchat').prop('disabled', true).trigger("chosen:updated");
			$('#ads_amazon').prop('disabled', true).trigger("chosen:updated");
			$('#properties_amazon').prop('disabled', true).trigger("chosen:updated");
			$('#ads_adwords').prop('disabled', true).trigger("chosen:updated");
			$('#properties_adwords').prop('disabled', true).trigger("chosen:updated");
			
			$('#fee_facebook').prop('disabled', true);
			$('#fee_twitter').prop('disabled', true);
			$('#fee_linkedin').prop('disabled', true);
			$('#fee_instagram').prop('disabled', true);
			$('#fee_snapchat').prop('disabled', true);
			$('#fee_amazon').prop('disabled', true);
			$('#fee_adwords').prop('disabled', true);	

		
			@foreach ($platforms as $platform)
			<?php 
				$ads_accounts_associated = json_decode($client->ads_accounts);
				$properties_associated = json_decode($client->properties);
				$fee_associated = json_decode($client->fee);

				if (isset($ads_accounts_associated->{strtoupper($platform->name)})){ ?>
					$('#ads_' + '<?php echo strtolower($platform->name); ?>').prop('disabled', false).trigger("chosen:updated");
					$('#properties_' + '<?php echo strtolower($platform->name); ?>').prop('disabled', false).trigger("chosen:updated");
					$('#fee_' + '<?php echo strtolower($platform->name); ?>').prop('disabled', false);
				<?php }
			
			?>
			@endforeach
		
			
		});

		function editClient(){
            
			$.ajax({
				url: '/customers/update',
				data: new FormData(document.getElementById('edit_form_customer')),
				type: 'POST',
				contentType: false,
				dataType: "json",
				processData: false,

				beforeSend: function() {
					$('#edit_customer').html('<img src="https://app.thesocialaudience.com/img/loaderIcon.gif" style="height: 22px;margin-right: 10px;">');
				},
			})
			.done(function(response) { 
				$('#edit_customer').html('Save');

				if (response.status) {
					toastr.success(response.message, "Customers");
				} else {
					toastr.error(response.message, "Customers");
				}
		
			});


		}
		
    </script>
@endsection
