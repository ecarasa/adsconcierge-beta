
			<h6 class="stap-subtitle">What do you promoted?</h6>
			<!-- Campo dependiente del boton Formato normal -->
			<div id="formt_norm">
				<div class="form-group row align-items-center">
					<div  class="col-12 form-group" >
						<div class="form-group row rad_btn_1" >
							<div class="form-group row rad_btn_1" style="margin:auto;">
								<ul style="padding-inline-start: 0px;">
									<li class="rad_btn">
										<input type="radio" name="rad__promote" id="radiobtn_peq1" value="1" class="btn_izq"  checked="checked" />
										<label for="radiobtn_peq1">
											Promote
											<h5 class="mb-0">Specific Posts</h5>
										</label>
										<input type="radio" name="rad__promote" id="radiobtn_peq2" value="2"  class="btn_der"/>
										<label for="radiobtn_peq2">
											Automatically Promoted
											<h5 class="mb-0">Latest Posts</h5>
										</label>
									</li>
								</ul>
							</div>
						</div>
						<div id="d_promote">
							
							
							@foreach ($Array_RSS as $this_rrss)
						
							<div class="dt-rrss" id="posts-{{strtolower($this_rrss)}}">
								<h6 class="stap-subtitle">{{$this_rrss}}</h6>
								<!-- <div class="col-form-label px-2"><h5>{{$this_rrss}}</h5></div> -->
								<div class="row mb-5 align-items-center">
									<div class="col-md-2">
										<label class="staps-label">Posts</label>
									</div>
									<div class="col-md-10">
										<div class="from-api kt-input-icon">
											<input type="text" class="form-control posts {{strtolower($this_rrss)}}" placeholder="Add posts" name="posts[{{strtolower($this_rrss)}}]" rrss="{{strtolower($this_rrss)}}">
											<span class="kt-input-icon__icon kt-input-icon__icon--right">
												<svg id="_27_Icon_pin" data-name="27) Icon/pin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M8,20a1,1,0,0,1-.651-.242C7.049,19.5,0,13.423,0,7.922a8,8,0,0,1,16,0c0,5.5-7.049,11.58-7.349,11.836A1,1,0,0,1,8,20ZM8,2A5.968,5.968,0,0,0,2,7.922c0,3.655,4.2,8.018,6,9.724,1.8-1.705,6-6.067,6-9.724A5.968,5.968,0,0,0,8,2Zm0,9a3.5,3.5,0,1,1,3.5-3.5A3.5,3.5,0,0,1,8,11ZM8,6A1.5,1.5,0,1,0,9.5,7.5,1.5,1.5,0,0,0,8,6Z" transform="translate(4 2)" fill="#222b45"/> </svg>
											</span>
											<div class="posts-box {{strtolower($this_rrss)}}"></div>
											<div class="form-group row p-3" >
												<ul class="posts-list {{strtolower($this_rrss)}}" >	</ul>
											</div>
										</div>
										<div class="error_interes alert-danger px-4 py-2" style="display:none;">This field is required.</div>
									</div>
								</div>
							</div>

							@endforeach
							
							
							<?php /*
							<div class="row">
								<label class="col-md-2 staps-label">
									Posts
								</label>
								<div class="col-md-10">
									<div class="kt-form__group--inline">
										<!--div class="kt-form__label">
											Posts
										</div-->
										<div class="kt-form__control">
											<select class="form-control m-select2" id="posts" name="posts">
												
													<option value="" selected>Please select a Posts</option>
																								
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row" >
								<label class="col-md-2 staps-label">
									
								</label>
								<div class="col-md-10">
									<div class="kt-form__group--inline">
										<div class="kt-form__label">
											or search by posts ID
										</div>
										<div class="kt-form__control">
											<div class="input-group-append">
												<input type="text" class="form-control" placeholder="Paste your Post ID here" name="posts">
												<div class="input-group-append">
												<button class="form-control kt-form__group--inline" id="buscar_posts" name="buscar_posts"><!--i class="la la-search"></i-->Search</button>
												</div>
												<div class="input-group-append">
												<button class="form-control" id="cancelar_posts" name="cancelar_posts">Cancel</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							*/
							?>
						</div>
						
					
						
						<div id="d_aPromoted">
							<div class=" form-group row demog ">
								<div class="kt-section col-md-10">
									<fieldset class="gender">
										<ul class="pl-0">
											<li class="radpe_btn_gen">
												
												<input type="radio" name="aPromoted" id="aposts1" value="1" class="rss" checked="checked" />
												<label for="aposts1"> RSS</label>
												
												<input type="radio" name="aPromoted" id="aposts2" value="2" class="ganalitycs" />
												<label for="aposts2">GAnalitycs</label>
												
												<input type="radio" name="aPromoted" id="aposts3" value="3" class="latestposts" />
												<label for="aposts3">Latest Posts</label>
											</li>
										</ul>
									</fieldset>
								</div>
							</div>
							
							<div id="post_url">
								<div class="mb-5 row align-items-center">
									<div class="col-md-4">
										<label class="staps-label">URLs</label>
									</div>
									<div class="col-md-8">
										<div class="kt-form__group--inline">
											<div class="kt-form__control">
												<input type="text" class="form-control" placeholder="Insert URL" name="url">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="select_ganalitycs" class="form-group row align-items-center" style="display:none;">
								<!-- <label class="col-form-label col-lg-2"></label> -->
								<div class="col-md-4">
									<label class="staps-label">Property *</label>
								</div>
								<div class="col-md-8">
									<div class="row">
										<div class="col-md-10">
											<select class="selectpicker w-100" id="ganalitycs" name="ganalitycs">
												@forelse ($data['ganalitycs'] as $data)
													<option value="{{ $properties->id }}">{{ $data->name }}</option>
												@empty
													<option value="">No hay datos</option>
												@endforelse
											</select>
										</div>
										<div class="col-md-2">
											<button type="button" class="btn btn-Info btn-icon-right" name="pixl">Sync
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"></path> </svg>
											</button>
										</div>
									</div>
								</div>
							</div>
							<div id="select_latestposts_post" style="display:none;">
								<div class="form-group row">
									<div class="col-md-4">
										<label class="staps-label">Posts</label>
									</div>
									<div class="col-md-8">
										<fieldset>
											<ul>
												<li class="radpe_btn_gen select_latestposts_post">
													<input type="radio" name="aPosts" id="aPposts1" value="1" class="tod" checked="checked" />
													<label for="aPposts1">
														ALL
													</label>
													<input type="radio" name="aPosts" id="aPposts2" value="2" class="hom" /><label for="aPposts2">Link</label>
													<input type="radio" name="aPosts" id="aPposts3" value="3" class="hom" /><label for="aPposts3">Status</label>
													<input type="radio" name="aPosts" id="aPposts4" value="4" class="hom" /><label for="aPposts4">Photo</label>
													<input type="radio" name="aPosts" id="aPposts5" value="5" class="muj" />
													<label for="aPposts5">
														Video
													</label>
												</li>
											</ul>
										</fieldset>
									</div>
								</div>
							</div>

							{{-- 
								fields to post

							howPromoted  $('input[name=rad__promote]:checked').val() // 1 al 2
							typePromoted  $('input[name=aPromoted]:checked').val() // 1 al 3
							postPromoted  $('input[name=aPosts]:checked').val()  // 1 al 5
							promoteForValue
							promoteForType
							t_postLink 
							t_postText


							if typePromoted = 2
								name="posti[0][t_promotePost]"
								name="posti[0][se_promotePost_GA]"

							if typePromoted = 3
								name="posti[0][t_promotePost]"
								name="posti[0][se_promotePost]" 
						
							--}}



							<div class="form-group row">
								<div class="col-md-4">
									<label class="staps-label">Promote posts only for</label>
								</div>
								<div class="col-md-8">
									<div class="row">
										<div class="input-group col-md-2" bis_skin_checked="1" style='float:left;'>
											<input type="text" class="form-control col-xs-1" placeholder="1" name="promoteForValue" id="promoteForValue">
										</div>
										<div class="col-md-6 bud_dist" bis_skin_checked="1">
											<select id="promoteForType" name="promoteForType" class="selectpicker">
												<option value="money">Spend during promotion</option>
												<option value="days">Days</option>
											</select>
										</div>
										<div class="col-12 mt-2">
											<label class="staps-label ">Choose between how much money do you want to spend or in how many days will be your post online</label>
										</div>
									</div>
								</div>
							</div>
							<br/>
							<div class="form-group row align-items-center">
								<div class="col-md-4">
									<label class="checkbox">
										<input type="checkbox" name="cb_postLink" id="cb_postLink">
										<span></span>
										Post link contains the word.
									</label>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" placeholder="Enter a word or part of the link (eg 'tour/latest')" name="t_postLink" id="t_postLink">
								</div>
							</div>

							<div class="form-group row align-items-center">
								<div class="col-md-4">
									<label class="checkbox">
										<input type="checkbox" name="cb_postText" id="cb_postText">
										<span></span>
										Post text contains any of the following keywords.
									</label>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" placeholder="Insert tags and press 'enter'.." name="t_postText" id="t_postText">
								</div>
							</div>
							
							<div class="promote_post_with_more_than" style="display:none;">
								<div class="row form-group">
									<div class="col-md-4">
										<label class="checkbox">
											<input type="checkbox" name="cb_promotePost[]" id="cb_promotePost"><span></span>Promote post with more than
										</label>
									</div>
									<div class="col-md-8">
										<div class="form-group" id="kt_repeater_13">
											<div data-repeater-list="posti">
												<div data-repeater-item class="form-group row align-items-center">
													<div class="col-md-3">
														<input type="text" class="form-control" placeholder="num" name="t_promotePost" id="t_promotePost">
													</div>
													<div class="col-md-4">
														<select class="selectpicker w-100" name="se_promotePost" id="se_promotePost">
															<option value="likes" selected>Likes</option> 
															<option value="shares">Shares</option> 
															<option value="comments">Comments</option> 
															<option value="videoduration">Video duration</option> 
															<option value="videoview">Video view</option> 
															<option value="lateshourpageviews">Latest hour pageviews</option> 
														</select> 
													</div>
													<div class="col-md-4">
														<select class="selectpicker w-100" name="se_promotePost_GA" id="se_promotePost_GA" style="display: none;">
															<option value="lateshourpageviews">Latest hour pageviews</option> 
														</select> 
													</div>
													<div class="col-md-1">
														<div data-repeater-delete="" class="btn btn-icon btn-danger delposti">
															<svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
														</div>
													</div>
												</div>
											</div>
											<div data-repeater-create="" class="btn btn-sm btn-info addposti">
												<span><i class="la la-plus"></i><span>Add</span></span>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row align-items-center">
								<div  class="col-md-12">
									<div class="btn btn-sm btn-info">
									Validate your post filter
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div> 

				<div id="link_stndr">					
				</div>
			</div>					