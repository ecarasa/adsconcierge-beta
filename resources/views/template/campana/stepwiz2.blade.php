<style>

	.target_container {
		display: grid;
		grid-template-columns: repeat(5, 1fr);
		grid-template-rows: repeat(5, 1fr);
		grid-column-gap: 0px;
		grid-row-gap: 0px;
	}

	.icon_target { grid-area: 1 / 1 / 2 / 3; }
	.check_target { grid-area: 2 / 1 / 3 / 2; }
	.text_target { grid-area: 2 / 2 / 3 / 3; }

</style>



<div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
	<div class="kt-form__section kt-form__section--first">
		<div class="kt-wizard-v1__form">
			<!-- <label class="kt-heading kt-heading--lg">AUDIENCE</label> -->
			<h5 class="stap-title">2 Audience</h5>
			<div class="row mb-4 align-items-center">
				<div class="col-12 col-lg-2 d-flex align-items-center">
					<label class="staps-label">Location</label>
				</div>
				<div class="col-12 col-lg-10">
					<div class="row align-items-center">
						<div class="col-12 col-md-2 col-lg-1">	
							<label class="switch">
								<input type="checkbox" id="target-location" data-id="location" name="target"> 
								<span></span>
							</label>
						</div>
					</div>
				</div>
			</div>

			<!-- <div class="kt-heading kt-heading--lg">
		
				
				<span class="switch switch-sm">
				
				</span>
			</div> -->

			@foreach ($Array_RSS as $this_rrss)

			<div class="dt-rrss row mb-4 align-items-center" id="dt2-{{strtolower($this_rrss)}}">
				<div class="col-12 col-lg-2 d-flex align-items-center">
					<label class="staps-label">{{$this_rrss}}</label>
				</div>
				<div class="col-12 col-lg-10" id="dest_1">
					<div class="row align-items-center">
						<div class="col-12 col-md-5 col-lg-3 mb-3">
							<div class="input-group-append">
								<select class="selectpicker" id="location-action-{{strtolower($this_rrss)}}" name="location-action[{{strtolower($this_rrss)}}]">
									<option value="IN" selected>Include</option>
									<option value="EX">Exclude</option>
								</select>
							</div>
						</div>
						<div class="col-12 col-md-7 col-lg-9 mb-3">
							<div class="row">
								<input type="text"  autocomplete="nope" name="location[{{strtolower($this_rrss)}}]" id="location_{{strtolower($this_rrss)}}" class="form-control form-control-sm ubic pr-5" placeholder="Add a country group, marketing area, country, state/province, city, ZIP or an address...">
								<!-- Este boton carga un mapa con JS -->
								<button type="button" class="btn loason map" name="btn-map">
									<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<path fill="#8F9BB3" style="transform: translate(4px, 2px);" d="M8,8.9995 C7.173,8.9995 6.5,8.3265 6.5,7.4995 C6.5,6.6725 7.173,5.9995 8,5.9995 C8.827,5.9995 9.5,6.6725 9.5,7.4995 C9.5,8.3265 8.827,8.9995 8,8.9995 M8,3.9995 C6.07,3.9995 4.5,5.5695 4.5,7.4995 C4.5,9.4295 6.07,10.9995 8,10.9995 C9.93,10.9995 11.5,9.4295 11.5,7.4995 C11.5,5.5695 9.93,3.9995 8,3.9995 M8,17.646 C6.325,16.062 2,11.615 2,7.922 C2,4.657 4.691,2 8,2 C11.309,2 14,4.657 14,7.922 C14,11.615 9.675,16.062 8,17.646 M8,0 C3.589,0 0,3.553 0,7.922 C0,13.397 7.049,19.501 7.349,19.758 C7.537,19.919 7.768,20 8,20 C8.232,20 8.463,19.919 8.651,19.758 C8.951,19.501 16,13.397 16,7.922 C16,3.553 12.411,0 8,0"></path>
									</svg>
								</button>
								<div id="location-box-{{strtolower($this_rrss)}}" class="location-box"></div>
							</div>
						</div>
						<div class="col-12">
							<select class="selectpicker" id="location_option_{{strtolower($this_rrss)}}" name="location_option[{{strtolower($this_rrss)}}]">
								<option value="EO" selected>Everyone in this location</option>
								<option value="WL">People who live in this location</option>
								<option value="PR">People recently in this location </option>
								<option value="PT">People traveling in this location</option>
							</select>
							<div class="tags" >
								<ul id="location-list-{{strtolower($this_rrss)}}" class="location-list" >
								</ul>
							</div>
						</div>
						<div class="col-12">
							<div class="kt-section dropz" id="dest_2" style="display: none;">
								<span>Included locations*</span>
								<div class="kt-dropzone dropzone dropz" action="inc/api/dropzone/upload.php" id="m-dropzone-one">
									<div class="kt-dropzone__msg dz-message needsclick">
										<h3 class="kt-dropzone__msg-title">
										<i class="fa fa-cloud-upload-alt"></i>Upload included location</h3>
										<span class="kt-dropzone__msg-desc"></span>
									</div>
								</div>
								<span class="form-text text-muted">
									Upload a spreadsheet (.xlsx/csv, max 5MB)
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
			<!-- multiSelect conecta con API de redes sociales-->
			<div class="row mb-4 align-items-center">
				<div class="col-12 col-lg-2 d-flex align-items-center">
					<label class="staps-label">Languages</label>
				</div>
				<div class="col-12 col-lg-10">
					<div class="row align-items-center">
						<div class="col-12 col-md-2 col-lg-1">
							<label class="switch">
								<input type="checkbox" id="target-language" data-id="language" name="target"> 
								<span></span>
							</label>
						</div>
						<div class="col-12 col-lg-2">
							<input type="text"  autocomplete="nope" name="language" id="language" class="form-control ubic" placeholder="Add a language...">
							<div id="language-box" class="language-box"></div>
							<div class="error_location alert-danger px-4 py-2" style="display:none;">This field is required.</div>
						</div>
						<div class="col-12">
							<div class="tags" >
								<ul id="language-list" class="language-list" >
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<h6 class="stap-subtitle">Demographics</h6>
			<div class="row mb-4 align-items-center">
				<div class="col-12 col-lg-2 d-flex align-items-center">
					<label class="staps-label">Gender</label>
				</div>
				<div class="col-12 col-lg-10">
					<div class="row align-items-center">
						<div class="col-12 col-md-2 col-lg-1">
							<label class="switch">
								<input type="checkbox" id="target-gender" data-id="gender" name="target">
								<span></span>
							</label>
						</div>
						<div class="col-12 col-lg-5">
							<fieldset class="gender">
								<ul class="list-unstyled pl-0 mb-0">
									<li class="radpe_btn_gen btn-redio-group">
										@php
											$first = true;
										@endphp
										@foreach ($data['gender'] as $gender)
											<input type="radio" name="gender" valor="{{$gender->valor}}" id="gender{{$gender->public_id}}" value="{{$gender->public_id}}" class="tod" <?php if ($first) { echo 'checked="checked"'; } ?> />
											<label for="gender{{$gender->public_id}}" class="{{$gender->nombre_ingles}}">{{substr($gender->nombre_ingles, 0, 1)}}</label>
											<?php $first = false; ?>
										@endforeach
									</li>
								</ul>
							</fieldset>
						</div>
					</div>
				</div>
			</div>

			<div class="row mb-4 align-items-center">
				<div class="col-12 col-lg-2 d-flex align-items-center">
					<label class="staps-label">Age</label>
				</div>
				<div class="col-12 col-lg-10">
					<div class="row align-items-center">
						<div class="col-12 col-md-2 col-lg-1">
							<label class="switch">
							<input type="checkbox" id="target-age" data-id="age" name="target"> 
								<span></span>
							</label>
						</div>
						<div class="col-12 col-lg-6" id="kt_repeater_age">
							<div id="kt_repeater_11">
								<div data-repeater-list="age">
									<div data-repeater-item="" >
										<div class="row align-items-center my-2">
											<div class="col-12 col-sm-8">
												<div style="margin-left: 9px; margin-right: 16px;">
													<div class="age-slider"></div>
													<input type="hidden" class="min-age" name="start" placeholder="From..."/>
													<input type="hidden" class="max-age" name="end" placeholder="...To" />
												</div>
											</div>
											<div class="px-2">
												<div data-repeater-delete="" class="btn btn-icon btn-danger deledad">
													<svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
												</div>
											</div>
											<div class="px-2">
												<div data-repeater-create="" class="btn btn-icon-left btn-basic addedad">Add</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-5" >
							<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" id="errorAge">
								<div class="toast-header">
									<strong class="mr-auto">Error</strong>
									<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close" id="closeAge">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="toast-body">Error message.</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<h6 class="stap-subtitle">Technology</h6>
			<div class="row mb-4 align-items-center">
				<div class="col-12 col-lg-2 d-flex align-items-center">
					<label class="staps-label">Devices</label>
				</div>
				<div class="col-12 col-lg-10">
					<div class="row align-items-center">
						<div class="col-12 col-md-2 col-lg-1">
							<label class="switch">
								<input type="checkbox" id="target-devices" data-id="devices" name="target"> 
								<span></span>
							</label>
						</div>
						<div class="col-12 col-lg-5">
							<fieldset class="devices">
								<ul class="list-unstyled pl-0 mb-0">
									<li class="radpe_btn_gen">
									@php
										$first = true;
									@endphp
									@foreach ($data['devices'] as $device)
										<input type="radio" name="devices" valor="{{$device->valor}}"  id="devices{{$device->public_id}}" value="{{$device->public_id}}" class="tod" <?php if ($first) { echo 'checked="checked"'; } ?> /><label for="devices{{$device->public_id}}">{{$device->nombre_ingles}}</label>
										<?php $first = false; ?>
									@endforeach
									</li>
								</ul>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
			<div class="row mb-4 align-items-center">
				<div class="col-12 col-lg-2 d-flex align-items-center">
					<label class="staps-label">Conectivity</label>
				</div>
				<div class="col-12 col-lg-10">
					<div class="row align-items-center">
						<div class="col-12 col-md-2 col-lg-1">
							<label class="switch">
								<input type="checkbox" id="target-conectivity" data-id="conectivity" name="target"> 
								<span></span>
							</label>
						</div>
						<div class="col-12 col-lg-5">
							<fieldset class="conectivity">
								<ul class="list-unstyled pl-0 mb-0">
									<li class="radpe_btn_gen">
										@php
											$first = true;
										@endphp
										@foreach ($data['conectivity'] as $conectivity)
											<input type="radio" name="conectivity"  valor="{{$conectivity->valor}}"  id="conectivity{{$conectivity->public_id}}" value="{{$conectivity->public_id}}" class="tod" <?php if ($first) { echo 'checked="checked"'; } ?> /><label for="conectivity{{$conectivity->public_id}}">{{$conectivity->nombre_ingles}}</label>
											<?php $first = false; ?>
										@endforeach
									</li>
								</ul>
							</fieldset>
						</div>
					</div>
				</div>
			</div>

			<h6 class="stap-subtitle">Detailed Targetihg</h6>
			<div class="row mb-4 align-items-center">
				<div class="col-12 col-lg-2 d-flex align-items-center">
					<label class="staps-label">Interest</label>
				</div>
				<div class="col-12 col-lg-10">
					<div class="row align-items-center">
						<div class="col-12 col-md-2 col-lg-1">
							<label class="switch">
								<input type="checkbox" id="target-interest" data-id="interest" name="target">
								<span></span>
							</label>
						</div>
					</div>
				</div>
			</div>
			 
			@foreach ($Array_RSS as $this_rrss)
						
			<div class="dt-rrss row mb-4 align-items-center" id="dt-{{strtolower($this_rrss)}}">
				<div class="col-12 col-lg-2 d-flex align-items-center">
					<label class="staps-label">{{$this_rrss}}</label>
				</div>
				<div class="col-12 col-lg-10">
					<div class="row align-items-center">
						<div class="col-12 col-lg-3">
							<div class="input-group-append">
								<select class="selectpicker intereses-action {{strtolower($this_rrss)}}" name="intereres-param[{{strtolower($this_rrss)}}]" >
									<option value="IN" selected>Include</option>
									<option value="EX">Exclude</option>
								</select>
							</div>
						</div>
						<div class="col-12 col-lg-9">
							<div class="kt-input-icon segm">
								<input type="text" class="form-control form-control-sm pr-5 intereses {{strtolower($this_rrss)}}" placeholder="Add interests" name="intereses[{{strtolower($this_rrss)}}]" rrss="{{strtolower($this_rrss)}}">
								<span class="kt-input-icon__icon kt-input-icon__icon--right">
									<span><i class="la la-search"></i></span>
								</span>
								<div class="intereses-box {{strtolower($this_rrss)}}"></div>
							</div>
						</div>
						<div class="col-12">
							<div class="tags" >
								<ul class="intereses-list {{strtolower($this_rrss)}}" >	</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach

			<div class="row mb-4 align-items-center">
				<div class="col-12 col-lg-2 d-flex align-items-center">
					<label class="staps-label">Custom Audience</label>
				</div>
				<div class="col-12 col-lg-10">
					<div class="row align-items-center">
						<div class="col-12 col-md-2 col-lg-1">
							<label class="switch">
								<input type="checkbox" id="target-custom-audience" data-id="custom-audience" name="target"> 
								<span></span>
							</label>
						</div>
					</div>
				</div>
			</div>
			
			@foreach ($Array_RSS as $this_rrss)
			<div class="dt-rrss row mb-4 align-items-center" id="dt3-{{strtolower($this_rrss)}}">
				<div class="col-12 col-lg-2 d-flex align-items-center">
					<label class="staps-label">{{$this_rrss}}</label>
				</div>
				<div class="col-12 col-lg-10">
					<div class="row align-items-center">
						<div class="col-12 col-lg-3">
							<div class="input-group-append">
								<select class="selectpicker audiencia-action {{strtolower($this_rrss)}}" name="audiencia-param[{{strtolower($this_rrss)}}]">
									<option value="IN" selected>Include</option>
									<option value="EX">Exclude</option>
								</select>
							</div>
						</div>
						<div class="col-12 col-lg-9">
							<div class="kt-input-icon segm">
								<input type="text" class="form-control form-control-sm ubic pr-5 audiencia {{strtolower($this_rrss)}}" placeholder="Please select a Custom Audience..." name="audiencia[{{strtolower($this_rrss)}}]" rrss="{{strtolower($this_rrss)}}">
								<span class="kt-input-icon__icon kt-input-icon__icon--right">
									<span><i class="la la-search"></i></span>
								</span>
								<div class="audiencia-box {{strtolower($this_rrss)}}"></div>
								
							</div>
						</div>
						<div class="col-12">
							<div class="tags" >
								<ul class="audiencia-list {{strtolower($this_rrss)}}" >	</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
