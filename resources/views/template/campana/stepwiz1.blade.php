<div class="kt-wizard-v1__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
    <h5 class="stap-title">1 Create Campaign</h5>
    <div class="kt-form__section kt-form__section--first">
        <div class="kt-wizard-v1__form">
            <div class="row mb-4 align-items-center">
                <div class="col-12 col-lg-2 d-flex align-items-center">
                    <label class="staps-label">Campaign Name *</label>
                </div>
                <div class="col-12 col-lg-3">
                    <input type="text" class="form-control" name="campaign_name" placeholder="Campaign Name">
                </div>
            </div>

            <div class="row mb-5 align-items-center">
                <div class="col-12 col-lg-2 d-flex align-items-center">
                    <label class="staps-label">Client *</label>
                </div>
                <div class="col-12 col-lg-3">
                    <select class="selectpicker col-12 px-0" id="select_client" name="client" data-entidad="clients"  show-tick title="" data-live-search="true">

                        @forelse ($data['customers'] as $client)
                        <option value="{{ $client->public_id }}">{{ $client->name }}</option>
                        @empty
                        <option value="">No hay cliente</option>
                        @endforelse
                    </select>
                </div>
                <div title="Update client"  class="ml-3 btn btn-primary btn-icon" data-toggle="modal" data-target="#ModalClientEdit">
                    <svg xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#FFFFFF"></path> </svg>
                </div>
                <div title="View client"  class="ml-2 btn btn-primary btn-icon la" data-toggle="modal" data-target="#ModalClientView"><!-- iCon by oNlineWebFonts.Com -->
                    <svg id="_27_Icon_eye" data-name="27) Icon/eye" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M10.025,14C4.153,14,.755,8.584.133,7.5a1,1,0,0,1,0-1C.987,5.014,4.2.145,9.73,0c.1,0,.2,0,.294,0,5.816,0,9.221,5.418,9.844,6.5a1,1,0,0,1,0,1c-.854,1.489-4.071,6.358-9.6,6.5ZM9.967,2,9.781,2C5.729,2.107,3.11,5.53,2.173,7c.988,1.5,3.733,5,7.875,5l.172,0c4.056-.1,6.671-3.527,7.607-4.993C16.839,5.5,14.1,2,9.967,2ZM10,10.5A3.5,3.5,0,1,1,13.5,7,3.5,3.5,0,0,1,10,10.5Zm0-5A1.5,1.5,0,1,0,11.5,7,1.5,1.5,0,0,0,10,5.5Z" transform="translate(2 4.999)" fill="#FFFFFF"/>
                    </svg>
                </div>
                <div class="ml-2 btn btn-primary btn-icon-left" data-toggle="modal" data-target="#ModalClient">+  Create Client</div>

            </div>

            <!-- Modal -->
            <div class="modal fade" id="ModalClient" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Create Client</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="text" class="form-control col-12 col-lg-8" id="clientName" placeholder="Client Name" />
                        </div>
                        <div class="alertclientName alert-danger px-4 py-2" style="display:none;" role="alert">
                            Please enter a Client Name
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-basic" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary saveClient">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Edit -->
            <div class="modal fade" id="ModalClientEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Update Client</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <span id="editForm">

                            </span>
                        </div>
                        <div class="alertclientName alert-danger px-4 py-2" style="display:none;" role="alert">
                            Please enter a Client Name
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-basic" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary updateClient">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal View -->
            <div class="modal fade" id="ModalClientView" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">View Client</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid client_view">

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-basic" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="client_view_html" style="display:none">
                <div class="row">
                    <div class="col-8 col-sm-6">
                        key_value
                    </div>
                    <div class="col-4 col-sm-6">
                        name_value
                    </div>
                </div>
            </div>
            <div class="client_edit_html" style="display:none">
                <div class="row">
                    <div class="col-8 col-sm-6">
                        key_value
                    </div>
                    <div class="col-4 col-sm-6">
                        <input value="name_value" name="key_value">
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-12">
                    <ul id="ulrrss" class="btnft so_li_set">
                        <li class="rad_btn">
                            @if(Auth::user()->checkPlatform('facebook'))
                            <input type="checkbox" name="rrss[]" id="checkbtn_gde1" value="facebook" tab="1" class="redSocial step1" data-option="#kt_tabs_1" />
                            <label for="checkbtn_gde1"><svg xmlns="http://www.w3.org/2000/svg" width="32" height="31.807" viewBox="0 0 32 31.807" style="margin-right: 25px;">
                                <path id="FB" d="M32,16A16,16,0,1,0,13.5,31.805V20.625H9.437V16H13.5V12.476c0-4.009,2.389-6.225,6.044-6.225a24.558,24.558,0,0,1,3.581.313V10.5H21.107A2.313,2.313,0,0,0,18.5,13v3h4.437l-.709,4.627H18.5v11.18A16.006,16.006,0,0,0,32,16Z" fill="#222b45"/>
                                </svg>FACEBOOK
                            </label>
                            @else
                            <a href="#" class="rad_btn_checkbox block"><svg xmlns="http://www.w3.org/2000/svg" width="32" height="31.807" viewBox="0 0 32 31.807" style="margin-right: 25px;">
                                <path id="FB" d="M32,16A16,16,0,1,0,13.5,31.805V20.625H9.437V16H13.5V12.476c0-4.009,2.389-6.225,6.044-6.225a24.558,24.558,0,0,1,3.581.313V10.5H21.107A2.313,2.313,0,0,0,18.5,13v3h4.437l-.709,4.627H18.5v11.18A16.006,16.006,0,0,0,32,16Z" fill="#222b45"/>
                                </svg>FACEBOOK
                            </a>
                            @endif
                            @if(Auth::user()->checkPlatform('twitter'))
                            <input type="checkbox" name="rrss[]" id="checkbtn_gde2" value="twitter" tab="2" class="redSocial step1" data-option="#kt_tabs_2" />
                            <label for="checkbtn_gde2"><i class="fab fa-twitter"></i>TWITTER</label>
                            @else
                            <a href="#"  class="rad_btn_checkbox block"><i class="fab fa-twitter"></i>TWITTER</a>
                            @endif
                            @if(Auth::user()->checkPlatform('linkedin'))
                            <input type="checkbox" name="rrss[]" id="checkbtn_gde3" value="linkedin" tab="3" class="redSocial step1" data-option="#kt_tabs_3" />
                            <label for="checkbtn_gde3"><i class="fab fa-linkedin"></i>LINKEDIN</label>
                            @else
                            <a href="#"  class="rad_btn_checkbox block"><i class="fab fa-linkedin"></i>LINKEDIN</a>
                            @endif
                            @if(Auth::user()->checkPlatform('instagram'))
                            <input type="checkbox" name="rrss[]" id="checkbtn_gde4" value="instagram" tab="4" class="redSocial step1" data-option="#kt_tabs_4" />
                            <label for="checkbtn_gde4"><i class="fab fa-instagram"></i>INSTAGRAM</label>
                            @else
                            <a href="#"  class="rad_btn_checkbox block"><i class="fab fa-instagram"></i>INSTAGRAM</a>
                            @endif
                            @if(Auth::user()->checkPlatform('snapchat'))
                            <input type="checkbox" name="rrss[]" id="checkbtn_gde5" value="snapchat" tab="5" class="redSocial step1" data-option="#kt_tabs_5" />
                            <label for="checkbtn_gde5"><i class="fab fa-snapchat-ghost"></i>SNAPCHAT</label>
                            @else
                            <a href="#"  class="rad_btn_checkbox block"><i class="fab fa-snapchat-ghost"></i>SNAPCHAT</a>
                            @endif
                            @if(Auth::user()->checkPlatform('amazon'))
                            <input type="checkbox" name="rrss[]" id="checkbtn_gde6" value="amazon" tab="6" class="redSocial step1" data-option="#kt_tabs_6" />
                            <label for="checkbtn_gde6"><i class="fab fa-amazon"></i>AMAZON</label>
                            @else
                            <a href="#"  class="rad_btn_checkbox block"><i class="fab fa-amazon"></i>AMAZON</a>
                            @endif
                            @if(Auth::user()->checkPlatform('adwords'))
                            <input type="checkbox" name="rrss[]" id="checkbtn_gde7" value="adwords" tab="7" class="redSocial step1" data-option="#kt_tabs_7" />
                            <label for="checkbtn_gde7"><i class="fab fa-google"></i>ADWORDS</label>
                            @else
                            <a href="#"  class="rad_btn_checkbox block"><i class="fab fa-google"></i>ADWORDS</a>
                            @endif
                        </li>
                    </ul>
                    <div class="error_rrss alert-danger px-4 py-2" style="display:none;">This field is required.</div>
                </div>
            </div>
            <h6 class="title-small">What do you want to promote?</h6>
            <div class="social-tab">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" style="display:none;"><a class="nav-link link-social-details" data-toggle="tab" href="#kt_tabs_1"><i class="socicon-facebook"></i>Facebook</a></li>
                    <li class="nav-item" style="display:none;"><a class="nav-link link-social-details" data-toggle="tab" href="#kt_tabs_2"><i class="socicon-twitter"></i>Twitter</a></li>
                    <li class="nav-item" style="display:none;"><a class="nav-link link-social-details" data-toggle="tab" href="#kt_tabs_3"><i class="socicon-linkedin"></i>Linkedin</a></li>
                    <li class="nav-item" style="display:none;"><a class="nav-link link-social-details" data-toggle="tab" href="#kt_tabs_4"><i class="socicon-instagram"></i>Instagram</a></li>
                    <li class="nav-item" style="display:none;"><a class="nav-link link-social-details" data-toggle="tab" href="#kt_tabs_5"><i class="socicon-snapchat"></i>Snapchat</a></li>
                    <li class="nav-item" style="display:none;"><a class="nav-link link-social-details" data-toggle="tab" href="#kt_tabs_6"><i class="socicon-amazon"></i>Amazon</a></li>
                    <li class="nav-item" style="display:none;"><a class="nav-link link-social-details" data-toggle="tab" href="#kt_tabs_7"><i class="socicon-google"></i>Adwords</a></li>
                </ul>
                <div class="tab-content" id="tab-social-details">
                    <div class="tab-pane" id="kt_tabs_1" role="tabpanel">
                        <div class="">
                            <div class="kt-section">
                                <div class="kt-section__content">
                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Ad Account *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            <select class="selectpicker col-12 px-0" id="adsaccount-facebook" name="adsaccount[facebook]" attr_rrss="facebook">
                                                @forelse ($data['ads_accounts_facebook'] as $account)
                                                <option value="{{ $account->public_id }}" currency="{{ $account->currency }}" account_id="{{ $account->account_id }}">{{ $account->name }}</option>
                                                @empty
                                                <option value="">No hay Ads accounts</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Property *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            @php $disabled=''; @endphp
                                            @if(count($data['properties_accounts_facebook']) < 2 )
                                            @php $disabled='disabled'; @endphp
                                            @endif
                                            <select class="selectpicker col-12 px-0" id="properties-facebook" name="properties[facebook]">
                                                @forelse ($data['properties_accounts_facebook'] as $properties)
                                                <option value="{{ $properties->public_id }}">{{ $properties->name }}</option>
                                                @empty
                                                <option value="">No hay properties</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>



                                    <div class="">
                                        <div class="gradient" id="table_objetive_facebook">
                                            

                                            @forelse ($objectives as $objective)
                                            @if(strpos($objective->metadata,'FACEBOOK')!==false)
                                            <div class="click mb-3">
                                                <label class="checkbox">
                                                    <input type="radio" name="objetive[facebook]" value="{{$objective->id}}" metadata="{{$objective->metadata}}" valor="{{$objective->name}}">
                                                    <span></span>
                                                    {{$objective->name}}
                                                </label>
                                            </div>

                                            @endif
                                            @empty

                                            @endforelse

                                            
                                        </div>

                                        <div class="error_objetive_facebook alert-danger px-4 py-2" style="display:none;">This field is required.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="kt_tabs_2" role="tabpanel">
                        <div class="">
                            <div class="kt-section">
                                <div class="kt-section__content">
                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Ad Account *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            <select class="selectpicker col-12 px-0" id="adsaccount-twitter" name="adsaccount[twitter]"  attr_rrss="twitter">
                                                @forelse ($data['ads_accounts_twitter'] as $account)
                                                <option value="{{ $account->public_id }}" currency="{{ $account->currency }}" account_id="{{ $account->account_id }}">{{ $account->name }}</option>
                                                @empty
                                                <option value="">No hay Ads accounts</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Property *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            @php $disabled=''; @endphp
                                            @if(count($data['properties_accounts_twitter']) < 2 )
                                            @php $disabled='disabled'; @endphp
                                            @endif
                                            <select class="selectpicker col-12 px-0" id="properties-twitter" name="properties[twitter]" {{$disabled}}>
                                                @forelse ($data['properties_accounts_twitter'] as $properties)
                                                <option value="{{ $properties->public_id }}">{{ $properties->name }}</option>
                                                @empty
                                                <option value="">No hay properties</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="">
                                        <div class="gradient" id="table_objetive_twitter">
                                            

                                            @forelse ($objectives as $objective)
                                            @if(strpos($objective->metadata,'TWITTER')!==false)
                                            <div class="click mb-3">
                                                <label class="checkbox">
                                                    <input type="radio" name="objetive[twitter]" value="{{$objective->id}}" metadata="{{$objective->metadata}}" valor="{{$objective->name}}">
                                                    <span></span>
                                                    {{$objective->name}}
                                                </label>
                                            </div>

                                            @endif
                                            @empty

                                            @endforelse

                                            
                                        </div>

                                        <div class="error_objetive_twitter alert-danger px-4 py-2" style="display:none;">This field is required.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="kt_tabs_3" role="tabpanel">
                        <div class="">
                            <div class="kt-section">
                                <div class="kt-section__content">

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Ad Account *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            <select class="selectpicker col-12 px-0" id="adsaccount-linkedin" name="adsaccount[linkedin]"  attr_rrss="linkedin">
                                                @forelse ($data['ads_accounts_linkedin'] as $account)
                                                <option value="{{ $account->public_id }}" currency="{{ $account->currency }}">{{ $account->name }}</option>
                                                @empty
                                                <option value="">No hay Ads accounts</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Property *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            @php $disabled=''; @endphp
                                            @if(count($data['properties_accounts_linkedin']) < 2 )
                                            @php $disabled='disabled'; @endphp
                                            @endif
                                            <select class="selectpicker col-12 px-0" id="properties-linkedin" name="properties[linkedin]" {{$disabled}}>
                                                @forelse ($data['properties_accounts_linkedin'] as $properties)
                                                <option value="{{ $properties->public_id }}">{{ $properties->name }}</option>
                                                @empty
                                                <option value="">No hay properties</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="">
                                        <div class="gradient" id="tablasel3">
                                            

                                            @forelse ($objectives as $objective)
                                            @if(strpos($objective->metadata,'LINKEDIN')!==false)
                                            <div class="click mb-3">
                                                <label class="checkbox">
                                                    <input type="radio" name="objetive[linkedin]" value="{{$objective->id}}" metadata="{{$objective->metadata}}" valor="{{$objective->name}}">
                                                    <span></span>
                                                    {{$objective->name}}
                                                </label>
                                            </div>

                                            @endif
                                            @empty

                                            @endforelse


                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="kt_tabs_4" role="tabpanel">
                        <div class="">
                            <div class="kt-section">
                                <div class="kt-section__content">

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Ad Account *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            <select class="selectpicker col-12 px-0" id="adsaccount-instagram" name="adsaccount[instagram]"  attr_rrss="instagram">
                                                @forelse ($data['ads_accounts_instagram'] as $account)
                                                <option value="{{ $account->public_id }}" currency="{{ $account->currency }}">{{ $account->name }}</option>
                                                @empty
                                                <option value="">No hay Ads accounts</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Property *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            @php $disabled=''; @endphp
                                            @if(count($data['properties_accounts_instagram']) < 2 )
                                            @php $disabled='disabled'; @endphp
                                            @endif
                                            <select class="selectpicker col-12 px-0" id="properties-instagram" name="properties[instagram]" >
                                                @forelse ($data['properties_accounts_instagram'] as $properties)
                                                <option value="{{ $properties->public_id }}">{{ $properties->name }}</option>
                                                @empty
                                                <option value="">No hay properties</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="">
                                        <div class="gradient" id="tablasel3">
                                            
                                            @forelse ($objectives as $objective)
                                            @if(strpos($objective->metadata,'INSTAGRAM')!==false)
                                            <div class="click mb-3">
                                                <label class="checkbox">
                                                    <input type="radio" name="objetive[instagram]" value=" {{$objective->id}}" metadata="{{$objective->metadata}}" valor="{{$objective->name}}">
                                                    <span></span>
                                                    {{$objective->name}}
                                                </label>
                                            </div>

                                            @endif
                                            @empty

                                            @endforelse
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="kt_tabs_5" role="tabpanel">
                        <div class="">
                            <div class="kt-section">
                                <div class="kt-section__content">

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Ad Account *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            <select class="selectpicker col-12 px-0" id="adsaccount-snapchat" name="adsaccount[snapchat]"  attr_rrss="snapchat">
                                                @forelse ($data['ads_accounts_snapchat'] as $account)
                                                <option value="{{ $account->public_id }}" currency="{{ $account->currency }}">{{ $account->name }}</option>
                                                @empty
                                                <option value="">No hay Ads accounts</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Property *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            @php $disabled=''; @endphp
                                            @if(count($data['properties_accounts_snapchat']) < 2 )
                                            @php $disabled='disabled'; @endphp
                                            @endif
                                            <select class="selectpicker col-12 px-0" id="properties-snapchat" name="properties[snapchat]" {{$disabled}}>
                                                @forelse ($data['properties_accounts_snapchat'] as $properties)
                                                <option value="{{ $properties->public_id }}">{{ $properties->name }}</option>
                                                @empty
                                                <option value="">No hay properties</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="">
                                        <div class="gradient" id="tablasel3">
                                            
                                            @forelse ($objectives as $objective)
                                            @if(strpos($objective->metadata,'SNAPCHAT')!==false)
                                            <div class="click mb-3">
                                                <label class="checkbox">
                                                    <input type="radio" name="objetive[snapchat]" value=" {{$objective->id}}" metadata="{{$objective->metadata}}" valor="{{$objective->name}}">
                                                    <span></span>
                                                    {{$objective->name}}
                                                </label>
                                            </div>

                                            @endif
                                            @empty

                                            @endforelse
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="kt_tabs_6" role="tabpanel">
                        <div class="">
                            <div class="kt-section">
                                <div class="kt-section__content">

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Ad Account *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            <select class="selectpicker col-12 px-0" id="adsaccount-amazon" name="adsaccount[amazon]"  attr_rrss="amazon">
                                                @forelse ($data['ads_accounts_amazon'] as $account)
                                                <option value="{{ $account->public_id }}" currency="{{ $account->currency }}">{{ $account->name }}</option>
                                                @empty
                                                <option value="">No hay Ads accounts</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Property *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            @php $disabled=''; @endphp
                                            @if(count($data['properties_accounts_amazon']) < 2 )
                                            @php $disabled='disabled'; @endphp
                                            @endif
                                            <select class="selectpicker col-12 px-0" id="properties-amazon" name="properties[amazon]" {{$disabled}}>
                                                @forelse ($data['properties_accounts_amazon'] as $properties)
                                                <option value="{{ $properties->public_id }}">{{ $properties->name }}</option>
                                                @empty
                                                <option value="">No hay properties</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="">
                                        <div class="gradient" id="tablasel3">
                                            
                                            @forelse ($objectives as $objective)
                                            @if(strpos($objective->metadata,'AMAZON')!==false)
                                            <div class="click mb-3">
                                                <label class="checkbox">
                                                    <input type="radio" name="objetive[amazon]" value="{{$objective->id}}" metadata="{{$objective->metadata}}" valor="{{$objective->name}}">
                                                    <span></span>
                                                    {{$objective->name}}
                                                </label>
                                            </div>

                                            @endif
                                            @empty

                                            @endforelse
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="kt_tabs_7" role="tabpanel">
                        <div class="">
                            <div class="kt-section">
                                <div class="kt-section__content">

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Ad Account *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            <select class="selectpicker col-12 px-0" id="adsaccount-adwords" name="adsaccount[adwords]"  attr_rrss="adwords">
                                                @forelse ($data['ads_accounts_adwords'] as $account)
                                                <option value="{{ $account->public_id }}" currency="{{ $account->currency }}">{{ $account->name }}</option>
                                                @empty
                                                <option value="">No hay Ads accounts</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="form-group row align-items-center">
                                        <label class="staps-label col-12 col-lg-2">Property *</label>
                                        <div class="col-md-3">
                                            <!-- Cambiar id y agregar otro scritp con el id nuevo en el js de la plantilla -->
                                            @php $disabled=''; @endphp
                                            @if(count($data['properties_accounts_adwords']) < 2 )
                                            @php $disabled='disabled'; @endphp
                                            @endif
                                            <select class="selectpicker col-12 px-0" id="properties-adwords" name="properties[adwords]" {{$disabled}}>
                                                @forelse ($data['properties_accounts_adwords'] as $properties)
                                                <option value="{{ $properties->public_id }}">{{ $properties->name }}</option>
                                                @empty
                                                <option value="">No hay properties</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary btn-icon-right svg-20" name="pixl">Sync <svg xmlns="http://www.w3.org/2000/svg" width="15.834" height="16.667" viewBox="0 0 15.834 16.667"><path d="M11.911,16.423a.833.833,0,0,1,0-1.178l1.078-1.078h-10A2.97,2.97,0,0,1,0,11.226V9.167a.833.833,0,0,1,1.667,0v2.059A1.3,1.3,0,0,0,2.992,12.5h10l-1.078-1.078a.833.833,0,1,1,1.178-1.178l2.5,2.5a.834.834,0,0,1,0,1.179l-2.5,2.5a.834.834,0,0,1-1.178,0ZM14.167,7.5V5.441a1.3,1.3,0,0,0-1.325-1.274h-10L3.923,5.244A.833.833,0,0,1,2.744,6.423l-2.5-2.5a.832.832,0,0,1,0-1.178l2.5-2.5A.833.833,0,1,1,3.923,1.423L2.846,2.5h10a2.97,2.97,0,0,1,2.992,2.941V7.5a.833.833,0,1,1-1.666,0Z" fill="#ffffff"/></svg></button>
                                        </div>
                                    </div>

                                    <div class="">
                                        <div class="gradient" id="tablasel3">
                                            
                                            @forelse ($objectives as $objective)
                                            @if(strpos($objective->metadata,'ADWORDS')!==false)
                                            <div class="click mb-3">
                                                <label class="checkbox">
                                                    <input type="radio" name="objetive[adwords]" value=" {{$objective->id}}" metadata="{{$objective->metadata}}" valor="{{$objective->name}}">
                                                    <span></span>
                                                    {{$objective->name}}
                                                </label>
                                            </div>

                                            @endif
                                            @empty

                                            @endforelse
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
#properties-twitter{
    -webkit-appearance: none;
    -moz-appearance: none;
}
#properties-linkedin{
    -webkit-appearance: none;
    -moz-appearance: none;
}
#properties-adwords{
    -webkit-appearance: none;
    -moz-appearance: none;
}
#properties-amazon{
    -webkit-appearance: none;
    -moz-appearance: none;
}
</style>