<div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
    <div class="kt-form__section kt-form__section--first">
        <div class="kt-wizard-v1__form">
            <h5 class="stap-title">5 BUDGET &AMP; BIDDING</h5>
                
                <div class="row mb-5 align-items-center">
                    <div class="col-md-1">
                        <label class="staps-label">Budget *</label>
                    </div>
                    <div class="col-md-11">
                        <div class="row">
                            <div class="col-md-2">
                                <input type="number" class="form-control" placeholder="Set an amount" name="budget" id="budget">
                            </div>
                            <div class="col-md-2">
                                <select class="selectpicker" placeholder="" name="budgetdistribution">
                                    <option value="2">Total</option>
                                    <option value="1">Per day</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    {{-- <fieldset class="bud-bid">
                        <ul>
                            <li class="radpe_btn">
                                <input type="radio" name="budgetdistribution" id="radiobot1" value="2" class="pizq" />
                                <label for="radiobot1"> Total</label>
                                <input type="radio" name="budgetdistribution" id="radiobot2" value="1" class="pder" checked="checked" />
                                <label for="radiobot2">Per day</label>
                            </li>
                        </ul>
                    </fieldset> --}}
                </div>

                <div class="" id='budget_split'>
                    {{-- <label class="col-form-label col-lg-1"></label> --}}
                       
                        @foreach ($Array_RSS as $this_rrss)
                       
                        <div class="budgetrs" id="b{{strtolower($this_rrss)}}">
                            <h6 class="stap-subtitle">{{$this_rrss}}</h6>
                            <div class="form-group row align-items-center">
                                <div class="col-md-1">
                                    <label class="staps-label">Price</label>
                                </div>
                                <div class="col-md-11">
                                    <div class="row align-items-center">
                                        <div class="col-md-2"><input type="number" class="form-control bi_rrss" placeholder="Set an amount" name="bi{{strtolower($this_rrss)}}" id="bi{{strtolower($this_rrss)}}" ></div>
                                        <div class="col-md-2">
                                            <select name="br{{strtolower($this_rrss)}}" class="selectpicker w-100" id="bp{{strtolower($this_rrss)}}">
                                                <option value="1" selected>Fixed amount</option>
                                                <option value="2" id="bp{{strtolower($this_rrss)}}">Percentage</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2"><label class="staps-label">Optimize for</label></div>
                                        <div class="col-md-2 bud_dist">
                                            <select class="selectpicker w-100" id="optimize_{{strtolower($this_rrss)}}" name="optimize_{{strtolower($this_rrss)}}">
                                                <?php 
                                                /*<option value="AK">Link clicks</option>
                                                <option value="HI">Impressions</option>
                                                <option value="HI">Unique daily reach</option>
                                                <option value="HI">Landing Page Views</option>
                                                <option value="HI">Post engagement</option>*/
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2"><label class="staps-label">Pay for *</label></div>
                                        <div class="col-md-2 bud_dist">
                                            <select class="selectpicker w-100" id="pay_{{strtolower($this_rrss)}}" name="pay_{{strtolower($this_rrss)}}">
                                                <?php 
                                                /*  <option value="AK">Link clicks</option>
                                                <option value="HI">Impressions</option>*/
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                {{-- <div class="col-6">
                                    <div class="input-group-append"><span class="input-group-text" id="price-{{strtolower($this_rrss)}}"></span></div> 
                                </div> --}}
                                
                                
                                {{-- <label class="checkbox kt-checkbox--bold kt-checkbox--brand cbbadget" style='margin-left: 2px;'>
                                    <input type="radio" name="br{{strtolower($this_rrss)}}"  value="1" checked/> Fixed amount
                                    <span></span>
                                </label>
                                <label class="checkbox kt-checkbox--bold kt-checkbox--brand cbbadget" style="float: right; margin-top: -25px; margin-right: -4px;">
                                    <input type="radio" name="br{{strtolower($this_rrss)}}"  value="2" />
                                    <span style='margin-left: 18px; margin-top: 25px;'></span>
                                </label>
                                <div class="col-5 text-right">
                                    <label class="col-lg-2 staps-label">Percentage</label>
                                    <input type="text" class="form-control form-porcen" placeholder="" name="bp{{strtolower($this_rrss)}}" >
                                </div> --}}
                            </div>

                            <div class="mb-5 row align-items-center">
                                <div class="col-md-1">
                                    <label class="staps-label">Bidding *</label>
                                </div>
                                <div class="col-md-11">
                                    <div class="row align-items-center">
                                        <div class="col-md-4">
                                            <label class="radio">
                                                <input type="radio" val="lowest_cost" name="bid_{{strtolower($this_rrss)}}" rrss="{{strtolower($this_rrss)}}" class="low-top"><span></span><div style="width: calc(100% - 34px);"><b> Lowest cost </b>Get the most link clicks for your budget
                                            </div></label>
                                        </div>
                                        <div class="col-md-8">
                                            <label class="radio">
                                                <input type="radio" val="lowest_cost_bid_cap" name="bid_{{strtolower($this_rrss)}}" rrss="{{strtolower($this_rrss)}}"
                                                    class="low-top low_top_rad" checked><span></span><div style="width: calc(100% - 34px);"><b>Lowest cost with bid cap </b>Insert the target price you'd like to pay for an action. <i>This price is not the cpm bid.</i></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                    <!-- Campo dependiente radio "coste + bajo con tope"   -->
                            </div>
                            <div id="low_cost_top_{{strtolower($this_rrss)}}" class="mb-5 row align-items-center" style="display: none;">
                                <div class="col-md-1">
                                        <label class="staps-label">Bid</label>
                                </div>
                                <div class="col-md-11">
                                    <input type="text" class="form-control" name="low_cost_top_{{strtolower($this_rrss)}}" id="input_low_cost_top_{{strtolower($this_rrss)}}" placeholder="" required>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="error_bidding alert-danger px-4 py-2" style="display:none; width:100%"></div>
                
                </div>
                <div class="row align-items-center mb-5">
                    <div class="col-md-1">
                        <label class="staps-label">Start Date *</label>
                    </div>
                    <div class="col-md-11">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="input-daterange input-group" id="kt_datepicker_5">
                                    <input type="text" class="form-control" name="start_date" id="start_date" placeholder="dd/mm/yyyy"/>
                                </div>
                                <div id="start_date_error_validate"></div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-daterange input-group" id="kt_datepicker_5">
                                    <input type="text" class="form-control" name="end_date" id="end_date" placeholder="dd/mm/yyyy"/>
                                </div>
                                <div id="end_date_error_validate"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <h6 class="stap-subtitle">Budget target</h6>
				<div class="row align-items-center">
                    <div class="col-md-1">
                         <label class="staps-label">Diary</label>
                    </div>
                    <div class="col-md-11">
                        <div class="row align-items-center">
                            <div class="col-md-3">
                                <input type="number" step="1" class="form-control" placeholder="Set an amount" name="budget_target_diary" id="budget_target_diary">
                            </div>
                            <div class="col-md-3 bud_dist">
                                <select class="selectpicker w-100" id="budget_target_diary_type" name="budget_target_diary_type">
                                    <option value="CLICKS">Clicks</option>
                                    <option value="IMPRESSIONS">Impressions</option>
                                    <option value="SPEND">Spend</option>
                                    <option value="CONVERSIONS">Conversions</option>
                                </select>
                            </div>
                            <div class="col-md-1">
                                <label class="staps-label">Total</label>
                            </div>
                            <div class="col-md-3">
                                <input type="number" step="1" class="form-control" placeholder="Set an amount" name="budget_target_total" id="budget_target_total">
                            </div>
                            <div class="col-md-2 bud_dist">
                                <select class="selectpicker w-100" id="budget_target_total_type" name="budget_target_total_type">
                                    <option value="CLICKS">Clicks</option>
                                    <option value="IMPRESSIONS">Impressions</option>
                                    <option value="SPEND">Spend</option>
                                    <option value="CONVERSIONS">Conversions</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <h6 class="stap-subtitle">Advanced</h6>

                <div class="row mb-5 align-items-center">
                    <div class="col-md-1">
                        <label class="staps-label">Spend on each ad</label>
                    </div>
                    <div class="col-md-11">
                        <div class="row align-items-center">
                            <div class="col-md-5">
                                <input type="number" step="1" class="form-control" placeholder="" name="spend_on_each_ad" id="spend_on_each_ad">
                            </div>
                            <div class="col-md-2">
                                <label class="staps-label">Impressions per day</label>
                            </div>
                            <div class="col-md-5">
                                <input type="number" step="1" class="form-control" placeholder="" name="impressions_per_day" id="impressions_per_day">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-md-3">
                        <label class="staps-label">Run only on schedule</label>
                    </div>
                    <div class="col-md-11 ml-auto">
                        <div id="weekly-schedule" class="table-schedule"></div>
                    </div>
                </div>

             <!--   <div class="accordion accordion-light accordion-toggle-plus" id="accordionExample6">
                    <div class="card">
                        <div class="card-header" id="headingOne6">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseOne6"
                                aria-expanded="true" aria-controls="collapseOne6"> Optimize for link clicks and pay for
                                impressions using lowest cost.
                            </div>
                        </div>
                        <div id="collapseOne6" class="collapse" aria-labelledby="headingOne6"
                            data-parent="#accordionExample6">
                            <div class="card-body">

                                <div class="form-group row align-items-center">
                                    <label class="col-md-3 col-form-label"><a href="#" class="tooltip-test"
                                            data-toggle="kt-tooltip" title=""
                                            data-original-title="This allows you to set the overall spending limit for an entire campaign. It doesn't affect your ads' delivery, but all the ads in your Campaign will stop running once this amount is reached."><i
                                                class="fa fa-question-circle"></i></a> Spend Cap
                                    </label>
                                    <div class="input-group budbid">
                                        <input type="text" class="form-control" name="spend_cap" placeholder=""
                                            required>
                                        <div class="input-group-append"><span class="input-group-text"
                                                id="basic-addon1">HKD</span></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="accordion accordion-light accordion-toggle-plus" id="accordionExample3">
                    <div class="card">
                        <div class="card-header" id="headingOne3">
                            <div class="card-title collapsed btn btn-basic" data-toggle="collapse" data-target="#collapseOne3" aria-expanded="true" aria-controls="collapseOne3">Advanced options | Dayparting, Adset and Ads name template</div>
                        </div>
                        <div id="collapseOne3" class="collapse" aria-labelledby="headingOne3" data-parent="#accordionExample3">

                            <div class="card-body">
                                <div class="form-group align-items-center">
                                    <label class="staps-label col-2">IO</label>
                                    <input type="text" class="form-control col-md-7" name="IO"
                                        placeholder="Define the template for your Adsets naming">
                                </div>
                                <div class="form-group align-items-center">
                                    <label class="staps-label col-2">Brand name</label>
                                    <input type="text" class="form-control col-md-7" name="brand_name"
                                        placeholder="Define the template for your Adsets naming">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>