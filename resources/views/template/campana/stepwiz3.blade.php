<div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
	<div class="kt-form__section kt-form__section--first">
		<div class="kt-wizard-v1__form">
			<h5 class="stap-title">3 PLACEMENTS</h5>
			<div class="kt-section__content">
					<div class="row" id="tablasel3">
							@php
								$old_section = "";
								$i=0;
							@endphp
							@foreach ($data['placements'] as $place)
								@php
									$i++; 
								@endphp
								@if ($place->platforms_name != $old_section)

									@if ($old_section !="")
										</ul>
										</div>
										</div>
									@endif
			
									@php
										$old_section = $place->platforms_name; 
									@endphp
	
									<div class="col-12 my-3 placements_{{ strtolower($place->platforms_name) }}" style="display:none;">
										<div class="">
											<h6 class="stap-subtitle">{{ $place->platforms_name }}</h6>
											<div class="error_placements_{{ strtolower($place->platforms_name) }} alert-danger px-4 py-2" style="display:none; width:100%">This field is required.</div>
												<ul class="btnft social-select">
													<li class="rad_btn"><input type="checkbox" id="checkbtn_place_all_{{ strtolower($place->platforms_name) }}"  value="" tab="0" class="redSocial" /><label for="checkbtn_place_all_{{ strtolower($place->platforms_name) }}">ALL</label></li>
													<li class="rad_btn"><input type="checkbox" name="placement[{{ strtolower($place->platforms_name) }}][]" valor="{{$place->name}}" id="checkbtn_place{{$i}}" value="{{ $place->public_id }}" tab="{{$i}}" class="redSocial" rrss="{{ strtolower($place->platforms_name) }}" data-option="#kt_ptabs_{{$i}}" /><label for="checkbtn_place{{$i}}"><!--i class="fab fa-facebook-square"></i-->{{ $place->name }}</label></li>
													@else
														<li class="rad_btn"><input type="checkbox" name="placement[{{ strtolower($place->platforms_name) }}][]" valor="{{$place->name}}" id="checkbtn_place{{$i}}" value="{{ $place->public_id }}" tab="{{$i}}" class="redSocial" rrss="{{ strtolower($place->platforms_name) }}" data-option="#kt_ptabs_{{$i}}" /><label for="checkbtn_place{{$i}}"><!--i class="fab fa-facebook-square"></i-->{{ $place->name }}</label></li>
													@endif

												@endforeach				
												</ul>
											</div>
										</div>
						
					</div>
			</div>
		</div>
	</div>
</div>
