<style>
	.containerImagenes{
		display: grid;
		grid-template-columns: repeat(3, 1fr);
		grid-template-rows: repeat(auto, 1fr);
		grid-column-gap: 0px;
		grid-row-gap: 0px;
	}

	.img_preview{
		padding: 6px;
	    width: 160px;
    	height: 160px;
		display: none;
	}

	</style>
			<h6 class="stap-subtitle">Create</h6>

			<!-- Campo dependiente del boton Formato normal -->
			<div id="formt_norm">
				<div class="row mb-5">
					<div class="col-md-2">
						<label class="staps-label">Headlines</label>
					</div>
					<div  class="col-md-10" id="kt_repeater_1">
						<div data-repeater-list="titulo">
							<div data-repeater-item class="form-group row align-items-center">
								<div class="col-md-9">
									<div class="kt-form__group--inline">
										<div class="kt-form__label">
										</div>
										<div class="kt-form__control">
											<input type="text" class="form-control" placeholder="Insert headline and use + to test more..." name="titulo">
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div data-repeater-delete="" class="btn-sm btn btn-icon btn-danger deltitulo">
										<span>
											<svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
										<!--	<span>Delete</span> -->
										</span>
									</div>
								</div>
								<div class="col-md-9">
									<div class="css-tagsinput">					
										<input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..." name="titulotags" />
									</div>
								</div>
							</div>
						</div>
						<div data-repeater-create="" class="btn btn btn-sm btn-info addtitulo">
							<span><i class="la la-plus"></i><span>Add</span></span>
						</div>
					</div>
				</div>
				<div class="row mb-5">
					<div class="col-md-2">
						<label class="staps-label">Ad Texts</label>
					</div>
					<div  class="col-lg-10" id="kt_repeater_7">
						<div data-repeater-list="descripcion">
							<div data-repeater-item class="form-group row align-items-center">
								<div class="col-md-9">
									<div class="kt-form__group--inline">
										<div class="kt-form__label">
										</div>
										<div class="kt-form__control">
											<textarea class="form-control"  rows="3" name="descripcion" placeholder="Insert an ad text and use + to test more..."></textarea>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div data-repeater-delete="" class="btn-sm btn btn-icon btn-danger deldescripcion">
										<span>
											<svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
										<!--	<span>Delete</span> -->
										</span>
									</div>
								</div>
								<div class="col-md-9">
									<div class="css-tagsinput">					
										<input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..." name="descripciontags"/>
									</div>
								</div>
							</div>
						</div>
						<div data-repeater-create="" class="btn btn btn-sm btn-info adddescripcion">
							<span>
								<i class="la la-plus"></i>
								<span>Add</span>
							</span>
						</div>
					</div>

					<?php /*<div class="row etiq">
						<label class="col-sm-1"><i class="fa fa-handshake"></i></label>
						<label class="col-form-label col-lg-2">
						Tag Business Partner
						</label>
						<div class="col-md-7 text-muted">To tag a Business Partner, please select an eligible Page first.
						</div>
					</div> */ ?>
				</div>
				<div class="row mb-5" id="kt_repeater_14">
					<div class="col-md-2">
						<a href="#" class="tooltip-test" data-toggle="kt-tooltip" title="" data-original-title="Images: Minimiun size for upload 1200x638px.">
							<svg id="_27_Icon_question-mark-circle" data-name="27) Icon/question-mark-circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M10,20A10,10,0,1,1,20,10,10.011,10.011,0,0,1,10,20ZM10,2a8,8,0,1,0,8,8A8.009,8.009,0,0,0,10,2Zm0,14a1,1,0,1,1,1-1A1,1,0,0,1,10,16Zm0-3a1,1,0,0,1-1-1V10a1,1,0,0,1,1-1A1.5,1.5,0,1,0,8.5,7.5a1,1,0,0,1-2,0A3.5,3.5,0,1,1,11,10.837V12A1,1,0,0,1,10,13Z" transform="translate(2 2)" fill="#222b45"/> </svg>
						</a>
						<label class="staps-label">Upload new images/videos</label>
					</div>
					<div  class="col-lg-10" >
						<div data-repeater-list="imagen" class="containerImagenes">
							<div data-repeater-item class="row mb-3">
								<div class="col-9 mb-3">
									<div class="kt-form__group--inline">
										<div class="kt-form__label"></div>
										<div class="kt-form__control">
											<div class="input-group">
												<div class="custom-file">
												<input type="file" class="form-control custom-file-input" aria-describedby="inputGroupFileAddon01" name="imagen" onchange="javascript:imgUpload(this);">
												<label class="custom-file-label" style='overflow: hidden;' name="imagenlabel">Choose file</label>
												</div>
											</div>
										</div>
									</div>
									<div data-role="imagenpreview" name="imagenpreview" class='img_preview' style="	background-size: cover; margin-top: 13px;"></div>
									<video name="imagenpreview_video_player" autoplay='true' controls class='img_preview'></video>
									<div data-role="imagenresponse" name="imagenresponse" style="padding: 10px;"></div>
								</div>
								<div class="col-3 mb-3">
									<div data-repeater-delete="" class="btn-sm btn btn-icon btn-danger delimagen"><span><svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg></span></div>
								</div>
								<div class="col-12" name="imagen_inputs_tags" style="display: none;">
									<div class="css-tagsinput">
										<input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..." name="imagentags"/>
									</div>
								</div>
							</div>
						</div>
						<div data-repeater-create class="btn btn btn-sm btn-info addimagen" style="margin-top: 10px;">
							<span><i class="la la-plus"></i><span>Add</span></span>
						</div>
					</div>
				</div>
				
				<div class="row mb-5">
					<div  class="col-md-2">
						<label class="staps-label">URLs *</label>
					</div>
					<div  class="col-lg-10" id="kt_repeater_8">
						<div data-repeater-list="url">
							<div data-repeater-item class="form-group row align-items-center">
								<div class="col-md-9">
									<div class="kt-form__group--inline">
										<div class="kt-form__label"></div>
										<div class="kt-form__control">
											<input type="text" class="form-control" placeholder="Insert URL and use + to test more..." name="url">
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div data-repeater-delete="" class="btn-sm btn btn-icon btn-danger delurl">
										<span>
											<svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
										<!--	<span>Delete</span> -->
										</span>
									</div>
								</div>
								<div class="col-md-9">
									<div class="css-tagsinput">
										<input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..." name="urltags"/>
									</div>
								</div>
							</div>
						</div>
						<div data-repeater-create="" class="btn btn-sm btn-info addurl">
							<span><i class="la la-plus"></i><span>Add</span></span>
						</div>
					</div>
				</div>
				<div class="row mb-5">
					<div class="col-md-2 d-flex align-items-center">
						<a href="#" class="tooltip-test" data-toggle="kt-tooltip" title="" data-original-title=" If your website link is long, you can enter text here to create a shorter, friendlier link that people will see on your ad.">
							<svg id="_27_Icon_question-mark-circle" data-name="27) Icon/question-mark-circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M10,20A10,10,0,1,1,20,10,10.011,10.011,0,0,1,10,20ZM10,2a8,8,0,1,0,8,8A8.009,8.009,0,0,0,10,2Zm0,14a1,1,0,1,1,1-1A1,1,0,0,1,10,16Zm0-3a1,1,0,0,1-1-1V10a1,1,0,0,1,1-1A1.5,1.5,0,1,0,8.5,7.5a1,1,0,0,1-2,0A3.5,3.5,0,1,1,11,10.837V12A1,1,0,0,1,10,13Z" transform="translate(2 2)" fill="#222b45"/> </svg>
						</a> 
						<label class="staps-label">Display Link</label>
					</div>
					<div class="col-md-10">
						<input type="text" class="form-control disp_link ml-0" name="link_display" placeholder="Insert a link caption...">
					</div>
				</div>

				<div class="mb-5 row">
					<div class="col-md-2 staps-label">
						<label class="staps-label">Link Description</label>
					</div>
					<div  class="col-md-10" id="kt_repeater_5">
						<div data-repeater-list="text_link">
							<div data-repeater-item class="form-group row align-items-center">
								<div class="col-md-9">
									<div class="kt-form__group--inline">
										<div class="kt-form__label">
										</div>
										<div class="kt-form__control">
											<textarea   class="form-control"  rows="3" placeholder="Insert a link description and use + to test more..." name="text_link"></textarea>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div data-repeater-delete="" class="btn-sm btn btn-icon btn-danger deltext_link">
										<span>
											<svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
										<!--	<span>Delete</span> -->
										</span>
									</div>
								</div>
								<div class="col-md-9">
									<div class="css-tagsinput">
										<input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..." name="text_linktags" />
									</div>
								</div>
							</div>
						</div>
						<div data-repeater-create="" class="btn btn-sm btn-info addtext_link">
							<span><i class="la la-plus"></i><span>Add</span></span>
						</div>
					</div>
				</div>

				<div class="row mb-5">
					<div class="col-md-2 staps-label">
						<label class="staps-label">Call To Action</label>
					</div>
					<div class="col-md-10" id="kt_repeater_4">
						<div data-repeater-list="call_to_action">
							<div data-repeater-item class="form-group row align-items-center">
								<div class="col-md-9">
									<div class="kt-form__group--inline">
										<div class="kt-form__label">
										</div>
										<div class="kt-form__control">
											<select   class="form-control" id="exampleSelect1" aria-invalid="false" name="call_to_action">
												<option value="" data-hidden="false"></option>
												<option value="REQUEST_TIME">Request Time</option>
												<option value="SEE_MENU">See Menu</option>
												<option value="SHOP_NOW">Shop Now</option>
												<option value="SIGN_UP">Sign Up</option>
												<option value="SUBSCRIBE">Subscribe</option>
												<option value="WATCH_MORE">Watch More</option>
												<option value="LISTEN_NOW">Listen Now</option>
												<option value="APPLY_NOW">Apply Now</option>
												<option value="BOOK_TRAVEL">Book Now</option>
												<option value="CONTACT_US">Contact Us</option>
												<option value="DOWNLOAD">Download</option>
												<option value="GET_OFFER">Get Offer</option>
												<option value="GET_QUOTE">Get Quote</option>
												<option value="GET_SHOWTIMES">Get Showtimes</option>
												<option value="LEARN_MORE">Learn More</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div data-repeater-delete="" class="btn-sm btn btn-icon btn-danger delcall_to_action">
										<span>
											<svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
										<!--	<span>Delete</span> -->
										</span>
									</div>
								</div>
								<div class="col-md-9">
									<div class="css-tagsinput">
										<input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..." name="call_to_actiontags" />
									</div>
								</div>
							</div>
						</div>
						<div data-repeater-create="" class="btn btn btn-sm btn-info addcall_to_action">
							<span>
								<i class="la la-plus"></i>
								<span>Add</span>
							</span>
						</div>
						<div>
						</div>
					</div>
				</div>
			</div>

			<hr class="col-md-9">
			<h6 class="stap-subtitle">Goal Tracking</h6>

			<div class="kt-section">
				<div class="mb-5">
					<div class="kt-checkbox-list">
						<!-- .....................Campo dependiente de checkbox 1.2...................... -->
						<div class="XXXXXXXclase para JS mb-3">
							<label class="checkbox">
								<input type="checkbox" id="go_track1" name="trak">
								<span></span>
								Track this campaign with <b>Google Analitics</b>
							</label>
							<div class="kt-portlet my-4" id="go-track1-capa" style="display: none;">
								<div class="form-group row">
									<div class="col-md-2">
										<label class="staps-label">utm_Source *</label>
									</div>
									<div class="col-md-10">
										<input type="text" class="form-control" placeholder="facebook..." name="GA_source" id="GA_source">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-2">
										<label class="staps-label">utm_Medium **</label>
									</div>
									<div class="col-md-10">
										<input type="text" class="form-control" placeholder="paidsocial" name="GA_medium" id="GA_medium">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-2">
										<label class="staps-label">Campaign Name</label>
									</div>
									<div class="col-md-10">
										<input type="text" class="form-control" name="GA_campaign" id="GA_campaign">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-2">
										<label class="staps-label">
											<a href="#" class="tooltip-test" data-toggle="kt-tooltip" title="" data-original-title="Send Ads' specific information to Google Analytics for better tracking & control. Select from the drop down which data you want to pass.">
												<svg id="_27_Icon_question-mark-circle" data-name="27) Icon/question-mark-circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M10,20A10,10,0,1,1,20,10,10.011,10.011,0,0,1,10,20ZM10,2a8,8,0,1,0,8,8A8.009,8.009,0,0,0,10,2Zm0,14a1,1,0,1,1,1-1A1,1,0,0,1,10,16Zm0-3a1,1,0,0,1-1-1V10a1,1,0,0,1,1-1A1.5,1.5,0,1,0,8.5,7.5a1,1,0,0,1-2,0A3.5,3.5,0,1,1,11,10.837V12A1,1,0,0,1,10,13Z" transform="translate(2 2)" fill="#222b45"/> </svg></a>Content
										</label>
									</div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-8">
												<input type="text" class="form-control" placeholder="ads specific information" name="GA_content_placeholder" id="GA_content_placeholder">
											</div>
											<div class="col-md-4">
												<div class="input-group-append">
													<div class="dropdown bootstrap-select">
														<button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" name="trakbtn">
															Insert Placeholder
														</button>
														<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
															<div class="inner show" style="max-height: 325px; overflow-y: auto;">
																<ul class="dropdown-menu inner show">
																	<?php 
																		$url_place = json_decode($data['url_placeholder'][0]->value);
																		foreach($url_place as $key => $value) {
																	?>
																	<li><a class="dropdown-item" href="javascript:appendPlaceHolder('GA_content_placeholder', '<?php echo $value ?>');"><?php echo $key ?></a></li>
																	<?php } ?>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--  .................. Campo dependiente de checkbox 2.2........................... -->
						<div class="XXXXXXXclase para JS mb-3">
							<label class="checkbox">
								<input type="checkbox" id="go_track2" name="">
								<span></span>
								Add extra options to your URL
							</label>
							<div class="kt-portlet my-4" id="go-track2-capa" style="display: none;">
								<div class="form-group row">
									<div class="col-md-2">
										<label class="staps-label">Url Parameters</label>
									</div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-8">
												<input type="text" class="form-control" placeholder="e.g. ref=facebook&gender={gender}" name="url_parameter_extra" id="url_parameter_extra">
											</div>
											<div class="col-md-4">
												<div class="input-group-append">
													<div class="dropdown">
														<button class="form-control dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" name="btndrop">
															Insert Placeholder
														</button>
														<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
															<?php 
															$url_place = json_decode($data['url_placeholder'][0]->value);
															foreach($url_place as $key => $value) {
																?>
																<a class="dropdown-item" href="javascript:appendPlaceHolder('url_parameter_extra', '<?php echo $value ?>');"><?php echo $key ?></a>
															<?php } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--  .................. Campo dependiente de checkbox 3.2........................... -->
						<div class="XXXXXXXclase para JS mb-3">
							<label class="checkbox">
								<input type="checkbox" id="go-track-viewpixel" name="">
								<span></span>
								Impression Pixel
							</label>
							<div class="kt-portlet my-4" id="go-track-viewpixel-capa" style="display: none;">
								<div class="form-group row">
									<div class="col-md-2">
										<label class="staps-label">Impression Tag</label>
									</div>
									<div class="col-md-10">
										<textarea class="form-control" rows="6" placeholder="e.g. moat script" id="pixelview" name="pixelview"></textarea>	 
									</div>
								</div>
							</div>
						</div>
						<!-- ........................Campo dependiente de checkbox 4.2...................... -->
						<div class="XXXXXXXclase para JS mb-3">
							<label class="checkbox">
								<input type="checkbox" id="go_track3">
								<span></span>
								Track goal conversions with <b>Pixels</b>
							</label>
							<div class="kt-portlet my-4" id="go-track3-capa" style="display: none;">

								<div class="form-group row pixels-facebook" style="display:none">
									<div class="col-md-2"><label class="staps-label">Facebook Pixels</label></div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-8">
												<select class="selectpicker w-100" name="pixels[facebook]" id="pixels-facebook">
													<option value="">No hay pixels</option>
												</select>
											</div>
											<div class="col-md-4">
												<button type="button" class="btn btn-icon-right btn-info pixel_refresh_facebook" onClick="javascript:sync_account('facebook');" name="pixl">Sync <svg id="_27_Icon_Sync" data-name="27) Icon/Sync" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"></path> </svg></button>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row pixels-twitter" style="display:none">
									<div class="col-md-2"><label class="staps-label">Twitter Pixels</label></div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-8">
												<select class="selectpicker w-100" name="pixels[twitter]" id="pixels[twitter]">
													<option value="">No hay pixels</option>
												</select>
											</div>
											<div class="col-md-4">
												<button type="button" class="btn btn-icon-right btn-info pixel_refresh_twitter" onClick="javascript:sync_account('twitter');" name="pixl">Sync <svg id="_27_Icon_Sync" data-name="27) Icon/Sync" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"></path> </svg></button>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row pixels-linkedin" style="display:none">
									<div class="col-md-2"><label class="staps-label">Linkedin Pixels</label></div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-8">
												<select class="selectpicker w-100" name="pixels[linkedin]" id="pixels[linkedin]">
													<option value="">No hay pixels</option>
												</select>
											</div>
											<div class="col-md-4">
												<button type="button" class="btn btn-icon-right btn-info pixel_refresh_linkedin" onClick="javascript:sync_account('linkedin');" name="pixl">Sync <svg id="_27_Icon_Sync" data-name="27) Icon/Sync" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"></path> </svg></button>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row pixels-instagram" style="display:none">
									<div class="col-md-2"><label class="staps-label">Instagram Pixels</label></div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-8">
												<select class="selectpicker w-100" name="pixels[instagram]" id="pixels[instagram]">
													<option value="">No hay pixels</option>
												</select>
											</div>
											<div class="col-md-4">
												<button type="button" class="btn btn-icon-right btn-info pixel_refresh_instagram" onClick="javascript:sync_account('instagram');" name="pixl">Sync <svg id="_27_Icon_Sync" data-name="27) Icon/Sync" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"></path> </svg></button>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row pixels-snapchat" style="display:none">
									<div class="col-md-2"><label class="staps-label">Snapchat Pixels</label></div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-8">
												<select class="selectpicker w-100"  name="pixels[snapchat]" id="pixels[snapchat]">
													<option value="">No hay pixels</option>
												</select>
											</div>
											<div class="col-md-4">
												<button type="button" class="btn btn-icon-right btn-info pixel_refresh_snapchat" onClick="javascript:sync_account('snapchat');" name="pixl">Sync <svg id="_27_Icon_Sync" data-name="27) Icon/Sync" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"></path> </svg></button>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row pixels-amazon" style="display:none">
									<div class="col-md-2"><label class="staps-label">Amazon Pixels</label></div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-8">
												<select class="selectpicker w-100" id="pixels[amazon]" name="pixels[amazon]">
													<option value="">No hay pixels</option>
												</select>
											</div>
											<div class="col-md-4">
												<button type="button" class="btn btn-icon-right btn-info pixel_refresh_amazon" onClick="javascript:sync_account('amazon');"  name="pixl">Sync <svg id="_27_Icon_Sync" data-name="27) Icon/Sync" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"></path> </svg></button>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row pixels-adwords" style="display:none">
									<div class="col-md-2"><label class="staps-label">Adwords Pixels</label></div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-8">
												<select class="selectpicker w-100" id="pixels[adwords]" name="pixels[adwords]">
													<option value="">No hay pixels</option>
												</select>
											</div>
											<div class="col-md-4">
												<button type="button" class="btn btn-icon-right btn-info pixel_refresh_adwords" name="pixl">Sync <svg id="_27_Icon_Sync" data-name="27) Icon/Sync" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"></path> </svg></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Fin de campos dependientes de checkbox -->
						</div>
						<!-- Fin de campos dependientes de checkbox -->
					</div>
				</div>
			</div>

