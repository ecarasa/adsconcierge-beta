<style type="text/css">

    .kt-wizard-v1 .kt-wizard-v1__wrapper {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: space-between;
        background-color: #f9fafc;
        align-items: center;
        flex-wrap: nowrap;
        flex-direction: row;
        align-content: stretch;
    }

    .rad_btn {
        width: auto;
        list-style: none;
        background-color: #f9f9fc;
        padding: 0px !important;
        border: 0px solid #3b5998 !important;
    }

</style>

<div class="modal fade" id="modalCampaign" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Campaign</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="kiki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px;">
                    <div class="kt-portlet">
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-grid  kt-wizard-v1 kt-wizard-v1--white" id="kt_wizard_v1_edit"
                                data-ktwizard-state="step-first">
                                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v1__wrapper">
                                    <form class="kt-form kt-form22" id="kt_form_edit" autocomplete="off" style="width: 100%!important; padding: 0rem!important;">
                                        @include('template.modalEdition.editionCampaign')
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-basic" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary " id="saveBtnCustomer" onClick="javascript:saveCampaign();">Save</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $('.redSocial_edit').click(function() {

        var is_block = $(this).attr('blocked');

        if (is_block) {
            window.location.href = "{{ route('mysubscription') }}";
            return false;
        }
        
        var tab = $(this).attr('tab');

        if ($(this).is(':checked')) {
            $("a[href='#kt_tabs_" + tab + "']").parent().css('display', 'block');
            $("a[href='#kt_tabs_" + tab + "']").click();
            $("a[href='#kt_tabs_audience_" + tab + "']").parent().css('display', 'block');
            $("a[href='#kt_tabs_audience_" + tab + "']").click();
            //checkRedActive();
            //fcross_plataformas(tab, 1);
        } else {
            $("a[href='#kt_tabs_" + tab + "']").parent().css('display', 'none');
            $("a[href='#kt_tabs_audience" + tab + "']").parent().css('display', 'none');
            //checkRedActive();
            //fcross_plataformas(tab, 0);
        }
    });

</script>
