<div class="kt-wizard-v1__content" data-ktwizard-type="step-content" data-ktwizard-state="current" id="step_1">
    <div class="kt-form__section kt-form__section--first">
        <div class="kt-wizard-v1__form">
            <div class="form-group row align-items-center">
                <label class="col-lg-3">Campaign Name *</label>
                <input type="text" class="form-control col-lg-9" name="campaign_platform_name" id="campaign_platform_name">
                <input type="hidden" class="form-control col-lg-4" name="campaign_platform_id" id="campaign_platform_id">
            </div>

            <hr>
            <div class="form-group row align-items-center">
                <label class="col-lg-3">Platform </label>
                <label class="col-lg-3" id="campaign_platform_platform">Platform</label>
            </div>
            <hr>

            <div class="row align-items-center">
                <label class="col-lg-3">Budget Daily *</label>
                <div class="input-group budbid">
                    <input type="number" class="form-control" placeholder="" name="campaign_platform_budget" id="campaign_platform_budget">
                </div>
                <label class="col-lg-3">Budget Total *</label>
                <div class="input-group budbid">
                    <input type="number" class="form-control" placeholder="" name="campaign_platform_budget_total" id="campaign_platform_budget_total">
                </div>
            </div>


        </div>
    </div>
</div>

<style>
    #properties-twitter {
        -webkit-appearance: none;
        -moz-appearance: none;
    }

    #properties-linkedin {
        -webkit-appearance: none;
        -moz-appearance: none;
    }

    #properties-adwords {
        -webkit-appearance: none;
        -moz-appearance: none;
    }

    #properties-amazon {
        -webkit-appearance: none;
        -moz-appearance: none;
    }

    .kt-wizard-v1 .kt-wizard-v1__wrapper .kt-form .kt-form22 {
        width: 100% !important;
        padding: 0rem !important;
    }

</style>
