<div class="kt-wizard-v1__content" data-ktwizard-type="step-content" data-ktwizard-state="current" id="step_1">
    <div class="kt-form__section kt-form__section--first">
        <div class="kt-wizard-v1__form">
            <div class="form-group row align-items-center">
                <div class="col-lg-3">
                    <label>Campaign Name *</label>
                </div>
                <div class="col-lg-9">
                    <input type="text" class="form-control" name="campaign_name" id="campaign_name">
                    <input type="hidden" class="form-control" name="customer_id" id="customer_id">
                    <input type="hidden" class="form-control" name="campaign_id" id="campaign_id">
                </div>
            </div>

            <hr>
            <div class="form-group row align-items-center">
                <div class="col-lg-3">
                    <label>Platforms</label>
                </div>
            </div>

            <div id="social_rrss"> </div>
            <hr>
            <div class="form-group row align-items-center">
                <div class="col-lg-3">
                    <label>Budget *</label>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="number" class="form-control w-100" placeholder="" name="budget" id="budget">
                        </div>
                        <div class="col-md-6">
                            <fieldset class="bud-bid">
                                <ul class="mb-0">
                                    <li class="radpe_btn">
                                        <input type="radio" name="budgetdistribution" id="radiobot1" value="2" class="pizq" /><label
                                            for="radiobot1"> Total</label>
                                        <input type="radio" name="budgetdistribution" id="radiobot2" value="1" class="pder" /><label
                                            for="radiobot2">Per day</label>
                                    </li>
                                </ul>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <label class="col-form-label col-lg-1"></label>
                <div class="col-lg-9 col-md-9 col-sm-12" id='budget_split'>

                </div>
            </div>
            <hr />

            <h5 class="modal-subtitle">Advanced</h5>

            <div class="form-group row align-items-center">
                <div class="col-lg-3">
                    <label>Spend on each ad</label>
                </div>
                <div class="col-lg-9">
                    <input type="number" class="form-control" placeholder="" name="spend_on_each_ad"
                        id="spend_on_each_ad">
                </div>
            </div>
            <div class="form-group row align-items-center">
                <div class="col-lg-3">
                    <label>Impressions per day</label>
                </div>
                <div class="col-lg-9">
                    <input type="number" class="form-control" placeholder="" name="impressions_per_day"
                        id="impressions_per_day">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    <label>Run only on schedule</label>
                </div>
                <div class="col-lg-12">
                    <div id="weekly-schedule" class="table-schedule schedule-smc" style="overflow: hidden;"></div>
                </div>
            </div>
            <hr />

            <h5 class="modal-subtitle">Budget target</h5>
            <div class="form-group row align-items-center">
                <div class="col-lg-3">
                    <label>Diary</label>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="number" step="1" class="form-control w-100" placeholder="" name="budget_target_diary" id="budget_target_diary">
                        </div>
                        <div class="col-md-6 bud_dist">
                            <select class="selectpicker w-100" id="budget_target_diary_type"
                                name="budget_target_diary_type">
                                <option value="CLICKS">Clicks</option>
                                <option value="IMPRESSIONS">Impressions</option>
                                <option value="SPEND">Spend</option>
                                <option value="CONVERSIONS">Conversions</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row align-items-center">
                <div class="col-lg-3">
                    <label>Total</label>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="number" step="1" class="form-control w-100" placeholder="" name="budget_target_total" id="budget_target_total">
                        </div>
                        <div class="col-md-6 bud_dist">
                            <select class="selectpicker w-100" id="budget_target_total_type"
                                name="budget_target_total_type">
                                <option value="CLICKS">Clicks</option>
                                <option value="IMPRESSIONS">Impressions</option>
                                <option value="SPEND">Spend</option>
                                <option value="CONVERSIONS">Conversions</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    #properties-twitter {
        -webkit-appearance: none;
        -moz-appearance: none;
    }

    #properties-linkedin {
        -webkit-appearance: none;
        -moz-appearance: none;
    }

    #properties-adwords {
        -webkit-appearance: none;
        -moz-appearance: none;
    }

    #properties-amazon {
        -webkit-appearance: none;
        -moz-appearance: none;
    }

    .kt-wizard-v1 .kt-wizard-v1__wrapper .kt-form .kt-form22 {
        width: 100% !important;
        padding: 0rem !important;
    }

</style>
