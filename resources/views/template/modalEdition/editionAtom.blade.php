<style>
   
    .target_container {
        display: grid;
        grid-template-columns: repeat(4, 1fr);
        grid-template-rows: repeat(4, 1fr);
        grid-column-gap: 0px;
        grid-row-gap: 0px;
    }

    .icon_target {
        grid-area: 1 / 1 / 1 / 1 / 1;
    }

    .check_target {
        grid-area: 1 / 1 / 1 / 1 / 1;
    }

    .text_target {
        grid-area: 1 / 1 / 1 / 1 / 1;
    }

</style>

<div class="kt-wizard-v1__content" data-ktwizard-type="step-content" id="step_2" >
    
    <div class="kt-form__section kt-form__section--first">

        <div class="form-group row align-items-center">
            <label class="col-lg-3">Atom Name *</label>
            <input type="text" class="form-control col-lg-9" name="atom_name" id="atom_name">
            <input type="hidden" class="form-control col-lg-4" name="atom_id" id="atom_id">
        </div>
        <div class="form-group row align-items-center">
            <label class="col-lg-3">Atom Budget *</label>
            <input type="text" class="form-control col-lg-9" name="atom_budget" id="atom_budget">
            
        </div>

        <div class="kt-wizard-v1__form">


            <label class="kt-heading kt-heading--md">AUDIENCE</label>

            <div class="kt-heading kt-heading--md">

                Location
                <br>
                <label class="switch">
                    <input type="checkbox" id="target-location" data-id="location" name="target">
                    <span class="slider round"></span>
                </label>

            </div>
            <?php $platforms = Config::get('app.CONST_PLATFORMS'); ?>
            
                @foreach ($platforms as $this_rrss)
                <div class="dt-rrss" id="dt2-{{ strtolower($this_rrss['name']) }}"  style="display: none;">
                    <div class="col-form-label px-2">
                        <h5>{{ $this_rrss['name'] }}</h5>
                    </div>

                    <div id="divlocation" class="form-group">

                        <div class="kt-section px-3" id="dest_1">
                            <div class="form-group row align-items-center">
                                <div class="input-group-append">
                                    <select class="form-control selct m-select2"
                                        id="location-action-{{ strtolower($this_rrss['name']) }}"
                                        name="location-action[{{ strtolower($this_rrss['name']) }}]">
                                        <option value="IN" selected>Include</option>
                                        <option value="EX">Exclude</option>
                                    </select>
                                </div>
                                <input type="text" autocomplete="nope" name="location[{{ strtolower($this_rrss['name']) }}]"
                                    id="location_{{ strtolower($this_rrss['name']) }}" class="form-control ubic col-md-8 location_{{ strtolower($this_rrss['name']) }}"
                                    placeholder="Add a country group, marketing area, country, state/province, city, ZIP or an address...">
                                <!-- Este boton carga un mapa con JS -->

                                <div class="input-group-append">
                                    <button type="button" class="btn btn-success map" name="btn-map"><svg id="_27_Icon_pin" data-name="27) Icon/pin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M8,20a1,1,0,0,1-.651-.242C7.049,19.5,0,13.423,0,7.922a8,8,0,0,1,16,0c0,5.5-7.049,11.58-7.349,11.836A1,1,0,0,1,8,20ZM8,2A5.968,5.968,0,0,0,2,7.922c0,3.655,4.2,8.018,6,9.724,1.8-1.705,6-6.067,6-9.724A5.968,5.968,0,0,0,8,2Zm0,9a3.5,3.5,0,1,1,3.5-3.5A3.5,3.5,0,0,1,8,11ZM8,6A1.5,1.5,0,1,0,9.5,7.5,1.5,1.5,0,0,0,8,6Z" transform="translate(4 2)" fill="#222b45"/> </svg>
                                    </button>&nbsp;
                                </div>
                                <div id="location-box-{{ strtolower($this_rrss['name']) }}" class="location-box"></div>
                            </div>
                            <div class="form-group row slct">
                                <select class="form-control slct m-select2"
                                    id="location_option_{{ strtolower($this_rrss['name']) }}"
                                    name="location_option[{{ strtolower($this_rrss['name']) }}]">
                                    <option value="EO" selected>Everyone in this location</option>
                                    <option value="WL">People who live in this location</option>
                                    <option value="PR">People recently in this location </option>
                                    <option value="PT">People traveling in this location</option>
                                </select>
                            </div>
                            <div class="form-group row align-items-center">
                                <ul id="location-list-{{ strtolower($this_rrss['name']) }}" class="location-list">
                                </ul>
                            </div>
                        </div>
                        <div class="kt-section dropz" id="dest_2" style="display: none;">
                            <div class="row" id="">
                                <div class="col-md-6" id="">
                                    <span>Included locations*</span>
                                    <div class="kt-dropzone dropzone dropz" action="inc/api/dropzone/upload.php"
                                        id="m-dropzone-one">
                                        <div class="kt-dropzone__msg dz-message needsclick">
                                            <h3 class="kt-dropzone__msg-title">
                                                <i class="fa fa-cloud-upload-alt"></i>Upload included location
                                            </h3>
                                            <span class="kt-dropzone__msg-desc"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span class="form-text text-muted">
                                Upload a spreadsheet (.xlsx/csv, max 5MB)
                            </span>
                        </div>
                        <!-- fin campo dependiente radio 2 -->

                    </div>


                </div>
            @endforeach
            <!-- multiSelect conecta con API de redes sociales -->

            <div class="kt-heading kt-heading--md">
                <div class="form-group row align-items-center">
                    <label class="col-form-label col-md-2">

                        Languages
                        <br>
                        <label class="switch">
                            <input type="checkbox" id="target-language" data-id="language" name="target">
                            <span class="slider round"></span>
                        </label>

                    </label>



                    <div class="col-md-8">
                        <input type="text" autocomplete="nope" name="language" id="language"
                            class="form-control ubic col-md-8" placeholder="Add a language...">
                        <div id="language-box" class="language-box"></div>
                        <div class="form-group row align-items-center">
                            <ul id="language-list" class="language-list">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="error_location alert-danger px-4 py-2" style="display:none;">This field is required.</div>

            </div>



            <div class="kt-heading kt-heading--md">
                DEMOGRAPHICS
            </div>
            <div class=" form-group row demog col-md-12">
                <label class="col-md-2">
                    Gender
                    <br>
                    <label class="switch">
                        <input type="checkbox" id="target-gender" data-id="gender" name="target">
                        <span class="slider round"></span>
                    </label>
                </label>
                <div class="kt-section col-md-7">
                    <fieldset class="gender">
                        <ul>
                            <li class="radpe_btn_gen">
                                <input type="radio" name="gender" valor="ALL"
                                    id="genderd6bed0cf-96f7-11eb-8d81-ac1f6b17ff4a"
                                    value="d6bed0cf-96f7-11eb-8d81-ac1f6b17ff4a" class="tod" checked="checked"><label
                                    for="genderd6bed0cf-96f7-11eb-8d81-ac1f6b17ff4a">All<i
                                        class="flaticon2-checkmark"></i></label>
                                <input type="radio" name="gender" valor="M"
                                    id="genderd58ee168-96f7-11eb-8d81-ac1f6b17ff4a"
                                    value="d58ee168-96f7-11eb-8d81-ac1f6b17ff4a" class="tod"><label
                                    for="genderd58ee168-96f7-11eb-8d81-ac1f6b17ff4a">Male<i
                                        class="flaticon2-checkmark"></i></label>
                                <input type="radio" name="gender" valor="F"
                                    id="genderd58ee398-96f7-11eb-8d81-ac1f6b17ff4a"
                                    value="d58ee398-96f7-11eb-8d81-ac1f6b17ff4a" class="tod"><label
                                    for="genderd58ee398-96f7-11eb-8d81-ac1f6b17ff4a">Female<i
                                        class="flaticon2-checkmark"></i></label>
                            </li>
                        </ul>
                    </fieldset>
                </div>
            </div>
            <div class="form-group row align-items-center">
                <label class="col-md-2 staps-label">
                    Age

                    <br>
                    <label class="switch">
                        <input type="checkbox" id="target-age" data-id="age" name="target">
                        <span class="slider round"></span>
                    </label>

                </label>
                <div class="col-lg-8" id="kt_repeater_age">
                    <div class="form-group row align-items-center" id="kt_repeater_11">
                        <div data-repeater-list="age" id='age-list'>
                            <div data-repeater-item class="row mb-3 align-items-center">
                                <div class="col-md-6">
                                    <div class="kt-form__group--inline">
                                        <div class="kt-form__label">
                                        </div>
                                        <div class="kt-form__control">
                                            <div class="input-daterange input-group"> <input type="number" class="form-control agepckr" name="start" placeholder="From..." /> <div class="input-group-append"> <span class="input-group-text"><i class="la la-ellipsis-h"></i></span></div> <input type="number" class="form-control agepckr" name="end" placeholder="...To" /></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div data-repeater-delete="" class="btn-sm btn btn-danger btn-pill deledad">
                                        <span>
                                            <i class="la la-trash-o"></i>
                                            <span> </span>
                                        </span>

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div data-repeater-create="" class="btn btn-sm btn-brand btn-pill addedad">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Add </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8"></div>
            </div>
            <div class="kt-heading kt-heading--md">
                TECHNOLOGY
            </div>
            <div class=" form-group row demog col-md-12">
                <label class="col-md-2">
                    Devices
                    <br>
                    <label class="switch">
                        <input type="checkbox" id="target-devices" data-id="devices" name="target">
                        <span class="slider round"></span>
                    </label>
                </label>
                <div class="kt-section col-md-7">
                    <fieldset class="devices">
                        <ul>
                            <li class="radpe_btn_gen">
                                <input type="radio" name="devices" valor="ALL"
                                    id="devicesd58ee504-96f7-11eb-8d81-ac1f6b17ff4a"
                                    value="d58ee504-96f7-11eb-8d81-ac1f6b17ff4a" class="tod" checked="checked"><label
                                    for="devicesd58ee504-96f7-11eb-8d81-ac1f6b17ff4a">All<i
                                        class="flaticon2-checkmark"></i></label>
                                <input type="radio" name="devices" valor="Desktop"
                                    id="devicesd58ee679-96f7-11eb-8d81-ac1f6b17ff4a"
                                    value="d58ee679-96f7-11eb-8d81-ac1f6b17ff4a" class="tod"><label
                                    for="devicesd58ee679-96f7-11eb-8d81-ac1f6b17ff4a">Desktop<i
                                        class="flaticon2-checkmark"></i></label>
                                <input type="radio" name="devices" valor="Mobile"
                                    id="devicesd58ee7e1-96f7-11eb-8d81-ac1f6b17ff4a"
                                    value="d58ee7e1-96f7-11eb-8d81-ac1f6b17ff4a" class="tod"><label
                                    for="devicesd58ee7e1-96f7-11eb-8d81-ac1f6b17ff4a">Mobile<i
                                        class="flaticon2-checkmark"></i></label>
                            </li>
                        </ul>
                    </fieldset>
                </div>
            </div>
            <div class=" form-group row demog col-md-12">
                <label class="col-md-2">

                    Conectivity
                    <br>
                    <label class="switch">
                        <input type="checkbox" id="target-conectivity" data-id="conectivity" name="target">
                        <span class="slider round"></span>
                    </label>
                </label>
                <div class="kt-section col-md-7">
                    <fieldset class="conectivity">
                        <ul>
                            <li class="radpe_btn_gen">
                                <input type="radio" name="conectivity" valor="ALL"
                                    id="conectivityd58ee93e-96f7-11eb-8d81-ac1f6b17ff4a"
                                    value="d58ee93e-96f7-11eb-8d81-ac1f6b17ff4a" class="tod" checked="checked"><label
                                    for="conectivityd58ee93e-96f7-11eb-8d81-ac1f6b17ff4a">All<i
                                        class="flaticon2-checkmark"></i></label>
                                <input type="radio" name="conectivity" valor="WiFi"
                                    id="conectivityd58eea9a-96f7-11eb-8d81-ac1f6b17ff4a"
                                    value="d58eea9a-96f7-11eb-8d81-ac1f6b17ff4a" class="tod"><label
                                    for="conectivityd58eea9a-96f7-11eb-8d81-ac1f6b17ff4a">WiFi<i
                                        class="flaticon2-checkmark"></i></label>
                                <input type="radio" name="conectivity" valor="3G"
                                    id="conectivityd58eec12-96f7-11eb-8d81-ac1f6b17ff4a"
                                    value="d58eec12-96f7-11eb-8d81-ac1f6b17ff4a" class="tod"><label
                                    for="conectivityd58eec12-96f7-11eb-8d81-ac1f6b17ff4a">3G<i
                                        class="flaticon2-checkmark"></i></label>
                            </li>
                        </ul>
                    </fieldset>
                </div>
            </div>

            <div class="kt-heading kt-heading--md">
                DETAILED TARGETING
            </div>
            <div class="kt-heading kt-heading--md">


                Interest
                <br>
                <label class="switch">
                    <input type="checkbox" id="target-interest" data-id="interest" name="target">
                    <span class="slider round"></span>
                </label>
            </div>

			@foreach ($platforms as $this_rrss)

                <div class="dt-rrss" id="dt-{{ strtolower($this_rrss['name']) }}" style="display: none;">
                    <div class="px-4">
                        <div class="col-form-label px-2">
                            <h5>{{ $this_rrss['name'] }}</h5>
                        </div>
                        <div class="form-group row from-api px-3" style="margin-bottom: 0 !important;">
                            <div class="input-group-append ">
                                <select class="form-control intereses-action {{ strtolower($this_rrss['name']) }}"
                                    name="intereres-param[{{ strtolower($this_rrss['name']) }}]"
                                    style="width: 89px; padding: 7px;">
                                    <option value="IN" selected>Include</option>
                                    <option value="EX">Exclude</option>
                                </select>
                            </div>


                            <div class="kt-input-icon segm col-md-8">
                                <input type="text" class="form-control intereses {{ strtolower($this_rrss['name']) }}"
                                    placeholder="Add interests" name="intereses[{{ strtolower($this_rrss['name']) }}]"
                                    rrss="{{ strtolower($this_rrss['name']) }}">
                                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                    <svg id="_27_Icon_pin" data-name="27) Icon/pin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M8,20a1,1,0,0,1-.651-.242C7.049,19.5,0,13.423,0,7.922a8,8,0,0,1,16,0c0,5.5-7.049,11.58-7.349,11.836A1,1,0,0,1,8,20ZM8,2A5.968,5.968,0,0,0,2,7.922c0,3.655,4.2,8.018,6,9.724,1.8-1.705,6-6.067,6-9.724A5.968,5.968,0,0,0,8,2Zm0,9a3.5,3.5,0,1,1,3.5-3.5A3.5,3.5,0,0,1,8,11ZM8,6A1.5,1.5,0,1,0,9.5,7.5,1.5,1.5,0,0,0,8,6Z" transform="translate(4 2)" fill="#222b45"/> </svg>
                                </span>
                                <div class="intereses-box {{ strtolower($this_rrss['name']) }}"></div>
                                <div class="form-group row align-items-center">
                                    <ul class="intereses-list {{ strtolower($this_rrss['name']) }}"> </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

{{--             <div class="kt-heading kt-heading--md">
                <!--<img src="assets/media/icons/target_short.png" class="icon-diana"/> -->
                Custom Audience
                <br>
                <label class="switch">
                    <input type="checkbox" id="target-custom-audience" data-id="custom-audience" name="target">
                    <span class="slider round"></span>
                </label>
            </div>

			@foreach ($platforms as $this_rrss)
                <div class="dt-rrss" id="dt3-{{ strtolower($this_rrss['name']) }}" style="display: none;">
                    <div class="px-4">
                        <div class="col-form-label px-2">
                            <h5>{{ $this_rrss['name'] }}</h5>
                        </div>
                        <div class="form-group row from-api px-3" style="margin-bottom: 0 !important;">
                            <div class="input-group-append">
                                <select class="form-control audiencia-action {{ strtolower($this_rrss['name']) }}"
                                    name="audiencia-param[{{ strtolower($this_rrss['name']) }}]"
                                    style="width: 89px; padding: 7px;">
                                    <option value="IN" selected>Include</option>
                                    <option value="EX">Exclude</option>
                                </select>
                            </div>
                            <div class="kt-input-icon segm col-md-8">
                                <input type="text" class="form-control audiencia {{ strtolower($this_rrss['name']) }}"
                                    placeholder="Please select a Custom Audience..."
                                    name="audiencia[{{ strtolower($this_rrss['name']) }}]"
                                    rrss="{{ strtolower($this_rrss['name']) }}">
                                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                    <svg id="_27_Icon_pin" data-name="27) Icon/pin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M8,20a1,1,0,0,1-.651-.242C7.049,19.5,0,13.423,0,7.922a8,8,0,0,1,16,0c0,5.5-7.049,11.58-7.349,11.836A1,1,0,0,1,8,20ZM8,2A5.968,5.968,0,0,0,2,7.922c0,3.655,4.2,8.018,6,9.724,1.8-1.705,6-6.067,6-9.724A5.968,5.968,0,0,0,8,2Zm0,9a3.5,3.5,0,1,1,3.5-3.5A3.5,3.5,0,0,1,8,11ZM8,6A1.5,1.5,0,1,0,9.5,7.5,1.5,1.5,0,0,0,8,6Z" transform="translate(4 2)" fill="#222b45"/> </svg>
                                </span>
                                <div class="audiencia-box {{ strtolower($this_rrss['name']) }}"></div>
                                <div class="form-group row align-items-center">
                                    <ul class="audiencia-list {{ strtolower($this_rrss['name']) }}"> </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach


 --}}

        </div>
    </div>
</div>
