@extends('template.general')
<!--begin::Page Vendors Styles(used by this page) -->

<link rel="stylesheet" href="https://rawgit.com/dbrekalo/attire/master/dist/css/build.min.css">
<script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>

<link href="/css/reporting.css" rel="stylesheet" type="text/css"/>
@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17">
                        <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"></path>
                    </svg>
				</span>
                <h3 class="kt-portlet__head-title">
                    <!--{ { $title } } -->
                    Cuál es la tendencia de tus KPIs más importantes
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">

                        </div>

                    <!--a href="/users/new" class="btn btn-brand btn-elevate btn-icon-sm">
							<i class="la la-plus"></i>
							{{ $newElement }}
                            </a-->
                        <button type="button"
                                class="btn btn-info"
                                style="float: right;"><i class="la la-download"></i> Descargar PDF
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Dashboard -->
            <div class="container-fluid">

                <div class="row fila">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form-inline" role="form" style="margin-bottom: 0px;">
                                    <div class="row">
                                        <div class="col-md-4 kt-form__control">
                                            <!--select name="s_sitio" id="s_sitio" class="form-control multipleSelectSitio stiloinput" multiple placeholder="Búsqueda por sitio">
                                            </select-->
                                            <select class="selectpicker" id="select_clients" data-entidad="clients"
                                                    multiple show-tick title="Clients" data-live-search="true">
                                                <option>Sitio1</option>
                                                <option>Sitio2</option>
                                                <option>Sitio3</option>
                                                <option>Sitio4</option>
                                                <option>Sitio5</option>
                                                <option>Sitio6</option>
                                            </select>

                                            <!--script>
                                                $('.multipleSelectSitio').fastselect();
                                            </script-->
                                        </div>
                                        <div class="col-md-4">
                                            <select class="selectpicker" id="select_platforms" data-entidad="platforms"
                                                    multiple show-tick title="Platforms" data-live-search="true">
                                                <option>Formato1</option>
                                                <option>Formato2</option>
                                                <option>Formato3</option>
                                                <option>Formato4</option>
                                                <option>Formato5</option>
                                                <option>Formato6</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="selectpicker" id="select_campaigns" data-entidad="campaigns"
                                                    multiple show-tick title="Campaigns" data-live-search="true">
                                                <option>Campaña1</option>
                                                <option>Campaña2</option>
                                                <option>Campaña3</option>
                                                <option>Campaña4</option>
                                                <option>Campaña5</option>
                                                <option>Campaña6</option>
                                            </select>
                                        </div>
                                    </div>

                                    <fieldset class="gender alineacion2" style="margin-top: 2px; margin-right:10px;">
                                        <ul style="margin-bottom: 0px;">
                                            <li class="radpe_btn_gen" style="height:40px !important;">
                                                <input type="radio" name="frecuencia" id="frecuencia1" value="1"
                                                       class="tod r_frecuencia" checked="checked">
                                                <label for="frecuencia1" style="margin:0px;">
                                                    Daily<i class="flaticon2-checkmark"></i>
                                                </label>
                                                <input type="radio" name="frecuencia" id="frecuencia7" value="7"
                                                       class="hom r_frecuencia">
                                                <label for="frecuencia7" style="margin:0px;">
                                                    Weeks<i class="flaticon2-checkmark"></i>
                                                </label>
                                                <input type="radio" name="frecuencia" id="frecuencia3" value="30"
                                                       class="muj r_frecuencia">
                                                <label for="frecuencia3" style="margin:0px;">
                                                    Monthly<i class="flaticon2-checkmark"></i>
                                                </label>
                                            </li>
                                        </ul>
                                    </fieldset>
                                    <div class="btn-group alineacion2" data-toggle="buttons">
                                        <!--
                                          <label class="btn btn-info" style="float: right;">
                                            Periodo
                                          </label>
            -->
                                        <div class="kt-form__control">
                                            <select class="selectpicker" id="periodo" aria-invalid="false"
                                                    Placeholder="Periodo" name="periodo" id="periodo">
                                                <option value="null">Period</option>
                                                <option value="30" selected>Last 30 days</option>
                                                <option value="7">Last 7 days</option>
                                                <option value="2">Last 3 days</option>
                                                <option value="0">Today</option>
                                                <option value="1">Yesterday</option>
                                                <option value="29">This Month</option>
                                                <option value="31">Last Month</option>
                                                <option value="364">This Year</option>
                                                <option value="365">Last Year</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="btn-group alineacion2 align-items-center" data-toggle="buttons">
                                        <button type="button" id="d_cargar_db" class="btn btn-primary btn-icon" style="float: right; margin-bottom: 0px; margin-left: 15px; margin-right: 5px;">
                                                <svg id="_27_Icon_settings-2" data-name="27) Icon/settings-2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                                    <path d="M10.943,20H9.051a1.678,1.678,0,0,1-1.677-1.676V17.233a.334.334,0,0,0-.227-.293.546.546,0,0,0-.2-.045.277.277,0,0,0-.2.077l-.77.77a1.683,1.683,0,0,1-2.377,0L2.257,16.4a1.681,1.681,0,0,1,0-2.38l.768-.765a.334.334,0,0,0,.048-.37.36.36,0,0,0-.309-.264H1.683A1.684,1.684,0,0,1,0,10.943V9.051A1.679,1.679,0,0,1,1.677,7.375h1.09a.335.335,0,0,0,.293-.228.353.353,0,0,0-.032-.4l-.77-.77a1.684,1.684,0,0,1,0-2.377L3.6,2.257a1.681,1.681,0,0,1,2.379,0l.765.769a.285.285,0,0,0,.207.082.427.427,0,0,0,.163-.034.363.363,0,0,0,.263-.309V1.683A1.685,1.685,0,0,1,9.058,0H10.95a1.678,1.678,0,0,1,1.676,1.676V2.767a.334.334,0,0,0,.227.293.538.538,0,0,0,.2.046.272.272,0,0,0,.2-.079l.77-.77a1.683,1.683,0,0,1,2.377,0L17.745,3.6a1.681,1.681,0,0,1,0,2.377l-.769.766a.332.332,0,0,0-.047.37.358.358,0,0,0,.308.264h1.084A1.684,1.684,0,0,1,20,9.057v1.893a1.678,1.678,0,0,1-1.676,1.676h-1.09a.335.335,0,0,0-.293.228l-.009.022,0,.011a.338.338,0,0,0,.046.37l.771.769a1.687,1.687,0,0,1,0,2.378L16.4,17.743a1.682,1.682,0,0,1-2.38,0l-.765-.769a.284.284,0,0,0-.207-.081.424.424,0,0,0-.164.034.36.36,0,0,0-.263.308v1.084A1.685,1.685,0,0,1,10.943,20ZM6.952,14.892a2.41,2.41,0,0,1,.934.189,2.347,2.347,0,0,1,1.489,2.152V18h1.251v-.767a2.327,2.327,0,0,1,1.463-2.142,2.5,2.5,0,0,1,.966-.2,2.268,2.268,0,0,1,1.622.667l.539.541.887-.887-.545-.545a2.323,2.323,0,0,1-.477-2.557l.012-.03a2.322,2.322,0,0,1,2.14-1.459H18V9.375h-.766a2.325,2.325,0,0,1-2.142-1.464,2.349,2.349,0,0,1,.47-2.588l.541-.538L15.216,3.9l-.545.545a2.276,2.276,0,0,1-1.622.666,2.414,2.414,0,0,1-.934-.189,2.348,2.348,0,0,1-1.489-2.153V2H9.375v.767A2.325,2.325,0,0,1,7.911,4.909a2.487,2.487,0,0,1-.965.2,2.266,2.266,0,0,1-1.621-.668L4.785,3.9,3.9,4.785l.545.544a2.329,2.329,0,0,1,.477,2.557A2.346,2.346,0,0,1,2.767,9.375H2v1.25h.767a2.322,2.322,0,0,1,2.142,1.463,2.349,2.349,0,0,1-.47,2.588l-.541.539.887.887.544-.545A2.276,2.276,0,0,1,6.952,14.892ZM10,13.5A3.5,3.5,0,1,1,13.5,10,3.5,3.5,0,0,1,10,13.5Zm0-5A1.5,1.5,0,1,0,11.5,10,1.5,1.5,0,0,0,10,8.5Z" transform="translate(2 2)" fill="#FFFFFF"/>
                                                  </svg>
                                                  </button>
                                        <button type="button" id="d_guardar" class="btn btn-primary btn-icon-left"  style="float: right; margin-bottom: 0px; margin-left: 5px; margin-right: 5px;">
                                                <svg id="_27_Icon_save" data-name="27) Icon/save" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                                    <path d="M15,18H3a3,3,0,0,1-3-3V3A3,3,0,0,1,3,0h7.172a2.978,2.978,0,0,1,2.121.879l4.828,4.828A2.982,2.982,0,0,1,18,7.829V15A3,3,0,0,1,15,18ZM3,2A1,1,0,0,0,2,3V15a1,1,0,0,0,1,1H5V13a1,1,0,0,1,1-1h6a1,1,0,0,1,1,1v3h2a1,1,0,0,0,1-1V7.829a1,1,0,0,0-.293-.708L10.879,2.293A1.011,1.011,0,0,0,10.172,2H7V6h3a1,1,0,0,1,0,2H6A1,1,0,0,1,5,7V2ZM7,14v2h4V14Z" transform="translate(3 3)" fill="#FFFFFF"/>
                                                  </svg>
                                                   Guardar
                                        </button>&ensp;
                                        <!--label class="btn btn-info" style="float: right;" id="guardar">
                                            Guardar
                                        </label-->
                                        <div class="kt-form__control">
                                            <select class="selectpicker" aria-invalid="false" name="s_dashboards"
                                                    id="s_dashboards">
                                                <option value="-1">New Report</option>
                                                @foreach ($dashboards as $dashboard)
                                                    <option value="{{$dashboard->id}}">{{$dashboard->name}}</option>
                                                @endforeach
                                            </select></div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row fila">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!--div class="btn-group btn-group-toggle alineacion" data-toggle="buttons"-->
                                        <fieldset class="gender alineacion" style="margin-top: 2px; margin-right:10px;">
                                            <ul style="margin-bottom: 0px; padding-inline-start:0px;">
                                                <li class="radpe_btn_gen">
                                                    <input type="radio" name="caoptions" id="impresions"
                                                           value="impresions" class="tod r_grafica" checked="checked">
                                                    <label for="impresions" class="text-left"
                                                           style="height: 100px; line-height:30px;">
                                                        <span class="parrafo1">Impresions</span><br>
                                                        <span class="parrafo2" id="p_impresions">136 MM</span><br>
                                                        <span class="parrafo1">122,3% vs. 30 días anteriores</span>
                                                    </label>
                                                    <input type="radio" name="caoptions" id="spend" value="spend"
                                                           class="hom r_grafica">
                                                    <label for="spend" class="text-left"
                                                           style="height: 100px; line-height:30px;">
                                                        <span class="parrafo1">Spend</span><br>
                                                        <span class="parrafo2" id="p_spend">0,4</span> EUR<br>
                                                        <span class="parrafo1">85,5%  vs. 30 días anteriores</span>
                                                    </label>
                                                    <input type="radio" name="caoptions" id="ecpm" value="ecpm"
                                                           class="hom r_grafica">
                                                    <label for="ecpm" class="text-left"
                                                           style="height: 100px; line-height:30px;">
                                                        <span class="parrafo1">eCPM</span><br>
                                                        <span class="parrafo2" id="p_ecpm">6,361 </span> EUR<br>
                                                        <span class="parrafo1">87,4%  vs. 30 días anteriores</span>
                                                    </label>
                                                    <input type="radio" name="caoptions" id="clicks" value="clicks"
                                                           class="hom r_grafica">
                                                    <label for="clicks" class="text-left"
                                                           style="height: 100px; line-height:30px;">
                                                        <span class="parrafo1">Clicks</span><br>
                                                        <span class="parrafo2" id="p_clicks">0</span><br>
                                                        <span class="parrafo1">100%  vs. 30 días anteriores</span>
                                                    </label>
                                                    <input type="radio" name="caoptions" id="ctr" value="ctr"
                                                           class="muj r_grafica">
                                                    <label for="ctr" class="text-left"
                                                           style="height: 100px; line-height:30px;">
                                                        <span class="parrafo1">CTR</span><br>
                                                        <span class="parrafo2" id="p_ctr">0%</span><br>
                                                        <span class="parrafo1">100%  vs. 30 días anteriores</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </fieldset>
                                        <!--/div-->
                                    </div>
                                </div>
                                <div class="row fila">
                                    <div class="col-md-12" id="chart">
                                        <!--img src="img/grafica.png" style="width: 100%;"-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row fila">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="parrafo3 pl-0 mb-2">Channels</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="parrafo3 pl-0 mb-2">Productos RTB</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-sm mb-2 mr-2 channel-buton-all">All</button>
                                        <button type="button" class="btn btn-primary btn-sm mb-2 mr-2 channel-buton">FB-icon</button>
                                        <button type="button" class="btn btn-primary btn-sm mb-2 mr-2 channel-buton">Instagram-icon</button>
                                        <button type="button" class="btn btn-primary btn-sm mb-2 mr-2 channel-buton">Twitter-icon</button>
                                        <button type="button" class="btn btn-primary btn-sm mb-2 mr-2 channel-buton">Linkedin-icon</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-sm mb-2 mr-2 channel-buton">Deal+</button>
                                        <button type="button" class="btn btn-primary btn-sm mb-2 mr-2 channel-buton">Acuerdo directo</button>
                                        <button type="button" class="btn btn-primary btn-sm mb-2 mr-2 channel-buton">Acuerdo garantizado
                                        </button>
                                        <button type="button" class="btn btn-primary btn-sm mb-2 mr-2 channel-buton">Open auction</button>
                                        <button type="button" class="btn btn-primary btn-sm mb-2 mr-2 channel-buton">Private auction</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal" id="kt_modal_nombre" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">New Report</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">Name:</label>
                                        <input type="text" class="form-control" id="dashboaard-name">
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-basic" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="guardaNombreDashboaard">Save</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end: Dashdoarb -->
            </div>
        </div>
    @endsection

    @section('customscript')
        <!--begin::Page Vendors(used by this page) -->
            <script src="/js/apexcharts.js" type="text/javascript"></script>
            <!--<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>-->

            <script src="/js/reporting.js" type="text/javascript"></script>
            <!--end::Page Vendors -->
            <!--begin::Page Scripts(used by this page) -->
            <!-- Con este script es que se hace la carga de la data-->

            <script type="text/javascript">
               
                var objetoDashboard = {};
                var totales = {};
                var filas = {};
                var grender = 0;

                var options = {
                    chart: {
                        type: 'line',
                        height: 390,
                        stroke: {
                            show: true,
                        curve: 'smooth',
                        lineCap: 'butt',
                        width: 2,
                        }
                    },
                    stroke: {
                        show: true,
                        curve: 'smooth',
                        lineCap: 'butt',
                        width: 2,
                    },
                    series: [{
                        name: '',
                        data: [] //o aqui la data de consulta ajax
                    }],
                    xaxis: {
                        categories: [],
                        stroke: {
                            show: true,
                            curve: 'smooth',
                            lineCap: 'butt',
                            width: 2,
                        },
                    },
                    colors: ['#FB9A99', '#FF7F00', '#FFC626']
                }

                var chart = new ApexCharts(document.querySelector("#chart"), options);

                let caoptions;

                $('input[name="caoptions"]').click(function () {
                    caoptions = $(this).val();
                    console.log(caoptions);
                });
                //aqui va el click del boton guardado
                $('#d_cargar_db').click(function () {
					updateobjetoglobal('periodo',jQuery('#periodo').val());
					updateobjetoglobal('frecuencia',jQuery('.r_frecuencia').val());
					updateobjetoglobal('grafica',jQuery('.r_grafica').val());
					updateobjetoglobal('clients',jQuery('#select_clients').val());
					updateobjetoglobal('platforms',jQuery('#select_platforms').val());
					updateobjetoglobal('campaigns',jQuery('#select_campaigns').val());

					$.ajax({
                        url: "/api/reporting/data",
                        type: "post",
                        dataType: "json",
                        data: {
                            'datos': objetoDashboard,
                            'caoptions': caoptions
                        }
                    })
                        .done(function (res) {
                            //console.log(res['data']);
                            mostrarDatos(res['data']);
                            console.log()
                        });
                });		//jose: no tiene sentido renderizar el chart sin haber cargado datos
                //			chart.render();
                ////jose: tampoco es buena practica activar un tab desde .click de js , hay que hacerlo con un class "active"
                //$("#impresiones").click();
                //console.log('hola');
                /*$(document).ready(function(){*/
                $.ajax({
                    url: "/api/reporting/entidades",
                    type: "get",
                    dataType: "json"
                })
                    .done(function (res) {
                        console.log('entidades', res);
                        Object.keys(res).forEach(function (va) {
                            cargarSelect(va, res[va]);
                        });
                    });

                /*});*/

                function cargarSelect(entidad, datos) {
                    $('#select_' + entidad).empty();
                    for (i = 0; i < datos.length; i++) {
                        $('#select_' + entidad).append("<option value='" + datos[i]['id'] + "' >" + datos[i]['nameValue'] + "</option>");
                    }
                    $('#select_' + entidad).selectpicker('refresh');
                };

                function updateobjetoglobal(entidad, dato) {
                    objetoDashboard[entidad] = dato;
                    //lanzar funcion update chart o update otros datos, segun logicas que se configuren aqui
                    //console.log('OG', objetoDashboard);
                    return true;
                }


                function mostrarDatos(datos) {
                    totales = datos['totals'];
                    filas = datos[0]['data'];

                    //coloca los valores totales en la cajas correspondientes
                    $.each(totales, function (key, value) {
                        $('#p_' + key).text(value);
                    });

                    cargarGrafica($('.r_grafica').val(), filas);  //ojo aqui van las preferencias
                }

                //se cargar los datos con el nombre de la caja como par�metro
                function cargarGrafica(parametro, filas) {
                    var datos = [];
                    var categoria = [];
                    console.log(filas)

                    $.each(filas, function (key, value) {
                        datos.push(value['y']);
                        categoria.push(value['x']);
                    });

                    if (grender == 0) {
                        chart.render( options);
                        grender = 1;
                    }

                    chart.updateSeries([{
                        data: datos
                    }]);
                    chart.updateOptions({
                        xaxis: {
                            categories: categoria
                        }
                    });
                }

                $(document).ready(function () {
                    updateobjetoglobal('periodo', jQuery('#periodo').val());
                    updateobjetoglobal('frecuencia', jQuery('.r_frecuencia').val());
                    updateobjetoglobal('grafica', jQuery('.r_grafica').val());
                    updateobjetoglobal('clients', jQuery('#select_clients').val());
                    updateobjetoglobal('platforms', jQuery('#select_platforms').val());
                    updateobjetoglobal('campaigns', jQuery('#select_campaigns').val());


                    $('#d_cargar_db').click();

                    /* Boton guardar (editar opciones) dashboard (configuracion) si es new Report abre un modal para solicitar el nombre */
                    $('#d_guardar').click(function () {
                        loading.show();
                        updateobjetoglobal('periodo', jQuery('#periodo').val());
                        updateobjetoglobal('frecuencia', jQuery('.r_frecuencia').val());
                        updateobjetoglobal('grafica', jQuery('.r_grafica').val());
                        updateobjetoglobal('clients', jQuery('#select_clients').val());
                        updateobjetoglobal('platforms', jQuery('#select_platforms').val());
                        updateobjetoglobal('campaigns', jQuery('#select_campaigns').val());


                        var db_id = parseInt($('#s_dashboards').val());
                        if (db_id != -1) {
                            //console.log('!=-1');
                            var ruta = "/api/dashboard/save/" + db_id;
                            $.ajax({
                                url: ruta,
                                type: "post",
                                dataType: "json",
                                data: {'datos': objetoDashboard}
                            })
                                .done(function (res) {
                                    console.log(res['datos']);
                                    //mostrar mensaje de datos guardados
                                    //mostrarDatos(res['datos']);
                                });
                        } else {
                            $('#kt_modal_nombre').modal('show');
                        }
                    });

                    /* Rutina para guardar un dashboard nuevos */
                    $('#guardaNombreDashboaard').click(function () {
                        loading.show();
                        var nombre = $('#dashboaard-name').val();
                        updateobjetoglobal('periodo', jQuery('#periodo').val());
                        updateobjetoglobal('frecuencia', jQuery('.r_frecuencia').val());
                        updateobjetoglobal('grafica', jQuery('.r_grafica').val());
                        updateobjetoglobal('clients', jQuery('#select_clients').val());
                        updateobjetoglobal('platforms', jQuery('#select_platforms').val());
                        updateobjetoglobal('campaigns', jQuery('#select_campaigns').val());
                        //console.log(nombre)

                        if (nombre != '') {
                            $.ajax({
                                url: "/api/dashboard/save",
                                type: "post",
                                dataType: "json",
                                data: {'datos': objetoDashboard, 'nombre': nombre}
                            })
                                .done(function (res) {
                                    console.log(res['datos']);
                                    //mostrarDatos(res['datos']);
                                    //mostrar mensaje de datos guardados
                                });
                            $('#kt_modal_nombre').modal('hide');
                        }
                    });


                    $('#s_dashboards').on('change', function () {
                        //loading.show();
                        var db_id = parseInt($(this).val());
                        $.ajax({
                            url: "/api/dashboard/" + db_id,
                            dataType: "json"
                        })
                            .done(function (res) {
                                let data = jQuery.parseJSON(res[0].configuration);
                                $('#periodo').selectpicker('val', data.periodo);
                                $('.r_frecuencia').selectpicker('val', data.frecuencia);
                                $('.r_grafica').selectpicker('val', data.grafica);
                                $('#select_clients').selectpicker('val', data.customers);
                                $('#select_platforms').selectpicker('val', data.platforms);
                                $('#select_campaigns').selectpicker('val', data.campaigns);

                                $('#d_cargar_db').click();
                                loading.hide();
                                //mostrar mensaje de datos guardados
                            });
                        //console.log(db_id);
                    })


                    $('#periodo').change(function () {
                        updateobjetoglobal('periodo', jQuery(this).val());
                    });
                    // $('#s_dashboards').on('change', function() {
                    // 	var db_id= parseInt($(this).val());
                    // 	console.log(db_id);
                    // })


                    $('.r_frecuencia').on('click', function () {
                        updateobjetoglobal('frecuencia', jQuery(this).val());
                    });
                    $('.r_grafica').on('click', function () {
                        updateobjetoglobal('grafica', jQuery(this).val());
                        cargarGrafica(jQuery(this).val());
                    });

                    $('.selectpicker').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
                        updateobjetoglobal(jQuery(this).data('entidad'), jQuery(this).val()[0]);
                    });

                });

            </script>

@endsection