@extends('template.general')
@section('title', 'Campaña')


@section('content')	
<!-- Inicio de contenido -->

<style type="text/css">

	.kt-form.nw_usr{
		width: 50%;
		margin-left: auto;
		margin-right: auto;
	}

	/* Boton crear usuario*/

	button.btn-success.creat{
		background-color: #46B346 !important;
		border-color: #46B346 !important;
	}

</style>
<!--begin::Form-->
<form class="kt-form nw_usr">
	<div class="kt-portlet__body">
		<div class="form-group form-group-last">
			<div class="alert alert-secondary" role="alert">
				<div class="alert-icon"><i class="flaticon-mark kt-font-brand"></i></div>
				<div class="alert-text">
					CREAR ROL
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label for="example-text-input" class="col-2 col-form-label">Rol</label>
			<div class="col-10">
				<input class="form-control" type="text" value="" id="example-text-input" placeholder="Rol del Usuario">
			</div>
		</div>		
		<div class="form-group row">
			<label for="example-email-input" class="col-2 col-form-label">Permisos</label>
			<div class="col-10">
				<div class="kt-checkbox-list">
					<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
						<input type="checkbox"> Crear Campaña
						<span></span>
					</label>
					<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
						<input type="checkbox"> Visualizar Campaña
						<span></span>
					</label>
				</div>
			</div>
		</div>		
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-2">
				</div>
				<div class="col-10">
					<button type="reset" class="btn btn-success creat">Crear</button>
					<button type="reset" class="btn btn-secondary">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
</form>



<!-- Fin de contenido -->
@endsection

@section('customscript')
@endsection

