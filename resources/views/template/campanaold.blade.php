@extends('template.general')

@section('title', 'Campaña')	
		<!--begin::link to css Styles(used by this page) -->
		
@section('content')	
<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

							<!--begin::Portlet-->
							<div class="kt-portlet">
								<div class="kt-portlet__head">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Información de Campaña
										</h3>
									</div>
								</div>

								<!--begin::Form-->
								<form class="kt-form kt-form--fit kt-form--label-right">
									<div class="kt-portlet__body">
									</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-3 col-sm-12">Nombre de la Campaña</label>
											<div class=" col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control"  placeholder="Campaña">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-3 col-sm-12">Red Social</label>
											<div class=" col-lg-4 col-md-9 col-sm-12">
												<select class="form-control m-select2" id="kt_select2_3" name="param" multiple="multiple">
														<option value="Facebook">Facebook</option>
														<option value="Twitter">Twitter</option>
													<!--<option value="Instagram">Instagram</	 option>
														<option value="Linkedin">Linkedin</option>
														<option value="Pinterest">Pinterest</option>
														<option value="Snapchat">Snapchat</option>
														<option value="Youtube">Youtube</option>
														<option value="Vimeo">Vimeo</option>-->
												</select>
											</div>
										</div>	
										<div class="form-group row">
											<label class="col-3 col-form-label">Genero</label>
											<div class="col-9">
												<div class="kt-checkbox-list">
													<label class="kt-checkbox">
														<input type="checkbox"> Hombres
														<span></span>
													</label>
													<label class="kt-checkbox">
														<input type="checkbox"> Mujeres
														<span></span>
													</label>
														<span></span>
													</label>
												</div>
											</div>
										</div>	






										<br>
										<br>
										<div class="form-group row">
											<label class="col-form-label col-lg-3 col-sm-12">Rango de Edad</label>
											<div class="col-lg-6 col-md-12 col-sm-12">
												<div class="row align-items-center">
													<div class="col-2" style="display:none">
														<input type="text" class="form-control" id="kt_nouislider_3_input" placeholder="Quantity">
													</div>
													<div class="col-2" style="display:none">
														<input type="text" class="form-control" id="kt_nouislider_3.1_input" placeholder="Quantity">
													</div>
													<div class="col-12">
														<div id="kt_nouislider_3" class="kt-nouislider"></div>
													</div>
												</div>
												<span class="form-text text-muted">Selecciona el rango de edades.</span>
											</div>
										</div>
										<br>
										<div class="form-group row">
											<label class="col-form-label col-lg-3 col-sm-12">Rango de Fechas</label>
											<div class="col-lg-4 col-md-9 col-sm-12">
												<div class='input-group' id='kt_daterangepicker_2'>
													<input type='text' class="form-control" readonly placeholder="Selecciona el rango de fechas" />
													<div class="input-group-append">
														<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
													</div>
												</div>
											</div>
										</div>





							
										<div class="form-group row">
											<label class="col-form-label col-lg-3 col-sm-12">Localización</label>
											<div class=" col-lg-4 col-md-9 col-sm-12">
												<select class="form-control m-select2" id="kt_select2_9" name="param" multiple>
													<option></option>
													
												</select>
											</div>
										</div>
										<br>
										<!--aqui las preferencias-->
										<div id="kt_repeater_1">
											<div class="form-group  row" id="kt_repeater_1">
												<label class="col-form-label col-lg-3 col-sm-12">Preferencias:</label>
												<div data-repeater-list="" class="col-lg-9">
													<div data-repeater-item class="form-group row align-items-center">
														<div class="col-md-3">
															<div class="kt-form__group--inline">
																<div class="kt-form__control">
																	<input type="text" class="form-control" placeholder="Preferencia">
																</div>
															</div>
															<div class="d-md-none kt-margin-b-10"></div>
														</div>
														<div class="col-md-4">
															<div data-repeater-delete="" class="btn-sm btn btn-danger btn-pill">
																<span>
																	<i class="la la-trash-o"></i>
																	<span>Borrar</span>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-form-label col-lg-3 col-sm-12"></label>
												<div class="col-lg-4">
													<div data-repeater-create="" class="btn btn btn-sm btn-brand btn-pill">
														<span>
															<i class="la la-plus"></i>
															<span>Añadir</span>
														</span>
													</div>
												</div>
											</div>
										</div>
										<br>
										<div class="form-group row">
											<label class="col-form-label col-lg-3 col-sm-12">Subir archivo o imagen</label>
											<div class="col-lg-4 col-md-9 col-sm-12">
												<div class="kt-dropzone dropzone m-dropzone--success" action="inc/api/dropzone/upload.php" id="m-dropzone-three">
													<div class="kt-dropzone__msg dz-message needsclick">
														<h3 class="kt-dropzone__msg-title">Arrastra los archivos aqui o haz click.</h3>
														<span class="kt-dropzone__msg-desc">Solo archivos de imágen. </span>
													</div>
												</div>
											</div>
										</div>
										<!--aqui el cuadro de texto-->
										<div class="form-group row">
											<label class="col-form-label col-lg-3 col-sm-12">Texto</label>
											<div class="col-lg-4 col-md-9 col-sm-12">
												<textarea class="form-control" placeholder="" rows="6"></textarea>
											</div>
										</div>
										<div class="kt-portlet__foot">
											<div class="kt-form__actions">
												<div class="row">
													<div class="col-lg-12 ml-lg-auto">
														<button type="reset" class="btn btn-brand">Enviar</button>
														<button type="reset" class="btn btn-secondary">Cancelar</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->

							<!--begin::Modal
							<div class="modal fade" id="kt_select2_modal" role="dialog" aria-labelledby="" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="">Select2 Examples</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" class="la la-remove"></span>
											</button>
										</div>
										<form class="kt-form kt-form--fit kt-form--label-right">
											<div class="modal-body">
												<div class="form-group row kt-margin-t-20">
													<label class="col-form-label col-lg-3 col-sm-12">Basic Example</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<select class="form-control m-select2" id="kt_select2_1_modal" name="param">
															<option value="AK">Alaska</option>
															<option value="HI">Hawaii</option>
															<option value="CA">California</option>
															<option value="NV">Nevada</option>
															<option value="OR">Oregon</option>
															<option value="WA">Washington</option>
															<option value="AZ">Arizona</option>
															<option value="CO">Colorado</option>
															<option value="ID">Idaho</option>
															<option value="MT">Montana</option>
															<option value="NE">Nebraska</option>
															<option value="NM">New Mexico</option>
															<option value="ND">North Dakota</option>
															<option value="UT">Utah</option>
															<option value="WY">Wyoming</option>
															<option value="AL">Alabama</option>
															<option value="AR">Arkansas</option>
															<option value="IL">Illinois</option>
															<option value="IA">Iowa</option>
															<option value="KS">Kansas</option>
															<option value="KY">Kentucky</option>
															<option value="LA">Louisiana</option>
															<option value="MN">Minnesota</option>
															<option value="MS">Mississippi</option>
															<option value="MO">Missouri</option>
															<option value="OK">Oklahoma</option>
															<option value="SD">South Dakota</option>
															<option value="TX">Texas</option>
															<option value="TN">Tennessee</option>
															<option value="WI">Wisconsin</option>
															<option value="CT">Connecticut</option>
															<option value="DE">Delaware</option>
															<option value="FL">Florida</option>
															<option value="GA">Georgia</option>
															<option value="IN">Indiana</option>
															<option value="ME">Maine</option>
															<option value="MD">Maryland</option>
															<option value="MA">Massachusetts</option>
															<option value="MI">Michigan</option>
															<option value="NH">New Hampshire</option>
															<option value="NJ">New Jersey</option>
															<option value="NY">New York</option>
															<option value="NC">North Carolina</option>
															<option value="OH">Ohio</option>
															<option value="PA">Pennsylvania</option>
															<option value="RI">Rhode Island</option>
															<option value="SC">South Carolina</option>
															<option value="VT">Vermont</option>
															<option value="VA">Virginia</option>
															<option value="WV">West Virginia</option>
														</select>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Nested Example</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<select class="form-control m-select2" id="kt_select2_2_modal" name="param">
															<optgroup label="Alaskan/Hawaiian Time Zone">
																<option value="AK">Alaska</option>
																<option value="HI">Hawaii</option>
															</optgroup>
															<optgroup label="Pacific Time Zone">
																<option value="CA">California</option>
																<option value="NV" selected>Nevada</option>
																<option value="OR">Oregon</option>
																<option value="WA">Washington</option>
															</optgroup>
															<optgroup label="Mountain Time Zone">
																<option value="AZ">Arizona</option>
																<option value="CO">Colorado</option>
																<option value="ID">Idaho</option>
																<option value="MT">Montana</option>
																<option value="NE">Nebraska</option>
																<option value="NM">New Mexico</option>
																<option value="ND">North Dakota</option>
																<option value="UT">Utah</option>
																<option value="WY">Wyoming</option>
															</optgroup>
															<optgroup label="Central Time Zone">
																<option value="AL">Alabama</option>
																<option value="AR">Arkansas</option>
																<option value="IL">Illinois</option>
																<option value="IA">Iowa</option>
																<option value="KS">Kansas</option>
																<option value="KY">Kentucky</option>
																<option value="LA">Louisiana</option>
																<option value="MN">Minnesota</option>
																<option value="MS">Mississippi</option>
																<option value="MO">Missouri</option>
																<option value="OK">Oklahoma</option>
																<option value="SD">South Dakota</option>
																<option value="TX">Texas</option>
																<option value="TN">Tennessee</option>
																<option value="WI">Wisconsin</option>
															</optgroup>
															<optgroup label="Eastern Time Zone">
																<option value="CT">Connecticut</option>
																<option value="DE">Delaware</option>
																<option value="FL">Florida</option>
																<option value="GA">Georgia</option>
																<option value="IN">Indiana</option>
																<option value="ME">Maine</option>
																<option value="MD">Maryland</option>
																<option value="MA">Massachusetts</option>
																<option value="MI">Michigan</option>
																<option value="NH">New Hampshire</option>
																<option value="NJ">New Jersey</option>
																<option value="NY">New York</option>
																<option value="NC">North Carolina</option>
																<option value="OH">Ohio</option>
																<option value="PA">Pennsylvania</option>
																<option value="RI">Rhode Island</option>
																<option value="SC">South Carolina</option>
																<option value="VT">Vermont</option>
																<option value="VA">Virginia</option>
																<option value="WV">West Virginia</option>
															</optgroup>
														</select>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Multi Select</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<select class="form-control m-select2" id="kt_select2_3_modal" name="param" multiple="multiple">
															<optgroup label="Alaskan/Hawaiian Time Zone">
																<option value="AK" selected>Alaska</option>
																<option value="HI">Hawaii</option>
															</optgroup>
															<optgroup label="Pacific Time Zone">
																<option value="CA">California</option>
																<option value="NV" selected>Nevada</option>
																<option value="OR">Oregon</option>
																<option value="WA">Washington</option>
															</optgroup>
															<optgroup label="Mountain Time Zone">
																<option value="AZ">Arizona</option>
																<option value="CO">Colorado</option>
																<option value="ID">Idaho</option>
																<option value="MT" selected>Montana</option>
																<option value="NE">Nebraska</option>
																<option value="NM">New Mexico</option>
																<option value="ND">North Dakota</option>
																<option value="UT">Utah</option>
																<option value="WY">Wyoming</option>
															</optgroup>
															<optgroup label="Central Time Zone">
																<option value="AL">Alabama</option>
																<option value="AR">Arkansas</option>
																<option value="IL">Illinois</option>
																<option value="IA">Iowa</option>
																<option value="KS">Kansas</option>
																<option value="KY">Kentucky</option>
																<option value="LA">Louisiana</option>
																<option value="MN">Minnesota</option>
																<option value="MS">Mississippi</option>
																<option value="MO">Missouri</option>
																<option value="OK">Oklahoma</option>
																<option value="SD">South Dakota</option>
																<option value="TX">Texas</option>
																<option value="TN">Tennessee</option>
																<option value="WI">Wisconsin</option>
															</optgroup>
															<optgroup label="Eastern Time Zone">
																<option value="CT">Connecticut</option>
																<option value="DE">Delaware</option>
																<option value="FL">Florida</option>
																<option value="GA">Georgia</option>
																<option value="IN">Indiana</option>
																<option value="ME">Maine</option>
																<option value="MD">Maryland</option>
																<option value="MA">Massachusetts</option>
																<option value="MI">Michigan</option>
																<option value="NH">New Hampshire</option>
																<option value="NJ">New Jersey</option>
																<option value="NY">New York</option>
																<option value="NC">North Carolina</option>
																<option value="OH">Ohio</option>
																<option value="PA">Pennsylvania</option>
																<option value="RI">Rhode Island</option>
																<option value="SC">South Carolina</option>
																<option value="VT">Vermont</option>
																<option value="VA">Virginia</option>
																<option value="WV">West Virginia</option>
															</optgroup>
														</select>
													</div>
												</div>
												<div class="form-group row kt-margin-b-20">
													<label class="col-form-label col-lg-3 col-sm-12">Placeholder</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<select class="form-control m-select2" id="kt_select2_4_modal" name="param">
															<option></option>
															<optgroup label="Alaskan/Hawaiian Time Zone">
																<option value="AK">Alaska</option>
																<option value="HI">Hawaii</option>
															</optgroup>
															<optgroup label="Pacific Time Zone">
																<option value="CA">California</option>
																<option value="NV">Nevada</option>
																<option value="OR">Oregon</option>
																<option value="WA">Washington</option>
															</optgroup>
															<optgroup label="Mountain Time Zone">
																<option value="AZ">Arizona</option>
																<option value="CO">Colorado</option>
																<option value="ID">Idaho</option>
																<option value="MT">Montana</option>
																<option value="NE">Nebraska</option>
																<option value="NM">New Mexico</option>
																<option value="ND">North Dakota</option>
																<option value="UT">Utah</option>
																<option value="WY">Wyoming</option>
															</optgroup>
															<optgroup label="Central Time Zone">
																<option value="AL">Alabama</option>
																<option value="AR">Arkansas</option>
																<option value="IL">Illinois</option>
																<option value="IA">Iowa</option>
																<option value="KS">Kansas</option>
																<option value="KY">Kentucky</option>
																<option value="LA">Louisiana</option>
																<option value="MN">Minnesota</option>
																<option value="MS">Mississippi</option>
																<option value="MO">Missouri</option>
																<option value="OK">Oklahoma</option>
																<option value="SD">South Dakota</option>
																<option value="TX">Texas</option>
																<option value="TN">Tennessee</option>
																<option value="WI">Wisconsin</option>
															</optgroup>
															<optgroup label="Eastern Time Zone">
																<option value="CT">Connecticut</option>
																<option value="DE">Delaware</option>
																<option value="FL">Florida</option>
																<option value="GA">Georgia</option>
																<option value="IN">Indiana</option>
																<option value="ME">Maine</option>
																<option value="MD">Maryland</option>
																<option value="MA">Massachusetts</option>
																<option value="MI">Michigan</option>
																<option value="NH">New Hampshire</option>
																<option value="NJ">New Jersey</option>
																<option value="NY">New York</option>
																<option value="NC">North Carolina</option>
																<option value="OH">Ohio</option>
																<option value="PA">Pennsylvania</option>
																<option value="RI">Rhode Island</option>
																<option value="SC">South Carolina</option>
																<option value="VT">Vermont</option>
																<option value="VA">Virginia</option>
																<option value="WV">West Virginia</option>
															</optgroup>
														</select>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-brand" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-secondary">Submit</button>
											</div>
										</form>
									</div>
								</div>
							</div>

							<end::Modal-->

							<!--begin::Portlet-->

							<!--end::Portlet-->
						</div>

						<!-- end:: Content -->






@endsection

@section('customscript')
		<!--begin::Page Scripts(used by this page) -->
		<script src="../assets/app/custom/general/crud/forms/widgets/select2.js" type="text/javascript"></script>
		<script src="../assets/app/custom/general/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
		<script src="../assets/app/custom/general/crud/forms/widgets/dropzone.js" type="text/javascript"></script>
		<script src="../assets/app/custom/general/crud/forms/widgets/form-repeater.js" type="text/javascript"></script>
		<script type="text/javascript">
			
		jQuery(document).ready(function() {


        // init slider
        var slider = document.getElementById('kt_nouislider_3');

        noUiSlider.create(slider, {
            start: [30, 45],
            step: 1,
            connect: true,
            tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 })],
            range: {
                'min': [0],
                'max': 100
            },
            format: wNumb({
                decimals: 0 
            })
        });
       

        // init slider input
        var sliderInput0 = document.getElementById('kt_nouislider_3_input');
        var sliderInput1 = document.getElementById('kt_nouislider_3.1_input');
        var sliderInputs = [sliderInput1, sliderInput0];        

        slider.noUiSlider.on('update', function( values, handle ) {
            sliderInputs[handle].value = values[handle];
        });
});
		</script>
		<!--end::Page Scripts -->
@endsection