<link href="/css/clients.css" rel="stylesheet" type="text/css" />
<link href="/css/chozen.css" rel="stylesheet" type="text/css" />
<link href="/css/reporting.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .kt-form.nw_usr {
        margin-left: auto;
        margin-right: auto;
    }

    button.btn-success.creat {
        background-color: #46B346 !important;
        border-color: #46B346 !important;
    }

</style>


<!-- Modal Customer -->
<div class="modal fade" id="modalCustomer" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="kiki-close"></i>
                </button>
            </div>
            <div class="modal-body">

                <!--begin::Form-->
                <form class="kt-form nw_usr" id="formEdit_Customer" method="POST" action="/customer">
                    {{ csrf_field() }}
                    <input type="hidden" name="client_id" id="client_id" />

                    <div class="kt-portlet__body">
                        <div class="form-group row align-items-center">
                            <label for="example-url-input" class="col-2 col-form-label">Name</label>
                            <div class="col-10">
                                <input class="form-control @error('name') is-invalid @enderror" name="name"
                                    type="text"
                                    value=""
                                    id="name" placeholder="Customer name" required>
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="example-url-input" class="col-2 col-form-label">Default</label>
                            <div class="col-10">
                                <label class="switch">
                                    <input type="checkbox" name="edit_default_customer" id="edit_default_customer">
                                    <span class="slider round check"></span>
                                </label>
                            </div>
                        </div>		
                        

                    <?php $platforms = Config::get('app.CONST_PLATFORMS'); ?>
                <!--startforeach-->
                    @foreach ($platforms as $platform)
                        <div class="form-group row align-items-center rad_btn" style="padding: 10px 0 !important;">
                            <div class="col-2">
                                <label class="switch p-0">
                                    <input type="checkbox" rrss="{{ strtolower($platform['name']) }}" name="rrss[{{ strtolower($platform['name']) }}][rrss]" id="rrss_{{ strtolower($platform['name']) }}" value="{{ strtolower($platform['name']) }}" tab="1" class="redSocial" >
                                    <span rrss="{{ strtolower($platform['name']) }}"></span>
                                </label>
                            </div>
                            <div class="col-10 d-flex align-items-center">
                                <i class="fab fa-{{ strtolower($platform['name']) }} mr-2 ed-icon"></i> {{ strtoupper($platform['name']) }}
                            </div>
                            <div class="col-12" style="margin-top: 15px; display: none;" id="settings_{{ strtolower($platform['name']) }}">
                                <div class="row">
                                    <label for="ads_accounts_facebook" class="col-3 col-form-label" >Ads Account</label>
                                    <div class="col-9">	
                                        <select id="ads_{{ strtolower($platform['name']) }}" name="rrss[{{ strtoupper($platform['name']) }}][ads][]" class="form-control @error('name') is-invalid @enderror chosen-select" name="ads_accounts_{{ strtolower($platform['name']) }}" data-placeholder="Choose a ads accounts..."  multiple="">
                                            <option value=""></option>				
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="properties_{{ strtolower($platform['name']) }}" class="col-3 col-form-label" >Properties</label>
                                    <div class="col-9">	
                                        <select id="properties_{{ strtolower($platform['name']) }}" name="rrss[{{ strtoupper($platform['name']) }}][properties][]" class="form-control @error('name') is-invalid @enderror chosen-select" name="properties_facebook" data-placeholder="Choose a properties..."  multiple="">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="fee" class="col-3 col-form-label">Fee</label>
                                    <div class="col-9">				
                                        <input class="form-control @error('name') is-invalid @enderror" name="rrss[{{ strtoupper($platform['name']) }}][fee]"  type="number" step="0.01" value="{{ old('name') ? old('name') : (isset($client) ? $client->name : '') }}" id="fee_{{ strtolower($platform['name']) }}" placeholder="Insert fee">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                <!--endforeach-->
                    </div>
                </form>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-outline-basic" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveBtnCustomer" onClick="javascript:saveCustomer();">Save</button>
            </div>
        </div>
    </div>
</div>
