@extends('template.general')
		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="../assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />

@section('content')						
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17">
                        <path d="M14,16V1a1,1,0,1,1,2,0V16a1,1,0,1,1-2,0ZM7,16V5A1,1,0,1,1,9,5V16a1,1,0,1,1-2,0ZM0,16V9A1,1,0,0,1,2,9v7a1,1,0,1,1-2,0Z" fill="#222b45"></path>
                    </svg>
				</span>
				<h3 class="kt-portlet__head-title">
				{{ $title }}
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						<div class="dropdown dropdown-inline">
							<!--
							<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="la la-download"></i> Export
							</button>											
							<div class="dropdown-menu dropdown-menu-right">
								<ul class="kt-nav">
									<li class="kt-nav__section kt-nav__section--first">
										<span class="kt-nav__section-text">Choose an option</span>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-print"></i>
											<span class="kt-nav__link-text">Print</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-copy"></i>
											<span class="kt-nav__link-text">Copy</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-file-excel-o"></i>
											<span class="kt-nav__link-text">Excel</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-file-text-o"></i>
											<span class="kt-nav__link-text">CSV</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-file-pdf-o"></i>
											<span class="kt-nav__link-text">PDF</span>
										</a>
									</li>
								</ul>
							</div>
							-->
						</div>
						@if (Auth::user()->damepermisos('crear_usuarios') == 1)
							<a href="/users/new" class="btn btn-info btn-icon-left-right">
								<i class="la la-plus"></i>
								{{ $newElement }} 
							</a>
						@else
							<a href="{{route('mysubscription')}}" class="btn btn-info btn-icon-left-right">
								<i class="la la-plus"></i>
								{{ $newElement }} &nbsp;
								<i class="fa fa-ban"></i>
							</a>
						@endif
						
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">

			<!--begin: Datatable -->
			<table class="table table-striped- table-checkable" id="kt_table_1"  data-page-length='10' >
				<thead>
					<tr>												
						@foreach ($columns as $column)
							<th>{{ $column }}</th>
						@endforeach
					</tr>
				</thead>
			</table>

			<!--end: Datatable -->
		</div>
	</div>						
@endsection

@section('customscript')
<!--begin::Page Vendors(used by this page) -->
		<script src="../assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		<!-- Con este script es que se hace la carga de la data-->
		<script src="../assets/app/custom/general/crud/datatables/extensions/scroller.js" type="text/javascript"></script>

		<script type="text/javascript">

			$('#kt_table_1').DataTable({
				 //dom: 'Bfrtip',
                 //select: false,
                processing: true,
				serverSide: true,
				aLengthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
				ajax: "{{ $api }}",
				columns: 
					[
					@foreach ($columns as $column)
						{
							data: '{{ $column }}',
							orderable: false,
						    @if ( $column == 'id' )
					            visible : false,
					        @else
					        	visible : true,
					        @endif 
						    @if ( $column != 'Actions' )
					            responsivePriority: -1,
					        @endif 
						},
					@endforeach
				],
				columnDefs: [
					{
						targets: -1,
						title: 'Actions',
						orderable: false,
						render: function(data, type, full, meta){
						return `
                        <ul class="list-group list-group-horizontal list-inline" >
	                        <!--
	                        <li class="list-inline-item" >
		                        <span class="dropdown">
		                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
		                              <i class="la la-ellipsis-h"></i>
		                            </a>
		                            <div class="dropdown-menu dropdown-menu-right">
		                                <a class="dropdown-item" href="/{{ $base }}/`+full.id+`/edit" title="Editar {{ $base }}" ><i class="la la-edit"></i></a>
		                                <a class="dropdown-item" href="/{{ $base }}/`+full.id+`/delete" title="Eliminar {{ $base }}" ><i class="la la-trash"></i></a>
		                            </div>
		                        </span>
	                        </li>
	                        -->
	                        <li class="list-inline-item" >
		                        <a class="dropdown-item" href="/{{ $base }}/`+full.id+`/edit" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Editar {{ $base }}" ><i class="la la-edit"></i></a>
		                    </li>
		                    <li class="list-inline-item" >
		                        <a class="dropdown-item" href="/{{ $base }}/`+full.id+`/delete" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Eliminar {{ $base }}" ><i class="la la-trash"></i></a>
	                        </li>	
                        </ul>
                        `;
					}
				}
				]
			});

		</script>

@endsection