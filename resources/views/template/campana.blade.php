@extends('template.general')

@section('title', 'Campaña')
    <!--begin::link to css Styles(Wizard-V1) -->
    <link href="../assets/app/custom/wizard/wizard-v1.default.css" rel="stylesheet" type="text/css" />
    <!--begin:: Styles(used by treeview) -->
    <link href="../assets/vendors/custom/jstree/jstree.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
    <link href="/css/interes.css" rel="stylesheet" type="text/css" />
    <link href="/css/campana.css" rel="stylesheet" type="text/css" />
    <link href="/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="/css/reporting.css" rel="stylesheet" type="text/css" />

    <style>
        .rowday_p {
            margin-bottom: 0px !important;
            /* margin-top: 8px!important; */
            margin-left: 22px;
        }

        .wrapper {
            width: 100%;
            font-family: 'Helvetica';
            font-size: 14px;
        }

        .icon-diana {
            width: 30px;
        }

        .StepProgress {
            position: relative;
            padding-left: 45px;
            list-style: none;
        }

        .StepProgress::before {
            display: inline-block;
            content: '';
            position: absolute;
            top: 0;
            left: 15px;
            width: 10px;
            height: 100%;
        }

        .StepProgress-item {
            position: relative;
            counter-increment: list;
        }

        .StepProgress-item:not(:last-child) {
            padding-bottom: 20px;
        }

        .StepProgress-item::before {
            display: inline-block;
            content: '';
            position: absolute;
            left: -29px;
            height: 100%;
            width: 10px;
        }

        .StepProgress-item::after {
            content: '';
            display: inline-block;
            position: absolute;
            top: 0;
            left: -37px;
            width: 12px;
            height: 12px;
            border: 2px solid #CCC;
            border-radius: 50%;
            background-color: #FFF;
        }

        .StepProgress-item.is-done::before {
            border-left: 2px solid #646c9a;
        }

        .StepProgress-item.is-done::after {
            font-size: 10px;
            color: #FFF;
            text-align: center;
            border: 2px solid #646c9a;
            background-color: #646c9a;
        }

        .StepProgress-item.current::before {
            border-left: 2px solid #646c9a;
        }

        .StepProgress-item.current {
            list-style: none;
        }

        .StepProgress-item.current::after {
            content: counter(list);
            padding-top: 1px;
            width: 30px;
            height: 30px;
            top: -4px;
            left: -44px;
            font-size: 18px;
            text-align: center;
            color: #646c9a;
            border: 2px solid #646c9a;
            background-color: white;
            font-weight: 600;
        }

        .StepProgress strong {
            display: block;
        }

        #resumen_step1 ul {
            list-style: none;
            margin-left: 15px;
        }


        .schedule-table {
            border-collapse: separate !important;
        }

        .schedule-table th {
            text-align: center;
        }

        .schedule-rows td {
            width: 50px;
            height: 20px;
            margin: 3px;
            padding: 3px;
            background-color: #cecece;
            cursor: pointer;
            font-size: 12px;
        }

        .schedule-rows td:first-child {
            background-color: transparent;
            text-align: right;
            position: relative;
        }

        .schedule-rows td[data-selected],
        .schedule-rows td[data-selecting] {
            background-color: #000;
        }

        .schedule-rows td[data-disabled] {
            opacity: 0.55;
        }

        #loading {
            background: #ffffff96;
        }

        #loading.modal .modal-content {
            background: none;
            border: none;
            color: white;
        }

        #loading.modal .modal-dialog {
            width: 50px;
        }

        .interesOptions span {
            margin-left: 50px;
        }

    </style>



@section('content')

    @php
    $Array_RSS = ['Facebook', 'Twitter', 'Linkedin', 'Instagram', 'Snapchat', 'Amazon', 'Adwords'];
    @endphp


    <!-- begin:: Content -->
    <div class="container" id="kt_content">
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid  kt-wizard-v1 kt-wizard-v1--white" id="kt_wizard_v1" data-ktwizard-state="step-first">
                    <div class="kt-grid__item">
                        <!--begin: Form Wizard Nav -->
                        <div class="kt-wizard-v1__nav pt-5 pb-5">
                            <div class="kt-wizard-v1__nav-items wizard-staps">
                                <a class="staps-item" href="#" data-ktwizard-type="step" data-ktwizard-state="current">
                                    <span>1</span>
                                    <h6>Create Campaign</h6>
                                    <svg id="_27_Icon_arrow-right" data-name="27) Icon/arrow-right" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M1,8H12.865L9.232,12.36a1,1,0,0,0,1.537,1.28l5-6a.936.936,0,0,0,.087-.154.947.947,0,0,0,.071-.124A.985.985,0,0,0,16,7s0,0,0,0,0,0,0,0a.985.985,0,0,0-.072-.358.947.947,0,0,0-.071-.124.936.936,0,0,0-.087-.154l-5-6A1,1,0,0,0,9.232,1.64L12.865,6H1A1,1,0,0,0,1,8" transform="translate(4 5)" fill="#222b45"/> </svg>
                                </a>
                                <a class="staps-item" href="#" data-ktwizard-type="step">
                                    <span>2</span>
                                    <h6>Audience</h6>
                                    <svg id="_27_Icon_arrow-right" data-name="27) Icon/arrow-right" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M1,8H12.865L9.232,12.36a1,1,0,0,0,1.537,1.28l5-6a.936.936,0,0,0,.087-.154.947.947,0,0,0,.071-.124A.985.985,0,0,0,16,7s0,0,0,0,0,0,0,0a.985.985,0,0,0-.072-.358.947.947,0,0,0-.071-.124.936.936,0,0,0-.087-.154l-5-6A1,1,0,0,0,9.232,1.64L12.865,6H1A1,1,0,0,0,1,8" transform="translate(4 5)" fill="#222b45"/> </svg>
                                </a>
                                <a class="staps-item" href="#" data-ktwizard-type="step">
                                    <span>3</span>
                                    <h6>Placement</h6>
                                    <svg id="_27_Icon_arrow-right" data-name="27) Icon/arrow-right" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M1,8H12.865L9.232,12.36a1,1,0,0,0,1.537,1.28l5-6a.936.936,0,0,0,.087-.154.947.947,0,0,0,.071-.124A.985.985,0,0,0,16,7s0,0,0,0,0,0,0,0a.985.985,0,0,0-.072-.358.947.947,0,0,0-.071-.124.936.936,0,0,0-.087-.154l-5-6A1,1,0,0,0,9.232,1.64L12.865,6H1A1,1,0,0,0,1,8" transform="translate(4 5)" fill="#222b45"/> </svg>
                                </a>
                                <a class="staps-item" href="#" data-ktwizard-type="step">
                                    <span>4</span>
                                    <h6>Ads Design</h6>
                                    <svg id="_27_Icon_arrow-right" data-name="27) Icon/arrow-right" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M1,8H12.865L9.232,12.36a1,1,0,0,0,1.537,1.28l5-6a.936.936,0,0,0,.087-.154.947.947,0,0,0,.071-.124A.985.985,0,0,0,16,7s0,0,0,0,0,0,0,0a.985.985,0,0,0-.072-.358.947.947,0,0,0-.071-.124.936.936,0,0,0-.087-.154l-5-6A1,1,0,0,0,9.232,1.64L12.865,6H1A1,1,0,0,0,1,8" transform="translate(4 5)" fill="#222b45"/> </svg>
                                </a>
                                <!--<a class="staps-item" href="#" data-ktwizard-type="step">
                                    <span>i</span>
                                    <h6>4a Ads Design 2</h6>
                                    <svg id="_27_Icon_arrow-right" data-name="27) Icon/arrow-right" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M1,8H12.865L9.232,12.36a1,1,0,0,0,1.537,1.28l5-6a.936.936,0,0,0,.087-.154.947.947,0,0,0,.071-.124A.985.985,0,0,0,16,7s0,0,0,0,0,0,0,0a.985.985,0,0,0-.072-.358.947.947,0,0,0-.071-.124.936.936,0,0,0-.087-.154l-5-6A1,1,0,0,0,9.232,1.64L12.865,6H1A1,1,0,0,0,1,8" transform="translate(4 5)" fill="#222b45"/> </svg>
                                </a-->
                                <a class="staps-item" href="#" data-ktwizard-type="step">
                                    <span>5</span>
                                    <h6>Budget &amp; Bidding</h6>
                                    <svg id="_27_Icon_arrow-right" data-name="27) Icon/arrow-right" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M1,8H12.865L9.232,12.36a1,1,0,0,0,1.537,1.28l5-6a.936.936,0,0,0,.087-.154.947.947,0,0,0,.071-.124A.985.985,0,0,0,16,7s0,0,0,0,0,0,0,0a.985.985,0,0,0-.072-.358.947.947,0,0,0-.071-.124.936.936,0,0,0-.087-.154l-5-6A1,1,0,0,0,9.232,1.64L12.865,6H1A1,1,0,0,0,1,8" transform="translate(4 5)" fill="#222b45"/> </svg>
                                </a>
                                <a class="staps-item" href="#" data-ktwizard-type="step">
                                    <span>6</span>
                                    <h6>Publish</h6>
                                </a>
                            </div>
                        </div>
                        <!--end: Form Wizard Nav -->
                    </div>
                    <div class="mt-4">
                        <!--begin: Form Wizard Form-->
                        <form class="kt-form" id="kt_form" autocomplete="off">
                            <!--begin: Form Wizard Step 1-->
                            @include('template.campana.stepwiz1')
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Wizard Step 2-->
                            @include('template.campana.stepwiz2')
                            <!--end: Form Wizard Step 2-->
                            <!--begin: Form Wizard Step 3-->
                            @include('template.campana.stepwiz3')
                            <!--end: Form Wizard Step 3-->
                            <!--begin: Form Wizard Step 4-->
                            @include('template.campana.stepwiz4')
                            <!--end: Form Wizard Step 4-->
                            <!--begin: Form Wizard Step 5-->
                            @include('template.campana.stepwiz5')
                            <!--end: Form Wizard Step 5-->
                            <!--begin: Form Wizard Step 6-->
                            @include('template.campana.stepwiz6')
                            <!--end: Form Wizard Step 6-->
                            <!--begin: Form Actions -->
                            <div class="kt-form__actions">
                                <div class="btn btn-basic kt-font-bold kt-font-transform-u"
                                    data-ktwizard-type="action-prev">
                                    PREVIOUS
                                </div>
                                @if (Auth::user()->consumo < Auth::user()->getRolePermission('importe_maximo_mes') and Auth::user()->campaigns->count() < Auth::user()->getRolePermission('num_campanas'))
                                    <div class="btn btn-outline-basic kt-font-bold kt-font-transform-u"
                                        data-ktwizard-type="action-submit"
                                        type_btn='draft'>
                                        SAVE DRAFT
                                    </div>
                                    <div class="btn btn-primary kt-font-bold kt-font-transform-u"
                                        data-ktwizard-type="action-submit" type_btn='readytopublish'>
                                        PUBLISH CAMPAIGN
                                    </div>
                                @else
                                    <div class="btn btn-primary btn-wide kt-font-bold kt-font-transform-u block"
                                        data-ktwizard-type="action-submit">
                                        <i class="fa fa-ban" style="color:red;"></i> PUBLISH
                                    </div>
                                @endif
                                <div id="action-next"
                                    class="btn btn-basic  kt-font-bold kt-font-transform-u"
                                    data-ktwizard-type="action-next">
                                    NEXT
                                </div>
                            </div>
                            <!--end: Form Actions -->
                        </form>
                        <!--end: Form Wizard Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="loading" class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-sm modal-dialog-centered">
            <div class="modal-content" style="width: 48px">
                <span class="fa fa-spinner fa-spin fa-3x" style="color: black;"></span>
            </div>
        </div>
    </div>



    <!-- Modal Atomo -->
    <div class="modal fade" id="modalSubscription" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">User Subscription</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="kiki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <h3>Would you like to activate this feature ?</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-basic" data-dismiss="modal">No, thanks!</button>
                    <button type="button" class="btn btn-primary" style='color:white;' id="btnSubscription">Yes!</button>
                </div>
            </div>
        </div>
    </div>


    <!-- end:: Content -->
@endsection

@section('customscript')
    <!-- Script del Wizard Tabs -->
    <script src="../assets/app/custom/wizard/wizard-v1.js" type="text/javascript"></script>
    <!-- Script de Repeat -->
    <script src="../assets/app/custom/general/crud/forms/widgets/form-repeater.js" type="text/javascript"></script>
    <!-- Script del select widget -->
    <script src="../assets/app/custom/general/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <!-- Scripts del arbol  -->
    <script src="../assets/vendors/custom/jstree/jstree.bundle.js" type="text/javascript"></script>
    <script src="../assets/app/custom/general/components/extended/treeview.js" type="text/javascript"></script>
    <!-- Scripts del Datapicker -->
    <script src="../assets/app/custom/general/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- sha1 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/hmac-sha1.min.js"></script>
    <!-- sha256 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/hmac-sha256.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/enc-base64.min.js"></script>
    <script src="../js/oauth-1.0a.js"></script>

    <script>
        @php
        if (Auth::user()->getRolePermission('cross_plataformas', 'true')) {
            echo 'var cross_plataformas = true;';
        } else {
            echo 'var cross_plataformas = false;';
        }
        @endphp

        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };

        /* tablasel1*/
        $(document).ready(function() {

            $('#tablasel1 tr.click').click(function() {
                $('#tablasel1 tr.click').removeClass("selecc");
                $(this).addClass("selecc");
                $(this).find('input[type="radio"]').prop("checked", true);
            });


        });


        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }



        $('#ModalClient').on('show.bs.modal', function(e) {
            $('.alertclientName').hide();
        })

        $(".saveClient").click(function() {
            var name_client = $('#clientName').val();
            if (name_client != "") {
                $.ajax({
                    type: "POST",
                    url: "/api/createClient/",
                    data: {
                        nameClient: name_client
                    },
                    success: function(data) {
                        $("#select_client option[value='']").remove();
                        $('#select_client option:selected').removeAttr('selected');
                        $("#select_client").append('<option value="' + data + '" selected>' +
                            name_client + '</option>');
                        $('#ModalClient').modal('toggle');
                    }
                });
            } else {
                $('.alertclientName').show();
            }
        });

        $('#ModalClientEdit').on('show.bs.modal', function(e) {
            let id = $('#select_client option:selected').val();
            let template = $(".client_edit_html").html();
            let template_name = $(".client_view_html").html();
            $("#editForm").html('');
            $.ajax({
                type: "get",
                url: "/api/getClient/" + id,
                data: {},
                success: function(data) {
                    let temp_template = '';
                    template_name = template_name.replace(/key_value/g, 'Original Name');
                    template_name = template_name.replace('name_value', data['name']);
                    $("#editForm").append(template_name);
                    Object.keys(data).forEach(function(key) {
                        temp_template = template;
                        if (key == 'id') {
                            temp_template = '<input type="hidden" name="id" value="' + data[
                                key] + '">';
                            $("#editForm").append(temp_template);
                            return;
                        }
                        temp_template = temp_template.replace(/key_value/g, key);
                        if (!data[key]) {
                            data[key] = '';
                        }
                        temp_template = temp_template.replace('name_value', data[key]);
                        $("#editForm").append(temp_template);
                    });
                }
            });
        })

        $(".updateClient").click(function() {
            let data = $('#editForm :input').serialize();
            $.ajax({
                type: "POST",
                url: "/api/updateClient/",
                data: data,
                success: function(data) {
                    $("#select_client option[value='']").remove();
                    let name_value = $('#editForm [name="name"]').val();
                    $('#select_client option:selected').text(name_value);
                    $('#ModalClientEdit').modal('toggle');
                }
            });
        });


        $('#ModalClientView').on('show.bs.modal', function(e) {
            let id = $('#select_client option:selected').val();
            let template = $(".client_view_html").html();
            $(".client_view").html('');
            $.ajax({
                type: "get",
                url: "/api/getClient/" + id,
                data: {},
                success: function(data) {
                    let temp_template = '';
                    Object.keys(data).forEach(function(key) {
                        temp_template = template;
                        if (key == 'id') return;
                        temp_template = temp_template.replace('key_value', key);
                        temp_template = temp_template.replace('name_value', data[key]);
                        $(".client_view").append(temp_template);
                    });
                }
            });
        })

        $("[id^=adsaccount-]").on('change', function() {
            let account_type = this.id;
            let id = $('#select_client').val();
            account_type = account_type.replace('adsaccount-', '').toUpperCase();
            let account_id = this.options[this.selectedIndex].value;
            let account_type_lower = account_type.toLowerCase();
            let socialmediaselected = $(this).attr('attr_rrss');
            let account_selected = $("#adsaccount-" + socialmediaselected).val();

            var currency = $("#adsaccount-" + socialmediaselected + " option:selected").attr('currency');
            $("#price-" + socialmediaselected).html(currency);
            $("#bid-" + socialmediaselected).html(currency);

            $("#properties-" + account_type_lower).empty();
            $("#pixels-" + account_type_lower).empty();
            // properties
            var filteredArray = container_ads_property['ads_accounts_properties_' + socialmediaselected].filter(
                function(itm) {
                    if (account_selected == itm.ad_account_public_id) {
                        return itm;
                    }
                });

            if (filteredArray.length > 0) {
                $.each(filteredArray, function(index, value) {
                    $("#properties-" + socialmediaselected).append('<option value="' + value
                        .property_publicid + '">' + value.name + '</option>');
                });
            } else {
                $("#properties-" + socialmediaselected).append('<option value="">No properties</option>');
            }

            // pixels
            var filteredArrayPixels = container_ads_property['ads_accounts_pixels_' + socialmediaselected].filter(
                function(itm) {
                    if (account_selected == itm.ad_account_public_id) {
                        return itm;
                    }
                });

            if (filteredArrayPixels.length > 0) {
                $.each(filteredArrayPixels, function(index, value) {
                    $("#pixels-" + socialmediaselected).append('<option value="' + value.id + '">' + value
                        .name + '</option>');
                });
            } else {
                $("#pixels-" + socialmediaselected).append('<option value="">No pixels</option>');
            }

        });

        /* });  close para $(document).on('click', '.addedad', function(){*/

        let acces_token = "{{ $data['access_token_FACEBOOK'] }}";
        let consumer_key_TWITTER = "{{ $data['consumer_key_TWITTER'] }}";
        let consumer_secret_TWITTER = "{{ $data['consumer_secret_TWITTER'] }}";
        let token_TWITTER = "{{ $data['token_TWITTER'] }}";
        let secret_TWITTER = "{{ $data['secret_TWITTER'] }}";
        let acces_token_LINKEDIN = "{{ $data['access_token_LINKEDIN'] }}";

    </script>


    <style>
        .errorLabel {
            color: red;
        }

        .successLabel {
            color: green;
        }

    </style>

    <script src="/js/customCampaign.js" type="text/javascript"></script>
    <script src="/js/bootstrap-tagsinput.min.js" type="text/javascript"></script>
    <style type="text/css">
        tr.click.selecc {
            background: #d6d6d6 !important;
            color: #000 !important;
        }

    </style>
@endsection
