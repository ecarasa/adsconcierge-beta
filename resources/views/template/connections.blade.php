@extends('template.general')
<!--begin::Page Vendors Styles(used by this page) -->
<link href="../assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>

<style>
    .dataTables_filter {
        display: none;
    }

    #kt_content {
        display: flex;
    }

    tr th input[type="text"] {
        width: 100%;
    }

    th,
    td {
        width: 150px;
        word-break: break-word;
    }

    .select2.select2-container.select2-container--default {
        max-width: 200px !important;
        min-width: 100px !important;
    }

    .select2-container--default.select2-container--focus .select2-selection--multiple,
    span.select2-selection.select2-selection--multiple,
    .select2-container--default .select2-selection--multiple {
        display: flex;
    }


    div.dataTables_wrapper div.dataTables_processing {
        position: absolute;
        top: 50%;
        left: 50%;
        width: 200px;
        margin-left: -100px;
        margin-top: -26px;
        text-align: center;

        height: 73px !important;
    }

</style>
@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <svg id="_27_Icon_share" data-name="27) Icon/share" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                          <path d="M16,18a3,3,0,0,1-3-3,1.746,1.746,0,0,1,.022-.238c0-.033.009-.065.013-.1L5.1,11.139a3,3,0,1,1,0-4.279l7.934-3.525c0-.033-.008-.065-.013-.1A1.746,1.746,0,0,1,13,3a3.01,3.01,0,1,1,.9,2.139L5.966,8.665c0,.032.008.064.013.1a1.321,1.321,0,0,1,0,.476c0,.032-.009.065-.013.1L13.9,12.861A3,3,0,1,1,16,18Zm0-4a1,1,0,1,0,1,1A1,1,0,0,0,16,14ZM3,8A1,1,0,1,0,4,9,1,1,0,0,0,3,8ZM16,2a1,1,0,1,0,1,1A1,1,0,0,0,16,2Z" transform="translate(2 3)" fill="#222b45"/>
                    </svg>
                </span>
                <h3 class="kt-portlet__head-title">
                    {{ $title }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions" style="display: flex;">
                        <div class="dropdown dropdown-inline">
                            <!--
                                                    <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="la la-download"></i> Export
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <ul class="kt-nav">
                                                            <li class="kt-nav__section kt-nav__section--first">
                                                                <span class="kt-nav__section-text">Choose an option</span>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="#" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-print"></i>
                                                                    <span class="kt-nav__link-text">Print</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="#" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-copy"></i>
                                                                    <span class="kt-nav__link-text">Copy</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="#" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                                                    <span class="kt-nav__link-text">Excel</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="#" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-file-text-o"></i>
                                                                    <span class="kt-nav__link-text">CSV</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="#" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                                                    <span class="kt-nav__link-text">PDF</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    -->
                        </div>
                        &nbsp;
                        <?php
                        /*
                        @if (Auth::user()->damepermisos('crear_usuarios') == 1)*/
                            ?>
                            <div class="card-toolbar">

                                <div class="dropdown dropdown-inline">
                                    <a href="#" class="btn btn-info btn-icon-left" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false"><i
                                            class="la la-plus"></i>{{ $newElement }}
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="">
                                        <!--begin::Navigation-->
                                        <div class="kt-notification">
                                            @if (Auth::user()->validarplataforma())
                                                <a href="{{ URL::to('/') }}/apis/jr_auth_fb.php"
                                                    class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="flaticon-facebook-letter-logo"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Facebook
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="{{ URL::to('/') }}/apis/jr_auth_fb-insights.php"
                                                    class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="fab fa-instagram"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Instagram insights
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="{{ URL::to('/') }}/apis/jr_auth_google.php"
                                                    class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        '<i class="fab fa-google"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Google
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="{{ URL::to('/') }}/apis/da_auth_in.php"
                                                    class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="flaticon-linkedin-logo"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Linkedin
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="{{ URL::to('/') }}/apis/cbsnap.php"
                                                    class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="fab fa-snapchat-ghost"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Snapchat
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="{{ URL::to('/') }}/api/callback/twitter/?new"
                                                    class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="flaticon-twitter-logo"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Twitter
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="{{ URL::to('/') }}/apis/da_ads.php"
                                                    class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        <img src="{{ asset('img/icon_ads.png') }}" style="height: 15px;">
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            ADS
                                                        </div>
                                                    </div>
                                                </a>

                                            @else
                                                <a href="{{ route('mysubscription') }}"
                                                    class="kt-notification__item block">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="flaticon-facebook-letter-logo"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Facebook <i class="fa fa-ban" style="color:red"></i>
                                                        </div>

                                                    </div>
                                                </a>
                                                <a href="{{ route('mysubscription') }}"
                                                    class="kt-notification__item block">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="fab fa-instagram"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Instagram insights <i class="fa fa-ban" style="color:red"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="{{ route('mysubscription') }}"
                                                    class="kt-notification__item block">
                                                    <div class="kt-notification__item-icon">
                                                        '<i class="fab fa-google"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Google <i class="fa fa-ban" style="color:red"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="{{ route('mysubscription') }}"
                                                    class="kt-notification__item block">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="flaticon-linkedin-logo"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Linkedin <i class="fa fa-ban" style="color:red"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="{{ route('mysubscription') }}"
                                                    class="kt-notification__item block">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="fab fa-snapchat-ghost"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Snapchat <i class="fa fa-ban" style="color:red"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="{{ route('mysubscription') }}"
                                                    class="kt-notification__item block">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="flaticon-twitter-logo"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Twitter <i class="fa fa-ban" style="color:red"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="{{ route('mysubscription') }}"
                                                    class="kt-notification__item block">
                                                    <div class="kt-notification__item-icon">
                                                        <img src="{{ asset('img/icon_ads.png') }}" style="height: 15px;">
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            ADS <i class="fa fa-ban" style="color:red"></i>
                                                        </div>
                                                    </div>
                                                </a>

                                            @endif
                                        </div>
                                        <!--end::Navigation-->
                                    </div>
                                </div>
                            </div>

                            <?php
                        /*@else

                            <a href="{{ route('mysubscription') }}" class="btn btn-brand btn-elevate btn-icon-sm block">
                                <i class="la la-plus"></i>
                                {{ $newElement }} &nbsp;
                                <i class="fa fa-ban"></i>
                            </a>
                        @endif*/
                        ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-checkable" id="kt_table_1"
                data-page-length='10'>
                <thead>
                    <tr>
                        @foreach ($columns as $column)
                            <th>{{ $column }}</th>
                        @endforeach
                    </tr>
                </thead>
            </table>

            <!--end: Datatable -->
        </div>
    </div>





@endsection

@section('customscript')

    <script src="../assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
        rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js">
    </script>
    <script src="../assets/app/custom/general/crud/datatables/extensions/scroller.js" type="text/javascript"></script>

    <script src="/js/app_constans.js" type="text/javascript"></script>


    <script type="text/javascript">
        var rrss = {
            'FACEBOOK': '<i class="fab fa-facebook-f"></i>',
            'LINKEDIN': '<i class="fab fa-linkedin-in"></i>',
            'TWITTER': '<i class="fab fa-twitter"></i>',
            'INSTAGRAM': '<i class="fab fa-instagram"></i>',
            'SNAPCHAT': '<i class="fab fa-snapchat-ghost"></i>',
            'GANALYTICS': '<i class="fab fa-google"></i>',
            'SUNNATIVO': '<i class="far fa-sun"></i>Nativo',
            'FBINSIGHTS': '<i class="fab fa-facebook-f"></i>Insights',
            'PINTEREST': '<i class="fab fa-pinterest-p"></i>',
            'ADWORDS': '<img src="{{ asset('img/icon_ads.png') }}" style="height: 15px;">'
        }

        $.fn.editable.defaults.mode = 'inline';
        $.fn.editableform.buttons =
            '<button type="submit" class="btn btn-primary btn-icon editable-submit"><i class="fas fa-check"></i></button><button type="button" class="btn btn-basic btn-icon editable-cancel"><i class="fas fa-ban"></i></button>';

        $(document).ready(function() {


            $('#kt_table_1 thead tr').clone(true).appendTo('#kt_table_1 thead');

            $('#kt_table_1 thead tr:eq(1) th').each(function(i) {

                var title = $(this).text();

                if (title != 'rrss') {

                    $(this).html('<input type="text" placeholder="Search ' + title +
                        '" class="form-control"/>');

                } else {

                    var strSelect = "";
                    strSelect = strSelect +
                        '<select id="rrss_select" data-show-content="true" class="form-control">';
                    strSelect = strSelect + '<option value="">RRSS select</option>';


                    $.each(RRSS_ICONS, function(i, item) {
                        // console.log('key', i);
                        strSelect = strSelect + '<option data-content="' + item + ' ' + capitalize(
                                i) +
                            '" value="' + i + '">' + i + '</option>';
                    });

                    strSelect = strSelect + '</select>';


                    $(this).html(strSelect);

                    $('#rrss_select').selectpicker();
                    $('.dropdown-toggle').addClass('form-control');

                }

                $('#rrss_select', this).on('change', function() {
                    table.column(0).search(this.value).draw();
                });



                $('input', this).on('keyup change', function() {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });

            });

            let initComplete = function(settings, json) {


                $('.sync').click(function() {
                    let button = $(this);

                    if (!button.hasClass("disabled")) {
                        button.addClass("disabled");
                        let id = button.attr('account');
                        button.text("Sync loading");

                        $.ajax({
                            type: "POST",
                            data: {
                                auth_public_id: id
                            },
                            url: "/api/connection/sync",
                            success: function(data) {
                                table.ajax.reload(initComplete);
                            },
                            error: function(jqXHR, error, errorThrown) {
                                if (jqXHR.status == 501) {
                                    toastr.error(
                                        'The method is not implemented for this platform.',
                                        'Not implemented')
                                } else if (jqXHR.status == 500) {
                                    toastr.error('The method did not refresh correctly.',
                                        'Error')
                                }

                            },
                            complete: function() {
                                button.removeClass("disabled");
                                button.text("Sync");
                            }
                        });

                    }

                });


                $('.refresh').click(function() {
                    let button = $(this);

                    if (!button.hasClass("disabled")) {
                        button.addClass("disabled");
                        let id = button.attr('account');
                        button.text("Refreshing");

                        $.ajax({
                            type: "POST",
                            data: {
                                ad_account: id
                            },
                            url: "/api/connection/updater",
                            success: function(data) {
                                table.ajax.reload(initComplete);
                            },
                            error: function(jqXHR, error, errorThrown) {
                                if (jqXHR.status == 501) {
                                    toastr.error(
                                        'The method is not implemented for this platform.',
                                        'Not implemented')
                                } else if (jqXHR.status == 500) {
                                    toastr.error('The method did not refresh correctly.',
                                        'Error')
                                }

                            },
                            complete: function() {
                                button.removeClass("disabled");
                                button.text("Refresh");
                            }
                        });

                    }

                });


            };


            $('.default_client').editable({
                selector: 'a',
                url: '/post',
                pk: 1
            });

            var table = $('#kt_table_1').DataTable({
                //dom: 'Bfrtip',
                //select: false,
                orderCellsTop: true,
                fixedHeader: true,
                processing: true,
                serverSide: true,
                autoWidth: false,
                //engthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
                ajax: "{{ $api }}",
                columns: [
                    @foreach ($columns as $column)
                        {
                        data: '{{ $column }}',
                        orderable: true,
                        @if ($column == 'id')
                            visible : false,
                        @else
                            visible : true,
                        @endif
                        @if ($column != 'Actions')
                            responsivePriority: -1,
                        @endif
                        },
                    @endforeach
                ],
                columnDefs: [{
                        targets: 0,
                        title: 'RRSS',
                        render: function(data, type, full, meta) {
                            return rrss[data];
                        }
                    },
                    {
                        targets: 1,
                        title: 'Platform user id'
                    },
                    {
                        targets: 2,
                        title: 'Platform email'
                    },
                    {
                        targets: 3,
                        title: 'Ads accounts',
                        render: function(data, type, full, meta) {
                            if (data == null) {
                                data = 0;
                            }
                            return "<a href='ads_accounts/" + full.id + "'>" + data + "</a>";
                        }
                    },
                    {
                        targets: 4,
                        title: 'Properties',
                        render: function(data, type, full, meta) {
                            if (data == null) {
                                data = 0;
                            }
                            return "<a href='properties/" + full.id + "'>" + data + "</a>";
                        }
                    },
                    {
                        targets: 5,
                        title: 'Default Client',
                        render: function(data, type, full, meta) {

                            console.log("data", data)
                            console.log("full", full)
                            return '<a href="#" data-type="select" data-pk="' + full.id +
                                '" data-value="' + full.public_id +
                                '" data-source="/api/getClients_select" data-title="Select Client" class="editable editable-click default_client select" data-original-title="" title="">' +
                                data.name + '</a>&ensp;'+
                                '<svg class="editselect" data-pk="' + full .id + '" style="margin-left: 5px;" xmlns="http://www.w3.org/2000/svg" width="15.999" height="15.978" viewBox="0 0 15.999 15.978"> <path d="M1,15.978a1,1,0,0,1-1-1.09l.379-4.17A1.975,1.975,0,0,1,.953,9.5l9-9A1.8,1.8,0,0,1,11.238,0a2.028,2.028,0,0,1,1.427.577L15.4,3.315a1.927,1.927,0,0,1,.069,2.715l-9,9a1.971,1.971,0,0,1-1.214.568l-4.17.38C1.064,15.977,1.034,15.978,1,15.978ZM7.984,5.3h0L2.367,10.918,2.1,13.874,5.08,13.6,10.68,8l-2.7-2.7Zm3.288-3.289h0L9.324,3.962l2.695,2.695,1.948-1.949L11.272,2.012Z" transform="translate(0 0)" fill="#222b45"></path> </svg>&ensp;'+
                                '<a href="javascript:UnSetClient(' + full.id + ');" pkk="' + full.public_id + '" title="Remove"> <svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#222b45"/> </svg> </a> ';
                        }
                    },

                    {
                        targets: 6,
                        title: 'Actions',
                        render: function(data, type, full, meta) {

                            console.log('actions column', full);

                            var button = '<button type="button" account="' + full.public_id +
                                '"  platform="' +
                                full.rrss +
                                '" class="btn btn-info btn-sm btn-icon-right btn-upper refresh">Refresh Token <svg id="_27_Icon_Sync" data-name="27) Icon/Sync" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"/> </svg></button>&ensp;';
                            button = button + ' <button type="button" account="' + full.public_id +
                                '"  platform="' + full.rrss +
                                '" class="btn btn-info btn-sm btn-icon-right btn-upper sync">Sync <svg id="_27_Icon_Sync" data-name="27) Icon/Sync" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M2.353,13.212l-.37,1.972a1,1,0,0,1-1.966-.369l.75-4a1.043,1.043,0,0,1,.068-.171,1.028,1.028,0,0,1,.077-.189A.806.806,0,0,1,1,10.36a.967.967,0,0,1,.181-.183c.011-.008.026-.01.037-.017a.952.952,0,0,1,.316-.137.053.053,0,0,1,.021,0h.019a1.013,1.013,0,0,1,.338-.009l4.25.706A1,1,0,0,1,6,12.705a.923.923,0,0,1-.164-.014l-1.644-.272A7.172,7.172,0,0,0,10.43,16a7.09,7.09,0,0,0,6.643-4.375,1,1,0,1,1,1.853.75A9.079,9.079,0,0,1,10.43,18,9.162,9.162,0,0,1,2.353,13.212ZM19.086,7.986l-4.25-.7a1,1,0,1,1,.327-1.972l1.644.272A7.17,7.17,0,0,0,10.57,2,7.09,7.09,0,0,0,3.927,6.375a1,1,0,1,1-1.853-.75A9.08,9.08,0,0,1,10.57,0a9.16,9.16,0,0,1,8.078,4.788l.369-1.973a1,1,0,0,1,1.967.37l-.749,4a.954.954,0,0,1-.072.189.9.9,0,0,1-.05.133,1.129,1.129,0,0,1-.139.167c-.03.03-.05.067-.083.094a.975.975,0,0,1-.2.116.846.846,0,0,1-.1.054A.992.992,0,0,1,19.252,8,.943.943,0,0,1,19.086,7.986Z" transform="translate(1.5 3)" fill="#FFFFFF"/> </svg></button>&ensp;';

                            if (full.status == 'Y') {
                                return '<div class="action-box"><svg id="_27_Icon_checkmark" data-name="27) Icon/checkmark" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M5.863,12a1,1,0,0,1-.729-.315L.271,6.506A1,1,0,0,1,1.728,5.137L5.853,9.528l8.408-9.2a1,1,0,1,1,1.477,1.348l-9.137,10A1,1,0,0,1,5.87,12Z" transform="translate(4 6)" fill="#222b45"/> </svg>&ensp;' + button+'</div>';
                            }
                            return '<div class="action-box"><svg id="_27_Icon_close" data-name="27) Icon/close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M7.414,6l4.293-4.293A1,1,0,1,0,10.293.293L6,4.586,1.707.293A1,1,0,0,0,.293,1.707L4.586,6,.293,10.293a1,1,0,1,0,1.414,1.414L6,7.414l4.293,4.293a1,1,0,0,0,1.414-1.414Z" transform="translate(6 6)" fill="#222b45"/> </svg> ' + button+'</div>';
                        }

                    },

                    /*{	targets: 8, title: 'User'},*/
                ],
                initComplete: initComplete
            });





            table.on('draw.dt', function() {

                $('.default_client').editable({
                    type: 'text',
                    url: '/api/set_default_client',

                    params: function(params) {

                        //console.log("paramsss", params)
                        return params;
                    },

                    success: function(response) {
                        if (response === false) {
                            return 'Could not save';
                        }
                    }
                });

                $('.clients').editable({
                    inputclass: 'input-large',
                    allowClear: true,
                    source: [
                        @foreach ($clients as $client)
                            {id: '{{ $client['id'] }}', text: '{{ $client['text'] }}'},
                        @endforeach
                    ],
                    type: 'select2',
                    select2: {
                        multiple: true,
                        initSelection: function(element, callback) {
                            console.log("entre");
                            /*return $.get('/getCountryById', { query: element.val() }, function (data) {
                                callback(data);
                            });*/
                        }
                    },
                    url: '/api/clients',
                    success: function(response) {
                        console.log(response);
                        if (response === false) {
                            return 'Could not save';
                        } else {
                            demo(response);
                        }
                    },
                    /*¡¡display: function(value) {
                       console.log(value);
                        //set new text for xeditable field from php response
                       //$(this).text(sourceData);
                    }*/
                });



                $(".clients").on("save", function(e, editable) {
                    if (arguments.length == 2) {
                        console.log(editable.input); // outputs undefined
                        $(".clients[data-pk='110']").html("fadfs");
                    }
                });



                $(document).on('click', '.editselect', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var data_pk = ".select[data-pk='" + $(this).attr("data-pk") + "']";
                    console.log(data_pk);
                    $(data_pk).editable('toggle');
                });

                $(document).on('click', '.editselect2', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var data_pk = ".select2[data-pk='" + $(this).attr("data-pk") + "']";
                    console.log(data_pk);
                    $(data_pk).editable('toggle');
                });

            });


        });


        function UnSetClient(pkk) {
            
            var fd = new FormData();
            fd.append("idPkAu", pkk)

            $.ajax({
                url: "api/usetClientConnection",
                type: "POST",
                data: fd,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                beforeSend: function() {

                }
            }).done(function(data) {
                console.log(data)
                $('#kt_table_1').DataTable().ajax.reload();



            });


        }

        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        async function demo(response) {
            await sleep(500);
            if (response.text != "") {
                $(".clients[data-pk='" + response.pk + "']").html(response.text);
                $(".clients[data-pk='" + response.pk + "']").attr("data-value", response.id);
                $(".clients[data-pk='" + response.pk + "']").removeClass("editable-empty");
            }
        }

        @if ($text_message != '')
            Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Connection created successfully',
            showConfirmButton: false,
            timer: 1500
            })
        @endif

    </script>

@endsection
