@extends('template.general')

<link href="../assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />

<style>
    .kt-portlet__head-toolbar
    {
        padding-left: 1em;
    }

    .fc-time-grid-event
    {
        width: 100%;
    }

    .kt-subheader__title a
    {
        color: #fff;
    }
	
	#errorList {
        background: rgb(245 60 121 / 25%);
        color: #000;
        border-radius: 10px;
		list-style: none;
    }

    #errorList li{
        padding: 0.5em;
    }
</style>

@section('subheader')
    @include('template.organic.partials._menu')
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-gear"></i>
				</span>
                <h3 class="kt-portlet__head-title">
                    Organic: Tags Dictionary
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="/organic/tag" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-toggle="modal" data-target="#form_create_tags">
                            <i class="la la-plus"></i>
                            Create Tag
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="tags-table"  data-page-length='25' >
                <thead>
                    <tr>
                        @foreach ($columnsTitles as $column)
                            <th>{{ $column }}</th>
                        @endforeach
                    </tr>
                </thead>
            </table>
        </div>
    </div>



	<!-- Modal-->
	<div class="modal fade" id="form_create_tags" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
		<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Organic: Create Tags Dictionary</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div id="errorBox">
						<ul id="errorList">
						</ul>
					</div>
					<!--begin::Form-->
            <form method="post" class="form" id="tag_details" action="/organic/tag/">
                @csrf
                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8">
                        <div class="my-5">
                            <h3 class="text-dark font-weight-bold mb-10">Dictionary Details:</h3>
                            <br />

                            <div class="form-group row">
                                <label class="col-3">Name</label>
                                <div class="col-9">
                                    <input id="tdetails_tag_name" name="tdetails_tag_name" class="form-control form-control-solid" type="text" value=""/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Platform</label>
                                <div class="col-9">
                                    <select id="tdetails_tag_platform" class="form-control" name="tdetails_tag_platform">
                                        @foreach($platforms as $key => $platform)
                                        	<option value="{{ $key }}">{{ $platform }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">URL</label>
                                <div class="col-9">
                                    <input id="tdetails_tag_url" name="tdetails_tag_url" class="form-control form-control-solid" type="text" value=""/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Auto Update</label>
                                <div class="col-9">
                                    <div class="checkbox-inline">
                                        <label class="checkbox">
                                            <input id="tdetails_tag_auto_update" name="tdetails_tag_auto_update" type="checkbox" value="Y"/> Yes
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="separator separator-dashed my-10"></div>
                    </div>
                    <div class="col-xl-2"></div>
                </div>
            </form>
            <!--end::Form-->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back</button>
					<button type="button" class="btn btn-primary font-weight-bold btn-save-tag">Save</button>
				</div>
			</div>
		</div>
	</div>




@endsection

@section('customscript')
    <script src="../assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

    <script>
		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
	
		$( "#tags-table" ).on( "click", '.form_setting', function() { 
			loading.show();
			var public_id = $( this ).attr('data-id');
			
			if(public_id == ""){
				$('#tdetails_tag_name').val('');
				$('#tdetails_tag_url').val('');
				$("#tdetails_tag_platform option:selected").prop("selected", false);
				$("#tdetails_tag_auto_update").prop("checked", false);	

				$("#setting_details").attr('action', '/organic/tag/');
				$("#errorList").empty();
				loading.hide();
			} else {
			
				$.getJSON( "/organic/getTag/"+public_id, function( data ) {
				  
					$("#tag_details").attr('action', '/organic/tag/'+data.tdetails_tag_public_id);

					$('#tdetails_tag_name').val(data.tdetails_tag_name);
					$('#tdetails_tag_url').val(data.tdetails_tag_url);
					$('#tdetails_tag_platform option[value="'+data.tdetails_tag_platform+'"]').prop("selected", true);
					
					if(data.tdetails_tag_auto_update == 'Y' ){
						$("#tdetails_tag_auto_update").prop("checked", true);	
					}
		
					$('#form_create_tags').modal('show');
					
					loading.hide();
				});
			}
			
		});	
		
		$('#form_create_tags').on('hidden.bs.modal', function (e) {
			$('#tdetails_tag_name').val('');
			$('#tdetails_tag_url').val('');
			$("#tdetails_tag_platform option:selected").prop("selected", false);
			$("#tdetails_tag_auto_update").prop("checked", false);	
			
			$("#setting_details").attr('action', '/organic/tag/');
			$("#errorList").empty();
		});
		
		$(document).ready(()=>
        {
            $(document).on('click', '.btn-save-tag', function(e)
            {
				loading.show();
                e.preventDefault();

                let form_data = $('#tag_details');
                let tag_url = $('#tag_details').attr("action");

                $.ajax({
                    url: tag_url,
                    data: new FormData(form_data[0]),
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    cache: false,
                    processData: false,
                    contentType: false,
                })
				.done(function(response){
					response = JSON.parse(response);

					if(response.hasOwnProperty('url'))
					{
						//window.location = response.url;
						$('#form_create_tags').modal('hide');
						table.ajax.reload();
					}

					if(response.hasOwnProperty('errors'))
					{
						$('#errorList').empty();

						$.each(response.errors, (i, e) => {
							let li = $('<li>');

							li.text(e);

							$('#errorList').append(li);
						});
					}
					
					loading.hide();
				});
            });
        });
		
        var table = $('#tags-table').DataTable({
            processing: true,
            serverSide: true,
			searching: false,
            aLengthMenu: [[25, 50], [25, 50]],
            pageLength: 25,
            ajax: "{{ $tableDataUrl }}",
            oLanguage: {
                sProcessing: "<div id='tags-table_processing' class='dataTables_processing'>Processing...</div>"
            },
            createdRow: function( row, data, dataIndex ) {
                $(row).addClass( 'tag' );
            },
            columns: [
                    @foreach ($columnsNames as $column)
                {
                    data: '{{ $column }}',
                    orderable: false,
                    visible : true,
                    @if($column === 'name')
                    fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                        $(nTd).html("<a href='#' class='form_setting' data-id='" + oData.public_id + "'>" + oData.{{ $column }} + "</a>");
                    }
                    @endif
                    @if($column === 'actions')
                    fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                        $(nTd).html("<a href='#'>Refresh</a>");
                    }
                    @endif
                },
                @endforeach
            ]
        });
    </script>
@endsection
