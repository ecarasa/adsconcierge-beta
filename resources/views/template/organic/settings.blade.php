@extends('template.general')

<link href="../assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" integrity="sha512-0nkKORjFgcyxv3HbE4rzFUlENUMNqic/EzDIeYCgsKa/nwqr2B91Vu/tNAu4Q0cBuG4Xe/D1f/freEci/7GDRA==" crossorigin="anonymous" />

<style>
    .kt-portlet__head-toolbar
    {
        padding-left: 1em;
    }

    .fc-time-grid-event
    {
        width: 100%;
    }

    .kt-subheader__title a
    {
        color: #fff;
    }

	#errorList {
        background: rgb(245 60 121 / 25%);
        color: #000;
        border-radius: 10px;
		list-style: none;
    }

    #errorList li{
        padding: 0.5em;
    }
</style>

@section('subheader')
    @include('template.organic.partials._menu')
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-gear"></i>
				</span>
                <h3 class="kt-portlet__head-title">
                    Organic: Settings
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="/organic/setting" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-toggle="modal" data-target="#form_create_settings">
                            <i class="la la-plus"></i>
                            Create Setting
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">
            <table class="table table-striped table-bordered table-hover table-checkable" id="settings-table"  data-page-length='25' >
                <thead>
                <tr>
                    @foreach ($columnsTitles as $column)
                        <th>{{ $column }}</th>
                    @endforeach
                </tr>
                </thead>
            </table>
        </div>
    </div>


	<!-- Modal-->
	<div class="modal fade" id="form_create_settings" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
		<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Organic: Create Setting</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div id="errorBox">
						<ul id="errorList">
						</ul>
					</div>
					<!--begin::Form-->
            <form method="post" class="form" id="setting_details" action="/organic/setting/">
                @csrf
                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8">
                        <div class="my-5">
                            <h3 class="text-dark font-weight-bold mb-10">Setting Details:</h3>
                            <br />

                            <div class="form-group row">
                                <label class="col-3">Name</label>
                                <div class="col-9">
                                    <input id="sdetails_setting_name" name="sdetails_setting_name" class="form-control form-control-solid" type="text" value=""/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Source Type</label>
                                <div class="col-9">
                                    <select id="sdetails_setting_source_type" class="form-control" name="sdetails_setting_source_type">
                                        @foreach($sourceTypes as $key => $sourceType)
                                       		<option value="{{ $key }}">{{ $sourceType }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

							<div class="form-group row Analytics" style="display:none">
                                <label class="col-3">Ad Account</label>
                                <div class="col-9">
                                    <select id="sdetails_setting_ad_account" class="form-control" name="sdetails_setting_ad_account">
                                        @foreach($ads_accounts as $value)
                                       		<option value="{{ $value->app_id }}">{{ $value->platform_email }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

							<div class="form-group row Analytics" style="display:none">
                                <label class="col-3">Property</label>
                                <div class="col-9">
                                    <select id="sdetails_setting_property" class="form-control" name="sdetails_setting_property">
                                        @foreach($properties_accounts_ganalytics as $value)
                                       		<option value="{{ $value->public_id }}">{{ $value->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row Analytics" style="display:none">
                                <label class="col-3">Users from</label>
                                <div class="col-9">
                                  <input id="sdetails_setting_user_from" name="sdetails_setting_user_from" class="form-control form-control-solid" type="number" value="0"/>
                                </div>
                            </div>

                            <div class="form-group row RRSS">
                                <label class="col-3">URL</label>
                                <div class="col-9">
                                    <input id="sdetails_setting_url" name="sdetails_setting_url" class="form-control form-control-solid" type="text" value=""/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Read Interval Minutes</label>
                                <div class="col-9">
                                    <input id="sdetails_setting_read_interval" name="sdetails_setting_read_interval" class="form-control form-control-solid" type="number" min="15" max="240"  onkeyup="javascript:enforceMinMax(this)"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Items to Read</label>
                                <div class="col-9">
                                    <input id="sdetails_setting_items_to_read" name="sdetails_setting_items_to_read" class="form-control form-control-solid" type="number" min="1" max="50" value=""  onkeyup="javascript:enforceMinMax(this)"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Item Choose Way</label>
                                <div class="col-9">
                                    <select id="sdetails_setting_item_choose_way" class="form-control" name="sdetails_setting_item_choose_way">
                                        @foreach($itemChooseWays as $key => $itemChooseWay)
                                        	<option value="{{ $key }}">{{ $itemChooseWay }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>



							<div class="form-group row">
                                <label class="col-3">Destination profiles</label>
                                <div class="col-9">
                                    <select id="sdetails_setting_destination_profiles" multiple class="form-control chosen_property" name="sdetails_setting_destination_profiles" data-placeholder="Choose a property...">
                                        @foreach($properties_accounts as $value)
                                       		<option value="{{ $value->public_id }}">{{ $value->name }} ({{ $value->platform }})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Post Interval Minutes</label>
                                <div class="col-9">
                                    <input id="sdetails_setting_post_interval" name="sdetails_setting_post_interval" class="form-control form-control-solid" type="text" value=""/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Tags</label>
                                <div class="col-9" id="sdetails_setting_tags">
									<div class="">
										<label class="checkbox">
											<input id="checkAll" type="checkbox"/> Check All
											<span></span>
										</label>
									</div>
									@foreach($tags as $key => $tag)
										<div class="checkbox-inline">
											<label class="checkbox">
												<input name="sdetails_setting_tags[]" type="checkbox" value="{{ $tag['id'] }}"/> {{ $tag['name'] }} - {{ $tag['platform'] }}
												<span></span>
											</label>
										</div>
									@endforeach
                                </div>
                            </div>
                        </div>
                        <div class="separator separator-dashed my-10"></div>
                    </div>
                    <div class="col-xl-2"></div>
                </div>
            </form>
            <!--end::Form-->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back</button>
					<button type="button" class="btn btn-primary font-weight-bold btn-save-setting">Save</button>
				</div>
			</div>
		</div>
	</div>



@endsection

@section('customscript')
    <script src="../assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
    <script src="/js/app_constans.js" type="text/javascript"></script>
    <script>

        function enforceMinMax(el){
            if(el.value != ""){
                if(parseInt(el.value) < parseInt(el.min)){
                el.value = el.min;
                }
                if(parseInt(el.value) > parseInt(el.max)){
                el.value = el.max;
                }
            }
        }

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

		$( "#settings-table" ).on( "click", '.form_setting', function() {
			loading.show();
			var public_id = $( this ).attr('data-id');
			console.log("public_id ->" + public_id);
			if(public_id == ""){
				$('#sdetails_setting_name').val('');
				$('#sdetails_setting_url').val('');
				$('#sdetails_setting_read_interval').val('');
				$('#sdetails_setting_items_to_read').val('');
				$('#sdetails_setting_post_interval').val('');
				$('#sdetails_setting_user_from').val('');
				$("#sdetails_setting_destination_profiles option:selected").prop("selected", false);
				$("#sdetails_setting_source_type option:selected").prop("selected", false);
				$("#sdetails_setting_item_choose_way option:selected").prop("selected", false);
				$("#sdetails_setting_tags input:checkbox").prop("checked", false);
				$("#setting_details").attr('action', '/organic/setting/');
				loading.hide();
			} else {

				$.getJSON( "/organic/getSetting/"+public_id, function( data ) {
				  	console.log(data);
					console.log(data.sdetails_setting_tags);
					$("#setting_details").attr('action', '/organic/setting/'+data.sdetails_post_id);


					$('#sdetails_setting_name').val(data.sdetails_setting_name);
					$('#sdetails_setting_url').val(data.sdetails_setting_url);
					$('#sdetails_setting_read_interval').val(data.sdetails_setting_read_interval);
					$('#sdetails_setting_items_to_read').val(data.sdetails_setting_items_to_read);
					$('#sdetails_setting_post_interval').val(data.sdetails_setting_post_interval);

  				$('#sdetails_setting_user_from').val(data.sdetails_setting_user_from);

					$.each(data.sdetails_setting_destination_profiles, function( index, value ) {
						console.log(value);
						$('#sdetails_setting_destination_profiles option[value="'+value+'"]').prop("selected", true);
					});
					$(".chosen_property").trigger("chosen:updated");

					$('#sdetails_setting_source_type option[value="'+data.sdetails_setting_source_type+'"]').prop("selected", true);

          if(data.sdetails_setting_source_type == 'Analytics'){
    				$(".Analytics").show();
    				$(".RRSS").hide();
    			}else{
    				$(".Analytics").hide();
    				$(".RRSS").show();
    			}

        	$('#sdetails_setting_item_choose_way option[value="'+data.sdetails_setting_item_choose_way+'"]').prop("selected", true);


					$("#sdetails_setting_tags input[type=checkbox]").prop("checked", false);

					var sdetails_setting_tags = jQuery.parseJSON( data.sdetails_setting_tags );

					$("#checkAll").prop("checked", true);
					$('.checkbox-inline input[type=checkbox]').each(function () {
						if(jQuery.inArray(($(this).val()).toString(), sdetails_setting_tags) >= 0){
							this.checked = true;
						}else{
							console.log("entre");
							$("#checkAll").prop("checked", false);
						}
					});


					$('#form_create_settings').modal('show');

					console.log(data);
					loading.hide();
				});
			}

		});

		$('#form_create_settings').on('hidden.bs.modal', function (e) {
			$('#sdetails_setting_name').val('');
			$('#sdetails_setting_url').val('');
			$('#sdetails_setting_read_interval').val('');
			$('#sdetails_setting_items_to_read').val('');
			$('#sdetails_setting_post_interval').val('');
			$('#sdetails_setting_user_from').val('');
			$("#sdetails_setting_destination_profiles option:selected").prop("selected", false);
			$("#sdetails_setting_source_type option:selected").prop("selected", false);
			$("#sdetails_setting_item_choose_way option:selected").prop("selected", false);
			$("#sdetails_setting_tags input:checkbox").prop("checked", false);

			$(".chosen_property").trigger("chosen:updated");
			$("#setting_details").attr('action', '/organic/setting/');
			$("#errorList").empty();
		});

		$( "#sdetails_setting_source_type" ).change(function() {
			console.log($(this).val());
			if($(this).val() == 'Analytics'){
				$(".Analytics").show();
				$(".RRSS").hide();
			}else{
				$(".Analytics").hide();
				$(".RRSS").show();
			}
		});

		$(document).ready(()=>
        {



			$(".chosen_property").chosen({ width: '100%' });

            $(document).on('click', '.btn-save-setting', function(e)
            {
				loading.show();
                e.preventDefault();

                let form_data = $('#setting_details');
                let setting_url = $('#setting_details').attr("action");

				var form = new FormData(form_data[0]);
				form.append('sdetails_setting_destination_profiles', $('#sdetails_setting_destination_profiles').val());

                $.ajax({
                    url: setting_url,
                    data: form,
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    cache: false,
                    processData: false,
                    contentType: false,
                })
                    .done(function(response){
                        response = JSON.parse(response);

                        if(response.hasOwnProperty('url'))
                        {
                            //window.location = response.url;
							$('#form_create_settings').modal('hide');
							table.ajax.reload();
                        }

                        if(response.hasOwnProperty('errors'))
                        {
                            $('#errorList').empty();

                            $.each(response.errors, (i, e) => {
                                let li = $('<li>');

                                li.text(e);

                                $('#errorList').append(li);
                            });
                        }
						loading.hide();
                    });
            });


			$(".checkbox-inline input[type=checkbox]").click(function(){
				var all_check = true;

				$('.checkbox-inline input[type=checkbox]').each(function () {
					if(!$(this).is(':checked')){
						all_check = false;
					}
				});

				$("#checkAll").prop("checked", all_check);

							console.log("entre"+all_check);
			});

            $("#checkAll").click(function(){
                $('input[name="sdetails_setting_tags[]"]').not(this).prop('checked', this.checked);
            });
        });

        var table = $('#settings-table').DataTable({
            processing: true,
            serverSide: true,
			searching: false,
            aLengthMenu: [[25, 50], [25, 50]],
            pageLength: 25,
            ajax: "{{ $tableDataUrl }}",
            oLanguage: {
                    sProcessing: "<div id='settings-table_processing' class='dataTables_processing'>Processing...</div>"
            },
            createdRow: function( row, data, dataIndex ) {
                    $(row).addClass( 'setting' );
            },
            columns: [
                @foreach ($columnsNames as $column)
                {
                    data: '{{ $column }}',
                    orderable: false,
                    visible : true,
                    @if($column === 'name' or $column === 'public_id')
                    fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                        $(nTd).html("<a href='#' class='form_setting' data-id='" + oData.public_id + "'>" + oData.{{ $column }} + "</a>");
                    }
                    @endif
                },
                @endforeach
            ]
        });
    </script>
@endsection
