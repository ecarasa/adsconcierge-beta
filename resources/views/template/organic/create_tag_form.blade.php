@extends('template.general')

<style>
    .kt-portlet__head-toolbar
    {
        padding-left: 1em;
    }

    .kt-content
    {
        padding: 0 !important;
    }

    .fc-time-grid-event
    {
        width: 100%;
    }

    .card-body
    {
        padding: 0rem 6rem !important;
    }

    .kt-portlet__head-bottom
    {
        border-top: 1px solid #ebedf2;
        border-bottom: 0px;
    }

    .kt-subheader__title a
    {
        color: #fff;
    }

    #errorBox {
        padding: 3rem 0 0 0;
    }

    #errorList {
        background: rgb(245 60 121 / 25%);
        color: #000;
        border-radius: 10px;
    }

    #errorList li{
        padding: 1em;
    }

</style>

@section('subheader')
    @include('template.organic.partials._menu')
@endsection

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-contract"></i>
				</span>
                <h3 class="kt-portlet__head-title">
                    Organic: Create Tag
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="/organic/tags" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                            Back
                        </a>
                        <button type="button" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u btn-save-tag">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">

            <div id="errorBox">
                <ul id="errorList">
                    @foreach ($errors->all() as $error)

                        <li>{{ $error }}</li>

                    @endforeach
                </ul>
            </div>

        <!--begin::Form-->
            <form method="post" class="form" id="tag_details" action="/organic/tag/{{ $id }}">
                @csrf
                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8">
                        <div class="my-5">
                            <h3 class="text-dark font-weight-bold mb-10">Tag Details:</h3>
                            <br />

                            <div class="form-group row">
                                <label class="col-3">Name</label>
                                <div class="col-9">
                                    <input name="tdetails_tag_name" class="form-control form-control-solid" type="text" value="{{ $tag['tdetails_tag_name'] }}"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Platform</label>
                                <div class="col-9">
                                    <select class="form-control" name="tdetails_tag_platform">
                                        @foreach($platforms as $key => $platform)
                                            @if($tag['tdetails_tag_platform'] === $key)
                                                <option value="{{ $key }}" selected>{{ $platform }}</option>
                                            @else
                                                <option value="{{ $key }}">{{ $platform }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">URL</label>
                                <div class="col-9">
                                    <input name="tdetails_tag_url" class="form-control form-control-solid" type="text" value="{{ $tag['tdetails_tag_url'] }}"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Auto Update</label>
                                <div class="col-9">
                                    <div class="checkbox-inline">
                                        <label class="checkbox">
                                            <input name="tdetails_tag_auto_update" type="checkbox"/> Yes
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="separator separator-dashed my-10"></div>
                    </div>
                    <div class="col-xl-2"></div>
                </div>
            </form>
            <!--end::Form-->
        </div>

        <div class="kt-portlet__head kt-portlet__head-bottom kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">

				</span>
                <h3 class="kt-portlet__head-title">

                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="/organic/tags" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                            Back
                        </a>
                        <button type="button" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u btn-save-tag">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customscript')

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script type="text/javascript">
        $(document).ready(()=>
        {
            $(document).on('click', '.btn-save-tag', function(e)
            {
                e.preventDefault();

                let form_data = $('#tag_details');
                let tag_url = $('#tag_details').attr("action");

                $.ajax({
                    url: tag_url,
                    data: new FormData(form_data[0]),
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    cache: false,
                    processData: false,
                    contentType: false,
                })
                    .done(function(response){
                        response = JSON.parse(response);

                        if(response.hasOwnProperty('url'))
                        {
                            window.location = response.url;
                        }

                        if(response.hasOwnProperty('errors'))
                        {
                            $('#errorList').empty();

                            $.each(response.errors, (i, e) => {
                                let li = $('<li>');

                                li.text(e);

                                $('#errorList').append(li);
                            });
                        }
                    });
            });
        });
    </script>

@endsection
