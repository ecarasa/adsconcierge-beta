@extends('template.general')

<style>
    .kt-portlet__head-toolbar
    {
        padding-left: 1em;
    }

    .kt-content
    {
        padding: 0 !important;
    }

    .fc-time-grid-event
    {
        width: 100%;
    }

    .card-body
    {
        padding: 0rem 6rem !important;
    }

    .kt-portlet__head-bottom
    {
        border-top: 1px solid #ebedf2;
        border-bottom: 0px;
    }

    .kt-subheader__title a
    {
        color: #fff;
    }

    #errorBox {
        padding: 3rem 0 0 0;
    }

    #errorList {
        background: rgb(245 60 121 / 25%);
        color: #000;
        border-radius: 10px;
    }

    #errorList li{
        padding: 1em;
    }

</style>

@section('subheader')
    @include('template.organic.partials._menu')
@endsection

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-contract"></i>
				</span>
                <h3 class="kt-portlet__head-title">
                    Organic: Create Post
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="/organic" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                            Back
                        </a>
                        <button type="button" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u btn-save-post">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">

            <div id="errorBox">
                <ul id="errorList">
                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach
                </ul>
            </div>

            <!--begin::Form-->
            <form method="post" class="form" id="post_details" action="/organic/post/{{ $id }}">
                @csrf
                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8">
                        <div class="my-5">
                            <h3 class="text-dark font-weight-bold mb-10">Post Details:</h3>
                            <br />
                            <div class="form-group row">
                                <label class="col-3">Title</label>
                                <div class="col-9">
                                    <input name="pdetails_post_title" class="form-control form-control-solid" type="text" value="{{ $post['pdetails_post_title'] }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Image</label>
                                <div class="col-9">
                                    @if(!empty($post['pdetails_post_image']))
                                        <span>
                                            Current file: {{ $post['pdetails_post_image']  }}
                                        </span>
                                        <input name="pdetails_post_image_uploaded" type="hidden" value="true">
                                    @endif
                                    <div class="custom-file">
                                        <input name="pdetails_post_image" type="file" class="custom-file-input" id="customImage">
                                        <label class="custom-file-label" for="customImage">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="contentTextarea">Content</label>
                                <div class="col-9">
                                    <textarea name="pdetails_post_content" class="form-control" id="contentTextarea" rows="2">{{ $post['pdetails_post_content'] }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Promoted</label>
                                <div class="col-9">
                                    <div class="radio-list">
                                        <label class="radio">
                                            @if($post['pdetails_post_promoted'] === 'Y')
                                                <input type="radio" name="pdetails_post_promoted" value="Y" checked="checked"/>
                                            @else
                                                <input type="radio" name="pdetails_post_promoted" value="Y"/>
                                            @endif
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            @if($post['pdetails_post_promoted'] === 'N')
                                                <input type="radio" name="pdetails_post_promoted" value="N" checked="checked"/>
                                            @else
                                                <input type="radio" name="pdetails_post_promoted" value="N"/>
                                            @endif
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Post Datetime</label>
                                <div class="col-9">
                                    <div class="radio-list">
                                        <label class="radio">
                                            <input type="radio" name="pdetails_post_date" value="POSTED" checked="checked"/>
                                            <span></span>
                                            Now
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="pdetails_post_date" value="SCHEDULED"/>
                                            <span></span>
                                            Schedule
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div id="calendar" class="form-group row">
                                <label class="col-3">Schedule Post Date</label>
                                <div class="col-9">
                                    <div id="scheduleDateTime" data-date="{{ $post['pdetails_post_date'] }}"></div>
                                    <input type="hidden" name="pdetails_post_schedule" value="{{ $post['pdetails_post_date'] }}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Platforms</label>
                                <div class="col-9">
                                    <div class="checkbox-inline">
                                        <label class="checkbox">
                                            <input id="checkAll" type="checkbox"/> Check All
                                            <span></span>
                                        </label>
                                    </div>
                                    @foreach($platforms as $key => $platform)
                                        <div class="checkbox-inline">
                                            <label class="checkbox">
                                                @if(in_array($key, $post['pdetails_post_platforms']))
                                                    <input name="pdetails_post_platforms[]" type="checkbox" value="{{ $key }}" checked/> {{ $platform }}
                                                @else
                                                    <input name="pdetails_post_platforms[]" type="checkbox" value="{{ $key }}"/> {{ $platform }}
                                                @endif
                                                <span></span>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="separator separator-dashed my-10"></div>
                    </div>
                    <div class="col-xl-2"></div>
                </div>
            </form>
            <!--end::Form-->
        </div>

        <div class="kt-portlet__head kt-portlet__head-bottom kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">

				</span>
                <h3 class="kt-portlet__head-title">

                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="/organic" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                            Back
                        </a>
                        <button type="button" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u btn-save-post">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customscript')

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script type="text/javascript">
        $(document).ready(()=>
        {
            if($('input[name="pdetails_post_schedule"]').val() !== '')
            {
                $('#calendar').show();

                $('#scheduleDateTime').datetimepicker({
                    inline: true,
                });

                $("input[name=pdetails_post_date][value='SCHEDULED']").prop("checked",true);
            }
            else
            {
                $('#scheduleDateTime').datetimepicker({
                    inline: true,
                });
            }

            $(document).on('click', '.btn-save-post', function(e)
            {
                e.preventDefault();

                let scheduleDate = $("#scheduleDateTime").data("datetimepicker").getDate().toISOString().slice(0, 19).replace('T', ' ');

                $('input[name="pdetails_post_schedule"]').val(scheduleDate);

                let form_data = $('#post_details');
                let post_url = $('#post_details').attr("action");

                $.ajax({
                    url: post_url,
                    data: new FormData(form_data[0]),
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    cache: false,
                    processData: false,
                    contentType: false,
                })
                .done(function(response){
                    response = JSON.parse(response);

                    if(response.hasOwnProperty('url'))
                    {
                        window.location = response.url;
                    }

                    if(response.hasOwnProperty('errors'))
                    {
                        $('#errorList').empty();

                        $.each(response.errors, (i, e) => {
                            let li = $('<li>');

                            li.text(e);

                            $('#errorList').append(li);
                        });
                    }
                });
            });

            $(document).on('change', 'input[name="pdetails_post_date"]', function ()
            {
                if($(this).val() === 'POSTED')
                {
                    $('#calendar').hide();
                }
                else
                {
                    $('#calendar').show();
                }

            });

            $("#checkAll").click(function(){
                $('input[name="pdetails_post_platforms[]"]').not(this).prop('checked', this.checked);
            });
        });
    </script>

@endsection
