<div class="fc-content-col">
    <a
        href="#"
        class="fc-time-grid-event fc-event fc-start fc-end fc-event-success fc-draggable fc-resizable"
        data-id="{{ $post['public_id'] }}"
        data-name="{{ $post['name'] }}"
        data-time_pos="{{ $post['time_position'] }}"
        data-date_pos="{{ $post['date_position'] }}"
        data-toggle="tooltip"
        data-trigger="focus hover"
        data-html="true"
        title="@include('template.organic.partials.tooltip')"
    >
        <div class="fc-content">
            <div class="fc-title">
                {{ $post['name'] }}
            </div>
        </div>
        <div class="fc-resizer fc-end-resizer"></div>
    </a>
</div>
