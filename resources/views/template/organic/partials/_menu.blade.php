<span class="kt-subheader__title">
        <a href="{{ route('organic_home') }}">Home</a>
</span>
<span class="kt-subheader__separator kt-hidden"></span>

<span class="kt-subheader__title">
    <a href="{{ route('organic_settings') }}">Settings</a>
</span>
<span class="kt-subheader__separator kt-hidden"></span>

<span class="kt-subheader__title">
    <a href="{{ route('organic_tags') }}">Tags dictionaries</a>
</span>
<span class="kt-subheader__separator kt-hidden"></span>
