<span class='label label-inline font-weight-bold label-light-primary'>
    ID: {{ $post['public_id'] }}
    @if($post['promoted'] == 'Y')
        <i class='fas fa-ad'></i>
    @endif
    <br />
</span>
<code>
    Total Reach: {{ $post['reach'] }} <br />
    Total Impressions: {{ $post['impressions'] }} <br />
    Total Clicks: {{ $post['clicks'] }} <br />
    Total Engagements: {{ $post['engagements'] }} <br />
    Total Reactions: {{ $post['reactions'] }} <br />
</code>
@foreach($post['platforms'] as $platform)
    @if($platform == 'FBINSIGHTS')
        <span><i class='fab fa-facebook-square'></i> Facebook Insights</span><br />
    @endif
    @if($platform == 'ADWORDS')
        <span><i class='fab fa-google'></i>Ad Words</span><br />
    @endif
    @if($platform == 'SUNNATIVO')
        <span><i class='fas fa-ad'></i>Sunnativo</span><br />
    @endif
@endforeach
@foreach($post['platforms'] as $platform)
    @if($platform == 'FACEBOOK')
        <i class='fab fa-facebook-square'></i>
    @endif
    @if($platform == 'LINKEDIN')
        <i class='fab fa-linkedin'></i>
    @endif
    @if($platform == 'TWITTER')
        <i class='fab fa-twitter-square'></i>
    @endif
    @if($platform == 'INSTAGRAM')
        <i class='fab fa-instagram'></i>
    @endif
    @if($platform == 'SNAPCHAT')
        <i class='fab fa-snapchat-square'></i>
    @endif
    @if($platform == 'GANALYTICS')
        <i class='fab fa-google'></i>
    @endif
    @if($platform == 'PINTEREST')
        <i class='fab fa-pinterest-square'></i>
    @endif
@endforeach
