@extends('template.general')

<link href="../assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css"
    integrity="sha512-0nkKORjFgcyxv3HbE4rzFUlENUMNqic/EzDIeYCgsKa/nwqr2B91Vu/tNAu4Q0cBuG4Xe/D1f/freEci/7GDRA=="
    crossorigin="anonymous" />

<style>
    .kt-portlet__head-toolbar {
        padding-left: 1em;
    }

    .fc-time-grid-event {
        width: 100%;
    }

    .fc-unthemed .fc-event.fc-start.fc-event-success .fc-content:before {
        display: none !important;
    }

    .fc th,
    .fc td {
        border-top: 0px !important;
    }

    .kt-subheader__title a {
        color: #fff;
    }

    .modal {
        background: #00000082;
    }

    #errorList {
        background: rgb(245 60 121 / 25%);
        color: #000;
        border-radius: 10px;
        list-style: none;
    }

    #errorList li {
        padding: 0.5em;
    }

</style>

@section('subheader')
    @include('template.organic.partials._menu')
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="d-flex align-items-center mb-3">
            <div class="d-flex align-items-center">
                <span class="kt-portlet__head-icon">
                    <svg id="_27_Icon_timer" data-name="27) Icon/timer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M9,21A9,9,0,0,1,8.012,3.058a.132.132,0,0,0-.006-.027A.107.107,0,0,1,8,3V2H7A1,1,0,0,1,7,0h4a1,1,0,0,1,0,2H10V3a.106.106,0,0,1-.006.032.132.132,0,0,0-.006.027A9,9,0,0,1,9,21ZM9,5.25A6.75,6.75,0,1,0,15.75,12,6.758,6.758,0,0,0,9,5.25ZM12,13H9a1,1,0,0,1-1-1V9a1,1,0,0,1,2,0v2h2a1,1,0,1,1,0,2Z" transform="translate(3 1)" fill="#222b45"/>
                    </svg>&ensp;
                </span>
                <h3 class="kt-portlet__head-title">
                    Organic: Home
                </h3>
                <div class="kt-portlet__head-toolbar">
                    <div class="fc fc-ltr fc-unthemed">
                        <div class="fc-toolbar fc-header-toolbar" style="margin: 0; padding: 0;">
                            <div class="d-flex align-items-center">
                                <button id="prevPeriod" type="button" class="btn btn-primary btn-icon"
                                    aria-label="prev">
                                    <svg id="_27_Icon_chevron-left" data-name="27) Icon/chevron-left" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                        <path d="M4.862,10A1,1,0,0,1,4.143,9.7L.28,5.7a1,1,0,0,1,.013-1.4l4-4A1,1,0,0,1,5.707,1.707L2.4,5.012,5.581,8.305A1,1,0,0,1,4.862,10" transform="translate(8.5 7)" fill="#ffffff"/>
                                      </svg>
                                      
                                </button>
                                <h2 id="periodTitle">{{ $weekData['month'] }} {{ $weekData['start_day'] }} -
                                    {{ $weekData['end_day'] }}, {{ $weekData['year'] }}</h2>
                                <button id="nextPeriod" type="button" class="btn btn-primary btn-icon"
                                    aria-label="next">
                                    <svg id="_27_Icon_chevron-right" data-name="27) Icon/chevron-right" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                        <path d="M1,10A1,1,0,0,1,.293,8.292L3.6,4.987.418,1.694A1,1,0,0,1,1.857.3l3.862,4a1,1,0,0,1-.012,1.4l-4,4A1,1,0,0,1,1,10" transform="translate(9.5 7.001)" fill="#ffffff"/>
                                      </svg>
                                      
                                </button>

                                <div id="loaderAnim" class="spinner-border text-primary" role="status"
                                    style="margin-top: 5px;margin-left: 3em;">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="#" class="btn btn-info"
                            data-toggle="modal" data-target="#form_create_post">
                            <i class="la la-plus"></i>
                            Create Post
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="fc fc-ltr fc-unthemed">
            <div class="fc-view-container">
                <div class="fc-view fc-timeGridWeek-view fc-timeGrid-view" style="">
                    <table class="">
                        <thead class="fc-head">
                            <tr>
                                <td class="fc-head-container fc-widget-header">
                                    <div class="fc-row fc-widget-header"
                                        style="border-right-width: 1px; margin-right: 14px;">
                                        <table class="">
                                            <thead id="calendarBaseHead">
                                                <tr>
                                                    <th class="fc-axis fc-widget-header" style="width:43.484375px"></th>

                                                    @foreach ($weekData['period'] as $date)
                                                        <th class="fc-day-header fc-widget-header fc-sun {{ $date['class'] }}"
                                                            data-date="{{ $date['raw'] }}">
                                                            <a
                                                                data-goto="{&quot;date&quot;:&quot;{{ $date['raw'] }}&quot;,&quot;type&quot;:&quot;day&quot;}">{{ $date['show'] }}</a>
                                                        </th>
                                                    @endforeach

                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </thead>

                        <tbody class="fc-body">
                            <tr>
                                <td class="fc-widget-content">
                                    <div class="fc-scroller fc-time-grid-container"
                                        style="overflow: hidden scroll; height: 685px;">
                                        <div class="fc-time-grid">
                                            <div class="fc-bg">
                                                <table>
                                                    <tbody id="calendarSkeletonHead">
                                                        <tr>
                                                            <td class="fc-axis fc-widget-content" style="width:43.484375px">
                                                            </td>

                                                            @foreach ($weekData['period'] as $date)
                                                                <td class="fc-day fc-widget-content fc-sun {{ $date['class'] }}"
                                                                    data-date="{{ $date['raw'] }}"></td>
                                                            @endforeach
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="fc-slats">
                                                <table class="">
                                                    <tbody>
                                                        <tr data-time="00:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>12am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="00:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="01:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>1am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="01:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="02:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>2am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="02:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="03:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>3am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="03:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="04:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>4am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="04:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="05:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>5am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="05:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="06:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>6am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="06:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="07:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>7am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="07:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="08:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>8am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="08:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="09:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>9am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="09:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="10:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>10am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="10:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="11:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>11am</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="11:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="12:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>12pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="12:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="13:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>1pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="13:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="14:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>2pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="14:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="15:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>3pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="15:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="16:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>4pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="16:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="17:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>5pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="17:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="18:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>6pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="18:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="19:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>7pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="19:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="20:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>8pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="20:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="21:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>9pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="21:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="22:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>10pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="22:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="23:00:00">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;">
                                                                <span>11pm</span>
                                                            </td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                        <tr data-time="23:30:00" class="fc-minor">
                                                            <td class="fc-axis fc-time fc-widget-content"
                                                                style="width: 43.4844px;"></td>
                                                            <td class="fc-widget-content"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <hr class="fc-divider fc-widget-header" style="display:none">
                                            <div class="fc-content-skeleton">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td class="fc-axis" style="width:43.484375px"></td>

                                                            @for ($pos = 0; $pos < 8; $pos++)
                                                                @include('template.organic.partials.posts_column')
                                                            @endfor
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="d-flex">
            <div class="d-flex align-items-center mb-3">
                <span class="kt-portlet__head-icon">
                    <svg id="_27_Icon_timer" data-name="27) Icon/timer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M9,21A9,9,0,0,1,8.012,3.058a.132.132,0,0,0-.006-.027A.107.107,0,0,1,8,3V2H7A1,1,0,0,1,7,0h4a1,1,0,0,1,0,2H10V3a.106.106,0,0,1-.006.032.132.132,0,0,0-.006.027A9,9,0,0,1,9,21ZM9,5.25A6.75,6.75,0,1,0,15.75,12,6.758,6.758,0,0,0,9,5.25ZM12,13H9a1,1,0,0,1-1-1V9a1,1,0,0,1,2,0v2h2a1,1,0,1,1,0,2Z" transform="translate(3 1)" fill="#222b45"></path>
                    </svg>&ensp;
                </span>
                <h3 class="kt-portlet__head-title">
                    Organic: Post Listing
                </h3>
            </div>
        </div>

        <div class="table-responsive"> {{-- kt-portlet__body --}}
            <table class="table table-striped- table-bordered table-hover table-checkable fc-view-container"
                id="posts-table" data-page-length='25'>
                <thead>
                    <tr>
                        @foreach ($columnsTitles as $column)
                            <th>{{ $column }}</th>
                        @endforeach
                    </tr>
                </thead>
            </table>
        </div>
    </div>


    <!-- Modal-->
    <div class="modal fade" id="form_create_post" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Organic: Create Post</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="errorBox">
                        <ul id="errorList">
                        </ul>
                    </div>
                    <!--begin::Form-->
                    <form method="post" class="form" id="post_details" action="/organic/post/">
                        @csrf
                        <div class="row">
                            <div class="col-xl-2"></div>
                            <div class="col-xl-8">
                                <div class="my-5">
                                    <h3 class="text-dark font-weight-bold mb-10">Post Details:</h3>
                                    <br />
                                    <div class="form-group row">
                                        <label class="col-3">Title</label>
                                        <div class="col-9">
                                            <input id="pdetails_post_title" name="pdetails_post_title"
                                                class="form-control form-control-solid" type="text" value="" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3">Image</label>
                                        <div class="col-9">
                                            <div id="actual-custom-file"></div>
                                            <div class="custom-file">
                                                <input name="pdetails_post_image" type="file" class="custom-file-input"
                                                    id="customImage">
                                                <label class="custom-file-label" for="customImage">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3" for="contentTextarea">Content</label>
                                        <div class="col-9">
                                            <textarea id="pdetails_post_content" name="pdetails_post_content"
                                                class="form-control" id="contentTextarea" rows="4"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3">Post Datetime</label>
                                        <div class="col-9">
                                            <div class="radio-list">
                                                <label class="radio">
                                                    <input type="radio" id="pdetails_post_date_P" name="pdetails_post_date"
                                                        value="POSTED" />
                                                    <span></span>
                                                    Now
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" id="pdetails_post_date_S" name="pdetails_post_date"
                                                        value="SCHEDULED" checked="checked" />
                                                    <span></span>
                                                    Schedule
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="calendar" class="form-group row">
                                        <label class="col-3">Schedule Post Date</label>
                                        <div class="col-9">
                                            <div id="scheduleDateTime"></div>
                                            <input type="hidden" class="form-control form-control-solid"
                                                id="pdetails_post_schedule" name="pdetails_post_schedule" value="" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3">Platforms</label>
                                        <div class="col-9">
                                            <div class="checkbox-inline">
                                                <label class="checkbox">
                                                    <input id="checkAll" type="checkbox" /><span></span> Check All
                                                    
                                                </label>
                                            </div>
                                            @foreach ($platforms as $key => $platform)
                                                <div class="checkbox-inline">
                                                    <label class="checkbox">
                                                        <input name="pdetails_post_platforms[]" type="checkbox"
                                                            value="{{ $key }}" /><span></span> {{ $platform }}
                                                        
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3">Promoted</label>
                                        <div class="col-9">
                                            <div class="radio-list">
                                                <label class="radio">
                                                    <input type="radio" id="pdetails_post_promoted_Y"
                                                        name="pdetails_post_promoted" value="Y" />
                                                    <span></span>
                                                    Yes
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" id="pdetails_post_promoted_N"
                                                        name="pdetails_post_promoted" value="N" />
                                                    <span></span>
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="promoted_data" style="display: none;">
                                        <div class="form-group row">
                                            <label class="col-3">Destination Campaign</label>
                                            <div class="col-9">
                                                <select onChange="javascript:fillAtoms(this);"
                                                    id="sdetails_setting_destination_campaign"
                                                    class="selectpicker"
                                                    name="sdetails_setting_destination_campaign"
                                                    data-placeholder="Choose a campaign...">
                                                    <option value="-1">Select Campaigns</option>
                                                    @foreach ($campaigns as $value)
                                                        <option value="{{ $value->public_id }}">{{ $value->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-3">Destination Atom</label>
                                            <div class="col-9">
                                                <select id="sdetails_setting_destination_campaign_atom"
                                                     name="sdetails_setting_destination_campaign_atom"
                                                    data-placeholder="Choose an Atom...">
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3">Destination property</label>
                                        <div class="col-9">
                                            <select id="sdetails_setting_destination_property[]" multiple
                                                class="selectpicker"
                                                name="sdetails_setting_destination_property[]"
                                                data-placeholder="Choose a property...">
                                                @foreach ($properties_accounts as $value)
                                                    <option value="{{ $value->public_id }}">{{ $value->name }}
                                                        ({{ $value->platform }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                </div>
                                <div class="separator separator-dashed my-10"></div>
                            </div>
                            <div class="col-xl-2"></div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-basic font-weight-bold" data-dismiss="modal">Back</button>
                    <button type="button" class="btn btn-primary font-weight-bold btn-save-post">Save</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('customscript')

    <script src="../assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        let atoms = [

            @foreach ($campaignsWithAtoms as $value)
                { "name": "{{ $value->atom_name }}", "id": "{{ $value->atom_id }}", "campaign_id":
                "{{ $value->campaign_id }}" },
            @endforeach

        ];


        function fillAtoms(elem) {

            //sdetails_setting_destination_campaign_atom

            console.log(elem.value);

            $('#sdetails_setting_destination_campaign_atom').empty();

            $.each(atoms, (i, e) => {
                if (e.campaign_id == elem.value) {
                    $('#sdetails_setting_destination_campaign_atom').append(" <option value='" + e.value + "'>" + e
                        .name + "</option> ");
                }
            });

            $(".chosen_property").chosen({
                width: '100%'
            });
            $('#sdetails_setting_destination_campaign_atom').trigger("chosen:updated");
            $("#sdetails_setting_destination_campaign_atom").chosen({
                width: '100%'
            });



        }

        function get_mindate() {
            var minDate = new Date();
            minDate.setMinutes(minDate.getMinutes() + 30);

            return minDate.getFullYear() + '-' + ('0' + (minDate.getMonth() + 1)).slice(-2) + '-' + ('0' + minDate
                .getDate()).slice(-2) + ' ' + minDate.getHours() + ':' + ('0' + (minDate.getMinutes())).slice(-2);
        }

        $(document).ready(() => {
            $(".chosen_property").chosen({
                width: '100%'
            });


            $('#scheduleDateTime').datetimepicker({
                inline: true,
                fontAwesome: true,
                startDate: get_mindate()
            });

            $(document).on('click', '.btn-save-post', function(e) {
                e.preventDefault();

                let scheduleDate = $("#scheduleDateTime").data("datetimepicker").getDate().toISOString()
                    .slice(0, 19).replace('T', ' ');

                $('input[name="pdetails_post_schedule"]').val(scheduleDate);

                let form_data = $('#post_details');
                let post_url = $('#post_details').attr("action");

                $.ajax({
                        url: post_url,
                        data: new FormData(form_data[0]),
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        cache: false,
                        processData: false,
                        contentType: false,
                    })
                    .done(function(response) {
                        response = JSON.parse(response);

                        if (response.hasOwnProperty('url')) {
                            window.location = "organic";
                        }

                        if (response.hasOwnProperty('errors')) {
                            $('#errorList').empty();

                            $.each(response.errors, (i, e) => {
                                let li = $('<li>');

                                li.text(e);

                                $('#errorList').append(li);
                            });
                        }
                    });
            });


            $('input[name=pdetails_post_promoted]').on('click', function() {


                if ($(this).val() == 'Y') {
                    $('#promoted_data').show();
                } else {
                    $('#promoted_data').hide();
                }

            });


            $(document).on('change', 'input[name="pdetails_post_date"]', function() {
                if ($(this).val() === 'POSTED') {
                    $('#calendar').hide();
                } else {
                    $('#calendar').show();
                }

            });

            $("#checkAll").click(function() {
                $('input[name="pdetails_post_platforms[]"]').not(this).prop('checked', this.checked);
            });
        });

        $('#loaderAnim').hide();

        $(".fc-view-container").on("click", '.fc-time-grid-event', function() {
            $('#loaderAnim').show();
            var id_post = $(this).attr('data-id');
            console.log("entre" + id_post);
            if (id_post == "") {
                $('#pdetails_post_title').val('');
                $('#pdetails_post_schedule').val('');
                $('#scheduleDateTime').val('');
                $('#pdetails_post_content').html('');
                $('#actual-custom-file').html('');
                $("#pdetails_post_promoted_Y").prop("checked", false);
                $("#pdetails_post_promoted_N").prop("checked", false);
                $("input[type=checkbox]").prop("checked", false);
                $("#post_details").attr('action', '/organic/post/');
            } else {

                $.getJSON("/organic/getPost/" + id_post, function(data) {

                    $("#post_details").attr('action', '/organic/post/' + data.pdetails_post_id);

                    $('#pdetails_post_title').val(data.pdetails_post_title);
                    $('#pdetails_post_schedule').val(data.pdetails_post_date);


                    if (data.pdetails_post_date != '') {
                        $('#calendar').show();
                        $('#scheduleDateTime').attr("data-date", data.pdetails_post_date);

                        $('#scheduleDateTime').datetimepicker({
                            inline: true,
                            fontAwesome: true,
                            startDate: get_mindate()
                        });


                        $("#pdetails_post_date_S").prop("checked", true);
                        console.log("entre");
                    } else {
                        $('#scheduleDateTime').datetimepicker({
                            inline: true,
                            fontAwesome: true,
                            startDate: get_mindate()
                        });
                    }


                    $('#scheduleDateTime').datetimepicker("update");

                    $('#scheduleDateTime').val(data.scheduleDateTime);
                    $('#pdetails_post_content').html(data.pdetails_post_content);

                    if (data.pdetails_post_image != "") {
                        $('#actual-custom-file').html('<span>Current file: <img src="' + data
                            .pdetails_post_image +
                            '" style="max-width:250px; max-height:250px;"/></span><input name="pdetails_post_image_uploaded" type="hidden" value="true">'
                        );
                    }

                    if (data.pdetails_post_promoted == 'Y') {
                        $("#pdetails_post_promoted_Y").prop("checked", true);
                    } else {
                        $("#pdetails_post_promoted_N").prop("checked", true);
                    }

                    $("input[type=checkbox]").prop("checked", false);

                    $('input[type=checkbox]').each(function() {
                        if ($(this).val() in data.pdetails_post_platforms) {
                            this.checked = true;
                        }
                    });


                    if (data.pdetails_post_disabled) {
                        $("#post_details input").prop("disabled", true);
                        $("#pdetails_post_content").prop("disabled", true);
                        $('#scheduleDateTime').datetimepicker("remove");
                        $("#pdetails_post_schedule").attr("type", "text");
                        $(".btn-save-post").hide();
                    }

                    $('#form_create_post').modal('show');
                    $('#loaderAnim').hide();
                    console.log(data);
                });
            }
        });

        $('#form_create_post').on('hidden.bs.modal', function(e) {
            $('#pdetails_post_title').val('');
            $('#pdetails_post_schedule').val('');
            $('#scheduleDateTime').val('');
            $('#pdetails_post_content').html('');
            $('#actual-custom-file').html('');
            $("#pdetails_post_promoted_Y").prop("checked", false);
            $("#pdetails_post_promoted_N").prop("checked", false);
            $("input[type=checkbox]").prop("checked", false);
            $('#scheduleDateTime').attr("data-date", new Date());
            $('#scheduleDateTime').datetimepicker("update");
            $("#post_details input").prop("disabled", false);
            $("#pdetails_post_content").prop("disabled", false);
            $('#scheduleDateTime').datetimepicker("update");
            $("#pdetails_post_schedule").attr("type", "hidden");
            $(".btn-save-post").show();
            $("#errorList").empty();
        })

        function createTooltipContent(post) {
            let tooltip = `<span class='label label-inline font-weight-bold label-light-primary'>ID: ${post['public_id']}`;

            if (post['promoted'] === 'Y') {
                tooltip = `${tooltip} <i class='fas fa-ad'></i>`;
            }

            tooltip = `${tooltip} <br /></span>`;

            tooltip = `${tooltip}
                                <code>
                                    Total Reach: ${post['reach']}<br />
                                    Total Impressions: ${post['impressions']}<br />
                                    Total Clicks: ${post['clicks']}<br />
                                    Total Engagements: ${post['engagements']}<br />
                                    Total Reactions: ${post['reactions']}<br />
                                </code>
                            `;

            $.each(post['platforms'], (e, p) => {
                if (p === 'FBINSIGHTS') {
                    tooltip =
                        `${tooltip} <span><i class='fab fa-facebook-square'></i> Facebook Insights</span><br />`;
                }

                if (p === 'ADWORDS') {
                    tooltip = `${tooltip} <span><i class='fab fa-google'></i>Ad Words</span><br />`;
                }

                if (p === 'SUNNATIVO') {
                    tooltip = `${tooltip} <span><i class='fas fa-ad'></i>Sunnativo</span><br />`;
                }
            });

            $.each(post['platforms'], (e, p) => {
                if (p === 'FACEBOOK') {
                    tooltip = `${tooltip} <i class='fab fa-facebook-square'></i>`;
                }

                if (p === 'LINKEDIN') {
                    tooltip = `${tooltip} <i class='fab fa-linkedin'></i>`;
                }

                if (p === 'TWITTER') {
                    tooltip = `${tooltip} <i class='fab fa-twitter-square'></i>`;
                }

                if (p === 'INSTAGRAM') {
                    tooltip = `${tooltip} <i class='fab fa-instagram-square'></i>`;
                }

                if (p === 'SNAPCHAT') {
                    tooltip = `${tooltip} <i class='fab fa-snapchat-square'></i>`;
                }

                if (p === 'GANALYTICS') {
                    tooltip = `${tooltip} <i class='fab fa-google'></i>`;
                }

                if (p === 'PINTEREST') {
                    tooltip = `${tooltip} <i class='fab fa-pinterest-square'></i>`;
                }
            });

            return tooltip;
        }

        function loadEventsInCalendar(posts) {
            let trContent = $('<tr>');

            trContent.append($('<td>').addClass('fc-axis').attr('style', 'width:43.484375px'));

            let tdEl = $('<td>').addClass('post-event-item');

            let eventTemplate = $('<div>').addClass('fc-content-col')
                .append(
                    $('<a>').addClass(
                        'fc-time-grid-event fc-event fc-start fc-end fc-event-success fc-draggable fc-resizable')
                    .append(
                        $('<div>').addClass('fc-content').append(
                            $('<div>').addClass('fc-title')
                        ),
                        $('<div>').addClass('fc-resizer fc-end-resizer')
                    )
                );

            let tdPos = [];

            for (let i = 0; i < 7; i++) {
                if (typeof tdPos[i] === 'undefined') {
                    tdPos[i] = tdEl.clone();
                }
            }

            $.each(posts, (i, post) => {
                let eventItem = eventTemplate.clone();

                let tooltip = createTooltipContent(post);

                eventItem.find('.fc-time-grid-event')
                    .attr('data-id', post['public_id'])
                    .attr('data-name', post['name'])
                    .attr('data-time_pos', post['time_position'])
                    .attr('data-date_pos', post['date_position'])
                    .attr('data-toggle', 'tooltip')
                    .attr('data-trigger', 'focus hover')
                    .attr('data-html', 'true');

                eventItem.find('.fc-time-grid-event').attr('title', tooltip);

                eventItem.find('.fc-title').text(post['name']);

                tdPos[post['date_position']].append(eventItem);
            });

            $.each(tdPos, (i, td) => {
                trContent.append(td);
            });

            $('.fc-content-skeleton tbody tr').replaceWith(trContent);
        }

        function fixEventsInCalendar() {
            let eventsList = $('.post-event-item');
            let colWidth = $('#calendarBaseHead tr th.fc-day-header').outerWidth() - 1;
            let innerWidth = $('#calendarBaseHead tr th.fc-day-header').width();
            let rowHeight = $('.fc-slats tr').height() + $('.fc-slats tr.fc-minor').height();

            $.each(eventsList, (i, pEvent) => {
                let gEvents = $(pEvent).find('.fc-time-grid-event');

                $.each(gEvents, (i, gEvent) => {
                    let timePos = $(gEvent).data('time_pos');

                    console.log("timePos ->" + timePos);

                    if (typeof timePos === 'undefined') {
                        timePos = 1;
                    }

                    let top = rowHeight * parseFloat(timePos);


                    console.log("height ->" + rowHeight);
                    console.log("top ->" + top);
                    console.log("--------");


                    $(gEvent).css({
                        "top": top,
                        "left": "6px",
                        "width": innerWidth,
                        "height": "30px"
                    });
                });

                $(pEvent).css({
                    "width": colWidth,
                    "border": "0px"
                });
            });
        }

        function replaceWeekTableHeaders(data) {
            let weekData = data['weekData'];

            let widgetHeader = $('<tr>');
            let widgetContent = $('<tr>');

            let th = $('<th>')
                .addClass('fc-axis')
                .addClass('fc-widget-header')
                .attr('style', $('th.fc-axis.fc-widget-header').attr('style'));

            let td = $('<th>')
                .addClass('fc-axis')
                .addClass('fc-widget-content')
                .attr('style', $('td.fc-axis.fc-widget-content').attr('style'));

            widgetHeader.append(th);
            widgetContent.append(td);

            let thDay = $('<th>')
                .addClass('fc-day-header')
                .addClass('fc-widget-header')
                .addClass('fc-sun');

            let tdDay = $('<td>')
                .addClass('fc-day')
                .addClass('fc-widget-content')
                .addClass('fc-sun');

            let aDay = $('<a>');

            $.each(weekData['period'], (i, date) => {
                let cloneTHDay = thDay.clone();
                let cloneADay = aDay.clone();
                let cloneTDDay = tdDay.clone();

                cloneADay
                    .data('goto', '{"date":"' + date['raw'] + '", "type": "day"}')
                    .text(date['show']);

                cloneTHDay
                    .data('date', date['raw'])
                    .addClass(date['class']);

                cloneTDDay
                    .data('date', date['raw'])
                    .addClass(date['class']);

                cloneTHDay.append(cloneADay);
                widgetHeader.append(cloneTHDay);

                widgetContent.append(cloneTDDay);
            });

            let periodTitle = weekData['month'] + ' ' + weekData['start_day'] + ' - ' + weekData['end_day'] + ', ' +
                weekData['year'];

            $('#calendarBaseHead tr').replaceWith(widgetHeader);
            $('#calendarSkeletonHead tr').replaceWith(widgetContent);
            $('#periodTitle').text(periodTitle);

        }

        function replaceWeekDetails(data) {
            replaceWeekTableHeaders(data);
            loadEventsInCalendar(data.posts);
            fixEventsInCalendar();
        }

        function getNextWeek() {
            $('#loaderAnim').show();

            let lastDayOfWeek = $('.last-day-of-week').data('date');

            let postData = {
                "previous_end_day": lastDayOfWeek
            };

            $.post("/organic/getPostsByPeriod", postData, function(data) {
                data = JSON.parse(data);
                replaceWeekDetails(data);
                $(document).tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
                $('#loaderAnim').hide();
            });

            this.blur();
        }

        function getPreviousWeek() {
            $('#loaderAnim').show();

            let firstDayOfWeek = $('.first-day-of-week').data('date');

            let postData = {
                "next_start_day": firstDayOfWeek
            };

            $.post("/organic/getPostsByPeriod", postData, function(data) {
                data = JSON.parse(data);
                replaceWeekDetails(data);
                $(document).tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
                $('#loaderAnim').hide();
            });

            this.blur();
        }

        $(document).ready(() => {
            fixEventsInCalendar();

            $(document).on('click', '#prevPeriod', getPreviousWeek);
            $(document).on('click', '#nextPeriod', getNextWeek);

            $("body").tooltip({
                selector: '[data-toggle="tooltip"]'
            });
        });

        $(window).on('resize', function() {
            fixEventsInCalendar();
        });

        $('#posts-table').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            aLengthMenu: [
                [25, 50],
                [25, 50]
            ],
            pageLength: 25,
            ajax: "{{ $tableDataUrl }}",
            oLanguage: {
                sProcessing: "<div id='posts-table_processing' class='dataTables_processing'>Processing...</div>"
            },
            createdRow: function(row, data, dataIndex) {
                $(row).addClass('post');
            },
            columns: [
                @foreach ($columnsNames as $column)
                    {
                    data: '{{ $column }}',
                    orderable: false,
                    visible : true,
                    fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html("<a href='#' class='fc-time-grid-event' data-id='"+oData.public_id+"'>" + oData.{{ $column }} +
                        "</a>");
                    }
                    },
                @endforeach
            ]
        });

    </script>
@endsection
