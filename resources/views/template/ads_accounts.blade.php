@extends('template.general')
		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="../assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		<style>
			.dataTables_filter{
				display:none;
			}
			#kt_content{
				display:flex;
			}
			
			tr th input[type="text"]{
				    width: 100%;
			}
			th, td {
				width:150px;
				word-break: break-word;
			}
			div.dataTables_wrapper div.dataTables_processing {
        position: absolute;
        top: 50%;
        left: 50%;
        width: 200px;
        margin-left: -100px;
        margin-top: -26px;
        text-align: center;

        height: 73px !important;
    }
		</style>
@section('content')						
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<svg id="_27_Icon_volume-up" data-name="27) Icon/volume-up" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
						<path d="M13,18a.989.989,0,0,1-.564-.175l-6.413-4.4H1a1,1,0,0,1-1-1V5.572a1,1,0,0,1,1-1H6.023l6.413-4.4A.988.988,0,0,1,13,0a1,1,0,0,1,1,1V17a1,1,0,0,1-1,1ZM2,6.572v4.857H6.333A1.013,1.013,0,0,1,6.9,11.6L12,15.1V2.9L6.9,6.4a1.013,1.013,0,0,1-.566.175ZM17.5,16a1,1,0,0,1-.637-1.771A6.789,6.789,0,0,0,19.5,9a6.793,6.793,0,0,0-2.638-5.23A1,1,0,0,1,17.5,2a1,1,0,0,1,.637.23A8.754,8.754,0,0,1,21.5,9a8.753,8.753,0,0,1-3.362,6.77A1,1,0,0,1,17.5,16ZM16,13a1,1,0,0,1-.779-1.627,4.014,4.014,0,0,0,0-4.746A1,1,0,0,1,16,5a1,1,0,0,1,.779.373,5.972,5.972,0,0,1,0,7.254A1,1,0,0,1,16,13Z" transform="translate(1 3)" fill="#222b45"/>
					</svg>
				</span>
				<h3 class="kt-portlet__head-title">
				{{ $title }} 
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions" style="display: flex;">
						<div class="dropdown dropdown-inline">
							<!--
							<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="la la-download"></i> Export
							</button>											
							<div class="dropdown-menu dropdown-menu-right">
								<ul class="kt-nav">
									<li class="kt-nav__section kt-nav__section--first">
										<span class="kt-nav__section-text">Choose an option</span>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-print"></i>
											<span class="kt-nav__link-text">Print</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-copy"></i>
											<span class="kt-nav__link-text">Copy</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-file-excel-o"></i>
											<span class="kt-nav__link-text">Excel</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-file-text-o"></i>
											<span class="kt-nav__link-text">CSV</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-file-pdf-o"></i>
											<span class="kt-nav__link-text">PDF</span>
										</a>
									</li>
								</ul>
							</div>
							-->
						</div>
						&nbsp;
						<?php /*
						@if (Auth::user()->damepermisos('crear_usuarios') == 1)
						<div class="card-toolbar">
							
							<div class="dropdown dropdown-inline">
								<a href="#" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="la la-plus"></i>{{ $newElement }} 
								</a>
								<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="">
									<!--begin::Navigation-->
									<div class="kt-notification">
												@if(Auth::user()->validarplataforma())
											<a href="{{URL::to('/')}}/apis/jr_auth_fb.php" class="kt-notification__item">
												<div class="kt-notification__item-icon">
													<i class="flaticon-facebook-letter-logo"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														Facebook
													</div>
												</div>
											</a>
											<a href="{{URL::to('/')}}/apis/jr_auth_fb-insights.php" class="kt-notification__item">
												<div class="kt-notification__item-icon">
													<i class="fab fa-instagram"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														Instagram insights
													</div>
												</div>
											</a>
											<a href="{{URL::to('/')}}/apis/jr_auth_google.php" class="kt-notification__item">
												<div class="kt-notification__item-icon">
													'<i class="fab fa-google"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														Google
													</div>
												</div>
											</a>
											<a href="{{URL::to('/')}}/apis/da_auth_in.php" class="kt-notification__item">
												<div class="kt-notification__item-icon">
													<i class="flaticon-linkedin-logo"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														Linkedin
													</div>
												</div>
											</a>
											<a href="{{URL::to('/')}}/apis/cbsnap.php" class="kt-notification__item">
												<div class="kt-notification__item-icon">
													<i class="fab fa-snapchat-ghost"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														Snapchat
													</div>
												</div>
											</a>
											<a href="{{URL::to('/')}}/apis/cbtwitter.php" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon-twitter-logo"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													Twitter
												</div>
											</div>
										</a>
							
										@else
											<a href="{{route('mysubscription')}}" class="kt-notification__item block">
												<div class="kt-notification__item-icon">
													<i class="flaticon-facebook-letter-logo"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														Facebook <i class="fa fa-ban" style="color:red"></i>
													</div>
													
												</div>
											</a>
											<a href="{{route('mysubscription')}}" class="kt-notification__item block">
												<div class="kt-notification__item-icon">
													<i class="fab fa-instagram"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														Instagram insights <i class="fa fa-ban" style="color:red"></i>
													</div>
												</div>
											</a>
											<a href="{{route('mysubscription')}}" class="kt-notification__item block">
												<div class="kt-notification__item-icon">
													'<i class="fab fa-google"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														Google <i class="fa fa-ban" style="color:red"></i>
													</div>
												</div>
											</a>
											<a href="{{route('mysubscription')}}" class="kt-notification__item block">
												<div class="kt-notification__item-icon">
													<i class="flaticon-linkedin-logo"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														Linkedin <i class="fa fa-ban" style="color:red"></i>
													</div>
												</div>
											</a>
											<a href="{{route('mysubscription')}}" class="kt-notification__item block">
												<div class="kt-notification__item-icon">
													<i class="fab fa-snapchat-ghost"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														Snapchat <i class="fa fa-ban" style="color:red"></i>
													</div>
												</div>
											</a>
											<a href="{{route('mysubscription')}}" class="kt-notification__item block">
												<div class="kt-notification__item-icon">
													<i class="flaticon-twitter-logo"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														Twitter <i class="fa fa-ban" style="color:red"></i>
													</div>
												</div>
											</a>

										@endif
</div>
									<!--end::Navigation-->
								</div>
							</div>
        				</div>
							
						@else
						
							<a href="{{route('mysubscription')}}" class="btn btn-brand btn-elevate btn-icon-sm block">
								<i class="la la-plus"></i>
								{{ $newElement }} &nbsp;
								<i class="fa fa-ban"></i>
							</a>
						@endif*/?>
						
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">

			<!--begin: Datatable -->
			<table class="table table-striped- table-checkable" id="kt_table_1"  data-page-length='10' >
				<thead>
					<tr>												
						@foreach ($columns as $column)
							<th>{{ $column }}</th>
						@endforeach
					</tr>
				</thead>
			</table>

			<!--end: Datatable -->
		</div>
	</div>		


	


@endsection

@section('customscript')

<script src="../assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"      rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="../assets/app/custom/general/crud/datatables/extensions/scroller.js" type="text/javascript"></script>

		<script type="text/javascript">
			$.fn.editable.defaults.mode = 'inline';
    		$.fn.editableform.buttons = '<button type="submit" class="btn btn-primary btn-icon editable-submit"><i class="fas fa-check"></i></button><button type="button" class="btn btn-basic btn-icon editable-cancel"><i class="fas fa-ban"></i></button>';

			// Setup - add a text input to each footer cell
			$('#kt_table_1 thead tr').clone(true).appendTo( '#kt_table_1 thead' );
			
			$('#kt_table_1 thead tr:eq(1) th').each( function (i) {
				var title = $(this).text();
				$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );

				$( 'input', this ).on( 'keyup change', function () {
					if ( table.column(i).search() !== this.value ) {
						table
							.column(i)
							.search( this.value )
							.draw();
					}
				} );
			} );
			
			var rrss = {'FACEBOOK':'<i class="fab fa-facebook-f"></i>', 'LINKEDIN'	:	'<i class="fab fa-linkedin-in"></i>', 'TWITTER'	:	'<i class="fab fa-twitter"></i>','INSTAGRAM'	:	'<i class="fab fa-instagram"></i>','SNAPCHAT' : '<i class="fab fa-snapchat-ghost"></i>','GANALYTICS' : '<i class="fab fa-google"></i>', 'SUNNATIVO' : '<i class="far fa-sun"></i>Nativo', 'FBINSIGHTS' : '<i class="fab fa-facebook-f"></i>Insights', 'PINTEREST' : '<i class="fab fa-pinterest-p"></i>', 'ADWORDS' : 'Adwords', 'OTHER' : 'Other'}
			
			var table = $('#kt_table_1').DataTable({
				//dom: 'Bfrtip',
                //select: false,
				orderCellsTop: true,
        		fixedHeader: true,
                processing: true,
				serverSide: true,
				//engthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
				ajax: "{{ $api }}",
				columns: 
					[
					@foreach ($columns as $column)
						{
							data: '{{ $column }}',
							orderable: true,
						    @if ( $column == 'id' )
					            visible : false,
					        @else
					        	visible : true,
					        @endif 
						    @if ( $column != 'Actions' )
					            responsivePriority: -1,
					        @endif 
						},
					@endforeach
				],
				columnDefs: [
					{
						targets: 0,
						title: 'RRSS',
						render: function(data, type, full, meta){
							return rrss[data];
						}
					},
					{	targets: 1, title: 'Name addaccount'},
					{	targets: 2, title: 'Platform user id'},
					{	targets: 3, title: 'User'},
					{	targets: 4, title: 'Customers',
            			render: function(data, type, full, meta){
							
							let botones = "";

							$.each(full.clients, function(key, item) {
								botones = botones + '<a href="#" data-type="select" data-pk="'+item.public_id+'" data-value="'+item.public_id+'" data-source="/api/getClients_select" data-title="Select Client" class="editable editable-click default_client select" data-original-title="" title="">' + item.name + ' <i class="fas fa-pen editselect" data-pk="'+item.public_id+'" style="margin-left: 5px;"></i></a>&nbsp;';
							});
							
							botones = botones + '<a href="#" data-type="select" data-pk="'+full.id+'" data-value="" data-source="/api/getClients_select" data-title="Select Client" class="editable editable-click default_client select editable-empty" data-original-title="" title="" style="">+Add New</a>';
							
							return botones;

            			}
					}
				]
			});


			table.on('draw.dt', function () {

	 			$('.default_client').editable({
					type: 'text',
					url: 'api/ads_account/add-client',
					success: function (response) {
						if (response === false) {
							return 'Could not save';
						}
					}
				});
			
				$(document).on('click', '.editselect', function(e){
					e.preventDefault();
					e.stopPropagation();
					var data_pk = ".select[data-pk='"+$(this).attr("data-pk")+"']";
					$(data_pk).editable('toggle');
				});

				$(document).on('click', '.editselect2', function(e){
					e.preventDefault();
					e.stopPropagation();
					var data_pk = ".select2[data-pk='"+$(this).attr("data-pk")+"']";
					$(data_pk).editable('toggle');
				});

			});



</script>

@endsection