<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 7
-->
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>Advertising Manager</title>
    <meta name="description" content="Page with empty content">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script src="https://js.chargebee.com/v2/chargebee.js" data-cb-site="adsconcierge-test"></script>

    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });

    </script>
    <!--end::Fonts -->
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/css/payment.css') }}" rel="stylesheet" type="text/css"/>
    <!--end::Page Vendors Styles -->
    <!-- aqui va metacsscommon-->
    @include('template.metas')
</head>
<!-- end::Head -->
<!-- begin::Body -->
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-page--loading ">

<div class="modal-content col-4 centered-plans">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Choose subscription plan</h5>
    </div>
    <div class="modal-body text-center">
        <select class="btn btn-secondary dropdown-toggle" name="subscriptionPlans" id="subscriptionPlans">
            <option class="dropdown-item" value="cbdemo_free">cbdemo_free</option>
            <option class="dropdown-item" value="cbdemo_grow">cbdemo_grow</option>
            <option class="dropdown-item" value="cbdemo_hustle">cbdemo_hustle</option>
            <option class="dropdown-item" value="cbdemo_scale">cbdemo_scale</option>
        </select>
    </div>
    <div class="modal-footer">
        <button type="button" id="continueToSub" class="btn btn-primary">Continue</button>
    </div>
</div>


@include('template.scriptsfoot')
@yield('customscript')
<script type="text/javascript">

    $(document).ready(function () {

        $('#choosePlanModal').modal('show');

        $("#continueToSub").on("click", function () {
            window.location.href = "/planChosen/"+$("#subscriptionPlans").val();
        });

    });
</script>
<!-- Global app Bundle-->
</body>

<!-- end::Body -->
</html>
