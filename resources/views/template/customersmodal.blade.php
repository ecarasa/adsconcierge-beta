<link href="/css/clients.css" rel="stylesheet" type="text/css" />
<link href="/css/chozen.css" rel="stylesheet" type="text/css" />

<style type="text/css">

	.kt-form.nw_usr{
		margin-left: auto;
		margin-right: auto;
	}

	button.btn-success.creat{
		background-color: #46B346 !important;
		border-color: #46B346 !important;
	}

</style>
<!--begin::Form-->
<form class="kt-form nw_usr" id="new_user_form" method="POST" action="{{ $form_action }}" >	
	{{ csrf_field() }}
	<input type="hidden" value="{{ isset($client->public_id) ? $client->public_id : '' }}" name="id" />
	<div class="kt-portlet__body">
		@if ($errors->any())
			<div class="alert alert-warning alert-elevate" role="alert">
				<div class="alert-icon">
					<i class="flaticon-warning kt-font-brand"></i>
				</div>
				<div class="alert-text">											
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			</div>
		@endif
		<!-- {% dd(get_defined_vars()) %} -->
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label" >Name</label>
			<div class="col-10">
				<input class="form-control @error('name') is-invalid @enderror" name="name" type="text" value="{{  old('name') ?  old('name') : ( isset($client) ? $client->name : '' ) }}" id="example-url-input" placeholder="Customer name" required>
			</div>
		</div>
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label">Default</label>
			<div class="col-10">
			<!--	<input type="radio" id="default_yes" name="default" value="Y" {{ (isset($client->isdefault) && ($client->isdefault == 'Y'))? 'checked="checked"' : '' }}>
				<label for="default_yes">Yes</label><br>
				<input type="radio" id="default_no" name="default" value="N" {{ (isset($client->isdefault) && ($client->isdefault == 'Y'))? '' : 'checked="checked"' }} >
				<label for="default_no">No</label> -->
				<span class="switch switch-sm">
				<label class="switch">
					<input type="checkbox" name="default_customer" id="default_customer">
					<span></span>
				</label>
				</span>
				<?php /*<input class="form-control @error('password') is-invalid @enderror" name="password" type="text" value="" id="example-url-input" placeholder="Insert Pass">*/?>
			</div>
		</div>		
		
	@foreach ($platforms as $platform)
		<div class="form-group row rad_btn">
			<div class="col-3">
				<span class="switch switch-sm">
				<label class="switch">
					<input type="checkbox" rrss="{{strtolower($platform['name'])}}" name="rrss[{{strtolower($platform['name'])}}][rrss]" id="rrss_{{strtolower($platform['name'])}}" value="{{strtolower($platform['name'])}}" tab="1" class="redSocial" >
					<span rrss="{{strtolower($platform['name'])}}"></span>
				</label>
				</span>
			</div>
			<div class="col-9">
				<i class="fab fa-{{strtolower($platform['name'])}}"></i> {{strtoupper($platform['name'])}}
			</div>
			<div class="col-12" style="margin-top: 15px; display: none;" id="settings_{{strtolower($platform['name'])}}">
				<div class="row">
					<label for="ads_accounts_facebook" class="col-3 col-form-label" >Ads Account</label>
					<div class="col-9">	
						@if (isset($ads_accounts[strtoupper($platform['name'])])  && count($ads_accounts[strtoupper($platform['name'])]) > 1)
						<select id="ads_{{strtolower($platform['name'])}}" name="rrss[{{strtoupper($platform['name'])}}][ads][]" class="form-control @error('name') is-invalid @enderror chosen-select" name="ads_accounts_{{strtolower($platform['name'])}}" data-placeholder="Choose a ads accounts..."  multiple="">
							<option value=""></option>
								@foreach ($ads_accounts[strtoupper($platform['name'])] as $item)
									<option value="{{$item['public_id']}}">{{$item['name']}}</option>>
								@endforeach							
						</select>
						@else
							No ads accounts		
						@endif
					</div>
				</div>
				<div class="row">
					<label for="properties_{{strtolower($platform['name'])}}" class="col-3 col-form-label" >Properties</label>
					<div class="col-9">	
						@if (isset($properties_accounts[strtoupper($platform['name'])])  && count($properties_accounts[strtoupper($platform['name'])]) > 1)
						<select id="properties_{{strtolower($platform['name'])}}" name="rrss[{{strtoupper($platform['name'])}}][properties][]" class="form-control @error('name') is-invalid @enderror chosen-select" name="properties_facebook" data-placeholder="Choose a properties..."  multiple="">
							<option value=""></option>
							@foreach ($properties_accounts[strtoupper($platform['name'])] as $item)
								<option value="{{$item['public_id']}}">{{$item['name']}}</option>>
							@endforeach	
						</select>
						@else
						<div style="padding: 0.65rem 1rem;">No properties</div>
						@endif
					</div>
				</div>
				<div class="row">
					<label for="fee" class="col-3 col-form-label">Fee</label>
					<div class="col-9">				
						<input class="form-control @error('name') is-invalid @enderror" name="rrss[{{strtoupper($platform['name'])}}][fee]"  type="number" step="0.01" value="{{  old('name') ?  old('name') : ( isset($client) ? $client->name : '' ) }}" id="fee_{{strtolower($platform['name'])}}" placeholder="Insert fee">
					</div>
				</div>
			</div>
		</div>
		@endforeach

		
	</div>
</form>



	