<!-- begin:: Header Menu -->
<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i>
</button>
<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
    <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
    <ul class="kt-menu__nav ">
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel {{ (request()->is('/')) ? 'kt-menu__item--active' : '' }} " data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="{{route('index')}}" class="kt-menu__link">
                    <div>
                        <svg width="25px" height="24px" viewBox="0 0 25 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path fill="#8F9BB3" d="M9.01193763,0.0046519929 C9.55193763,-0.0493480071 10.0229376,0.373651993 10.0229376,0.915651993 L10.0229376,0.915651993 L10.0229376,9.06665199 C10.0229376,9.56965199 10.4309376,9.97765199 10.9349376,9.97765199 L10.9349376,9.97765199 L19.0839376,9.97765199 C19.6259376,9.97765199 20.0499376,10.449652 19.9959376,10.988652 C19.4519376,16.414652 14.5799376,20.566652 8.88193763,19.936652 C4.29693763,19.430652 0.569937626,15.703652 0.0629376262,11.118652 C-0.565062374,5.42065199 3.58493763,0.547651993 9.01193763,0.0046519929 Z M12.5004376,0.000151992897 C16.6354376,0.000151992897 20.0004376,3.36415199 20.0004376,7.50015199 C20.0004376,7.96115199 19.6274376,8.33315199 19.1674376,8.33315199 L19.1674376,8.33315199 L12.5004376,8.33315199 C12.0404376,8.33315199 11.6674376,7.96115199 11.6674376,7.50015199 L11.6674376,7.50015199 L11.6674376,0.833151993 C11.6674376,0.373151993 12.0404376,0.000151992897 12.5004376,0.000151992897 Z" id="Dashboard"></path>
                        </svg>
                    </div>
                    <span class="kt-menu__link-text">Dashboard</span>
                </a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel {{ (request()->is('campana')) ? 'kt-menu__item--active' : '' }}" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="/campana" class="kt-menu__link">
                    <div>
                        <svg width="25px" height="24px" viewBox="0 0 25 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path fill="#8F9BB3" d="M12.434,0.175720699 C12.74,-0.0342793014 13.137,-0.0582793014 13.466,0.115720699 C13.794,0.288720699 14,0.629720699 14,1.0007207 L14,1.0007207 L14,17.0007207 C14,17.3717207 13.794,17.7127207 13.466,17.8857207 C13.32,17.9627207 13.159,18.0007207 13,18.0007207 C12.801,18.0007207 12.604,17.9417207 12.434,17.8257207 L12.434,17.8257207 L6.023,13.4287207 L1,13.4287207 C0.447,13.4287207 0,12.9817207 0,12.4287207 L0,12.4287207 L0,5.5717207 C0,5.0187207 0.447,4.5717207 1,4.5717207 L1,4.5717207 L6.023,4.5717207 Z M17.2294,2.3630207 C17.5814,1.9370207 18.2124,1.8780207 18.6374,2.2300207 C20.8054,4.0260207 21.9994,6.4310207 21.9994,9.0010207 C21.9994,11.5700207 20.8054,13.9750207 18.6374,15.7710207 C18.4504,15.9250207 18.2244,16.0010207 18.0004,16.0010207 C17.7124,16.0010207 17.4274,15.8770207 17.2294,15.6380207 C16.8774,15.2130207 16.9364,14.5830207 17.3624,14.2300207 C19.0624,12.8210207 19.9994,10.9640207 19.9994,9.0010207 C19.9994,7.0370207 19.0624,5.1800207 17.3624,3.7710207 C16.9364,3.4180207 16.8774,2.7880207 17.2294,2.3630207 Z M15.873,5.2215207 C16.303,4.8765207 16.932,4.9435207 17.279,5.3735207 C18.915,7.4075207 18.915,10.5935207 17.279,12.6275207 C17.081,12.8725207 16.791,13.0005207 16.499,13.0005207 C16.279,13.0005207 16.058,12.9285207 15.873,12.7795207 C15.443,12.4335207 15.374,11.8045207 15.72,11.3735207 C16.756,10.0875207 16.756,7.9135207 15.72,6.6275207 C15.374,6.1965207 15.443,5.5675207 15.873,5.2215207 Z"></path>
                        </svg>
                    </div>
                    <span class="kt-menu__link-text">New Campaign</span>
                </a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel  {{ (request()->is('reporting')) ? 'kt-menu__item--active' : '' }}" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="/reporting3" class="kt-menu__link">
                    <div>
                        <svg width="25px" height="24px" viewBox="0 0 25 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path fill="#8F9BB3" d="M8,2.172 L8,4 C8,4.553 8.448,5 9,5 C9.552,5 10,4.553 10,4 L10,2.172 C10.923,2.498 11.651,3.267 11.907,4.271 C11.97,4.492 12,4.732 12,5 C12,6.654 10.654,8 9,8 C7.346,8 6,6.654 6,5 C6,4.738 6.031,4.505 6.091,4.288 C6.094,4.276 6.095,4.265 6.098,4.253 L6.102,4.238 C6.354,3.256 7.08,2.496 8,2.172 M15,3 L13.578,3 C12.799,1.201 11.017,0 9,0 C6.977,0 5.191,1.209 4.42,3 L3,3 C1.346,3 0,4.346 0,6 L0,17 C0,18.654 1.346,20 3,20 L15,20 C16.654,20 18,18.654 18,17 L18,6 C18,4.346 16.654,3 15,3"></path>
                        </svg>
                    </div>
                    <span class="kt-menu__link-text">Reporting</span>
                </a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel  {{ (request()->is('organic')) ? 'kt-menu__item--active' : '' }}" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="/organic" class="kt-menu__link">
                    <div>
                        <svg width="25px" height="24px" viewBox="0 0 25 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path id="feather_stoke" d="M3.45297592,13.8195947 L3.45297592,8.63918931 C3.45297592,4.13914387 8.03852794,0.40752522 17.2648796,0.00518040534 L19,0 L18.9913676,1.73198219 C18.5735575,10.9151141 14.8477965,15.5463964 10.3589278,15.5463964 L3.45297592,15.5463964 L13.8119037,5.18558575 L0,19 L3.45297592,15.5463964 L3.45297592,13.8195947 Z M8.6324398,6.04898664 L8.6324398,10.3659911 L8.6324398,6.04898664 Z M8.6324398,10.3659911 L12.9486597,10.3659911 L8.6324398,10.3659911 Z" style="transform: translate(3px, 3px);" stroke="#8F9BB3" stroke-width="2"></path>
                        </svg>
                    </div>
                    <span class="kt-menu__link-text">Organic</span>
                </a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel  {{ (request()->is('customers')) ? 'kt-menu__item--active' : '' }}" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="/customers" class="kt-menu__link">
                    <div>
                        <svg width="25px" height="24px" viewBox="0 0 25 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path fill="#8F9BB3" d="M7,10 C8.927,10 10.673,10.783 11.94,12.046 C12.809,11.374 13.879,11 15,11 C17.757,11 20,13.243 20,16 C20,16.552 19.553,17 19,17 L19,17 L14,17 C14,17.552 13.553,18 13,18 L13,18 L1,18 C0.447,18 0,17.552 0,17 C0,13.14 3.141,10 7,10 Z M15,4 C16.654,4 18,5.346 18,7 C18,8.654 16.654,10 15,10 C13.346,10 12,8.654 12,7 C12,5.346 13.346,4 15,4 Z M7,0 C9.206,0 11,1.794 11,4 C11,6.206 9.206,8 7,8 C4.794,8 3,6.206 3,4 C3,1.794 4.794,0 7,0 Z"></path>
                        </svg>
                    </div>
                    <span class="kt-menu__link-text">Customers</span>
                </a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel  {{ (request()->is('campaigns')) ? 'kt-menu__item--active' : '' }}" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="/campaigns" class="kt-menu__link">
                    <div>
                        <svg width="25px" height="24px" viewBox="0 0 25 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path fill="#8F9BB3" d="M15,4 L3,4 C2.449,4 2,3.551 2,3 C2,2.449 2.449,2 3,2 L15,2 C15.552,2 16,2.449 16,3 C16,3.551 15.552,4 15,4 L15,4 Z M12,10.134 C12,10.61 11.61,11 11.134,11 L6.866,11 C6.39,11 6,10.61 6,10.134 L6,9.866 C6,9.39 6.39,9 6.866,9 L11.134,9 C11.61,9 12,9.39 12,9.866 L12,10.134 Z M15,0 L3,0 C1.346,0 0,1.346 0,3 C0,3.883 0.391,4.67 1,5.22 L1,15 C1,16.654 2.346,18 4,18 L14,18 C15.654,18 17,16.654 17,15 L17,5.22 C17.609,4.67 18,3.883 18,3 C18,1.346 16.654,0 15,0 L15,0 Z"></path>
                        </svg>
                    </div>
                    <span class="kt-menu__link-text">Campaigns</span>
                </a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="javascript:;" class="kt-menu__link kt-menu__toggle {{ (request()->is('/settings*')) ? 'kt-menu__item--active' : '' }}">
                    <div>
                        <svg width="25px" height="24px" viewBox="0 0 25 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path fill="#8F9BB3" d="M10,8.5 C9.173,8.5 8.5,9.173 8.5,10 C8.5,10.827 9.173,11.5 10,11.5 C10.827,11.5 11.5,10.827 11.5,10 C11.5,9.173 10.827,8.5 10,8.5 M10,13.5 C8.07,13.5 6.5,11.93 6.5,10 C6.5,8.07 8.07,6.5 10,6.5 C11.93,6.5 13.5,8.07 13.5,10 C13.5,11.93 11.93,13.5 10,13.5 M18.317,7.375 L17.233,7.375 C17.086,7.375 16.986,7.258 16.925,7.111 C16.879,7 16.867,6.846 16.972,6.741 L17.742,5.975 C18.06,5.658 18.236,5.235 18.236,4.787 C18.236,4.337 18.062,3.915 17.744,3.598 L16.403,2.257 C15.749,1.601 14.682,1.603 14.026,2.257 L13.256,3.028 C13.152,3.133 12.998,3.12 12.853,3.06 C12.743,3.015 12.625,2.914 12.625,2.767 L12.625,1.676 C12.625,0.752 11.874,0 10.949,0 L9.057,0 C8.129,0 7.375,0.755 7.375,1.683 L7.375,2.767 C7.375,2.914 7.258,3.014 7.111,3.075 C7,3.122 6.845,3.134 6.741,3.028 L5.976,2.259 C5.658,1.94 5.236,1.765 4.787,1.765 L4.785,1.765 C4.336,1.765 3.914,1.939 3.597,2.257 L2.257,3.597 C1.602,4.251 1.602,5.317 2.257,5.974 L3.028,6.744 C3.132,6.848 3.12,7.003 3.06,7.147 C3.015,7.257 2.914,7.375 2.767,7.375 L1.676,7.375 C0.752,7.375 0,8.126 0,9.051 L0,10.943 C0,11.871 0.755,12.625 1.683,12.625 L2.767,12.625 C2.914,12.625 3.014,12.742 3.075,12.889 C3.121,13 3.133,13.154 3.028,13.259 L2.259,14.024 C1.94,14.342 1.765,14.764 1.765,15.213 C1.764,15.663 1.939,16.085 2.257,16.403 L3.597,17.743 C4.251,18.399 5.318,18.397 5.974,17.743 L6.744,16.972 C6.849,16.869 7.003,16.881 7.147,16.94 C7.257,16.985 7.375,17.086 7.375,17.233 L7.375,18.324 C7.375,19.248 8.126,20 9.051,20 L10.943,20 C11.871,20 12.625,19.245 12.625,18.317 L12.625,17.233 C12.625,17.086 12.742,16.986 12.889,16.925 C12.999,16.879 13.154,16.867 13.259,16.972 L14.024,17.741 C14.342,18.06 14.764,18.235 15.213,18.235 L15.215,18.235 C15.664,18.235 16.086,18.061 16.403,17.743 L17.743,16.403 C18.398,15.749 18.398,14.683 17.743,14.026 L16.972,13.256 C16.868,13.152 16.88,12.997 16.927,12.886 C16.927,12.884 16.94,12.854 16.94,12.853 C16.985,12.743 17.086,12.625 17.233,12.625 L18.324,12.625 C19.248,12.625 20,11.874 20,10.949 L20,9.057 C20,8.129 19.245,7.375 18.317,7.375"></path>
                        </svg>
                    </div>
                    <span class="kt-menu__link-text">Settings</span>
                </a>
                <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true">
                            <a href="/users" class="kt-menu__link ">
                                <span class="kt-menu__link-text">
                                    <svg id="_27_Icon_people-fill" data-name="27) Icon/people-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                          <path d="M1,18a1,1,0,0,1-1-1,7,7,0,0,1,11.94-4.955A5,5,0,0,1,20,16a1,1,0,0,1-1,1H14a1,1,0,0,1-1,1ZM12,7a3,3,0,1,1,3,3A3,3,0,0,1,12,7ZM3,4A4,4,0,1,1,7,8,4,4,0,0,1,3,4Z" transform="translate(2 3)" fill="#222b45"/>
                                    </svg>&ensp;Users
                                </span>
                            </a>
                        </li>
                        <li class="kt-menu__item " aria-haspopup="true">
                            <a href="/connections" class="kt-menu__link ">
                                <span class="kt-menu__link-text">
                                    <svg id="_27_Icon_share-fill" data-name="27) Icon/share-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                        <path d="M16,12a2.991,2.991,0,0,0-2.1.861L5.966,9.335A2.858,2.858,0,0,0,6,9a2.858,2.858,0,0,0-.034-.335L13.9,5.139A3,3,0,1,0,13,3a2.858,2.858,0,0,0,.034.335L5.1,6.861a3,3,0,1,0,0,4.278l7.935,3.526A2.858,2.858,0,0,0,13,15a3,3,0,1,0,3-3" transform="translate(2 3)" fill="#222b45"/>
                                    </svg>&ensp;Connections
                                </span>
                            </a>
                        </li>
                        <li class="kt-menu__item " aria-haspopup="true">
                            <a href="/ads_accounts" class="kt-menu__link ">
                                <span class="kt-menu__link-text">
                                    <svg id="_27_Icon_volume-up-fill" data-name="27) Icon/volume-up-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                        <path d="M12.434,17.825l-6.412-4.4H1a1,1,0,0,1-1-1V5.572a1,1,0,0,1,1-1H6.023l6.412-4.4A1,1,0,0,1,14,1V17a1,1,0,0,1-1.566.824Zm4.8-2.187a1,1,0,0,1,.133-1.408A6.8,6.8,0,0,0,20,9a6.8,6.8,0,0,0-2.637-5.23A1,1,0,0,1,18.637,2.23,8.756,8.756,0,0,1,22,9a8.754,8.754,0,0,1-3.362,6.77,1,1,0,0,1-1.408-.132Zm-1.356-2.859a1,1,0,0,1-.153-1.406,4.012,4.012,0,0,0,0-4.746,1,1,0,0,1,1.559-1.254,5.974,5.974,0,0,1,0,7.254A1,1,0,0,1,16.5,13,.99.99,0,0,1,15.873,12.779Z" transform="translate(1 2.999)" fill="#222b45"/>
                                    </svg>&ensp;Ads Accounts
                                </span>
                            </a>
                        </li>
                        <li class="kt-menu__item " aria-haspopup="true">
                            <a href="/properties" class="kt-menu__link ">
                                <span class="kt-menu__link-text">
                                    <svg id="_27_Icon_camera-fill" data-name="27) Icon/camera-fill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                        <path d="M17,18H3a3,3,0,0,1-3-3V7A3,3,0,0,1,3,4H6V2.5A2.5,2.5,0,0,1,8.5,0h3A2.5,2.5,0,0,1,14,2.5V4h3a3,3,0,0,1,3,3v8A3,3,0,0,1,17,18ZM10,7.5A3.5,3.5,0,1,0,13.5,11,3.5,3.5,0,0,0,10,7.5ZM8.5,2a.5.5,0,0,0-.5.5V4h4V2.5a.5.5,0,0,0-.5-.5ZM10,12.5A1.5,1.5,0,1,1,11.5,11,1.5,1.5,0,0,1,10,12.5Z" transform="translate(2 3)" fill="#222b45"/>
                                    </svg>&ensp;Properties
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- end:: Header Menu -->