

<style>
	.containerImagenes{
		display: grid;
		grid-template-columns: repeat(4, 1fr);
		grid-template-rows: repeat(auto, 1fr);
		grid-column-gap: 0px;
		grid-row-gap: 0px;
	}

	.img_preview{
		padding: 6px;
	    width: 160px;
    	height: 160px;
		display: none;
	}

	</style><!-- Modal creas -->
<div class="modal fade" id="modalCreas" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Creativity</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="kiki-close"></i>
                </button>
            </div>
            <div class="modal-body">

                <form class="kt-form nw_usr" id="form_edit_creas" method="POST" action="/creativity">
                    
                    {{ csrf_field() }}


                    <div class="form-group row">
                        <label class="col-lg-3">Creativity Name *</label>
                        <input type="text" class="form-control col-lg-9" name="creas_name" id="creas_name">
                        <input type="hidden" class="form-control col-lg-4" name="creas_id" id="creas_id">
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3">Budget *</label>
                        <input type="text" class="form-control col-lg-9" name="creas_budget" id="creas_budget">
                        
                    </div>

                    <div id="formt_norm">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">
                                Headlines
                            </label>
                            <div  class="col-lg-9 form-group p-0" id="kt_repeater_1">
                                <div class="row" id="kt_repeater_1">
                                    <div data-repeater-list="titulo" class="col-md-12">
                                        <div data-repeater-item class="form-group row align-items-center">
                                            <div class="col-md-9">
                                                <div class="kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                    </div>
                                                    <div class="kt-form__control">
                                                        <input type="text" class="form-control" placeholder="Insert headline and use + to test more..." name="titulo">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div data-repeater-delete="" class="btn-sm btn btn-icon btn-danger  deltitulo">
                                                    <span>
                                                        <svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
                                                    <!--	<span>Delete</span> -->
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="css-tagsinput">					
                                                    <input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..." name="titulotags" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div data-repeater-create="" class="btn btn btn-sm btn-info addtitulo">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Add</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">
                                Ad Texts
                            </label>
                            <div  class="col-lg-9 form-group p-0" id="kt_repeater_7">
                                <div class="row" id="kt_repeater_7">
                                    <div data-repeater-list="descripcion" class="col-md-12">
                                        <div data-repeater-item class="form-group row align-items-center">
                                            <div class="col-md-9">
                                                <div class="kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                    </div>
                                                    <div class="kt-form__control">
                                                        <textarea class="form-control"  rows="3" name="descripcion" placeholder="Insert an ad text and use + to test more..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div data-repeater-delete="" class="btn-sm btn btn-danger btn-icon deldescripcion">
                                                    <span>
                                                        <svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
                                                    <!--	<span>Delete</span> -->
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="css-tagsinput">					
                                                    <input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..." name="descripciontags"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div data-repeater-create="" class="btn btn btn-sm btn-info adddescripcion">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Add</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
        
                        </div>
                        <div id="form-group row" id="kt_repeater_14">
                            <div class="form-group row" id="kt_repeater_14">
                                <label class="col-md-3 col-form-label">
                                    <a href="#" class="tooltip-test" data-toggle="kt-tooltip" title="" data-original-title="Images: Minimiun size for upload 1200x638px.">
                                    <svg id="_27_Icon_question-mark-circle" data-name="27) Icon/question-mark-circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M10,20A10,10,0,1,1,20,10,10.011,10.011,0,0,1,10,20ZM10,2a8,8,0,1,0,8,8A8.009,8.009,0,0,0,10,2Zm0,14a1,1,0,1,1,1-1A1,1,0,0,1,10,16Zm0-3a1,1,0,0,1-1-1V10a1,1,0,0,1,1-1A1.5,1.5,0,1,0,8.5,7.5a1,1,0,0,1-2,0A3.5,3.5,0,1,1,11,10.837V12A1,1,0,0,1,10,13Z" transform="translate(2 2)" fill="#222b45"/> </svg></a> Upload new images/videos
                                    <div data-repeater-create class="btn btn btn-sm btn-info addimagen" style="margin-top: 10px;">
                                            <span>
                                                <i class="la la-plus"></i>
                                                <span>Add</span>
                                            </span>
                                        </div>
                                </label>
                                <div  class="col-lg-9 form-group p-0" >
                                    <div class="row" >
                                        <div data-repeater-list="imagen" class="containerImagenes" >
                                            <div data-repeater-item class="form-group align-items-center">
                                                <div class="col-md-12">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label"></div>
                                                        <div class="kt-form__control">
                                                            <div class="input-group">
                                                              <div class="custom-file">
                                                                <input type="file" class="form-control custom-file-input" aria-describedby="inputGroupFileAddon01" name="imagen" onchange="javascript:imgUpload(this);">
                                                                <label class="custom-file-label" style='overflow: hidden;' name="imagenlabel">Choose file</label>
                                                              </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div data-role="imagenpreview" name="imagenpreview" class='img_preview' style="	background-size: cover; margin-top: 13px;"></div>
                                                    <video name="imagenpreview_video_player" autoplay='true' controls class='img_preview'></video>
                                                    <div data-role="imagenresponse" name="imagenresponse" style="padding: 10px;"></div>
                                                </div>
                                                <div class="col-md-12" name="imagen_inputs_tags" style="display: none;">
                                                    <div class="css-tagsinput">
                                                        <input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..." name="imagentags"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style='    text-align: right; margin-top: 10px;'>
                                                    <div data-repeater-delete="" class="btn-sm btn btn-icon btn-danger  delimagen"><span><svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">
                                URLs *
                            </label>
                            <div  class="col-lg-9 form-group p-0" id="kt_repeater_8">
                                <div class="row" id="kt_repeater_8">
                                    <div data-repeater-list="url" class="col-md-12">
                                        <div data-repeater-item class="form-group row align-items-center">
                                            <div class="col-md-9">
                                                <div class="kt-form__group--inline">
                                                    <div class="kt-form__label"></div>
                                                    <div class="kt-form__control">
                                                        <input type="text" class="form-control" placeholder="Insert URL and use + to test more..." name="url">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div data-repeater-delete="" class="btn-sm btn btn-danger btn-icon delurl">
                                                    <span>
                                                        <svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="css-tagsinput">
                                                    <input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..." name="urltags"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div data-repeater-create="" class="btn btn btn-sm btn-info addurl">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Add</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label"><a href="#" class="tooltip-test" data-toggle="kt-tooltip" title="" data-original-title=" If your website link is long, you can enter text here to create a shorter, friendlier link that people will see on your ad.">
                                <svg id="_27_Icon_question-mark-circle" data-name="27) Icon/question-mark-circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M10,20A10,10,0,1,1,20,10,10.011,10.011,0,0,1,10,20ZM10,2a8,8,0,1,0,8,8A8.009,8.009,0,0,0,10,2Zm0,14a1,1,0,1,1,1-1A1,1,0,0,1,10,16Zm0-3a1,1,0,0,1-1-1V10a1,1,0,0,1,1-1A1.5,1.5,0,1,0,8.5,7.5a1,1,0,0,1-2,0A3.5,3.5,0,1,1,11,10.837V12A1,1,0,0,1,10,13Z" transform="translate(2 2)" fill="#222b45"/> </svg></a> Display Link
                            </label>
                            {{-- disp_link --}}<input type="text" class="form-control col-md-7" name="link_display" id="link_display" placeholder="Insert a link caption...">
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">
                                Link Description
                            </label>
                            <div  class="col-lg-9 form-group p-0" id="kt_repeater_5">
                                <div class="row" id="kt_repeater_5">
                                    <div data-repeater-list="text_link" class="col-md-12">
                                        <div data-repeater-item class="form-group row align-items-center">
                                            <div class="col-md-9">
                                                <div class="kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                    </div>
                                                    <div class="kt-form__control">
                                                        <textarea   class="form-control"  rows="3" placeholder="Insert a link description and use + to test more..."  id="text_link" name="text_link"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div data-repeater-delete="" class="btn-sm btn btn-danger btn-icon deltext_link">
                                                    <span>
                                                        <svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="css-tagsinput">
                                                    <input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..."id="text_linktags" name="text_linktags" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div data-repeater-create="" class="btn btn btn-sm btn-info addtext_link">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Add</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">
                                Call To Action
                            </label>
                            <div  class="col-lg-9" id="kt_repeater_4">
                                <div class="row" id="kt_repeater_4">
                                    <div data-repeater-list="call_to_action" class="col-md-12 p-0">
                                        <div data-repeater-item class="form-group row align-items-center">
                                            <div class="col-md-9">
                                                <div class="kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                    </div>
                                                    <div class="kt-form__control">
                                                        <select   class="form-control" id="call_to_action" aria-invalid="false" name="call_to_action">
                                                            <option value="" data-hidden="false"></option>
                                                            <option value="REQUEST_TIME">Request Time</option>
                                                            <option value="SEE_MENU">See Menu</option>
                                                            <option value="SHOP_NOW">Shop Now</option>
                                                            <option value="SIGN_UP">Sign Up</option>
                                                            <option value="SUBSCRIBE">Subscribe</option>
                                                            <option value="WATCH_MORE">Watch More</option>
                                                            <option value="LISTEN_NOW">Listen Now</option>
                                                            <option value="APPLY_NOW">Apply Now</option>
                                                            <option value="BOOK_TRAVEL">Book Now</option>
                                                            <option value="CONTACT_US">Contact Us</option>
                                                            <option value="DOWNLOAD">Download</option>
                                                            <option value="GET_OFFER">Get Offer</option>
                                                            <option value="GET_QUOTE">Get Quote</option>
                                                            <option value="GET_SHOWTIMES">Get Showtimes</option>
                                                            <option value="LEARN_MORE">Learn More</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div data-repeater-delete="" class="btn-sm btn btn-danger btn-icon delcall_to_action">
                                                    <span>
                                                        <svg id="_27_Icon_trash" data-name="27) Icon/trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"> <path d="M15,20H5a3,3,0,0,1-3-3V6H1A1,1,0,0,1,1,4H6V2.328A2.421,2.421,0,0,1,8.5,0h3A2.422,2.422,0,0,1,14,2.328V4h5a1,1,0,0,1,0,2H18V17A3,3,0,0,1,15,20ZM4,6V17a1,1,0,0,0,1,1H15a1,1,0,0,0,1-1V6ZM8.5,2c-.286,0-.5.173-.5.328V4h4V2.328C12,2.173,11.786,2,11.5,2Z" transform="translate(2 2)" fill="#FFFFFF"></path> </svg>
                                                    <!--	<span>Delete</span> -->
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="css-tagsinput">
                                                    <input type="text" class="form-control" value="" data-role="tagsinput" placeholder="Insert tags and press 'enter'..." name="call_to_actiontags" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-repeater-create="" class="btn btn btn-sm btn-info addcall_to_action">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Add</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="">
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-basic" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveBtnCustomer" onClick="javascript:saveCreas();">Save</button>
            </div>
        </div>
    </div>
</div>
