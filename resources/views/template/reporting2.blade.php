@extends('template.general')
		<!--begin::Page Vendors Styles(used by this page) -->
		
		<link rel="stylesheet" href="https://rawgit.com/dbrekalo/attire/master/dist/css/build.min.css">
		<!--link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"-->
		<script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

  		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  		<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"-->

        <link href="/css/reporting.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
        	.dataSource
			{
			  cursor: pointer;
			  z-index: 1000;
			}

			.card
			{
				min-height: 400px;
			}


			.chartContainer2
			{
			    border-color: coral;
			    border-style: solid;
			}
			.pull-right
			{
				left:auto !important;
			}
			.calItem
			{
				padding-left: 6px;
			}
			.dropdown-menu
			{
				min-width: 8rem !important;
			}
			.ui-resizable-handle {
			  position: absolute;
			  font-size: 0.1px;
			  display: block;
			  touch-action: none;
			  width: 30px;
			  right: -15px;
			}
			.ui-resizable-handle:after {
			  content: "";
			  display: block;
			  position: absolute;
			  top: 50%;
			  left: 50%;
			  transform: translate(-50%, -50%);
			  margin-top: -10px;
			  width: 4px;
			  height: 4px;
			  border-radius: 50%;
			  background: #ddd;
			  box-shadow: 0 10px 0 #ddd, 0 20px 0 #ddd;
			}
			
		</style>
@section('content')	
<style type="text/css">
	@media (min-width: 576px) {
		.col-sm-3, .col-md-3
		{
			width: 25%;
		}
		.col-sm-4, .col-md-4
		{
			width: 33.33333%;
		}
		.col-sm-6, .col-md-6
		{
			width: 50%;
		}
		.col-sm-12, .col-md-12
		{
			width: 100%;
		}
	}
</style>	
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<span class="float-left"></span>
	    	<span class="float-right"><div class="form-group row">
				<div class="btn btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u dataSource" itemid="itm-1" value="dataSource1">
			    <!--span class="kt-portlet__head-icon"-->
					<i class="kt-font-brand flaticon2-line-chart" value="dataSource1"></i>
				<!--/span-->
			    </div>
			    <div itemid="itm-1" class="btn btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u dataSource" value="dataSource2" >
			    <!--span class="kt-portlet__head-icon"-->
					<i class="kt-font-brand flaticon2-image-file"  value="dataSource2"></i>
				<!--/span-->
				</div>
				<div itemid="itm-1" class="btn btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u dataSource" value="dataSource3" >
			    <!--span class="kt-portlet__head-icon"-->
					<i class="kt-font-brand flaticon-customer" value="dataSource3"></i>
				<!--/span-->
				</div>
				<div style="max-width:220px;min-width:150px;">
					<select class="form-control" name="reports" id="reports" style="max-width: 200px;">
						<option value="">select</option>
						<option value="nuevo">nuevo</option>
						<!--option id="1111">1111</option>
						<option id="2222">2222</option>
						<option id="3333">3333</option-->
					</select>
				</div>
				<button type="button" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" id="guardar" style="margin-left: 10px; margin-right: 10px;">
					<i class="la la-save"></i>
				</button>
			</div>
			</span>
		</div>
	</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
				<!--{ { $title } } -->
				Make your reports here
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						<div class="dropdown dropdown-inline">
							
						</div>
						
						<!--a href="/users/new" class="btn btn-brand btn-elevate btn-icon-sm">
							<i class="la la-plus"></i>
							{{ $newElement }} 
						</a-->
						<!--button type="button" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" style="float: right;"><i class="la la-download"></i> Descargar PDF</button-->
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin: Dashboard -->
			<div class="container-fluid">
		      <div class="row fila" id="contentCard">
		      	<?php  /*
		    	<div class="col-md-6 containercard resizable" id="ccard1" style="-webkit-box-flex: unset; max-width: none; flex: 0 0 auto;">
		    	  <div class="card">
				  	<div class="card-body">
				      <div class="row">
				    	<?php $idchart=time()+rand(1,100); 
				    	/*<?php echo $idchart; ?>* /
				    	?>
				    	
				    	<div class="col-md-12 chartContainer2">
				    	  <div class="row">
				    		<p class="arrastre" >Arrastre hasta aquí su gráfica</p>
				    		<div class="col-md-12" name="timingc1<?php /*echo $idchart;* /?>" id="timingc1<?php /*echo $idchart;* /?>" style="display:none;">
			    			  <div class="row" style="height: 30px;">
					    		<h5>Title <a href="#"><i class="la la-pen" style="font-size:20px; padding-top:2px;"></i></a></h5>
					    	  </div>
					    	  <div class="form-group row" style="margin-bottom: 0px !important;">
						    	<div style="max-width:220px;min-width: 150px;">
						    		<select class="form-control metricas1" name="metricas_1_1" id="metricas_1_1">
						    			<option id="">select</option>
						    		</select>
						    	</div>
					    		<div style="max-width:220px;min-width: 150px; display:none;" id="divMetricas_1">
					    			<select class="form-control metricas2" name="metricas_1_2" id="metricas_1_2">
					    				<option id="">select</option>
					    			</select>
					    		</div>
					    		<button type="button" class="btn btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u addMetrics" id="addMetrics_1" style="margin-left: 10px; margin-right: 10px; padding-left: 0px; padding-right: 0px;">
					    			<i class="la la-plus"></i>
					    		</button>
					    		<!--button type="button" class="btn btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" id="del_metrics" style="margin-right: 10px; margin-right: 10px;display:none;">
					    			<i class="la la-minus"></i>
					    		</button-->
					    	  </div>
				    		</div>
				    	  </div>
					      <div class="chart chartContainer" rel-type="globalChart" id="c1<?php /*echo 'c'.$idchart;* / ?>"  style="height: 400px; width: 100%;">
					    		<!--img src="img/grafica.png" style="width: 100%;"-->
					    		<!--button id="pruebaChart">press</button-->
					      </div>
				    	</div>
					  </div>
					</div>
				  </div>    			
		    	</div>	    	
			    	
		    	<!--/div-->
		    		<!-- fin fila 2 graficas -->
		    	*/ ?>
		      </div>
		    </div>
		    <!--Modal para nuevo report-->	
		    <div class="modal" id="kt_modal_nombre" tabindex="-1" role="dialog">
			    <div class="modal-dialog modal-sm" role="document">
			        <div class="modal-content">
			            <div class="modal-header">
			                <h5 class="modal-title" id="exampleModalLabel">New Report</h5>
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                </button>
			            </div>
			            <div class="modal-body">
			                <form>
			                    <div class="form-group">
			                        <label for="recipient-name" class="form-control-label">Name:</label>
			                        <input type="text" class="form-control" id="dashboard-name">
			                    </div>
			                </form>
			            </div>
			            <div class="modal-footer">
			                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			                <button type="button" class="btn btn-primary" id="guardaNameDashboard">Save</button>
			            </div>
			        </div>
			    </div>
			</div>
			<!--end: Modal para nuevo report-->
			<!--end: Dashboard -->
		</div>
	</div>						
@endsection

@section('customscript')
<!--begin::Page Vendors(used by this page) -->
		<!--script src="/js/apexcharts-3.11.js" type="text/javascript"></script-->
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  		<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"-->
		<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    	<script src="/js/reporting.js" type="text/javascript"></script>
    	<script src="https://canvasjs.com/assets/script/canvasjs.min.js" type="text/javascript"></script>

    	<!--script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script-->
 		<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		<!--script src="https://code.jquery.com/jquery-2.2.3.min.js" type="text/javascript"></script-->

		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		<!-- Con este script es que se hace la carga de la data-->
		

<script type="text/javascript">
	var	objetoDashboard = {};
	var totales={};
	var filas={};
	var grender=0;
	var select_val=["impresions","spend","ecpm","clicks","ctr"];
	var charts = [];
	var i_cards=0; //variables para controlar los id de nuevos card
	var graficasId; 
	<?php if(isset($idGraph)){ echo "graficasId='$idGraph';"; } ?>
	//console.log(graficasId);
	var a_impresions=[];
	var a_spend=[];
	var a_ecpm=[];
	var a_clicks=[];
	var a_ctr=[];
	var a_totals=[];
	var total;
	var categoria=[];

	var arrayGraficas = new Object();

	/*
	//llenar select metricas
	$('#metricas_1').empty();
	for (i=0;i<select_val.length;i++){
		$('#metricas_1').append("<option value='"+select_val[i]+"' >"+select_val[i]+"</option>");
	}
	$('#metricas_1').selectpicker('refresh');

	//llenar select metricas 2
	$('#metricas_2').empty();
	for (i=0;i<select_val.length;i++){
		$('#metricas_2').append("<option value='"+select_val[i]+"' >"+select_val[i]+"</option>");
	}
	$('#metricas_2').append("<option value='none'>none</option>");
	$('#metricas_2').selectpicker('refresh');
	*/
	/* Ojo: esto debe hacerse para que funcione para cada gráfica nueva */
	//llenar select metricas_2
	/* 20/06
	$('#metricas_1_1').empty();
	for (i=0;i<select_val.length;i++){
		$('#metricas_1_1').append("<option value='"+select_val[i]+"' >"+select_val[i]+"</option>");
	}
	$('#metricas_1_1').selectpicker('refresh');

	//llenar select metricas_2 2
	$('#metricas_1_2').empty();
	for (i=0;i<select_val.length;i++){
		$('#metricas_1_2').append("<option value='"+select_val[i]+"' >"+select_val[i]+"</option>");
	}
	$('#metricas_1_2').append("<option value='none'>none</option>");
	$('#metricas_1_2').selectpicker('refresh');
	*/
	var options = {
		chart: {
			type: 'line',
			height: 350,
			toolbar: {
		        show: true,
		        offsetX: 0,
		        offsetY: -30,
		        tools: {
		          download: true,
		          selection: true,
		          zoom: false,
		          zoomin: false,
		          zoomout: false,
		          pan: false,
		          reset: false | '<img src="/static/icons/reset.png" width="20">',
		          customIcons: [
					{
						icon: '<a href="#" class="b_delete" id="b_eliminar_c"><i class="la la-trash" style="font-size:20px; padding-top:2px;"></i></a>',
						index: -3,
						title: 'delete',
						class: 'custom-icon',
						click: function (chart, options, e) {
						  var divcontent = $("#"+chart['el'].id).closest(".containercard");
						  console.log(divcontent);
						  $(divcontent).remove();
						}
					},
					{
						icon: '<div class="btn-group open"><a class="" href="javascript::" data-toggle="dropdown" aria-expanded="false"><i class="la la-calendar" style="font-size:20px; padding-top:2px;"></i></a><ul class="dropdown-menu pull-right" role="menu" style="float:right;left:auto !important; right:0;"><li class="calItem">Today</li><li class="calItem">Yesterday</li><li class="calItem">Last 2 days</li><li class="calItem">This week</li><li class="calItem">Last week</li><li class="calItem">Last 7 days</li><li class="calItem active">This month</li><li class="calItem">Last month</li><li class="calItem">Last 30 days</li><li class="calItem active">All time</li></ul></div>',
						index: -2,
						title: 'date',
						class: 'custom-icon',
						click: function (chart, options, e) {
						  console.log("clicked fechas")
						}
					}]
		        },
		        autoSelected: 'zoom' 
		    },
		},
		series: [{
		name: 'impresions',
		data: [30,40,35,50,49,60,70,91,136] //o aqui la data de consulta ajax
		}],
		xaxis: {
			/*labels: {
				show: true,
			 	rotate: -45,
	            rotateAlways: false,
	            hideOverlappingLabels: true,
	            trim: false
	        },*/
			categories: [1991,1992,1993,1994,1995,1996,1997, 1998,1999]
		},
		redrawOnParentResize: true,
		/*labels: ['impresions'],*/
		legend: {
			show: false
		}
	};

	//1706 var chart = new ApexCharts(document.querySelector("#chart"), options); //#chart
	
	//var chartDrag = new ApexCharts(document.querySelector(".chartContainer"), options);

//se sustituyó por un objeto
/*
$.ajax({
    url: "/api/reporting/data",
    type: "post",
    dataType: "json",
	data: { 'datos': objetoDashboard }	 
})
    .done(function(res){
        console.log(res['data']);
        mostrarDatos(res['data']);

        if (graficasId) {,
			cargarGraficasId(graficasId);
		}
    });
*/
var objetoDatosBase = {
		filas:{ 20200501: {impresions: 41963, spend: 10649, ecpm: 94483, clicks: 89651, ctr: 76298},
				20200502: {impresions: 48441, spend: 57281, ecpm: 15100, clicks: 41631, ctr: 13116},
				20200503: {impresions: 83820, spend: 68789, ecpm: 75352, clicks: 30768, ctr: 21666},
				20200504: {impresions: 83478, spend: 35135, ecpm: 43899, clicks: 20173, ctr: 25449},
				20200505: {impresions: 12763, spend: 58412, ecpm: 14191, clicks: 49430, ctr: 71945},
				20200506: {impresions: 14522, spend: 55053, ecpm: 99709, clicks: 73018, ctr: 46363},
				20200507: {impresions: 25739, spend: 43564, ecpm: 20614, clicks: 8449, ctr: 74432},
				20200508: {impresions: 45590, spend: 55960, ecpm: 67483, clicks: 16043, ctr: 34002},
				20200509: {impresions: 85827, spend: 5168, ecpm: 49896, clicks: 29225, ctr: 36453},
				20200510: {impresions: 12099, spend: 37849, ecpm: 75842, clicks: 15815, ctr: 94060},
				20200511: {impresions: 15763, spend: 97875, ecpm: 44326, clicks: 85991, ctr: 63912},
				20200512: {impresions: 64978, spend: 49664, ecpm: 9568, clicks: 93176, ctr: 83740},
				20200513: {impresions: 5370, spend: 61598, ecpm: 78762, clicks: 19710, ctr: 41056},
				20200514: {impresions: 12124, spend: 27636, ecpm: 78334, clicks: 69327, ctr: 65571},
				20200515: {impresions: 13405, spend: 67151, ecpm: 89177, clicks: 67799, ctr: 30555},
				20200516: {impresions: 74114, spend: 96683, ecpm: 68214, clicks: 60888, ctr: 13450},
				20200517: {impresions: 20212, spend: 78086, ecpm: 33434, clicks: 1301, ctr: 70400},
				20200518: {impresions: 35230, spend: 87773, ecpm: 45591, clicks: 23835, ctr: 84069},
				20200519: {impresions: 12022, spend: 95081, ecpm: 32792, clicks: 52899, ctr: 39200},
				20200520: {impresions: 54660, spend: 47720, ecpm: 68413, clicks: 91733, ctr: 1562},
				20200521: {impresions: 80835, spend: 72862, ecpm: 68605, clicks: 61464, ctr: 3944},
				20200522: {impresions: 51920, spend: 90238, ecpm: 59328, clicks: 41939, ctr: 61668},
				20200523: {impresions: 87092, spend: 26767, ecpm: 72454, clicks: 42252, ctr: 72955},
				20200524: {impresions: 75563, spend: 3827, ecpm: 31299, clicks: 95858, ctr: 42197},
				20200525: {impresions: 91605, spend: 70874, ecpm: 84937, clicks: 68358, ctr: 52039},
				20200526: {impresions: 41194, spend: 11505, ecpm: 93711, clicks: 12116, ctr: 13975},
				20200527: {impresions: 18666, spend: 57489, ecpm: 28692, clicks: 79966, ctr: 22767},
				20200528: {impresions: 32800, spend: 44661, ecpm: 32948, clicks: 88966, ctr: 15789},
				20200529: {impresions: 21821, spend: 19588, ecpm: 51894, clicks: 57998, ctr: 93244},
				20200530: {impresions: 34823, spend: 63529, ecpm: 11529, clicks: 70876, ctr: 60189},
				20200531: {impresions: 21823, spend: 32529, ecpm: 19529, clicks: 40876, ctr: 23189},
		},
		totales: {impresions: 14535, spend: 16486, ecpm: 3020, clicks: 58876, ctr: 89610}
	};
	mostrarDatos(objetoDatosBase);

	var objetoGlobalGraficas=[
		{
			id: "1111",
			graficas: [
				{id: "c1", titulo: "Líneas", tipoChart: "line", ancho: 25, metricas: ["impresions","spend"], date: "all time"},
				{id: "c2", titulo: "Barras", tipoChart: "line", ancho: 33, metricas: ["clicks"], date: "all time"},
				{id: "c3", titulo: "líneas", tipoChart: "bar", ancho: 33, metricas: ["impresions"], date: "all time"},
				{id: "c4", titulo: "Title", tipoChart: undefined, ancho: 50, metricas: ["impresions"], date: "all time"}
			]
		},
		{
			id: "2222",
			graficas: [
				{id: "c1", titulo: "Líneas", tipoChart: "line", ancho: 25, metricas: ["impresions","spend"], date: "all time"},
				{id: "c2", titulo: "Title", tipoChart: undefined, ancho: 50, metricas: ["impresions"], date: "all time"}
			]
		},
		{
			id: "3333",
			graficas: [
				{id: "c1", titulo: "Barras", tipoChart: "bar", ancho: 33, metricas: ["clicks"], date: "all time"},
				{id: "c2", titulo: "Title", tipoChart: undefined, ancho: 50, metricas: ["impresions"], date: "all time"}
			]
		}
	];

	//se completa el select de reports con los datos de la consulta (ids de graficas guardadas)
	objetoGlobalGraficas.forEach(function(elemento){
		$('#reports').append("<option value='"+elemento.id+"' >"+elemento.id+"</option>");
	});

    if (graficasId) {
		cargarGraficasId(graficasId);
	}
	else{
		mostrarNuevoCard();
	}
/*});*/

			/*function cargarSelect(entidad, datos){
				$('#select_'+entidad).empty();
				for (i=0;i<datos.length;i++){
					$('#select_'+entidad).append("<option value='"+datos[i]['id']+"' >"+datos[i]['nameValue']+"</option>");
				}
				$('#select_'+entidad).selectpicker('refresh');
			};*/

//Dotos del objetoGlogal cargados (1111)
/*

objetoGlobalReport={
	id: "option1",
	graficas: [
		{id: "c1", titulo: "Title ", tipoChart: "radialBar", ancho: 25, metricas: ["impresions","spend"], date: "all time"},
		{id: "c2", titulo: "Title ", tipoChart: "radialBar", ancho: 33, metricas: ["impresions"], date: "all time"},
		{id: "c3", titulo: "Title ", tipoChart: undefined, ancho: 50, metricas: ["impresions"], date: "all time"}
	]
}
*/


function updateobjetoglobal(entidad,dato) {
	objetoDashboard[entidad]=dato;
	//lanzar funcion update chart o update otros datos, segun logicas que se configuren aqui
	//console.log('OG', objetoDashboard);
	return true;
}



function mostrarDatos(datos){
	totales=datos['totals'];
	filas=datos['filas'];
	//coloca los valores totales en la cajas correspondientes
	$.each(totales, function(key, value){
		$('#p_'+key).text(value);
	});

	cargarGrafica('clicks');  //ojo aqui van las preferencias
}
/*var a_impresions=[];
var a_spend=[];
var a_ecpm=[];
var a_clicks=[];
var a_ctr=[];
var a_totals=[];
var total;
var categoria=[];*/
//se cargar los datos con el nombre de la caja como parámetro
function cargarGrafica(parametro){
	
	//console.log(filas);
	$.each(filas, function(key, value){
		//console.log(value);
		a_impresions.push(value['impresions']); //ojo debe ir en funcion a parametros
		a_spend.push(value['spend']);
		a_ecpm.push(value['ecpm']);
		a_clicks.push(value['clicks']);
		a_ctr.push(value['ctr']);
		total = value['impresions'] + value['spend'] + value['clicks'];
		a_totals.push(total);
		categoria.push(key);
	});
	/* esta parte cargaba la gráfica generica (inicial)
	if (grender==0){
		chart.render();
		grender=1;
	}
	
	chart.updateSeries([{
  		data: a_impresions
	}]);
	chart.appendSeries({
	  name: 'spend',
	  data: a_spend
	});
	chart.appendSeries({
	  name: 'ecpm',
	  data: a_ecpm
	});
	chart.appendSeries({
	  name: 'clicks',
	  data: a_clicks
	});
	chart.appendSeries({
	  name: 'ctr',
	  data: a_ctr
	}); */
	/*chart.appendSeries({
	  name: 'totals',
	  data: a_totals
	})*/
	/*1706
	chart.updateOptions({
		xaxis: {
			categories: categoria,
			labels: {
			 	rotate: -45,
	            rotateAlways: false,
	            hideOverlappingLabels: true,
	            trim: true/*,
	            formatter: function(value, timestamp, index) {
	              if (value%5 == 0){
	              	//console.log(value+' '+timestamp+' '+index);
	              	return value
	              } 
	            }* /
	        }
		}
	});
	mostrarUnicaSerie(chart,$('#metricas_1').val());*/
	
}
function mostrarUnicaSerie(graphi, parametro){
	graphi.hideSeries('impresions');
	graphi.hideSeries('clicks');
	graphi.hideSeries('spend');
	graphi.hideSeries('ecpm');
	graphi.hideSeries('ctr');
	graphi.showSeries(parametro);
}
//para mostrar las 2 series, se pudo hacer con un arreglo de parametros a mostrar en una sola funcion
function mostrarDosSeries(graphi, parametro1,parametro2){
	graphi.hideSeries('impresions');
	graphi.hideSeries('clicks');
	graphi.hideSeries('spend');
	graphi.hideSeries('ecpm');
	graphi.hideSeries('ctr');
	graphi.showSeries(parametro1);
	graphi.showSeries(parametro2);
}

$('.dataSource').draggable({
  cursor: 'move',
  helper: "clone"
});

//funcion para crear el nuevo card para mas gráficas
function mostrarNuevoCard(){
	/*if (numero!=0){
		i_cards=numero;
		oculto='';
	}else{*/
		i_cards++;	
		/*oculto='style="display:none"';
	}*/
	
    var chartContainer2 = '<div class="col-md-12 chartContainer2"><div class="row"><p class="arrastre" >Arrastre hasta aquí su gráfica</p><div class="col-md-12" name="timingc'+i_cards+'" id="timingc'+i_cards+'" style="display:none"><div class="row" style="height: 30px;"><h5>Title <a href="#"><i class="la la-pen" style="font-size:20px; padding-top:2px;"></i></a></h5></div><div class="form-group row" style="margin-bottom: 0px !important;"><div style="max-width:220px;min-width: 150px;"><select class="form-control metricas1" name="metricas_'+i_cards+'_1" id="metricas_'+i_cards+'_1"><option id="">select</option></select></div><div style="max-width:220px;min-width: 150px; display:none;" id="divMetricas_'+i_cards+'"><select class="form-control metricas2" name="metricas_'+i_cards+'_2" id="metricas_'+i_cards+'_2"><option id="">select</option></select></div><button type="button" class="btn btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u addMetrics" id="addMetrics_'+i_cards+'" style="margin-left: 10px; margin-right: 10px; padding-left: 0px; padding-right: 0px;"><i class="la la-plus"></i></button></div></div></div><div class="chart chartContainer" rel-type="globalChart" id="c'+i_cards+'" style="height: 400px; width: 100%;"></div></div>';

    $("#contentCard").append('<div class="col-md-6 containercard resizable" id="ccard'+i_cards+'" style="-webkit-box-flex: unset; max-width: none; flex: 0 0 auto;"><div class="card"><div class="card-body"><div class="row">'+chartContainer2+'</div></div></div></div>');

    $('#metricas_'+i_cards+'_1').empty();
	for (i=0;i<select_val.length;i++){
		$('#metricas_'+i_cards+'_1').append("<option value='"+select_val[i]+"' >"+select_val[i]+"</option>");
	}
	$('#metricas_'+i_cards+'_1').selectpicker('refresh');

	//llenar select metricas_2 2
	$('#metricas_'+i_cards+'_2').empty();
	for (i=0;i<select_val.length;i++){
		$('#metricas_'+i_cards+'_2').append("<option value='"+select_val[i]+"' >"+select_val[i]+"</option>");
	}
	$('#metricas_'+i_cards+'_2').append("<option value='none'>none</option>");
	$('#metricas_'+i_cards+'_2').selectpicker('refresh');

    $(".chartContainer").droppable(droppableChartOption);
    console.log('nuevo');
    /*para el resize*/
	$(resizeOption);
	/*fin resize*/
}

//Función para crear div card con las graficas guardadas
function mostrarCardGuardados(graficaItem){
	var numero;
	if (graficaItem.tipoChart){
		numero= graficaItem.id.substring(1);
		//console.log(numero);
		i_cards=numero;
		ocultoDiv='';
		ocultoP='style="display:none"';
	}else{
		i_cards++;
		ocultoDiv='style="display:none"';
		ocultoP='';
	}
	switch (graficaItem.ancho) {
	    case 25:
	    	bAncho = '3';
	    	break;
	    case 33:
	    	bAncho = '4';
	    	break;
	    case 50:
	    	bAncho = '6';
	    	break;
	    case 100:
	    	bAncho = '12';
	    	break;
	    }

	//i_cards=graficaItem.id.substring(2);
	console.log('i_cards '+i_cards);
	
    var chartContainer2 = '<div class="col-md-12 chartContainer2"><div class="row"><p class="arrastre" '+ocultoP+'>Arrastre hasta aquí su gráfica</p><div class="col-md-12" name="timingc'+i_cards+'" id="timingc'+i_cards+'" '+ocultoDiv+'><div class="row" style="height: 30px;"><h5>'+graficaItem.titulo+' <a href="#"><i class="la la-pen" style="font-size:20px; padding-top:2px;"></i></a></h5></div><div class="form-group row" style="margin-bottom: 0px !important;"><div style="max-width:220px;min-width: 150px;"><select class="form-control metricas1" name="metricas_'+i_cards+'_1" id="metricas_'+i_cards+'_1"><option id="">select</option></select></div><div style="max-width:220px;min-width: 150px; display:none;" id="divMetricas_'+i_cards+'"><select class="form-control metricas2" name="metricas_'+i_cards+'_2" id="metricas_'+i_cards+'_2"><option id="">select</option></select></div><button type="button" class="btn btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u addMetrics" id="addMetrics_'+i_cards+'" style="margin-left: 10px; margin-right: 10px; padding-left: 0px; padding-right: 0px;"><i class="la la-plus"></i></button></div></div></div><div class="chart chartContainer" rel-type="globalChart" id="c'+i_cards+'" style="height: 400px; width: 100%;"></div></div>';

    $("#contentCard").append('<div class="col-md-'+bAncho+' containercard resizable" id="ccard'+i_cards+'" style="-webkit-box-flex: unset; max-width: none; flex: 0 0 auto;"><div class="card"><div class="card-body"><div class="row">'+chartContainer2+'</div></div></div></div>');

    $('#metricas_'+i_cards+'_1').empty();
	for (i=0;i<select_val.length;i++){
		$('#metricas_'+i_cards+'_1').append("<option value='"+select_val[i]+"' >"+select_val[i]+"</option>");
	}
	$('#metricas_'+i_cards+'_1').selectpicker('refresh');

	//llenar select metricas_2 2
	$('#metricas_'+i_cards+'_2').empty();
	for (i=0;i<select_val.length;i++){
		$('#metricas_'+i_cards+'_2').append("<option value='"+select_val[i]+"' >"+select_val[i]+"</option>");
	}
	$('#metricas_'+i_cards+'_2').append("<option value='none'>none</option>");
	$('#metricas_'+i_cards+'_2').selectpicker('refresh');

    $(".chartContainer").droppable(droppableChartOption);
    console.log('nuevo');
    /*para el resize*/
	$(resizeOption);
	/*fin resize*/
	contenedor='c'+i_cards;
	if (i_cards==numero){
		montarGrafica(contenedor, graficaItem.tipoChart);
	}
	$('#metricas_'+i_cards+'_1').val(graficaItem.metricas[0]);
	$('#metricas_'+i_cards+'_1').selectpicker('refresh');
	//$('#metricas_'+i_cards+'_1').change();
	graphD = charts.find(x => x.id == contenedor);
	/*if (!$('#divMetricas_'+aNombreSelect[1]).is(":visible")){
		mostrarUnicaSerie(graphD.chartDrag, $(this).val());
		}else{
			mostrarDosSeries(graphD.chartDrag, $(this).val(),$('#metricas_'+aNombreSelect[1]+'_2').val());
			*/

	if (graficaItem.metricas[1]){
		$('#metricas_'+i_cards+'_2').val(graficaItem.metricas[1]);
		$('#metricas_'+i_cards+'_2').selectpicker('refresh');
		//$('#metricas_'+i_cards+'_2').change();
		$('#divMetricas_'+i_cards).show();
		console.log('dos series');
		mostrarDosSeries(graphD.chartDrag, graficaItem.metricas[0],graficaItem.metricas[1]);
		console.log('met '+graficaItem.metricas[1]);
	}
	else{
		if(ocultoDiv==''){
			mostrarUnicaSerie(graphD.chartDrag, graficaItem.metricas[0]);
		}
	}
}
//Fin de cargar graficas guardadas

//montar grafica en el div
function montarGrafica(contenedor, tipoGrafica){
	//var contenedor = $(this).attr('id');
  	console.log('contenedor: ' + contenedor);
  	var graph = new Object();
    //if (cd==0){
    graph = charts.find(x => x.id == contenedor);
    if (graph == undefined){
     	graph={};
      	graph.id = contenedor;
      	/*var options1 = {
		  chart: {
		    type: 'line'
		  },
		  series: [{
		    name: 'sales',
		    data: [30,40,35,50,49,60,70,91,125]
		  }],
		  xaxis: {
		    categories: [1991,1992,1993,1994,1995,1996,1997, 1998,1999]
		  }
		}*/
      	graph.chartDrag = new ApexCharts(document.querySelector("#"+contenedor), options);
      	graph.chartDrag.render();  
      	charts.push(graph);
      	cd=1;	
      	$('#timing'+contenedor).show();
      	$('.arrastre').hide();
      	//mostrarNuevoCard();
    }else{
      	//chartDrag = ApexCharts(document.querySelector(".chartContainer"), options);
      	console.log('encontrado');
    }
    //graph.chartDrag.render();  
    if(tipoGrafica == 'line'){   
      	/*if (cd==0){
	      	graph.chartDrag.render();  
	      	cd=1;	
	    }*/
        graph.chartDrag.updateOptions({
      	  	chart: {
				type: 'line',
				width: "100%",
				height: 350
			},
			series: [{
				name: 'impresions',
				data: [30,40,35,50,49,60,70,91,136] //o aqui la data de consulta ajax
			}],
			redrawOnParentResize: true,
			/*labels: ['impresions','clicks','spend','ecpm','ctr'],
			xaxis: {
				labels: {
					show: true,
				 	rotate: -45,
		            rotateAlways: false,
		            hideOverlappingLabels: true,
		            trim: true
		        },
				categories: [1991,1992,1993,1994,1995,1996,1997, 1998,1999]
			},*/

	    });
		actualizarSeries(graph.chartDrag);  
		//console.log('grafica');
		//console.log(graph.chartDrag);
		var idSelectChart= contenedor.substring(1);
		//mostrarUnicaSerie(graph.chartDrag, $('#metricas_'+idSelectChart+'_1').val());
		$('#'+contenedor).find('.typeChart').text('line');
		arrayGraficas[contenedor]='line';

		/*$(window).resize(function(){
		    $(window).width($(this).width()-100).height($(this).height()-100);
			console.log('ancho '+ $(window).width()-100);    
		});

		$(window).resize();
		console.log('aqui');
		*/
      
    }
    else if(tipoGrafica == 'radialBar'){   
    	/*if (cd==0){
	      	graph.chartDrag.render();  
	      	cd=1;	
	    }*/
    	graph.chartDrag.updateOptions({
		  	chart: {
		      	height: 350,
		      	type: 'radialBar',
		  	},
		  	series: [70],
		  	labels: ['Progress'],
		})
		$('#'+contenedor).find('.typeChart').text('radialBar');
		arrayGraficas[contenedor]='radialBar';
    }
    else if(tipoGrafica == 'bar') {
	    /*if (cd==0){
	      	graph.chartDrag.render();  
	      	cd=1;	
	    }*/
	    graph.chartDrag.updateOptions({
	      	chart: {
				type: 'bar',
				height: 350
				},
			series: [{
				name: 'impresions',
				data: [30,40,35,50,49,60,70,40,60] //o aqui la data de consulta ajax
			}]
			/*xaxis: {
				categories: [1991,1992,1993,1994,1995,1996,1997, 1998,1999]
			}*/
	    });
	    actualizarSeries(graph.chartDrag);
		var idSelectChart= contenedor.substring(1);
		mostrarUnicaSerie(graph.chartDrag, $('#metricas_'+idSelectChart+'_1').val());
		$('#'+contenedor).find('.typeChart').text('bar');
		arrayGraficas[contenedor]='bar';
	}
}
//fin montar grafica en el div
//var elPB;
var resizeOption=function() {
	  var
	    resizableEl = $('.resizable').not(':last-child'),
	    columns = 12,
	    fullWidth = resizableEl.parent().width(),
	    columnWidth = fullWidth / columns,
	    totalCol, // this is filled by start event handler
	    updateClass = function(el, col) {
	    	//console.log(col);
	    	if (col<=3) {
	    		col=3;
	    	}
	    	if (col>3 && col<5) {
	    		col=4;
	    	}
	    	if (col>=5 && col<7){
	    		col=6
	    	}
	    	if (col>=7){
	    		col=12
	    	}
	    	//col=6;
	      el.css('width', ''); // remove width, our class already has it
	      el.removeClass(function(index, className) {
	        return (className.match(/(^|\s)col-\S+/g) || []).join(' ');
	      }).addClass('col-md-' + col);   //ancho de los cuadros
	    };
	  // jQuery UI Resizable
	  resizableEl.resizable({
	    handles: 'e',
	    start: function(event, ui) {
	      var
	        target = ui.element,
	        next = target.next(),
	        targetCol = Math.round(target.width() / columnWidth),
	        nextCol = Math.round(next.width() / columnWidth);
	      // set totalColumns globally
	      totalCol = 12; //targetCol + nextCol;
	      //console.log(totalCol);
	      target.resizable('option', 'minWidth', columnWidth);
	      target.resizable('option', 'maxWidth', ((totalCol - 1) * columnWidth));
	    },
	    resize: function(event, ui) {
	      var
	        target = ui.element,
	        next = target.next(),
	        targetColumnCount = Math.round(target.width() / columnWidth),
	        nextColumnCount = Math.round(next.width() / columnWidth),
	        targetSet = targetColumnCount, //totalCol - nextColumnCount, //targetColumnCount;
	        nextSet = totalCol - targetColumnCount; //nextColumnCount; // 
	        
	      // Just showing class names inside headings
	      //console.log(totalCol);
	      //console.log('targeTt: '+targetSet+' next: '+ nextSet+' columnCount: '+ targetColumnCount);
	      //target.find('h3').text('col-sm-' + targetSet);
	      //next.find('h3').text('col-sm-' + nextSet);

	      updateClass(target, targetSet);
	      //updateClass(next, nextSet);
	    },
	    stop: function( event, ui ) {
	    	//console.log(event);
	    	var elPB=ui.element;
	    	//console.log(elPB);
	    	var idDiv=elPB[0].id.substring(5);
	    	console.log(idDiv);
	    	var contenedorD='c'+idDiv;
			var graphiD; 
			graphD = charts.find(x => x.id == contenedorD);
			/*ocultarSeries();
			chart.toggleSeries($(this).val());
			chart.showSeries($(this).val());*/
			
			//console.log($(this).attr('id'));
			if (!$('#divMetricas_'+idDiv).is(":visible")){
				mostrarUnicaSerie(graphD.chartDrag, $('#metricas_'+idDiv+'_1').val());
			}else{
				mostrarDosSeries(graphD.chartDrag, $('#metricas_'+idDiv+'_1').val(),$('#metricas_'+idDiv+'_2').val());
			}


	    }
	  });
	};


//Para cargar los datos de las graficas
function actualizarSeries(graphChart){
	graphChart.updateSeries([{
  		data: a_impresions
	}]);
	graphChart.appendSeries({
	  name: 'spend',
	  data: a_spend
	});
	graphChart.appendSeries({
	  name: 'ecpm',
	  data: a_ecpm
	});
	graphChart.appendSeries({
	  name: 'clicks',
	  data: a_clicks
	});
	graphChart.appendSeries({
	  name: 'ctr',
	  data: a_ctr
	});
	graphChart.updateOptions({
	  xaxis: {
	  	categories: categoria
	  }
	});

}

cd=0;

//se cambio id por clase para poder hacerlo dinamico
var droppableChartOption={
  drop: function(event) {
  	  var contenedor = $(this).attr('id');
  	  console.log('contenedor: ' + contenedor);
  	  var graph = new Object();
      //if (cd==0){
      graph = charts.find(x => x.id == contenedor);
      if (graph == undefined){
      	graph={};
      	graph.id = contenedor;
      	graph.chartDrag = new ApexCharts(document.querySelector("#"+contenedor), options);
      	graph.chartDrag.render();  
      	charts.push(graph);
      	cd=1;	
      	$('#timing'+contenedor).show();
      	$('.arrastre').hide();
      	mostrarNuevoCard();
      }else{
      	//chartDrag = ApexCharts(document.querySelector(".chartContainer"), options);
      	console.log('encontrado');
      }
      //graph.chartDrag.render();  
      if($(event.originalEvent.toElement).attr("value") == 'dataSource1'){   
      	/*if (cd==0){
	      	graph.chartDrag.render();  
	      	cd=1;	
		}*/
      graph.chartDrag.updateOptions({
      	chart: {
					type: 'line',
					width: "100%",
					height: 350
				},
				series: [{
				name: 'impresions',
				data: [30,40,35,50,49,60,70,91,136] //o aqui la data de consulta ajax
				}],
				/*labels: ['impresions','clicks','spend','ecpm','ctr'],
				xaxis: {
					labels: {
						show: false,
					 	rotate: -45,
			            rotateAlways: false,
			            hideOverlappingLabels: true,
			            trim: true
			        },
					categories: [1991,1992,1993,1994,1995,1996,1997, 1998,1999]
				}*/
      }); 
	  actualizarSeries(graph.chartDrag);  
	  var idSelectChart= contenedor.substring(1);
	  mostrarUnicaSerie(graph.chartDrag, $('#metricas_'+idSelectChart+'_1').val());
	  $('#'+contenedor).find('.typeChart').text('line');
	  arrayGraficas[contenedor]='line';
      
    }
    else if($(event.originalEvent.toElement).attr("value") == 'dataSource2'){   
    	/*if (cd==0){
	      	graph.chartDrag.render();  
	      	cd=1;	
	    }*/
    	graph.chartDrag.updateOptions({
		  chart: {
		      height: 350,
		      type: 'radialBar',
		  },
		  series: [70],
		  labels: ['Progress'],
		})
		$('#'+contenedor).find('.typeChart').text('radialBar');
		arrayGraficas[contenedor]='radialBar';
    }
    else if($(event.originalEvent.toElement).attr("value") == 'dataSource3') {
      /*if (cd==0){
      	graph.chartDrag.render();  
      	cd=1;	
      }*/
      graph.chartDrag.updateOptions({
      	chart: {
					type: 'bar',
					height: 350
				},
				series: [{
				name: 'impresions',
				data: [30,40,35,50,49,60,70,40,60] //o aqui la data de consulta ajax
				}],
				/*xaxis: {
					categories: [1991,1992,1993,1994,1995,1996,1997, 1998,1999]
				}*/
      });
      actualizarSeries(graph.chartDrag);
	  var idSelectChart= contenedor.substring(1);
	  mostrarUnicaSerie(graph.chartDrag, $('#metricas_'+idSelectChart+'_1').val());
	  $('#'+contenedor).find('.typeChart').text('bar');
	  arrayGraficas[contenedor]='bar';
    }         
  }
};
$(".chartContainer").droppable(droppableChartOption);

/*para el resize*/
$(resizeOption);
/*fin resize*/

function cargarGraficasId(graficasId){
	//limpiar contenedor
	//llamada ajax, verificar ruta y parametros
	/*
	$.ajax({
        url: "/api/reporting2/data",  //verificar ruta
        type: "get",
        dataType: "json",
		data: { 'id': graficasId }	 
    }).done(function(res){
            //console.log(res['data']);
            cargarGraficasId(res);
        });
    */
    //Dotos del objetoGlogal cargados (1111)
    objetoSeleccionadoGraficas = objetoGlobalGraficas.find(x => x.id == graficasId);

	/*var objetoGlobalGraficas={
		id: "1111",
		graficas: [
			{id: "c1", titulo: "Líneas", tipoChart: "line", ancho: 25, metricas: ["impresions","spend"], date: "all time"},
			{id: "c2", titulo: "Barras", tipoChart: "bar", ancho: 33, metricas: ["clicks"], date: "all time"},
			{id: "c3", titulo: "líneas", tipoChart: "line", ancho: 33, metricas: ["impresions"], date: "all time"},
			{id: "c4", titulo: "Title", tipoChart: undefined, ancho: 50, metricas: ["impresions"], date: "all time"}
		]
	}*/

	$("#contentCard").empty();
	
	//inicializar charts
	charts=[];

	//colocarg el id en select reports
	//console.log(objetoGlobalGraficas.id);
	$('#reports').val(objetoSeleccionadoGraficas.id);

	//con un foreach de las graficas cargar las graficas.
	//console.log(objetoGlobalGraficas.graficas);
	//var j=0;
	console.log('Cargar div de graficas');
	objetoSeleccionadoGraficas.graficas.forEach(function(elemento){
		//j++;
		console.log('grafica');
		mostrarCardGuardados(elemento);
		//console.log(elemento.ancho);
	});
}

$(document).ready(function(){


	/*boton (+) addMetric dinámico*/
	$('#contentCard').on('click', '.addMetrics', function(){
		var aNombreSelect=$(this).attr('id').split("_");
		var contenedorD='c'+aNombreSelect[1];
		var graphiD; 
		graphD = charts.find(x => x.id == contenedorD);
		//console.log(aNombreSelect);
		$('#divMetricas_'+aNombreSelect[1]).show();
		mostrarDosSeries(graphD.chartDrag,$('#metricas_'+aNombreSelect[1]+'_1').val(),$('#metricas_'+aNombreSelect[1]+'_2').val());

	});

	$('#contentCard').on('change', '.metricas2', function () { 
		//var aNombreSelect = $(this).attr('id'));

		var aNombreSelect=$(this).attr('id').split('_');
		console.log(aNombreSelect);
		var contenedorD='c'+aNombreSelect[1];
		var graphiD; 
		graphD = charts.find(x => x.id == contenedorD);
		if ($(this).val()=='none'){
			$('#divMetricas_'+aNombreSelect[1]).hide();
			mostrarUnicaSerie(graphD.chartDrag,$('#metricas_'+aNombreSelect[1]+'_1').val());
		}else{
			mostrarDosSeries(graphD.chartDrag,$(this).val(),$('#metricas_'+aNombreSelect[1]+'_1').val());
		}
		//console.log(graphD.chartDrag);
	});
	$('#contentCard').on('change','.metricas1', function () { 
		var aNombreSelect=$(this).attr('id').split('_');
		console.log(aNombreSelect);
		var contenedorD='c'+aNombreSelect[1];
		var graphiD; 
		graphD = charts.find(x => x.id == contenedorD);
		/*ocultarSeries();
		chart.toggleSeries($(this).val());
		chart.showSeries($(this).val());*/
		
		//console.log($(this).attr('id'));
		if (!$('#divMetricas_'+aNombreSelect[1]).is(":visible")){
			mostrarUnicaSerie(graphD.chartDrag, $(this).val());
		}else{
			mostrarDosSeries(graphD.chartDrag, $(this).val(),$('#metricas_'+aNombreSelect[1]+'_2').val());
		}
		//console.log(graphD.chartDrag);
	});



	$('#pruebaChart').on('click', function () {
		//para obtener el id del div contenedor
		console.log($(this).closest('div').attr("id")); 
	});

	//para cambiar el url cuando se selecciona otro id
	function modifyState(dato) { 
        let stateObj = { id: "100" }; 
        let direccion = "/reporting2/"+dato;
        window.history.replaceState(stateObj, 
                    "Page 3", direccion); 
    } 

	var bandNombre;
	//Para nuevos reportes a guardar
	$('#reports').on('change', function () {
		//para obtener el id del div contenedor
		if ($('#reports').val()=='nuevo'){
			//asignarNombreReports();
			//colocar bandera de llamado por report.change()
			bandNombre=0;
			$('#kt_modal_nombre').modal('show');
		}else{
			if ($('#reports').val()!=''){
				modifyState($('#reports').val());
				cargarGraficasId($('#reports').val());
			}
		}
	});

	function asignarNombreReports(nombreReporte){
		//var nombreReporte=prompt("Ingrese el nuevo nombre");

		//console.log(nombreReporte);
		if (nombreReporte){
			//Agregar el nombre al select report y seleccionarlo
			$('#reports').append("<option value='"+nombreReporte+"' >"+nombreReporte+"</option>");
			$('#reports').val(nombreReporte);
			$('#reports').selectpicker('refresh');
		}
		return nombreReporte;
	}

	//Botón guardar del modal del nombre de report
	$('#guardaNameDashboard').click(function(){
    	var nombre = $('#dashboard-name').val();
    	console.log(nombre)
    	if (nombre != ''){
    		/*
    		$.ajax({
		        url: "/api/dashboard/save",
		        type: "post",
                dataType: "json",
                data: { 'datos': objetoDashboard, 'nombre': nombre }
		    })
		        .done(function(res){
		            console.log(res['datos']);
		            //mostrarDatos(res['datos']);
		            //mostrar mensaje de datos guardados
		        });*/
		    asignarNombreReports(nombre);
		 	
		 	//var valor=asignarNombreReports(nombre);
			$('#kt_modal_nombre').modal('hide');
			if (bandNombre){
				$("#guardar").click();
			}
    	}else{
    		if (!bandNombre){
    			$('#reports').val('');
    		}
    		$('#kt_modal_nombre').modal('hide');
    	}
    });

	$('#guardar').on('click', function () { 
		/*ocultarSeries();
		
		chart.toggleSeries($(this).val());
		chart.showSeries($(this).val());*/
		if ($('#reports').val()==''){
			//solicitar un nombre para el reporte a guardar
			//alert('indique un nombre para el reporte a guardar');
			//colocar bandera de llamado por grardar.click()
			bandNombre=1;
			$('#kt_modal_nombre').modal('show');	
			//var valor=asignarNombreReports();
			/*
			if (valor){
				$("#guardar").click();
			}*/
			//tomar los datos
			//agregarlo al select reports y colocarlo como selected
		}else{
			//Se recorren los div de las gráficas
			var claseAncho, bAncho, ancho, titulo;
			var graficasSave = [];
			var objGrafica = new Object();
			$(".containerCard").each(function(index,className){
				//console.log($(this).attr('class'));
				var numero = $(this).attr('id').substring(5);
				var metricasSave=[];
				
				var id = 'c'+numero;
				titulo = $(this).find('h5').text();
				tipoGrafica = arrayGraficas[id];
				//console.log(id);
	    	    claseAncho=$(this).attr('class').match(/(^|\s)col-md-\S+/g);
	    	    bAncho = claseAncho[0].trim().substring(7);
	    	    //console.log(bAncho);
	    	    switch (bAncho) {
	    	    case '3':
	    	    	ancho = 25;
	    	    	break;
	    	    case '4':
	    	    	ancho = 33;
	    	    	break;
	    	    case '6':
	    	    	ancho = 50;
	    	    	break;
	    	    case '12':
	    	    	ancho = 100;
	    	    	break;
	    	    }
	    	    //console.log(arrayGraficas);
	    	    //console.log(tipoGrafica);
	    	    metricasSave.push($('#metricas_'+numero+'_1').val());
	    	    if ($('#divMetricas_'+numero).is(":visible")){
					metricasSave.push($('#metricas_'+numero+'_2').val());
				}
				objGrafica={id:id, titulo: titulo, tipoChart: tipoGrafica, ancho:ancho, metricas: metricasSave, date: 'all time'};
				//console.log(objGrafica);
				graficasSave.push(objGrafica);

	    	});
			
			objetoGlobalReport={
				id: $('#reports').val(),
				graficas: graficasSave
			}

			objetoGlobalGraficas.push(objetoGlobalReport);

			console.log(objetoGlobalReport);


    	/*
		var ancho = 100; //debe ir un variable con el valor del ancho (100,50,33,25)
		var tipoChart = 'line'; //debe ir una vrible con el tipo de grafica ('line', ...)
		var datos = eval('a_'+$('#metricas').val());
		//console.log(datos);
		var series= [{
			name: $('#metricas').val(),
			data: datos
		}]
		var categories= categoria;
		var settings = {serie: series, categoria: categories}
		var filas=[[{ancho: ancho, tipoChart: tipoChart, settings: settings}]];
		//console.log('guardar');
		console.log(filas);
		*/
		
		/*aqui va el llamado ajax para guardar la configurcion en bbdd
		$.ajax({
	        url: "/api/reporting2/data",  //verificar ruta
	        type: "post",
	        dataType: "json",
			data: { 'datos': objetoGlobalReport }	 
	    })
	        .done(function(res){
	            console.log(res['data']);
	        });
		*/
		}

	});


	/*updateobjetoglobal('periodo',jQuery('#periodo').val());
	updateobjetoglobal('frecuencia',jQuery('.r_frecuencia').val());
	updateobjetoglobal('grafica',jQuery('.r_grafica').val());
	updateobjetoglobal('clients',jQuery('#select_clients').val());
	updateobjetoglobal('platforms',jQuery('#select_platforms').val());
	updateobjetoglobal('campaigns',jQuery('#select_campaigns').val());*/
	

		//$('#d_cargar_db').click();

		/* Boton guardar (editar opciones) dashboard (configuracion) si es new Report abre un modal para solicitar el nombre */
		/*
    $('#d_guardar').click(function(){
    	var db_id= parseInt($('#s_dashboards').val());
    	if (db_id != -1){
    		//console.log('!=-1');
    		var ruta = "/api/dashboard/save/"+db_id;
    		$.ajax({
		        url: ruta,
		        type: "post",
                dataType: "json",
                data: { 'datos': objetoDashboard }
		    })
		        .done(function(res){
		            console.log(res['datos']);
		            //mostrar mensaje de datos guardados
		            //mostrarDatos(res['datos']);
		        });
	    } else {
	    	$('#kt_modal_nombre').modal('show');
	    }
    });
	*/
    /* Rutina para guardar un dashboard nuevos */
    /* $('#guardaNombreDashboaard').click(function(){
    	var nombre = $('#dashboaard-name').val();
    	console.log(nombre)
    	if (nombre != ''){
    		$.ajax({
		        url: "/api/dashboard/save",
		        type: "post",
                dataType: "json",
                data: { 'datos': objetoDashboard, 'nombre': nombre }
		    })
		        .done(function(res){
		            console.log(res['datos']);
		            //mostrarDatos(res['datos']);
		            //mostrar mensaje de datos guardados
		        });
		 	$('#kt_modal_nombre').modal('hide');
    	}
    });

	$('#periodo').change(function(){
		updateobjetoglobal('periodo',jQuery(this).val());
	});

	$('#s_dashboards').on('change', function() {
		var db_id= parseInt($(this).val());
		console.log(db_id);
	})

$('.r_frecuencia').on('click', function () { 
updateobjetoglobal('frecuencia',jQuery(this).val());
});
$('.r_grafica').on('click', function () { 
updateobjetoglobal('grafica',jQuery(this).val());
cargarGrafica(jQuery(this).val());
});

$('.selectpicker').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) { 
updateobjetoglobal(jQuery(this).data('entidad'),jQuery(this).val()[0]);
});*/
	
});

</script>

@endsection