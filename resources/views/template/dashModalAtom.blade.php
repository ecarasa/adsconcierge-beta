
<!-- Modal Atomo -->
<div class="modal fade" id="modalAtom" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Atom</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="kiki-close"></i>
                </button>
            </div>
            <div class="modal-body">

                <form class="kt-form nw_usr" id="form_edit_atom" method="POST" action="/atom">
                    
                    {{ csrf_field() }}

                    <input type="hidden" name="atom_id" id="atom_id" />
                    
                    
                    @include('template.modalEdition.editionAtom')

                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-basic" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveBtnCustomer" onClick="javascript:saveAtom();">Save</button>
            </div>
        </div>
    </div>
</div>
