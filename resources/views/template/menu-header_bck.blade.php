<!-- begin:: Header -->

						<!-- begin:: Header Menu -->
						<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i>
						</button>
						<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
							<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
								
							
							<ul class="kt-menu__nav ">
									<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel {{ (request()->is('/')) ? 'kt-menu__item--active' : '' }} " data-ktmenu-submenu-toggle="click" aria-haspopup="true">
									<a href="{{route('index')}}" class="kt-menu__link">
										<span class="kt-menu__link-text"><i class="la la-bullhorn"></i>Your Campaigns</span>
									</a>
									</li>
								

										<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel {{ (request()->is('campana')) ? 'kt-menu__item--active' : '' }}" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
											<a href="/campana" class="kt-menu__link">
												<span class="kt-menu__link-text">
													<i class="flaticon2-digital-marketing"></i>
													New Campaign
												</span>
											</a>
										</li>								
								
								
								
									<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel  {{ (request()->is('reporting')) ? 'kt-menu__item--active' : '' }}" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
										<a href="/reporting" class="kt-menu__link">
											<span class="kt-menu__link-text">
												<i class="flaticon2-digital-marketing"></i>
												Reporting
											</span>
										</a>
									</li>								
									<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
										<a href="javascript:;" class="kt-menu__link kt-menu__toggle {{ (request()->is('/settings*')) ? 'kt-menu__item--active' : '' }}">
											<span class="kt-menu__link-text">Settings</span>
										</a>
										<div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
											<ul class="kt-menu__subnav">
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="/users" class="kt-menu__link ">
														<span class="kt-menu__link-text">
															<i class="kt-menu__link-icon far fa-user"></i>Users
														</span>
													</a>
												</li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="/clients" class="kt-menu__link ">
														<span class="kt-menu__link-text">
															<i class="kt-menu__link-icon fas fa-user-friends"></i>Clients
														</span>
													</a>
												</li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="/campaigns" class="kt-menu__link ">
														<span class="kt-menu__link-text">
															<i class="kt-menu__link-icon fas fa-bullhorn"></i>campaigns
														</span>
													</a>
												</li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="/campai" class="kt-menu__link ">
														<span class="kt-menu__link-text">
															<i class="kt-menu__link-icon fas fa-project-diagram"></i>Connections
														</span>
													</a>
												</li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="/ads_accounts" class="kt-menu__link ">
														<span class="kt-menu__link-text">
															<i class="kt-menu__link-icon fas fa-bullhorn"></i>Ads Accounts
														</span>
													</a>
												</li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="/properties" class="kt-menu__link ">
														<span class="kt-menu__link-text">
															<i class="kt-menu__link-icon fas fa-briefcase"></i>Properties
														</span>
													</a>
												</li>
												<?/*<li class="kt-menu__item  kt-menu__item--submenu" data-ktmenu-submenu-toggle="hover" aria-haspopup="true">
													<a href="components_datatable_v1.html" class="kt-menu__link kt-menu__toggle">
														<span class="kt-menu__link-text"><i class="kt-menu__link-icon flaticon2-protected"></i>Roles
														</span>
													</a>
												</li>*/?>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</div>

						<!-- end:: Header Menu -->