<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 7
-->
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>Advertising Manager | {{ $title }}</title>
    <meta name="description" content="Page with empty content">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });

    </script>
    <!--end::Fonts -->
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/css/payment.css') }}" rel="stylesheet" type="text/css"/>
    <!--end::Page Vendors Styles -->
    <!-- aqui va metacsscommon-->
    @include('template.metas')
</head>
<!-- end::Head -->
<!-- begin::Body -->
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-page--loading ">

<!-- begin:: Page -->

<!-- begin:: Header Mobile -->
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
        <a href="index.html">
            <img alt="Logo" src="/assets/media/logos/logo-light.png"/>
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler">
            <span></span></button>
        <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                    class="flaticon-more"></i></button>
    </div>
</div>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root"> c
    <!-- begin:: Aside -->
    <!-- begin:: Aside Menu -->
    <!-- end:: Aside Menu -->
    <!-- end:: Aside -->
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper"
    @if ($title == 'reporting 2')
        {{"style=padding-top:39px"}}
            @endif
    >
        <!-- begin:: Header -->
        <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
        @include('template.menu-header')
        <!-- begin:: Header Menu
						<! begin:: Header Topbar -->
            <!-- Button trigger modal -->

            {{--            <button type="button" class="btn" data-toggle="modal" data-target="#exampleModal">--}}
            {{--                <i class="fas fa-credit-card"></i>--}}
            {{--            </button>--}}
            <div class="kt-header__topbar">
                {{--
				<div class="dropdown row p-1">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        Payment
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item p-4" href="javascript:void(0)" data-cb-type="portal">Manage account</a>
                        <a class="dropdown-item p-4" href="javascript:void(0)" data-toggle="modal"
                           data-target="#changePlanModal">Change plan</a>
                        <a class="dropdown-item p-4" href="javascript:void(0)" data-toggle="modal"
                           data-target="#cancelSubscriptionModal">Cancel Subscription</a>
                    </div>
                </div>
                --}}
				<div class="row">
                    <button class="btn" id="trelloButton"><i class="fas fa-lightbulb"></i></button>
                </div>
                <!--aqui van las notificaciones	 -->
            	@include('template.notificaciones')
            	<!--begin: Quick Actions -->
                <div class="kt-header__topbar-item dropdown">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px"
                         aria-expanded="true">
                    </div>
                </div>
                <!--end: Quick Actions -->
                <!--begin: Quick panel toggler -->
            <?php /*<div class="kt-header__topbar-item kt-header__topbar-item--quick-panel" data-toggle="kt-tooltip" title="Quick panel" data-placement="right">
								<span class="kt-header__topbar-icon" id="kt_quick_panel_toggler_btn">
								</span>
							</div>*/?>
            <!--begin: User Bar -->


                <div class="kt-header__topbar-item kt-header__topbar-item--user">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                        <div class="kt-header__topbar-user">
                            <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                            <span class="kt-header__topbar-username kt-hidden-mobile">
											@if ( isset( Auth::user()->name ) )
                                    {{ Auth::user()->name }}
                                @endif
										</span>
                            <img class="kt-hidden" alt="Pic" src="/assets/media/users/300_25.jpg"/>

                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">
											<!--@if ( isset( Auth::user()->name ) )
                                    {{-- Auth::user()->name[0] --}}
                                @endif-->
										</span>
                        </div>
                    </div>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
                        <!--begin: Head -->
                        <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                             style="background-image: url(../assets/media/misc/bg-1.jpg)">
                            <div class="kt-user-card__avatar">
                                <img class="kt-hidden" alt="Pic" src="/assets/media/users/300_25.jpg"/>

                                <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">
												<!--@if ( isset( Auth::user()->name ) )
                                        {{-- Auth::user()->name[0] --}}
                                    @endif-->
											</span>
                            </div>
                            <div class="kt-user-card__name">
                                @if ( isset( Auth::user()->name ) )
                                    {{ Auth::user()->name }}
                                @endif
                            </div>
                        </div>

                        <!--end: Head -->

                        <!--begin: Navigation -->
                        <div class="kt-notification">
                            <a href="#" class="kt-notification__item">
                                <!--<div class="kt-notification__item-icon">
                                    <i class="flaticon2-calendar-3 kt-font-success"></i>
                                </div>-->
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        Mi perfil
                                    </div>
                                    <div class="kt-notification__item-time">
                                        Configuración y más
                                    </div>
                                </div>
                            </a>


                            <div class="kt-notification__custom">
                                <a href="{{ route('logout') }}" target="_blank"
                                   class="btn btn-label-brand btn-sm btn-bold" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Cerrar sesión</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                      {{ csrf_field() }}
                                </form>
                            </div>
                        </div>

                        <!--end: Navigation -->
                    </div>
                </div>

                <!--end: User Bar -->
            </div>

            <!-- end:: Header Topbar -->
        </div>
    <!-- end:: Header -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

            <!-- begin:: Subheader -->
            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                <div class="kt-subheader__main">
                    @if (isset($isOrganic) and $isOrganic)
                        @yield('subheader')

                    @else
                        <h3 class="kt-subheader__title">
                            {{ $title }}</h3>
                        <span class="kt-subheader__separator kt-hidden"></span>
                    @endif
                </div>
            </div>
            <!-- end:: Subheader -->

            <!-- begin:: Content -->

            <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                @if(session()->has('success'))
                    <div class="alert alert-success alert-elevate" role="alert">
                        <div class="alert-icon">
                            <i class="flaticon-warning kt-font-brand"></i>
                        </div>
                        <div class="alert-text">
                            {{ session()->get('success') }}
                        </div>
                    </div>
                @endif
                @yield('content')
                <link href="/css/campana.css" rel="stylesheet" type="text/css"/>

            </div>

            <!-- end:: Content -->
        </div>

        <!-- begin:: Footer -->
        <div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
        </div>
        <!--end Footer -->
    </div>
</div>
</div>

<!-- Modal -->
	<?/*
<div class="modal fade" id="changePlanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Subscription plan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <select class="btn btn-secondary dropdown-toggle" name="subscriptionPlans" id="subscriptionPlans">
                    <option class="dropdown-item" value="cbdemo_free" @if($user_plan === "cbdemo_free") selected @endif>cbdemo_free</option>
                    <option class="dropdown-item" value="cbdemo_grow" @if($user_plan === "cbdemo_grow") selected @endif>cbdemo_grow</option>
                    <option class="dropdown-item" value="cbdemo_hustle" @if($user_plan === "cbdemo_hustle") selected @endif>cbdemo_hustle</option>
                    <option class="dropdown-item" value="cbdemo_scale" @if($user_plan === "cbdemo_scale") selected @endif>cbdemo_scale</option>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" id="changeSubscriptionPlanButton" class="btn btn-primary">Change plan</button>
            </div>
        </div>
    </div>
</div>
	*/?>
	
<!-- Modal -->
<div class="modal fade" id="cancelSubscriptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cancel subscription</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Are you sure you want to cancel subscription?
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="button" id="cancelSubscriptionButton" class="btn btn-primary">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->

<!-- Aqui va el quick panel -->
@include('template.quickpanel')
<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->
</a>
</div>
</div>

<!-- end::Demo Panel -->

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!-- aqui van los cripts-->
@include('template.scriptsfoot')
@yield('customscript')
<!--begin::Page Vendors(used by this page)
		<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
		<script src="{{ URL::asset('/assets/vendors/custom/gmaps/gmaps.js') }}" type="text/javascript"></script>
-->
<script src="{{ URL::asset('/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}"
        type="text/javascript"></script>


<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{ URL::asset('/assets/app/custom/general/dashboard.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    var loading = new KTDialog({'type': 'loader', 'placement': 'top center', 'message': 'Loading ...'});
</script>
<!--end::Page Scripts -->
<script type="text/javascript">

    $(document).ready(function () {

        $("#trelloButton").on("click", function () {
            window.open(
                "https://trello.com/b/lkVQgSdm/adsconcierge-roadmap",
                '_blank'
            );
        });

    });

    $(document).ready(function () {
        const themes = [
            {
                'main-bg': '#262E3A',
                'secondary-bg': '#3e4b5b',
                'main-text': '#fff',
                'placeholder': '#939aa3',
                'placeholder-focused': '#ccc',
                'primary-btn': '#6EEDB6',
                'btn-text': '#1C2026',
                'primary-btn-focus': 'rgb(70, 203, 145)',
                'invalid': '#FF7C4A',
                'invalid-focused': '#e44d5f',
                'invalid-placeholder': '#FFCCA5',
            }
        ]
        $("input").on("focus", function () {
            $(this).addClass("focus");
        });

        $("input").on("blur", function () {
            $(this).removeClass("focus");
        });

        $("input").on("keyup", function () {
            if ($(this).val()) {
                $(this).removeClass("empty");
                $(this).addClass("val");
            } else {
                $(this).addClass("empty");
                $(this).removeClass("val");
            }
        });
	});     


</script>
<!-- Global app Bundle-->
</body>

<!-- end::Body -->
</html>
