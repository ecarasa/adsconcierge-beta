@extends('template.general')


@section('content')	
<style type="text/css">

	.kt-form.nw_usr{
		width: 50%;
		margin-left: auto;
		margin-right: auto;
	}

	/* Boton crear usuario*/

	button.btn-success.creat{
		background-color: #46B346 !important;
		border-color: #46B346 !important;
	}

</style>
<!--begin::Form-->
<form class="kt-form nw_usr" method="POST" action="{{ $formaction }}" >
	{{ csrf_field() }}
	
	<input type="hidden" value="{{ isset($user->public_id) ? $user->public_id : '' }}" name="id" />
	
	<div class="kt-portlet__body">
		<div class="form-group form-group-last">
			<div class="alert alert-secondary" role="alert">
				<div class="alert-icon"><i class="flaticon-user kt-font-brand"></i></div>
				<div class="alert-text">
				@if ( is_null( $user ) )
					Create new User
				@else
					Edit <b>{{ $user->name }}</b>
				@endif
				</div>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-warning alert-elevate" role="alert">
				<div class="alert-icon">
					<i class="flaticon-warning kt-font-brand"></i>
				</div>
				<div class="alert-text">											
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			</div>
		@endif
		<!-- {% dd(get_defined_vars()) %} -->
		<div class="form-group row">
			<label for="example-text-input" class="col-2 col-form-label">Email</label>
			<div class="col-10">
				<input class="form-control @error('email') is-invalid @enderror" type="email" name="email" value="{{  old('email') ?  old('email') : ( $user ? $user->email : '' ) }}" id="example-text-input" placeholder="Email" readonly required>
			</div>
		</div>
		@if ((Auth::user()->id===$role_user->user_id && $role_user->role_id===2) || ($role_user->role_id===1))
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label" >Name</label>
			<div class="col-10">
				<input class="form-control @error('name') is-invalid @enderror" name="name" type="text" value="{{  old('name') ?  old('name') : ( $user ? $user->name : '' ) }}" id="example-url-input" placeholder="Insert Name" required>
			</div>
		</div>
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label" >Password</label>
			<div class="col-10">
				<input class="form-control @error('password') is-invalid @enderror" name="password" type="text" value="" id="example-url-input" placeholder="Insert Pass">
			</div>
		</div>
		@else
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label" >Name</label>
			<div class="col-10">
				<input class="form-control @error('name') is-invalid @enderror" name="name" type="text" value="{{  old('name') ?  old('name') : ( $user ? $user->name : '' ) }}" id="example-url-input" readonly >
			</div>
		</div>
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label" >Password</label>
			<div class="col-10">
				<input class="form-control @error('password') is-invalid @enderror" name="password" type="text" value="" id="example-url-input" readonly >
			</div>
		</div>
		@endif
		
		<!--
		<div class="form-group row">
			<label for="example-url-input" class="col-2 col-form-label">Account Manager</label>
			<div class="col-10">
				<input class="form-control" type="text" value="" id="example-url-input">
			</div>
		</div>
		-->
		<!--
		<div class="form-group row">
			<label for="example-tel-input" class="col-2 col-form-label">Traffiquer</label>
			<div class="col-10">
				<input class="form-control" type="text" value="" id="example-tel-input">
			</div>
		</div>
		-->
		<!--
		<div class="form-group row">
			<label for="example-number-input" class="col-2 col-form-label">Number</label>
			<div class="col-10">
				<input class="form-control" type="number" value="42" id="example-number-input">
			</div>
		</div>
		-->
		@if (!is_null( $user ) && $user->getLastIdRole() == 1)
			<div class="form-group row ">
				<label for="example-search-input" class="col-2 col-form-label">Rol</label>
				<div class="input-group col-10">
					<input class="form-control" name="rola" type="text" value="{{  $roles[$user->getLastIdRole()]->description }}" id="example-url-input" placeholder="" disabled>
				</div>
			</div>
		@else
			<div class="form-group row ">
				<label for="example-search-input" class="col-2 col-form-label">Rol</label>
				<div class="input-group col-10">
					<select class="form-control m-select2" id="rol" name="rol" required >
						<option disabled value="" selected hidden>Select user role</option>
						@foreach ($roles as $rol)
							<option value="{{ $rol->id }}" @if ( $rol->id == (  old('rol') ?  old('rol') : ( $user ? $user->getLastIdRole() : '' ) )  ) selected  @endif  >{{ $rol->description }}</option>	
						@endforeach
					</select>
				</div>
			</div>
		@endif
		<!--
		<div class="form-group row">
			<label for="example-email-input" class="col-2 col-form-label">Offices</label>
			<div class="col-10">
				<select class="form-control m-select2" id="sele_leng" name="offices" multiple="multiple">
					@foreach ($offices as $o)
						<option value="{{ $o->id }}">{{ $o->office }}</option>	
					@endforeach
				</select>
			</div>
		</div>
		-->
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-2">
				</div>
				<div class="col-10">
					<button type="submit" class="btn btn-success">Submit</button>
					<button type="buttom" class="btn btn-secondary">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</form>



@endsection

@section('customscript')
	<script src="/assets/app/custom/general/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script>
		$(document).ready(function(){
			$('#rol').select2({ placeholder : 'Select a role' });
		});
	</script>
@endsection
